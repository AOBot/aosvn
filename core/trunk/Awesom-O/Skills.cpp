#include "Skills.h"

Skills::Skills()
{
}

Skills::~Skills()
{
}
	
void Skills::add(SKILL skill) throw(...)
{
	cs.Lock();
	list.push_back(skill);
	cs.Unlock();
}

void Skills::remove(SkillType::SkillType skill) throw(...)
{
	cs.Lock();

	for (std::list<SKILL>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->skill == skill)
		{
			list.erase(iter);
			break;
		}
	}

	cs.Unlock();
}

bool Skills::find(SkillType::SkillType skill) throw(...)
{
	bool result = false;
	cs.Lock();

	for (std::list<SKILL>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->skill == skill)
		{
			result = true;
			break;
		}
	}

	cs.Unlock();
	return result;
}

int Skills::getSkillLevel(SkillType::SkillType skill) throw(...)
{
	int result = -1;
	cs.Lock();

	for (std::list<SKILL>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->skill == skill)
		{
			result = static_cast<int>(iter->baseLevel);
			break;
		}
	}

	cs.Unlock();
	return result;
}
