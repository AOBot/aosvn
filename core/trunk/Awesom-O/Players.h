#pragma once

#include "CriticalSection.h"
#include <list>
#include "d2data/CharacterClass.h"
#include "d2data/AreaLevel.h"
#include "Skills.h"
#include "d2data/Players.h"
//#include "Pets.h"
#include <cmath>
#include <vector>

//tolua_begin
struct PLAYER
{
	std::string playerName;
	std::string accountName;
	CharacterClass::CharacterClass charClass;
	unsigned short level;
	short partyID;
	unsigned int uid;	
	short x;
	short y;	
	bool hostiled;
	bool close;
	int life;
	PartyRelationshipType::PartyRelationshipType relationship;
	AreaLevel::AreaLevel area;
	//Pets pets;

	PLAYER()
	{
		uid = 0;
		x = 0;
		y = 0;
		life = 100;
		hostiled = false;
		close = false;
		relationship = PartyRelationshipType::None;
	}

	int getDistance(short x, short y)
	{
		return (int)::sqrt((double)((this->x - x) * (this->x - x) + (this->y - y) * (this->y - y)));
	}

	//Skills skills;
};
//tolua_end

class Players { // tolua_export

private:
	CriticalSection cs;
	std::list<PLAYER> list;
	unsigned int uid;
	
public:
	Players();
	~Players();
	void setUID(unsigned int uid);
	//tolua_begin
	void add(PLAYER player);
	void UpdateSkill(const unsigned char* data);
	bool FindSkill(unsigned int uid, SkillType::SkillType skill);
	void SkillsLog(const unsigned char* data);
	unsigned int account(std::string playerName, std::string accountName);
	void remove(std::string playerName);
	void remove(unsigned int uid);
	PLAYER find(unsigned int uid);
	PLAYER findByPlayerName(std::string playerName);
	PLAYER findPlayerInRadius(short x, short y, int radius);
	void update(unsigned int uid, short x, short y);
	void update(unsigned int uid, short x, short y, bool close);
	void update(unsigned int uid, bool close);
	void update(unsigned int uid, int life, AreaLevel::AreaLevel area);
	void setLife(unsigned int uid, int life);
	void setRelationship(unsigned int uid, PartyRelationshipType::PartyRelationshipType relationship);
	void hostile(unsigned int uid);
	int distance(unsigned int uid, short x, short y);
	unsigned int size();
	std::vector<PLAYER> findByRelationship(PartyRelationshipType::PartyRelationshipType relationship);
};
// tolua_end
	