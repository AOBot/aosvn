#include "BitsReader.h"

BitsReader::BitsReader():index_(0), data_(0){
};
BitsReader::BitsReader(const BYTE *data, size_t sizeInBYTE): data_(data), size_(sizeInBYTE), index_(0){
};
BitsReader::~BitsReader(){
//	if (data_)
//		delete data_;
}
bool BitsReader::skip(size_t bitCount) const{
	if (index_ + bitCount < size_ * BITS_PER_BYTE){
		index_ += bitCount;
		return true;
	}
	else
		return false;
};

unsigned int BitsReader::read(std::size_t bits) const{
	unsigned int result = 0;
	getBits(bits, &result);
	return result;
}

bool BitsReader::getBits(size_t bitCount, unsigned int *result) const{
	return getBits(index_, bitCount, result);
}
bool BitsReader::getBits(size_t pos, const size_t bitCount, unsigned int *result) const{
	*result = 0;
	if (bitCount > sizeof(unsigned int) * BITS_PER_BYTE)
		return false;
	if (pos + bitCount > size_ * BITS_PER_BYTE)
		return false;

	size_t bitIndexInResult = 0;
	size_t startBYTEIndex = pos / BITS_PER_BYTE;
	size_t startBitIndex = pos % BITS_PER_BYTE;//index within its own BYTE, not global index

	unsigned int res = *((unsigned int *) &data_[startBYTEIndex]);
	//see if all bits can be read at one time
	if (bitCount > (sizeof(unsigned int) * BITS_PER_BYTE - startBitIndex)){//cannot read all at a time
		*result = res >> startBitIndex;
		unsigned int remainedBitCount = bitCount - ((sizeof(unsigned int) * BITS_PER_BYTE - startBitIndex));
		res = *((unsigned int *) &data_[startBYTEIndex + sizeof(unsigned int)]);
		res <<= (sizeof(unsigned int) * BITS_PER_BYTE - remainedBitCount);
		res >>= (sizeof(unsigned int) * BITS_PER_BYTE - bitCount);
		*result |= res;
	}
	else {
		res <<= sizeof(unsigned int) * BITS_PER_BYTE - bitCount - startBitIndex;//get rid of high bits
		*result = res >> (sizeof(unsigned int) * BITS_PER_BYTE - bitCount);//get rid of low bits
	}

	index_ = pos + bitCount;
	return true;
}
void BitsReader::printBits(int bits){
	int i = sizeof(bits) * BITS_PER_BYTE -1;//no output
	//while (i >= 0 && (bits & (1 << i)) == 0)//skip all zeros in the beginning
	//i--;
	while (i>= 0){
		putchar( bits & (1 << i--) ? '1' : '0');
		if ((i + 1) % BITS_PER_BYTE == 0)
			putchar(' ');
	}
	putchar('\n');

}

BYTE BitsReader::getByte() const{
	unsigned int result = 0;
	getBits(BITS_PER_BYTE, &result);
	return (BYTE) result;
}



WORD BitsReader::getWord() const{
	unsigned int result = 0;
	getBits(BITS_PER_WORD, &result);
	return (BYTE) result;
}

DWORD BitsReader::getDword() const{
	unsigned int result = 0;
	getBits(BITS_PER_INT, &result);
	return result;
}

size_t BitsReader::getDataSize() const{
	return size_;
}

size_t BitsReader::getHandledBitCount() const{
	return index_;
}
