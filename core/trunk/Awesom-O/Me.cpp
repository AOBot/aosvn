#include "Me.h"

Me::Me()
{
	uid = 0;
	waypoints.clear();
	corpseid = 0;
	gold = 0;
	tps = 0;
	ids = 0;
	experience = 0;
	gameExperience = 0;
	goldbank = 0;
	act = 0;
	x = 0;
	y = 0;
	life = 0;
	stamina = 0;
	mana = 0;
	baselife = 0;
	maxlife = 0;
	basemana = 0;
	maxmana = 0;
	level = 0;
	corpseid = 0;
	bolife = 0;
	bomana = 0;
	oaklife = 0;
	oakmana = 0;
	wolflife = 0;
	wolfmana = 0;
	bearlife = 0;
	bearmana = 0;
	weaponset = 0;
	lastValid.x = 0;
	lastValid.y = 0;
}

void Me::setSkill(SkillHand::SkillHand hand, SkillType::SkillType skill)
{
	if (hand == SkillHand::Right)
	{
		right = skill;
	}
	else if (hand == SkillHand::Left)
	{
		left = skill;
	}
}

SkillType::SkillType Me::getSkill(SkillHand::SkillHand hand)
{
	if (hand == SkillHand::Right)
	{
		return right;
	}
	else if (hand == SkillHand::Left)
	{
		return left;
	}

	return SkillType::Invalid;
}
