#include "Cube.h"

std::vector<ITEM> Cube::get()
{
	std::vector<ITEM> vector;

	for (std::map<unsigned int, ITEM>::iterator iter = cube.begin(); iter != cube.end(); iter++)
	{
		vector.push_back(iter->second);
	}

	return vector;
}

bool Cube::Check(ITEM * item, int x, int y)
{
	short width = item->baseItem->invWidth;
	short height = item->baseItem->invHeight;

	for (int w = x; w < x + width; w++)
	{
		for (int h = y; h < y + height; h++)
		{
			if (map[w][h] != 0)
			{
				return false;
			}
		}
	}

	return true;
}

POINT Cube::findLocation(ITEM item)
{
	POINT result;
	result.x = -1;
	result.y = -1;

	for (int x = 0; x < 3; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			map[x][y] = 0;
		}
	}

	for (std::map<unsigned int, ITEM>::iterator iter = cube.begin(); iter != cube.end(); iter++)
	{
		for (int x = iter->second.x; x < iter->second.x + iter->second.baseItem->invWidth; x++)
		{
			for (int y = iter->second.y; y < iter->second.y + iter->second.baseItem->invHeight; y++)
			{
				map[x][y] = 1;
			}
		}
	}

	short width = item.baseItem->invWidth;
	short height = item.baseItem->invHeight;

	for (int x = 0; x <= 3 - width; x++)
	{
		for (int y = 0; y <= 4 - height; y++)
		{
			if (Check(&item, x, y))
			{
				result.x = x;
				result.y = y;

				return result;
			}
		}
	}

	return result;
}

void Cube::add(ITEM item)
{
	cube[item.uid] = item;
}

void Cube::remove(ITEM item)
{
	cube.erase(item.uid);
}

std::vector<ITEM> Cube::findByCode(std::string code)
{
	std::vector<ITEM> result;

	for (std::map<unsigned int, ITEM>::iterator iter = cube.begin(); iter != cube.end(); iter++)
	{
		if (iter->second.baseItem->code.compare(code) == 0) 
		{
			result.push_back(iter->second);
		}
	}
	return result;
}
