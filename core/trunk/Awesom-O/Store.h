#pragma once

#include "CriticalSection.h"
#include <list>
#include <vector>
#include "Item.h"

class Store { // tolua_export

private:
	CriticalSection cs;
	std::list<ITEM> list;

public:
	Store();
	~Store();
	//tolua_begin
	void add(ITEM item);
	void clear();
	ITEM findBaseItem(std::string code);
	std::vector<ITEM> gamble();
	std::vector<ITEM> buy();
};
// tolua_end
