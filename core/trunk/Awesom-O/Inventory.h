#pragma once

#include "Item.h"
#include <windows.h>
#include <vector>
#include <map>

class Inventory { // tolua_export

private:
	std::map<unsigned int, ITEM> inventory;
	bool map[10][4];
	bool Check(ITEM * item, int x, int y);

public:
	//tolua_begin
	std::vector<ITEM> get();
	POINT findLocation(ITEM item);
	void add(ITEM item);
	void remove(ITEM item);
	ITEM find(unsigned int uid);
	std::vector<ITEM> findByCode(std::string code);
	int countColumns();
};

// tolua_end
