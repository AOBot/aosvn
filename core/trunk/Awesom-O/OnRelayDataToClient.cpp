#include "AwesomO.h"
#include "BitReader.h"
#include "ItemReader.h"
#include "NpcReader.h"
#include "Log.h"
#include "d2data/Players.h"
#include "d2data/NPCCode.h"
#include "d2data/Quests.h"
#include "BitsReader.h"

extern long CalculateAngle(long x1, long y1, long x2, long y2);


int AwesomO::OnRelayDataToClient(unsigned char * data, int size)
{

	const unsigned char packetId = data[0];

	if (waiting && waiting == packetId && waiting != 0x63)
	{
		waiting = 0;
	}

	if (pingsent > 0)
	{
		if(GetTickCount() - pingsent > lagpause && !(lagcheck))
		{
			ping = GetTickCount() - pingsent;
			lagcheck = true;
		}
	}

	switch (packetId)
	{
		case 0x05: // Unload Done
		case 0xB0: // Game Over
		{
			if (exiting)
			{
				return 0;
			}
		}
		break;
		case 0x03: // Load Act
		{
			unsigned int mapid = *reinterpret_cast<const unsigned int*>(data + 2);
			AreaLevel::AreaLevel level = (AreaLevel::AreaLevel) data[6];

			switch (data[1])
			{
				case 0x00: me->act = 1; break;
				case 0x01: me->act = 2; break;
				case 0x02: me->act = 3; break;
				case 0x03: me->act = 4; break;
				case 0x04: me->act = 5; break;
			}

			maps->setMapId(mapid, difficulty);
			maps->setLevel(level);

			LOG(logDEBUG, this) << "Level = " << maps->getLevel();
		}
		break;
		case 0x07: // Map Add
		{
			int x = *reinterpret_cast<const int*>(data + 1);
			int y = *reinterpret_cast<const int*>(data + 3);
			AreaLevel::AreaLevel level = (AreaLevel::AreaLevel) data[5];

			maps->loadMap(me->act, level);
			maps->mapAdd(level, x, y);
			set("mapAddArea", level);
			Event("mapAdd", true);
		}
		break;
		case 0x08: // Map Remove
		{
			int x = *reinterpret_cast<const int*>(data + 1);
			int y = *reinterpret_cast<const int*>(data + 3);
			AreaLevel::AreaLevel level = (AreaLevel::AreaLevel) data[5];
			
			maps->mapRemove(level, x, y);
		}
		break;
		case 0x01: // Game Flags 
		{
			difficulty = *reinterpret_cast<const unsigned char*>(data + 1);
			expansion = (*reinterpret_cast<const unsigned char*>(data + 6)) > 0 ? true : false;

			me->stash.expansion = expansion;
		}
		break;
		case 0x0b: // Game Handshake
		{
			me->uid = *reinterpret_cast<const unsigned int*>(data + 2);
			players->setUID(me->uid);
		}
		break;
		case 0x0c: // NPC Get Hit
		{
			UnitType::UnitType type = (UnitType::UnitType) data[1];

			if (type == UnitType::NPC)
			{
				unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 2);

				if (merc->uid == uid)
				{
					merc->life = data[8] * 100 / 128;
					//Chat("merc life = %d", merc->life);
					Event("mercLife", true);
				}				
			}
		}
		break;
		case 0x0d: // Player Stop
		{
			UnitType::UnitType type = (UnitType::UnitType) data[1];

			if (type == UnitType::Player)
			{
				unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 2);
				int life = data[12];
				short x = *reinterpret_cast<const short*>(data + 7);
				short y = *reinterpret_cast<const short*>(data + 9);

				players->setLife(uid, life);
				players->update(uid, x, y);
				if (uid == me->uid) {
					me->x = x;
					me->y = y;
				}

			}
		}
		break;
		case 0x5a: // Player Info (Left, Dropped, Died etc)
		{
			const char* playerName = reinterpret_cast<const char*>(data + 8);
			const char* accountName = reinterpret_cast<const char*>(data + 24);

			switch (data[1])
			{
				case 0x02:
				{
					players->account(playerName, accountName);
				}
				break;
				case 0x00:
				case 0x03:
				{
					set("playerName", playerName);
					set("playerUID", players->findByPlayerName(playerName).uid);
					Event("playerExit", true);

					LOG(logINFO, this) << "Player " << playerName << " Left Game";

					players->remove(playerName);
				}
				break;
				case 0x06:
				{
					set("playerName", playerName);
					set("playerUID", players->findByPlayerName(playerName).uid);
					Event("slain", true);
				}
				break;
				case 0x12:
				{
					LOG(logINFO, this) << "Diablo Walks The Earth";
					Event("dClone", true);
				}
				break;
			}
		}
		break;
		case 0x5b: // Player In Game
		{
			char playerName[20];
			strcpy_s(playerName, sizeof(playerName), reinterpret_cast<const char*>(data + 8));

			PLAYER player;
			player.uid = *reinterpret_cast<const int*>(data + 3);
			player.charClass = (CharacterClass::CharacterClass) data[7];
			
			player.level = *reinterpret_cast<const short*>(data + 24);
			player.partyID = *reinterpret_cast<const short*>(data + 26);
			player.playerName = playerName;
			players->add(player);

			LOG(logINFO, this) << "Player " << playerName << " In Game";

			set("playerUID", player.uid);
			Event("playerJoin", false);
		}
		break;
		case 0x63: // Open Waypoint
		{
			unsigned int uid = *reinterpret_cast<const int*>(data + 1);
			
			me->waypoints.clear();
			
			BitReader br(data, 7);
				
			for (int i = 0; i < 39; i++)
			{
				me->waypoints.push_back(br.ReadBoolean(1));
			}
			
			if (waiting == 0x63)
				waiting = 0; // wait for this packet for full wp list

			if (IsRunning())
			{
				WaypointInteract(uid, WaypointDestination::CloseWaypoint);
				//LOG(logINFO, this) << "keeping waypoint hidden";
				return 0;
			}						
		}
		break;
		case 0x09: // Assign Warp
		{
			WARP warp;

			warp.unitType = (UnitType::UnitType) data[1];
			warp.uid = *reinterpret_cast<const unsigned int*> (data + 2);
			warp.id = (WarpType::WarpType) data[6];
			warp.x = *reinterpret_cast<const short*>(data + 7);
			warp.y = *reinterpret_cast<const short*>(data + 9);

			warps->add(warp);
		}
		break;
		case 0x0a: // Remove Game Object
		{
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 2);

			switch (data[1])
			{
				case 0x00: // Player
				{
					players->update(uid, false);
				}
				break;
				case 0x01: // NPC
				{
					NPC npc = npcs->find(uid);
					if (npc.uid != 0 && !isTownFolk(npc.id))
						npcs->remove(uid);
				}
				break;
				case 0x02: // Game Object
				{
					objects->remove(uid);
				}
				break;
				case 0x04: // Item
				{
				    if(waiting_uid  == *reinterpret_cast<const unsigned int*>(data + 2)) // RemoveGroundUnit (covers gold too + faster than WorldItem WorldItemAction)
				    {
				 		//LOG(logDEBUG1, this) << "#" << waiting_uid << " <- was picked...";
						waiting_uid = 0;
				    }
					items->remove(uid);
				}
				break;
				case 0x05: // Warp
				{
					warps->remove(uid);
				}
				break;
			}
		}
		break;
		case 0x0e: // Set Game Object Mode
		{
			UnitType::UnitType unitType = (UnitType::UnitType) data[1];
			unsigned int uid = *reinterpret_cast<const int*> (data + 2);
			bool canChangeBack = data[7] == 0x00 ? false : true;
			GameObjectMode::GameObjectMode objectMode = (GameObjectMode::GameObjectMode) *reinterpret_cast<const int*> (data + 8);
    
			objects->SetGameObjectMode(uid, canChangeBack, objectMode);
		}
		break;
		case 0x0f: // Player Move
		{
			unsigned int uid = *reinterpret_cast<const int*>(data + 2);
			short x = *reinterpret_cast<const short*>(data + 7);
			short y = *reinterpret_cast<const short*>(data + 9);
			MovementType::MovementType type = (MovementType::MovementType) data[6];

			players->update(uid, x, y);
			if (uid == me->uid) //err...
			{
				me->x = x;
				me->y = y;
			}
			//set("playermoveuid", uid);
			//set("playermovex", x);
			//set("playermovey", y);
			//Event("playermove", true);
		}
		break;
		case 0x11: // Report Kill
		{
			UnitType::UnitType unitType = (UnitType::UnitType) data[1];
			unsigned int uid = *reinterpret_cast<const int*>(data + 2);

			//npcs->life(uid, 0);
		}
		break;
		case 0x15: // Player Reassign
		{
			unsigned int uid = *reinterpret_cast<const int*>(data + 2);
			short x = *reinterpret_cast<const short*>(data + 6);
			short y = *reinterpret_cast<const short*>(data + 8);

			if (data[10] == 0x01) // Reassign true
			{
				switch (data[1])
				{
					case 0x00: // Player Reassign
					{
						players->update(uid, x, y);

						if (uid == me->uid)
						{
							me->x = x;
							me->y = y;
							maps->setPosition(x, y);
						}

						if (uid == merc->uid)
						{
							merc->x = x;
							merc->y = y;
						}

						if (uid == me->uid && data[10] == 0x01) // Anti Flash
						{
							data[10] = 0x00;
						}
					}
					//set("playerreassignuid", uid);
					//set("playerreassignx", x);
					//set("playerreassigny", y);
					//Event("playerreassign", true);
					break;
					case 0x01: // NPC Reassign
					{
						//Chat(_chatName, false, "NPC Reassign %u --> (%u,%u)", uid, x, y);
						npcs->update(uid, x, y);
						npcs->update(uid, NPCMode::Alive);
					}
					break;
				}
			}
		}
		break;
		case 0x18: //POT
		{
			short x = 0, y = 0;

			BitReader  br(data, 1);			// Skip 0x18
			me->life = br.ReadUInt16(15);	// LIFE
			me->mana = br.ReadUInt16(15);	// MANA
			me->stamina = br.ReadUInt16(15); // STAMINA
			br.ReadUInt16(14);
			me->x = br.ReadUInt16(15);
			br.ReadUInt16(1);
			me->y = br.ReadUInt16(15);
			//printf("X: %u Y: %u\n", x, y);
			maps->setPosition(me->x, me->y);

			Event("lifeMana", true);
		};
		break;
		case 0x19: // Small Gold Add
		{
			me->gold += data[1];
			//Chat("Gold: %u", me->gold);
		}
		break;
		case 0x1a: // GS: 0x1A ByteToExperience; Experience: 237 Exp add
		{
			//GS: 2    1a ed
			int experience = data[1];
			me->experience += experience;
			me->gameExperience += experience;
			//Chat("gameExp BYTE: %u", me->gameExperience);
		}
		break;
		case 0x1b: // GS: 0x1B WordToExperience; Experience: 13050 Exp add
		{
			//GS: 3    1b fa 32
			unsigned short experience = data[1] + (data[2] << 8);
			me->experience += experience;
			me->gameExperience += experience;
			//Chat("gameExp WORD: %u", me->gameExperience);
		}
		break;
		case 0x1c: //GS: 0x1C DWordToExperience; Experience: 1101495352 Total Exp (maybe was a high number and they prefered to send total exp instead of having client calculate it? (they'd have to send a DWORD anyway)
		{
			//GS: 5    1c 38 7c a7 41 (inverse byte order?)
			DWORD experience = data[1] + (data[2] << 8) + (data[3] << 16) + (data[4] << 24);
			if (me->experience > 0)
				me->gameExperience += (experience - me->experience);
			else
				me->gameExperience = 0;
			me->experience = experience;
			//Chat("gameExp DWORD: %u", me->gameExperience);
		}
		break;
		case 0x1d: // Attribute Byte
		{
			if (data[1] == StatType::Gold)
			{
				me->gold = data[2];
				//Chat("Gold: %u", me->gold);
			}
         else if (data[1] == StatType::GoldBank)
			{
				me->goldbank = data[2];
				//Chat("Gold Bank: %u", me->goldbank);
			}
         else if (data[1] == StatType::Level)
         {
            me->level = data[2];
			Event("levelUp", true);
         }
		}
		break;
		case 0x1e: // Attribute Word
		{
			if (data[1] == StatType::MaxLife && !me->weaponset)
			{
				int part1 = data[2];
				int part2 = data[3];

				if (part1)
				{
					if (part1 >= 0x80)
					{
						part1-=0x80;
						//Chat("Your life was flagged with 0x80!");

						me->baselife = (part1 << 8) | data[3];
					}
					else
					{
						me->baselife = *reinterpret_cast<const short*>(data + 2);
					}
				}
				else
				{
					me->baselife = part2;
				}

				me->maxlife = me->baselife;
				//Chat("Base Life: %u", me->baselife);
			}

			if (data[1] == StatType::MaxMana && !me->weaponset)
			{
				int part1 = data[2];
				int part2 = data[3];

				if (part1)
				{
					if (part1 >= 0x80)
					{
						part1-=0x80;
						//Chat("Your mana was flagged with 0x80!");

						me->basemana = (part1 << 8) | data[3];
					}
					else
					{
						me->basemana = *reinterpret_cast<const short*>(data + 2);
					}
				}
				else
				{
					me->basemana = part2;
				}

				me->maxmana = me->basemana;
				//Chat("Base Mana: %u", me->basemana);
			}

			if (data[1] == StatType::Gold)
			{
				me->gold = *reinterpret_cast<const unsigned short*>(data + 2);
				//Chat("Gold: %u", me->gold);
			}

			if (data[1] == StatType::GoldBank)
			{
				me->goldbank = *reinterpret_cast<const unsigned short*>(data + 2);
				//Chat("Gold Bank: %u", me->goldbank);
			}
		}
		break;
		case 0x1f: // Attribute DWord
		{
			if (data[1] == StatType::MaxLife && !me->weaponset)
			{
				int part1 = data[2];
				int part2 = data[3];
				int part3 = data[4];
				int part4 = data[5];

				if (part1)
				{
					if (part1 >= 0x80)//remove 0x80..damn this flag!
					{
						me->baselife = part2+part3*256+part4*2*256;
					}
					else
					{
						me->baselife = *reinterpret_cast<const int*>(data + 2);
					}
				}
				else
				{
					me->baselife = part2+part3*256+part4*2*256;
				}

				me->maxlife = me->baselife;
				//Chat("Base Life: %u", me->baselife);
			}

			if (data[1] == StatType::MaxMana && !me->weaponset)
			{
				int part1 = data[2];
				int part2 = data[3];
				int part3 = data[4];
				int part4 = data[5];

				if (part1)
				{
					if (part1 >= 0x80)//remove 0x80..damn this flag!
					{
						me->basemana = part2+part3*256+part4*2*256;
					}
					else
					{
						me->basemana = *reinterpret_cast<const int*>(data + 2);
					}
				}
				else
				{
					me->basemana = part2+part3*256+part4*2*256;
				}

				me->maxmana = me->basemana;
				//Chat("Base Mana: %u", me->basemana);
			}

			if (data[1] == StatType::Gold)
			{
				me->gold = *reinterpret_cast<const unsigned int*>(data + 2);
				//Chat("Gold: %u", me->gold);
			}

			if (data[1] == StatType::GoldBank)
			{
				me->goldbank = *reinterpret_cast<const unsigned int*>(data + 2);
				//Chat("Gold Bank: %u", me->goldbank);
			}
		}
		break;
		case 0x21: // Update Skill
		{
			unsigned int uid = *reinterpret_cast<const int*> (data + 3);

			if (uid == me->uid)
			{
				SKILL skill;
				skill.skill = (SkillType::SkillType) *reinterpret_cast<const short*> (data + 7);
				skill.baseLevel = data[9];

				if (!skills->find(skill.skill))
				{
					LOG(logINFO, this) << "Set Skill" << skill.skill;
					skills->add(skill);
				}
			}
		}
		break;
		case 0x22: // Update Skill Status 
		{
			if (data[7] == SkillType::BookOfTownportal)
			{
				me->tps = data[9];
			}
			if (data[7] == SkillType::BookOfIdentify)
			{
				me->ids = data[9];
			}
		}
		break;
		case 0x23: // Assign Skill
		{
			UnitType::UnitType unitType = (UnitType::UnitType) data[1];
			unsigned int uid = *reinterpret_cast<const int*> (data + 2);
			SkillHand::SkillHand hand = (SkillHand::SkillHand) data[6];
			SkillType::SkillType type = (SkillType::SkillType) *reinterpret_cast<const short*> (data + 7);
			unsigned int chargedItemUID = *reinterpret_cast<const int*> (data + 9);

			if (uid == me->uid)
			{
				me->setSkill(hand, type);
			}
		}
		break;
		case 0x26: // Game Message Received
		{
			const char* playerName = reinterpret_cast<const char*>(data + 10);
			int charNameOffset = int(strlen((char*)playerName))+11;
			const char* text = reinterpret_cast<const char*>(data + charNameOffset);

			set("player", playerName);
			set("message", text);
			Event("playerChat", false);
		}
		break;
		case 0x28: // Update Quest Info 
			for (int i = 0; i < 41; i++)
			{
				me->Quests[i].type = (QuestType::QuestType)i;
				me->Quests[i].state = (QuestState::QuestState)data[6 + i * 2];
				me->Quests[i].standing = (QuestStanding::QuestStanding)data[7 + i * 2];
			}
			if (IsRunning())
			{
				LOG(logINFO, this) << "keeping quest info hidden";
				return 0;
			}
			break;
		case 0x29: // Update Game Quest Log
		{
			if (IsRunning())
			{
				LOG(logINFO, this) << "keeping update quest log hidden";
				return 0;
			}
		}
		break;
		case 0x3e: //UpdateItemStats. Mad hacks....
		{				
			int amount = (data[7] >> 5) + (data[8] << 3);
			unsigned int uid;
			std::stringstream ss;
			std::string s, st;

			__int64 d = *reinterpret_cast<const __int64*>(data + 1)&0xFFFFFFFFFFFF;
			d = d << 2;

			ss << std::hex << d;
			ss >> s;

			if (s.substr(0,2).compare("'11'"))
			{
				ss.clear();
				ss << "";
				st = s.substr(2, 8);
				ss << std::hex << st;
				ss >> uid;
				me->body.repair(uid);
			}

			s.clear();
			ss.clear();
		}
		break;
		case 0x3f: // Use Stackable Item
		{
			if (IsRunning())
			{
				LOG(logINFO, this) << "using stackable";
				return 0;
			}
		}
		break;
		case 0x42: // Player Clear Cursor
		{
			me->cursor.uid = 0;
		}
		break;
		case 0x4c: // Unit Use Skill On Target
		{
			/* // Example from wave2
			GS: 0x4C UnitUseSkillOnTarget; 
				UnitType: NPC; 
				UID: 2647036716; 
				Skill: Resurrect2; 
			--> TargetUID: 661755083; 
				Unknown8: 08 01; Unknown14: 00 00
			GS: 0x4D UnitUseSkill; 
				UnitType: NPC; 
			--> UID: 661755083; 
				Skill: Skeletonraise; 
				X: 0; Y: 0; 
				Unknown10: 08; Unknown15: 00 00
			*/
			SkillType::SkillType skill = (SkillType::SkillType)*reinterpret_cast<const short*>(data + 6);
			unsigned int uid = *reinterpret_cast<const int*> (data + 2);
			unsigned int target = *reinterpret_cast<const int*> (data + 10);
			UnitType::UnitType unitType = (UnitType::UnitType) data[1];	
			if (target == me->uid)
			{
				set("attackeruid", uid);
				set("skill", skill);
				Event("attacked", true);
			}
			//	Merc's faith/etc revives arent cast. 
			//	Everytime a revive spawns due to merc's item, the dead npcs "selfcasts" skeletonraise and reborn as a new npc.
			//	New UID and all.
		}
		break;
		case 0x4d: // Unit Use Skill
		{
			SkillType::SkillType skill = (SkillType::SkillType)*reinterpret_cast<const int*>(data + 6);
			unsigned int uid = *reinterpret_cast<const int*> (data + 2);
			UnitType::UnitType unit = (UnitType::UnitType)*reinterpret_cast<const int*>(data + 6);
			if (skill == SkillType::BaalMonsterSpawn)
			{
				baallevel++;
				preattack = 0;
			}
			if (skill == SkillType::Skeletonraise)
			{
				NPC npc = npcs->find(uid);
				if (npc.mode == NPCMode::Dead)
					npcs->update(uid, NPCMode::Alive);
			}
		}
		break;
		case 0x51: // Assign Game Object
		{
			OBJECT object;

			object.uid = *reinterpret_cast<const int*> (data + 2);
			object.objectID = (GameObjectID::GameObjectID) *reinterpret_cast<const short*>(data + 6); 
			object.x = *reinterpret_cast<const short*> (data + 8); 
			object.y = *reinterpret_cast<const short*> (data + 10); 
			object.objectMode = (GameObjectMode::GameObjectMode) data[12];
			object.ownerUID = -1;

			if (object.objectID == GameObjectID::TownPortal || object.objectID == GameObjectID::PermanentTownPortal)
			{
				object.interactType = GameObjectInteractType::TownPortal;
				object.destination = (AreaLevel::AreaLevel) data[13];
			}
			else
			{
				object.interactType = (GameObjectInteractType::GameObjectInteractType) data[13];
				object.destination = AreaLevel::None;
			}

			objects->add(object);
		}
		break;
		case 0x59: // Assign Player
		{
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 1);
			short x = *reinterpret_cast<const short*>(data + 22);
			short y = *reinterpret_cast<const short*>(data + 24);

			players->update(uid, x, y, true);

			if (me->uid == 0)
			{
				me->uid = uid;
				players->setUID(me->uid);
			}
		}
		break;
		case 0x67: // NPC Move
		{
	        unsigned int uid = *reinterpret_cast<const int*>(data + 1);
			short x = *reinterpret_cast<const short*>(data + 6);
	        short y = *reinterpret_cast<const short*>(data + 8);
			NPC npc = npcs->find(uid);

			npcs->update(uid, x, y);
			npcs->update(uid, NPCMode::Alive);

		}
		break;
		case 0x68: // NPC Move To Object
		{
	        unsigned int uid = *reinterpret_cast<const int*>(data + 1);
	        char movementType = data[5];
	        short currentX = *reinterpret_cast<const short*>(data + 6);
	        short currentY = *reinterpret_cast<const short*>(data + 8);
			UnitType::UnitType targetType = (UnitType::UnitType) data[10];
	        unsigned int targetUID = *reinterpret_cast<const int*>(data + 11);

			switch (targetType)
			{
				case UnitType::NPC:
				{
					NPC npc = npcs->find(targetUID);

					npcs->update(uid, npc.x, npc.y);
					npcs->update(uid, NPCMode::Alive);
				}
				break;
			}
		}
		break;
		case 0x69: // Set NPC Mode
		{
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 1);
			NPCMode::NPCMode mode = (NPCMode::NPCMode) data[5];
			short x = *reinterpret_cast<const short*>(data + 6);
			short y = *reinterpret_cast<const short*>(data + 8);
			char life = data[10];

			switch (mode)
			{
				case NPCMode::Alive:
				{
					npcs->update(uid, x, y, life, mode);
				}
				break;
				case NPCMode::Dying:
				case NPCMode::Dead:
				{
					npcs->update(uid, x, y, 0, mode);

					if (uid == merc->uid) // If its my merc, set him to dead
					{
						merc->uid = 0;
						Event("mercDeath", true);
					}
				}
				break;
			}
		}
		break;
		case 0x6c: // Monster Attack
		{
			unsigned int uid = *reinterpret_cast<const int*>(data + 1);
			short attackType = *reinterpret_cast<const short*>(data + 5);
			unsigned int targetUID = *reinterpret_cast<const int*>(data + 7);
			UnitType::UnitType targetType = (UnitType::UnitType) data[11];
			short x = *reinterpret_cast<const short*>(data + 12);
			short y = *reinterpret_cast<const short*>(data + 14);

			npcs->update(uid, x, y);
			if(targetUID == me->uid)
			{
				set("attackeruid", uid);
				set("skill", attackType);
				Event("attacked", true);
			}
		}
		break;
		case 0x6d: // NPC Stop
		{
			unsigned int uid = *reinterpret_cast<const int*>(data + 1);
			short x = *reinterpret_cast<const short*>(data + 5);
			short y = *reinterpret_cast<const short*>(data + 7);
			unsigned char life = data[9];
			NPC npc = npcs->find(uid);

			npcs->update(uid, x, y, life);
			npcs->update(uid, NPCMode::Alive);

			if (uid == merc->uid)
			{
				merc->life = life * 100 / 128;
				Event("mercLife", true);
				//Chat("merc life = %d", merc->life);
			}
			
		}
		break;
		case 0x75: // About Player
		{
			unsigned int uid = *reinterpret_cast<const int*>(data + 1);
			unsigned int partyid = *reinterpret_cast<const short*>(data + 5);
			PlayerRelationshipType::PlayerRelationshipType relationship = (PlayerRelationshipType::PlayerRelationshipType) data[9];

			if (relationship == PlayerRelationshipType::Hostile && me->uid != uid && players->find(uid).uid)
			{
				//Event("hostile", true);
			}
		}
		break;
		case 0x76: // Player In Sight
		{
			UnitType::UnitType unitType = (UnitType::UnitType) data[1];
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 2);

			if (players->find(uid).hostiled)
			{
				set("uid", uid);
				Event("hostileinsight", true);
			}
		}
		break;
		case 0x77: // Button Actions
		{
			if (IsRunning())
			{
				ItemUIAction::ItemUIAction action = (ItemUIAction::ItemUIAction) data[1];

				if (action == ItemUIAction::RequestTrade)
				{
					ClickButton(GameButton::CloseTrade, 0);
					return 0;
				}

				if (action == ItemUIAction::OpenStash)
				{
					return 0;
				}

				if (action == ItemUIAction::OpenCube)
				{
					return 0;
				}
			}
		}
		break;
		case 0x7a: // Summon Action
		{
			SummonActionType::SummonActionType actionType = (SummonActionType::SummonActionType) data[1];
			char skillTree = data[2];
			PetType::PetType petType = (PetType::PetType) *reinterpret_cast<const short*>(data + 3);
			unsigned int playerUID = *reinterpret_cast<const int*>(data + 5);
			unsigned int petUID = *reinterpret_cast<const int*>(data + 9);

			PLAYER pl = players->find(playerUID);

			if (playerUID == me->uid)
			{
				if (actionType == SummonActionType::UnsummonedOrLostSight)
				{
					me->pets.removePet(petUID);
				}
				else if (actionType == SummonActionType::SummonedOrReassigned)
				{
					me->pets.addPet(petType, petUID);
				}
			}
		}
		break;
		case 0x81: // Merc Assign
		{
			unsigned int ownerUID = *reinterpret_cast<const int*>(data + 4);
			unsigned int uid = *reinterpret_cast<const int*>(data + 8);

			if (ownerUID == me->uid)
			{
				merc->uid = uid;
				merc->life = 100;
			}
		}
		break;
		case 0x82: // Portal Ownership
		{
			unsigned int ownerUID = *reinterpret_cast<const int*>(data + 1);
			unsigned int portalLocalUID = *reinterpret_cast<const int*>(data + 21);
			unsigned int portalRemoteUID = *reinterpret_cast<const int*>(data + 25);

			objects->add_owner(portalLocalUID, ownerUID);
		}
		break;
		case 0x8b: // Players Relation Status
		{
			unsigned int uid = *reinterpret_cast<const int*>(data + 1);
			PartyRelationshipType::PartyRelationshipType relationship = (PartyRelationshipType::PartyRelationshipType) data[5];

			players->setRelationship(uid, relationship);
			set("uid", uid);
			set("relationship", relationship);
			Event("playerRelation", false);
		}
		break;
		case 0x8c: // Player Relationship
		{
			unsigned int subjectUID = *reinterpret_cast<const int*>(data + 1);
			unsigned int objectUID = *reinterpret_cast<const int*>(data + 5);
			unsigned short relation = *reinterpret_cast<const short*>(data + 9);

			if (relation == 0x08 && subjectUID == me->uid)  // Hostile
			{
				players->hostile(objectUID);
				//set("uid", objectUID);
				//Event("hostile", true);
			}
		}
		break;
		case 0x8d: // Assign Player to Party
		{

		}
		break;
		case 0x8e: // Assign Player Corpse
		{
			unsigned char assign = data[1];
			unsigned int playerUID = *reinterpret_cast<const int*>(data + 2);
			unsigned int corpseUID = *reinterpret_cast<const int*>(data + 6);

			if (assign == 0x00)
			{
				me->corpseid = 0;
			}
			else
			{
				if (playerUID == me->uid)
				{
					//Chat("Body Found");
					me->corpseid = corpseUID;

					Event("death", true);
				}
				else
				{
					players->setLife(playerUID, 0);
					Event("playerDeath", true);
				}
			}
		}
		break;
		case 0x7F: // Party Member Update
		{
	        bool isPlayer = *reinterpret_cast<const bool*>(data + 1);
		    int lifePercent = *reinterpret_cast<const int*>(data + 2);
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 4);
			AreaLevel::AreaLevel area = (AreaLevel::AreaLevel) data[8];

			if (isPlayer)
			{
				players->update(uid, lifePercent, area);
			}
		}
		break;
		case 0x9c: // World Item Action
		case 0x9d: // Owned Item Action
		{
			ProcessItem(data, size);
		}
		break;
		case 0xa7: // Delayed State
		{
			UnitType::UnitType type = (UnitType::UnitType) data[1];
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 2);
			StateType::StateType state = (StateType::StateType) data[6];

			if (type == UnitType::Player && uid == me->uid)
			{
				if (waiting_state == state)
				{
					waiting_state = StateType::None;
				}

				set("state", state);
				Event("setState", true);
			}
			if (type == UnitType::NPC && uid == merc->uid)
			{
				//set("state", state);
				//Event("mercsetstate", true);
			}
			if (type == UnitType::NPC && (state == StateType::Redeemed || state == StateType::RestInPeace))
				npcs->redeem(uid);
			if (type == UnitType::NPC && state == StateType::Revive) //Let's see if removing them helps.
			{
				PET pet = me->pets.find(uid);
				if (pet.uid == 0)				
					npcs->remove(uid);
			}
/*
			if (type == UnitType::Player && uid == me->uid && state == StateType::IronMaiden)
			{
				Chat("�c1Warning: you have been cursed with �c8Iron Maiden");
				Event("ironmaiden", true);
			}

			if (type == UnitType::NPC && uid == merc->uid && state == StateType::IronMaiden)
			{
				Chat("�c1Warning: your merc has been cursed with �c8Iron Maiden");
				Event("mercironmaiden", true);
			}
*/
		}
		break;
		case 0xa8: // Set State
		{
			UnitType::UnitType type = (UnitType::UnitType) data[1];
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 2);
			StateType::StateType state = (StateType::StateType) data[7];

			std::vector<StatBase> stats;

			BitReader br(data, 8);
			int statID, val, param;
			BaseStat * stat;
			while (true)
			{
				statID = br.ReadInt32(9);
				if (statID == 0x1FF)
					break;

				stat = BaseStat::Get(statID);
				val = br.ReadInt32(stat->SendBits);
				if (stat->SendParamBits > 0)
				{
					param = br.ReadInt32(stat->SendParamBits);
					if (stat->Signed)
						stats.push_back(SignedStatParam(stat, val, param));
					else
						stats.push_back(UnsignedStatParam(stat, (unsigned int)val, (unsigned int)param));
				}
				else
				{
					if (stat->Signed)
						stats.push_back(SignedStat(stat, val));
					else
						stats.push_back(UnsignedStat(stat, (unsigned int)val));
				}
			}

			if (type == UnitType::Player && uid == me->uid)
			{
				if (waiting_state == state)
				{
					waiting_state = StateType::None;
				}

				me->states.setState(state, stats);
				set("state", state);
				Event("setstate", false);
			}
			if (type == UnitType::NPC && uid == merc->uid)
			{
				merc->states.setState(state, stats);
				//set("state", state);
				//Event("mercsetstate", true);
			}
/*
			if (type == UnitType::Player && uid == me->uid && state == StateType::Poison)
			{
				Chat("�c1Warning: you have been cursed with �c8Poison");
			}
*/
			if (type == UnitType::Player && uid == me->uid && state == StateType::BattleOrders)
			{
				//Chat("�c:Bonus Life Modifier: �c8Battle Orders");
				float mod_percent = ((((float) data[8]) + 1) / 100);
				me->bolife = me->baselife * (unsigned int)(mod_percent + 0.5);
				me->bomana = me->basemana * (unsigned int)(mod_percent + 0.5);
			}
			if (type == UnitType::Player && uid == me->uid && state == StateType::Oaksage)
			{
				//Chat("�c:Bonus Life Modifier: �c8Oak Sage");
				float mod_percent = ((((float) data[8]) + 1) / 100);
				me->oaklife = me->baselife * (unsigned int)(mod_percent + 0.5);
				me->oakmana = me->basemana * (unsigned int)(mod_percent + 0.5);
			}
			if (type == UnitType::Player && uid == me->uid && state == StateType::Wolf)
			{
				//Chat("�c:Bonus Life Modifier: �c8Wolf Form");
				float mod_percent = ((((float) data[8]) + 1) / 100);
				me->wolflife = me->baselife * (unsigned int)(mod_percent + 0.5);
				me->wolfmana = me->basemana * (unsigned int)(mod_percent + 0.5);
			}
			if (type == UnitType::Player && uid == me->uid && state == StateType::Bear)
			{
				//Chat("�c:Bonus Life Modifier: �c8Bear Form");
				float mod_percent = ((((float) data[8]) + 1) / 100);
				me->bearlife = me->baselife * (unsigned int)(mod_percent + 0.5);
				me->bearmana = me->basemana * (unsigned int)(mod_percent + 0.5);
			}
/*
			if (type == UnitType::Player && uid == me->uid && state == StateType::Enchant)
			{
				Chat("�c:Bonus Damage Modifier: �c8Enchant");
			}
*/
		}
		break;
		case 0xa4: // Next Baal Wave, You get this after succesfully clearing the throne.
		{
			/*
			0xa4 3e 0xa4 17 -> Shaman and fallens
			0xa4 69 0xa4 7d -> Mummies and skeles
			0xa4 2d --> Council
			0xa4 2e --> Pitlords
			0xa4 3b --> Minions
			*/
			NPCCode::NPCCode npccode = (NPCCode::NPCCode)(data[1]);
			if (npccode == NPCCode::WarpedShaman)
				preattack = 1;
			if (npccode == NPCCode::BaalSubjectMummy)
				preattack = 2;
			if (npccode == NPCCode::CouncilMemberBall)
				preattack = 3;
			if (npccode == NPCCode::VenomLord2)
				preattack = 4;
			if (npccode == NPCCode::BaalsMinion || npccode == NPCCode::BaalsMinion2 || npccode == NPCCode::BaalsMinion3)
				preattack = 5;
		}
		break;

		case 0xa9: // End State
		{
			UnitType::UnitType type = (UnitType::UnitType) data[1];
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 2);
			StateType::StateType state = (StateType::StateType) data[6];

			if (type == UnitType::Player && uid == me->uid)
			{
				me->states.endState(state);
				set("state", state);
				Event("endState", true);
			}
			if (type == UnitType::NPC && uid == merc->uid)
			{
				merc->states.endState(state);
				//set("state", state);
				//Event("mercendstate", true);
			}
			if (type == UnitType::Player && uid == me->uid && state == StateType::BattleOrders)
			{
				//Chat("�c1Warning: �c8Battle Orders �c1has worn off");
				me->maxlife -= me->bolife;
				me->maxmana -= me->bomana;
				me->bolife = 0;
				me->bomana = 0;
			}
			if (type == UnitType::Player && uid == me->uid && state == StateType::Oaksage)
			{
				//Chat("�c1Warning: �c8Oak Sage �c1has worn off");
				me->maxlife -= me->oaklife;
				me->maxmana -= me->oakmana;
				me->oaklife = 0;
				me->oakmana = 0;
			}
			if (type == UnitType::Player && uid == me->uid && state == StateType::Wolf)
			{
				//Chat("�c1Warning: �c8Wolf form �c1has worn off");
				me->maxlife -= me->wolflife;
				me->maxmana -= me->wolfmana;
				me->wolflife = 0;
				me->wolfmana = 0;
			}
			if (type == UnitType::Player && uid == me->uid && state == StateType::Bear)
			{
				//Chat("�c1Warning: �c8Bear form �c1has worn off");
				me->maxlife -= me->bearlife;
				me->maxmana -= me->bearmana;
				me->bearlife = 0;
				me->bearmana = 0;
			}
/*
			if (type == UnitType::Player && uid == me->uid && state == StateType::Enchant)
			{
				Chat("�c1Warning: �c8Enchant �c1has worn off");
			}
			if (type == UnitType::Player && uid == me->uid && state == StateType::IronMaiden)
			{
				Chat("�c:Curse: �c8Iron Maiden �c:has worn off");
			}
			if (type == UnitType::NPC && uid == merc->uid && state == StateType::IronMaiden)
			{
				Chat("�c:Merc curse: �c8Iron Maiden �c:has worn off");
			}
*/
		}
		break;
		case 0xaa: // Add Unit
		{
			//Check for revive state
			// aa [BYTE Unit Type] [DWORD Unit Id] [BYTE Packet Length] [VOID State Info] 
			unsigned int uid = *reinterpret_cast<const unsigned int*>(data + 2);
			UnitType::UnitType type = (UnitType::UnitType) data[1];
			NPC npc = npcs->find(uid);

			if(npc.uid != 0 && type == UnitType::NPC)
			{
				if (NpcReader::parseAAPacket(data, size, npc, difficulty))
				{
					npcs->update(npc);
				}
				else
				{
					LOG(logERROR, this) << "error in parseAAPacket";
				}
			}
		}
		break;
		case 0xac: // Assign NPC
		{				
			NPC npc = NpcReader::NewMonster(data, size, difficulty);
			npcs->add(npc);
		}
		break;
		case 0x8f: // Pong
		{
			//ping = GetTickCount() - pingsent;
			//pingsent = 0;
			//lagcheck = false;
			if (me->life > me->maxlife)
				me->maxlife = me->life;
			if (me->mana > me->maxmana)
				me->maxmana = me->mana;
			Event("pong", true);
		}
		break;
		case 0x94: // Skills Log
		{
			unsigned int uid = *reinterpret_cast<const int*> (data + 2);

			if (uid == me->uid)
			{
				for (int i = 0; i < data[1]; i++)
				{
					SKILL skill;
					skill.skill = (SkillType::SkillType) *reinterpret_cast<const short*> (data + 6 + i * 3);
					skill.baseLevel = data[8 + i * 3];
					
					skills->add(skill);
				}
			}
		}
		break;
		case 0x95: // Player Life, Mana Change
		{
			short x = 0, y = 0;

			BitReader br(data, 1);			// Skip 0x95
			me->life = br.ReadUInt16(15);	// LIFE
			me->mana = br.ReadUInt16(15);	// MANA
			me->stamina = br.ReadUInt16(15); // STAMINA
				
			x = br.ReadUInt16(15);			// X
			br.ReadBoolean(1);				// bool?
			y = br.ReadUInt16(15);			// Y
												/// Unkown 20 bits left...
			/*LOG(logDEBUG2, this) 
				<< "0x95 -->" 
				<< " Life: " << me->life
				<< " Mana: " << me->mana
				<< " Stamina: " << me->stamina
				<< " X: " << x << " Y: " << y;*/
				
			if (x > 1000 && y > 1000)
			{				
				me->x = x;
				me->y = y;
				maps->setPosition(me->x, me->y);
			}					

			Event("lifeMana", true);
		}
		break;
		case 0x96: //Walk Verify
		{
			short stamina = 0, random = 0, byte = 0;
			BitReader * br = new BitReader(data, 1);
			stamina = br->ReadInt32(15);
			me->x = br->ReadInt32(16);
			me->y = br->ReadInt32(16);

			//17 bits left, last 16 mostly 00 00 but changes as you go along.
			//^80 

			maps->setPosition(me->x, me->y);
			delete br;
		}
		break;
		case 0x97: // Switch Weapon Set
		{
			me->weaponset = !me->weaponset;

			if (me->weaponset)
			{
				//Chat("Primary");
			}
			else
			{
				//Chat("Secondary");
			}
		}
		break;
	}

	return size;
}
bool isPetSkill(SkillType::SkillType skill)
{
	switch(skill){
		case SkillType::Bloodgolem:
		case SkillType::ClayGolem:
		case SkillType::Firegolem:
		case SkillType::Hydra:
		case SkillType::IronGolem:
		case SkillType::OakSage:
		case SkillType::RaiseSkeletalMage:
		case SkillType::RaiseSkeleton:
		case SkillType::Raven:
		case SkillType::Resurrect:
		case SkillType::Resurrect2:
		case SkillType::Revive:
		case SkillType::Selfresurrect:
		case SkillType::ShadowMaster:
		case SkillType::ShadowWarrior:
		case SkillType::Skeletonraise:
		case SkillType::SummonDireWolf:
		case SkillType::SummonGrizzly:
		case SkillType::SummonSpiritWolf:
		case SkillType::Valkyrie:
			return true;
		default:
			return false;	
	}
}