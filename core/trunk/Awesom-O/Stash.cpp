#include "Stash.h"

std::vector<ITEM> Stash::get()
{
	std::vector<ITEM> vector;

	for (std::map<unsigned int, ITEM>::iterator iter = stash.begin(); iter != stash.end(); iter++)
	{
		vector.push_back(iter->second);
	}

	return vector;
}

bool Stash::Check(ITEM * item, int x, int y)
{
	short width = item->baseItem->invWidth;
	short height = item->baseItem->invHeight;

	for (int w = x; w < x + width; w++)
	{
		for (int h = y; h < y + height; h++)
		{
			if (map[w][h] != 0)
			{
				return false;
			}
		}
	}

	return true;
}

POINT Stash::findLocation(ITEM item)
{
	POINT result = {-1, -1};

	for (int x = 0; x < 6; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			map[x][y] = 0;
		}
	}

	for (std::map<unsigned int, ITEM>::iterator iter = stash.begin(); iter != stash.end(); iter++)
	{
		for (int x = iter->second.x; x < iter->second.x + iter->second.baseItem->invWidth; x++)
		{
			for (int y = iter->second.y; y < iter->second.y + iter->second.baseItem->invHeight; y++)
			{
				map[x][y] = 1;
			}
		}
	}

	short width = item.baseItem->invWidth;
	short height = item.baseItem->invHeight;

	for (int x = 0; x <= 6 - width; x++)
	{
		for (int y = 0; y <= (expansion ? 8 : 4) - height; y++)
		{
			if (Check(&item, x, y))
			{
				result.x = x;
				result.y = y;

				return result;
			}
		}
	}

	return result;
}

void Stash::add(ITEM item)
{
	stash[item.uid] = item;
}

void Stash::remove(ITEM item)
{
	stash.erase(item.uid);
}
std::vector<ITEM> Stash::findByCode(std::string code)
{
	std::vector<ITEM> result;

	for (std::map<unsigned int, ITEM>::iterator iter = stash.begin(); iter != stash.end(); iter++)
	{
		if (iter->second.baseItem->code.compare(code) == 0) 
		{
			result.push_back(iter->second);
		}
	}
	return result;
}


