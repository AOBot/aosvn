#ifndef CONTROL_H
#define CONTROL_H

#include <iostream>
#include <winsock2.h>
#include <vector>

#include "Connection.h"

class AwesomO;
class Connection;

class Controller : public Connection
{
public:
	virtual void Process();

	AwesomO * awesomo;

public:
	Controller(SOCKET s) : Connection(s)
	{
		awesomo = NULL;
	}
	Controller::~Controller();

	void Write(const char * buffer, int length);
};

namespace ControllerEvent
{
	enum ControllerEvent
	{
		CONNECTION_SUCCESSFUL = 0x01,
		ALREADY_CONNECTED,
		BNET_AUTH_RESPONSE,
		BNET_LOGON_RESPONSE,
		TEMPORARY_IP_BAN,
		CHARACTER_SELECT,
		JOIN_CHANNEL,
		LOGON_SUCCESS,
		JOIN_GAME_SUCCESSFUL,
		CONNECTED_GAME_PROXY,
		GAME_ALREADY_EXISTS,
		GAME_DOES_NOT_EXISTS,
		FAILED_TO_JOIN,
		MESSAGE,
		EXIT_GAME,
		CONTROLLER_MESSAGE,
		GAME_LIST,
		CHAT_EVENT,
		CHAT_DESTRUCTOR,
		REALM_DESTRUCTOR,
		GAME_DESTRUCTOR,
		CANT_CONNECT,
		CHAR_COUNT,
	};
};

#endif //CONTROL_H
