#pragma once

#include "CriticalSection.h"
#include "Items.h"
#include <vector>
#include <map>

class Belt { // tolua_export

private:
	CriticalSection cs;
	std::map<PotionType::PotionType, std::vector<ITEM>> belt;

public:
	//tolua_begin
	void add(ITEM item)
	{
		cs.Lock();
		belt[Items::GetPotType(item.baseItem->code)].push_back(item);
		cs.Unlock();
	}

	void remove(ITEM item)
	{
		cs.Lock();
		for (std::vector<ITEM>::iterator iter = belt[Items::GetPotType(item.baseItem->code)].begin(); iter != belt[Items::GetPotType(item.baseItem->code)].end(); iter++)
		{
			if (iter->uid == item.uid)
			{
				belt[Items::GetPotType(item.baseItem->code)].erase(iter);
				break;
			}
		}
		cs.Unlock();
	}

	ITEM getPotion(PotionType::PotionType type)
	{
		ITEM result;
		result.uid = 0;

		cs.Lock();
		if (belt[type].size() > 0)
		{
			result = belt[type].front();
		}
		cs.Unlock();

		return result;
	}

	int getPotionCount(PotionType::PotionType type)
	{
		cs.Lock();
		int result = belt[type].size();
		cs.Unlock();
		return result;
	}

	int getPotionCountInRow(PotionType::PotionType type, int row)
	{
		cs.Lock();
		int result = 0;

		for (std::map<PotionType::PotionType, std::vector<ITEM>>::iterator iter1 = belt.begin(); iter1 != belt.end(); iter1++)
		{
			for (std::vector<ITEM>::iterator iter2 = iter1->second.begin(); iter2 != iter1->second.end(); iter2++)
			{
				if (iter2->x % 4 == row)
				{
					result++;
				}
			}
		}

		cs.Unlock();
		return result;
	}

	std::vector<ITEM> getBelt()
	{
		std::vector<ITEM> result;
		cs.Lock();
		result.insert(result.end(), belt[PotionType::Blue].begin(), belt[PotionType::Blue].end());
		result.insert(result.end(), belt[PotionType::Red].begin(), belt[PotionType::Red].end());
		result.insert(result.end(), belt[PotionType::Purple].begin(), belt[PotionType::Purple].end());
		result.insert(result.end(), belt[PotionType::Error].begin(), belt[PotionType::Error].end());
		cs.Unlock();
		return result;
	}

	std::vector<ITEM> getBeltRow(int row)
	{
	cs.Lock();
	std::vector<ITEM> result;

	for (std::map<PotionType::PotionType, std::vector<ITEM>>::iterator iter1 = belt.begin(); iter1 != belt.end(); iter1++)
	{
		for (std::vector<ITEM>::iterator iter2 = iter1->second.begin(); iter2 != iter1->second.end(); iter2++)
		{
			if (iter2->x % 4 == row)
			{
				result.push_back(*iter2);
			}
		}
	}

	cs.Unlock();
	return result;
	}

};
// tolua_end
