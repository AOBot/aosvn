#ifndef AWESOMO_H
#define AWESOMO_H

#include "CdKeyManager.h"
#include "SocksManager.h"
#include "Controller.h"
#include "Proxy.h"
#include "MapHack.h"
#include "Thread.h"
#include "d2data/UnitType.h"
#include "d2data/Messaging.h"
#include "d2data/item/ItemEnums.h"
#include "d2data/Game.h"
#include "d2data/SkillType.h"
#include "d2data/StatType.h"
#include "d2data/StateType.h"
#include "d2data/AreaLevel.h"
#include "d2data/Quests.h"
#include "d2data/CharacterClass.h"

#include "Objects.h"
#include "Warps.h"
#include "Maps.h"
#include "Npcs.h"
#include "Players.h"
#include "Skills.h"
#include "Merc.h"
#include "Me.h"
#include "Store.h"
#include "Items.h"
#include "Pickit.h"
#include "Commands.h"
#include "Files.h"

#define PI 3.1415926535897932384626433832795

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

#include "tolua++.h"

class Proxy;
class Controller;
class MapHack;

class AwesomO : public Thread
{
public:
	Proxy * chat;
	Proxy * realm;
	Proxy * game;

	Controller * controller;
	MapHack * maphack;

	char owner[20];

	int realm_port;
	char realm_address[16];
	char game_address[16];

	char gamename[20];
	char password[20];

	unsigned char chat_hash[64];

	unsigned char realm_hash[4];
	unsigned char realm_token[2];

	std::string account;
	std::string character;

	time_t start_time;
	bool exiting;

	unsigned char difficulty;
	bool expansion;

	AwesomO(const char * o, Sock * sock = NULL);
	~AwesomO();

	void Initialize();

	std::list<const CdKey*> keys;

	const CdKey* AcquireKey(int productId);
	void ReleaseKeys();

	Commands * commands;

	Players * players;
	Objects * objects;
	Warps * warps;
	Npcs * npcs;
	Skills * skills;
	Store * store;
	Items * items;
	Pickit * pickit;
	Files * files;

	Me * me;
	Merc * merc;

	Maps * maps;

	unsigned long ping;
	unsigned long pingsent;
	unsigned long lagpause;
	bool lagcheck;

	int baallevel;
	int preattack;

	int gamenumber;
	bool events;

	int Routine();

	int OnRelayDataToClient(unsigned char * data, int size);
	int OnRelayDataToServer(unsigned char * data, int size);

	int Event(std::string name, bool running);

	void EnableEvents();
	void DisableEvents();

	void ProcessItem(const unsigned char* data, int size);

	unsigned char waiting;
	unsigned int waiting_uid;
	StateType::StateType waiting_state;

	lua_State* lua_pickit;
	lua_State* lua_event;

	std::map<std::string, std::string> vars;

	std::ofstream of;

	CdKeyManager * keyManager;
	Sock * sock;

public:

	void AwesomO::Tasks();

	POINT getDistantPoint(int px, int py, int x, int y, int dist);

	void Sleep(unsigned long miliseconds);
	unsigned long GetTimeInGame();

	bool WaitForPacket(unsigned char packetid, int timeout);
	void InitWaitingItem(unsigned int uid);
	bool WaitForItem(int timeout);
	void InitWaitingState(StateType::StateType state);
	bool WaitForState(int timeout);
	void InitWaitingPacket(unsigned char packetid); // use this to set the packetId
	bool WaitForInitPacket(int timeout); // and this to actually wait for the packet

	void set(std::string name, std::string value);
	void set(std::string name, int value);

	std::string getString(std::string name);
	int getNumber(std::string name);

	void Run(std::string file, std::string function);
	void RunNow(std::string file, std::string function);

	void SendController(std::string message, char id = ControllerEvent::MESSAGE);


	// GC
	void WalkToLocation(unsigned short x, unsigned short y);
	void WalkToTarget(UnitType::UnitType unitType, unsigned int uid);
	void RunToLocation(unsigned short x, unsigned short y);
	void RunToTarget(UnitType::UnitType unitType, unsigned int uid);
	void CastLeftSkill(unsigned short x, unsigned short y);
	void CastLeftSkillOnTarget(UnitType::UnitType unitType, unsigned int uid);
	void CastLeftSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid);
	void RecastLeftSkill(unsigned short x, unsigned short y);
	void RecastLeftSkillOnTarget(UnitType::UnitType unitType, unsigned int uid);
	void RecastLeftSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid);
	void CastRightSkill(unsigned short x, unsigned short y);
	void CastRightSkillOnTarget(UnitType::UnitType unitType, unsigned int uid);
	void CastRightSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid);
	void RecastRightSkill(unsigned short x, unsigned short y);
	void RecastRightSkillOnTarget(UnitType::UnitType unitType, unsigned int uid);
	void RecastRightSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid);
	void UnitInteract(UnitType::UnitType unitType, unsigned int uid);
	void SendOverheadMessage(std::string message);
	void SendOverheadMessage(const char* format, ...);
	void SendMessage(const char* message, GameMessageType::GameMessageType type = GameMessageType::GameMessage, const char* recipient = NULL);
	void Speak(const char* text);
	void PickItem(unsigned int requestID, unsigned int uid, bool toCursor);
	void DropItem(unsigned int uid);
	void DropItemToContainer(unsigned int uid, unsigned int x, unsigned int y, ItemContainerGC::ItemContainerGC container);
	void PickItemFromContainer(unsigned int uid);
	void EquipItem(unsigned int uid, EquipmentLocation::EquipmentLocation location);
	void UnequipItem(EquipmentLocation::EquipmentLocation location);
	void SwapEquippedItem(unsigned int uid, EquipmentLocation::EquipmentLocation location);
	void SwapContainerItem(unsigned int SubjectUID, unsigned int ObjectUID, unsigned int x, unsigned int y);
	void UseInventoryItem(unsigned int uid, unsigned int meX, unsigned int meY);
	void StackItems(unsigned int subjectUID, unsigned int objectUID);
	void AddBeltItem(unsigned int uid, unsigned int x);
	void RemoveBeltItem(unsigned int uid);
	void SwapBeltItem(unsigned int oldItemUID, unsigned int newItemUID);
	void UseBeltItem(unsigned int uid, bool toMerc = false, unsigned int unknown9 = 0);
	void IdentifyItem(unsigned int itemUID, unsigned int scrollUID);
	void EmbedItem(unsigned int subjectUID, unsigned int objectUID);
	void ItemToCube(unsigned int itemUID, unsigned int cubeUID);
	void TownFolkInteract(UnitType::UnitType unitType, unsigned int uid);
	void TownFolkCancelInteraction(UnitType::UnitType unitType, unsigned int uid);
	void DisplayQuestMessage(unsigned int uid, unsigned int message);
	void BuyItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost, bool fillStack = false);
	void GambleItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost);
	void SellItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost);
	void CainIdentifyItems(unsigned int uid);
	void TownFolkRepair(unsigned int dealerUID);
	void TownFolkRepair(unsigned int dealerUID, unsigned int itemUID);
	void HireMercenary(unsigned int dealerUID, unsigned int mercID);
	void IdentifyGambleItem(unsigned int uid);
	void TownFolkMenuSelect(TownFolkMenuItem::TownFolkMenuItem selection, unsigned int uid, unsigned int unknown9 = 0);
	void IncrementAttribute(StatType::StatType attribute, unsigned char count = 1);
	void IncrementSkill(SkillType::SkillType skill);
	bool SelectSkill(SkillType::SkillType skill, SkillHand::SkillHand hand = SkillHand::Right, unsigned int chargedItemUID = 0xFFFFFFFF);
	void HoverUnit(unsigned int uid);
	void SendCharacterSpeech(GameSound::GameSound speech);
	void RequestQuestLog();
	void Respawn();
	void WaypointInteract(unsigned int uid, WaypointDestination::WaypointDestination destination);
	void RequestReassign(UnitType::UnitType unitType, unsigned int meUID);
	void ClickButton(GameButton::GameButton button, unsigned int complement);
	void DropGold(unsigned int meUID, unsigned int amount);
	void SetSkillHotkey(SkillType::SkillType skill, unsigned int slot, unsigned int itemUID = 0xFFFFFFFF);
	void CloseQuest(QuestType::QuestType quest);
	void GoToTownFolk(UnitType::UnitType unitType, unsigned int uid, unsigned int x, unsigned int y);
	void SetPlayerRelation(PlayerRelationType::PlayerRelationType relation, bool value, unsigned int uid);
	void PartyRequest(PartyAction::PartyAction action, unsigned int playerUID);
	void UpdatePosition(unsigned int x, unsigned int y);
	void SwitchWeapons();
	void ChangeMercEquipment(EquipmentLocation::EquipmentLocation location, bool unequip = false);
	void ResurrectMerc(unsigned int dealerUID);
	void InventoryItemToBelt(unsigned int uid);
	void WardenResponse(unsigned char * data);
	void GameLogonRequest(unsigned int d2GShash, unsigned short d2GSToken, CharacterClass::CharacterClass charClass, unsigned int version, std::string name);
	void ExitGame();
	void EnterGame();
	void Ping(unsigned int tickCount, unsigned long long unknown);
	void Chat(const char* format, ...);

	// GS
	void GameLoading();
	void GameLogonSuccess();
	void LoadDone();
	void UnloadDone();
	void GameLogoutSuccess();
	void MapAdd(AreaLevel::AreaLevel area, unsigned int x, unsigned int y);
	void MapRemove(AreaLevel::AreaLevel area, unsigned int x, unsigned int y);
	void RemoveGroundUnit(UnitType::UnitType type, unsigned int uid);
	void GameHandshake(UnitType::UnitType type, unsigned int uid);
	void NPCGetHit(UnitType::UnitType type, unsigned int uid, unsigned char life, unsigned int anim);
	void PlayerMove(UnitType::UnitType type, unsigned int uid, MovementType::MovementType movement, unsigned int targetX, unsigned int targetY, unsigned char unknown12, unsigned int currentX, unsigned int currentY);
	void ReportKill(UnitType::UnitType type, unsigned int uid);
	void PlayerReassign(UnitType::UnitType type, unsigned int uid, unsigned int x, unsigned int y, bool reassign);
	void SmallGoldAdd(unsigned char amount);
	void AttributeByte(StatType::StatType stat, unsigned char value);
	void AttributeWord(StatType::StatType stat, unsigned short value);
	void AttributeDWord(StatType::StatType stat, unsigned long value);
	void GameMessage(GameMessageType::GameMessageType type, unsigned char charFlags, std::string charName, std::string message);
	void GameMessage(UnitType::UnitType type, unsigned int uid, unsigned short random, std::string message);
	void PlaySound(UnitType::UnitType unitType, unsigned int uid, GameSound::GameSound sound);
	void PlayerClearCursor(UnitType::UnitType unitType, unsigned int uid);
	void Relator1(unsigned int uid, unsigned short param1, unsigned int param2);
	void Relator2(unsigned int uid, unsigned short param1, unsigned int param2);
	void MercForHireListStart();
	void PlayerInGame(unsigned int uid, CharacterClass::CharacterClass charClass, std::string name, unsigned short level, unsigned short partyID);
	void PlayerLeaveGame(unsigned int uid);
	void PlayerCorpseVisible(bool assign, unsigned int playerUID, unsigned int corpseUID);
	void PlayerInSight(UnitType::UnitType unitType, unsigned int uid);
	void UpdateItemUI(ItemUIAction::ItemUIAction action);
	void AcceptTrade(std::string name, unsigned int uid);
	void AssignSkillHotkey(unsigned char slot, SkillType::SkillType skill, unsigned int itemUID = 0xFFFFFFFF);
	void UseSpecialItem(SpecialItemType::SpecialItemType action, unsigned int uid);
	void SetItemState(UnitType::UnitType ownerType, unsigned int ownerUID, unsigned int itemUID, ItemStateType::ItemStateType state);
	void AssignPlayerToParty(unsigned int uid, unsigned short partyNumber);
	void AssignPlayerCorpse(bool assign, unsigned int playerUID, unsigned int corpseUID);
	void Pong();
	void PartyMemberPulse(unsigned int uid, unsigned int x, unsigned int y);
	void SwitchWeaponSet();
	void GameOver();
	void LoadSkillOnUnit(SkillType::SkillType skill, UnitType::UnitType type, unsigned int meUID, unsigned int uid);
	void LoadSkill(SkillType::SkillType skill, unsigned int meUID, unsigned int uid);

	void PrintData(const unsigned char * start, unsigned int length, int offset, const char *title, const char *action);
};
extern std::vector<AwesomO *> ao;
#endif //AWESOMO_H
