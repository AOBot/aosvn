#pragma once

#include "CriticalSection.h"
#include <vector>

//tolua_begin
namespace SummonActionType
{
	enum SummonActionType
	{
		UnsummonedOrLostSight	= 0,
		SummonedOrReassigned	= 1,
	};
};

namespace PetType
{
	enum PetType
	{
		ErrorTop,
		Revive = 19,
		ClayGolem = 289,
		BloodGolem = 290,
		IronGolem = 291,
		FireGolem = 292,
		BonePrison = 340, //No idea which one is necros
		BonePrison2 = 341,
		BonePrison3 = 342,
		BonePrison4 = 343,
		BoneWall = 344,
		Decoy = 356,
		Valkyrie = 357,
		Skeleton = 363,
		SkeletonMage = 364,
		WakeOfDestruction = 410, //Wake Of Fire
		ChargedBoltSentry = 411,
		LightningSentry = 412,
		BladeCreeper = 413, //Blade Sentry 
		InfernoSentry = 415,
		DeathSentry = 416,
		ShadowWarrior = 417,
		ShadowMaster = 418,
		DruidHawk = 419,
		DruidSpiritWolf = 420,
		DruidFenris = 421,  //Summon Dire Wolf???
		SpiritOfBarbs = 422,
		HeartOfWolverine = 423,
		OakSage = 424,
		DruidPlaguePoppy = 425, //Poison Creeper???
		DruidCycleOfLife = 426, //Solar Creeper???
		VineCreature = 427, //Carrion Vine???
		DruidBear = 428, //Summon Grizzly
		ErrorBottom,
	};
};

struct PET
{
	unsigned int uid;
	PetType::PetType petType;
};
// tolua_end

class Pets { // tolua_export

private:
	CriticalSection cs;
	std::vector<PET> pets;

public:
	void addPet(PetType::PetType petType, unsigned int uid);
	void removePet(unsigned int uid);
	PET Pets::find(unsigned int uid);

	//tolua_begin
	std::vector<PET> getPets();
	std::vector<PET> getPets(PetType::PetType petType);
};
// tolua_end
