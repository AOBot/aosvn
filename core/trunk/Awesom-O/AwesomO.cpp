#include "AwesomO.h"
#include "Binding.h"
#include "ItemReader.h"
#include "CdKey.h"

#include <iostream>
#include <windows.h>
#include <time.h>

std::vector<AwesomO *> ao;

AwesomO::AwesomO(const char * owner, Sock * s) : chat(NULL), realm(NULL), game(NULL), controller(NULL), maphack(NULL), sock(s)
{
	memcpy(this->owner, owner, 20);
	waiting = 0;
	gamenumber = 0;

	char keyfile[255];
	sprintf_s(keyfile, sizeof(keyfile), "config/keys/%s.txt", owner);
	keyManager = new CdKeyManager(keyfile);

	char logfile[255];
	sprintf_s(logfile, sizeof(logfile), "logs/%s.log", owner);
	of.open(logfile, std::ios::app);

	lua_pickit = NULL;
	lua_event = NULL;

	waiting = 0;
	waiting_uid = 0;
	waiting_state = StateType::None;
}

AwesomO::~AwesomO()
{
	Abort();
	Join();

	delete keyManager;

	if (game) delete game;
	if (realm) delete realm;
	if (chat) delete chat;

	if (controller) delete controller;
	if (maphack) delete maphack;
}

void AwesomO::Initialize()
{
	lua_pickit = lua_open();
	tolua_aobot_open(lua_pickit);
	luaL_openlibs(lua_pickit);
	tolua_pushusertype(lua_pickit, (void *) this, "AwesomO");
	lua_setglobal(lua_pickit, "Ao");

	lua_event = lua_open();
	tolua_aobot_open(lua_event);
	luaL_openlibs(lua_event);
	tolua_pushusertype(lua_event, (void *) this, "AwesomO");
	lua_setglobal(lua_event, "Ao");

	luaL_dofile(lua_pickit, "scripts\\pickit\\pickItem.lua");
	lua_getglobal(lua_pickit, "debug");
	lua_getfield(lua_pickit, -1, "traceback");

	luaL_dofile(lua_event, "scripts\\events.lua");
	lua_getglobal(lua_event, "debug");
	lua_getfield(lua_event, -1, "traceback");

	start_time = time(NULL);
	exiting = false;

	commands = new Commands();
	maps = new Maps();
	players = new Players();
	objects = new Objects();
	warps = new Warps();
	npcs = new Npcs();
	skills = new Skills();
	me = new Me();
	merc = new Merc();
	store = new Store();
	items = new Items();
	pickit = new Pickit();
	files = new Files();

	maps->setAwesomO(this);

	baallevel = 0;
	preattack = 0;
	events = true;
	gamenumber++;

	vars.clear();
}

const CdKey* AwesomO::AcquireKey(int productId)
{
	const CdKey* key = NULL;

	key = keyManager->AcquireKey(productId);

	if (key != 0)
	{
		keys.push_back(key);
		LOG(logDEBUG1, this) << "Acquired Key " << key->GetKeyText() << " for product " << key->GetProductId();
	}

	return key;
}

void AwesomO::ReleaseKeys()
{
	for (std::list<const CdKey*>::iterator i = keys.begin(); i != keys.end(); i++)
	{
		const CdKey* key = *i;
		keyManager->ReleaseKey(key);
		LOG(logDEBUG1, this) << "Released Key " << key->GetKeyText() << " for product " << key->GetProductId();
	}
	
	keys.clear();
}

int AwesomO::Routine()
{
	lua_State* L;

	try
	{
		if (commands->size() > 0)
		{
			Command command = commands->pop_front();

			char file[100];
			sprintf_s(file, sizeof(file), "scripts\\%s", command.file.c_str());

			L = lua_open();
			tolua_aobot_open(L);
			luaL_openlibs(L);
			tolua_pushusertype(L, (void *) this, "AwesomO");
			lua_setglobal(L, "Ao");

			luaL_dofile(L, file);
			lua_getglobal(lua_pickit, "debug");
			lua_getfield(lua_pickit, -1, "traceback");
			lua_getglobal(L, command.function.c_str());

			if (lua_pcall(L, 0, 0, -2) != 0)
			{
				LOG(logERROR, this) << lua_tostring(L, -1);
				lua_pop(L, 1);
			}

			lua_close(L);
		}
	}
	catch (char * message)
	{
		LOG(logERROR, this) << "Return: " << message;
		lua_close(L);
		return -1;
	}
	catch (...)
	{
		LOG(logERROR, this) << "Error: Unknown Error";
		lua_close(L);
		return -1;
	}

	return 0;
}

POINT AwesomO::getDistantPoint(int px, int py, int x, int y, int dist)
{		
	std::vector<POINT> points;
	POINT p;
	//npcs->getPoints(points); --Lets do this in lua.
	p.x = px;
	p.y = py;
	points.push_back(p);
	return maps->getDistantPoint(points, x, y, dist);
}

void AwesomO::Tasks() throw(...)
{
	if (IsAborting())
	{
		throw "aborting";
	}

	if (commands->size() > 0 && !commands->isLocked())
	{
		Command command = commands->pop_front();

		int result = 0;

		char file[100];
		sprintf_s(file, sizeof(file), "scripts\\%s", command.file.c_str());

		lua_State* L;
		L = lua_open();
		tolua_aobot_open(L);
		luaL_openlibs(L);
		tolua_pushusertype(L, (void *) this, "AwesomO");
		lua_setglobal(L, "Ao");

		luaL_dofile(L, file);
		lua_getglobal(lua_pickit, "debug");
		lua_getfield(lua_pickit, -1, "traceback");
		lua_getglobal(L, command.function.c_str());

		if (lua_pcall(L, 0, 0, -2) != 0)
		{
			LOG(logERROR, this) << lua_tostring(L, -1);
			lua_pop(L, 1);
		}
		
		lua_close(L);
	}
}

void AwesomO::Sleep(unsigned long miliseconds) throw(...)
{
	unsigned long remaining = miliseconds;

	clock_t t1=clock();
    clock_t t2;

	while (remaining > 0)
	{
		t2=clock();
		if ((t2-t1) >= miliseconds)
			break;
		if (remaining >= 16) // 16 sleep because "accurate" sleeps is about 16 for newer systems
		{
			::Sleep(16);
			remaining -= 16;
		}
		else if (remaining < 16)
		{
			::Sleep(remaining);
			remaining -= remaining;
		}
		Tasks();
	}
}

unsigned long AwesomO::GetTimeInGame()
{
	return (unsigned long)(time(NULL) - start_time);
}

bool AwesomO::WaitForPacket(unsigned char packetid, int timeout)
{
	int i = 0;
	waiting = packetid;

	clock_t t1=clock();
    clock_t t2;
	
	while (waiting)
	{
		t2 = clock();
		if ((t2-t1) >= timeout)
		{
			waiting = 0;
			return false;
		}
		Sleep(16);
		i++;

		if (i * 16 > timeout)
		{
			waiting = 0;
			return false;
		}
	}

	return true;
}

void AwesomO::InitWaitingItem(unsigned int uid)
{
	waiting_uid = uid;
}

bool AwesomO::WaitForItem(int timeout)
{
	int i = 0;
	clock_t t1=clock();
    clock_t t2;
	
	while (waiting_uid) // if the packet was received since InitWaitingPacket() was called, it has been already received...
	{
      //LOG(logDEBUG1, this) << "#" << uid << " <- waiting for...";
		t2 = clock();
		if ((t2-t1) >= timeout)
		{
			waiting_uid = 0;
			return false;
		}
		Sleep(16);
		i++;

		if (i * 16 > timeout)
		{
			waiting_uid = 0;
         //LOG(logDEBUG1, this) << "#" << uid << " <- TIMED OUT...";
			return false;
		}
	}

	return true;
}

void AwesomO::InitWaitingState(StateType::StateType state) // use this to set the packetId
{	
	waiting_state = state;
}

bool AwesomO::WaitForState(int timeout) // and this to actually wait for the packet
{
	int i = 0;
	clock_t t1=clock();
    clock_t t2;
	
	while (waiting_state) // if the packet was received since InitWaitingPacket() was called, it has been already received...
	{
		t2 = clock();
		if ((t2-t1) >= timeout)
		{
			waiting_state = StateType::None;
			return false;
		}
		Sleep(16);
		i++;

		if (i * 16 > timeout)
		{
			waiting_state = StateType::None;
			return false;
		}
	}

	return true;
}

void AwesomO::InitWaitingPacket(unsigned char packetid) // use this to set the packetId
{	
	waiting = packetid;
}

bool AwesomO::WaitForInitPacket(int timeout) // and this to actually wait for the packet
{
	int i = 0;
	
	clock_t t1=clock();
    clock_t t2;

	while (waiting) // if the packet was received since InitWaitingPacket() was called, it has been already received...
	{
		t2 = clock();
		if ((t2-t1) >= timeout)
		{
			waiting = 0;
			return false;
		}
		Sleep(16);
		i++;

		if (i * 16 > timeout)
		{
			waiting = 0;
			return false;
		}
	}

	return true;
}

void AwesomO::set(std::string name, std::string value)
{
	vars[name] = value;
}

void AwesomO::set(std::string name, int value)
{
	std::stringstream ss;
	ss << value;
	vars[name] = ss.str();
}

std::string AwesomO::getString(std::string name)
{
	return vars[name];
}

int AwesomO::getNumber(std::string name)
{
	return atoi(vars[name].c_str());
}

void AwesomO::Run(std::string file, std::string function)
{
	commands->push_back(Command(file, function, true));
	Create(false);
}

void AwesomO::RunNow(std::string file, std::string function)
{
	commands->push_front(Command(file, function, false));
	Create(false);
}

void AwesomO::EnableEvents()
{
	events = true;
}

void AwesomO::DisableEvents()
{
	events = false;
}

int AwesomO::Event(std::string name, bool running)
{
	int result = 0;

	if ((IsRunning() || !running) && events)
	{
		//clock_t start = clock();
		//LOG(logDEBUG1, this) << "calling event " << name;

		lua_getglobal(lua_event, name.c_str());

		if (lua_pcall(lua_event, 0, 1, -2) != 0)
		{
			LOG(logERROR, this) << lua_tostring(lua_event, -1);
		}
		else
		{
			result = lua_toboolean(lua_event, -1);
		}

		lua_pop(lua_event, 1);

		//LOG(logDEBUG1, this) << "call to event " << name << " took " << clock() - start << " ms";
	}

	return result;
}

void AwesomO::SendController(std::string message, char id)
{
	if (controller)
	{
		int length = message.length() + 1;
		char * buffer = new char[length];
		buffer[0] = id;
		memcpy(buffer + 1, message.c_str(), message.length());
		controller->Write(buffer, length);
		delete buffer;
	}
}

void AwesomO::ProcessItem(const unsigned char* data, int size)
{
	ITEM item = ItemReader::Read(data);
	item.area = maps->getLevel();

	if (item.baseItem->code == "box")
	{
		me->cube.uid = item.uid;
	}

	if (item.destination != ItemDestination::Cursor && item.uid == me->cursor.uid)
	{
		me->cursor.uid = 0;
	}

	if (item.destination == ItemDestination::Cursor)
	{
		if (item.action != ItemActionType::RemoveFromShop) // for some reason whne u buy an item, server tells us it is going to cursor
		{
			me->cursor = item;
		}
	}



	switch (item.action)
	{
		case ItemActionType::Equip:
		case ItemActionType::WeaponSwitch:
		case ItemActionType::SwapBodyItem:
		{
			if (item.ownerUID == me->uid && item.ownerType == UnitType::Player)
			{
				me->body.equip(item);

				if (item.location == EquipmentLocation::Belt && item.destination != ItemDestination::Cursor)
				{						
					me->beltrows = BaseItem::GetBeltRow(item.baseItem);
				}
			}
		}
		break;
		case ItemActionType::Unequip:
		{
			if (item.ownerUID == me->uid && item.ownerType == UnitType::Player)
			{
				me->body.unequip(item);

				if (item.location == EquipmentLocation::Belt)
				{						
					me->beltrows = 1;
				}
			}
		}
		break;
		case ItemActionType::AddToShop:
		{
			store->add(item);
		}
		break;





		case ItemActionType::AddToGround:
		case ItemActionType::DropToGround:
		case ItemActionType::OnGround:
		{
			items->add(item);

			if (IsRunning())
			{
				int priority = 0;

				lua_getglobal(lua_pickit, "pickItem");
				tolua_pushusertype(lua_pickit, (void *) &item, "ITEM");
				
				if (lua_pcall(lua_pickit, 1, 1, -3) != 0)
				{
					LOG(logERROR, this) << lua_tostring(lua_pickit, -1);
				}
				else
				{
					priority = (int) lua_tointeger(lua_pickit, -1);
				}

				lua_pop(lua_pickit, 1);
			}
		}
		break;






		case ItemActionType::PutInBelt:
		{
			me->belt.add(item);
		}
		break;
		case ItemActionType::RemoveFromBelt:
		{
			me->belt.remove(item);
		}
		break;
		case ItemActionType::SwapInBelt:
		{
			if (item.destination == ItemDestination::Belt)
			{
				me->belt.add(item);
			}
			if (item.destination == ItemDestination::Cursor)
			{
				me->belt.remove(item);
			}
		}
		break;
		case ItemActionType::PutInContainer:
		case ItemActionType::UpdateStats:
		{
			if (item.container == ItemContainer::Inventory)
			{
				me->inventory.add(item);
			}
			if (item.container == ItemContainer::Stash)
			{
				me->stash.add(item);
			}
			if (item.container == ItemContainer::Cube)
			{
				me->cube.add(item);
			}
		}
		break;
		case ItemActionType::RemoveFromContainer:
		{
			if (item.container == ItemContainer::Inventory)
			{
				me->inventory.remove(item);
			}
			if (item.container == ItemContainer::Stash)
			{
				me->stash.remove(item);
			}
			if (item.container == ItemContainer::Cube)
			{
				me->cube.remove(item);
			}
		}
		break;
	}
}
void AwesomO::PrintData(const unsigned char * start, unsigned int length, int offset, const char *title, const char *action)
{
CriticalSection cs;
std::string stream;
char *t;
cs.Lock();
	t = new char[2048];

	if (strlen(title) > 0)
	{
		stream += action;
		stream += " ",
		stream += title;
		stream += " (";
		sprintf(t, "%d", length);
		stream += t;
		stream += "bytes)";
	}

	stream += "\n";
	
	if (offset > 0) {
		sprintf(t, "Packet ID: %02X", start[0]);
		stream += t;
	}
    unsigned int row, col, i = 0;

    for (col = 0; col<68;col++)
    {
        if(col==48||col==49)
            stream += " ";
        else
			if (offset>0 && col>7)
            stream += "_";
        if (col==68)
            stream += "\n";
    }
    for (col = 0; col<70;col++)
        stream += " ";
    stream += "\n";
    for (row = 0; (i + 1) < length; row++)
    {

        for (col = 0; col<16; col++)
        {
            i = row*16+col;

            if (col==8)
                stream += " ";

            if (i<length) {
				sprintf(t, "%02X", start[offset + i]);
				stream += t;
			}
            else
                stream += "  ";

            stream += " ";
        }
        stream += " ";

        for (col = 0; col<16; col++)
        {
            i = row*16+col;

            if (col==8)
                stream += "  ";

            if (offset + i<length)
            {
                if (start[offset + i]>0x20 && start[offset + i]<0x7F) {
					sprintf(t, "%c", start[offset + i]);
					stream += t;
				}
                else {
                    stream += ".";
				}
            }
            else
                break;
        }
        stream += "\n";
    }
    for (col = 0; col<68;col++)
    {
        if(col==48||col==49)
            stream += " ";
        else
            stream += "_";
        if (col==68)
            stream += "\n";
    }
    stream += "\n";
	LOG(logDEBUG2, this) << stream;
	cs.Unlock();
}