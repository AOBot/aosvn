#include <string.h>
#include <ctype.h>
#include "CdKey.h"

const unsigned char CdKey::Map[] = 
{
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0x00, 0xff, 0x01, 0xff, 0x02, 0x03, 0x04, 0x05, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b,
	0x0c, 0xff, 0x0d, 0x0e, 0xff, 0x0f, 0x10, 0xff, 0x11, 0xff, 0x12, 0xff,
	0x13, 0xff, 0x14, 0x15, 0x16, 0xff, 0x17, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0xff, 0x0d, 0x0e,
	0xff, 0x0f, 0x10, 0xff, 0x11, 0xff, 0x12, 0xff, 0x13, 0xff, 0x14, 0x15,
	0x16, 0xff, 0x17, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff
};

CdKey::CdKey(const char* keyText) :
	_valid(false),
	_productId(-1),
	_publicValue(-1),
	_privateValue(-1)
{
	Parse(keyText);
}

bool CdKey::Parse(const char* keyText)
{
	strcpy_s(_keyText, sizeof(_keyText), keyText);
	_valid = ProcessKey(_keyText, &_productId, &_publicValue, &_privateValue);
	return _valid;
}

bool CdKey::ComputeHash(int clientToken, int serverToken, void* hash, int size) const
{
	if (IsValid() && size >= HashSize)
	{
		HashGroup group;

		group.clientToken = clientToken;
		group.serverToken = serverToken;
		group.productId = _productId;
		group.publicValue = _publicValue;
		group.privateValue = _privateValue;
		group.unknown1 = 0;
		group.unknown2 = 0;

		return ComputeHash(&group, sizeof(group), hash, HashSize);
	}

	return false;
}

bool CdKey::IsValid() const
{
	return _valid;
}

int CdKey::GetProductId() const
{
	return _productId;
}

int CdKey::GetPublicValue() const
{
	return _publicValue;
}

int CdKey::GetPrivateValue() const
{
	return _privateValue;
}

const char* CdKey::GetKeyText() const
{
	return IsValid() ? _keyText : 0;
}

int CdKey::RotateLeft(unsigned int value, int shift)
{
	shift &= 0x1f;
	value = (value >> (0x20 - shift)) | (value << shift);
	return value;
}

int CdKey::HexToValue(char c)
{
	c = tolower(c);

	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}

	if (c >= 'a' && c <= 'f')
	{
		return c - 'a' + 0x0a;
	}

	return -1;
}

char CdKey::ValueToHex(int v)
{
	if (v >= 0 && v <= 9)
	{
		return '0' + v;
	}

	if (v >= 0x0a && v <= 0x0f)
	{
		return 'a' + v - 0x0a;
	}

	return -1;
}

int CdKey::HexToValue(const char* s, int n)
{
	int v = 0;

	for (int i = 0; i < n; i++)
	{
		v |= HexToValue(s[i]) << ((n - i - 1) * 4);
	}

	return v;
}

bool CdKey::ComputeHash(const void* input, int sizeIn, void* output, int sizeOut)
{
	if (sizeIn > 1024 || sizeOut < 20)
	{
		return false;
	}

	unsigned char buffer[1024];
	memset(buffer, 0, sizeof(buffer));
	memcpy(buffer, input, sizeIn);
	unsigned int* data = reinterpret_cast<unsigned int*>(buffer);

	for (int i = 0; i < 64; i++)
	{
		int shift = (data[i] ^ data[i + 8] ^ data[i + 2] ^ data[i + 13]) % 32;
		data[i + 16] = RotateLeft(1, shift);
	}

	unsigned int a = 0x67452301;
	unsigned int b = 0xefcdab89;
	unsigned int c = 0x98badcfe;
	unsigned int d = 0x10325476;
	unsigned int e = 0xc3d2e1f0;
	unsigned int g = 0;

	for (int i = 0; i < 20; i++)
	{
		g = *data++ + RotateLeft(a, 5) + e + ((b & c) | (~b & d)) + 0x5a827999;
		e = d; 
		d = c; 
		c = RotateLeft(b, 30); 
		b = a; 
		a = g;
	}

	for (int i = 0; i < 20; i++)
	{
		g = (d ^ c ^ b) + e + RotateLeft(g, 5) + *data++ + 0x6ed9eba1;
		e = d;
		d = c;
		c = RotateLeft(b, 30);
		b = a; 
		a = g;
	}

	for (int i = 0; i < 20; i++)
	{
		g = *data++ + RotateLeft(g, 5) + e + ((c & b) | (d & c) | (d & b)) - 0x70e44324;
		e = d; 
		d = c; 
		c = RotateLeft(b, 30);
		b = a;
		a = g;
	}

	for (int i = 0; i < 20; i++)
	{
		g = (d ^ c ^ b) + e + RotateLeft(g, 5) + *data++ - 0x359d3e2a;
		e = d; 
		d = c;
		c = RotateLeft(b, 30); 
		b = a; 
		a = g;
	}

	unsigned int* hash = static_cast<unsigned int*>(output);

	hash[0] = 0x67452301 + a;
	hash[1] = 0xefcdab89 + b;
	hash[2] = 0x98badcfe + c;
	hash[3] = 0x10325476 + d;
	hash[4] = 0xc3d2e1f0 + e;

	return true;
}

bool CdKey::ProcessKey(const char* keyText, int* productId, int* publicValue, int* privateValue)
{
	char buffer[KeySize + 1];
	strcpy_s(buffer, sizeof(buffer), keyText);

	unsigned int r = 1;
	unsigned int checksum = 0;
	unsigned int n1 = 0;
	unsigned int n2 = 0;
	unsigned int v1 = 0;
	unsigned int v2 = 0;
	unsigned char c1 = 0;
	unsigned char c2 = 0;

	for (int i = 0; i < 16; i += 2) 
	{
		c1 = Map[keyText[i]];
		n1 = c1 * 3;

		c2 = Map[buffer[i + 1]];
		n1 = c2 + n1 * 8;

		if (n1 >= 0x100)
		{ 
			n1 -= 0x100;
			checksum |= r;
		}

		n2 = n1 >> 4;

		buffer[i] = ValueToHex(n2 % 16);
		buffer[i + 1] = ValueToHex(n1 % 16);

		r <<= 1;
	}

	v1 = 0x03;
	for (int i = 0; i < 16; i++) 
	{
		c1 = buffer[i] & 0xff;
		n1 = HexToValue(c1);
		n2 = v1 * 2;
		n1 ^= n2;
		v1 += n1;
	}
	v1 &= 0xff;

	if (v1 != checksum)
	{
		*productId = -1;
		*publicValue = -1;
		*privateValue = -1;

		return false;
	}

	n1 = 0;
	for (int i = 15; i >= 0; i--) 
	{
		c1 = buffer[i];
		if (i > 8) 
		{
			n1 = i - 9;
		} 
		else 
		{
			n1 = 0x0f + i - 8;
		}

		n1 &= 0x0f;
		c2 = buffer[n1];
		buffer[i] = c2;
		buffer[n1] = c1;
	}

	v2 = 0x13ac9741;
	for (int i = 15; i >= 0; i--) 
	{
		buffer[i] = toupper(buffer[i]);
		c1 = buffer[i];

		if (c1 <= '7') 
		{
			v1 = v2;
			c2 = ((v1 & 0xff) & 7) ^ c1;
			v1 >>= 3;
			buffer[i] = c2;
			v2 = v1;
		} 

		else if (c1 < 'A') 
		{
			buffer[i] = (i & 1) ^ c1;
		}
	}

	*productId = HexToValue(buffer, 2);
	*publicValue = HexToValue(buffer + 2, 6);
	*privateValue = HexToValue(buffer + 8, 8);

	return true;
}