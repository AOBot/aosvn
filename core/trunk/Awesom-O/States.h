#pragma once

#include "CriticalSection.h"
#include "d2data/StateType.h"
#include "d2data/Stat.h"
#include <vector>
#include <map>

//class StatBase;

class States { // tolua_export

private:
	CriticalSection cs;
	std::map<StateType::StateType, std::vector<StatBase>> state_map;

public:
	void setState(StateType::StateType state, std::vector<StatBase> stats);
	void endState(StateType::StateType state);
	//tolua_begin
	bool isActive(StateType::StateType state);
	std::vector<StatBase> getStats(StateType::StateType state);
};
// tolua_end
