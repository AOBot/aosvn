#pragma once

#include <ctime>
#include "MonsterInfo.h"
#include "BitsReader.h"
#include "Npcs.h"

class NpcReader
{
private:

	static int bsr(BYTE data)
	{
		for (size_t i = sizeof(data) * 8 - 1; i >= 0; i--)
			if (data & (1 << i))
				return (int)i;
		return -1;
	}

public:
	static MonsterInfo monInfo;

	static NPC parseACPacket(const BYTE * packetData, size_t size, unsigned char difficulty) // Special thx to anyway1234 for the AC parser
	{
		NPC npc;
		size_t resultLen = 0;
		npc.flags = 0;
		npc.uniqueid = SuperUnique::None;
		npc.time = (int)time(NULL);
		npc.count = 0;
		npc.mode = NPCMode::Alive;
		npc.revived = false;
		BitsReader bitStream(packetData, size);
		unsigned int result = 0;
		bitStream.getBits(8, &result); //skip packetid
		bitStream.getBits(32, &npc.uid); //read  uid  
		
		bitStream.getBits(16, (unsigned int *) &npc.id); //read npcid
		bitStream.getBits(32, &result); //read  location

		npc.x = (WORD) result;
		npc.y = (WORD) (result >> 16);
		
		ResistInfo resist;
		monInfo.getMonsterResist(npc.id,&resist);
		memcpy(npc.res,&resist.res[difficulty], sizeof(resist.res[difficulty]));
		/*resist.res[RT_Cold];
		resist.res[RT_Damage];
		resist.res[RT_Fire];
		resist.res[RT_Lightning];
		resist.res[RT_Magic];
		resist.res[RT_Poison];*/

		bitStream.getBits(8,&result); //read hp
		npc.life = result;
		if(result < 0) npc.life_percent = result ^ 0xFF; // make it positive
		npc.life_percent = static_cast<unsigned char>((static_cast<float>(npc.life_percent))/128*100);

		bitStream.getBits(8,&result); //read packet length
		//npc.length = result;
		BYTE gfxTypeInfo[NPC_GFX_TYPE_NUM];
		monInfo.getGfxTypeInfo(npc.id, gfxTypeInfo); 

		bitStream.getBits(4, &result);	//step 1
		bitStream.getBits(1, &result);	//step 2
		if (result){//step 3
			for (size_t i = 0; i < NPC_GFX_TYPE_NUM; i++){
				BYTE bitCount = 1;
				if (gfxTypeInfo[i] > 2){
					bitCount = bsr(gfxTypeInfo[i] - 1) + 1;
				}
				else
					bitCount =1;
				bitStream.getBits(bitCount, &result);
			}
		}
		bitStream.getBits(1, &result); //step 4
		if (result == 0)
			return npc;

		//step 5
	/*  bit 4 = Is Champion
		bit 5 = Is Super prefixed
		bit 6 = Is Super unique (bit 5 must be set, other wise normal monster?)
		bit 7 = Is Minion
		bit 8 = Is Cloaked*/

		WORD flags = 0;
		bitStream.getBits(1, &result);//4
		flags |= (result << 2);//2 champion, berserk have this set also?
		bitStream.getBits(1, &result);//8
		flags |= (result << 3);//both boss, champion, berserk have this set? bosses do have this set
		bitStream.getBits(1, &result);
		flags |= (result << 1);
		bitStream.getBits(1, &result);//0x10
		flags |= (result << 4); //4th bit is the minion bit
		bitStream.getBits(1, &result);//0x40 if it is ghostly, set this bit?
		flags |= (result << 6);

		npc.flags = flags;	

		//step 6
		if (flags & 2){
			bitStream.getBits(16, &result);
			npc.uniqueid = (SuperUnique::SuperUnique) result;
			//if (monInfo.getSuperUniqueName(result, name)) 
				//myOutputDebugStr("super unique: %s\n", name);
			//else
				//myOutputDebugStr("super unique id: %s", result);
		}
		//else if (flags & 0x10)
			//myOutputDebugStr("minion\n");
		//else if ((flags & 0xc) == 8)
			//myOutputDebugStr("unique boss\n");
		//step 7
		bitStream.getBits(8, &result);
		if (result){
			//myOutputDebugStr("%s\n", umodNames[result]);		
			npc.mods.push_back((MonsterMod::MonsterMod) result);
			do{ //step 8
				if (!bitStream.getBits(8, &result))
					break;			
				npc.mods.push_back((MonsterMod::MonsterMod) result);
				switch((MonsterMod::MonsterMod) result)
				{			
					case MonsterMod::cold:
					{
						npc.res[ResistType::Cold] += 75;					
					}
					break;
					case MonsterMod::fire:
					{
						npc.res[ResistType::Fire] += 75;
					}
					break;
					case MonsterMod::light:
					{
						npc.res[ResistType::Lightning] += 75;
					}
					break;
					case MonsterMod::resist:
					{
						npc.res[ResistType::Lightning] += 40;
						npc.res[ResistType::Fire] += 40;
						npc.res[ResistType::Cold] += 40;
					}
					break;
					case MonsterMod::manahit:
					{
						npc.res[ResistType::Magic] += 20;
					}
					break;
					case MonsterMod::spectralhit:
					{
						npc.res[ResistType::Lightning] += 20;
						npc.res[ResistType::Fire] += 20;
						npc.res[ResistType::Cold] += 20;
					}
					case MonsterMod::stoneskin:
					{
						npc.res[ResistType::Damage] += 50;				
					}
					break;
				}
				//if (result)
					//myOutputDebugStr("%s\n", umodNames[result]);
			}
			while(result);
		}
		
		//step 9
		bitStream.getBits(16, &result);
		
		//step 10
		bitStream.getBits(1, &result);

		if (result == 0)
			return npc;

		//step 11
		bitStream.getBits(32, &result);

		return npc;
	}

	static bool parseAAPacket(const BYTE *packetData, size_t size, NPC &npc, unsigned char difficulty)
	{
		int c1 = 0;
		int c2 = 0;

		npc.states.clear();

		BitsReader bitStream(packetData, size);
		unsigned int result = 0;

		bitStream.getBits(8, &result);
		bitStream.getBits(8, &result);
		bitStream.getBits(32, &result);
		bitStream.getBits(8, &result);
		int size2 = sizeof(unsigned int);			
		do
		{
			c1++;
			c2 = 0;

			if (c1 > 100)
				return false;

			bitStream.getBits(8, &result);
			if(result == 0xFF)
				break;
			if(result == 96)
				npc.revived = true;
			npc.states.push_back((StateType::StateType)result);
			bitStream.getBits(1, &result);		
			if (result == 0)
				continue;
			do
			{
				c2++;

				if (c2 > 100)
					return false;

				bitStream.getBits(9, &result); // itemstatscost->id
				if (result > 0x166)
					break;
				//read the Send Bits and Send Param Bits from itemstatcost.txt and read them
				ItemStatCost cost;
				monInfo.getItemStatCost(result,&cost);
				unsigned int sendbits;
				unsigned int sendparambits;
				if(cost.SendBits > 0)
					bitStream.getBits(cost.SendBits, &sendbits);
				if(cost.SendParamBits > 0 )
					bitStream.getBits(cost.SendParamBits, &sendparambits);			
			} while(true);
		}while(true);
		
		return true;
	}
	static NPC NewMonster(const BYTE * data, size_t size, unsigned char difficulty) 		
	{
		////////////////////////////////////////////////////////////
		//thx to TheUnkownSoldier @ EON for the complete structure//
		////////////////////////////////////////////////////////////
		unsigned int PacketSize;
		NPC npc;
		unsigned int r = 0;

		BitsReader br(data,size);
		br.read(8);

		npc.uid = br.read(32);
		npc.id = (NPCCode::NPCCode)br.read(16);
		npc.x = br.read(16);
		npc.y = br.read(16);
		npc.life = br.read(8);
		if(npc.life < 0) npc.life_percent = npc.life ^ 0xFF; // make it positive
		npc.life_percent = static_cast<unsigned char>((static_cast<float>(npc.life_percent))/128*100);

		PacketSize = br.read(8);
		short MonAnim = br.read(4);

		ResistInfo resist;
		monInfo.getMonsterResist(npc.id,&resist);
		memcpy(npc.res,&resist.res[difficulty], sizeof(resist.res[difficulty]));

		r = br.read(1);		

		if (r)
		{
			int id = npc.id;

			/*switch(id)
			{
				case NPCCode::UberMephisto: id = 604; break;
				case NPCCode::Lilith: id = 605; break;
				case NPCCode::UberIzual: id = 606; break;
				case NPCCode::UberDuriel: id = 607; break;
				case NPCCode::UberBaal: id = 608; break;
			}
			if (id > 608)
				return npc;*/

			BYTE gfxTypeInfo[NPC_GFX_TYPE_NUM];
			monInfo.getGfxTypeInfo(id, gfxTypeInfo); 

			int bits = 0;
			for (size_t i = 0; i < NPC_GFX_TYPE_NUM; i++)
			{
				if (gfxTypeInfo[i] > 2 && gfxTypeInfo[i] < 10 )
					bits += bsr(gfxTypeInfo[i] - 1) + 1;
				else
					bits += 1;
			}
			br.read(bits);
		}

		r = br.read(1);		

		if (r) //Monster mods
		{
			npc.hero = (bool)br.read(1);	
			npc.unique = (bool)br.read(1);
			npc.superunique = (bool)br.read(1);
			npc.minion = (bool)br.read(1);
			npc.ghostly = (bool)br.read(1);
			if (npc.superunique)
				npc.uniqueid = (SuperUnique::SuperUnique)br.read(16);

			for (int i = 0; i <= 10; i++)
			{
				short result = br.read(8);
				if(result == 0)
					break;
				npc.mods.push_back((MonsterMod::MonsterMod) result);
				switch((MonsterMod::MonsterMod) result)
				{			
					case MonsterMod::cold:
					{
						npc.res[ResistType::Cold] += 75;					
					}
					break;
					case MonsterMod::fire:
					{
						npc.res[ResistType::Fire] += 75;
					}
					break;
					case MonsterMod::light:
					{
						npc.res[ResistType::Lightning] += 75;
					}
					break;
					case MonsterMod::resist:
					{
						npc.res[ResistType::Lightning] += 40;
						npc.res[ResistType::Fire] += 40;
						npc.res[ResistType::Cold] += 40;
					}
					break;
					case MonsterMod::manahit:
					{
						npc.res[ResistType::Magic] += 20;
					}
					break;
					case MonsterMod::spectralhit:
					{
						npc.res[ResistType::Lightning] += 20;
						npc.res[ResistType::Fire] += 20;
						npc.res[ResistType::Cold] += 20;
					}
					case MonsterMod::stoneskin:
					{
						npc.res[ResistType::Damage] += 50;				
					}
					break;
				}
			}
			short MonNameIndex = br.read(16);

			npc.hasowner = br.read(1);
			if (npc.hasowner)
				npc.owner = br.read(32);

		}
		
		npc.isPet = br.read(1);
		if (npc.isPet)
			npc.petowner = br.read(32);

		//nbytes of stats left, cba.
		
		return npc;
	}
	
};

MonsterInfo NpcReader::monInfo;