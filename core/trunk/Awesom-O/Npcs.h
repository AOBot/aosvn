#pragma once

#include "CriticalSection.h"
#include <map>
#include <vector>
#include <algorithm>
#include <list>
#include <cmath>
#include "MonsterInfo.h"
#include "d2data/NPCType.h"
#include "d2data/NPCCode.h"
#include "d2data/StateType.h"
#include "d2data/Game.h"

//tolua_begin
struct NPC
{
	unsigned int uid, owner, petowner;
	NPCCode::NPCCode id;
	NPCMode::NPCMode mode;
	short x;
	short y;
	char life;
	unsigned char life_percent;
	int time;
	int count;
	bool revived, hero, unique, superunique, minion, ghostly, hasowner, ispet, hasStats;
	SuperUnique::SuperUnique uniqueid;
	signed short res[6];
	unsigned short flags;
	std::vector<MonsterMod::MonsterMod> mods;
	std::vector<StateType::StateType> states;
	bool redeemed;
	bool isPet;
	int distance;
	std::string name;
	//std::vector<StatBase> Stats;
	
	NPC()
	{
		uid = 0;
		owner = 0;
		petowner = 0;
		x = 0;
		y = 0;
		life = 0;
		life_percent = 0;
		time = 0;
		count = 0;
		revived = false;
		redeemed = false;
		hero = false;
		superunique = false;
		unique = false;
		minion = false;
		ghostly = false;
		hasowner = false;
		ispet = false;
		distance = 9999;
		uniqueid = (SuperUnique::SuperUnique)-1;
	}

	bool IsImmune(ResistType::ResistType resist)
	{
		return res[resist] >= 100;
	}

	bool IsHero()
	{
		return !minion;
	}

	bool HasMod(MonsterMod::MonsterMod mod)
	{
		for (std::vector<MonsterMod::MonsterMod>::iterator iter = mods.begin(); iter != mods.end(); iter++)
		{
			if (*iter == mod)
			{
				return true;
			}
		}

		return false;
	}
	
	bool HasState(StateType::StateType state)
	{
		for (std::vector<StateType::StateType>::iterator iter = states.begin(); iter != states.end(); iter++)
		{
			if (*iter == state)
			{
				return true;
			}
		}
		
		return false;
	}

	bool HasFlags(unsigned short flags)
	{
		return (this->flags & flags) == flags;
	}

	int getDistance(short x, short y)
	{
		return (int)::sqrt((double)((this->x - x) * (this->x - x) + (this->y - y) * (this->y - y)));
	}
	signed short getResist(ResistType::ResistType resist)
        {
                signed short theres;
                theres = res[resist];
                return theres;
        }
};
// tolua_end

class Npcs { // tolua_export

private:
	CriticalSection cs;
	std::list<NPC> list;

public:
	Npcs();
	~Npcs();
	//tolua_begin
	void add(NPC npc);
	void remove(unsigned int uid);
	NPC findByNpcCode(NPCCode::NPCCode id, int max=0);
	NPC find(unsigned int uid, int max=0);
	NPC findInRadius(short x, short y, short radius, int max=0);
	NPC findHero();
	NPC findHeroInRadius(short x, short y, short radius);
	NPC findBySuperUnique(SuperUnique::SuperUnique uniqueid, int max=0);
	NPC findCorpseInRadius(short x, short y, short radius);
    std::vector<NPC> findAllInRadius(short x, short y, short radius);

	int countInRadius(short x, short y, short radius);
	void update(NPC npc);
	void Npcs::update(unsigned int uid, NPCMode::NPCMode mode);
	void update(unsigned int uid, short x, short y);
	void update(unsigned int uid, short x, short y, char life);
	void update(unsigned int uid, short x, short y, char life, NPCMode::NPCMode mode);
	void life(unsigned int uid, char life);
	void getPoints(std::vector<POINT> &points);
	void Npcs::redeem(unsigned int uid);
};
// tolua_end
