#include "Players.h"
#include "Log.h"

Players::Players()
{
}

Players::~Players()
{
}

void Players::setUID(unsigned int uid)
{
	this->uid = uid;
}
	
void Players::add(PLAYER player) throw(...)
{
	cs.Lock();
	list.push_back(player);
	cs.Unlock();
}

void Players::UpdateSkill(const unsigned char* data) throw(...)
{
//	short unitType = *reinterpret_cast<const short*> (data + 1);
	unsigned int uid = *reinterpret_cast<const int*> (data + 3);
//	short skill = 
//	char baseLevel
//	char bonus = data[10];

	cs.Lock();
/*
	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			SKILL skill;
			skill.skill = (SkillType::SkillType) *reinterpret_cast<const short*> (data + 7);
			skill.baseLevel = data[9];

			iter->skills.add(skill);
		}
	}
*/
	cs.Unlock();
}

bool Players::FindSkill(unsigned int uid, SkillType::SkillType skill) throw(...)
{
	bool result;
	cs.Lock();
	result = false;
/*
	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			result = iter->skills.find(skill);
		}
	}
*/
	cs.Unlock();
	return result;
}

void Players::SkillsLog(const unsigned char* data) throw(...)
{
	unsigned int uid = *reinterpret_cast<const int*> (data + 2);

	cs.Lock();
/*
	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			for (int i = 0; i < data[1]; i++)
			{
				SKILL skill;
				skill.skill = (SkillType::SkillType) *reinterpret_cast<const short*> (data + 6 + i * 3);
				skill.baseLevel = data[8 + i * 3];
				
				iter->skills.add(skill);
			}
		}
	}
*/
	cs.Unlock();
}

unsigned int Players::account(std::string playerName, std::string accountName) throw(...)
{
	unsigned int result = 0;
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (playerName.compare(iter->playerName) == 0)
		{
			result = iter->uid;
			iter->accountName = accountName;
			break;
		}
	}

	cs.Unlock();
	return result;
}

void Players::remove(std::string playerName) throw(...)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (playerName.compare(iter->playerName) == 0)
		{
			list.erase(iter);
			break;
		}
	}

	cs.Unlock();
}

void Players::remove(unsigned int uid) throw(...)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			list.erase(iter);
			break;
		}
	}

	cs.Unlock();
}

PLAYER Players::find(unsigned int uid) throw(...)
{
	PLAYER player;
	player.uid = 0;
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			player = *iter;
		}
	}

	cs.Unlock();
	return player;
}

PLAYER Players::findByPlayerName(std::string playerName) throw(...)
{
	PLAYER player;
	player.uid = 0;
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (playerName.compare(iter->playerName) == 0)
		{
			player = *iter;
		}
	}

	cs.Unlock();
	return player;
}

PLAYER Players::findPlayerInRadius(short x, short y, int radius) throw(...)
{
	PLAYER player;
	player.uid = 0;
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid != uid && iter->getDistance(x, y) < radius)
		{
			player = *iter;
		}
	}

	cs.Unlock();
	return player;
}

void Players::update(unsigned int uid, short x, short y) throw(...)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->x = x;
			iter->y = y;
		}
	}

	cs.Unlock();
}

void Players::update(unsigned int uid, short x, short y, bool close) throw(...)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->x = x;
			iter->y = y;
			iter->close = close;
		}
	}

	cs.Unlock();
}

void Players::update(unsigned int uid, bool close) throw(...)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->close = close;
		}
	}

	cs.Unlock();
}

void Players::update(unsigned int uid, int life, AreaLevel::AreaLevel area)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			//LOG(logDEBUG1, NULL) << "update " << uid << ", life " << life << ", area " << AreaLevel::toString(area);
			iter->life = life;
			iter->area = area;
		}
	}

	cs.Unlock();
}

void Players::setLife(unsigned int uid, int life) throw(...)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->life = life;
		}
	}

	cs.Unlock();
}

void Players::setRelationship(unsigned int uid, PartyRelationshipType::PartyRelationshipType relationship)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->relationship = relationship;
		}
	}

	cs.Unlock();
}

void Players::hostile(unsigned int uid) throw(...)
{
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->hostiled = true;
		}
	}

	cs.Unlock();
}

int Players::distance(unsigned int uid, short x, short y) throw(...)
{
	int result = -1;
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			result = (int)::sqrt((double)((iter->x - x) * (iter->x - x) + (iter->y - y) * (iter->y - y)));
		}
	}

	cs.Unlock();
	return result;
}

unsigned int Players::size() throw(...)
{
	int result;
	cs.Lock();

	result = (int)list.size();

	cs.Unlock();
	return result;
}

std::vector<PLAYER> Players::findByRelationship(PartyRelationshipType::PartyRelationshipType relationship) throw(...)
{
	std::vector<PLAYER> players;
	cs.Lock();

	for (std::list<PLAYER>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->relationship == relationship)
		{
			players.push_back(*iter);
		}
	}

	cs.Unlock();
	return players;
}