#ifndef GAMEPROXY_H
#define GAMEPROXY_H

#include "Compression.h"
#include "Connection.h"
#include "Proxy.h"
#include "Log.h"

class GameProxy : public Proxy
{
public:
	bool magic;

	virtual int OnRelayDataToServer();
	virtual int OnRelayDataToClient();

	virtual void error();

	void WriteServer(const unsigned char * buffer, int length);
	void WriteClient(const unsigned char * buffer, int length);

	static const int ServerPacketSizes[];
	static const int ClientPacketSizes[];

private:
	int GetDecompressedPacketSize(const void * data, int size);
	int GetChatPacketSize(const void * data, int size);

	bool GetCurrentPacketSize(int* size, bool* ready) const;
	bool GetBubblePacketSize(int* size, bool* ready) const;
	bool GetChatPacketSize(int* size, bool* ready) const;
	bool GetWardenPacketSize(int* size, bool* ready) const;

public:
	GameProxy(SOCKET s) : Proxy(s), magic(false)
	{
		LOG(logINFO, awesomo) << "created game proxy";
	}

	~GameProxy();
};

const int GameProxy::ServerPacketSizes[] = 
{
	1,	8,	1,	12,	1,	1,	1,	6,	6,	11,	6,	6,	9,	13,	12,	16, 
	16,	8,	26,	14,	18,	11,	-1,	-1,	15,	2,	2,	3,	5,	3,	4,	6,
	10,	12,	12,	13,	90,	90,	-1,	40,	103,97,	15,	-1,	8,	-1,	-1,	-1,
	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	34,	8,
	13,	-1,	6,	-1,	-1,	13,	-1,	11,	11,	-1,	-1,	-1,	16,	17,	7,	1,
	15,	14,	42,	10,	3,	-1,	-1,	14,	7,	26,	40,	-1,	5,	6,	38,	5,
	7,	2,	7,	21,	-1,	7,	7,	16,	21,	12,	12,	16,	16,	10,	1,	1,
	1,	1,	1,	32,	10,	13,	6,	2,	21,	6,	13,	8,	6,	18,	5,	10,
	4,	20,	29,	-1,	-1,	-1,	-1,	-1,	-1,	2,	6,	6,	11,	7,	10,	33,
	13,	26,	6,	8,	-1,	13,	9,	1,	7,	16,	17,	7,	-1,	-1,	7,	8,
	10,	7,	8,	24,	3,	8,	-1,	7,	-1,	7,	-1,	7,	-1,	-1,	-1,	-1, 
	1
};

const int GameProxy::ClientPacketSizes[] =
{
	-1,	5,	9,	5,	9,	5,	9,	9,	5,	9,	9,	1,	5,	9,	9,	5,	
	9,	9,	1,	9,	-1,	-1,	13,	5,	17,	5,	9,	9,	3,	9,	9,	17, 
	13,	9,	5,	9,	5,	9,	13,	9,	9,	9,	9,	-1,	-1,	1,	3,	9, 
	9,	9,	17,	17,	5,	17,	9,	5,	13,	5,	3,	3,	9,	5,	5,	3, 
	1,	1,	1,	1,	17,	9,	13,	13,	1,	9,	-1,	9,	5,	3,	-1,	7,
	9,	9,	5,	1,	1,	-1,	-1,	-1,	3,	17,	-1,	-1,	-1,	7,	6,	5,
	1,	3,	5,	5,	9,	17,	-1,	-1,	37,	1,	1,	1,	1,	13,	-1, 1
};

GameProxy::~GameProxy()
{
	if (awesomo)
	{
		awesomo->Abort();
		awesomo->Join();
		awesomo->maps->JoinAll();

		delete awesomo->commands;
		delete awesomo->maps;
		delete awesomo->players;
		delete awesomo->objects;
		delete awesomo->warps;
		delete awesomo->npcs;
		delete awesomo->skills;
		delete awesomo->me;
		delete awesomo->merc;
		delete awesomo->store;
		delete awesomo->items;
		delete awesomo->pickit;
		delete awesomo->files;

		awesomo->game = NULL;
		lua_close(awesomo->lua_pickit);
		lua_close(awesomo->lua_event);

		if (awesomo->controller)
		{
			char buffer[1] = {ControllerEvent::GAME_DESTRUCTOR};
			awesomo->controller->Write(buffer, sizeof(buffer));
		}
	}

	LOG(logINFO, awesomo) << "deleted game proxy";
}

void GameProxy::error()
{
	if (awesomo->game)
	{
		LOG(logERROR, awesomo) << "random game socket error";
	}
	else
	{
		LOG(logERROR, awesomo) << "Game socket error, disconnected";
		LOG(logINFO, awesomo) << "shutting down bot thread";
		awesomo->Abort();
		awesomo->Join();
		LOG(logINFO, awesomo) << "bot thread shut down";
	}
}

void GameProxy::WriteClient(const unsigned char * buffer, int length) throw(...)
{
	client->writing.Lock();
	memcpy(client->write + client->write_length, buffer, length);
	client->write_length += length;
	client->writing.Unlock();
}

void GameProxy::WriteServer(const unsigned char * buffer, int length) throw(...)
{
	char compressed[kBufferSize];
	int compressed_size = Compression::CompressPacket((char *)buffer, length, compressed, kBufferSize);

	server->writing.Lock();
	memcpy(server->write + server->write_length, compressed, compressed_size);
	server->write_length += compressed_size;
	server->writing.Unlock();
}

int GameProxy::OnRelayDataToServer()
{
	bool packetReady;
	int packetSize;

	if (!GetCurrentPacketSize(&packetSize, &packetReady))
	{
		// packet could not be deserialized because of a problem with the packet parser;
		// send what we have any way in an attempt to keep the application from disconnecting
		LOG(logERROR, awesomo) << "Unexpected client packetId encountered: " << std::hex << server->read[0];
		
		// write the data to the socket
		client->Write(server->read, server->read_length);

		// set the buffer to zero
		server->read_length = 0;
	}
	else if (packetReady && packetSize <= server->read_length)
	{
//		packet = new Packet(GetData(), packetSize);
//		RemoveData(packet->GetSize());

		if (server->read[0] == 0x68)
		{
			unsigned char hash[4];
			unsigned char token[2];

			memcpy(hash, server->read + 1, 4);
			memcpy(token, server->read + 5, 2);

			for (size_t i = 0; i < ao.size(); i++)
			{
				if (memcmp(ao[i]->realm_hash, hash, 4) == 0 && memcmp(ao[i]->realm_token, token, 2) == 0)
				{
					awesomo = ao[i];

					if (awesomo->game)
					{
						LOG(logINFO, awesomo) << "game should be null";
					}

					awesomo->game = this;
					awesomo->Initialize();

					SOCKET c = new_client(4000, awesomo->game_address, awesomo->sock);

					if (c == SOCKET_ERROR)
					{
						LOG(logERROR, NULL) << "can't connect to game server...";
						return 1;
					}

					client = new Connection(c);

					LOG(logINFO, awesomo) << "connected game proxy";

					if (awesomo && awesomo->controller)
					{
						char buffer[1] = {ControllerEvent::CONNECTED_GAME_PROXY};
						awesomo->controller->Write(buffer, sizeof(buffer));
					}
				}
			}
		}

		int size = packetSize;

		try
		{
			size = awesomo->OnRelayDataToServer((unsigned char *) server->read, packetSize);
		}
		catch (char * message)
		{
			LOG(logERROR, awesomo) << "Error: " << message;
		}
		catch (...)
		{
			LOG(logERROR, awesomo) << "Error: Unknown Error";
		}

		if (size > 0)
		{
			WriteClient((const unsigned char *)server->read, size);
		}

		if (packetSize == server->read_length)
		{
			// Everything got sent, so take a shortcut on clearing buffer.
			server->read_length = 0;
		}
		else
		{
			// We sent part of the buffer's data.  Remove that data from
			// the buffer.
			server->read_length -= packetSize;
			memmove(server->read, server->read + packetSize, server->read_length);
		}
	}

	return 0;
}


int GameProxy::OnRelayDataToClient()
{
	int packet_size = client->read_length;

	if (client->magic)
	{
		int compressedSize = Compression::GetPacketSize(client->read, client->read_length);

		if (compressedSize <= client->read_length)
		{
			char buffer[2048];
			
			int decompressedSize = Compression::DecompressPacket(client->read, compressedSize, buffer, sizeof(buffer));

			if (decompressedSize > 0)
			{
				packet_size = compressedSize;

				int offset = 0;

				while (offset < decompressedSize)
				{
					int packetSize = GetDecompressedPacketSize(buffer + offset, decompressedSize - offset);

					if (packetSize < 0)
					{
						LOG(logINFO, awesomo) << "Error: Unexpected server packetId encountered: 0x" << (int) buffer[offset];
						break;
					}

					if (packetSize < 0 || decompressedSize - offset < 0)
					{
						LOG(logINFO, awesomo) << "Error: Unexpected end of server data encountered";
						break;
					}
/*
					printf("GS: %-5d", packetSize);

					for (int i = 0; i < packetSize; i++)
					{
						printf("%02x ", (unsigned char) buffer[i + offset]);
					}
					printf("\n");
*/
					int size = packetSize;

					try
					{
						size = awesomo->OnRelayDataToClient((unsigned char *) buffer + offset, packetSize);
					}
					catch (char * message)
					{
						LOG(logERROR, awesomo) << "Error: " << message;
					}
					catch (...)
					{
						LOG(logERROR, awesomo) << "Error: Unknown Error";
					}

					if (size > 0)
					{
						WriteServer((const unsigned char *)buffer + offset, size);
					}

					offset += packetSize;
				}

				if (packet_size == client->read_length)
				{
					// Everything got sent, so take a shortcut on clearing buffer.
					client->read_length = 0;
				}
				else
				{
					// We sent part of the buffer's data.  Remove that data from
					// the buffer.
					client->read_length -= packet_size;
					memmove(client->read, client->read + packet_size, client->read_length);
				}

				return 0;
			}
			else
			{
				LOG(logERROR, awesomo) << "zero length packet";
			}
		}
		else
		{
			//printf("\nincomplete packet %d %d\n", compressedSize, client->read_length);
			return 0;
		}
	}
	else
	{
		client->read_length = 0;
		client->magic = true;
		return 0;
	}

	// write the data to the socket
	server->Write(client->read, packet_size);

	if (packet_size == client->read_length)
	{
        // Everything got sent, so take a shortcut on clearing buffer.
		client->read_length = 0;
    }
    else
	{
        // We sent part of the buffer's data.  Remove that data from
        // the buffer.
		client->read_length -= packet_size;
		memmove(client->read, client->read + packet_size, client->read_length);
    }

	return 0;
}

int GameProxy::GetDecompressedPacketSize(const void * data, int size)
{
	const unsigned char* bytes = reinterpret_cast<const unsigned char*>(data);
	int packetId = *static_cast<const unsigned char*>(data);

	switch (packetId)
	{
	case 0x26:
		return GetChatPacketSize(data, size);

	case 0x5b:
		return *reinterpret_cast<const short*>(bytes + 1);

	case 0x94:
		if (size >= 2)
		{
			return bytes[1] * 3 + 6;
		}
		break;

	case 0xa8:
		if (size >= 7)
		{
			return bytes[6];
		}
		break;

	case 0xaa:
		if (size >= 7)
		{
			return bytes[6];
		}
		break;

	case 0xac:
		if (size >= 13)
		{
			return bytes[12];
		}
		break;

	case 0xae:
		if (size >= 3)
		{
			return 3 + *reinterpret_cast<const short*>(bytes + 1);
		}
		break;

	case 0x9c:
		if (size >= 3)
		{
			return bytes[2];
		}
		break;

	case 0x9d:
		if (size >= 3)
		{
			return bytes[2];
		}
		break;

	default:
		if (packetId < sizeof(ServerPacketSizes) / sizeof(int))
		{
			return ServerPacketSizes[packetId];
		}
		break;
	}

	return -1;
}

int GameProxy::GetChatPacketSize(const void * data, int size)
{
	if (size >= 12)
	{
		const unsigned char* bytes = reinterpret_cast<const unsigned char*>(data);
		
		const char* name = reinterpret_cast<const char*>(bytes + 10);
		int nameLength = static_cast<int>(strlen(name));

		const char* message = reinterpret_cast<const char*>(bytes + 10 + nameLength + 1);
		int messageLength = static_cast<int>(strlen(message));
		
		return 10 + nameLength + 1 + messageLength + 1;
	}

	return -1;
}













bool GameProxy::GetCurrentPacketSize(int* size, bool* ready) const
{
	if (server->read_length <= 0)
	{
		// no packet data has been read in at all; this obviously means that 
		// it is impossible to determine the length of this packet
		*size = -1;
		*ready = false;
		return false;
	}

	// first byte of the packet contains the packetId
	int packetId = *static_cast<const char*>(server->read);
	if (packetId > sizeof(ClientPacketSizes) / sizeof(int))
	{
		// packetId is greater than the size of the packetId table;
		// this means that this packet is probably invalid
		*size = -1;
		*ready = false;
		return false;
	}

	// get the size of the packet out of the packet size table via packetId
	int packetSize = ClientPacketSizes[packetId];
	if (packetSize >= 0)
	{
		// packetSize is valid; this means that the size of this
		// packet is fixed and doesn't need interpretation
		*size = packetSize;
		*ready = true;
		return true;
	}

	// packetSize is invalid; this means that the packet has a variable
	// size so the contents must be interpreted
	switch (packetId)
	{
	case 0x14:
		return GetBubblePacketSize(size, ready);

	case 0x15:
		return GetChatPacketSize(size, ready);

	case 0x66:
		return GetWardenPacketSize(size, ready);
	}

	// don't know how to interpret this packet, perhaps it was never seen before
	*size = -1;
	*ready = false;
	return false;
}

bool GameProxy::GetBubblePacketSize(int* size, bool* ready) const
{
	int chatOffset = 3;

	if (server->read_length < chatOffset)
	{
		// not enough data has been received to be able to get the beginning
		// of the player's chat string; impossible to determine packet size
		*size = -1;
		*ready = false;
		return true;
	}

	const unsigned char* bytes = reinterpret_cast<const unsigned char*>(server->read);
	
	bool chatTerminated = false;
	int chatLength = 0;

	// iterate through the packet data in order to try to find the 
	// zero terminator for the player's chat string
	for (int i = chatOffset; i < server->read_length && !chatTerminated; i++)
	{
		if (bytes[i] == 0x00)
		{
			chatTerminated = true;
		}

		else
		{
			chatLength++;
		}
	}

	if (!chatTerminated)
	{
		// the character's chat string has not been zero terminated; 
		// there is not enough data to determine packet size
		*size = -1;
		*ready = false;
		return true;
	}

	// calculate the size of the packet
	*size = chatOffset + chatLength + 1 + 2;
	*ready = true;
	return true;
}

bool GameProxy::GetChatPacketSize(int* size, bool* ready) const
{
	int chatOffset = 3;

	if (server->read_length < chatOffset)
	{
		// not enough data has been received to be able to get the beginning
		// of the player's chat string; impossible to determine packet size
		*size = -1;
		*ready = false;
		return true;
	}

	const unsigned char* bytes = reinterpret_cast<const unsigned char*>(server->read);
	
	bool chatTerminated = false;
	int chatLength = 0;

	// iterate through the packet data in order to try to find the 
	// zero terminator for the player's chat string
	for (int i = chatOffset; i < server->read_length && !chatTerminated; i++)
	{
		if (bytes[i] == 0x00)
		{
			chatTerminated = true;
		}

		else
		{
			chatLength++;
		}
	}

	if (!chatTerminated)
	{
		// the character's chat string has not been zero terminated; 
		// there is not enough data to determine packet size
		*size = -1;
		*ready = false;
		return true;
	}

	int playerOffset = chatOffset + chatLength + 1;
	bool playerTerminated = false;
	int playerLength = 0;

	// iterate through the packet data in order to try to find the
	// zero terminator for the player's player string
	for (int i = playerOffset; i < server->read_length && !playerTerminated; i++)
	{
		if (bytes[i] == 0x00)
		{
			playerTerminated = true;
		}

		else
		{
			playerLength++;
		}
	}

	if (!playerTerminated)
	{
		// the character's player string has not been zero terminated;
		// there is not enough data to determine packet size
		*size = -1;
		*ready = false;
		return true;
	}

	// calculate the size of the packet
	*size = playerOffset + playerLength + 1 + 1;
	*ready = true;
	return true;
}

bool GameProxy::GetWardenPacketSize(int* size, bool* ready) const
{
	int wardenSizeOffset = 1;

	if (server->read_length < wardenSizeOffset + static_cast<int>(sizeof(short)))
	{
		// not enough data has been received to be able to get the size
		// of the warden data; impossible to determine packet size
		*size = -1;
		*ready = false;
	}

	else
	{
		const unsigned char* bytes = reinterpret_cast<const unsigned char*>(server->read);
		int wardenSize = *reinterpret_cast<const short*>(bytes + wardenSizeOffset);

		// calculate the size of the packet from the header
		*size = wardenSizeOffset + sizeof(short) + wardenSize;
		*ready = true;
	}

	return true;
}

#endif //GAMEPROXY_H
