#include <iostream>
#include <string>
#include <ctime>
#include "server.h"
#include "AwesomO.h"
#include "Log.h"
#include "D2Map.h"
#include "d2data/BaseStat.h"
#include "d2data/item/BaseProperty.h"
#include "d2data/item/BaseItemType.h"
#include "d2data/item/BaseItem.h"
#include "d2data/item/ItemSet.h"
#include "d2data/item/SetItem.h"
#include "d2data/item/BaseRuneword.h"
#include "d2data/item/UniqueItem.h"
#include "Maps.h"

bool stealth;
int newServerIp;
bool mapL_FFS;

std::string select_realm()
{
	int result = -1;

	while (result < 1 || result > 4)
	{
		std::cout << "Select the realm..." << std::endl;
		std::cout << std::endl;
		std::cout << "1) U.S. West" << std::endl;
		std::cout << "2) U.S. East" << std::endl;
		std::cout << "3) Asia" << std::endl;
		std::cout << "4) Europe" << std::endl;
		std::cout << std::endl;
		std::cout << "awesom-o # ";
		std::cin >> result;
	}

	switch (result)
	{
		case 1: return "uswest.battle.net";
		case 2: return "useast.battle.net";
		case 3: return "asia.battle.net";
		case 4: return "europe.battle.net";
	}

	return "error";
}

class Test : public Thread
{
	Maps * maps;
	char * name;
	AwesomO * awesomo;

public:
	Test(char * name)
	{
		this->name = name;
		awesomo = new AwesomO(name);
	}

	virtual int Routine()
	{
		std::cout << "======================================================================================" <<  std::endl;
		std::cout << "=========================================start========================================" <<  std::endl;
		std::cout << "======================================================================================" <<  std::endl;

		Sleep(2000);

		unsigned char bActLevels[] = {1, 40, 75, 103, 109, 137};
		srand(time(NULL));

		//while (true)
		for (int q = 0; q < 200; q++)
		{
			try
			{
				maps = new Maps();
				maps->setAwesomO(awesomo);
				maps->setMapId(rand(), 0);
				maps->setLevel(AreaLevel::Harrogath);
				maps->loadMap(4, AreaLevel::Harrogath);

				maps->getMap(AreaLevel::RogueEncampment);
				maps->getMap(AreaLevel::LutGholein);
				maps->getMap(AreaLevel::KurastDocks);
				maps->getMap(AreaLevel::ThePandemoniumFortress);
				maps->getMap(AreaLevel::Harrogath);

/*
				for (int a = 0; a < 10 + (rand() % 10); a++)
				{
					int random = 1 + (rand() % 135);

					for (int i = 1; i < 6; i++)
					{
						if (random < bActLevels[i])
						{
							maps->loadMap(i, (AreaLevel::AreaLevel) (random + 1));
							//Sleep(5000 + rand() % 10000);


							//if (rand() % 2)
							//{
								maps->setLevel((AreaLevel::AreaLevel) (random + 1));
								POINT p1 = maps->pathToLevel((AreaLevel::AreaLevel) random);
								if (maps->IsValidAbsLocation((AreaLevel::AreaLevel) (random + 1), p1.x, p1.y))
								{
									maps->setPosition(p1.x, p1.y);
									POINT p2 = maps->pathToLevel((AreaLevel::AreaLevel) (random + 2));
									
									if (maps->IsValidAbsLocation((AreaLevel::AreaLevel) (random + 1), p2.x, p2.y))
									{
										std::vector<POINT> ptPath = maps->pathTo(p2.x, p2.y, false, 20);
										LOG(logDEBUG2, awesomo) << "Path size is " << ptPath.size();

										for (size_t i = 0; i < ptPath.size(); i++)
										{
											LOG(logDEBUG2, awesomo) << "ptPath[" << i << "] = (" << ptPath[i].x << ", " << ptPath[i].y << ")";
										}
									}
								}
							//}

							//Sleep(5000 + rand() % 10000);

							//Sleep(1000);

							break;
						}
					}
				}
			*/	
				maps->JoinAll();

				std::cout << "--------------------------------------------------------------------------------------" << std::endl;
				delete maps;
				std::cout << "--------------------------------------------------------------------------------------" << std::endl;
				std::cout << "--------------------------------------------------------------------------------------" << std::endl;
				std::cout << "--------------------------------------------------------------------------------------" << std::endl;

				Sleep(10000);
			}
			catch (char * msg)
			{
				std::cout << msg << std::endl;
				return false;
			}
			catch (...)
			{
				std::cout << "unknown error" << std::endl;
				return false;
			}
		}
		
		std::cout << "======================================================================================" <<  std::endl;
		std::cout << "=========================================done=========================================" <<  std::endl;
		std::cout << "======================================================================================" <<  std::endl;
		
		return true;
	}
};


std::string enter_folder()
{
	std::string folder = "C:\\Program Files\\Diablo II";
	std::string input;
	std::cout << "Enter your diablo folder... or press <return> to use the default ('" << folder << "')" << std::endl;
	std::cout << "awesom-o # ";

	getline(std::cin, input);

	if (input.empty())
	{
		return folder;
	}
	else
	{
		return input;
	}
}

int main(int argc, char *argv[])
{
	Log::of.open("logs\\core.log", std::ios::app);

	bool usage = false;
	std::string foldername = "";
	std::string ip = "127.0.0.1";
	std::string args = "";
	std::string socks_file = "config\\socks.txt";
	char realm_address[50];
	realm_address[0] = '\0';

	if (argc > 2)
	{
		for (int i = 1; i < argc; i++)
		{
			args = argv[i];

			if(args == "-d")
			{
				if(i+1 < argc)
				{
					foldername = argv[i+1];
					i++;
				}
				else
				{
					LOG(logERROR, NULL) << "Missing value for argument " << args;
				}
			}
			else if (args == "-r")
			{
				if (i+1 < argc)
				{
					strcpy_s(realm_address, sizeof(realm_address), argv[i+1]);
					/*
					std::string realm = argv[i+1];
					if (realm == "uswest.battle.net" || realm == "useast.battle.net" || realm == "asia.battle.net" || realm == "europe.battle.net")
					{
						strcpy_s(realm_address, sizeof(realm_address), argv[i+1]);
					}
					else
					{
						LOG(logERROR, NULL) << "Unknown realm: " << argv[i+1];
						strcpy_s(realm_address, sizeof(realm_address), select_realm().c_str());
					}
					*/
					i++;
				}
				else
				{
					LOG(logERROR, NULL) << "Missing value for argument " << args;
				}
			}
			else if (args == "-ip")
			{
				if (i+1 < argc)
				{
					ip = argv[i+1];
					i++;
				}
				else
				{
					LOG(logERROR, NULL) << "Missing value for argument " << args;
				}
			}
			else if (args == "-s" || args == "-stealth")
			{
				stealth = true;
			}
			else if (args == "-p")
			{
				if(i+1 < argc)
				{
					socks_file = argv[i+1];
					i++;
				}
				else
				{
					LOG(logERROR, NULL) << "Missing value for argument " << args;
				}
			}
			else
			{
				LOG(logERROR, NULL) << "Invalid argument : " << argv[i];
				if(!usage)
				{
					usage = true;
					LOG(logERROR, NULL) << "Usage is:";
					LOG(logERROR, NULL) << "-d <\"dia2dir\"> (where D2 is installed to)";
					LOG(logERROR, NULL) << "-r <realm address> (example useast.battle.net)";
					LOG(logERROR, NULL) << "-ip <proxy ip> (interface to listen on)";
					LOG(logERROR, NULL) << "-s / -stealth (stealth mode on)";
				}
			}
		}
	}

	if (foldername == "") foldername = enter_folder();
		init(foldername.c_str());

	if(realm_address[0] == '\0') strcpy_s(realm_address, sizeof(realm_address), select_realm().c_str());
		newServerIp = inet_addr(ip.c_str());	

	LOG(logDEBUG1, NULL) << "Foldername set to: \"" << foldername << "\"";
	LOG(logDEBUG1, NULL) << "Realm set to: \"" << realm_address << "\"";
	LOG(logDEBUG1, NULL) << "AO Proxy IP set to: \"" << newServerIp << "\" (" << ip << ")";
	LOG(logDEBUG1, NULL) << (stealth ? "Stealth: Enabled" : "Stealth: Disabled");

	BaseStat::init();
	BaseProperty::init();
	BaseItemType::init();
	BaseItem::init();
	ItemSet::init();
	BaseSetItem::init();
	BaseRuneword::init();
	BaseUniqueItem::init();

	init();

	socksManager = new SocksManager(socks_file.c_str());

	realm_host = gethostbyname(realm_address);

	if (!realm_host)
	{
		LOG(logERROR, NULL) << "cant get host name...";
		return 0;
	}
/*

				Maps * maps;
				AwesomO * awesomo;

				awesomo = new AwesomO("LuaRockFive");
				maps = new Maps();
				maps->setAwesomO(awesomo);
				maps->setMapId(742418175, 1);
				maps->setLevel(AreaLevel::RogueEncampment);
				
				POINT p1 = {119,94};
				POINT p2 = maps->getMap(AreaLevel::RogueEncampment)->pathToLevel(AreaLevel::BloodMoor);

//) to (152,199
				maps->getMap(AreaLevel::RogueEncampment)->RelativeToAbs(p1);


				std::vector<POINT> pts;
				pts.push_back(p1);
				pts.push_back(p2);

				maps->getMap(AreaLevel::RogueEncampment)->DumpMap("derek1.txt", pts);

				pts = maps->getMap(AreaLevel::RogueEncampment)->pathTo(p1.x, p1.y, p2.x, p2.y, false, 20);
				maps->getMap(AreaLevel::RogueEncampment)->DumpMap("derek2.txt", pts);
				std::cout << " ";

*/

				//maps->loadMap(0, AreaLevel::RogueEncampment);






	/*
	AwesomO * awesomo = new AwesomO("LuaRockFive");

	while (true)
	{
		std::cout << "create" << std::endl;
		awesomo->Initialize();
		Sleep(1000);

		awesomo->Abort();
		awesomo->Join();
		awesomo->maps.Join();
		awesomo->game = NULL;
		lua_close(awesomo->lua_pickit);
		lua_close(awesomo->lua_event);
		Sleep(1000);
	}
	*/

/*
	Test t1("test1");
	t1.Create(false);
	Sleep(1000);
	Test t2("test2");
	t2.Create(false);
	Sleep(1000);
	Test t3("test3");
	t3.Create(false);
	Sleep(1000);
	Test t4("test4");
	t4.Create(false);
	Sleep(1000);
	Test t5("test5");
	t5.Create(false);
	Sleep(1000);
	Test t6("test6");
	t6.Create(false);
	Sleep(1000);
	Test t7("test7");
	t7.Create(false);
	Sleep(1000);
	Test t8("test8");
	t8.Create(false);
	Sleep(1000);
	Test t9("test9");
	t9.Create(false);
	Sleep(1000);
	Test t10("test10");
	t10.Create(false);
	Sleep(1000);
	Test t11("test11");
	t11.Create(false);
	Sleep(1000);
	Test t12("test12");
	t12.Create(false);
*/
	while (true)
	{
		server_tasks();
		SwitchToThread();
	}

	server_clean();

	return 0;
}
