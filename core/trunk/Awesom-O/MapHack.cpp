#include "MapHack.h"
#include "AwesomO.h"

MapHack::~MapHack()
{
	if (awesomo)
	{
		LOG(logINFO, awesomo) << "Maphack for " << awesomo->owner << " disconnected";
		awesomo->maphack = NULL;
	}
}

void MapHack::Process()
{
	switch (read[0])
	{
		case 0x01:
		{
			if (read_length == 1) return;
			const char* owner = static_cast<const char*>(read + 1);

			for (size_t i = 0; i < ao.size(); i++)
			{
				if (strcmp(ao[i]->owner, owner) == 0)
				{
					awesomo = ao[i];
					break;
				}
			}

			if (awesomo)
			{
				if (awesomo->maphack)
				{
					LOG(logINFO, awesomo) << "Maphack for " << owner << " already connected";

					char responce[] = { MapHackEvent::ALREADY_CONNECTED };
					Write(responce, sizeof(responce));
					read_length = 0;
					return;
				}
				else
				{
					awesomo->maphack = this;
				}
			}
			else
			{
				awesomo = new AwesomO(owner);
				awesomo->maphack = this;
				ao.push_back(awesomo);
			}

			LOG(logINFO, awesomo) << "Maphack for " << owner << " connected";

			char responce[] = { MapHackEvent::CONNECTION_SUCCESSFUL };
			Write(responce, sizeof(responce));
		}
		break;
	}

	read_length = 0;
}
