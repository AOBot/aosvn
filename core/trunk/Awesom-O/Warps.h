#pragma once

#include "CriticalSection.h"
#include <list>
#include "d2data/UnitType.h"
#include "d2data/WarpType.h"

//tolua_begin
struct WARP
{
	UnitType::UnitType unitType;
	unsigned int uid;
	WarpType::WarpType id;
	short x;
	short y;
};
//tolua_end

class Warps { // tolua_export

private:
	CriticalSection cs;
	std::list<WARP> list;
	
public:
	Warps();
	~Warps();
	//tolua_begin
	void add(WARP warp);
	void remove(unsigned int uid);
	WARP find(WarpType::WarpType id);
	WARP find(short x, short y, int dist = 5);
};
// tolua_end
