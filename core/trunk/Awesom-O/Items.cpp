#include "Items.h"
#include <cmath>

Items::Items()
{
}

Items::~Items()
{
}

void Items::add(ITEM item) throw(...)
{
	cs.Lock();
	list.push_back(item);	
	cs.Unlock();
}

void Items::remove(unsigned int uid) throw(...)
{
	cs.Lock();

	for (std::list<ITEM>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter = list.erase(iter);
			break;
		}
	}

	cs.Unlock();
}

ITEM Items::findPot(PotionType::PotionType type, short x, short y) throw(...) // http://www.swig.org/Doc1.3/Lua.html#Lua_nn17
{
	ITEM item;
	item.uid = 0;
	cs.Lock();
	
	for (std::list<ITEM>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if ((GetPotType(iter->baseItem->code) == type) && 
			(int)::sqrt((double)((iter->x - x) * (iter->x - x) + (iter->y - y) * (iter->y - y))) < 20 &&
			(!item.uid || 
			(int)::sqrt((double)((iter->x - x) * (iter->x - x) + (iter->y - y) * (iter->y - y))) < 
			(int)::sqrt((double)((item.x - x) * (item.x - x) + (item.y - y) * (item.y - y)))))
		{
			item = *iter;
			break;
		}
	}

	cs.Unlock();
	return item;
}

std::vector<ITEM> Items::findItemInRadius(short x, short y, int radius)
{
	std::vector<ITEM> items;
	cs.Lock();

	for (std::list<ITEM>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->getDistance(x, y) < radius)
		{
			items.push_back(*iter);
		}
	}

	cs.Unlock();
	return items;
}

bool Items::isOnGround(unsigned int uid) throw(...) // http://www.swig.org/Doc1.3/Lua.html#Lua_nn17
{
	bool result = false;
	cs.Lock();
		
	for (std::list<ITEM>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			result = true;
			break;
		}
	}

	cs.Unlock();
	return result;
}

ITEM Items::findByCode(std::string code) throw(...) // http://www.swig.org/Doc1.3/Lua.html#Lua_nn17
{
	ITEM item;
	item.uid = 0;
	cs.Lock();
	
	for (std::list<ITEM>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->baseItem->code.compare(code) == 0)
		{
			item = *iter;
			break;
		}
	}

	cs.Unlock();
	return item;
}

std::vector<ITEM> Items::get() throw(...)
{
	std::vector<ITEM> itmlist;
	cs.Lock();

	itmlist.insert(itmlist.begin(), list.begin(), list.end());

	cs.Unlock();
	return itmlist;
}

PotionType::PotionType Items::GetPotType(std::string code) throw(...)
{
	if (code.find("mp1") == 0 || code.find("mp2") == 0 || code.find("mp3") == 0 || code.find("mp4") == 0 || code.find("mp5") == 0)
	{
		return PotionType::Blue;
	}
	if (code.find("hp1") == 0 || code.find("hp2") == 0 || code.find("hp3") == 0 || code.find("hp4") == 0 || code.find("hp5") == 0)
	{
		return PotionType::Red;
	}
	if (code.find("rvl") == 0 || code.find("rvs") == 0)
	{
		return PotionType::Purple;
	}
	else
	{
		return PotionType::Error;
	}
}
