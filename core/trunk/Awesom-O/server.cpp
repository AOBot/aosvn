#include "server.h"
//#include "Compression.h"

#include "Proxy.h"
#include "ChatProxy.h"
#include "RealmProxy.h"
#include "GameProxy.h"

#include "Controller.h"
#include "MapHack.h"

hostent * realm_host;

SOCKET chat;
SOCKET realm;
SOCKET game;
SOCKET controller;
SOCKET maphack;

SOCKET SetUpListener(int nPort)
{
    SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);

	if (sd != INVALID_SOCKET)
	{
        sockaddr_in sinInterface;
        sinInterface.sin_family = AF_INET;
        sinInterface.sin_addr.s_addr = htonl(INADDR_ANY);
        sinInterface.sin_port = htons(nPort);
        
		if (bind(sd, (sockaddr*)&sinInterface, sizeof(sockaddr_in)) != SOCKET_ERROR)
		{
            listen(sd, SOMAXCONN);
            return sd;
        }
        else
		{
			LOG(logINFO, NULL) << "bind() failed";
        }
    }

	return INVALID_SOCKET;
}

void SetupFDSets(fd_set& ReadFDs, fd_set& WriteFDs, fd_set& ExceptFDs)
{
    FD_ZERO(&ReadFDs);
    FD_ZERO(&WriteFDs);
    FD_ZERO(&ExceptFDs);

    // Add the listener socket to the read and except FD sets, if there is one.
    if (chat != INVALID_SOCKET)
	{
        FD_SET(chat, &ReadFDs);
        FD_SET(chat, &ExceptFDs);
    }
    // Add the listener socket to the read and except FD sets, if there is one.
    if (realm != INVALID_SOCKET)
	{
        FD_SET(realm, &ReadFDs);
        FD_SET(realm, &ExceptFDs);
    }
    // Add the listener socket to the read and except FD sets, if there is one.
    if (game != INVALID_SOCKET)
	{
        FD_SET(game, &ReadFDs);
        FD_SET(game, &ExceptFDs);
    }
    // Add the control socket to the read and except FD sets, if there is one.
    if (controller != INVALID_SOCKET)
	{
        FD_SET(controller, &ReadFDs);
        FD_SET(controller, &ExceptFDs);
    }
    // Add the maphack socket to the read and except FD sets, if there is one.
    if (maphack != INVALID_SOCKET)
	{
        FD_SET(maphack, &ReadFDs);
        FD_SET(maphack, &ExceptFDs);
    }

    // add connections
	std::set<Connection *>::iterator iter = Connection::connections.begin();

	while (iter != Connection::connections.end())
	{
		Connection * connection = *iter;

		if (connection->read_length < kBufferSize)
		{
			// There's space in the read buffer, so pay attention to
			// incoming data.
			FD_SET(connection->socket, &ReadFDs);
		}

        if (connection->write_length > 0)
		{
            // There's data still to be sent on this socket, so we need
            // to be signalled when it becomes writable.
			FD_SET(connection->socket, &WriteFDs);
        }

		FD_SET(connection->socket, &ExceptFDs);

        iter++;
	}
}

int init()
{
	WSADATA t_wsa; // WSADATA structure
	WORD wVers; // version number
	int iError; // error number

	wVers = MAKEWORD(2, 2); // Set the version number to 2.2
	iError = WSAStartup(wVers, &t_wsa); // Start the WSADATA


	if (iError != NO_ERROR)
	{
		WSACleanup();
		return 0;
	}

	if (LOBYTE(t_wsa.wVersion) != 2 || HIBYTE(t_wsa.wVersion) != 2)
	{
		WSACleanup();
		return 0;
	}

    chat = SetUpListener(6112);
	realm = SetUpListener(6113);
	game = SetUpListener(4000);

	controller = SetUpListener(2004);
	maphack = SetUpListener(2005);

	if (chat == INVALID_SOCKET || realm == INVALID_SOCKET || game == INVALID_SOCKET || controller == INVALID_SOCKET || maphack == INVALID_SOCKET)
	{
        return 3;
    }
	else
	{
		LOG(logINFO, NULL) << "listener established.";
	}
	
	return 0;
}

bool ShutdownConnection(SOCKET sd)
{
    // Disallow any further data sends.  This will tell the other side
    // that we want to go away now.  If we skip this step, we don't
    // shut the connection down nicely.
    if (shutdown(sd, SD_SEND) == SOCKET_ERROR)
	{
        return false;
    }

    // Receive any extra data still sitting on the socket.  After all
    // data is received, this call will block until the remote host
    // acknowledges the TCP control packet sent by the shutdown above.
    // Then we'll get a 0 back from recv, signalling that the remote
    // host has closed its side of the connection.
    char acReadBuffer[kBufferSize];

	while (1)
	{
        int nNewBytes = recv(sd, acReadBuffer, kBufferSize, 0);

		if (nNewBytes == SOCKET_ERROR)
		{
            return false;
        }
        else if (nNewBytes != 0)
		{
			LOG(logINFO, NULL) << "FYI, received " << nNewBytes << " unexpected bytes during shutdown.";
        }
        else
		{
            // Okay, we're done!
            break;
        }
    }

    // Close the socket.
    if (closesocket(sd) == SOCKET_ERROR)
	{
        return false;
    }

    return true;
}

void server_tasks()
{
    sockaddr_in sinRemote;
    int nAddrSize = sizeof(sinRemote);

	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

//    while (1)
//	{
        fd_set ReadFDs, WriteFDs, ExceptFDs;
        SetupFDSets(ReadFDs, WriteFDs, ExceptFDs);

		int result = select(0, &ReadFDs, &WriteFDs, &ExceptFDs, &timeout);

		// Something happened on one of the sockets.
		if (result > 0)
		{
			// was there an error?
			if (FD_ISSET(chat, &ExceptFDs) || FD_ISSET(realm, &ExceptFDs) || FD_ISSET(game, &ExceptFDs) || FD_ISSET(controller, &ExceptFDs) || FD_ISSET(maphack, &ExceptFDs))
			{
                int err;
                int errlen = sizeof(err);

				getsockopt(chat, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);
                LOG(logERROR, NULL) << "Exception on chat listening socket: " << err;

				getsockopt(realm, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);
                LOG(logERROR, NULL) << "Exception on realm listening socket: " << err;

				getsockopt(game, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);
                LOG(logERROR, NULL) << "Exception on game listening socket: " << err;

				getsockopt(controller, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);
                LOG(logERROR, NULL) << "Exception on controller listening socket: " << err;

				getsockopt(maphack, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);
                LOG(logERROR, NULL) << "Exception on maphack listening socket: " << err;

				return;
            }

			// was it the chat listener socket?
			if (FD_ISSET(chat, &ReadFDs))
			{
                SOCKET s = accept(chat, (sockaddr*)&sinRemote, &nAddrSize);

				if (s != INVALID_SOCKET)
				{
                    // tell user we accepted the socket, and add it to our connecition list
//					LOG(logINFO) << "accepted connection from " << inet_ntoa(sinRemote.sin_addr) << ":" << ntohs(sinRemote.sin_port) << ", socket " << s << ".";
                    pConnections.push_back(new ChatProxy(s));

                    // mark the socket as non-blocking, for safety
                    u_long nNoBlock = 1;
                    ioctlsocket(s, FIONBIO, &nNoBlock);
                }
                else
				{
                    LOG(logERROR, NULL) << "accept() failed";
                    return;
                }
            }

            // was it the realm listener socket?
            if (FD_ISSET(realm, &ReadFDs))
			{
                SOCKET s = accept(realm, (sockaddr*)&sinRemote, &nAddrSize);

				if (s != INVALID_SOCKET)
				{
                    // tell user we accepted the socket, and add it to our connecition list
//                    LOG(logINFO) << "accepted connection from " << inet_ntoa(sinRemote.sin_addr) << ":" << ntohs(sinRemote.sin_port) << ", socket " << s << ".";
                    pConnections.push_back(new RealmProxy(s));

                    // mark the socket as non-blocking, for safety
                    u_long nNoBlock = 1;
                    ioctlsocket(s, FIONBIO, &nNoBlock);
                }
                else
				{
                    LOG(logERROR, NULL) << "accept() failed";
                    return;
                }
            }

			// was it the game listener socket?
            if (FD_ISSET(game, &ReadFDs))
			{
                SOCKET s = accept(game, (sockaddr*)&sinRemote, &nAddrSize);

				if (s != INVALID_SOCKET)
				{
                    // tell user we accepted the socket, and add it to our connecition list
//                    LOG(logINFO) << "accepted connection from " << inet_ntoa(sinRemote.sin_addr) << ":" << ntohs(sinRemote.sin_port) << ", socket " << s << ".";


					char buffer[2] = {0xaf, 0x01};

					int nBytes = send(s, buffer, 2, 0);

					if (nBytes != 2)
					{
						LOG(logERROR, NULL) << "FUCK";
					}

					//SOCKET g = new_client(4000, ao[0]->oldGameServerAddress);

                    pConnections.push_back(new GameProxy(s));

					// mark the socket as non-blocking, for safety
                    u_long nNoBlock = 1;
                    ioctlsocket(s, FIONBIO, &nNoBlock);
                }
                else
				{
                    LOG(logERROR, NULL) << "accept() failed";
                    return;
                }
            }
			
			// was it the controller listener socket?
			if (FD_ISSET(controller, &ReadFDs))
			{
				SOCKET s = accept(controller, (sockaddr*)&sinRemote, &nAddrSize);

				if (s != INVALID_SOCKET)
				{
                    Controller * c = new Controller(s);

                    // mark the socket as non-blocking, for safety
                    u_long nNoBlock = 1;
                    ioctlsocket(s, FIONBIO, &nNoBlock);
					
                }
                else
				{
                    LOG(logERROR, NULL) << "accept() failed";
                    return;
                }
			}
			
			// was it the maphack listener socket?
			if (FD_ISSET(maphack, &ReadFDs))
			{
				SOCKET s = accept(maphack, (sockaddr*)&sinRemote, &nAddrSize);

				if (s != INVALID_SOCKET)
				{
                    MapHack * m = new MapHack(s);

                    // mark the socket as non-blocking, for safety
                    u_long nNoBlock = 1;
                    ioctlsocket(s, FIONBIO, &nNoBlock);
					
                }
                else
				{
                    LOG(logERROR, NULL) << "accept() failed";
                    return;
                }
			}
			









			std::set<Connection *>::iterator iter = Connection::connections.begin();

			while (iter != Connection::connections.end())
			{
				Connection * connection = *iter;


                int error = 0;
                const char* pcErrorType = 0;

                // see if this socket's flag is set in any of the FD sets.
				if (FD_ISSET(connection->socket, &ExceptFDs))
				{
                    error = 1;
                    pcErrorType = "General socket error";
                    FD_CLR(connection->socket, &ExceptFDs);
                }
                else
				{
                    if (FD_ISSET(connection->socket, &ReadFDs))
					{
						error = connection->ReadData();
						pcErrorType = "Read error";
						FD_CLR(connection->socket, &ReadFDs);
                    }

					if (FD_ISSET(connection->socket, &WriteFDs))
					{
                        error = connection->WriteData();
                        pcErrorType = "Write error";
                        FD_CLR(connection->socket, &WriteFDs);
                    }

					if (connection->read_length > 0)
					{
						connection->Process();
					}
				}

				// process any data
				ProxyVector::iterator i = pConnections.begin();

				while (i != pConnections.end())
				{
					Proxy * it = *i;

					if (it->client && it->client->read_length > 0)
					{
						error = it->OnRelayDataToClient();
					}
					if (it->server && it->server->read_length > 0)
					{
						error = it->OnRelayDataToServer();
					}

					i++;
				}

				if (error)
				{
                    // Something bad happened on the socket, or the
                    // client closed its half of the connection.  Shut
                    // the conn down and remove it from the list.
                    int err;
                    int errlen = sizeof(err);
					getsockopt(connection->socket, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);

					if (err != NO_ERROR)
					{
						 LOG(logINFO, NULL) << pcErrorType << err << std::endl;
					}

					for (ProxyVector::iterator it = pConnections.begin(); it != pConnections.end(); it++)
					{
						Proxy * p = *it;

						if (error < 0 && (p->client == connection || p->server == connection))
						{
							if (strcmp(pcErrorType, "Read error") == 0) //Cant catch this, someone fix? -- you can't just to == on string in c++
							{
								LOG(logDEBUG4, NULL) << "Socket Read Error!";
							}
							p->error();
						}

						if (p->client == connection)
						{
							pConnections.erase(it);
							if (p->server)
								delete p->server;
							delete p;
							break;
						}

						if (p->server == connection)
						{
							pConnections.erase(it);
							if (p->client)
								delete p->client;
							delete p;
							break;
						}
					}

					delete connection;

					iter = Connection::connections.begin();
                }
                else
				{
                    // Go on to next connection
                    ++iter;
                }
			}
/*




			// was it one of the proxy sockets?
            ProxyVector::iterator i = pConnections.begin();

			while (i != pConnections.end())
			{
				Proxy * it = *i;

                bool bOK = true;
                const char* pcErrorType = 0;

                // see if this socket's flag is set in any of the FD sets.
				if (it->server && FD_ISSET(it->server->socket, &ExceptFDs))
				{
                    bOK = false;
                    pcErrorType = "General socket error";
                    FD_CLR(it->server->socket, &ExceptFDs);
                }
                else if (it->client && FD_ISSET(it->client->socket, &ExceptFDs))
				{
                    bOK = false;
                    pcErrorType = "General socket error";
                    FD_CLR(it->client->socket, &ExceptFDs);
                }
                else
				{
                    if (it->server && FD_ISSET(it->server->socket, &ReadFDs))
					{
						//bOK = it->ReadData(it->server);
						pcErrorType = "Read error";
						FD_CLR(it->server->socket, &ReadFDs);
                    }

					if (it->server && FD_ISSET(it->server->socket, &WriteFDs))
					{
						if (it->server->read_length > 0)
						{
							it->OnRelayDataToServer();
						}

						if (it->server->write_length > 0)
						{
							//bOK = it->WriteData(it->server);
							pcErrorType = "Write error";
						}

						FD_CLR(it->server->socket, &WriteFDs);
                    }

					if (it->client && FD_ISSET(it->client->socket, &ReadFDs))
					{
						//bOK = it->ReadData(it->client);
						pcErrorType = "Read error";
						FD_CLR(it->client->socket, &ReadFDs);
                    }

					if (it->client && FD_ISSET(it->client->socket, &WriteFDs))
					{
						if (it->client->read_length > 0)
						{
							it->OnRelayDataToClient();
						}

						if (it->client->write_length > 0)
						{
							//bOK = it->WriteData(it->client);
							pcErrorType = "Write error";
						}

						FD_CLR(it->client->socket, &WriteFDs);
                    }
                }

                if (!bOK)
				{
					it->Disconnect();

                    // Something bad happened on the socket, or the
                    // client closed its half of the connection.  Shut
                    // the conn down and remove it from the list.
                    int err;
                    int errlen = sizeof(err);
					getsockopt(it->server->socket, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);

					if (err != NO_ERROR)
					{
						LOG(logINFO, NULL) << pcErrorType << err;
					}

					delete it;
//					ShutdownConnection(it->server->socket);
//					ShutdownConnection(it->client->socket);
//					pConnections.erase(i);
                    i = pConnections.begin();
                }
                else
				{
                    // Go on to next connection
                    ++i;
                }
			}

			// was it one of the controler sockets?
            ControllerVector::iterator c = cConnections.begin();

			while (c != cConnections.end())
			{
				Controller * it = *c;

                bool bOK = true;
                const char* pcErrorType = 0;

                // see if this socket's flag is set in any of the FD sets.
				if (FD_ISSET(it->socket, &ExceptFDs))
				{
                    bOK = false;
                    pcErrorType = "General socket error";
                    FD_CLR(it->socket, &ExceptFDs);
                }
                else
				{
                    if (FD_ISSET(it->socket, &ReadFDs))
					{
						bOK = it->ReadData();
						pcErrorType = "Read error";
						FD_CLR(it->socket, &ReadFDs);
                    }

					if (FD_ISSET(it->socket, &WriteFDs))
					{
                        bOK = it->WriteData();
                        pcErrorType = "Write error";
                        FD_CLR(it->socket, &WriteFDs);
                    }
				}

                if (!bOK)
				{
                    // Something bad happened on the socket, or the
                    // client closed its half of the connection.  Shut
                    // the conn down and remove it from the list.
                    int err;
                    int errlen = sizeof(err);
					getsockopt(it->socket, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);

					if (err != NO_ERROR)
					{
						LOG(logINFO, NULL) << pcErrorType << err;
					}

					ShutdownConnection(it->socket);
                    cConnections.erase(c);
                    c = cConnections.begin();
                }
                else
				{
                    // Go on to next connection
                    ++c;
                }
			}

			// was it one of the maphack sockets?
            MapHackVector::iterator m = mConnections.begin();

			while (m != mConnections.end())
			{
				MapHack * it = *m;

                bool bOK = true;
                const char* pcErrorType = 0;

                // see if this socket's flag is set in any of the FD sets.
				if (FD_ISSET(it->socket, &ExceptFDs))
				{
                    bOK = false;
                    pcErrorType = "General socket error";
                    FD_CLR(it->socket, &ExceptFDs);
                }
                else
				{
                    if (FD_ISSET(it->socket, &ReadFDs))
					{
						bOK = it->ReadData();
						pcErrorType = "Read error";
						FD_CLR(it->socket, &ReadFDs);
                    }

					if (FD_ISSET(it->socket, &WriteFDs))
					{
                        bOK = it->WriteData();
                        pcErrorType = "Write error";
                        FD_CLR(it->socket, &WriteFDs);
                    }
				}

                if (!bOK)
				{
                    // Something bad happened on the socket, or the
                    // client closed its half of the connection.  Shut
                    // the conn down and remove it from the list.
                    int err;
                    int errlen = sizeof(err);
					getsockopt(it->socket, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);

					if (err != NO_ERROR)
					{
						LOG(logINFO, NULL) << pcErrorType << err;
					}

					ShutdownConnection(it->socket);
                    mConnections.erase(m);
                    m = mConnections.begin();
                }
                else
				{
                    // Go on to next connection
                    ++m;
                }
			}
*/
		}
        else if (result == SOCKET_ERROR)
		{
            LOG(logERROR, NULL) << "select() failed";
			exit(1);
        }
    //}
}

int server_clean()
{
	// shut down connections
	for (size_t i = 0; i < pConnections.size(); i++)
	{
		closesocket(pConnections[i]->server->socket);
		closesocket(pConnections[i]->client->socket);
	}

	// shut down listeners
	closesocket(game);
	closesocket(realm);
	closesocket(chat);
	closesocket(controller);
	closesocket(maphack);

	// shut down winsock
	WSACleanup();
	return 0;
}