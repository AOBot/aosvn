#pragma once

#include "Item.h"
#include <windows.h>
#include <vector>
#include <map>

class Stash { // tolua_export

private:
	std::map<unsigned int, ITEM> stash;
	bool map[6][8];
	bool Check(ITEM * item, int x, int y);

public:
	bool expansion;

	//tolua_begin
	std::vector<ITEM> get();
	POINT findLocation(ITEM item);
	std::vector<ITEM> findByCode(std::string code);
	void add(ITEM item);
	void remove(ITEM item);
};
// tolua_end
