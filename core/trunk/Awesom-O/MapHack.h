#ifndef MAPHACK_H
#define MAPHACK_H

#include <iostream>
#include <winsock2.h>
#include <vector>

#include "AwesomO.h"

class AwesomO;



class MapHack : public Connection
{
public:
	virtual void Process();

	AwesomO * awesomo;

public:
	MapHack(SOCKET s) : Connection(s, 2000000)
	{
		awesomo = NULL;
	}
	~MapHack();

	void Write(const char * buffer, int length)
	{
		writing.Lock();
		int* len = reinterpret_cast<int*>(write + write_length);
		write_length += sizeof(int);
		*len = length;
		memcpy(write + write_length, buffer, length);
		write_length += length;
		writing.Unlock();
	}
};

namespace MapHackEvent
{
	enum MapHackEvent
	{
		CONNECTION_SUCCESSFUL = 0x01,
		ALREADY_CONNECTED = 0x02,
		MAP_DATA = 0x03,
		CHANGE_LEVEL = 0x04,
		SET_POSITION = 0x05,
		PATH = 0x06,
		EXITS = 0x07,
		NPCS = 0x08,
		OBJECTS = 0x09
	};
};

#endif //MAPHACK_H
