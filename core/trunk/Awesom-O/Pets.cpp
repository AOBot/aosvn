#include "Pets.h"

void Pets::addPet(PetType::PetType petType, unsigned int uid)
{
	PET pet;
	pet.petType = petType;
	pet.uid = uid;

	cs.Lock();
	pets.push_back(pet);
	cs.Unlock();
}

void Pets::removePet(unsigned int uid)
{
	cs.Lock();
	for (std::vector<PET>::iterator iter = pets.begin(); iter != pets.end(); iter++)
	{
		if (iter->uid == uid)
		{
			pets.erase(iter);
			break;
		}
	}
	cs.Unlock();
}

PET Pets::find(unsigned int uid)
{
	PET pet;
	pet.uid = 0;
	cs.Lock();
	for (std::vector<PET>::iterator iter = pets.begin(); iter != pets.end(); iter++)
	{
		if (iter->uid == uid)
		{
			pet = *iter;
		}
	}
	cs.Unlock();
	return pet;
}



std::vector<PET> Pets::getPets()
{
	cs.Lock();
	std::vector<PET> results(pets);
	cs.Unlock();

	return results;
}

std::vector<PET> Pets::getPets(PetType::PetType petType)
{
	std::vector<PET> results;

	cs.Lock();
	for (std::vector<PET>::iterator iter = pets.begin(); iter != pets.end(); iter++)
	{
		if (iter->petType == petType)
		{
			results.push_back(*iter);
		}
	}
	cs.Unlock();

	return results;
}
