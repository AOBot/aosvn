#pragma once

#include "CriticalSection.h"
#include <list>

struct Command
{
	std::string file;
	std::string function;
	bool lockable;

	Command(std::string file, std::string function, bool lockable)
	{
		this->file = file;
		this->function = function;
		this->lockable = lockable;
	}
};

class Commands { // tolua_export

private:
	CriticalSection cs;
	std::list<Command> list;
	bool locked;

public:
	Commands();

	void push_front(Command command);
	void push_back(Command command);
	Command pop_front();
	int size();
	bool isLocked();

	//tolua_begin
	void lock();
	void unlock();
};

// tolua_end
