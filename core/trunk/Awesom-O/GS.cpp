#include "AwesomO.h"

#pragma region Packet 0x00 - Game Loading
/// <summary>
/// Game Server Packet 0x00 - Game Loading
/// <para>Sent during game logon sequence to let client know the server is processing the request.</para>
/// <para>The join can still fail after this...</para>
/// </summary>
/// <remarks>
/// Part of logon sequence
/// <para>Previous packet: <see cref="GameServer.GameLogonReceipt"/></para>
/// <para>Next packet: <see cref="GameServer.GameLogonSuccess"/></para>
/// </remarks>
void AwesomO::GameLoading() throw(...)
{
	unsigned char buffer[1] = { 0x00 };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x02 - Game Logon Success
/// <summary>
/// Game Server Packet 0x02 - Game Logon Success
/// <para>Last packet of the logon sequence sent only if the game logon is successful.</para>
/// <para>If this packet is not received the connexion will be dropped.</para>
/// </summary>
/// <remarks>
/// Part of logon sequence
/// <para>Previous packet: <see cref="GameServer.GameLoading"/></para>
/// <para>Response: <see cref="GameClient.EnterGame"/></para>
/// </remarks>
void AwesomO::GameLogonSuccess() throw(...)
{
	unsigned char buffer[1] = { 0x02 };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x04 - Act Loaded
/// <summary>
/// Game Server Packet 0x04 - Act Loaded
/// <para>Loading screen lights up...</para>
/// </summary>
void AwesomO::LoadDone() throw(...)
{
	unsigned char buffer[1] = { 0x04 };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x05 - Unload Done
/// <summary>
/// Game Server Packet 0x05 - Unload Done
/// <para>Sent before 0x03 on act change or after 0xB0 on game quit (but not if GS connection is interrupted.)</para>
/// </summary>
void AwesomO::UnloadDone() throw(...)
{
	unsigned char buffer[1] = { 0x05 };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x06 - Game Logout Success
/// <summary>
/// Game Server Packet 0x06 - Game Logout Success
/// <para>Sent on game quit if the connection with Battle.net is not dropped.</para>
/// </summary>
void AwesomO::GameLogoutSuccess() throw(...)
{
	unsigned char buffer[1] = { 0x06 };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x07 - Map Add
/// <summary>
/// Game Server Packet 0x07 - Map Add
/// <para>Marks a room as active.</para>
/// </summary>
void AwesomO::MapAdd(AreaLevel::AreaLevel area, unsigned int x, unsigned int y) throw(...)
{
	unsigned char buffer[6] = { 0x07,
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8),
		(unsigned char)area
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x08 - Map Remove
/// <summary>
/// Game Server Packet 0x08 - Map Remove
/// <para>Marks a room as inactive.</para>
/// </summary>
void AwesomO::MapRemove(AreaLevel::AreaLevel area, unsigned int x, unsigned int y) throw(...)
{
	unsigned char buffer[6] = { 0x08,
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8),
		(unsigned char)area
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0A - Remove Ground Unit
/// <summary>
/// Game Server Packet 0x0A - Remove Ground Unit
/// <para>Usually because it's no longer in proximity or was picked up.</para>
/// </summary>
void AwesomO::RemoveGroundUnit(UnitType::UnitType type, unsigned int uid) throw(...)
{
	unsigned char buffer[6] = { 0x0A,
		(unsigned char)(type),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0B - Game Handshake
/// <summary>
/// Game Server Packet 0x0B - Game Handshake
/// <para>Sent as part of game join data... UID will always be that of the receiving player.</para>
/// </summary>
void AwesomO::GameHandshake(UnitType::UnitType type, unsigned int uid) throw(...)
{
	unsigned char buffer[6] = { 0x0B,
		(unsigned char)(type),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0C - NPC Get Hit
/// <summary>
/// Game Server Packet 0x0C - NPC Get Hit
/// <para>An NPC takes damage within your range of view.</para>
/// </summary>
void AwesomO::NPCGetHit(UnitType::UnitType type, unsigned int uid, unsigned char life, unsigned int anim) throw(...)
{
	unsigned char buffer[9] = { 0x0C,
		(unsigned char)(type),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(anim), (unsigned char)(anim >> 8),
		life
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0F - Player Move
/// <summary>
/// Game Server Packet 0x0F - Player Move
/// <para>Another player is moving to a new location in your range of view.</para>
/// </summary>
void AwesomO::PlayerMove(UnitType::UnitType type, unsigned int uid, MovementType::MovementType movement, unsigned int targetX, unsigned int targetY, unsigned char unknown12, unsigned int currentX, unsigned int currentY) throw(...)
{
	unsigned char buffer[16] = { 0x0F,
		(unsigned char)(type),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(movement),
		(unsigned char)(targetX), (unsigned char)(targetX >> 8),
		(unsigned char)(targetY), (unsigned char)(targetY >> 8),
		unknown12,
		(unsigned char)(currentX), (unsigned char)(currentX >> 8),
		(unsigned char)(currentY), (unsigned char)(currentY >> 8)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x11 - Report Kill
/// <summary>
/// Game Server Packet 0x11 - Report Kill
/// <para>A player or his pet has killed something.</para>
/// <para>TEST: Only applies to collateral damage with multi shot attack (like chain lightning etc) ??
/// Seems it applies to Me kills too and UnitType is killer type not victim ??
/// Does it apply only to monster kills or PVP / merc death / etc ??</para>
/// </summary>
void AwesomO::ReportKill(UnitType::UnitType type, unsigned int uid) throw(...)
{
	unsigned char buffer[6] = { 0x11,
		(unsigned char)(type),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x15 - Player Reassign
/// <summary>
/// Game Server Packet 0x15 - Player Reassign
/// <para>Assigns a player or his pets at a location.</para>
/// <para>Sent when out of sync, joining game, teleporting or using a warp, portal or waypoint.</para>
/// </summary>
void AwesomO::PlayerReassign(UnitType::UnitType type, unsigned int uid, unsigned int x, unsigned int y, bool reassign) throw(...)
{
	unsigned char buffer[11] = { 0x15,
		(unsigned char)(type),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(x), (unsigned char)(y >> 8), 
		(unsigned char)(y), (unsigned char)(y >> 8), 
		(unsigned char)(reassign ? 1 : 0)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x19 - Small Gold Add
/// <summary>
/// Game Server Packet 0x19 - Small Gold Add
/// <para>Notifies you that gold has been picked up or transferred from bank to inventory.</para>
/// <para>If amount is larger than 254, a <see cref="GameServer.AttributeNotification"/> of type Gold packet will be sent instead.</para>
/// </summary>
void AwesomO::SmallGoldAdd(unsigned char amount) throw(...)
{
	unsigned char buffer[2] = { 0x19, amount };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x1D - Attribute Byte
/// <summary>
/// Game Server Packet 0x1D - Attribute Byte
/// <para>Notifies you of base attributes with a rating no higher than a single byte.</para>
/// </summary>
void AwesomO::AttributeByte(StatType::StatType stat, unsigned char value) throw(...)
{
	unsigned char buffer[3] = { 0x1D, (unsigned char)stat, value };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x1E - Attribute Word
/// <summary>
/// Game Server Packet 0x1E - Attribute Word
/// <para>Notifies you of base attributes with a rating.</para>
/// </summary>
void AwesomO::AttributeWord(StatType::StatType stat, unsigned short value) throw(...)
{
	unsigned char buffer[4] = { 0x1E, (unsigned char)stat, (unsigned char)value, (unsigned char)(value >> 8) };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x1F - Attribute DWord
/// <summary>
/// Game Server Packet 0x1F - Attribute DWord
/// <para>Notifies you of base attributes with a rating.</para>
/// </summary>
void AwesomO::AttributeDWord(StatType::StatType stat, unsigned long value) throw(...)
{
	unsigned char buffer[6] = { 0x1F, (unsigned char)stat, (unsigned char) value, (unsigned char) (value >> 8), (unsigned char) (value >> 16), (unsigned char) (value >> 24) };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x26 - Game Chat
/// <summary>
/// Game Server Packet 0x26 - Game Chat
/// <para>Game message or whisper coming from in game player or player / shrine overhead message.</para>
/// <para>If a shrine displays a overhead message, the message will be a 4 character number specifying the message.</para>
/// </summary>
void AwesomO::GameMessage(GameMessageType::GameMessageType type, unsigned char charFlags, std::string charName, std::string message) throw(...)
{
    if ((charName.length() != 0) && (message.length() != 0))
	{
		unsigned char* buffer = new unsigned char[12 + charName.length() + message.length()];
		int offset = 0;

		buffer[offset++] = 0x26;
		buffer[offset++] = (unsigned char)type;
		buffer[offset++] = 0x02;
		buffer[offset++] = charFlags;

		for (unsigned int i = 0; i < charName.length(); i++)
			buffer[offset++] = (unsigned char)charName[i];

		buffer[offset++] = 0x00;

		for (unsigned int i = 0; i < message.length(); i++)
			buffer[offset++] = (unsigned char)message[i];

		buffer[offset++] = 0x00;
		buffer[offset++] = 0x00;

		game->WriteServer(buffer, sizeof(offset));

		delete[] buffer;
	}
}

void AwesomO::GameMessage(UnitType::UnitType type, unsigned int uid, unsigned short random, std::string message) throw(...)
{
    if (message.length() != 0)
	{
		unsigned char* buffer = new unsigned char[12 + message.length()];
		int offset = 0;

		buffer[offset++] = 0x26;
		buffer[offset++] = (byte)GameMessageType::OverheadMessage;
		buffer[offset++] = (byte)type;
		buffer[offset++] = (byte)(uid);
		buffer[offset++] = (byte)(uid >> 8);
		buffer[offset++] = (byte)(uid >> 16);
		buffer[offset++] = (byte)(uid >> 24);
		buffer[offset++] = (byte)(random);
		buffer[offset++] = (byte)(random >> 8);

		for (unsigned int i = 0; i < message.length(); i++)
			buffer[offset++] = (byte)message[i];

		game->WriteServer(buffer, sizeof(offset));

		delete[] buffer;
	}
}
#pragma endregion
#pragma region Packet 0x2C - Play Sound
/// <summary>
/// Game Server Packet 0x2C - Play Sound
/// </summary>
void AwesomO::PlaySound(UnitType::UnitType unitType, unsigned int uid, GameSound::GameSound sound) throw(...)
{
	unsigned char buffer[8] = { 0x2c, 
		(unsigned char)unitType, 
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(sound), (unsigned char)((unsigned short)sound >> 8)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x42 - Player Clear Cursor
/// <summary>
/// Game Server Packet 0x42 - Player Clear Cursor
/// <para>Player's cursor is emptied (e.g. when selling an item or give it to merc, 
/// but not when dropped to ground or container...)</para>
/// <para>Only sent for the receiving player for that purpose, but may have other uses too !?</para>
/// </summary>
void AwesomO::PlayerClearCursor(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[6] = { 0x42, 
		(unsigned char)unitType,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x47 - Relator 1
/// <summary>
/// Game Server Packet 0x47 - Relator 1
/// <para>This is sent in relation to another packet, usually to provide the related unit's UID.</para>
/// <para>It's (almost?) always seen along with it's twin, <see cref="GameServer.Relator2"/>.</para>
/// </summary>
void AwesomO::Relator1(unsigned int uid, unsigned short param1, unsigned int param2) throw(...)
{
	unsigned char buffer[11] = { 0x47, 
		(unsigned char)(param1), (unsigned char)(param1 >> 8),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(param2), (unsigned char)(param2 >> 8), (unsigned char)(param2 >> 16), (unsigned char)(param2 >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x48 - Relator 2
/// <summary>
/// Game Server Packet 0x48 - Relator 2
/// <para>This is sent in relation to another packet, usually to provide the related unit's UID.</para>
/// <para>It's (almost?) always seen along with it's twin, <see cref="GameServer.Relator1"/>.</para>
/// </summary>
void AwesomO::Relator2(unsigned int uid, unsigned short param1, unsigned int param2) throw(...)
{
	unsigned char buffer[11] = { 0x48, 
		(unsigned char)(param1), (unsigned char)(param1 >> 8),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(param2), (unsigned char)(param2 >> 8), (unsigned char)(param2 >> 16), (unsigned char)(param2 >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x4F - Merc For Hire List Start
/// <summary>
/// Game Server Packet 0x4F - Merc For Hire List Start
/// <para>Sent before the 0x4E packets when interacting with a slaver town folk.</para>
/// </summary>
void AwesomO::MercForHireListStart() throw(...)
{
	unsigned char buffer[1] = { 0x4F };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x5B - Player In Game
/// <summary>
/// Game Server Packet 0x5B - Player In Game
/// <para>Sent for each player in game (including you) on join.</para>
/// <para>It is also sent for every player that joins afterwards, followed by a 0x5A packet of type JoinedGame.</para>
/// </summary>
void AwesomO::PlayerInGame(unsigned int uid, CharacterClass::CharacterClass charClass, std::string name, unsigned short level, unsigned short partyID) throw(...)
{
	if ((name.length() > 0) && (name.length() <= 16))
	{
		unsigned char buffer[34];

        buffer[0] = 0x5B;
        buffer[1] = 34;
        buffer[3] = (byte)(uid);
        buffer[4] = (byte)(uid >> 8);
        buffer[5] = (byte)(uid >> 16);
        buffer[6] = (byte)(uid >> 24);
        buffer[24] = (byte)(level);
        buffer[25] = (byte)(level >> 8);
        buffer[26] = (byte)(partyID);
        buffer[27] = (byte)(partyID >> 8);

		for (unsigned int i = 0; i < name.length(); i++)
            buffer[8 + i] = (byte)name[i];

		game->WriteServer(buffer, sizeof(buffer));
	}
}
#pragma endregion
#pragma region Packet 0x5C - Player Leave Game
/// <summary>
/// Game Server Packet 0x5C - Player Leave Game
/// <para>Sent when another player leaves the game.</para>
/// </summary>
void AwesomO::PlayerLeaveGame(unsigned int uid) throw(...)
{
	unsigned char buffer[5] = { 0x5c,
		(byte)(uid), (byte)(uid >> 8), (byte)(uid >> 16), (byte)(uid >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x74 - Player Corpse Visibility
/// <summary>
/// Game Server Packet 0x74 - Player Corpse Visibility
/// <para>Assigns a corpse UID to a slain player or remove a picked up corpse.</para>
/// </summary>
void AwesomO::PlayerCorpseVisible(bool assign, unsigned int playerUID, unsigned int corpseUID) throw(...)
{
	unsigned char buffer[10] = { 0x74,
		(byte)(assign ? 1 : 0),
		(byte)(playerUID), (byte)(playerUID >> 8), (byte)(playerUID >> 16), (byte)(playerUID >> 24),
		(byte)(corpseUID), (byte)(corpseUID >> 8), (byte)(corpseUID >> 16), (byte)(corpseUID >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x76 - Player In Sight
/// <summary>
/// Game Server Packet 0x76 - Player In Sight
/// <para>Can be used to grab other players' UID when they come in your sight.</para>
/// </summary>
void AwesomO::PlayerInSight(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[6] = { 0x76,
		(byte)unitType,
		(byte)(uid), (byte)(uid >> 8), (byte)(uid >> 16), (byte)(uid >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x77 - Update Item UI
/// <summary>
/// Game Server Packet 0x77 - Update Item UI
/// <para>Notifies of item related UI changes (mostly trade related but also stash and cube).</para>
/// </summary>
void AwesomO::UpdateItemUI(ItemUIAction::ItemUIAction action) throw(...)
{
	unsigned char buffer[2] = { 0x77, (byte)action };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x78 - Accept Trade
/// <summary>
/// Game Server Packet 0x78 - Accept Trade
/// <para>Notifies you of who you are trading with after you have accepted a trade request.</para>
/// </summary>
void AwesomO::AcceptTrade(std::string name, unsigned int uid) throw(...)
{
    if ((name.length() > 0) && (name.length() <= 16))
	{
		unsigned char buffer[21];

		buffer[0] = 0x78;
		buffer[17] = (byte)(uid);
		buffer[18] = (byte)(uid >> 8);
		buffer[19] = (byte)(uid >> 16);
		buffer[20] = (byte)(uid >> 24);

		for (unsigned int i = 0; i < name.length(); i++)
			buffer[1 + i] = (byte)name[i];

		game->WriteServer(buffer, sizeof(buffer));
	}
}
#pragma endregion
#pragma region Packet 0x7B - Assign Skill Hotkey
/// <summary>
/// Game Server Packet 0x7B - Assign Skill Hotkey
/// <para>Sent on game join to assign each skill to a hotkey slot.</para>
/// </summary>
void AwesomO::AssignSkillHotkey(unsigned char slot, SkillType::SkillType skill, unsigned int itemUID) throw(...)
{
	unsigned char buffer[8] = { 0x7B,
		slot,
		(unsigned char)(skill), (unsigned char)((unsigned short)skill >> 8),
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x7C - Use Special Item
/// <summary>
/// Game Server Packet 0x7C - Use Special Item
/// <para>Only known when identify / portal scroll / tome is used.</para>
/// <para>This is sent twice, with an UpdateContainerItem in between...</para>
/// </summary>
void AwesomO::UseSpecialItem(SpecialItemType::SpecialItemType action, unsigned int uid) throw(...)
{
	unsigned char buffer[6] = { 0x7C,
		(byte)action,
		(byte)(uid), (byte)(uid >> 8), (byte)(uid >> 16), (byte)(uid >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x7D - Set Item State
/// <summary>
/// Game Server Packet 0x7D - Set Item State
/// <para>Only known when an item breaks or a broken item is repaired...</para>
/// </summary>
void AwesomO::SetItemState(UnitType::UnitType ownerType, unsigned int ownerUID, unsigned int itemUID, ItemStateType::ItemStateType state) throw(...)
{
	unsigned char buffer[18] = { 0x7D,
		(byte)ownerType,
		(byte)(ownerUID), (byte)(ownerUID >> 8), (byte)(ownerUID >> 16), (byte)(ownerUID >> 24),
		(byte)(itemUID), (byte)(itemUID >> 8), (byte)(itemUID >> 16), (byte)(itemUID >> 24),
		0,
		(byte)(state), (byte)((unsigned int)state >> 8), (byte)((unsigned int)state >> 16), (byte)((unsigned int)state >> 24),
		(byte)(state), (byte)((unsigned short)state >> 8),
		0
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x8D - Assign Player To Party
/// <summary>
/// Game Server Packet 0x8D - Assign Player To Party
/// <para>Notifies you that a player is now in a party.</para>
/// </summary>
void AwesomO::AssignPlayerToParty(unsigned int uid, unsigned short partyNumber) throw(...)
{
	unsigned char buffer[7] = { 0x8D,
		(byte)(uid), (byte)(uid >> 8), (byte)(uid >> 16), (byte)(uid >> 24),
		(byte)(partyNumber), (byte)(partyNumber >> 8)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x8E - Assign Player Corpse
/// <summary>
/// Game Server Packet 0x8E - Assign Player Corpse
/// <para>Assigns a corpse UID to a slain player or remove a picked up corpse.</para>
/// </summary>
void AwesomO::AssignPlayerCorpse(bool assign, unsigned int playerUID, unsigned int corpseUID) throw(...)
{
	unsigned char buffer[10] = { 0x8E,
		(byte)(assign ? 1 : 0),
		(byte)(playerUID), (byte)(playerUID >> 8), (byte)(playerUID >> 16), (byte)(playerUID >> 24),
		(byte)(corpseUID), (byte)(corpseUID >> 8), (byte)(corpseUID >> 16), (byte)(corpseUID >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x8F - Pong
/// <summary>
/// Game Server Packet 0x8F - Pong
/// </summary>
void AwesomO::Pong() throw(...)
{
	unsigned char buffer[1] = { 0x8F };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x90 - Party Member Pulse
/// <summary>
/// Game Server Packet 0x90 - Party Member Pulse
/// <para>You should receive this packet every second for each member of your party not in range of view.</para>
/// </summary>
void AwesomO::PartyMemberPulse(unsigned int uid, unsigned int x, unsigned int y) throw(...)
{
	unsigned char buffer[13] = { 0x90,
		(byte)(uid), (byte)(uid >> 8), (byte)(uid >> 16), (byte)(uid >> 24),
		(byte)(x), (byte)(x >> 8), (byte)(x >> 16), (byte)(x >> 24),
		(byte)(y), (byte)(y >> 8), (byte)(y >> 16), (byte)(y >> 24)
	};

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x97 - Switch Weapon Set
/// <summary>
/// Game Server Packet 0x97 - Switch Weapon Set
/// <para>Sent when switching weapons to toogle the active weapon set.</para>
/// <para>Also sent when joining game with alternate weapons as active set.</para>
/// </summary>
void AwesomO::SwitchWeaponSet() throw(...)
{
	unsigned char buffer[1] = { 0x97 };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0xB0 - Game Over
/// <summary>
/// Game Server Packet 0xB0 - Game Over
/// <para>Notifies client of server's intention to drop the connection.</para>
/// <para>Sent in reply to GameClient.ExitGame, normally followed by UnloadDone and GameLogoutSuccess.</para>
/// </summary>
void AwesomO::GameOver() throw(...)
{
	unsigned char buffer[1] = { 0xB0 };

	game->WriteServer(buffer, sizeof(buffer));
}
#pragma endregion
//Rider@Redvex.net
#pragma region Packet 0x4c
/// <summary>
/// Game Server Packet 0x4c
/// <para>Makes player attacks visible to client </para>
/// <para></para>
/// </summary>
void AwesomO::LoadSkillOnUnit(SkillType::SkillType skill, UnitType::UnitType unitType, unsigned int meUID, unsigned int uid) throw(...)
{
	unsigned char buffer[16] = { 
		0x4c,
		(unsigned char)(unitType),
		(unsigned char)(meUID), 
		(unsigned char)(meUID >> 8), 
		(unsigned char)(meUID>> 16), 
		(unsigned char)(meUID >> 24),
		(unsigned char)(skill),      // skill
		0x00,
		0x2a,
		0x01, 
		(unsigned char)(uid), 
		(unsigned char)(uid >> 8), 
		(unsigned char)(uid >> 16), 
		(unsigned char)(uid >> 24),
		0x00,
		0x00
	};

	game->WriteServer(buffer, sizeof(buffer));
}

void AwesomO::LoadSkill(SkillType::SkillType skill, unsigned int meUID, unsigned int uid) throw(...)
{
	if (skill == SkillType::Teleport)
		return; 

	unsigned char buffer[16] = { 
		0x4c,
		0x00,
		(unsigned char)(meUID), 
		(unsigned char)(meUID >> 8), 
		(unsigned char)(meUID>> 16), 
		(unsigned char)(meUID >> 24),
		(unsigned char)(skill),      // skill
		0x00,
		0x1b,
		0x00, 
		(unsigned char)(uid), 
		(unsigned char)(uid >> 8), 
		(unsigned char)(uid >> 16), 
		(unsigned char)(uid >> 24),
		0x00,
		0x00
	};

	game->WriteServer(buffer, sizeof(buffer));
}

#pragma endregion
