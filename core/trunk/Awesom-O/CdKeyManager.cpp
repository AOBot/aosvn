#include <ctime>
#include <cctype>
#include "CdKeyManager.h"
#include "CdKey.h"

std::list<CdKeyManager::KeyEntry*> CdKeyManager::_keys;

CdKeyManager::CdKeyManager(const char* filename)
{
	srand(static_cast<unsigned int>(time(0)));
	LoadKeys(filename);
}

CdKeyManager::~CdKeyManager()
{
	ClearKeys();
}

const CdKey* CdKeyManager::AcquireKey(int productId)
{
	KeyEntry* entry = NULL;

	if (_keys.size() > 0)
	{
		for (std::list<KeyEntry*>::iterator iter = _keys.begin(); iter != _keys.end(); iter++)
		{
			if ((*iter)->key->GetProductId() == productId && !(*iter)->acquired)
			{
				if ((entry == NULL) || (entry->count > (*iter)->count))
				{
					entry = *iter;
				}
			}
		}
	}

	if (entry != NULL)
	{
		entry->acquired = true;
		entry->count += 1;
		return entry->key;
	}
	else
	{
		return NULL;
	}
}

bool CdKeyManager::ReleaseKey(const CdKey* key)
{
	for (std::list<KeyEntry*>::iterator i = _keys.begin(); i != _keys.end(); i++)
	{
		KeyEntry* entry = *i;

		if (entry->key == key)
		{
			entry->acquired = false;
			return true;
		}
	}

	return false;
}
void CdKeyManager::ReleaseKeys()
{
	for (std::list<KeyEntry*>::iterator i = _keys.begin(); i != _keys.end(); i++)
	{
		KeyEntry* entry = *i;
		entry->acquired = false;
	}
}

bool CdKeyManager::AddKey(const char* keyText)
{
	CdKey* key = new CdKey(keyText);

	if (key->IsValid())
	{
		KeyEntry* entry = new KeyEntry();

		entry->key = key;
		entry->acquired = false;
		entry->count = 0;

		_keys.push_back(entry);
		return true;
	}

	delete key;
	return false;
}

void CdKeyManager::ClearKeys()
{
	for (std::list<KeyEntry*>::iterator i = _keys.begin(); i != _keys.end(); i++)
	{
		KeyEntry* entry = *i;
		delete entry->key;
		delete entry;
	}

	_keys.clear();
}

int CdKeyManager::LoadKeys(const char* filename)
{
	int count = 0;
	FILE* fp = 0;
	
	if (fopen_s(&fp, filename, "r") == 0)
	{
		char key[17];
		char c = fgetc(fp);
		int offset = 0;

		while (c != EOF)
		{
			if (isalnum(c))
			{
				key[offset++] = c;
				key[offset] = '\0';
			}

			if (offset == 16)
			{
				if (AddKey(key))
				{
					count++;
				}

				offset = 0;
			}

			if (c == 0x0a || c == 0x0d)
			{
				offset = 0;
			}
						
			c = fgetc(fp);
		}

		fclose(fp);
	}

	return count;
}