#include "MonsterInfo.h"
#include <tchar.h>
#include <string>
#include <fstream>
#include "Log.h"

// Special thx to anyway1234 for the AC parser

const char *umodNames[] = 
{
	"none",
	"rndname",
	"hpmultiply",
	"light enchanted",
	"leveladd",
	"extra strong",
	"extra fast",
	"cursed",
	"resist",
	"fire enchanted",
	"poisondead",
	"durieldead",
	"bloodraven",
	"rage",
	"spcdamage",
	"partydead",
	"champion",
	"lightning enchanted",
	"cold enchanted",
	"hireable",
	"scarab",
	"killself",
	"questcomplete",
	"poisonhit",
	"thief",
	"manahit",
	"teleport",
	"spectralhit",
	"stoneskin",
	"multishot",
	"aura",
	"goboom",
	"firespike_explode",
	"suicideminion_explode",
	"ai_after_death",
	"shatter_on_death",
	"ghostly",
	"fanatic",
	"possessed",
	"berserk",
	"worms_on_death",
	"always_run_ai",
	"lightningdeath"
};

MonsterInfo::MonsterInfo()
{
	_init = false;
}

MonsterInfo::~MonsterInfo(){
	resistData_.clear();
}

void MonsterInfo::Init()
{
	if(!_init)
	{
		_init = true;

		loadResistInfo();
		loadClassIdNameFile();
		loadGfxTypeFile();
		loadSuperUniqueFile();
		loadItemStatCostFile();
	}
}

bool MonsterInfo::getMonsterResist(WORD npcid, ResistInfo *resInfo){
	MonsterInfo::Init();
	if (resistData_.find(npcid) != resistData_.end()){
		*resInfo = resistData_[npcid];
	}
	return false;
}

bool MonsterInfo::getNameByClassId(WORD npcid, char *name){
	MonsterInfo::Init();
	if (!name)
		return false;
	if (classIdName_.find(npcid) != classIdName_.end()){
		strcpy(name, classIdName_[npcid].c_str());
	}
	return false;
}
bool MonsterInfo::getGfxTypeInfo(WORD npcid, BYTE gfxTypeInfo[16]){
	MonsterInfo::Init();
	if (npcGfxType_.find(npcid) != npcGfxType_.end()){
		memcpy(gfxTypeInfo, (const void *) npcGfxType_[npcid].gfxTypeItemCount, NPC_GFX_TYPE_NUM);
	}
	return false;
}

bool MonsterInfo::getSuperUniqueName(WORD suid, char name[30]){
	MonsterInfo::Init();
	if (superUniques_.find(suid) != superUniques_.end()){
		strncpy(name, superUniques_[suid].name, 30);
		return true;
	}
	return false;
}

bool MonsterInfo::getItemStatCost(WORD id, ItemStatCost *costInfo) {
	MonsterInfo::Init();
	if (itemStatCost_.find(id) != itemStatCost_.end()){
		*costInfo = itemStatCost_[id];
	}
	return false;
}


//IDR_BINCLSIDNAME        BIN                     "ClassIdName.bin"
//IDR_BINNPCGFXTYPE       BIN                     "NpcGfxType.bin"
//IDR_BINNPCSTATS         BIN                     "NpcStats.bin"
//IDR_BINSUPERUNIQUE      BIN                     "SuperUnique.bin"
//IDR_BINITEMSTATCOST     BIN                     "itemstatcost.bin"


//use template later to clean these up
bool MonsterInfo::loadResistInfo()
{
	MonsterInfo::Init();

	ResistInfo info;

	std::ifstream in("data\\NpcStats.bin", std::ios::in | std::ios::binary);

	if (!in)
	{
		LOG(logERROR, NULL) << "can not find data\\NpcStats.bin";
		exit(1);
	}

	while (in.read(reinterpret_cast<char*>(&info), sizeof(ResistInfo)))
	{
		resistData_[info.npcid] = info;
	}

	return true;
}

bool MonsterInfo::loadClassIdNameFile()
{
	MonsterInfo::Init();
	ClassIdName info;

	std::ifstream in("data\\ClassIdName.bin", std::ios::in | std::ios::binary);

	if (!in)
	{
		LOG(logERROR, NULL) << "can not find data\\ClassIdName.bin";
		exit(1);
	}
	
	while (in.read(reinterpret_cast<char*>(&info), sizeof(ClassIdName)))
	{
		classIdName_[info.classId] = info.name;
	}

	return true;
}

bool MonsterInfo::loadGfxTypeFile()
{
	MonsterInfo::Init();
	NpcGfxType info;

	std::ifstream in("data\\NpcGfxType.bin", std::ios::in | std::ios::binary);

	if (!in)
	{
		LOG(logERROR, NULL) << "can not find data\\NpcGfxType.bin";
		exit(1);
	}

	while (in.read(reinterpret_cast<char*>(&info), sizeof(NpcGfxType)))
	{
		npcGfxType_[info.id] = info;
	}

	return true;
}

bool MonsterInfo::loadSuperUniqueFile()
{
	MonsterInfo::Init();
	SupUnique info;

	std::ifstream in("data\\SuperUnique.bin", std::ios::in | std::ios::binary);

	if (!in)
	{
		LOG(logERROR, NULL) << "can not find data\\SuperUnique.bin";
		exit(1);
	}

	while (in.read(reinterpret_cast<char*>(&info), sizeof(SupUnique)))
	{
		superUniques_[info.suId] = info;
	}

	return true;
}

bool MonsterInfo::loadItemStatCostFile()
{
	MonsterInfo::Init();
	ItemStatCost info;

	std::ifstream in("data\\itemstatcost.bin", std::ios::in | std::ios::binary);

	if (!in)
	{
		LOG(logERROR, NULL) << "can not find data\\itemstatcost.bin";
		exit(1);
	}

	while (in.read(reinterpret_cast<char*>(&info), sizeof(ItemStatCost)))
	{
		itemStatCost_[info.id] = info;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
