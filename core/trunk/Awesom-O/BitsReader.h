#pragma once

#include <stdio.h>
#include <windows.h>
#include <cstddef>

class BitsReader
{
private:
	const BYTE *data_;
	size_t size_;
	mutable size_t index_;
public:
	static const BYTE BITS_PER_BYTE = 8;
	static const BYTE BITS_PER_WORD = 16;
	static const BYTE BITS_PER_INT = 32;
	BitsReader();
	BitsReader(const BYTE *data, size_t sizeInByte);
	~BitsReader();
	bool skip(unsigned int bitCount) const;
	bool getBits(size_t bitCount, unsigned int *result) const;
	bool getBits(size_t pos, const size_t bitCount, unsigned int *result) const;

	BYTE getByte() const;
	WORD getWord() const;
	DWORD getDword() const;
	unsigned int read(std::size_t bits) const;

	size_t getDataSize() const;
	size_t getHandledBitCount() const;
	static void printBits(int bits);
};
