#include "Warps.h"
#include <cmath>

Warps::Warps()
{
}

Warps::~Warps()
{
}
	
void Warps::add(WARP warp) throw(...)
{
	cs.Lock();
	list.push_back(warp);
	cs.Unlock();
}

void Warps::remove(unsigned int uid) throw(...)
{
	cs.Lock();

	for (std::list<WARP>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter = list.erase(iter);
			break;
		}
	}

	cs.Unlock();
}

WARP Warps::find(WarpType::WarpType id) throw(...)
{
	//unsigned int uid = 0;
	WARP warp;
	warp.uid = 0;
	cs.Lock();
		
	for (std::list<WARP>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->id == id)
		{
			//uid = iter->uid;
			warp = *iter;
		}
	}

	cs.Unlock();
	//return uid;
	return warp;
}

WARP Warps::find(short x, short y, int dist) throw(...)
{
	//unsigned int uid = 0;
	WARP warp;
	warp.uid = 0;
	cs.Lock();
	
	for (std::list<WARP>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (((int)::sqrt((double)((iter->x - x) * (iter->x - x) + (iter->y - y) * (iter->y - y)))) <= 5)
		{
			//uid = iter->uid;
			warp = *iter;
		}
	}

	cs.Unlock();
	//return uid;
	return warp;
}
