#pragma once

#include "Item.h"
#include <vector>
#include <map>
#include "Log.h"

class Body { // tolua_export

private:
	std::map<EquipmentLocation::EquipmentLocation, ITEM> body;

public:

	void equip(ITEM item)
	{
		body[item.location] = item;
	}

	void unequip(ITEM item)
	{
		body.erase(item.location);
	}

	//tolua_begin
	std::vector<ITEM> getBody()
	{
		std::vector<ITEM> result;
		
		for (std::map<EquipmentLocation::EquipmentLocation, ITEM>::iterator iter = body.begin(); iter != body.end(); iter++)
		{
			result.push_back(iter->second);
		}

		return result;
	}

	ITEM find(unsigned int uid)
	{
		ITEM item;
		item.uid = 0;

		for (std::map<EquipmentLocation::EquipmentLocation, ITEM>::iterator iter = body.begin(); iter != body.end(); iter++)
		{
			if (iter->second.uid == uid)
			{
				item = iter->second;
				break;
			}
		}

	return item;
	}

	void repair(unsigned int uid)
	{
		for (std::map<EquipmentLocation::EquipmentLocation, ITEM>::iterator iter = body.begin(); iter != body.end(); iter++)
		{
			if (iter->second.uid == uid)
			{
				int dura = 100;
				for (int i = 0; i < iter->second.stats.size(); i++) {
					if (iter->second.stats[i].Stat->Type == StatType::MaxDurability) {
						dura = iter->second.stats[i].Value;
					}
					if (iter->second.stats[i].Stat->Type == StatType::Durability){
						iter->second.stats[i].Value = dura;
						break;
					}
				}
				break;
			}
		}
	}

	ITEM getLocation(EquipmentLocation::EquipmentLocation location)
	{
		return body[location];
	}
};
// tolua_end
