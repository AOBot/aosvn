#include "States.h"
//#include "d2data/Stat.h"

void States::setState(StateType::StateType state, std::vector<StatBase> stats)
{
	cs.Lock();
	state_map[state] = stats;
	cs.Unlock();
}

void States::endState(StateType::StateType state)
{
	cs.Lock();
	state_map.erase(state);
	cs.Unlock();
}
bool States::isActive(StateType::StateType state)
{
	if (state_map.find(state) != state_map.end())
		return true;
	return false;
}

std::vector<StatBase> States::getStats(StateType::StateType state)
{
	cs.Lock();
	std::vector<StatBase> stats = state_map[state];
	cs.Unlock();

	return stats;
}
