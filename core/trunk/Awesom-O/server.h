#ifndef SERVER_H
#define SERVER_H

#pragma comment(lib, "WS2_32.lib")
#include <iostream>
#include <winsock2.h>
#include <queue>
#include <vector>

#include "CdKeyManager.h"

extern hostent * realm_host;

extern SOCKET chat;
extern SOCKET realm;
extern SOCKET game;
extern SOCKET controller;

extern int newServerIp;

//// SetUpListener /////////////////////////////////////////////////////
// Sets up a listener on the given interface and port, returning the
// listening socket if successful; if not, returns INVALID_SOCKET.
SOCKET SetUpListener(int nPort);


//// SetupFDSets ///////////////////////////////////////////////////////
// Set up the three FD sets used with select() with the sockets in the
// connection list.  Also add one for the listener socket, if we have
// one.
void SetupFDSets(fd_set& ReadFDs, fd_set& WriteFDs, fd_set& ExceptFDs);

int init();

//// ShutdownConnection ////////////////////////////////////////////////
// Gracefully shuts the connection sd down.  Returns true if we're
// successful, false otherwise.
bool ShutdownConnection(SOCKET sd);

void server_tasks();

int server_clean();

#endif //SERVER_H
