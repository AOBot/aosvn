#pragma once

#include <list>

class CdKey;

class CdKeyManager
{
public:
	CdKeyManager(const char* filename);
	~CdKeyManager();

	const CdKey* AcquireKey(int productId);
	bool ReleaseKey(const CdKey* key);
	void ReleaseKeys();
	bool AddKey(const char* keyText);
	void ClearKeys();
	
private:
	int LoadKeys(const char* filename);

	struct KeyEntry
	{
		CdKey* key;
		bool acquired;
		int count;
	};

	static std::list<KeyEntry*> _keys;
};
