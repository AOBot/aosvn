#include "Proxy.h"
#include "AwesomO.h"
#include "socks.h"

ProxyVector pConnections;
SocksManager * socksManager;

Proxy::~Proxy()
{
}

SOCKET Proxy::new_client(int port, char * address, Sock * sock)
{
	if (sock)
	{
		CSocks cs;
		cs.SetVersion(SOCKS_VER4);  //SOCKS_VER5

		char aIP[50];
		strcpy_s(aIP, sizeof(aIP), sock->ip);

		cs.SetSocksPort(sock->port);
		cs.SetSocksAddress(aIP);

		cs.SetDestinationPort(port);
		cs.SetDestinationAddress(address);
		
		int c = cs.Connect();

		if (cs.m_IsError)
		{
			LOG(logERROR, awesomo) << cs.GetLastErrorMessage();
			return SOCKET_ERROR;
		}

		if (c == SOCKET_ERROR)
		{
			LOG(logERROR, awesomo) << "socks error";
			return SOCKET_ERROR;
		}

		DWORD ul=1;
		int ret=ioctlsocket(c,FIONBIO,&ul);

		LOG(logINFO, awesomo) << "connected thru socks";
		return c;
	}
	else
	{
		SOCKET c = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); 
		if (c == INVALID_SOCKET)
		{
			LOG(logERROR, NULL) << "invalid socket!";
			//WSACleanup(); and another stupid thing ^^
			return SOCKET_ERROR;
		}

		SOCKADDR_IN sinClient;
		memset(&sinClient, 0, sizeof(sinClient));

		char cIP[50];
		strcpy_s(cIP, sizeof(cIP), address);

		sinClient.sin_family = AF_INET;
		sinClient.sin_addr.s_addr = inet_addr(cIP); // Where to start server?
		sinClient.sin_port = htons(port); // Port
		LOG(logDEBUG2, NULL) << "Connecting to : " << cIP;

		if (connect(c, (LPSOCKADDR)&sinClient, sizeof(sinClient)) == SOCKET_ERROR)
		{
			LOG(logERROR, NULL) << "could not connect to the server!";
			//WSACleanup(); now that was stupid :|
			return SOCKET_ERROR;
		}

		DWORD ul=1;
		int ret=ioctlsocket(c,FIONBIO,&ul);

		//LOG(logINFO) << "opened connection to " << inet_ntoa(sinClient.sin_addr) << ":" << ntohs(sinClient.sin_port) << ", socket " << c << ".";

		return c;
	}

	return SOCKET_ERROR;
}