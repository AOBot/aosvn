#include "Inventory.h"

std::vector<ITEM> Inventory::get()
{
	std::vector<ITEM> vector;

	for (std::map<unsigned int, ITEM>::iterator iter = inventory.begin(); iter != inventory.end(); iter++)
	{
		vector.push_back(iter->second);
	}

	return vector;
}

bool Inventory::Check(ITEM * item, int x, int y)
{
	short width = item->baseItem->invWidth;
	short height = item->baseItem->invHeight;

	for (int w = x; w < x + width; w++)
	{
		for (int h = y; h < y + height; h++)
		{
			if (map[w][h] != 0)
			{
				return false;
			}
		}
	}

	return true;
}

POINT Inventory::findLocation(ITEM item)
{
	POINT result = {-1, -1};

	for (int x = 0; x < 10; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			map[x][y] = 0;
		}
	}

	for (std::map<unsigned int, ITEM>::iterator iter = inventory.begin(); iter != inventory.end(); iter++)
	{
		for (int x = iter->second.x; x < iter->second.x + iter->second.baseItem->invWidth; x++)
		{
			for (int y = iter->second.y; y < iter->second.y + iter->second.baseItem->invHeight; y++)
			{
				map[x][y] = 1;
			}
		}
	}

	short width = item.baseItem->invWidth;
	short height = item.baseItem->invHeight;

	for (int x = 0; x <= 10 - width; x++)
	{
		for (int y = 0; y <= 4 - height; y++)
		{
			if (Check(&item, x, y))
			{
				result.x = x;
				result.y = y;

				return result;
			}
		}
	}

	return result;
}

void Inventory::add(ITEM item)
{
	inventory[item.uid] = item;
}

void Inventory::remove(ITEM item)
{
	inventory.erase(item.uid);
}

ITEM Inventory::find(unsigned int uid)
{
	ITEM item;
	item.uid = 0;

	for (std::map<unsigned int, ITEM>::iterator iter = inventory.begin(); iter != inventory.end(); iter++)
	{
		if (iter->second.uid == uid) 
		{
			item = iter->second;
		}
	}

	
	return item;
}


std::vector<ITEM> Inventory::findByCode(std::string code)
{
	std::vector<ITEM> result;

	for (std::map<unsigned int, ITEM>::iterator iter = inventory.begin(); iter != inventory.end(); iter++)
	{
		if (iter->second.baseItem->code.compare(code) == 0) 
		{
			result.push_back(iter->second);
		}
	}
	return result;
}

int Inventory::countColumns()
{
	for (int x = 0; x < 10; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			map[x][y] = 0;
		}
	}

	for (std::map<unsigned int, ITEM>::iterator iter = inventory.begin(); iter != inventory.end(); iter++)
	{
		for (int x = iter->second.x; x < iter->second.x + iter->second.baseItem->invWidth; x++)
		{
			for (int y = iter->second.y; y < iter->second.y + iter->second.baseItem->invHeight; y++)
			{
				map[x][y] = 1;
			}
		}
	}

	int result = 0;

	for (int x = 0; x < 10; x++)
	{
		int y;

		for (y = 0; y < 4; y++)
		{
			if (map[x][y])
			{
				break;
			}
		}

		if (y == 4)
		{
			result++;
		}
	}

	return result;
}
