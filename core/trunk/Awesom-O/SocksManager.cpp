#include "SocksManager.h"
#include "Log.h"

SocksManager::SocksManager(const char * socks_file)
{
	file.open(socks_file, std::ios_base::in | std::ios_base::out);
	std::string line;
	
	while (file >> line)
	{
		add(line);
	}
}

SocksManager::~SocksManager()
{
	clear();
	file.close();
}

Sock * SocksManager::get()
{
	if (list.empty())
	{
		return NULL;
	}
	
	LOG(logINFO, NULL) << "socks requested";
	Sock * sock = list.front();
	list.push_back(sock);
	list.pop_front();

	file.clear();
	file.seekp(std::ios_base::beg);

	for (std::list<Sock *>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		file << (*iter)->ip << ":" << (*iter)->port << std::endl;
	}

	return sock;
}

bool SocksManager::empty()
{
	return list.empty();
}

void SocksManager::clear()
{
	for (std::list<Sock *>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		delete *iter;
	}

	list.clear();
}

void SocksManager::add(std::string line)
{
	int pos = line.find(':');

	std::string ip = line.substr(0, pos);
	std::string port = line.substr(pos + 1);

	Sock * sock = new Sock();
	
	strcpy_s(sock->ip, 16, ip.c_str());
	sock->port = atoi(port.c_str());
	sock->count = 0;

	list.push_back(sock);
}
