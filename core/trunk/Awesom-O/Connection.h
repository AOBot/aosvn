#ifndef CONNECTION_H
#define CONNECTION_H

#include <iostream>
#include <winsock2.h>
#include <queue>
#include <vector>
#include <set>

#include "Log.h"
#include "CriticalSection.h"

const int kBufferSize = 100000;

bool ShutdownConnection(SOCKET sd);

class Connection
{
public:
    SOCKET socket;

	char * read;
	int read_length;

	char * write;
	int write_length;

	bool magic;

	CriticalSection writing;

public:
	Connection() : read_length(0), write_length(0), magic(false)
	{
		read = new char[kBufferSize];
		write = new char[kBufferSize];

		Connection::connections.insert(this);
	}

	Connection(SOCKET s) : socket(s), read_length(0), write_length(0), magic(false)
	{
		read = new char[kBufferSize];
		write = new char[kBufferSize];

		Connection::connections.insert(this);
	}

	Connection(SOCKET s, const int size) : socket(s), read_length(0), write_length(0), magic(false)
	{
		read = new char[size];
		write = new char[size];
		LOG(logINFO, NULL) << "create connection";

		Connection::connections.insert(this);
	}

	virtual ~Connection()
	{
		ShutdownConnection(socket);
		delete [] read;
		delete [] write;
		LOG(logINFO, NULL) << "delete connection";

		Connection::connections.erase(this);
	}

	virtual void Process()
	{
	}

	virtual void Write(const char * buffer, int length)
	{
		writing.Lock();
		memcpy(write + write_length, buffer, length);
		write_length += length;
		writing.Unlock();
	}

	virtual int ReadData()
	{
		int nBytes = recv(socket, read + read_length, kBufferSize - read_length, 0);

		if (nBytes == 0)
		{
			return 1;
		}
		else if (nBytes == SOCKET_ERROR)
		{
			int err;
			int errlen = sizeof(err);
			getsockopt(socket, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);
			
			if (err == WSAEWOULDBLOCK)
			{
				return 0;
			}
			else
			{
				return -1;
			}
		}

		read_length += nBytes;

		return 0;
	}

	virtual int WriteData()
	{
		int nBytes = send(socket, write, write_length, 0);

		if (nBytes == SOCKET_ERROR)
		{
			// Something bad happened on the socket.  Deal with it.
			int err;
			int errlen = sizeof(err);
			getsockopt(socket, SOL_SOCKET, SO_ERROR, (char*)&err, &errlen);

			if (err == WSAEWOULDBLOCK)
			{
				return 0;
			}
			else
			{
				return -1;
			}
		}

		if (nBytes == write_length)
		{
			// Everything got sent, so take a shortcut on clearing buffer.
			write_length = 0;
		}
		else
		{
			// We sent part of the buffer's data.  Remove that data from
			// the buffer.
			write_length -= nBytes;
			memmove(write, write + nBytes, write_length);
		}
	    
		return 0;
	}

	static std::set<Connection *> connections;
};

#endif //CONNECTION_H
