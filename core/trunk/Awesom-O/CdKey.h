#pragma once

class CdKey
{
public:
	CdKey(const char* keyText);

	bool ComputeHash(int clientToken, int serverToken, void* hash, int size) const;

	bool IsValid() const;
	int GetProductId() const;
	int GetPublicValue() const;
	int GetPrivateValue() const;
	const char* GetKeyText() const;

	static const int KeySize = 16;
	static const int HashSize = 20;

private:
#pragma pack(push)
#pragma pack(1)
	struct HashGroup
	{
		int clientToken;
		int serverToken;
		int productId;
		int publicValue;
		int unknown1;
		int privateValue;
		short unknown2;
	};
#pragma pack(pop)

	static const unsigned char Map[];

	bool Parse(const char* keyText);

	static int RotateLeft(unsigned int value, int shift);
	static int HexToValue(char c);	
	static char ValueToHex(int v);
	static int HexToValue(const char* s, int n);
	static bool ComputeHash(const void* input, int sizeIn, void* output, int sizeOut);
	static bool ProcessKey(const char* keyText, int* productId, int* publicValue, int* privateValue);

	char _keyText[KeySize + 1];
	bool _valid;
	int _productId;
	int _publicValue;
	int _privateValue;
};
