#include "AwesomO.h"
#include "Log.h"

#pragma region Packet 0x01 - Walk To Location
/// <summary>
/// Game Client Packet 0x01 - Walk To Location
/// </summary>
void AwesomO::WalkToLocation(unsigned short x, unsigned short y) throw(...)
{
	unsigned char buffer[5] = { 0x01,
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x02 - Walk To Target
/// <summary>
/// Game Client Packet 0x02 - Walk To Target
/// </summary>
void AwesomO::WalkToTarget(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x02, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x03 - Run To Location
/// <summary>
/// Game Client Packet 0x03 - Run To Location
/// </summary>
void AwesomO::RunToLocation(unsigned short x, unsigned short y) throw(...)
{
	unsigned char buffer[5] = { 0x03,
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));

	// to make the player run instead of warping around
	unsigned char buffer2[16] = { 0x0F,
		(unsigned char)(UnitType::Player),
		(unsigned char)(me->uid), (unsigned char)(me->uid >> 8), (unsigned char)(me->uid >> 16), (unsigned char)(me->uid >> 24),
		(unsigned char)(MovementType::Run),
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8),
		0x00, // unused
		(unsigned char)(me->x), (unsigned char)(me->x >> 8),
		(unsigned char)(me->y), (unsigned char)(me->y >> 8)
	};

	game->WriteServer(buffer2, sizeof(buffer2));

}
#pragma endregion
#pragma region Packet 0x04 - Run To Target
/// <summary>
/// Game Client Packet 0x04 - Run To Target
/// </summary>
void AwesomO::RunToTarget(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x04, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x05 - Cast Left Skill
/// <summary>
/// Game Client Packet 0x05 - Cast Left Skill
/// <para>Casts the currently set left hand skill on a target location.</para>
/// </summary>
void AwesomO::CastLeftSkill(unsigned short x, unsigned short y) throw(...)
{
	// to see the attack client side (object target)
	LoadSkill(me->left, me->uid, me->uid);

	unsigned char buffer[5] = { 0x05,
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x06 - Cast Left Skill On Target
/// <summary>
/// Game Client Packet 0x06 - Cast Left Skill On Target
/// <para>Casts the currently set left hand skill on a target unit.</para>
/// </summary>
void AwesomO::CastLeftSkillOnTarget(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	// to see the attack client side (object target)
	LoadSkillOnUnit(me->left, UnitType::Player, me->uid, uid);

	unsigned char buffer[9] = { 0x06, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x07 - Cast Left Skill On Target Stopped
/// <summary>
/// Game Client Packet 0x07 - Cast Left Skill On Target Stopped
/// <para>Casts the currently set left hand skill on a target unit.</para>
/// </summary>
void AwesomO::CastLeftSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x07, 
		(byte)unitType, 0, 0, 0,
		(byte)(uid), (byte)(uid >> 8), (byte)(uid >> 16), (byte)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x08 - Recast Left Skill
/// <summary>
/// Game Client Packet 0x08 - Recast Left Skill
/// <para>Recasts the currently set left hand skill on a target location when spamming.</para>
/// </summary>
void AwesomO::RecastLeftSkill(unsigned short x, unsigned short y) throw(...)
{
	// to see the attack client side (object target)
	LoadSkill(me->left, me->uid, me->uid);

	unsigned char buffer[5] = { 0x08,
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x09 - Recast Left Skill On Target
/// <summary>
/// Game Client Packet 0x09 - Recast Left Skill On Target
/// <para>Recasts the currently set left hand skill on a target unit when spamming.</para>
/// </summary>
void AwesomO::RecastLeftSkillOnTarget(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	// to see the attack client side (object target)
	LoadSkillOnUnit(me->left, UnitType::Player, me->uid, uid);

	unsigned char buffer[9] = { 0x09, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0A - Recast Left Skill On Target Stopped
/// <summary>
/// Game Client Packet 0x0A - Recast Left Skill On Target Stopped
/// <para>Recasts the currently set left hand skill on a target unit when spamming.</para>
/// </summary>
void AwesomO::RecastLeftSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x0A, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0C - Cast Right Skill
/// <summary>
/// Game Client Packet 0x0C - Cast Right Skill
/// <para>Casts the currently set right hand skill on a target location.</para>
/// </summary>
void AwesomO::CastRightSkill(unsigned short x, unsigned short y) throw(...)
{
	// to see the attack client side (object target)
	LoadSkill(me->right, me->uid, me->uid);

	unsigned char buffer[5] = { 0x0C,
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0D - Cast Right Skill On Target
/// <summary>
/// Game Client Packet 0x0D - Cast Right Skill On Target
/// <para>Casts the currently set right hand skill on a target unit.</para>
/// </summary>
void AwesomO::CastRightSkillOnTarget(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	// to see the attack client side (object target)
	LoadSkillOnUnit(me->right, UnitType::Player, me->uid, uid);

	unsigned char buffer[9] = { 0x0D, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0E - Cast Right Skill On Target Stopped
/// <summary>
/// Game Client Packet 0x0E - Cast Right Skill On Target Stopped
/// <para>Casts the currently set right hand skill on a target unit.</para>
/// </summary>
void AwesomO::CastRightSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x0E, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x0F - Recast Right Skill
/// <summary>
/// Game Client Packet 0x0F - Recast Right Skill
/// <para>Recasts the currently set right hand skill on a target location when spamming.</para>
/// </summary>
void AwesomO::RecastRightSkill(unsigned short x, unsigned short y) throw(...)
{
	// to see the attack client side (object target)
	LoadSkill(me->right, me->uid, me->uid);

	unsigned char buffer[5] = { 0x0F,
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x10 - Recast Right Skill On Target
/// <summary>
/// Game Client Packet 0x10 - Recast Right Skill On Target
/// <para>Recasts the currently set right hand skill on a target unit when spamming.</para>
/// </summary>
void AwesomO::RecastRightSkillOnTarget(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	// to see the attack client side (object target)
	LoadSkillOnUnit(me->right, UnitType::Player, me->uid, uid);

	unsigned char buffer[9] = { 0x10, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x11 - Recast Right Skill On Target Stopped
/// <summary>
/// Game Client Packet 0x11 - Recast Right Skill On Target Stopped
/// <para>Recasts the currently set right hand skill on a target unit when spamming.</para>
/// </summary>
void AwesomO::RecastRightSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x11, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x13 - Unit Interact
/// <summary>
/// Game Client Packet 0x13 - Unit Interact
/// <para>Try to interact with a unit. Result will vary depending on unit type...</para>
/// </summary>
void AwesomO::UnitInteract(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x13, 
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x14 - Send Overhead Message
/// <summary>
/// Game Client Packet 0x14 - Send Overhead Message
/// <para>Sends a message to be displayed over the character.</para>
/// </summary>
void AwesomO::SendOverheadMessage(std::string message) throw(...)
{
	if (message.length() != 0)
	{
        unsigned char* buffer = new unsigned char[6 + message.length()];
        int offset = 0;

		buffer[offset++] = 0x14;
        
		for (unsigned int i = 0; i < message.length(); i++)
            buffer[offset++] = (unsigned char)message[i];
	
		buffer[offset++] = 0x00;

		game->WriteClient(buffer, offset);

		delete[] buffer;
	}
}
void AwesomO::SendOverheadMessage(const char* format, ...) throw(...)
{
	char text[4096];

	va_list arguments;
	va_start(arguments, format);
	vsprintf_s(text, sizeof(text), format, arguments);
	va_end(arguments);	

	SendOverheadMessage(std::string(text));
}
#pragma endregion
#pragma region Packet 0x15 - Send Message
/// <summary>
/// Game Client Packet 0x15 - Send Message
/// <para>Send a public game message or whisper to a player.</para>
/// </summary>
void AwesomO::SendMessage(const char* message, GameMessageType::GameMessageType type, const char* recipient) throw(...)
{
	char text[4096];

//	if (recipient == NULL)
//		if(getManager(false)->settings->get(INI::PublicChat) == false)
//			return;

	strcpy_s(text, sizeof(text), message);
	char *pch = strtok(text, "~");

	while (pch != NULL)
	{		
		LOG(logINFO, this) << "Message: " << pch;

		if (strlen(pch) != 0)
		{
			int length = (int)6 + strlen(pch);

			if (recipient != NULL)
			{
				length += (int)strlen(recipient);
			}

			unsigned char* buffer = new unsigned char[length];
			int offset = 0;

			buffer[offset++] = 0x15;
			buffer[offset++] = (unsigned char)type;
			buffer[offset++] = (unsigned char)((unsigned char)type >> 8);
	        
			for (unsigned int i = 0; i < strlen(pch); i++)
				buffer[offset++] = (unsigned char)pch[i];
		
			buffer[offset++] = 0x00;

			if (recipient != NULL)
				for (unsigned int i = 0; i < strlen(recipient); i++)
					buffer[offset++] = (unsigned char)recipient[i];

			buffer[offset++] = 0x00;
			buffer[offset++] = 0x00;

			game->WriteClient(buffer, offset);

			delete[] buffer;
		}

		pch = strtok(NULL, "~");
	}
}

void AwesomO::Speak(const char* text)
{
	if (strlen(text) > 0)
	{
		int size = static_cast<int>(strlen(text)) + 6;
		char unsigned * buffer = new unsigned char[size];
		int offset = 0;

		buffer[offset++] = 0x15;
		buffer[offset++] = 0x01;
		buffer[offset++] = 0x00;

		for (int i = 0; text[i] != '\0'; i++)
		{
			buffer[offset++] = text[i];
		}
		buffer[offset++] = 0x00;

		buffer[offset++] = 0x00;
		buffer[offset++] = 0x00;

//		IPacket* packet = getGame(false)->_proxy->CreatePacket(buffer, offset);
		//packet->SetFlag(IPacket::PacketFlag_Hidden);
//		getGame(false)->_proxy->RelayDataToServer(packet, getGame());

		chat->WriteClient(buffer, offset);

		delete[] buffer;
	}
}
/*
void AwesomO::SendMessage(const char* format, ...) throw(...)
{
	char text[4096];

	va_list arguments;
	va_start(arguments, format);
	vsprintf_s(text, sizeof(text), format, arguments);
	va_end(arguments);	

	SendMessage(text);
}
*/
#pragma endregion
#pragma region Packet 0x16 - PickItem
/// <summary>
/// Game Client Packet 0x16 - PickItem
/// <para>Pick up an item from the ground.</para>
/// </summary>
void AwesomO::PickItem(unsigned int requestID, unsigned int uid, bool toCursor) throw(...)
{
	unsigned char buffer[13] = { 0x16, 
		(unsigned char)(requestID), (unsigned char)(requestID >> 8), (unsigned char)(requestID >> 16), (unsigned char)(requestID >> 24),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(toCursor ? 1 : 0), 0, 0, 0
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x17 - DropItem
/// <summary>
/// Game Client Packet 0x17 - DropItem
/// <para>Drop the cursor item to the ground.</para>
/// </summary>
void AwesomO::DropItem(unsigned int uid) throw(...)
{
	unsigned char buffer[5] = { 0x17, 
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x18 - Drop Item To Container
/// <summary>
/// Game Client Packet 0x18 - Drop Item To Container
/// <para>Drop an item from cursor into a container.</para>
/// </summary>
void AwesomO::DropItemToContainer(unsigned int uid, unsigned int x, unsigned int y, ItemContainerGC::ItemContainerGC container) throw(...)
{
	unsigned char buffer[17] = { 0x18, 
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)x, 0, 0, 0,
		(unsigned char)y, 0, 0, 0,
		(unsigned char)container, 0, 0, 0
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x19 - Pick Item From Container
/// <summary>
/// Game Client Packet 0x19 - Pick Item From Container
/// <para>Remove an item from a container and place it on cursor.</para>
/// </summary>
void AwesomO::PickItemFromContainer(unsigned int uid) throw(...)
{
	unsigned char buffer[5] = { 0x19, 
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x1A - Equip Item
/// <summary>
/// Game Client Packet 0x1A - Equip Item
/// <para>Equip an item on the cursor to a specified body location.</para>
/// <para>Item must be on cursor, of right type for location and location must be empty.</para>
/// </summary>
void AwesomO::EquipItem(unsigned int uid, EquipmentLocation::EquipmentLocation location) throw(...)
{
	unsigned char buffer[9] = { 0x1A, 
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)location, 0, 0, 0
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x1C - Unequip Item
/// <summary>
/// Game Client Packet 0x1C - Unequip Item
/// <para>Removes an equipped item from body and places on the cursor.</para>
/// </summary>
void AwesomO::UnequipItem(EquipmentLocation::EquipmentLocation location) throw(...)
{
	unsigned char buffer[3] = { 0x1C, (unsigned char)location, 0 };

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x1D - Swap Equipped Item
/// <summary>
/// Game Client Packet 0x1D - Swap Equipped Item
/// <para>Swaps an equipped item with another one on cursor.</para>
/// </summary>
void AwesomO::SwapEquippedItem(unsigned int uid, EquipmentLocation::EquipmentLocation location) throw(...)
{
	unsigned char buffer[9] = { 0x1D, 
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)location, 0, 0, 0
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x1F - Swap Container Item
/// <summary>
/// Game Client Packet 0x1F - Swap Container Item
/// <para>Swap cursor (<see cref="GameClient.SwapContainerItem.SubjectUID"/>) 
/// item with another item in a container (<see cref="GameClient.SwapContainerItem.ObjectUID"/>).</para>
/// <para>Don't send this packet to replace items in trade as trade doesn't support item replacement.</para>
/// </summary>
void AwesomO::SwapContainerItem(unsigned int subjectUID, unsigned int objectUID, unsigned int x, unsigned int y) throw(...)
{
	unsigned char buffer[17] = { 0x1F,
		(unsigned char)(subjectUID), (unsigned char)(subjectUID >> 8), (unsigned char)(subjectUID >> 16), (unsigned char)(subjectUID >> 24),
		(unsigned char)(objectUID), (unsigned char)(objectUID >> 8), (unsigned char)(objectUID >> 16), (unsigned char)(objectUID >> 24),
		(unsigned char)(x), (unsigned char)(x >> 8), (unsigned char)(x >> 16), (unsigned char)(x >> 24),
		(unsigned char)(y), (unsigned char)(y >> 8), (unsigned char)(y >> 16), (unsigned char)(y >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x20 - Use Inventory Item
/// <summary>
/// Game Client Packet 0x20 - Use Inventory Item
/// <para>Use an item (drink potion, use tome, open cube, etc.) in inventory.</para>
/// <para><see cref="GameClient.UseInventoryItem.MeX"/> and <see cref="GameClient.UseInventoryItem.MeY"/> 
/// are your standing location for the area you are in, not item position in inventory !</para>
/// </summary>
void AwesomO::UseInventoryItem(unsigned int itemUID, unsigned int meX, unsigned int meY) throw(...)
{
	unsigned char buffer[13] = { 0x20,
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24),
		(unsigned char)(meX), (unsigned char)(meX >> 8), (unsigned char)(meX >> 16), (unsigned char)(meX >> 24),
		(unsigned char)(meY), (unsigned char)(meY >> 8), (unsigned char)(meY >> 16), (unsigned char)(meY >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x21 - Stack Items
/// <summary>
/// Game Client Packet 0x21 - Stack Items
/// <para>Stack cursor (subject) item into another item (object).</para>
/// </summary>
void AwesomO::StackItems(unsigned int subjectUID, unsigned int objectUID) throw(...)
{
	unsigned char buffer[9] = { 0x21,
		(unsigned char)(subjectUID), (unsigned char)(subjectUID >> 8), (unsigned char)(subjectUID >> 16), (unsigned char)(subjectUID >> 24),
		(unsigned char)(objectUID), (unsigned char)(objectUID >> 8), (unsigned char)(objectUID >> 16), (unsigned char)(objectUID >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x23 - Add Belt Item
/// <summary>
/// Game Client Packet 0x23 - Add Belt Item
/// <para>Place the item on cursor into specified position in belt. Must be a potion or scroll...</para>
/// </summary>
void AwesomO::AddBeltItem(unsigned int uid, unsigned int x) throw(...)
{
	unsigned char buffer[9] = { 0x23,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(x), (unsigned char)(x >> 8), (unsigned char)(x >> 16), (unsigned char)(x >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x24 - Remove Belt Item
/// <summary>
/// Game Client Packet 0x24 - Remove Belt Item
/// <para>Remove an item (potion or scroll) from belt and place it on cursor.</para>
/// </summary>
void AwesomO::RemoveBeltItem(unsigned int uid) throw(...)
{
	unsigned char buffer[5] = { 0x24,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x25 - Swap Belt Item
/// <summary>
/// Game Client Packet 0x25 - Swap Belt Item
/// <para>Place cursor item belt and replace it with the specified item in it's location. </para>
/// </summary>
void AwesomO::SwapBeltItem(unsigned int oldItemUID, unsigned int newItemUID) throw(...)
{
	unsigned char buffer[9] = { 0x25,
		(unsigned char)(oldItemUID), (unsigned char)(oldItemUID >> 8), (unsigned char)(oldItemUID >> 16), (unsigned char)(oldItemUID >> 24),
		(unsigned char)(newItemUID), (unsigned char)(newItemUID >> 8), (unsigned char)(newItemUID >> 16), (unsigned char)(newItemUID >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x26 - Use Belt Item
/// <summary>
/// Game Client Packet 0x26 - Use Belt Item
/// <para>Consume an item (potion or scroll) in belt.</para>
/// </summary>
void AwesomO::UseBeltItem(unsigned int itemUID, bool toMerc, unsigned int unknown9) throw(...)
{
	unsigned char buffer[13] = { 0x26,
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24),
		(unsigned char)(toMerc ? 1 : 0), 0, 0, 0,
		(unsigned char)(unknown9), (unsigned char)(unknown9 >> 8), (unsigned char)(unknown9 >> 16), (unsigned char)(unknown9 >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x27 - Identify Item
/// <summary>
/// Game Client Packet 0x27 - Identify Item
/// <para>Use an identify scroll on an item.</para>
/// <para>If already indentified, id cursor is canceled and scroll is not used.</para>
/// <para>If not clicking on an item, UID is that of scroll and action is canceled.</para>
/// </summary>
void AwesomO::IdentifyItem(unsigned int itemUID, unsigned int scrollUID) throw(...)
{
	unsigned char buffer[9] = { 0x27,
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24),
		(unsigned char)(scrollUID), (unsigned char)(scrollUID >> 8), (unsigned char)(scrollUID >> 16), (unsigned char)(scrollUID >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x29 - Embed Item
/// <summary>
/// Game Client Packet 0x29 - Embed Item
/// <para>Put an item into a container item (e.g. scroll in tome.)</para>
/// </summary>
void AwesomO::EmbedItem(unsigned int subjectUID, unsigned int objectUID) throw(...)
{
	unsigned char buffer[9] = { 0x29,
		(unsigned char)(subjectUID), (unsigned char)(subjectUID >> 8), (unsigned char)(subjectUID >> 16), (unsigned char)(subjectUID >> 24),
		(unsigned char)(objectUID), (unsigned char)(objectUID >> 8), (unsigned char)(objectUID >> 16), (unsigned char)(objectUID >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x2A - Item To Cube
/// <summary>
/// Game Client Packet 0x2A - Item To Cube
/// <para>Drop an item in the Horadric cube by clicking on it with the item on cursor.</para>
/// <para>Note that a regular ItemToContainer packet is used if the item is dropped at a location in the cube and not on it.</para>
/// </summary>
void AwesomO::ItemToCube(unsigned int itemUID, unsigned int cubeUID) throw(...)
{
	unsigned char buffer[9] = { 0x2A,
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24),
		(unsigned char)(cubeUID), (unsigned char)(cubeUID >> 8), (unsigned char)(cubeUID >> 16), (unsigned char)(cubeUID >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x2F - Town Folk Interact
/// <summary>
/// Game Client Packet 0x2F - Town Folk Interact
/// <para>Actual interact request sent after NPC is in interaction range.</para>
/// <para>A regular UnitInteract is sometimes(?) also sent first...</para>
/// </summary>
void AwesomO::TownFolkInteract(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x2F,
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x30 - Town Folk Cancel Interaction
/// <summary>
/// Game Client Packet 0x30 - Town Folk Cancel Interaction
/// <para>Close interact menu or trade etc. screen and break interact with town folk.</para>
/// </summary>
void AwesomO::TownFolkCancelInteraction(UnitType::UnitType unitType, unsigned int uid) throw(...)
{
	unsigned char buffer[9] = { 0x30,
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x31 - Display Quest Message
/// <summary>
/// Game Client Packet 0x31 - Display Quest Message
/// <para>Notifies D2GS that a quest message was displayed so it can update the quest log.</para>
/// </summary>
void AwesomO::DisplayQuestMessage(unsigned int uid, unsigned int message) throw(...)
{
	unsigned char buffer[9] = { 0x31,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(message), (unsigned char)(message >> 8), (unsigned char)(message >> 16), (unsigned char)(message >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x32 - Buy Item
/// <summary>
/// Game Client Packet 0x32 - Buy Item
/// <para>Buy an item from a town folk.</para>
/// </summary>
void AwesomO::BuyItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost, bool fillStack) throw(...)
{
	unsigned char buffer[17] = { 0x32,
		(unsigned char)(dealerUID), (unsigned char)(dealerUID >> 8), (unsigned char)(dealerUID >> 16), (unsigned char)(dealerUID >> 24),
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24),
		0x00, 0, 0, fillStack ? 0x80 : 0,
		(unsigned char)(cost), (unsigned char)(cost >> 8), (unsigned char)(cost >> 16), (unsigned char)(cost >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
}
void AwesomO::GambleItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost) throw(...)
{
	unsigned char buffer[17] = { 0x32,
		(unsigned char)(dealerUID), (unsigned char)(dealerUID >> 8), (unsigned char)(dealerUID >> 16), (unsigned char)(dealerUID >> 24),
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24),
		0x02, 0, 0, 0,
		(unsigned char)(cost), (unsigned char)(cost >> 8), (unsigned char)(cost >> 16), (unsigned char)(cost >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x33 - Sell Item
/// <summary>
/// Game Client Packet 0x33 - Sell Item
/// <para>Sell an item to a town folk.</para>
/// </summary>
void AwesomO::SellItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost) throw(...)
{
	unsigned char buffer[17] = { 0x33,
		(unsigned char)(dealerUID), (unsigned char)(dealerUID >> 8), (unsigned char)(dealerUID >> 16), (unsigned char)(dealerUID >> 24),
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24),
		4, 0, 0, 0,
		(unsigned char)(cost), (unsigned char)(cost >> 8), (unsigned char)(cost >> 16), (unsigned char)(cost >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x34 - Cain Identify Items
/// <summary>
/// Game Client Packet 0x34 - Cain Identify Items
/// <para>Select the Identify Items option when interacted with Cain.</para>
/// </summary>
void AwesomO::CainIdentifyItems(unsigned int uid) throw(...)
{
	unsigned char buffer[5] = { 0x34,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x35 - Town Folk Repair
/// <summary>
/// Game Client Packet 0x35 - Town Folk Repair
/// <para>Ask a blacksmith to repair one or all item(s).</para>
/// </summary>
void AwesomO::TownFolkRepair(unsigned int dealerUID) throw(...)
{
	unsigned char buffer[17] = { 0x35,
		(unsigned char)(dealerUID), (unsigned char)(dealerUID >> 8), (unsigned char)(dealerUID >> 16), (unsigned char)(dealerUID >> 24),
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0x80
	};

	game->WriteClient(buffer, sizeof(buffer));
}

void AwesomO::TownFolkRepair(unsigned int dealerUID, unsigned int itemUID) throw(...)
{
	//TODO: overload for RepairItem
	unsigned char buffer[17] = { 0x35,
		(unsigned char)(dealerUID), (unsigned char)(dealerUID >> 8), (unsigned char)(dealerUID >> 16), (unsigned char)(dealerUID >> 24),
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0x80
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x36 - Hire Mercenary
/// <summary>
/// Game Client Packet 0x36 - Hire Mercenary
/// <para>Hire a mercenary from a town folk.</para>
/// </summary>
void AwesomO::HireMercenary(unsigned int dealerUID, unsigned int mercID) throw(...)
{
	unsigned char buffer[9] = { 0x36,
		(unsigned char)(dealerUID), (unsigned char)(dealerUID >> 8), (unsigned char)(dealerUID >> 16), (unsigned char)(dealerUID >> 24),
		(unsigned char)(mercID), (unsigned char)(mercID >> 8), (unsigned char)(mercID >> 16), (unsigned char)(mercID >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x37 - Identify Gamble Item
/// <summary>
/// Game Client Packet 0x37 - Identify Gamble Item
/// <para>Identify a gambled item after you bought it.</para>
/// </summary>
void AwesomO::IdentifyGambleItem(unsigned int uid) throw(...)
{
	unsigned char buffer[5] = { 0x37,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x38 - Town Folk Menu Select
/// <summary>
/// Game Client Packet 0x38 - Town Folk Menu Select
/// <para>Choose an menu item / action type once interacted with the town folk.</para>
/// </summary>
void AwesomO::TownFolkMenuSelect(TownFolkMenuItem::TownFolkMenuItem selection, unsigned int uid, unsigned int unknown9) throw(...)
{
	unsigned char buffer[13] = { 0x38,
		(unsigned char)selection, (unsigned char)((unsigned int)selection >> 8), (unsigned char)((unsigned int)selection >> 16), (unsigned char)((unsigned int)selection >> 24),
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(unknown9), (unsigned char)(unknown9 >> 8), (unsigned char)(unknown9 >> 16), (unsigned char)(unknown9 >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x3A - Increment Attribute
/// <summary>
/// Game Client Packet 0x3A - Increment Attribute
/// <para>Raise an attribute by one point.</para>
/// </summary>
void AwesomO::IncrementAttribute(StatType::StatType attribute, unsigned char count) throw(...)
{
	unsigned char buffer[3] = { 0x3A, (unsigned char)attribute, (unsigned char)((unsigned short)attribute >> 8) };

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x3B - Increment Skill
/// <summary>
/// Game Client Packet 0x3B - Increment Skill
/// <para>Raise a skill by one point.</para>
/// </summary>
void AwesomO::IncrementSkill(SkillType::SkillType skill) throw(...)
{
	unsigned char buffer[3] = { 0x3B, (unsigned char)skill, (unsigned char)((unsigned short)skill >> 8) };

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x3C - SelectSkill
/// <summary>
/// Game Client Packet 0x3C - SelectSkill
/// <para>Sets a given skill as the active one for specified hand.</para>
/// </summary>
bool AwesomO::SelectSkill(SkillType::SkillType skill, SkillHand::SkillHand hand, unsigned int chargedItemUID) throw(...)
{
	if (me->getSkill(hand) == skill)
	{
		return false;
	}

	if (!skills->find(skill))
	{		
//		chargedItemUID = me.findChargedSkill(skill);
//
//		if (!chargedItemUID)
//		{
			Chat("Skill not found!! Check your settings ");
			LOG(logERROR, this) << "Selecting skill you don't have " << (int)skill;
			return false;
//		}
		//Chat(getGame()->_chatName, false, "Charged Skill found!!");
	}
	else
	{
		//Chat(getGame()->_chatName, false, "Skill found!!");
	}

	unsigned char buffer[9] = { 0x3C, 
		(unsigned char)skill, (unsigned char)((unsigned short)skill >> 8),
		0, (unsigned char)(hand == SkillHand::Left ? 0x80 : 0),
		(unsigned char)(chargedItemUID), (unsigned char)(chargedItemUID >> 8), (unsigned char)(chargedItemUID >> 16), (unsigned char)(chargedItemUID >> 24),
	};

	game->WriteClient(buffer, sizeof(buffer));
	me->setSkill(hand, skill); // Thanks nathan259 
	return true;
}
#pragma endregion
#pragma region Packet 0x3D - Hover Unit
/// <summary>
/// Game Client Packet 0x3D - Hover Unit
/// <para>Notifies the server client want's to be informed of special mode changes for unit.</para>
/// <para>This is used to update doors' text to "Blocked Door" when a unit passes over it...</para>
/// </summary>
void AwesomO::HoverUnit(unsigned int uid) throw(...)
{
	unsigned char buffer[5] = { 0x3D,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x3F - Send Character Speech
/// <summary>
/// Game Client Packet 0x3F - Send Character Speech
/// </summary>
void AwesomO::SendCharacterSpeech(GameSound::GameSound speech) throw(...)
{
	unsigned char buffer[3] = { 0x3F, (unsigned char)speech, (unsigned char)((unsigned short)speech >> 8) };

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x40 - Request Quest Log
/// <summary>
/// Game Client Packet 0x40 - Request Quest Log
/// <para>Request the character's quest log for the current act.</para>
/// </summary>
void AwesomO::RequestQuestLog() throw(...)
{
	unsigned char buffer[1] = { 0x40 };

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x41 - Respawn
/// <summary>
/// Game Client Packet 0x41 - Respawn
/// <para>Restart in town (when dead.)</para>
/// </summary>
void AwesomO::Respawn() throw(...)
{
	unsigned char buffer[1] = { 0x41 };

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x49 - Waypoint Interact
/// <summary>
/// Game Client Packet 0x49 - Waypoint Interact
/// </summary>
void AwesomO::WaypointInteract(unsigned int uid, WaypointDestination::WaypointDestination destination) throw(...)
{
	unsigned char buffer[9] = { 0x49,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)destination, 0, 0, 0
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x4B - Request Reassign
/// <summary>
/// Game Client Packet 0x4B - Request Reassign
/// <para>Requests a PlayerReassign (GS 0x15) from server.</para>
/// <para>Sent when the client is out of sync to request the current position on server.</para>
/// <para>This is also used to bring pets along when using a waypoint or portal, with a NPC UnitType.</para>
/// </summary>
void AwesomO::RequestReassign(UnitType::UnitType unitType, unsigned int meUID) throw(...)
{
	unsigned char buffer[9] = { 0x4B,
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(meUID), (unsigned char)(meUID >> 8), (unsigned char)(meUID >> 16), (unsigned char)(meUID >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x4F - Click Button
/// <summary>
/// Game Client Packet 0x4F - Click Button
/// <para>Click on a UI button.</para>
/// </summary>
void AwesomO::ClickButton(GameButton::GameButton button, unsigned int complement) throw(...)
{
	unsigned char buffer[7] = { 0x4F, 
		(unsigned char)button, (unsigned char)((unsigned int)button >> 8), 
		(unsigned char)(complement >> 16), (unsigned char)(complement >> 24), (unsigned char)(complement), (unsigned char)(complement >> 8) 
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x50 - Drop Gold
/// <summary>
/// Game Client Packet 0x50 - Drop Gold
/// <para>Remove gold from inventory and drop it on the ground.</para>
/// </summary>
void AwesomO::DropGold(unsigned int meUID, unsigned int amount) throw(...)
{
	unsigned char buffer[9] = { 0x50, 
		(unsigned char)(meUID), (unsigned char)(meUID >> 8), (unsigned char)(meUID >> 16), (unsigned char)(meUID >> 24),
		(unsigned char)(amount), (unsigned char)(amount >> 8), (unsigned char)(amount >> 16), (unsigned char)(amount >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x51 - Set Skill Hotkey
/// <summary>
/// Game Client Packet 0x51 - Set Skill Hotkey
/// <para>Assigns or unassigns a skill to a hotkey.</para>
/// </summary>
void AwesomO::SetSkillHotkey(SkillType::SkillType skill, unsigned int slot, unsigned int itemUID) throw(...)
{
	unsigned char buffer[9] = { 0x51,
		(unsigned char)(skill), (unsigned char)((unsigned short)skill >> 8),
		(unsigned char)(slot), (unsigned char)((unsigned short)slot >> 8),
		(unsigned char)(itemUID), (unsigned char)(itemUID >> 8), (unsigned char)(itemUID >> 16), (unsigned char)(itemUID >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x58 - Close Quest
/// <summary>
/// Game Client Packet 0x58 - Close Quest
/// <para>Notifies D2GS that a quest's completion animation in pannel was viewed.</para>
/// <para>Sent when the quest pannel is opened.</para>
/// </summary>
/// <remarks>
/// Length: 3
/// </remarks>
void AwesomO::CloseQuest(QuestType::QuestType quest) throw(...)
{
	unsigned char buffer[3] = { 0x58,
		(unsigned char)(quest), (unsigned char)((unsigned short)quest >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x59 - Go To Town Folk
/// <summary>
/// Game Client Packet 0x59 - Go To Town Folk
/// <para>Notify the server of intention to interact with a town folk.</para>
/// <para>First pack sent when clicking on a town folk, before running / walking toward it before actual interaction.</para>
/// </summary>
void AwesomO::GoToTownFolk(UnitType::UnitType unitType, unsigned int uid, unsigned int x, unsigned int y) throw(...)
{
	unsigned char buffer[17] = { 0x59,
		(unsigned char)unitType, 0, 0, 0,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)(x), (unsigned char)(x >> 8), (unsigned char)(x >> 16), (unsigned char)(x >> 24),
		(unsigned char)(y), (unsigned char)(y >> 8), (unsigned char)(y >> 16), (unsigned char)(y >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x5D - Set Player Relation
/// <summary>
/// Game Client Packet 0x5D - Set Player Relation
/// </summary>
void AwesomO::SetPlayerRelation(PlayerRelationType::PlayerRelationType relation, bool value, unsigned int uid) throw(...)
{
	unsigned char buffer[7] = { 0x5D, 
		(unsigned char)relation, (unsigned char)(value ? 1 : 0), 
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x5E - Party Request
/// <summary>
/// Game Client Packet 0x5E - Party Request
/// </summary>
void AwesomO::PartyRequest(PartyAction::PartyAction action, unsigned int playerUID) throw(...)
{
	unsigned char buffer[6] = { 0x5E,
		(unsigned char)action, 
		(unsigned char)(playerUID), (unsigned char)(playerUID >> 8), (unsigned char)(playerUID >> 16), (unsigned char)(playerUID >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x5F - Update Position
/// <summary>
/// Game Client Packet 0x5F - Update Position
/// </summary>
void AwesomO::UpdatePosition(unsigned int x, unsigned int y) throw(...)
{
	unsigned char buffer[5] = { 0x5F, 
		(unsigned char)(x), (unsigned char)(x >> 8),
		(unsigned char)(y), (unsigned char)(y >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x60 - Switch Weapons
/// <summary>
/// Game Client Packet 0x60 - Switch Weapons
/// <para>Toogle the active weapon tab.</para>
/// </summary>
void AwesomO::SwitchWeapons() throw(...)
{
	unsigned char buffer[1] = { 0x60 };

	bool weaponset = me->weaponset;

	while (weaponset == me->weaponset)
	{
		/*if (weaponset)
		{
			Chat("Switching to primary weapon");
		}
		else
		{
			Chat("Switching to secondary weapon");
		}*/

		game->WriteClient(buffer, sizeof(buffer));

		for (int i = 0; i < 15 && weaponset == me->weaponset; i++)
		{
			Sleep(100);
		}
	}
}
#pragma endregion
#pragma region Packet 0x61 - Change Merc Equipment
/// <summary>
/// Game Client Packet 0x61 - Change Merc Equipment
/// <para>Equip or unequip merc item.</para>
/// </summary>
void AwesomO::ChangeMercEquipment(EquipmentLocation::EquipmentLocation location, bool unequip) throw(...)
{
	unsigned char buffer[3] = { 0x61, 
		(unsigned char)location, (unsigned char)((unsigned short)location >> 8)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x62 - Resurrect Merc
/// <summary>
/// Game Client Packet 0x62 - Resurrect Merc
/// <para>Resurrect your current mercenary at a slaver town folk.</para>
/// </summary>
void AwesomO::ResurrectMerc(unsigned int dealerUID) throw(...)
{
	unsigned char buffer[5] = { 0x62, 
		(unsigned char)(dealerUID), (unsigned char)(dealerUID >> 8), (unsigned char)(dealerUID >> 16), (unsigned char)(dealerUID >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x63 - Inventory Item To Belt
/// <summary>
/// Game Client Packet 0x63 - Inventory Item To Belt
/// <para>Move an item (potion or scroll) directly from inventory to belt.</para>
/// </summary>cket 0x49 - Waypoint Interact
/// </summary>
void AwesomO::InventoryItemToBelt(unsigned int uid) throw(...)
{
	unsigned char buffer[5] = { 0x63, 
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24)
	};

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x66 - Warden Response
/// <summary>
/// Game Client Packet 0x66 - Warden Response
/// <para>Sent in reply to GS 0xAE; Warden check's response.</para>
/// </summary>
void AwesomO::WardenResponse(unsigned char * data) throw(...)
{
	game->WriteClient(data, sizeof(data));
}
#pragma endregion
#pragma region Packet 0x68 - Game Logon Request
/// <summary>
/// Game Client Packet 0x68 - Game Logon Request
/// <para>This is the first packet sent to D2GS, using info from RS (0x04 - JoinGame)</para>
///	<para>This is the main packet for the D2GS logon where you need to send your char Information to the game server.
///	This packet must be 37 bytes long, so if your char name is not 15 letters in lengh, you will need to add the correct number of bytes after your Char name.
///	Your char name should be inbedded to the start of the following hex strand:
///	Code: 00000000B5D6779A81B36F4B00000000
///	The strand must be 16 bytes long and contain your character name with a null terminator.
///	The server will not answer this packet if it is not 37 bytes long, doesn't contain the D2GS hash or D2GS token or doesn't contain your char name.</para>
/// </summary>
void AwesomO::GameLogonRequest(unsigned int d2GShash, unsigned short d2GSToken, CharacterClass::CharacterClass charClass, unsigned int version, std::string name) throw(...)
{
	/*
	unsigned char buffer[9] = { 0x68,
		(unsigned char)(uid), (unsigned char)(uid >> 8), (unsigned char)(uid >> 16), (unsigned char)(uid >> 24),
		(unsigned char)destination, 0, 0, 0
	};

	game->WriteClient(buffer, sizeof(buffer));
	*/
}
#pragma endregion
#pragma region  0x69 - Exit Game
/// <summary>
/// Game Client Packet 0x69 - Exit Game
/// <para>Notify D2GS that you are leaving the game.</para>
/// <para>NOTE: Upon receiving this packet, D2 will send a WM_CLOSE message to it's main window.
/// If the "ExitGame" flag is not set first it will not just the leave the game, but close Diablo II !</para>
/// </summary>
void AwesomO::ExitGame()
{
	if (!exiting)
	{
		exiting = true;
		
		LOG(logINFO, this) << "Exiting Game";

		unsigned char buffer[1] = { 0x69 };

		game->WriteClient(buffer, sizeof(buffer));

		if (controller)
		{
			char buffer[1] = {ControllerEvent::EXIT_GAME};
			controller->Write(buffer, sizeof(buffer));
		}
	}
}
#pragma endregion
#pragma region Packet 0x6B - Enter Game
/// <summary>
/// Game Client Packet 0x6B - Enter Game
/// <para>Sent after successful logon. Server will then send all the game's information.</para>
/// </summary>
void AwesomO::EnterGame() throw(...)
{
	unsigned char buffer[1] = { 0x6B };

	game->WriteClient(buffer, sizeof(buffer));
}
#pragma endregion
#pragma region Packet 0x6D - Ping
/// <summary>
/// Game Client Packet 0x6D - Ping
/// <para>Should be sent every 5 to 7 seconds to stop from dropping from the game.</para>
/// </summary>
void AwesomO::Ping(unsigned int tickCount, unsigned long long unknown) throw(...)
{
	unsigned char buffer[13] = { 0x6D, 
		(unsigned char)(tickCount), (unsigned char)(tickCount >> 8), (unsigned char)(tickCount >> 16), (unsigned char)(tickCount >> 24),
		(unsigned char)(unknown), (unsigned char)(unknown >> 8), (unsigned char)(unknown >> 16), (unsigned char)(unknown >> 24), 
		(unsigned char)(unknown >> 32), (unsigned char)(unknown >> 40), (unsigned char)(unknown >> 48), (unsigned char)(unknown >> 56)
	};

	game->WriteClient(buffer, sizeof(buffer));

}
#pragma endregion

void AwesomO::Chat(const char* format, ...) throw(...)
{
	const char* name = "�c8Awesom-O�c0";
	bool whisper = false;
	char text[4096];

	va_list arguments;
	va_start(arguments, format);
	vsprintf_s(text, sizeof(text), format, arguments);
	va_end(arguments);	

	LOG(logINFO, this) << "Chat: " << text;

	//TODO if (getManager() == NULL || getManager()->_chat)
//	if (getGame(false)->_manager == NULL || getManager(false)->settings->get(INI::Chat))
//	{
		int length = static_cast<int>(strlen(text) + strlen(name)) + 12;
		unsigned char * buffer = new unsigned char[length];
		int offset = 0;

		buffer[offset++] = 0x26;
		buffer[offset++] = whisper ? 0x02 : 0x01;
		buffer[offset++] = 0x00;
		buffer[offset++] = 0x02;
		buffer[offset++] = 0x00;
		buffer[offset++] = 0x00;
		buffer[offset++] = 0x00;
		buffer[offset++] = 0x00;
		buffer[offset++] = 0x00;
		buffer[offset++] = whisper ? 0x01 : 0x05;

		for (int i = 0; name[i] != '\0'; i++)
		{
			buffer[offset++] = name[i];
		}
		buffer[offset++] = 0x00;

		for (int i = 0; text[i] != '\0'; i++)
		{
			buffer[offset++] = text[i];
		}
		buffer[offset++] = 0x00;

		game->WriteServer(buffer, length);

		delete[] buffer;
//	}
}
