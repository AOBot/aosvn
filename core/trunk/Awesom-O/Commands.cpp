#include "Commands.h"

Commands::Commands()
{
	locked = false;
}

void Commands::push_front(Command command)
{
	cs.Lock();
	list.push_front(command);
	cs.Unlock();
}

void Commands::push_back(Command command)
{
	cs.Lock();
	list.push_back(command);
	cs.Unlock();
}

Command Commands::pop_front()
{
	cs.Lock();
	Command result = list.front();
	list.pop_front();
	cs.Unlock();
	return result;
}

int Commands::size()
{
	cs.Lock();
	int result = list.size();
	cs.Unlock();
	return result;
}

void Commands::lock()
{
	locked = true;
}

void Commands::unlock()
{
	locked = false;
}

bool Commands::isLocked()
{
	bool lockable = true;

	cs.Lock();
	if (list.begin() != list.end())
	{
		lockable = list.front().lockable;
	}
	cs.Unlock();

	return locked && lockable;
}
