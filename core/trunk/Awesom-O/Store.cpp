#include "Store.h"
#include "Log.h"

Store::Store()
{
}

Store::~Store()
{
}

void Store::add(ITEM item) throw(...)
{
	cs.Lock();
	list.push_back(item);
	cs.Unlock();
}

void Store::clear() throw(...)
{
	cs.Lock();
	list.clear();
	cs.Unlock();
}

ITEM Store::findBaseItem(std::string code) throw(...)
{
	ITEM item;
	item.uid = 0;
	cs.Lock();

	for (std::list<ITEM>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->baseItem->code.find(code) == 0 && (item.baseItem == NULL || item.baseItem->code <= iter->baseItem->code))
		{
			item = *iter;
		}
	}

	cs.Unlock();
	return item;
}



std::vector<ITEM> Store::gamble() throw(...)
{
	cs.Lock();
	std::vector<ITEM> vector(list.size());
	copy(list.begin(), list.end(), vector.begin());
	cs.Unlock();
	return vector;	
}

std::vector<ITEM> Store::buy() throw(...)
{
	return Store::gamble();	
}
