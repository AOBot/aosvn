#pragma once

#include "CriticalSection.h"
#include <list>
#include "d2data/SkillType.h"

//tolua_begin
struct SKILL
{
	SkillType::SkillType skill;
	char baseLevel;
};
//tolua_end

class Skills { // tolua_export

private:
	CriticalSection cs;
	std::list<SKILL> list;
	
public:
	Skills();
	~Skills();
	//tolua_begin
	void add(SKILL skill);
	bool find(SkillType::SkillType skill);
	void remove(SkillType::SkillType skill);
	int getSkillLevel(SkillType::SkillType skill);
};
// tolua_end
