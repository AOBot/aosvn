#include "Controller.h"
#include "AwesomO.h"

Controller::~Controller()
{
	if (awesomo)
	{
		LOG(logINFO, awesomo) << "Controller for " << awesomo->owner << " disconnected";
		awesomo->controller = NULL;
	}
}

void Controller::Process()
{
	//char ffs = reinterpret_cast<char>(awesomo->owner);
	switch (read[0])
	{
		case 0x01:
		{
			const char * owner = static_cast<const char*>(read + 1);

			for (size_t i = 0; i < ao.size(); i++)
			{
				if (strcmp(ao[i]->owner, owner) == 0)
				{
					awesomo = ao[i];
				}
			}

			if (awesomo)
			{
				if (awesomo->controller)
				{
					LOG(logINFO, awesomo) << "Controller for " << owner << " already connected";

					write[0] = ControllerEvent::ALREADY_CONNECTED;
					write_length = 1;
					read_length = 0;
					return;
				}
				else
				{
					awesomo->controller = this;
				}
			}
			else
			{
				awesomo = new AwesomO(owner);
				awesomo->controller = this;
				ao.push_back(awesomo);
			}

			LOG(logINFO, awesomo) << "Controller for " << owner << " connected";

			char buffer[] = {ControllerEvent::CONNECTION_SUCCESSFUL};
			Write(buffer, 1);
		}
		break;
		case 0x02:
		{
			if (awesomo->game)
			{
				awesomo->commands->push_back(Command("aobot.lua", "Routine", true));
				awesomo->Create(false);
			}
		}
		break;
		case 0x03:
		{
			const char * data = static_cast<const char*>(read + 1);
			int length = strlen(data) + 1;
			char * buffer = new char(length);
			buffer[0] = ControllerEvent::CONTROLLER_MESSAGE;
			strcpy(buffer + 1, data);

			for (size_t i = 0; i < ao.size(); i++)
			{
				Controller * controller = ao[i]->controller;

				if (controller)
				{
					controller->Write(buffer, length);
				}
			}

			delete buffer;
		}
		break;
		case 0x04:
		{
			const char * name = static_cast<const char*>(read + 1);
			const char * value = static_cast<const char*>(read + 1 + strlen(name) + 1); //added +1 to skip the \o char when reading or value was null
			
			if (awesomo)
			{
				LOG(logINFO, awesomo) << "Controler set variable " << name << " to " << value;
				awesomo->vars[name] = value;
			}
		}
		break;
		case 0x05:
		{
			const char * file = static_cast<const char*>(read + 1);
			const char * function = static_cast<const char*>(read + 1 + strlen(file) + 1); // referenced above code and hopefully works and makes perfect sense too

			if (awesomo)
			{
				LOG(logINFO, awesomo) << "Controler running file " << file << ", function " << function << ".";
				awesomo->commands->push_back(Command(file, function, true));
				awesomo->Create(false);
			}
		}
		break;
		case 0x06:
		{
			if (awesomo)
			{
				LOG(logINFO, awesomo) << "Controler is exiting game";
				awesomo->ExitGame();
			}
		}
		break;
		case 0x07:
		{
			const char * name = static_cast<const char*>(read + 1);

			if (awesomo)
			{
				const char * value = awesomo->vars[name].c_str();

				LOG(logINFO, awesomo) << "Sending variable " << name << " to with value " << value << " to controller";
				
				int length = 2 + strlen(value);
				char * buffer = new char(length);
				strcpy(buffer + 1, value);
				Write(buffer, length);
				delete buffer;
			}
		}
		break;
		case 0x08:
		{
			awesomo->ReleaseKeys();
			const char * key = static_cast<const char*>(read + 1);
			const char * keyver = static_cast<const char*>(read + 1 + strlen(key) + 1);
			int ver = atoi(keyver);
			if (awesomo)
			{
				LOG(logINFO, awesomo) << "Adding d2key : " << key << " version : " << ver << " (" << awesomo->owner << ")";
				if (awesomo->keyManager->AddKey(key))
				{
					LOG(logINFO, awesomo) << "d2key succesfully added";
				}
				else 
				{
					LOG(logINFO, awesomo) << "d2key failed : " << key << "Complete packet : " << read;
				}
			}
		}
		break;
		case 0x09:
		{
			if (awesomo)
			{
				LOG(logINFO, awesomo) << "Releasing and clearing d2keys";
				awesomo->ReleaseKeys();
				awesomo->keyManager->ClearKeys();
			}
		}
		break;
		case 0x10:  // sends a chat message
		{
			const char * msg = static_cast<const char*>(read + 1);
			if (awesomo->game)
				awesomo->SendMessageA(msg, GameMessageType::GameMessage);

		}
		break;
		case 0x12:
		{
			if (awesomo)
			{
				LOG(logINFO, awesomo) << "Releasing and clearing d2keys";
				awesomo->keyManager->ReleaseKeys();
			}
		}
		break;
	}

	read_length = 0;
}

void Controller::Write(const char * buffer, int length)
{
	writing.Lock();
	int* len = reinterpret_cast<int*>(write + write_length);
	write_length += sizeof(int);
	*len = length;
	memcpy(write + write_length, buffer, length);
	write_length += length;
	writing.Unlock();
}
