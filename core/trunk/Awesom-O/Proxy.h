#ifndef PROXY_H
#define PROXY_H

#include <iostream>
#include <winsock2.h>
#include <vector>

#include "Common.h"
#include "Connection.h"
#include "AwesomO.h"
#include "SocksManager.h"

class AwesomO;

class Proxy
{
public:
	Connection * server;
	Connection * client;
	
	AwesomO * awesomo;

	virtual int OnRelayDataToServer() = 0;
	virtual int OnRelayDataToClient() = 0;

//	void Write(Connection * conn, const char * buffer, int length)
//	{
//		conn->writing.Lock();
//		memcpy(conn->write + conn->write_length, buffer, length);
//		conn->write_length += length;
//		conn->writing.Unlock();
//	}

	virtual void WriteClient(const unsigned char * buffer, int length)
	{
		client->Write((char *)buffer, length);
	}

	virtual void WriteServer(const unsigned char * buffer, int length)
	{
		server->Write((char *)buffer, length);
	}

	virtual void error()
	{
		LOG(logERROR, awesomo) << "socket error";
	}

public:
	Proxy(SOCKET s, SOCKET c)
	{
		server = new Connection(s);
		client = new Connection(c);
		awesomo = NULL;
	}

	Proxy(SOCKET s)
	{
		server = new Connection(s);
		client = NULL;
		awesomo = NULL;
	}

	virtual ~Proxy();
	SOCKET new_client(int port, char * address, Sock * sock);
};

typedef std::vector<Proxy *> ProxyVector;
extern ProxyVector pConnections;
extern SocksManager * socksManager;

#endif //PROXY_H
