#pragma once

#include "CriticalSection.h"
#include <list>
#include "Item.h"
#include "PotionType.h"

struct itemPriority
{
	int priority;
	ITEM item;

	itemPriority()
	{
		this->priority = 0;
	}

	itemPriority(int priority, ITEM item)
	{
		this->priority = priority;
		this->item = item;
	}

	bool operator < (const itemPriority &item) const
	{
		return priority < item.priority;
	}

	bool operator > (const itemPriority &item) const
	{
		return priority > item.priority;
	}
};

class Items { // tolua_export

private:
	CriticalSection cs;
	std::list<ITEM> list;
	
public:
	Items();
	~Items();
	//tolua_begin
	void add(ITEM item);
	void remove(unsigned int uid);
	ITEM findPot(PotionType::PotionType type, short x, short y);
	std::vector<ITEM> findItemInRadius(short x, short y, int radius);
	bool isOnGround(unsigned int uid);
	ITEM findByCode(std::string code);
	std::vector<ITEM> get();
	static PotionType::PotionType GetPotType(std::string code);
};
// tolua_end
