#pragma once

#include <list>
#include <string>
#include <fstream>

struct Sock
{
	char ip[16];
	int port;
	int count;
};

class SocksManager
{
private:
	std::list<Sock *> list;
	std::fstream file;

public:
	SocksManager(const char * socks_file);
	~SocksManager();

	Sock * get();
	void rotate();
	bool empty();
	void clear();
	void add(std::string line);
};
