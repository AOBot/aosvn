#include "AwesomO.h"

int AwesomO::OnRelayDataToServer(unsigned char * data, int size)
{
	const unsigned char packetId = data[0];

	switch (packetId)
	{
		case 0x15: // Chat
		{
			const char* text = reinterpret_cast<const char*>(data + 3);

			if (text[0] == '.')
			{
				set("message", text);
				Event("chat", false);
				return 0;
			}
		}
		break;

		case 0x6D: // Ping
		{
			pingsent = GetTickCount();
		}
		break;

		case 0x5F: //UpdatePosition
		{
			//if (maps->inTown())
				//return 0;
		}
		break;

		//case 0x03: // Run To Location
		case 0x05: // CastLeftSkill
		case 0x06: // CastLeftSkillOnTarget
		case 0x07: // CastLeftSkillOnTargetStopped
		case 0x08: // RecastLeftSkill
		case 0x09: // RecastLeftSkillOnTarget
		case 0x0a: // RecastLeftSkillOnTargetStopped

		case 0x0c: // CastRightSkill
		case 0x0d: // CastRightSkillOnTarget
		case 0x0e: // CastRightSkillOnTargetStopped
		case 0x0f: // RecastRightSkill
		case 0x10: // RecastRightSkillOnTarget
		case 0x11: // RecastRightSkillOnTargetStopped

		case 0x13: // Unit Interact
		case 0x18: // Drop Item To Container
		case 0x19: // Pick Item From Container
		case 0x1A: // Equip Item
		case 0x1C: // Unequip Item
		case 0x20: // Use Inventory Item
		case 0x23: // Add Belt Item
		case 0x24: // Remove Belt Item
		case 0x25: // Swap Belt Item
		case 0x26: // Use Belt Item
		case 0x3C: // Select Skill
		case 0x49: // WaypointInteract
		//case 0x5F: // Update Position
		case 0x60: // Switch Weapons
		{
					//char buffer[] = { MapHackEvent::OBJECTS };
					//awesomo->maphack->Write(buffer, 1);
			if (Event("pissedoff", true))
			{
				return 0;
			}
		}
		break;
	}

	return size;
}