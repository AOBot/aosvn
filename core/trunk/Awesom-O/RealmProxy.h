#ifndef REALMPROXY_H
#define REALMPROXY_H

#include "Connection.h"
#include "Proxy.h"

class RealmProxy : public Proxy
{
public:
	bool magic;

	virtual int OnRelayDataToServer();
	virtual int OnRelayDataToClient();

public:
	RealmProxy(SOCKET s) : Proxy(s), magic(false)
	{ 
		LOG(logINFO, awesomo) << "created realm proxy";
	}

	~RealmProxy();
};





RealmProxy::~RealmProxy()
{
	if (awesomo)
	{
		awesomo->realm = NULL;

		if (awesomo->controller)
		{
			char buffer[1] = {ControllerEvent::REALM_DESTRUCTOR};
			awesomo->controller->Write(buffer, sizeof(buffer));
		}
	}

	LOG(logINFO, awesomo) << "deleted realm proxy";
}

int RealmProxy::OnRelayDataToServer()
{
//	printf("RC: ");
//
//	for (int i = 0; i < server->read_length; i++)
//	{
//		printf("%02x ", (unsigned char) server->read[i]);
//	}
//	printf("\n");

	if (server->read[0] == 0x01)
	{
		unsigned char hash[64];
		memcpy(hash, server->read + 4, 64);
		
		for (size_t i = 0; i < ao.size(); i++)
		{
			if (memcmp(ao[i]->chat_hash, hash, 64) == 0)
			{
				awesomo = ao[i];

				if (awesomo->realm)
				{
					LOG(logINFO, awesomo) << "realm should be null";
				}

				awesomo->realm = this;

				SOCKET c = new_client(awesomo->realm_port, awesomo->realm_address, awesomo->sock);

				if (c == SOCKET_ERROR)
				{
					LOG(logERROR, NULL) << "can't connect to realm server...";
					return 1;
				}

				client = new Connection(c);

				LOG(logINFO, awesomo) << "connected realm proxy";
			}
		}
	}

	// CharacterLogonRequest
	if (awesomo && server->read[2] == 0x07)
	{
		char character[20];
		strcpy_s(character, sizeof(character), server->read + 3);
		awesomo->character = character;
		LOG(logINFO, awesomo) << "Character name set to " << awesomo->character.c_str();
	}

	if (client == NULL)
	{
		return 0;
	}

	// write the data to the socket
	client->Write(server->read, server->read_length);

    // Everything got sent, so take a shortcut on clearing buffer.
	server->read_length = 0;

	return 0;
}

int RealmProxy::OnRelayDataToClient()
{
//	printf("RS: ");
//
//	for (int i = 0; i < client->read_length; i++)
//	{
//		printf("%02x ", (unsigned char) client->read[i]);
//	}
//	printf("\n");

	if (client == NULL)
	{
		return 1;
	}

	unsigned short packet_size = client->read_length;

//	if (magic)
//	{
		packet_size = *reinterpret_cast<const unsigned short*>(client->read); 

		if (packet_size > client->read_length)
		{
			//printf("\nincomplete realm packet %d %d\n", packet_size, client->read_length);
			return 0;
		}
//	}
//	else
//	{
//		magic = true;
//		packet_size = 1;
//	}

	if (awesomo)
	{
		switch (client->read[2])
		{
			case 0x01: // Realm Startup Response
			{
				int result = *reinterpret_cast<const int *>(client->read + 3);

				if (result == 0x7f) // Temporary IP Ban
				{
					LOG(logINFO, awesomo) << "Temporary IP Ban";

					if (awesomo && awesomo->controller)
					{
						char buffer[1] = {ControllerEvent::TEMPORARY_IP_BAN};
						awesomo->controller->Write(buffer, sizeof(buffer));
					}
				}
			}
			break;
			case 0x03: // Create Game Response
			{
				if (client->read[9] == 0x1f) // Game Already Exists
				{
					LOG(logINFO, awesomo) << "Game Already Exists";

					if (awesomo && awesomo->controller)
					{
						char buffer[1] = {ControllerEvent::GAME_ALREADY_EXISTS};
						awesomo->controller->Write(buffer, sizeof(buffer));
					}
				}
			}
			break;
			case 0x04: // Join Game Response
			{
				const unsigned int result = *reinterpret_cast<const unsigned int *>(client->read + 17);

				switch (result)
				{
					case 0x00: // Join game successful
					{
						LOG(logINFO, awesomo) << "Successfully joined the game";

						if (awesomo && awesomo->controller)
						{
							char buffer[1] = {ControllerEvent::JOIN_GAME_SUCCESSFUL};
							awesomo->controller->Write(buffer, sizeof(buffer));
						}
					}
					break;
					case 0x2a: // Game does not exist
					{
						LOG(logINFO, awesomo) << "The game does not exist";

						if (awesomo && awesomo->controller)
						{
							char buffer[1] = {ControllerEvent::GAME_DOES_NOT_EXISTS};
							awesomo->controller->Write(buffer, sizeof(buffer));
						}
					}
					break;
					default:
					{
						LOG(logINFO, awesomo) << "Failed to join the game";

						if (awesomo && awesomo->controller)
						{
							char buffer[1] = {ControllerEvent::FAILED_TO_JOIN};
							awesomo->controller->Write(buffer, sizeof(buffer));
						}
					}
					break;
				}
			}
			break;
			case 0x05: //GameList thingie
				{					
					std::string gamename,description;
					gamename.assign(client->read, client->read_length);
					if (gamename[9] == '\x00')
						break;

					std::string game = gamename.substr(14, gamename.find('\x00', 14) - 14);

					if (gamename[15 + game.length()] != '\x00')
						description = gamename.substr(15 + game.length(), gamename.find('\x00',15 + game.length()) - (15 + game.length()));
					else 
						description = '\00';
					
					 
					if (awesomo && awesomo->controller) {
						std::string buffer;

						buffer += ControllerEvent::GAME_LIST;
						buffer += '\x00';
						buffer += game;
						buffer += '\x00';
						buffer += gamename[9];
						buffer += '\x00';
						buffer += description;

						awesomo->controller->Write(buffer.c_str(),buffer.length());
					}					
				}
				break;
			case 0x07: // Character Logon Response
			{
				const unsigned int result = *reinterpret_cast<const unsigned int *>(client->read + 3);

				if (result == 0x00)
				{
					//0x00: Success
					//0x46: Player not found
					//0x7A: Logon failed
					//0x7B: Character expired

					if (awesomo && awesomo->controller)
					{
						char buffer[1] = {ControllerEvent::LOGON_SUCCESS};
						awesomo->controller->Write(buffer, sizeof(buffer));
					}
				}
			}
			break;
			case 0x19: // Character Select
			{
				if (awesomo && awesomo->controller)
				{
					// send the amount of chars to the controller
					unsigned short listed = *reinterpret_cast<const unsigned int *>(client->read + 9);
					//LOG(logDEBUG, awesomo) << "Found " << listed << " characters";
					char buffer_ch[2] = {ControllerEvent::CHAR_COUNT, (char)listed};
					awesomo->controller->Write(buffer_ch, sizeof(buffer_ch));

					char buffer[1] = {ControllerEvent::CHARACTER_SELECT};
					awesomo->controller->Write(buffer, sizeof(buffer));
				}
			}
			break;
		}
	}



	// need to patch the realm packet so client connects thru the proxy
	if (client->read[2] == 0x04)
	{
		// extract game server and game result
		unsigned int* serverIp = reinterpret_cast<unsigned int *>(client->read + 9);
		unsigned int* result = reinterpret_cast<unsigned int *>(client->read + 17);

		// only bother to patch the packet if game creation request was successful
		if (*result == 0)
		{
			// compute the address of the proxy
			//char hostName[256];
			//gethostname(hostName, sizeof(hostName));
			//hostent* host = gethostbyname(hostName);

			// store off the original and patched addresses
			int oldServerIp = *serverIp;
			//int newServerIp = *reinterpret_cast<int*>(host->h_addr_list[0]);

			// convert server addresses from numerical to string form
			char newServerAddress[16];
			strcpy_s(newServerAddress, inet_ntoa(*reinterpret_cast<in_addr*>(&newServerIp)));
			strcpy_s(awesomo->game_address, inet_ntoa(*reinterpret_cast<in_addr*>(&oldServerIp)));

			// patch the packet with the modified data
			*serverIp = newServerIp;

			// save the hash and token
			memcpy(awesomo->realm_hash, client->read + 13, 4);
			memcpy(awesomo->realm_token, client->read + 5, 2);
		}
		else
		{
			LOG(logERROR, awesomo) << "Game server logon denied (" << *result << ")";
		}
	}




	// write the data to the socket
	server->Write(client->read, packet_size);

	if (packet_size == client->read_length)
	{
        // Everything got sent, so take a shortcut on clearing buffer.
		client->read_length = 0;
    }
    else
	{
        // We sent part of the buffer's data.  Remove that data from
        // the buffer.
		client->read_length -= packet_size;
		memmove(client->read, client->read + packet_size, client->read_length);
    }

	return 0;
}

#endif //REALMPROXY_H
