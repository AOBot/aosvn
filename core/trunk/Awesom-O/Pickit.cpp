#include "Pickit.h"

Pickit::Pickit()
{
}

Pickit::~Pickit()
{
}

void Pickit::add(ITEM item, int priority)
{
	cs.Lock();
	queue.push(itemPriority(priority, item));
	cs.Unlock();
}

ITEM Pickit::pop()
{
	cs.Lock();
	ITEM item = queue.top().item;
	queue.pop();
	cs.Unlock();
	return item;
}

bool Pickit::empty()
{
	cs.Lock();
	bool result = queue.empty();
	cs.Unlock();
	return result;
}
