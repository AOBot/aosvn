#pragma once

#include <windows.h>
#include <map>

//tolua_begin
namespace ResistType
{
	enum ResistType
	{
		Damage = 0, Magic, Fire, Lightning, Cold, Poison
	};
};

namespace MonsterMod
{
	enum MonsterMod
	{
		none = 0,
		rndname,
		hpmultiply,
		light,
		leveladd,
		strong,
		fast,
		curse,
		resist,
		fire,
		poisondead,
		durieldead,
		bloodraven,
		rage,
		spcdamage,
		partydead,
		champion,
		lightning,
		cold,
		hireable,
		scarab,
		killself,
		questcomplete,
		poisonhit,
		thief,
		manahit,
		teleport,
		spectralhit,
		stoneskin,
		multishot,
		aura,
		goboom,
		firespike_explode,
		suicideminion_explode,
		ai_after_death,
		shatter_on_death,
		ghostly,
		fanatic,
		possessed,
		berserk,
		worms_on_death,
		always_run_ai,
		lightningdeath,
	};
};
// tolua_end

struct ResistInfo
{
	WORD npcid;
	short res[3][6];
};

struct ItemStatCost
{
	WORD id;
	WORD SendBits;
	WORD SendParamBits;
};

const int NPC_GFX_TYPE_NUM = 16;

struct NpcGfxType
{
	DWORD id;
	BYTE gfxTypeItemCount[NPC_GFX_TYPE_NUM];
};

struct SupUnique
{
	char name[30];
	WORD classId;
	WORD suId;
};

struct ClassIdName
{
	DWORD classId;
	char name[28];//max length 26
};

const char *umodNames[];

class MonsterInfo{
public:
	MonsterInfo();
	~MonsterInfo();
	void Init();
	static MonsterInfo* instance(BYTE difficulty);
	bool getMonsterResist(WORD npcid, ResistInfo *resInfo);
	bool getNameByClassId(WORD npcid, char *name);
	bool getGfxTypeInfo(WORD npcid, BYTE gfxTypeInfo[16]);
	bool getSuperUniqueName(WORD suid, char name[30]);
	bool getItemStatCost(WORD id, ItemStatCost *costInfo);
private:
	std::map<WORD, ResistInfo> resistData_;
	std::map<WORD, std::string> classIdName_;
	std::map<WORD, NpcGfxType> npcGfxType_;
	std::map<WORD, SupUnique> superUniques_;
	std::map<WORD, ItemStatCost> itemStatCost_;

	bool _init;

	bool loadResistInfo();
	bool loadClassIdNameFile();
	bool loadGfxTypeFile();
	bool loadSuperUniqueFile();
	bool loadItemStatCostFile();
};
