#ifndef AWESOMO_H
#define AWESOMO_H

class AwesomO { // tolua_export

public:

	//tolua_begin

	char owner[20];
	std::string account;
	std::string character;

	char realm_address[16];
	char game_address[16];

	char gamename[20];
	char password[20];

	time_t start_time;
	bool exiting;

	unsigned char difficulty;
	bool expansion;

	int gamenumber;

	Commands * commands;

	Players * players;
	Objects * objects;
	Warps * warps;
	Npcs * npcs;
	Skills * skills;
	Store * store;
	Items * items;
	Pickit * pickit;
	Files * files;

	POINT getDistantPoint(int px, int py, int x, int y, int dist);

	void Tasks();

	void SendController(std::string message);
	void SendController(std::string message, char id);

	Me * me;
	Merc * merc;

	int baallevel;
	int preattack;

	unsigned long ping;
	unsigned long pingsent;
	unsigned long lagpause;
	bool lagcheck;

	Maps * maps;

	void Sleep(unsigned long miliseconds);
	unsigned long GetTimeInGame();

	void EnableEvents();
	void DisableEvents();

	bool WaitForPacket(unsigned char packetid, int timeout);
	void InitWaitingItem(unsigned int uid);
	bool WaitForItem(int timeout);
	void InitWaitingState(StateType::StateType state);
	bool WaitForState(int timeout);
	void InitWaitingPacket(unsigned char packetid); // use this to set the packetId
	bool WaitForInitPacket(int timeout); // and this to actually wait for the packet

	void set(std::string name, std::string value);
	void set(std::string name, int value);

	std::string getString(std::string name);
	int getNumber(std::string name);

	void Run(std::string file, std::string function);
	void RunNow(std::string file, std::string function);
	void Abort();
	bool IsRunning();

	// GC
	void WalkToLocation(unsigned short x, unsigned short y);
	void WalkToTarget(UnitType::UnitType unitType, unsigned int uid);
	void RunToLocation(unsigned short x, unsigned short y);
	void RunToTarget(UnitType::UnitType unitType, unsigned int uid);
	void CastLeftSkill(unsigned short x, unsigned short y);
	void CastLeftSkillOnTarget(UnitType::UnitType unitType, unsigned int uid);
	void CastLeftSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid);
	void RecastLeftSkill(unsigned short x, unsigned short y);
	void RecastLeftSkillOnTarget(UnitType::UnitType unitType, unsigned int uid);
	void RecastLeftSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid);
	void CastRightSkill(unsigned short x, unsigned short y);
	void CastRightSkillOnTarget(UnitType::UnitType unitType, unsigned int uid);
	void CastRightSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid);
	void RecastRightSkill(unsigned short x, unsigned short y);
	void RecastRightSkillOnTarget(UnitType::UnitType unitType, unsigned int uid);
	void RecastRightSkillOnTargetStopped(UnitType::UnitType unitType, unsigned int uid);
	void UnitInteract(UnitType::UnitType unitType, unsigned int uid);
	void SendOverheadMessage(std::string message);
	void SendOverheadMessage(const char* format);
	void SendMessage(const char* message, GameMessageType::GameMessageType type, const char* recipient);
	void SendMessage(const char* message, GameMessageType::GameMessageType type);
	void SendMessage(const char* message);
	void Speak(const char* text);
	void PickItem(unsigned int requestID, unsigned int uid, bool toCursor);
	void DropItem(unsigned int uid);
	void DropItemToContainer(unsigned int uid, unsigned int x, unsigned int y, ItemContainerGC::ItemContainerGC container);
	void PickItemFromContainer(unsigned int uid);
	void EquipItem(unsigned int uid, EquipmentLocation::EquipmentLocation location);
	void UnequipItem(EquipmentLocation::EquipmentLocation location);
	void SwapEquippedItem(unsigned int uid, EquipmentLocation::EquipmentLocation location);
	void SwapContainerItem(unsigned int SubjectUID, unsigned int ObjectUID, unsigned int x, unsigned int y);
	void UseInventoryItem(unsigned int uid, unsigned int meX, unsigned int meY);
	void StackItems(unsigned int subjectUID, unsigned int objectUID);
	void AddBeltItem(unsigned int uid, unsigned int x);
	void RemoveBeltItem(unsigned int uid);
	void SwapBeltItem(unsigned int oldItemUID, unsigned int newItemUID);
	void UseBeltItem(unsigned int uid, bool toMerc, unsigned int unknown9);
	void UseBeltItem(unsigned int uid, bool toMerc);
	void UseBeltItem(unsigned int uid);
	void IdentifyItem(unsigned int itemUID, unsigned int scrollUID);
	void EmbedItem(unsigned int subjectUID, unsigned int objectUID);
	void ItemToCube(unsigned int itemUID, unsigned int cubeUID);
	void TownFolkInteract(UnitType::UnitType unitType, unsigned int uid);
	void TownFolkCancelInteraction(UnitType::UnitType unitType, unsigned int uid);
	void DisplayQuestMessage(unsigned int uid, unsigned int message);
	void BuyItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost, bool fillStack);
	void BuyItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost);
	void GambleItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost);
	void SellItem(unsigned int dealerUID, unsigned int itemUID, unsigned int cost);
	void CainIdentifyItems(unsigned int uid);
	void TownFolkRepair(unsigned int dealerUID);
	void TownFolkRepair(unsigned int dealerUID, unsigned int itemUID);
	void HireMercenary(unsigned int dealerUID, unsigned int mercID);
	void IdentifyGambleItem(unsigned int uid);
	void TownFolkMenuSelect(TownFolkMenuItem::TownFolkMenuItem selection, unsigned int uid, unsigned int unknown9);
	void TownFolkMenuSelect(TownFolkMenuItem::TownFolkMenuItem selection, unsigned int uid);
	void IncrementAttribute(StatType::StatType attribute, unsigned char count);
	void IncrementAttribute(StatType::StatType attribute);
	void IncrementSkill(SkillType::SkillType skill);
	bool SelectSkill(SkillType::SkillType skill, SkillHand::SkillHand hand, unsigned int chargedItemUID);
	bool SelectSkill(SkillType::SkillType skill, SkillHand::SkillHand hand);
	bool SelectSkill(SkillType::SkillType skill);
	void HoverUnit(unsigned int uid);
	void SendCharacterSpeech(GameSound::GameSound speech);
	void RequestQuestLog();
	void Respawn();
	void WaypointInteract(unsigned int uid, WaypointDestination::WaypointDestination destination);
	void RequestReassign(UnitType::UnitType unitType, unsigned int meUID);
	void ClickButton(GameButton::GameButton button, unsigned int complement);
	void DropGold(unsigned int meUID, unsigned int amount);
	void SetSkillHotkey(SkillType::SkillType skill, unsigned int slot, unsigned int itemUID);
	void SetSkillHotkey(SkillType::SkillType skill, unsigned int slot);
	void CloseQuest(QuestType::QuestType quest);
	void GoToTownFolk(UnitType::UnitType unitType, unsigned int uid, unsigned int x, unsigned int y);
	void SetPlayerRelation(PlayerRelationType::PlayerRelationType relation, bool value, unsigned int uid);
	void PartyRequest(PartyAction::PartyAction action, unsigned int playerUID);
	void UpdatePosition(unsigned int x, unsigned int y);
	void SwitchWeapons();
	void ChangeMercEquipment(EquipmentLocation::EquipmentLocation location, bool unequip);
	void ChangeMercEquipment(EquipmentLocation::EquipmentLocation location);
	void ResurrectMerc(unsigned int dealerUID);
	void InventoryItemToBelt(unsigned int uid);
	void WardenResponse(unsigned char * data);
	void GameLogonRequest(unsigned int d2GShash, unsigned short d2GSToken, CharacterClass::CharacterClass charClass, unsigned int version, std::string name);
	void ExitGame();
	void EnterGame();
	void Ping(unsigned int tickCount, unsigned long long unknown);
	void Chat(const char* format);

	// GS
	void GameLoading();
	void GameLogonSuccess();
	void LoadDone();
	void UnloadDone();
	void GameLogoutSuccess();
	void MapAdd(AreaLevel::AreaLevel area, unsigned int x, unsigned int y);
	void MapRemove(AreaLevel::AreaLevel area, unsigned int x, unsigned int y);
	void RemoveGroundUnit(UnitType::UnitType type, unsigned int uid);
	void GameHandshake(UnitType::UnitType type, unsigned int uid);
	void NPCGetHit(UnitType::UnitType type, unsigned int uid, unsigned char life, unsigned int anim);
	void PlayerMove(UnitType::UnitType type, unsigned int uid, MovementType::MovementType movement, unsigned int targetX, unsigned int targetY, unsigned char unknown12, unsigned int currentX, unsigned int currentY);
	void ReportKill(UnitType::UnitType type, unsigned int uid);
	void PlayerReassign(UnitType::UnitType type, unsigned int uid, unsigned int x, unsigned int y, bool reassign);
	void SmallGoldAdd(unsigned char amount);
	void AttributeByte(StatType::StatType stat, unsigned char value);
	void AttributeWord(StatType::StatType stat, unsigned short value);
	void AttributeDWord(StatType::StatType stat, unsigned long value);
	void GameMessage(GameMessageType::GameMessageType type, unsigned char charFlags, std::string charName, std::string message);
	void GameMessage(UnitType::UnitType type, unsigned int uid, unsigned short random, std::string message);
	void PlaySound(UnitType::UnitType unitType, unsigned int uid, GameSound::GameSound sound);
	void PlayerClearCursor(UnitType::UnitType unitType, unsigned int uid);
	void Relator1(unsigned int uid, unsigned short param1, unsigned int param2);
	void Relator2(unsigned int uid, unsigned short param1, unsigned int param2);
	void MercForHireListStart();
	void PlayerInGame(unsigned int uid, CharacterClass::CharacterClass charClass, std::string name, unsigned short level, unsigned short partyID);
	void PlayerLeaveGame(unsigned int uid);
	void PlayerCorpseVisible(bool assign, unsigned int playerUID, unsigned int corpseUID);
	void PlayerInSight(UnitType::UnitType unitType, unsigned int uid);
	void UpdateItemUI(ItemUIAction::ItemUIAction action);
	void AcceptTrade(std::string name, unsigned int uid);
	void AssignSkillHotkey(unsigned char slot, SkillType::SkillType skill, unsigned int itemUID);
	void AssignSkillHotkey(unsigned char slot, SkillType::SkillType skill);
	void UseSpecialItem(SpecialItemType::SpecialItemType action, unsigned int uid);
	void SetItemState(UnitType::UnitType ownerType, unsigned int ownerUID, unsigned int itemUID, ItemStateType::ItemStateType state);
	void AssignPlayerToParty(unsigned int uid, unsigned short partyNumber);
	void AssignPlayerCorpse(bool assign, unsigned int playerUID, unsigned int corpseUID);
	void Pong();
	void PartyMemberPulse(unsigned int uid, unsigned int x, unsigned int y);
	void SwitchWeaponSet();
	void GameOver();
	void LoadSkillOnUnit(SkillType::SkillType skill, UnitType::UnitType type, unsigned int meUID, unsigned int uid);
};
// tolua_end

#endif //AWESOMO_H
