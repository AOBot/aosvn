//tolua_begin

struct POINT
{
    int x;
    int y;
};

namespace std
{
	class vector {

		TOLUA_TEMPLATE_BIND(T, ITEM, POINT, OBJECT, PLAYER, PET, NPC, AreaLevel::AreaLevel, MagicPrefixType::MagicPrefixType, MagicSuffixType::MagicSuffixType, StatBase, std::string, bool)

		void clear();
		int size() const;

		const T& operator[](int index) const;
		T& operator[](int index);
		void push_back(T val);
		const T& back() const;
		T& back();

		vector();
		~vector();
	};
};
// tolua_end
