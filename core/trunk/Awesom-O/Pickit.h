#pragma once

#include "CriticalSection.h"
#include <queue>
#include "Items.h"

class Pickit { // tolua_export

private:
	CriticalSection cs;
	std::priority_queue<itemPriority> queue;
	
public:
	Pickit();
	~Pickit();
	//tolua_begin
	void add(ITEM item, int priority);
	ITEM pop();
	bool empty();
};
// tolua_end