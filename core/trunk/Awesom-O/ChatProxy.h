#ifndef CHATPROXY_H
#define CHATPROXY_H

#include <string>

#include "Connection.h"
#include "Proxy.h"
#include "CdKey.h"
#include "CdKeyManager.h"

#pragma pack(push)
#pragma pack(1)
	struct LogonRequestKey
	{
		int length;
		int productId;
		int publicValue;
		int unknown;
		unsigned char hash[20];
	};

	struct LogonRequestHeader
	{
		int clientToken;
		int gameVersion;
		int gameHash;
		int keyCount;
		int usingSpawn;
	};
#pragma pack(pop)

extern bool stealth;

class ChatProxy : public Proxy
{
public:
	bool magic;
	int token;

	virtual int OnRelayDataToServer();
	virtual int OnRelayDataToClient();

	virtual void error();

	Sock * sock;

public:
	ChatProxy(SOCKET s) : Proxy(s), magic(false)
	{
		LOG(logINFO, awesomo) << "created chat proxy";
		sock = NULL;
	}

	~ChatProxy();
};

ChatProxy::~ChatProxy()
{
	if (awesomo)
	{
		awesomo->ReleaseKeys();
		awesomo->chat = NULL;

		if (awesomo->controller)
		{
			char buffer[1] = {ControllerEvent::CHAT_DESTRUCTOR};
			awesomo->controller->Write(buffer, sizeof(buffer));
		}
	}

	LOG(logINFO, awesomo) << "deleted chat proxy";
}

void ChatProxy::error()
{
	for (unsigned int i = 0; i < ao.size(); i++)
	{
		if ((ao[i]->controller) && !(ao[i]->chat))
		{
			LOG(logDEBUG2, awesomo) << "Cant connect to chat server.";
			char buffer[1] = {ControllerEvent::CANT_CONNECT};
			ao[i]->controller->Write(buffer, sizeof(buffer));
			//char ffs = reinterpret_cast<char>(awesomo->owner);
			//awesomo->keyManager->ReleaseKeys(awesomo->owner);
			ao[i]->ReleaseKeys();
		}
	}

	LOG(logERROR, awesomo) << "chat socket error";
}

int ChatProxy::OnRelayDataToServer()
{
//	printf("CC: %-5d", server->read_length);
//
//	for (int i = 0; i < server->read_length; i++)
//	{
//		printf("%02x ", (unsigned char) server->read[i]);
//	}
//	printf("\n");

	unsigned short packet_size = server->read_length;
	bool dead = false;

	// I hate this bs
	if ((unsigned char) server->read[0] == 0x01)
	{
		packet_size = 1;
	}

	if ((unsigned char) server->read[0] == 0xff)
	{
		packet_size = *reinterpret_cast<const unsigned short*>(server->read + 2); 

		if (packet_size > server->read_length)
		{
			//printf("\nincomplete chat server packet %d %d\n", packet_size, server->read_length);
			return 0;
		}

		if (packet_size < server->read_length)
		{
			//LOG(logINFO, awesomo) << "multi packet";
		}

		// NotifyJoin
		if (server->read[1] == 0x22)
		{
			strcpy_s(awesomo->gamename, sizeof(awesomo->gamename), server->read + 12);
			strcpy_s(awesomo->password, sizeof(awesomo->password), server->read + 13 + strlen(awesomo->gamename));

			LOG(logINFO, awesomo) << "gamename " << awesomo->gamename << ", password " << awesomo->password;
		}

		// BnetAuthRequest
		if (server->read[1] == 0x51)
		{
			const char * owner = reinterpret_cast<const char *>(server->read + 129);

			for (size_t i = 0; i < ao.size(); i++)
			{
				if (strcmp(ao[i]->owner, owner) == 0)
				{
					awesomo = ao[i];

					if (awesomo->chat)
					{
						LOG(logINFO, awesomo) << "chat should be null";
					}

					awesomo->chat = this;
					LOG(logINFO, awesomo) << "found existing awesomo for " << owner;
				}
			}

			if (awesomo == NULL)
			{
				awesomo = new AwesomO(owner, sock);
				ao.push_back(awesomo);
				LOG(logINFO, awesomo) << "created new awesomo for OWNERNAME : " << owner;
			}
			else
			{
				awesomo->sock = sock;
			}

			LogonRequestHeader * header = reinterpret_cast<LogonRequestHeader *>(server->read + 4);
			LogonRequestKey * keys = reinterpret_cast<LogonRequestKey *>(server->read + 4 + sizeof(LogonRequestHeader));

			for (int i = 0; i < header->keyCount; i++)
			{
				LogonRequestKey* request = keys + i;
				if(request->productId > 0)
				{
					LOG(logDEBUG2, NULL) << awesomo->owner << " Acquiring Key! (auto) ";
					const CdKey* key = awesomo->AcquireKey(request->productId);
					if (key != 0 && key->IsValid())
					{
						key->ComputeHash(header->clientToken, token, request->hash, sizeof(request->hash));
						request->publicValue = key->GetPublicValue();
						
					}
				}
				else
				{
					LOG(logINFO, awesomo) << "Found invalid product. Is installation key valid?";

					if(i == 0)
					{
						LOG(logDEBUG2, NULL) << awesomo->owner << " Acquiring Key! (classic) ";
						const CdKey* key = awesomo->AcquireKey(6); // request a d2 classic key
						
						if (key != 0 && key->IsValid())
						{
							key->ComputeHash(header->clientToken, token, request->hash, sizeof(request->hash));
							request->publicValue = key->GetPublicValue();
							request->length = 16; // make the packet valid
							request->productId = 6; // d2
						}
					}
					else if(i > 0)
					{
						LOG(logDEBUG2, NULL) << awesomo->owner << " Acquiring Key! (lod) ";
						const CdKey* key = awesomo->AcquireKey(10); // request a d2 lod key
						
						if (key != 0 && key->IsValid())
						{
							key->ComputeHash(header->clientToken, token, request->hash, sizeof(request->hash));
							request->publicValue = key->GetPublicValue();
							request->length = 16; // make the packet valid
							request->productId = 10; // lod
						}
					}
				}
			}
		}

		// BnetLogonRequest
		if (awesomo && server->read[1] == 0x3a)
		{
			char account[20];
			strcpy_s(account, sizeof(account), server->read + 32);
			awesomo->account = account;
			LOG(logINFO, awesomo) << "Account name set to " << awesomo->account.c_str();
		}

		// JoinChannel
		if (awesomo && awesomo->controller && server->read[1] == 0x0c)
		{
			char buffer[1] = {ControllerEvent::JOIN_CHANNEL};
			awesomo->controller->Write(buffer, sizeof(buffer));
		}

		if (stealth && (server->read[1] == 0x1c || server->read[1] == 0x22))
		{
			// new to kill this packet (stealth)
			dead = true;
		}
	}
	else
	{
		//printf("unrecognized chat server packet format\n");
	}

	if (client == NULL)
	{
		sock = socksManager->get();
		SOCKET c = new_client(6112, inet_ntoa(*(struct in_addr *)*realm_host->h_addr_list), sock);
		int tries = 5;
		
		while (sock && c == SOCKET_ERROR && tries > 0)
		{
			sock = socksManager->get();
			c = new_client(6112, inet_ntoa(*(struct in_addr *)*realm_host->h_addr_list), sock);
			tries--;
		}

		if (c == SOCKET_ERROR)
		{
			for (unsigned int i = 0; i < ao.size(); i++)
			{
				if (!ao[i]->chat && ao[i]->controller)
				{
					//char buffer[1] = {ControllerEvent::CANT_CONNECT};
					LOG(logERROR, NULL) << "CANT CONNECT (CHAT SOCKET ERROR)"; //I dont think this will ever happen.
					//ao[i]->controller->Write(buffer, sizeof(buffer));
				}
			}

			LOG(logERROR, NULL) << "can't connect to chat server... ip ban?";
			return 1;
		}

		client = new Connection(c);
	}

	if (!dead)
	{
		// if the packet isn't dead
		// write the data to the socket
		client->Write(server->read, packet_size);
	}

	if (packet_size == server->read_length)
	{
        // Everything got sent, so take a shortcut on clearing buffer.
		server->read_length = 0;
    }
    else
	{
        // We sent part of the buffer's data.  Remove that data from
        // the buffer.
		server->read_length -= packet_size;
		memmove(server->read, server->read + packet_size, server->read_length);
    }

	return 0;
}

int ChatProxy::OnRelayDataToClient()
{
//	printf("CS: %-5d", client->read_length);
//
//	for (int i = 0; i < client->read_length; i++)
//	{
//		printf("%02x ", (unsigned char) client->read[i]);
//	}
//	printf("\n");

	unsigned short packet_size = client->read_length;

	if ((unsigned char) client->read[0] == 0xff)
	{
		packet_size = *reinterpret_cast<const unsigned short*>(client->read + 2); 

		if (packet_size > client->read_length)
		{
			//printf("\nincomplete chat client packet %d %d\n", packet_size, client->read_length);
			return 0;
		}

		if (packet_size < client->read_length)
		{
			//LOG(logINFO, awesomo) << "multi packet";
		}

		// BnetConnectionResponse
		if (client->read[1] == 0x50)
		{
			token = *reinterpret_cast<const int*>(client->read + 8);
		}

		// BnetAuthResponse
		if (awesomo && awesomo->controller && client->read[1] == 0x51)
		{
			char buffer[2] = {ControllerEvent::BNET_AUTH_RESPONSE, client->read[4]};
			awesomo->controller->Write(buffer, sizeof(buffer));
		}

		// BnetLogonResponse
		if (awesomo && awesomo->controller && client->read[1] == 0x3a)
		{
			char buffer[2] = {ControllerEvent::BNET_LOGON_RESPONSE, client->read[4]};
			awesomo->controller->Write(buffer, sizeof(buffer));
		}

		// RealmLogonResponse
		if (awesomo && client->read[1] == 0x3e)
		{
			// extract realm server ip and port
			unsigned int* serverIp = reinterpret_cast<unsigned int*>(client->read + 20);
			unsigned short* serverPort = reinterpret_cast<unsigned short*>(client->read + 24);

			// store off the old values for server ip and port
			unsigned int oldServerIp = *serverIp;
			unsigned int oldServerPort = *serverPort;

			// compute the address of the proxy
			//char hostName[256];
			//gethostname(hostName, sizeof(hostName));
			//hostent* host = gethostbyname(hostName);

			// redirect realm server ip and port to local interface
			//int newServerIp = *reinterpret_cast<int*>(host->h_addr_list[0]);
			unsigned int newServerPort = htons(6113);
								
			// convert server addresses from numerical to string form
			char newServerAddress[16];
			strcpy_s(newServerAddress, inet_ntoa(*reinterpret_cast<in_addr*>(&newServerIp)));

			awesomo->realm_port = ntohs(oldServerPort);
			strcpy_s(awesomo->realm_address, inet_ntoa(*reinterpret_cast<in_addr*>(&oldServerIp)));

			// patch the packet with the modified data
			*serverPort = newServerPort;
			*serverIp = newServerIp;

			// save hash
			memcpy(awesomo->chat_hash, client->read + 4, 16);
			memcpy(awesomo->chat_hash + 16, client->read + 28, 48); 
		}





//BS: 0x0F ChatEvent; Event: WhisperSent; Ping: 297; Account: innumerable; Name: ManyMany; Message: sdgsdgsdg; CharacterTitle: None
//BS: 58   0f 3b 00 0a 00 00 00 00 00 00 00 29 01 00 00 00 00 00 00 0d f0 ad ba 0d f0 ad ba 4d 61 6e 79 4d 61 6e 79 2a 69 6e 6e 75 6d 65 72 61 62 6c 65 00 73 64 67 73 64 67 73 64 67 00

//BS: 0x0F ChatEvent; Event: ReceiveWhisper; Ping: 297; Account: innumerable; Name: ManyMany; Message: sdgsdgsdg; CharacterTitle: None
//BS: 58   0f 3b 00 04 00 00 00 00 00 00 00 29 01 00 00 00 00 00 00 0d f0 ad ba 0d f0 ad ba 4d 61 6e 79 4d 61 6e 79 2a 69 6e 6e 75 6d 65 72 61 62 6c 65 00 73 64 67 73 64 67 73 64 67 00
		
//BS: 0x0F ChatEvent; Event: ChannelJoin; Ping: 47; Account: cold-reaver; Name: Whirlin-Reaver; Realm: USEast; Client: Diablo2LoD; ClientVersion: 132; CharacterType: Barbarian; CharacterLevel: 91; CharacterFlags: Died, Expansion, Ladder, Realm; CharacterAct: 666; CharacterTitle: Patriarch
//BS: 114  0f 73 00 02 00 00 00 00 00 00 00 2f 00 00 00 00 00 00 00 0d f0 ad ba 0d f0 ad ba 57 68 69 72 6c 69 6e 2d 52 65 61 76 65 72 2a 63 6f 6c 64 2d 72 65 61 76 65 72 00 50 58 32 44 55 53 45 61 73 74 2c 57 68 69 72 6c 69 6e 2d 52 65 61 76 65 72 2c 84 80 5b 02 02 03 01 04 04 ff 03 03 ff 05 ff ff ff ff ff ff ff ff ff ff ff 5b e8 9e ff ff 05 ff ff 00

//BS: 0x0F ChatEvent; Event: ChannelMessage; Ping: 31; Account: TheDestroyer017; Name: HotSexyGen; Message: FT:13/13 Treks 4 Pul(or gores), Raven Lore 4 Hr wisper me fast------>TheDestroyer017; CharacterTitle: None
//BS: 139  0f 8c 00 05 00 00 00 00 00 00 00 1f 00 00 00 00 00 00 00 0d f0 ad ba 0d f0 ad ba 48 6f 74 53 65 78 79 47 65 6e 2a 54 68 65 44 65 73 74 72 6f 79 65 72 30 31 37 00 46 54 3a 31 33 2f 31 33 20 54 72 65 6b 73 20 34 20 50 75 6c 28 6f 72 20 67 6f 72 65 73 29 2c 20 52 61 76 65 6e 20 4c 6f 72 65 20 34 20 48 72 20 77 69 73 70 65 72 20 6d 65 20 66 61 73 74 2d 2d 2d 2d 2d 2d 3e 54 68 65 44 65 73 74 72 6f 79 65 72 30 31 37 00

		// ChatEvent
		if (awesomo && /*awesomo->controller &&*/ client->read[1] == 0x0f)
		{
			const BYTE* bChatData = new const BYTE[packet_size];
			memcpy((void *)bChatData, client->read, packet_size);

			DWORD dwEvent =	*reinterpret_cast<const DWORD*>( bChatData + 4 );
			DWORD dwFlags =	*reinterpret_cast<const DWORD*>( bChatData + 8 );
			DWORD dwPing = *reinterpret_cast<const DWORD*>( bChatData + 12 );

			int iPos = ( 7 * sizeof( DWORD ) );
			char *szSpeaker = new char[MAX_PATH];
			szSpeaker = (char *)bChatData + iPos;
			iPos += (int)( strlen( szSpeaker ) + 1 );
			char *szSaid = new char[MAX_PATH];
			szSaid = (char *)bChatData + iPos;

			if( dwEvent == 0x04 // Received whisper message
				|| dwEvent == 0x05 // Received chat message
				|| dwEvent == 0x17 // Received Emote
				)
			{
				int iLen = (int)strlen( szSpeaker ) + 1;

				char *szAccount = new char[MAX_PATH];
				char *szCharacter = new char[MAX_PATH];
				strncpy( szAccount, szSpeaker, iLen + 1 );
				strncpy( szCharacter, szSpeaker, iLen + 1 );

				char* szAccountTmp = strpbrk( szAccount, "*" );
				szAccountTmp++;
				szAccount = szAccountTmp;
				
				char* szCharacterTmp = strtok( szCharacter, "*" );
				szCharacter = szCharacterTmp;

				LOG(logINFO, awesomo) << "account - " << szAccount << ", character - " << szCharacter << ", said - " << szSaid;

				if (awesomo->controller)
				{
					int length = strlen(szAccount) + 4 + strlen(szCharacter) + 4 + strlen(szSaid) + 4;
					char * buffer = new char[length];
					int offset = 0;

					buffer[offset++] = ControllerEvent::CHAT_EVENT;

					for (int i = 0; szAccount[i] != '\0'; i++)
					{
						buffer[offset++] = szAccount[i];
					}
					buffer[offset++] = 0x00;

					for (int i = 0; szCharacter[i] != '\0'; i++)
					{
						buffer[offset++] = szCharacter[i];
					}
					buffer[offset++] = 0x00;

					for (int i = 0; szSaid[i] != '\0'; i++)
					{
						buffer[offset++] = szSaid[i];
					}
					buffer[offset++] = 0x00;
					
					awesomo->controller->Write(buffer, length);
					delete [] buffer;
				}

				szAccount = 0; if( szAccount ) delete [] szAccount; // hmmmmmmmmmmmm... should prob fix?
				szCharacter = 0; if( szCharacter ) delete [] szCharacter; // hmmmmmmmmmmmm... should prob fix?
			}

			szSaid = 0; if( szSaid ) delete [] szSaid; // hmmmmmmmmmmmm... should prob fix?
			szSpeaker = 0; if( szSpeaker ) delete [] szSpeaker; // hmmmmmmmmmmmm... should prob fix?
			delete [] bChatData;
		}
	}
	else
	{
		//printf("unrecognized chat client packet format\n");
	}



	// write the data to the socket
	server->Write(client->read, packet_size);

	if (packet_size == client->read_length)
	{
        // Everything got sent, so take a shortcut on clearing buffer.
		client->read_length = 0;
    }
    else
	{
        // We sent part of the buffer's data.  Remove that data from
        // the buffer.
		client->read_length -= packet_size;
		memmove(client->read, client->read + packet_size, client->read_length);
    }

	return 0;
}







#endif //CHATPROXY_H
