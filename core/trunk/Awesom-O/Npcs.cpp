#include "Npcs.h"
#include "Log.h"

long CalculateDistance(long x1, long y1, long x2, long y2);
long CalculateDistance(const POINT& pt1, const POINT& pt2);

Npcs::Npcs()
{
}

Npcs::~Npcs()
{
}

void Npcs::add(NPC npc) throw(...)
{
	cs.Lock();

	if(isResurrecter(npc.id) || npc.minion || npc.unique || npc.superunique || npc.ghostly)
		list.push_front(npc);
	else
		list.push_back(npc);
	
	cs.Unlock();
}

void Npcs::remove(unsigned int uid) throw(...)
{
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter = list.erase(iter);
			break;
		}
	}

	cs.Unlock();
}

NPC Npcs::findByNpcCode(NPCCode::NPCCode id, int max) throw(...)
{
	NPC npc;
	npc.uid = 0;
	cs.Lock();
	std::list<NPC>::iterator pos = list.end();
		
	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->id == id && iter->mode == NPCMode::Alive && !iter->HasState(StateType::Conversion) && !iter->HasState(StateType::Revive))
		{
			if (!max || iter->count < max)
			{
				pos = iter;
				npc = *pos;
			}
		}
	}
		
	if (pos != list.end())
	{
		pos->count++;
	}
	cs.Unlock();
	return npc;
}

NPC Npcs::find(unsigned int uid, int max) throw(...)
{
	NPC npc;
	npc.uid = 0;
	cs.Lock();
	std::list<NPC>::iterator pos = list.end();
	
	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			if (!max || iter->count < max)
			{
				pos = iter;
				npc = *pos;
			}
		}
	}

	if (pos != list.end())
	{
		pos->count++;
	}
	cs.Unlock();
	return npc;
}

NPC Npcs::findInRadius(short x, short y, short radius, int max) throw(...)
{
	NPC npc;
	npc.uid = 0;
	cs.Lock();
	std::list<NPC>::iterator pos = list.end();	

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (isMonster(iter->id) && iter->mode == NPCMode::Alive && iter->getDistance(x, y) <= radius && !iter->isPet && !iter->hasowner&& !iter->HasState(StateType::Conversion) && !iter->HasState(StateType::Revive))

		{
			if (!max || iter->count < max)
			{
				iter->count++;
				npc = *iter;
				break;
			}
		}
	}
	
	if(!npc.uid)
	{
		LOG(logDEBUG4, NULL) << "No NPC found";
	}

	cs.Unlock();
	return npc;
}
struct ClDst
{
     bool operator()(NPC npc1, NPC npc2)
     {
          return npc1.distance < npc2.distance;
     }
};


NPC Npcs::findBySuperUnique(SuperUnique::SuperUnique uniqueid, int max) throw(...)
{
	NPC npc;
	npc.uid = 0;
	cs.Lock();
	
	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->superunique && iter->uniqueid == uniqueid)
		{
			if (!max || iter->count < max)
			{
				iter->count++;
				npc = *iter;
				break;
			}
		}
	}

	cs.Unlock();
	return npc;
}

NPC Npcs::findHero() throw(...)
{
	NPC npc;
	npc.uid = 0;
	cs.Lock();
	
	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if ((iter->ghostly || iter->hero || iter->minion || iter->unique || iter->superunique) && isMonster(iter->id) && iter->mode == NPCMode::Alive&& !iter->HasState(StateType::Conversion) && !iter->HasState(StateType::Revive))
		{
			//cerr << " Flags: " << iter->flags << "uniqueID:" << iter->uniqueid << endl;
			iter->count++;
			npc = *iter;
			break;
		}
	}

	cs.Unlock();
	return npc;
}

NPC Npcs::findHeroInRadius(short x, short y, short radius) throw(...)
{
	NPC npc;;
	npc.uid = 0;
	std::vector<NPC> res;
	cs.Lock();

	for(std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		iter->distance = iter->getDistance(x,y);
		if ((iter->ghostly || iter->hero || iter->minion || iter->unique || iter->superunique) && isMonster(iter->id) && iter->mode == NPCMode::Alive && iter->distance <= radius&& !iter->HasState(StateType::Conversion) && !iter->HasState(StateType::Revive))
		{
			res.push_back(*iter);
		}
	}
	if (res.size() > 0) {
		std::sort(res.begin(), res.end(), ClDst());
		npc = *res.begin();
	}

	cs.Unlock();

	return npc;
	
}

NPC Npcs::findCorpseInRadius(short x, short y, short radius) throw(...)
{
	NPC  npc;
	npc.uid = 0;
	std::vector<NPC> res;
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		iter->distance = iter->getDistance(x,y);
		if (iter->mode == NPCMode::Dead && iter->redeemed == false && iter->distance <= radius)
		{
			res.push_back(*iter);
		}
	}
	if (res.size() > 0) {
		std::sort(res.begin(), res.end(), ClDst());
		npc = *res.begin();
	}

	cs.Unlock();
	
	return npc;
	
}

int Npcs::countInRadius(short x, short y, short radius) throw(...)
{
	int result = 0;
	cs.Lock();
	
	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (isMonster(iter->id) && iter->mode == NPCMode::Alive && iter->getDistance(x, y) <= radius && !iter->hasowner && !iter->isPet&& !iter->HasState(StateType::Conversion) && !iter->HasState(StateType::Revive))
		{
			result++;
		}
	}

	cs.Unlock();
	return result;
}


void Npcs::update(NPC npc) throw(...)
{
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == npc.uid)
		{
			iter = list.erase(iter);
			break;
		}
	}

	list.push_back(npc);
	cs.Unlock();
}

void Npcs::update(unsigned int uid, short x, short y) throw(...)
{	
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->distance = iter->getDistance(x,y);
			iter->x = x;
			iter->y = y;
			break;
		}
	}

	cs.Unlock();
}
void Npcs::update(unsigned int uid, NPCMode::NPCMode mode) throw(...)
{	
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->mode = mode;
			break;
		}
	}

	cs.Unlock();
}

void Npcs::update(unsigned int uid, short x, short y, char life) throw(...)
{
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->distance = iter->getDistance(x,y);
			iter->x = x;
			iter->y = y;
			if(life < 0) life = life ^ 0xFF; // make it positive (making this positive before we apply it to the npc...
			iter->life = life;
			iter->life_percent = static_cast<unsigned char>((static_cast<float>(life))/128*100);
			break;
		}
	}

	cs.Unlock();
}

void Npcs::update(unsigned int uid, short x, short y, char life, NPCMode::NPCMode mode) throw(...)
{
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->distance = iter->getDistance(x,y);
			iter->x = x;
			iter->y = y;
			if(life < 0) life = life ^ 0xFF; // make it positive
			iter->life = life;
			iter->life_percent = static_cast<unsigned char>((static_cast<float>(life))/128*100);
			iter->mode = mode;
			break;
		}
	}

	cs.Unlock();
}

void Npcs::life(unsigned int uid, char life) throw(...)
{
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			if(life < 0) life = life ^ 0xFF; // make it positive
			iter->life = life;
			iter->life_percent = static_cast<unsigned char>((static_cast<float>(life))/128*100);
			break;
		}
	}

	cs.Unlock();
}

void Npcs::getPoints(std::vector<POINT> &points) throw(...)
{
	points.clear();
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		POINT p;
		p.x = iter->x;
		p.y = iter->y;
		points.push_back(p);
	}

	cs.Unlock();
}

void Npcs::redeem(unsigned int uid) throw(...)
{	
	cs.Lock();

	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		if (iter->uid == uid)
		{
			iter->redeemed = true;
		}
	}

	cs.Unlock();
}

std::vector<NPC> Npcs::findAllInRadius(short x, short y, short radius) throw(...)
{
	std::vector<NPC> result;
	cs.Lock();
	
	for (std::list<NPC>::iterator iter = list.begin(); iter != list.end(); iter++)
	{
		iter->distance = iter->getDistance(x,y);
		if (isMonster(iter->id) && iter->mode == NPCMode::Alive && iter->distance <= radius && !iter->hasowner && !iter->isPet&& !iter->HasState(StateType::Conversion) && !iter->HasState(StateType::Revive))
		{
			result.push_back(*iter);
		}
	}
	
	cs.Unlock();
	return result;
}