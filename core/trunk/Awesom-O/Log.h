#pragma once

#include <sstream>
#include <iostream>
#include <fstream>
#include "CriticalSection.h"

class AwesomO;

enum TLogLevel 
{
	logERROR,
	logWARNING,
	logINFO,
	logDEBUG,
	logDEBUG1,
	logDEBUG2,
	logDEBUG3,
	logDEBUG4
};

#define LOG(level, awesomo) \
if (level > Log::ReportingLevel) ; \
else Log(awesomo).Get(level)

class Log
{
public:
	Log();
	Log(AwesomO* awesomo);
	virtual ~Log();
	std::ostringstream& Get(TLogLevel level = logINFO);

public:
	static TLogLevel ReportingLevel;
	static std::ofstream of;
	static CriticalSection cs;

protected:
	std::ostringstream os;

private:
	Log(const Log&);
	Log& operator =(const Log&);

private:
	TLogLevel messageLevel;
	AwesomO* awesomo;
};
