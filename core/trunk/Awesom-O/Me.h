#pragma once

#include "d2data/SkillType.h"
#include "Item.h"
#include "Inventory.h"
#include "Stash.h"
#include "Belt.h"
#include "Body.h"
#include "Cube.h"
#include "States.h"
#include "Pets.h"
#include "PotionType.h"
#include "d2data\Quests.h"
#include <vector>
#include <map>

class Me { // tolua_export

public:

	Me();
	//tolua_begin
	unsigned int uid;
	short act;
	short x;
	short y;
	POINT lastValid;
	unsigned int gold;
	unsigned int goldbank;
	unsigned long long experience;
	unsigned long gameExperience;
	unsigned char tps;
	unsigned char ids;
	bool weaponset;
	std::vector<bool> waypoints;
	SkillType::SkillType left;
	SkillType::SkillType right;
	unsigned int beltrows;

	unsigned short life;
	unsigned short stamina;
	unsigned short baselife;
	unsigned short maxlife;

	unsigned short bolife;
	unsigned short bomana;
	unsigned short oaklife;
	unsigned short oakmana;
	unsigned short wolflife;
	unsigned short wolfmana;
	unsigned short bearlife;
	unsigned short bearmana;

	unsigned short mana;
	unsigned short basemana;
	unsigned short maxmana;

   unsigned short level;

	unsigned int corpseid;

	void setSkill(SkillHand::SkillHand hand, SkillType::SkillType skill);
	SkillType::SkillType getSkill(SkillHand::SkillHand hand);

	QuestData Quests[41];
	Waypoints wps[39];
	Inventory inventory;
	Stash stash;
	Body body;
	Belt belt;
	Cube cube;
	ITEM cursor;
	States states;
	Pets pets;
};
// tolua_end
