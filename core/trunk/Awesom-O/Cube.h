#pragma once

#include "Item.h"
#include <windows.h>
#include <vector>
#include <map>

class Cube { // tolua_export

private:
	std::map<unsigned int, ITEM> cube;
	bool map[3][4];
	bool Check(ITEM * item, int x, int y);

public:
	//tolua_begin
	unsigned int uid;

	std::vector<ITEM> get();
	POINT findLocation(ITEM item);
	void add(ITEM item);
	void remove(ITEM item);
	std::vector<ITEM> findByCode(std::string code);
};

// tolua_end
