--[[ AO General Settings ]]--

--[[ Generic Options ]]--
blockUserAction = true --> blocks most user actions from taking place in D2 (even if animation is still shown)
hotIPList = {} --> List of Hot IPs to halt bot in (DURING ANY RUN)
haltForDiabloClone = true --> halt bot in any game diablo clone spawns in (probably want to disable for public runs)

--[[ Rushing Options ]]--
rushGiveWP = false --> true to give WPs used during rush
rushSideQuests = false --> true to do extra rush sidequests
questDen = true --> Den Of Evil Quest
questCain = false --> The Search For Cain Quest
questImbue = false --> Tools Of The Trade Quest
questRadament = true --> Radament's Lair Quest
questGoldenBird = false --> The Golden Bird Quest
questTome = true --> Lam Esen's Tome Quest
questIzual = true --> The Fallen Angel Quest
questForge = false --> Hell's Forge Quest
questSocket = true --> Siege On Harrogath Quest
questAnya = true --> Prison Of Ice Quest
questAncients = true --> Rite Of Passage Quest (required for most rushes)

--[[ Logging Options ]]--
logLevel = 1 --> 1 = debug, 2 = info, 3 = errors
logDisplay = true --> on screen display of logging
logSoldItem = true --> log items sold
logDumpedItem = true --> log items dumped
logGambledItem = true --> log items gambled
logShoppedItem = true --> log items shopped
logStashedItem = true --> log items stashed
logSkippedItem = true --> log items skipped