--[[ Including ]]--
--[[
	Including files in AO is a bit tricky.  When determining if an include is needed we have to consider 
	the source of the call for that file/function.  If it was called from events it has been created in a new
	thread and also a new lua VM.  This means that there will be NO included files when the function is loaded.
	
	With this in mind, we also have to consider that AO can use up a lot of memory from this process.
	Keep includes to a minimum is of the highest importance in AO.  We would like the bot to take up as little
	memory and processing power as possible without losing out on performance or functionality.
--]]
_ScriptDir = "scripts\\"
_CharDir = "scripts\\chars\\"
_BuildsDir = "scripts\\chars\\builds\\"
_PickitDir = "scripts\\pickit\\"
_LogDir = "logs\\"..Ao.character.."\\"
_LibDir = "scripts\\libs\\"

includeSettings = function() --> character settings (and custom) includes should be done last
	--Ao:Chat("includes.lua includeSettings() called")
	protectedLoadFile(_ScriptDir.."aosettings")
	if not protectedLoadFile(_CharDir..Ao.character) then --> check on load character file
		--Ao:Chat("includes.lua set automateSettings")
		Ao:set("automateSettings", 1) --> settings file missing, lets set to create it
	else --> character loaded successfully, so lets load their build
		Ao:set("automateSettings", 0) --> do not automate any settings, we have a character file already
		os.execute("mkdir logs\\"..Ao.character)
		protectedLoadFile(_BuildsDir..build) --> load build
	end
	protectedLoadFile(_ScriptDir.."custom")
	--Ao:Chat("includes.lua includeSettings() finished")
end

includeCritical = function()
	--Ao:Chat("includes.lua includeCritical() called")
	protectedLoadFile(_LibDir.."logging")
	--Ao:Chat("includes.lua includeCritical() finished")
end

protectedLoadFile = function(filename) --> file loader with error handling
	--Ao:Chat("includes.lua loading file "..filename)
	local loader, err = loadfile(filename..".lua")
	if loader then 
		--Ao:Chat("includes.lua "..filename.." is loaded")
		loader()
		return true
	end
    logSyntaxError(filename, err)
	-- default return on error
	return false
end

logSyntaxError = function(filename, err) --> error handling
	--Ao:Chat("includes.lua logSyntaxError() called")
	local msg = err:gsub(filename..':', "")
	if msg:match("No such file or directory") then
		msg = msg:gsub("cannot open  ", "")
	elseif msg:match("not enough memory") then
		msg = msg..", collecting garbage, attempting reload"
		collectgarbage("collect")
	else msg = "line "..msg
	end
	io.output(io.open("logs\\lua_err.log", "a+"))
	io.write(os.date(), " - Error\n")
	io.write("      Failed to load: ", filename, "\n")
	io.write("      Error: ", msg, "\n")
	io.close()
	--Ao:Chat("includes.lua logSyntaxError() finished")
end

