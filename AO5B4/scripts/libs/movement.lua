--[[ Moving : including town movements, walking and teleporting ]]--
--[[
	With AO being a packet based bot, it is hard sometimes to get a real feel for how well we are tracking ourselves.  Location updates in town are sometimes
	really far apart making it hard to determine if we have gotten stuck, or veered from our path.  One of the ways we are going to try to combat this is to cast
	any spells we can in town if we have full mana.  When life and mana are updated (and mana regens pretty regularly) we are given location updates.  This of
	course will not work with every character, but when it does work it will be very helpful in keeping town location more up to date.  
	One of the goals in the production of movements for AO is to work seamless with characters that do not have teleport, and with characters that do have teleport.
	This includes characters that have teleport but do not have a lot of mana (low level sorcs for example).  We will attempt to make the process of walking and teleporting
	as smooth as possible.  Another goal will be fore a hammerdin that walks, and to cast hammers as he is moving which would be ideal for classic paladins that have to walk
	through chaos sanctuary, or any paladin that gets stuck while walking around.  
	Walk pathing is something that is pretty difficult to do, it takes a ton of consideration to get it done right.  There will a strong effort to make sure that Awesom-O is 
	a bot that is readily available for ladder resets, classic, as well as hardcore characters of any sort.  
--]]

