--[[ Events Called From Server -> Client Packets ]]--
--[[ 
	Events in AO run on a seperate thread and interrupt the bot at any time a packet is called
	that triggers the event.  It is possible to stop events from being called by using the command
	Ao:DisableEvents() in which case no events will be called until the command Ao:EnableEvents() is
	called.  Another option is to lock commands (further actions from the bots and the way that events
	work) in which case (some but not all) command calls like Ao:Run() and Ao:RunNow() will not work
	this can be done by calling Ao.commands:lock() and be removed by calling Ao.commands:unlock().
	
	It should be noted that any event that runs a function should be ran by the use of Ao:Run(file, function) which
	will load that function into the list.  Another option is to use Ao:RunNow(file, function) which will make that function
	the next function to be executed in the core. Another option is to set variable in the core by using
	Ao:set("variableName", value) value can be either a number or a string.  If a string is used then Ao:getString("variableName")
	will need to be used to retrieve the current value.  If a number is used then Ao:getNumber("variableName") will need
	to be used.  Almost all information from Diablo 2 is enumerated, which means that all the information is in reference
	to a number, so one could do something like : Ao:set("MyArea", AreaLevel.RogueEncampment) which would set MyArea
	variable in core to 1 (the enumeration for the AreaLevel Rogue Encampemnt)  All enumerations are listed on
	the AO Wiki.
--]]
--Ao:Chat("events.lua loading includes")
loadfile("scripts\\includes.lua")()
--Ao:Chat("events.lua including Settings")
includeSettings() --> includes character file, ao settings, character build, and custom.lua
includeCritical() --> logging
--Ao:Chat("events.lua settings included")

chat = function()
	Ao:Chat("events.lua chat() called")
	local msg = Ao:getString("message")
	if msg == ".start" then --> Start the bot (file = aobot.lua || function = Routine)
		Ao:Chat("events.lua .start called : Running aobot.lua->Routine()")
		return Ao:Run("aobot.lua", "Routine") --> we return because lua wont change VM out (supposedly)
	elseif msg == ".stop" then --> Stop the bot (ends entire run and cannot be started from last position)
		Ao:Chat("events.lua .stop called : Aborting AO")
		return Ao:Abort() --> again return for no VM
	elseif msg == ".rainbow" then --> print off colors (just for my enjoyment)
		Ao:Chat("�c0000000000000000000000000000000000000000")
		Ao:Chat("�c1111111111111111111111111111111111111111")
		Ao:Chat("�c2222222222222222222222222222222222222222")
		Ao:Chat("�c3333333333333333333333333333333333333333")
		Ao:Chat("�c4444444444444444444444444444444444444444")
		Ao:Chat("�c5555555555555555555555555555555555555555")
		Ao:Chat("�c6666666666666666666666666666666666666666")
		Ao:Chat("�c7777777777777777777777777777777777777777")
		Ao:Chat("�c8888888888888888888888888888888888888888")
		Ao:Chat("�c9999999999999999999999999999999999999999")
		Ao:Chat("�c::::::::::::::::::::::::::::::::::::::::")
		Ao:Chat("�c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;")
		Ao:Chat("�c,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,")
		Ao:Chat("�c\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\")
		Ao:Chat("�c!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		Ao:Chat("�c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
		Ao:Chat("�c''''''''''''''''''''''''''''''''''''''''")
		Ao:Chat("�c&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
		Ao:Chat("�c((((((((((((((((((((((((((((((((((((((((")
		Ao:Chat("�c****************************************")
		Ao:Chat("�c))))))))))))))))))))))))))))))))))))))))")
		Ao:Chat("�c++++++++++++++++++++++++++++++++++++++++")
		Ao:Chat("�c,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,")
		Ao:Chat("�c----------------------------------------")
		Ao:Chat("�c........................................")
		Ao:Chat("�c<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
		Ao:Chat("�c\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"")
		Ao:Chat("�c########################################")
	elseif msg == ".loglevel" then
		Ao:Chat(logLevel)
	end
end

mapAdd = function() --> Whenever a map comes into "d2 view" it gets loaded, this is useful for knowing when to transition during movement
	Ao:Chat("events.lua mapAdd() called")
	local area = Ao:getNumber("mapAdd")
end

mercLife = function() --> called whenever mercs life gets an update
	Ao:Chat("events.lua mercLife() called")
end

playerExit = function() --> called whenever a player exits game
	Ao:Chat("events.lua playerExit() called")
	local pName = Ao:getString("playerName")
	local pUID = Ao:getNumber("playerUID")
end

slain = function() --> called whenever we are slain by another player
	Ao:Chat("events.lua slain() called")
	local pName = Ao:getString("playerName")
	local pUID = Ao:getNumber("playerUID")
end

dClone = function() --> called whenever dclone spawns in game
	Ao:Chat("events.lua dClone() called")
end

playerJoin = function() --> called whenever a player joins the game
	Ao:Chat("events.lua playerJoin() called")
	local pUID = Ao:getNumber("playerUID")
end

lifeMana = function() --> called whenever our life is updated
	Ao:Chat("events.lua lifeMana() called")
end

levelUp = function() --> called whenever we level up
	Ao:Chat("events.lua levelUp() called")
end

playerChat = function() --> called whenever another play chats in game
	Ao:Chat("events.lua playerChat() called")
	local pName = Ao:getString("player")
	local msg = Ao:getString("message")
end

attacked = function() --> called (hopefully) whenever a monster attacks us
	Ao:Chat("events.lua attacked() called")
	local aUID = Ao:getNumber("attackeruid")
	local skill = Ao:getNumber("skill") --> could be a monsterSkill
end

mercDeath = function() --> called when merc dies
	Ao:Chat("events.lua mercDeath() called")
end

hostileInSight = function() --> called when a hostile player enters "D2 view"
	Ao:Chat("events.lua hostileInSight() called")
	local pUID = Ao:getNumber("uid")
end

playerRelation = function() --> called whenever a relationship changes with another player (invite, etc)
	Ao:Chat("events.lua playerRelation() called")
	local pUID = Ao:getNumber("uid")
	local relation = Ao:getNumber("relationship")
end

death = function() --> called when we are killed by a monster
	Ao:Chat("events.lua death() called")
end

playerDeath = function() --> called whenever another player is killed
	Ao:Chat("events.lua playerDeath() called")
end

setState = function() --> called whenever a state begins on character
	Ao:Chat("events.lua setState() called")
	local state = Ao:getNumber("state")
end

endState = function() --> called whenever a state ends on character
	Ao:Chat("events.lua endState() called")
	local state = Ao:getNumber("state")
end

pissedoff = function() --> called whenever user action should be blocked
	Ao:Chat("events.lua pissedoff() called")
end

pong = function() --> called periodically (in response to ping) its a good heartbeat ';)
	Ao:Chat("events.lua pong() called")
end