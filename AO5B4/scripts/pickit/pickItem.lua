loadfile("scripts\\includes.lua")()
IncludeSettings()
IncludeLibs()
_PRMainDir = "scripts\\pickit\\"..Settings.Inventory.Pickit.."\\"
loadfile(_PRMainDir.."gateway.lua")()
loadfile("scripts\\characters\\" ..Ao.character..".lua")()  --Dont ask me why but if its not there, we get errors... wtf???

function pickItem(item)
	local t = os.clock()
	--Ao:RequestReassign()
	Log(1, "timerpick", "reassign : "..os.clock()-t)
	if distance(Ao.me.x, Ao.me.y, item.x, item.y) > 42 then
		Log(1, "timerpick", "dist too far : "..os.clock()-t)
		return
	end
	setVersion(item)
	local priority = processItem(item, _Pick)
	local pn = tonumber(priority)
	Log(1, "timerpick", "priority check : "..os.clock()-t)
	if priority and pn > 0 then
		CallPickitEvent(item, pn*10)
		Ao:set(item.uid, getEnumKey(AreaLevel, Ao.maps:getLevel()))
		Log(1, "timerpick", "ran pick item "..os.clock()-t)
		return
	elseif KeepForCubing(item) then
		CallPickitEvent(item, 9)
		Log(1, "timerpick", "ran pick cubing "..os.clock()-t)
	elseif inventory.GoldCheck(item) then
		CallPickitEvent(item, 1)
		Log(1, "timerpick", "ran pick gold "..os.clock()-t)
	elseif inventory.ScrollCheck(item) then
		CallPickitEvent(item, 1)
		Log(1, "timerpick", "ran pick scroll "..os.clock()-t)
	elseif belt.PotCheck(item) then
		CallPickitEvent(item, 1)
		Log(1, "timerpick", "ran pick pot "..os.clock()-t)
	end
	Log(1, "timerpick", "end "..os.clock()-t)
end


function CallPickitEvent(item, priority)
    Ao.pickit:add(item, priority)
        Ao:Run("pickit\\pickit.lua", "Routine")
end
 
