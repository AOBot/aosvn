-- classic\white\armors.lua "strict"

local keepForImbue = Sockets == 0 -- "Charsi Quest"

local threeSocket =
{ priority = 3,
	goodItem =
		( Sockets == 3
			and DefensePercent >= 10
		--or Sockets == 0 -- keep weapon for imbue "Charsi Quest"
		)
}

local fourSocket =
{ priority = 3,
	goodItem =
		( Sockets == 4
			and DefensePercent >= 10
		--or Sockets == 0 -- keep weapon for imbue "Charsi Quest"
		)
}

white.armors =
{ checkStats = true,
--[[ Normal Armors
	["qui"] = keepForImbue, -- Quilted Armor
	["lea"] = keepForImbue, -- Leather Armor
	["hla"] = keepForImbue, -- Hard Leather Armor
	["stu"] = keepForImbue, -- Studded Leather
	["rng"] = threeSocket, -- Ring Mail
	["scl"] = keepForImbue, -- Scale Mail
	["brs"] = threeSocket, -- Breast Plate
	["chn"] = keepForImbue, -- Chain Mail
	["spl"] = keepForImbue, -- Splint Mail
	["ltp"] = threeSocket, -- Light Plate
	["plt"] = keepForImbue, -- Plate Mail
	["fld"] = keepForImbue, -- Field Plate
	["gth"] = fourSocket, -- Gothic Plate
	["ful"] = fourSocket, -- Full Plate Mail
	["aar"] = fourSocket, -- Ancient Armor
--]]

---[[ Exceptional Armors
	--["xui"] = keepForImbue, -- Ghost Armor
	--["xea"] = keepForImbue, -- Serpentskin Armor
	--["xla"] = keepForImbue, -- Demonhide Armor
	--["xtu"] = keepForImbue, -- Trellised Armor
	--["xcl"] = threeSocket, -- Tigulated Mail
	--["xng"] = threeSocket, -- Linked Mail
	["xrs"] = threeSocket, -- Cuirass
	--["xhn"] = threeSocket, -- Mesh Armor
	--["xpl"] = threeSocket, -- Russet Armor
	["xtp"] = threeSocket, -- Mage Plate
	--["xlt"] = threeSocket, -- Templar Coat
	--["xld"] = threeSocket, -- Sharktooth Armor
	["xth"] = fourSocket, -- Embossed Plate
	["xul"] = fourSocket, -- Chaos Armor
	["xar"] = fourSocket, -- Ornate Plate
--]]

}