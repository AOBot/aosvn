-- classic\unique\shields.lua "strict"

unique.shields =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- Shields ------------------------------------------
-----------------------------------------------------
--[[ Pelta Lunata (Buckler)
	["buc"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 38 --note: 30-40 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Umbral Disk (Small Shield)
	["sml"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 48 --note: 40-50 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Stormguild (Large Shield)
	["lrg"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 58 --note: 50-60 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Steelclash (Kite Shield)
	["kit"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 98 --note: 60-100 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Swordback Hold (Spiked Shield)
	["spk"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 58 --note: 30-60 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Bverrit Keep (Tower Shield)
	["tow"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 115 --note: 80-120 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Wall of the Eyeless (Bone Shield)
	["bsh"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 38 --note: 30-40 ed
				)
		},	
--]]
-----------------------------------------------------
---[[ The Ward (Gothic Shield)
	["gts"] = 
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or AllResist >= 50 --note: 30-50 all res
				)
		},	
--]]
-----------------------------------------------------
}