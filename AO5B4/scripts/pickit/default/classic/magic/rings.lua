-- classic\magic\rings.lua "strict"

magic.rings =
{ checkStats = true,
	["rin"] = -- Ring
		{ priority = 2, identify = true,
			goodItem = ( Unidentified or MagicFind >= 40)
		},
}