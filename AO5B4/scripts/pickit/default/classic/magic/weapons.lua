-- classic\magic\weapons.lua "strict"

----------------------------------------------------
-------------------- Settings ----------------------
----------------------------------------------------

-- Minimum Skill Level Setting
local minSkillLevel = 5

----------------- Sorceress Settings --------------------
local isGoodMagicSorcWeap =
function(item)
	local sorcSkiller = false

	sorcSkiller = 
		(ClassSkillsBonus + FireBallSkill)
		or (ClassSkillsBonus + MeteorSkill)
		or (ClassSkillsBonus + EnchantSkill)
		--or (ClassSkillsBonus + FireBoltSkill)
		--or (ClassSkillsBonus + WarmthSkill)
		--or (ClassSkillsBonus + InfernoSkill)
		--or (ClassSkillsBonus + BlazeSkill)
		--or (ClassSkillsBonus + FireWallSkill)
		--or (ClassSkillsBonus + HydraSkill)
		--or (ClassSkillsBonus + FireMasterySkill)
		or (ClassSkillsBonus + BlizzardSkill)
		or (ClassSkillsBonus + FrozenOrbSkill)
		--or (ClassSkillsBonus + IceBoltSkill)
		--or (ClassSkillsBonus + FrozenArmorSkill)
		--or (ClassSkillsBonus + FrostNovaSkill)
		--or (ClassSkillsBonus + IceBlastSkill)
		--or (ClassSkillsBonus + ShiverArmorSkill)
		--or (ClassSkillsBonus + GlacialSpikeSkill)
		--or (ClassSkillsBonus + ChillingArmorSkill)
		--or (ClassSkillsBonus + ColdMasterySkill)
		or (ClassSkillsBonus + LightningSkill)
		--or (ClassSkillsBonus + ChainLightningSkill)
		--or (ClassSkillsBonus + ChargedBoltSkill)
		--or (ClassSkillsBonus + StaticFieldSkill)
		--or (ClassSkillsBonus + TelekinesisSkill)
		--or (ClassSkillsBonus + NovaSkill)
		--or (ClassSkillsBonus + ThunderStormSkill)
		--or (ClassSkillsBonus + EnergyShieldSkill)
		--or (ClassSkillsBonus + TeleportSkill)
		--or (ClassSkillsBonus + LightningMasterySkill)
		    
	return
		( Unidentified
			or sorcSkiller >= minSkillLevel
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Paladin Settings --------------------
local isGoodMagicPalWeap =
function(item)
	local pallySkiller = false

	pallySkiller = 
		(ClassSkillsBonus + BlessedHammerSkill) 
		--or (ClassSkillsBonus + SacrificeSkill) 
		--or (ClassSkillsBonus + SmiteSkill) 
		--or (ClassSkillsBonus + HolyBoltSkill) 
		--or (ClassSkillsBonus + ZealSkill) 
		--or (ClassSkillsBonus + ChargeSkill) 
		--or (ClassSkillsBonus + VengeanceSkill)
		--or (ClassSkillsBonus + ConversionSkill)
		--or (ClassSkillsBonus + HolyShieldSkill) 
		--or (ClassSkillsBonus + FistOfTheHeavensSkill)		
		--or (ClassSkillsBonus + ConcentrationSkill) 
		--or (ClassSkillsBonus + FanaticismSkill) 
		--or (ClassSkillsBonus + ConvictionSkill) 
		--or (ClassSkillsBonus + MightSkill) 
		--or (ClassSkillsBonus + HolyFireSkill) 
		--or (ClassSkillsBonus + ThornsSkill) 
		--or (ClassSkillsBonus + BlessedAimSkill) 
		--or (ClassSkillsBonus + HolyFreezeSkill) 
		--or (ClassSkillsBonus + HolyShockSkill) 
		--or (ClassSkillsBonus + SanctuarySkill) 		
		--or (ClassSkillsBonus + RedemptionSkill) 
		--or (ClassSkillsBonus + PrayerSkill) 
		--or (ClassSkillsBonus + ResistFireSkill) 
		--or (ClassSkillsBonus + DefianceSkill) 
		--or (ClassSkillsBonus + ResistColdSkill) 
		--or (ClassSkillsBonus + CleansingSkill) 
		--or (ClassSkillsBonus + ResistLightningSkill) 
		--or (ClassSkillsBonus + VigorSkill) 
		--or (ClassSkillsBonus + MeditationSkill) 
		--or (ClassSkillsBonus + SalvationSkill)

	return
		( Unidentified
			or pallySkiller >= minSkillLevel
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Necromancer Settings --------------------
local isGoodMagicNecWeap =
function(item)
	local necroSkiller = false

	necroSkiller = 
		(ClassSkillsBonus + BoneSpearSkill)
		or (ClassSkillsBonus + BoneSpiritSkill)
		or (ClassSkillsBonus + PoisonNovaSkill)
		--or (ClassSkillsBonus + TeethSkill)
		--or (ClassSkillsBonus + BoneArmorSkill)
		--or (ClassSkillsBonus + PoisonDaggerSkill)
		--or (ClassSkillsBonus + CorpseExplosionSkill)
		--or (ClassSkillsBonus + BoneWallSkill)
		--or (ClassSkillsBonus + PoisonExplosionSkill)
		--or (ClassSkillsBonus + BonePrisonSkill)
		--or (ClassSkillsBonus + RaiseSkeletonSkill)
		--or (ClassSkillsBonus + SkeletonMasterySkill)
		--or (ClassSkillsBonus + ClayGolemSkill)
		--or (ClassSkillsBonus + GolemMasterySkill)
		--or (ClassSkillsBonus + RaiseSkeletalMageSkill)
		--or (ClassSkillsBonus + BloodgolemSkill)
		--or (ClassSkillsBonus + SummonResistSkill)
		--or (ClassSkillsBonus + IronGolemSkill)
		--or (ClassSkillsBonus + FiregolemSkill)
		--or (ClassSkillsBonus + ReviveSkill)
		--or (ClassSkillsBonus + LowerResistSkill)
		--or (ClassSkillsBonus + DecrepifySkill)
		--or (ClassSkillsBonus + AmplifyDamageSkill)
		--or (ClassSkillsBonus + DimVisionSkill)
		--or (ClassSkillsBonus + WeakenSkill)
		--or (ClassSkillsBonus + IronMaidenSkill)
		--or (ClassSkillsBonus + TerrorSkill)
		--or (ClassSkillsBonus + ConfuseSkill)
		--or (ClassSkillsBonus + LifeTapSkill)
		--or (ClassSkillsBonus + AttractSkill)

	return
		( Unidentified
			or necroSkiller >= minSkillLevel
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Non Class Settings --------------------
local isGoodMagicWeap =
function(item)
	local prefix, suffix = 0, 0

	prefix =
		count{ 
			MaxDamagePercent >= 90, 
			MaxDamagePercent >= 81 and ToHit >= 121,
			--ClassSkillsBonus >= 2,
		}
	suffix =
		count{ 
			FasterAttackRate >= 40, 
			Strength >= 9,
			MinDamage >= 14, 
			MaxDamage >= 20, 
			LifeDrainMinDamage >= 7, 
			--ManaDrainMinDamage >= 7, 
			--LowerRequirementsPercent ~= 0,
			--PreventHeal ~= 0,
            --Knockback ~= 0,
		}
	return
		( Unidentified
			or (prefix + suffix) >= 2
		)
end	
----------------- Other Settings --------------------
-- goodMagicWeapon checks Non Class Settings for good items

local goodMagicWeapon =
{ priority = 3, identify = true,
	isGoodItem = isGoodMagicWeap
}

----------------- Magic Weapons --------------------
magic.weapons =
{ checkStats = true,

---[[ Sorceress Staffs
	["Staff"] =
		{ priority = 3, identify = true,
   			isGoodItem = isGoodMagicSorcWeap
		},
--]]

---[[ Scepters
	["Scepter"] =  
		{ priority = 3, identify = true,
			isGoodItem = isGoodMagicPalWeap
		},
--]]

---[[ Wands
	["Wand"] = 
		{ priority = 3, identify = true,
   			isGoodItem = isGoodMagicNecWeap
		},
--]]

----------------------------------------
-- Axes 
----------------------------------------
--[[ Normal Axes
	["hax"] = goodMagicWeapon, -- Hand Axe
	["axe"] = goodMagicWeapon, -- Axe
	["2ax"] = goodMagicWeapon, -- Double Axe
	["mpi"] = goodMagicWeapon, -- Military Pick
	["wax"] = goodMagicWeapon, -- War Axe
	["lax"] = goodMagicWeapon, -- Large Axe
	["bax"] = goodMagicWeapon, -- Broad Axe
	["btx"] = goodMagicWeapon, -- Battle Axe
	["gax"] = goodMagicWeapon, -- Great Axe
	["gix"] = goodMagicWeapon, -- Giant Axe
--]]

--[[ Exceptional Axes
	["9ha"] = goodMagicWeapon, -- Hatchet
	["9ax"] = goodMagicWeapon, -- Cleaver
	["92a"] = goodMagicWeapon, -- Twin Axe
	["9mp"] = goodMagicWeapon, -- Crowbill
	["9wa"] = goodMagicWeapon, -- Naga
	["9la"] = goodMagicWeapon, -- Military Axe
	["9ba"] = goodMagicWeapon, -- Bearded Axe
	["9bt"] = goodMagicWeapon, -- Tabar
	["9ga"] = goodMagicWeapon, -- Gothic Axe
	["9gi"] = goodMagicWeapon, -- Ancient Axe
--]]

----------------------------------------
-- Bows
----------------------------------------
--[[ Normal Bows
	["sbw"] = goodMagicWeapon, -- Short Bow
	["hbw"] = goodMagicWeapon, -- Hunter's Bow
	["lbw"] = goodMagicWeapon, -- Long Bow
	["cbw"] = goodMagicWeapon, -- Composite Bow
	["sbb"] = goodMagicWeapon, -- Short Battle Bow
	["lbb"] = goodMagicWeapon, -- Long Battle Bow
	["swb"] = goodMagicWeapon, -- Short War Bow
	["lwb"] = goodMagicWeapon, -- Long War Bow
--]]

--[[ Exceptional Bows
	["8sb"] = goodMagicWeapon, -- Edge Bow
	["8hb"] = goodMagicWeapon, -- Razor Bow
	["8lb"] = goodMagicWeapon, -- Cedar Bow
	["8cb"] = goodMagicWeapon, -- Double Bow
	["8s8"] = goodMagicWeapon, -- Short Siege Bow
	["8l8"] = goodMagicWeapon, -- Large Siege Bow
	["8sw"] = goodMagicWeapon, -- Rune Bow
	["8lw"] = goodMagicWeapon, -- Gothic Bow
--]]

----------------------------------------
-- Crossbows
----------------------------------------
--[[ Normal Crossbows
	["lxb"] = goodMagicWeapon, -- Light Crossbow
	["mxb"] = goodMagicWeapon, -- Crossbow
	["hxb"] = goodMagicWeapon, -- Heavy Crossbow
	["rxb"] = goodMagicWeapon, -- Repeating Crossbow
--]]

--[[ Exceptional Crossbows
	["8lx"] = goodMagicWeapon, -- Arbalest
	["8mx"] = goodMagicWeapon, -- Siege Crossbow
	["8hx"] = goodMagicWeapon, -- Ballista
	["8rx"] = goodMagicWeapon, -- Chu-Ko-Nu
--]]

----------------------------------------
-- Daggers
----------------------------------------
--[[ Normal Daggers
	["dgr"] = goodMagicWeapon, -- Dagger
	["dir"] = goodMagicWeapon, -- Dirk
	["kri"] = goodMagicWeapon, -- Kris
	["bld"] = goodMagicWeapon, -- Blade
--]]

--[[ Exceptional Daggers
	["9dg"] = goodMagicWeapon, -- Poignard
	["9di"] = goodMagicWeapon, -- Rondel
	["9kr"] = goodMagicWeapon, -- Cinquedeas
	["9bl"] = goodMagicWeapon, -- Stiletto
--]]

----------------------------------------
-- Maces 
----------------------------------------
--[[ Normal Maces
	["clb"] = goodMagicWeapon, -- Club
	["spc"] = goodMagicWeapon, -- Spiked Club
	["mac"] = goodMagicWeapon, -- Mace
	["mst"] = goodMagicWeapon, -- Morning Star
	["fla"] = goodMagicWeapon, -- Flail
	["whm"] = goodMagicWeapon, -- War Hammer
	["mau"] = goodMagicWeapon, -- Maul
	["gma"] = goodMagicWeapon, -- Great Maul
--]]

--[[ Exceptional Maces
	["9cl"] = goodMagicWeapon, -- Cudgel
	["9sp"] = goodMagicWeapon, -- Barbed Club
	["9ma"] = goodMagicWeapon, -- Flanged Mace
	["9mt"] = goodMagicWeapon, -- Jagged Star
	["9fl"] = goodMagicWeapon, -- Knout
	["9wh"] = goodMagicWeapon, -- Battle Hammer
	["9m9"] = goodMagicWeapon, -- War Club
	["9gm"] = goodMagicWeapon, -- Martel de Fer
--]]

----------------------------------------
-- Polearms
----------------------------------------
--[[ Normal Polearms
	["bar"] = goodMagicWeapon, -- Bardiche
	["vou"] = goodMagicWeapon, -- Voulge
	["scy"] = goodMagicWeapon, -- Scythe
	["pax"] = goodMagicWeapon, -- Poleaxe
	["hal"] = goodMagicWeapon, -- Halberd
	["wsc"] = goodMagicWeapon, -- War Scythe
--]]

--[[ Exceptional Polearms
	["9b7"] = goodMagicWeapon, -- Lochaber Axe
	["9vo"] = goodMagicWeapon, -- Bill
	["9s8"] = goodMagicWeapon, -- Battle Scythe
	["9pa"] = goodMagicWeapon, -- Partizan
	["9h9"] = goodMagicWeapon, -- Bec-De-Corbin
	["9wc"] = goodMagicWeapon, -- Grim Scythe
--]]

----------------------------------------
-- Spears 
----------------------------------------
--[[ Normal Spears
	["spr"] = goodMagicWeapon, -- Spear
	["tri"] = goodMagicWeapon, -- Trident
	["brn"] = goodMagicWeapon, -- Brandistock
	["spt"] = goodMagicWeapon, -- Spetum
	["pik"] = goodMagicWeapon, -- Pike
--]]

--[[ Exceptional Spears
	["9sr"] = goodMagicWeapon, -- War Spear
	["9tr"] = goodMagicWeapon, -- Fuscina
	["9br"] = goodMagicWeapon, -- War Fork
	["9st"] = goodMagicWeapon, -- Yari
	["9p9"] = goodMagicWeapon, -- Lance
--]]

----------------------------------------
-- Swords
----------------------------------------
--[[ Normal Swords
	["ssd"] = goodMagicWeapon, -- Short Sword
	["scm"] = goodMagicWeapon, -- Scimitar
	["sbr"] = goodMagicWeapon, -- Sabre
	["flc"] = goodMagicWeapon, -- Falchion
	["crs"] = goodMagicWeapon, -- Crystal Sword
	["bsd"] = goodMagicWeapon, -- Broad Sword
	["lsd"] = goodMagicWeapon, -- Long Sword
	["wsd"] = goodMagicWeapon, -- War Sword
	["2hs"] = goodMagicWeapon, -- Two-Handed Sword
	["clm"] = goodMagicWeapon, -- Claymore
	["gis"] = goodMagicWeapon, -- Giant Sword
	["bsw"] = goodMagicWeapon, -- Bastard Sword
	["flb"] = goodMagicWeapon, -- Flamberge
	["gsd"] = goodMagicWeapon, -- Great Sword
--]]

--[[ Exceptional Swords
	["9ss"] = goodMagicWeapon, -- Gladius
	["9sm"] = goodMagicWeapon, -- Cutlass
	["9sb"] = goodMagicWeapon, -- Shamshir
	["9fc"] = goodMagicWeapon, -- Tulwar
	["9cr"] = goodMagicWeapon, -- Dimensional Blade
	["9bs"] = goodMagicWeapon, -- Battle Sword
	["9ls"] = goodMagicWeapon, -- Rune Sword
	["9wd"] = goodMagicWeapon, -- Ancient Sword
	["92h"] = goodMagicWeapon, -- Espandon
	["9cm"] = goodMagicWeapon, -- Dacian Falx
	["9gs"] = goodMagicWeapon, -- Tusk Sword
	["9b9"] = goodMagicWeapon, -- Gothic Sword
	["9fb"] = goodMagicWeapon, -- Zweihander
	["9gd"] = goodMagicWeapon, -- Executioner Sword
--]]

----------------------------------------
-- Javelins 
----------------------------------------
--[[ Normal Javelins
	["jav"] = goodMagicWeapon, -- Javelin
	["pil"] = goodMagicWeapon, -- Pilum
	["ssp"] = goodMagicWeapon, -- Short Spear
	["glv"] = goodMagicWeapon, -- Glaive
	["tsp"] = goodMagicWeapon, -- Throwing Spear
--]]

--[[ Exceptional Javelins
	["9ja"] = goodMagicWeapon, -- War Javelin
	["9pi"] = goodMagicWeapon, -- Great Pilum
	["9s9"] = goodMagicWeapon, -- Simbilan
	["9gl"] = goodMagicWeapon, -- Spiculum
	["9ts"] = goodMagicWeapon, -- Harpoon
--]]

----------------------------------------
-- Throwing Weapons
----------------------------------------
--[[ Normal Throwables
	["tkf"] = goodMagicWeapon, -- Throwing Knife
	["tax"] = goodMagicWeapon, -- Throwing Axe
	["bkf"] = goodMagicWeapon, -- Balanced Knife
	["bal"] = goodMagicWeapon, -- Balanced Axe
--]]

--[[ Exceptional Throwables
	["9tk"] = goodMagicWeapon, -- Battle Dart
	["9ta"] = goodMagicWeapon, -- Francisca
	["9bk"] = goodMagicWeapon, -- War Dart
	["9b8"] = goodMagicWeapon, -- Hurlbat
--]]

}

                                               
                                                     
                                                  
                                                       