-- classic\magic\helms.lua "strict"

local goodMagicHelm =
{ priority = 2, identify = true,
	goodItem =
		( Unidentified
			or DefensePercent >= 81
				and MaxLife >= 31
		)
}

magic.helms =
{ checkStats = true,
--[[ Normal Helms
	["cap"] = goodMagicHelm, -- Cap
	["skp"] = goodMagicHelm, -- Skull Cap
	["hlm"] = goodMagicHelm, -- Helm
	["fhl"] = goodMagicHelm, -- Full Helm
	["ghm"] = goodMagicHelm, -- Great Helm
	["msk"] = goodMagicHelm, -- Mask
	["crn"] = goodMagicHelm, -- Crown
	["bhm"] = goodMagicHelm, -- Bone Helm
--]]

---[[ Exceptional Helms
	["xap"] = goodMagicHelm, -- War Hat
	["xkp"] = goodMagicHelm, -- Sallet
	["xlm"] = goodMagicHelm, -- Casque
	["xhl"] = goodMagicHelm, -- Basinet
	["xhm"] = goodMagicHelm, -- Winged Helm
	["xsk"] = goodMagicHelm, -- Death Mask
	["xrn"] = goodMagicHelm, -- Grand Crown
	["xh9"] = goodMagicHelm, -- Grim Helm
--]]

}