-- classic\magic\shields.lua "strict"

local goodMagicShield =
{ priority = 3, identify = true,
	goodItem =
		( Unidentified
		    or FasterBlockRate >= 30
		        and (AllResist >= 20 or DefensePercent >= 81)
		)
}

magic.shields =
{ checkStats = true,
--[[ Normal Shields
	["sml"] = goodMagicShield, -- Small Shield
	["lrg"] = goodMagicShield, -- Large Shield
	["kit"] = goodMagicShield, -- Kite Shield
	["spk"] = goodMagicShield, -- Spiked Shield
	["tow"] = goodMagicShield, -- Tower Shield
	["bsh"] = goodMagicShield, -- Bone Shield
	["gts"] = goodMagicShield, -- Gothic Shield
--]]

---[[ Exceptional Shields
	--["xml"] = goodMagicShield, -- Round Shield
	--["xrg"] = goodMagicShield, -- Scutum
	["xit"] = goodMagicShield, -- Dragon Shield
	["xpk"] = goodMagicShield, -- Barbed Shield
	["xow"] = goodMagicShield, -- Pavise
	["xsh"] = goodMagicShield, -- Grim Shield
	["xts"] = goodMagicShield, -- Ancient Shield
--]]

}