-- classic\rare\rings.lua "strict"

local isGoodRareRing =
function(item)
	local fcr, goodMods, statMods, meleeMods = false, 0, 0, 0
	
	fcr = FasterCastRate >= 10

	goodMods =
		count{
			TotalResist >= 55,
			MagicFind >= 20,
		}

	statMods =
		count{
			MaxLife >= 31,
			MaxMana >= 41,
			Strength >= 10,
			Dexterity >= 10 and Energy >= 11,
			--Dexterity >= 10,
			--Energy >= 11,
		}

	meleeMods =
		count{
			ToHit >= 101,
			ToHitPercent ~= 0,
			LifeDrainMinDamage >= 5 and ManaDrainMinDamage >= 4,
			MinDamage >= 8 and MaxDamage >= 3,
		}

	return
		( Unidentified
			--or fcr
				--and goodMods >= 1
			or (goodMods + statMods) >= 2
			    and fcr
			or (statMods + meleeMods) >= 3
			or goodMods >= 1
				and meleeMods >= 2
		)
end

rare.rings =
{ checkStats = true,
	["rin"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareRing
		}
}
