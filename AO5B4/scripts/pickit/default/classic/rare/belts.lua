-- classic\rare\belts.lua "strict"

local isGoodRareBelt =
function(item)
	return
		( Unidentified
			or FasterHitRecovery >= 24
				and Strength >= 20
				and
					( MaxLife >= 41
						or TotalResist >= 55
						--or DefensePercent >= 81
					)
		)
end

rare.belts =
{ checkStats = true,
	["Belt"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareBelt
		}
}
