-- classic\rare\helms.lua "strict"

local isGoodRareHelm =
function(item)
	local sexyMods, goodMods = 0, 0
	
    sexyMods = -- mods with colors
        count{
			DefensePercent >= 81, -- 66-100 .. Dark Gold Color
			--LightRadius == 2, -- 2 .. Light Yellow Color
			MaxLife >= 31, -- 31-40 .. Crystal Red Color
		}
		
	goodMods =
	    count{
			TotalResist >= 55,
			FasterHitRecovery >= 10,
            ToHit >= 15, 
			ToHitPercent ~= 0,
	    }

	return
		( Unidentified
		    or (sexyMods + goodMods) >= 3
			--or sexyMods >= 2
			    --and goodMods >= 1
		)
end

local goodRareHelm =
{ priority = 5, identify = true,
	isGoodItem = isGoodRareHelm
}

local greaterRareHelm =
{ priority = 8, identify = true,
	isGoodItem = isGoodRareHelm
}

rare.helms =
{ checkStats = true,
--[[ Normal Helms
	["cap"] = goodRareHelm, -- Cap
	["skp"] = goodRareHelm, -- Skull Cap
	["hlm"] = goodRareHelm, -- Helm
	["fhl"] = goodRareHelm, -- Full Helm
	["ghm"] = goodRareHelm, -- Great Helm
	["msk"] = goodRareHelm, -- Mask
	["crn"] = goodRareHelm, -- Crown
	["bhm"] = goodRareHelm, -- Bone Helm
--]]

---[[ Exceptional Helms
	["xap"] = greaterRareHelm, -- War Hat
	["xkp"] = greaterRareHelm, -- Sallet
	["xlm"] = greaterRareHelm, -- Casque
	["xhl"] = greaterRareHelm, -- Basinet
	["xhm"] = greaterRareHelm, -- Winged Helm
	["xsk"] = greaterRareHelm, -- Death Mask
	["xrn"] = greaterRareHelm, -- Grand Crown
	["xh9"] = greaterRareHelm, -- Grim Helm
--]]

}