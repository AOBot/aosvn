-- expansion\magic\amulets.lua "strict"

-- Amulets For Craft Settings
local minItemLvl = 91 --note: forCrafting
local keepCraftUnid = (ItemLevel < minItemLvl)
local forCrafting = ItemLevel >= minItemLvl

local isGoodMagicAmulet =
function(item)
	local greatMod, prefix, suffix = false, false, false
	
	greatMod = MagicFind >= 50
	prefix =
		( SkillTabBonus >= 3
			--or ClassSkillsBonus >= 2
			--or AllResist >= 26 
		)
	suffix =
		( MaxLife >= 100
			or FasterCastRate >= 10
			or Strength >= 30
			or Dexterity >= 30
			--or Energy >= 30
			or MagicFind >= 35
			--or DamageReduction >= 25
			--or LifePerLevel ~= 0
			--or ManaPerLevel ~= 0
			--or PoisonLengthReduction >= 75
		)

	return
		( Unidentified
		    --or forCrafting
			or greatMod
			or (prefix and suffix)
			--or (prefix or suffix)
			--or prefix
		)
end

magic.amulets =
{ checkStats = true,
	["amu"] = -- Amulet
		{ priority = 2, identify = true,
			isGoodItem = isGoodMagicAmulet
		}
}