-- expansion\magic\smallCharms.lua "strict"

-- Small Charms For Reroll Settings
local minItemLvl = 99 --note: forRerolling
local keepRerollUnid = (ItemLevel < minItemLvl)
local forRerolling = ItemLevel >= minItemLvl


local isGoodSmallCharm =
function(item)
	local greatMod, prefix, suffix = false, false, false
	greatMod =
			( AllResist >= 5
				or MaxLife >= 20
				--or MagicFind >= 7
				or PoisonMaxDamage >= 175
				or ColdMaxDamage >= 21
				or FireMaxDamage >= 30
				or LightMaxDamage >= 72
				or MaxDamage >= 3
					and ToHit >= 20
				or MaxDamage >= 2
					and ToHit >= 10
					and MaxLife >= 15
				or MaxDamage >= 4
			)
	prefix =
			( AllResist >= 5
				or SingleResist >= 11
				or ToHit >= 36
				or MaxDamage >= 3 and ToHit >= 20
				or MaxMana >= 17
				--or ArmorClass >= 30
				--or PoisonMaxDamage >= 100
				--or ColdMaxDamage >= 20
				--or FireMaxDamage >= 29
				--or LightMaxDamage >= 71
			)
	suffix =
			( MaxLife >= 20
				--or Strength >= 2
				--or Dexterity >= 2
				or MagicFind >= 7
				--or GoldFind >= 10
				or FasterHitRecovery ~= 0
				or FasterMoveVelocity ~= 0
			)
	return
		( Unidentified
			or prefix and suffix
			or PoisonMaxDamage >= 200
			or MaxDamage >= 4
		    --or forRerolling
			--or greatMod
			--or prefix and suffix
			--or prefix or suffix
		)
end

magic.smallCharms =
{ checkStats = true,
	["cm1"] = -- Small Charm
		{ priority = 5, identify = true,
			isGoodItem = isGoodSmallCharm
		}
}