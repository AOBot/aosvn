-- expansion\magic\grandCharms.lua "strict"

-- Grand Charms For Reroll Settings
local minItemLvl = 100 --note: forRerolling
local keepRerollUnid = (ItemLevel < minItemLvl)
local forRerolling = ItemLevel >= minItemLvl

local isGoodGrandCharm =
function(item)
	local greatMod, prefix, suffix = false, false, false
	local greatSkill, goodSkill = false, false
	
	greatMod =
		( --AllResist >= 15
			--MaxDamage >= 10
				--and ToHit >= 70
			--or ToHit >= 115
				--and MaxLife >= 41
			--or ToHit >= 130
			--or MaxDamage >= 11
			MaxDamage >= 10 and ToHit >= 70 and MaxLife >= 41
			or ToHit >= 130 and MaxLife >= 41
			or MaxDamage >= 11
		)
		
	prefix =
		( AllResist >= 15
			--or SingleResist >= 30
			--or ToHit >= 118
			or MaxDamage >= 10 and ToHit >= 49
			--or MaxMana >= 57
		)
		
	suffix =
		( MaxLife >= 21
			or FasterHitRecovery ~= 0
			--or FasterMoveVelocity ~= 0
			or Strength >= 6
			or Dexterity >= 6
		)
		
	greatSkill =
	    ( PaladinCombatTab ~= 0
	      	or LightningTab ~= 0
	    )

	goodSkill =
	    ( --OffensiveAurasTab ~= 0
	        --or DefensiveAurasTab ~= 0
	        --or BowAndCrossBowTab ~= 0
	        --or PassiveAndMagicTab ~= 0
	        JavelinAndSpearTab ~= 0
	        or FireTab ~= 0
	        or ColdTab ~= 0
			or LightningTab ~= 0
	        --or CursesTab ~= 0
	        or PoisonAndBoneTab ~= 0
	        --or NecroSummoningTab ~= 0
	        --or BarbarianCombatTab ~= 0
	        --or MasteriesTab ~= 0
	        --or WarcriesTab ~= 0
	        --or DruidSummoningTab ~= 0
	        --or ShapeShiftingTab ~= 0
	        or ElementalTab ~= 0
	        or TrapsTab ~= 0
	        --or ShadowDisciplinesTab ~= 0
	        --or MartialArtsTab ~= 0
			or PaladinCombatTab ~= 0
		)
		
	return
		( Unidentified
			or greatMod
			or goodSkill and suffix
		    --or forRerolling
			--or greatMod
			--or SkillTabBonus ~= 0
			--or greatSkill
			--or goodSkill
				--and suffix
			--or prefix and suffix
		)
		
end

magic.grandCharms =
{ checkStats = true,
	["cm3"] = -- Grand Charm
		{ priority = 4, identify = keepRerollUnid,
			isGoodItem = isGoodGrandCharm
		},
}