-- expansion\magic\jewels.lua "strict"

local isGoodMagicJewel =
function(item)
	local greatMod, prefix, suffix = false, false, false
	
	greatMod =
			( --AllResist >= 15
				--or MaxDamagePercent >= 40
				--or MaxDamage >= 15
				MinDamage >= 18
				or MaxDamage >= 30
			)
	prefix =
			( MaxDamagePercent >= 40
				or AllResist >= 15
				or SingleResist >= 30
				--or MaxMana >= 20
				--or ArmorClass >= 64
				or ToHit >= 100
				--or DamageToMana == 12
			)
	suffix =
			( FasterAttackRate ~= 0
				or FasterHitRecovery ~= 0
				or LowerRequirementsPercent ~= 0
				or Strength >= 9
				or Dexterity >= 9
				or Energy >= 9
				or MaxLife >= 20
				--or LightMaxDamage >= 97
			)
	return
		( Unidentified
			or greatMod
			or prefix and suffix
		)
end

magic.jewels =
{ checkStats = true,
	["jew"] = -- Jewel
		{ priority = 3, identify = true, --pickLimit = 3,
			isGoodItem = isGoodMagicJewel
		}
}