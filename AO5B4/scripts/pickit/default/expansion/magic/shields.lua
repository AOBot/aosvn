-- expansion\magic\shields.lua "strict"

local minItemLvl = 91 --note: forCrafting
local keepCraftUnid = (ItemLevel < minItemLvl)
local forCrafting = 
	ItemLevel >= minItemLvl
		and isItemInList{
			--"sml", -- Small Shield / Caster
			--"kit", -- Kite Shield / Safety
			--"spk", -- Spiked Shield / Blood
			--"gts", -- Gothic Shield / Hit Power
			--"xml", -- Round Shield / Caster
			--"xit", -- Dragon Shield / Safety
			--"xpk", -- Barbed Shield / Blood
			--"xts", -- Ancient Shield / Hit Power
			"uml", -- Luna / Caster
			"upk", -- Blade Barrier / Blood
			"uts", -- Ward / Hit Power
			"uit" -- Monarch / Safety					
		}
local ThreeSocket = 
{ priority = 3, identify = true, 
	goodItem = 
		(Unidentified 
			--or forCrafting
			or Sockets == 3 
				and FasterBlockRate >= 15				
		)
}
local FourSocket = 
{ priority = 3, identify = true, 
	goodItem = 
		(Unidentified 
			--or forCrafting		
			or Sockets >= 4 
				and FasterBlockRate >= 30
		)
}

magic.shields =
{ checkStats = true,
--[[ Normal Shields
	["sml"] = {}, -- Small Shield / Caster
	["lrg"] = {}, -- Large Shield
	["kit"] = ThreeSocket, -- Kite Shield / Safety
	["spk"] = {}, -- Spiked Shield / Blood
	["tow"] = ThreeSocket, -- Tower Shield
	["bsh"] = {}, -- Bone Shield
	["gts"] = ThreeSocket, -- Gothic Shield / Hit Power
--]]

--[[ Exceptional Shields
	["xml"] = {}, -- Round Shield / Caster
	["xrg"] = ThreeSocket, -- Scutum
	["xit"] = ThreeSocket, -- Dragon Shield / Safety
	["xpk"] = {}, -- Barbed Shield / Blood
	["xow"] = ThreeSocket, -- Pavise
	["xsh"] = {}, -- Grim Shield
	["xts"] = ThreeSocket, -- Ancient Shield / Hit Power
--]]

---[[ Elite Shields
	--["uuc"] = {}, -- Heater
	--["uml"] = {}, -- Luna / Caster
	--["urg"] = ThreeSocket, -- Hyperion
	["uit"] = FourSocket, -- Monarch / Safety
	--["upk"] = ThreeSocket, -- Blade Barrier / Blood
	--["uow"] = FourSocket, -- Aegis
	--["ush"] = ThreeSocket, -- Troll Nest
	--["uts"] = FourSocket, -- Ward / Hit Power
--]]
}