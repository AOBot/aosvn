-- expansion\rare\classShields.lua "strict"

local isGoodRarePalShield =
function(item)
	local sock, fbr, goodMod = false, false, 0

	sock = Sockets >= 2
    fbr = FasterBlockRate >= 30
    goodMod = 
    	count{
			FasterHitRecovery >= 10,
    		Strength >= 9,
    		MaxLife >= 41,
    		--DefensePercent >= 101,
    		--ClassSkillsBonus >= 2,
    		--SkillTabBonus >= 2,
			--TotalResist >= 120,
			--AllResist >= 40,
    	}
	return
		( Unidentified
			or (ClassSkillsBonus >= 2 or PaladinCombatTab >= 2)
			and AllResist >= 45
			and Sockets >= 2
			and FasterBlockRate >= 30
			and goodMod >= 1
			--or sock and fbr
			   -- and goodMod >= 2
			--or sock
				--and goodMod >= 2
			--or fbr
				--and goodMod >= 2
		
		)
end

local isGoodRareNecShield =
function(item)
	local necroSkiller = false
	local goodMod = 0
	
	necroSkiller = 
		(ClassSkillsBonus + PoisonAndBoneTab + BoneSpearSkill)
		or (ClassSkillsBonus + PoisonAndBoneTab + BoneSpiritSkill)
		or (ClassSkillsBonus + PoisonAndBoneTab + PoisonNovaSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + TeethSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BoneArmorSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + PoisonDaggerSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + CorpseExplosionSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BoneWallSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + PoisonExplosionSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BonePrisonSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + RaiseSkeletonSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + SkeletonMasterySkill)
		--or (ClassSkillsBonus + NecroSummoningTab + ClayGolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + GolemMasterySkill)
		--or (ClassSkillsBonus + NecroSummoningTab + RaiseSkeletalMageSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + BloodgolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + SummonResistSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + IronGolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + FiregolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + ReviveSkill)
		--or (ClassSkillsBonus + CursesTab + LowerResistSkill)
		--or (ClassSkillsBonus + CursesTab + DecrepifySkill)
		--or (ClassSkillsBonus + CursesTab + AmplifyDamageSkill)
		--or (ClassSkillsBonus + CursesTab + DimVisionSkill)
		--or (ClassSkillsBonus + CursesTab + WeakenSkill)
		--or (ClassSkillsBonus + CursesTab + IronMaidenSkill)
		--or (ClassSkillsBonus + CursesTab + TerrorSkill)
		--or (ClassSkillsBonus + CursesTab + ConfuseSkill)
		--or (ClassSkillsBonus + CursesTab + LifeTapSkill)
		--or (ClassSkillsBonus + CursesTab + AttractSkill)
		
    goodMod = 
    	count{
			--Sockets >= 2,
			--FasterBlockRate >= 30,
			FasterHitRecovery >= 10,
    		Strength >= 9,
    		MaxLife >= 31,
    		--DefensePercent >= 101,
			TotalResist >= 55,
			AllResist >= 18,
    	}
    	
	return
		( Unidentified
			or BoneSpearSkill >= 1
			and (ClassSkillsBonus >= 2 or PoisonAndBoneTab >= 2)
			and Sockets >= 2
			and FasterBlockRate >= 30
			and goodMod >= 1
			--or necroSkiller >= 5
				--and goodMod >= 1
          	--or SkillTabBonus >= 2
          	--or ClassSkillsBonus >= 2
		)
end

rare.classShields =
{ checkStats = true,
---[[ Paladin Shields
	["AuricShields"] = 
		{ priority = 3, identify = true,
			isGoodItem = isGoodRarePalShield
		},
--]]

---[[ Necromancer Shields
	["VoodooHeads"] = 
		{ priority = 3, identify = true,
			isGoodItem = isGoodRareNecShield
		},
--]]
}