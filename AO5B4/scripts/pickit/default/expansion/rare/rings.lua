-- expansion\rare\rings.lua "strict"

local isGoodRareRing =
function(item)
	local fcr, goodMods, statMods, meleeMods = false, 0, 0, 0
	
	fcr = FasterCastRate >= 10

	goodMods =
		count{
			TotalResist >= 55,
			MagicFind >= 20,
			AllResist >= 11,
		}

	statMods =
		count{
			MaxLife >= 31,
			MaxMana >= 61,
			Strength >= 20,
			Dexterity >= 10 and Energy >= 11,
			Dexterity >= 15,
			Energy >= 15,
		}

	meleeMods =
		count{
			ToHit >= 101,
			ToHitPercent ~= 0,
			LifeDrainMinDamage >= 6 and ManaDrainMinDamage >= 6,
			MinDamage >= 4 and MaxDamage >= 6,
		}
	prefix = count{
		ToHit >= 100,
		MaxMana >= 70,
		AllResist >= 8,
		SingleResist >= 25,
		DualResist >= 40,
		TripleResist >= 45,
	}
	suffix = count{
		Strength >= 10,
		Dexterity >= 8,
		MaxLife >= 15,
		ToHitPercent ~= 0,
		LifeDrainMinDamage >= 6,
		ManaDrainMinDamage >= 6,
		LifeRegen >= 5,
		FasterCastRate >= 10,
	}
	return
		( Unidentified
			or (prefix + suffix) >= 4
			--[[or fcr
				and (goodMods + statMods) >= 2
			or (statMods + meleeMods) >= 3
			or (goodMods + meleeMods) >= 2
			--]]
			--or fcr and (Strength >= 12 or Dexterity >= 12) and (MaxLife >= 35 or MaxMana >= 70)
			--or Strength >= 15 and Dexterity >= 15 and MaxLife >= 30 and ToHit >= 80
			
		)
end

rare.rings =
{ checkStats = true,
	["rin"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareRing
		}
}

--[[
prefix
ar 101-120
mana 61-90
sres 31-40
ares 8-11
mf 5-10

suffix
str 16-20
dex 10-15
eng 10-15
life 31-40
ar% 5
ll 6-9
ml 6-9
gold 25-40
mf 16-25
fcr 10
]]