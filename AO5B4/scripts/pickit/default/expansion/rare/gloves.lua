-- expansion\rare\gloves.lua "strict"

local isGoodRareGloves =
function(item)
	if Ethereal and Identified and ReplenishDurability == 0 then
		return false -- not keeping eth unless repairs
	end
	local skill, ias, stat, other = false, false, false, false
	skill =
		( JavelinAndSpearTab >= 2
			or PassiveAndMagicTab >= 2
			or BowAndCrossBowTab >= 2
			or MartialArtsTab >= 2
		)
	ias = FasterAttackRate >= 20
	stat =
		( Strength >= 9
			or Dexterity >= 15
			--or MaxMana >= 38
		)
	other =
		( FasterHitRecovery >= 10
			or TotalResist >= 55
			or LifeDrainMinDamage >= 3 and ManaDrainMinDamage >= 3
			or ToHitPercent >= 5
			or DefensePercent >= 101
			or ArmorPerLevel ~= 0
		)
	return
		( Unidentified
			or (PassiveAndMagicTab >= 2 or JavelinAndSpearTab >= 2)
				and FasterAttackRate >= 20
				and Strength >= 9
				and Dexterity >= 9
			or FasterAttackRate >= 20
				and Strength >= 12
				and Dexterity >= 12
			--[[or skill and ias and (stat or other)
			--or ias and stat and other
			--or skill and ias
			]]--
			-- Bowa & Java
			or (PassiveAndMagicTab >= 2 or JavelinAndSpearTab >= 2) and ias and Strength >= 10 and Dexterity >= 10 and SingleResist >= 20
			-- Smiter
			or ias and Strength >= 12 and Decterity >= 12 and (SingleResist >= 30 or DualResist >= 30 or TripleResist >= 30 or AllResist >= 10) 
		)
end

rare.gloves =
{ checkStats = true,
	["Gloves"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareGloves
		}
}

--[[
prefix
defperlevel ?
mana 31-40
zontabs 2
sinMAtab 2

suffix
str 10-15
dex 10-15
ar% 5
ll 2-5
ml 2-5
ias 20
gold 41-80
mf 16-25
]]