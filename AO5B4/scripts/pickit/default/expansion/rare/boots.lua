-- expansion\rare\boots.lua "strict"

local isGoodRareBoots =
function(item)
	if Ethereal and Identified and ReplenishDurability == 0 then
		return false -- not keeping eth unless repairs
	end
	local frw, fhr, prefix, suffix = false, false, 0, 0
	
	frw = FasterMoveVelocity >= 20
	fhr = FasterHitRecovery >= 10
	prefix =
		count{
			DefensePercent >= 101,
			ArmorPerLevel ~= 0,
			MaxMana >= 38,
		}
	suffix =
		count{
			Dexterity >= 9,
			LifeRegen >= 5,
			MagicFind >= 25,
			GoldFind >= 80,
		}
	return
		( Unidentified
			or(TripleResist >= 30
				and FasterMoveVelocity >= 20
				and (FasterHitRecovery >= 10 or Dexterity >= 5 or LifeRegen >= 4))
			or	(DoubleResist >= 50
				and FasterMoveVelocity >= 20
				and MaxMana >= 30
				and (FasterHitRecovery >= 10 or Dexterity >= 5 or LifeRegen >= 4))
		)
end

rare.boots =
{ checkStats = true,
	["Boots"] =
		{ priority = 4, identify = true,
			isGoodItem = isGoodRareBoots
		}
}

--[[
prefix
defperlevel ?
mana 31-40
sres 31-40

suffix
dex 6-9
gold 41-80
mf 16-25
frw 30
]]