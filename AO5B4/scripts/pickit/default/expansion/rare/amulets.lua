-- expansion\rare\amulets.lua "strict"

local isGoodRareAmulet =
function(item)
-- Good Skill Amount Settings
	local minSkill = 2 -- Class Skills
	local minTab = 2 -- Skill Tab
-------------------------------------------------
	local anySkill, casterSkill, meleeSkill, fcr = false, false, false, false
	local statMods, goodMods, meleeMods = false, 0, 0

	skiller = (
		PaladinSkills >= 2
		or SorceressSkills >= 2
		or DruidSkills >= 2
		or AssassinSkills >= 2
		or NecromancerSkills >= 2
		or AmazonSkills >= 2
		or BarbarianSkills >= 2
		or PaladinCombatTab >= 2
		or LightningTab >= 2
		or FireTab >= 2
		or ColdTab >= 2
		or PoisonAndBoneTab >= 2
		or WarcriesTab >= 2
		or ElementalTab >= 2
		or TrapsTab >= 2
		or JavelinAndSpearTab >= 2
	)
	prefixes = count{
			AllResist >= 16,
			MaxMana >= 61,
	}
	suffixes = count{
		Strength >= 10,
		Dexterity >= 10,
		MaxLife >= 20,
		LifeRegen >= 5,
		FasterCastRate >= 10,
	}
	fcr = FasterCastRate >= 10
	statMods =
			( 
			Strength >= 30 or 
			Dexterity >= 20 or 
			Strength >= 21 and Dexterity >= 16)
				and (MaxLife >= 50 or MaxMana >= 80 or 
				MaxLife >= 41 and (MaxMana >= 61 or Energy >= 16))
	goodMods =
		count{
            (AllResist >= 16 or DoubleResist >= 60 or SingleResist >= 40),
            MagicFind >= 10, --note: 10 mf = perfect prefix mf or any suffix mf
            GoldFind >= 70,
            LifeRegen >= 6,
            DamageToMana >= 12,
            PoisonLengthReduction >= 75,
            TeleportChargesLevel ~= 0,
		}			
	meleeMods =
		count{
			(LifeDrainMinDamage + ManaDrainMinDamage) >= 12,
			(MaxDamage >= 4 or MinDamage >= 9 or MaxDamage >= 3 and MinDamage >= 7),
			ToHitPercent ~= 0,			
		}
	return
		( Unidentified
			or skiller and (prefixes + suffixes) >= 3
		    --or fcr and casterSkill and statMods and goodMods >= 1
			--or meleeSkill and statMods and goodMods >= 1 and meleeMods >= 1
			--or anySkill and (fcr or meleeMods >= 1) and statMods and goodMods >= 1
		)
end

rare.amulets =
{ checkStats = true,
	["amu"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareAmulet
		}
}

--[[ High affix values
prefix
mana 61-90
sres 31-40
ares 16-20
cskill 2
tskill 2
dam2mana 7-12
mf 5-10

suffix
str 21-30
dex 16-20
eng 16-20
life 41-60
ll 6
ml 7-8
mf 16-25
fcr 10
]]