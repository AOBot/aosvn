-- expansion\rare\belts.lua "strict"

local isGoodRareBelt =
function(item)
	if Ethereal and Identified and ReplenishDurability == 0 then
		return false -- not keeping eth unless repairs
	end
	return
		( Unidentified
			or FasterHitRecovery >= 24
				and Strength >= 20
				and MaxLife >= 20
					or TripleResist >= 30
					--( MaxLife >= 41
					--	or TotalResist >= 55
						--or DefensePercent >= 101
						--or ArmorPerLevel ~= 0
					--)
		)
end

rare.belts =
{ checkStats = true,
	["Belt"] =
		{ priority = 4, identify = true,
			isGoodItem = isGoodRareBelt
		}
}

--[[
prefix
mana 21-31
defperlevel ?

suffix
str 21-30
life 41-60
fhr 24
gold 41-80
]]