-- expansion\unique\gloves.lua "strict"

unique.gloves =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- ** Normal Gloves ** --
-----------------------------------------------------
--[[ The Hand of Broc (Leather Gloves)
	["lgl"] =
		{
			priority = 2, identify = true,
			goodItem =
   			( not Ethereal and
				( Unidentified
					or DefensePercent >= 20 --note: 10-20 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Bloodfist (Heavy Gloves)
	["vgl"] =
		{
			priority = 2, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 20 --note: 10-20 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Chance Guards (Chain Gloves)
	["mgl"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or MagicFind >= 40 --note: 25-40 mf
						--and DefensePercent >= 20 --note: 20-30 ed
				)
			)
		},
--]]
-----------------------------------------------------
---[[ Magefist (Light Gauntlets)
	["tgl"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 30 --note: 20-30 ed
						and TotalDefense >= 25
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Frostburn (Gauntlets)
	["hgl"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 20 --note: 10-20 ed
				)
			)
		},
--]]
-----------------------------------------------------
-- ** Exceptional Gloves ** --
-----------------------------------------------------
--[[ Venom Grip (Demonhide Gloves)
	["xlg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or TotalDefense >= 116 --note: 97-118 def
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Gravepalm (Sharkskin Gloves)
	["xvg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 140 --note: 140-180 ed
					    and UndeadDamagepercent >= 190 --note: 100-200 dam undead
					    --and UndeadToHit >= 100 --note: 100-200 AR to undead
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Ghoulhide (Heavy Bracers)
	["xmg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 180 --note: 150-190 ed
					    and ManaDrainMinDamage >= 5 --note: 4-5 manastolen
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Lava Gout (Battle Gauntlets)
	["xtg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 195 --note: 150-200 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Hellmouth (War Gauntlets)
	["xhg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 195 --note: 150-200 ed
				)
			)
		},
--]]
-----------------------------------------------------
-- ** Elite Gloves ** --
-----------------------------------------------------
---[[ Dracul's Grasp (Vampirebone Gloves)
	["uvg"] =
		{
			priority = 7, identify = false,
			goodItem =
			( not Ethereal and
				( Unidentified
					or Strength >= 15 --note: 10-15 str
						and LifeDrainMinDamage >= 10 --note: 7-10 lifestolen
						and HealAfterKill >= 10 --note: 5-10 life after kill
						and DefensePercent >= 120 --note: 90-120 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Soul Drainer (Vambraces)
	["umg"] =
		{
			priority = 6, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or LifeDrainMinDamage >= 7 --note: 4-7 lifestolen
						and ManaDrainMinDamage >= 7 --note: 4-7 manastolen
						and DefensePercent >= 90 --note: 90-120 ed
				)
			)
		},
--]]
-----------------------------------------------------
---[[ Steelrend (Ogre Gauntlets)
	["uhg"] =
		{
			priority = 8, identify = false,
			goodItem =
			( not Ethereal and
				( Unidentified
					or TotalDefense >= 281 --note: 232-281 def
						and MaxDamagePercent >= 60 --note: 30-60 edam
						and Strength >= 20 --note: 15-20 str
				)
			)
		}
--]]
-----------------------------------------------------
}