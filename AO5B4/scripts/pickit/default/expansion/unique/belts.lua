-- expansion\unique\belts.lua "strict"

unique.belts =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- ** Normal Belts ** --
-----------------------------------------------------
--[[ Lenymo (Sash)
	["lbl"] =
		{
			priority = 2, identify = true,
			goodItem = ( not Ethereal )
			--note: all the same, unless ethereal
		},

--]]
-----------------------------------------------------
--[[ Snakecord (Light Belt)
	["vbl"] =
		{
			priority = 2, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 30 --note: 20-30 ed
				)
			)
		},
	
--]]
-----------------------------------------------------
--[[ Nightsmoke (Belt)
	["mbl"] =
		{
			priority = 2, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 50 --note: 30-50 ed
				)
			)
		},
	
--]]
-----------------------------------------------------
--[[ Goldwrap (Heavy Belt)
	["tbl"] =
		{
			priority = 3, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 50 --note: 40-60 ed
					    and GoldFind >= 80 --note: 50-80 extra gold
				)
			)
		},
	
--]]
-----------------------------------------------------
--[[ Bladebuckle (Plated Belt)
	["hbl"] =
		{
			priority = 2, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 100 --note: 80-100 ed
				)
			)
		},
	
--]]
-----------------------------------------------------
-- ** Exceptional Belts ** --
-----------------------------------------------------
--[[ String of Ears (Demonhide Sash)
	["zlb"] =
		{
			priority = 6, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DamageResist >= 15 --note: 10-15 dam redu
						and LifeDrainMinDamage >= 8 --note: 6-8 lifestolen
						and MagicDamageReduction >= 15 --note: 10-15 magic dam redu
						--and DefensePercent >= 150 --note: 150-180 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Razortail (Sharkskin Belt)
	["zvb"] =
		{
			priority = 3, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 145 --note: 120-150 ed
				)
			)
		},
	
--]]
-----------------------------------------------------
--[[ Gloom's Trap (Mesh Belt)
	["zmb"] =
		{
			priority = 3, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 145 --note: 120-150 ed
				)
			)
		},
	
--]]
-----------------------------------------------------
--[[ Snowclash (Battle Belt)
	["ztb"] =
		{
			priority = 6, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 130 --note: 130-170 ed
					    and SkillOnGetHit >= 20 --note: 7-20 blizzard when struck
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Thundergod's Vigor (War Belt)
	["zhb"] =
		{
			priority = 7, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 200 --note: 160-200 ed
				)
			)
		},
--]]
-----------------------------------------------------
-- ** Elite Belts ** --
-----------------------------------------------------
---[[ Arachnid Mesh (Spiderweb Sash)
	["ulc"] =
		{
			priority = 9, identify = false,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 120 --note: 90-120 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Nosferatu's Coil (Vampirefang Belt)
	["uvc"] =
		{
			priority = 7, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or LifeDrainMinDamage >= 7 --note: 5-7 lifestolen
					    --and BaseDefense >= 56 --note: 56-63 base def
				)
			)
		},
--]]
-----------------------------------------------------
---[[ Verdungo's Hearty Cord (Mithril Coil)
	["umc"] =
		{
			priority = 8, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DamageResist >= 15 --note: 10-15 dam redu
						and Vitality >= 40 --note: 30-40 vit
						--and LifeRegen >= 10 --note: 10-13 replen life
						--and DefensePercent >= 90 --note: 90-140 ed
						--and MaxStamina >= 100 --note: 100-120 max stamina
				)
			)
		}
--]]
-----------------------------------------------------
}