--------------------------------------------------
-------------- Expansion "Strict" ----------------
--------------------------------------------------

unique.files = {
--------------------------------------------------
--------------- Unique Item Files ----------------
--------------------------------------------------
["amulets.lua"] = {kind = "Amulet"},
["armors.lua"] = {kind = "Armor"},
["belts.lua"] = {kind = "Belt"},
["boots.lua"] = {kind = "Boots"},
["charms.lua"] = {kind = "Charm"},
["gloves.lua"] = {kind = "Gloves"},
["helms.lua"] = {kind = "AnyHelm"},
["jewels.lua"] = {kind = "Jewel"},
["rings.lua"] = {kind = "Ring"},
["shields.lua"] = {kind = "AnyShield"},
["weapons.lua"] = {kind = "AnyWeapon"},
--------------------------------------------------
}
rare.files = {
--------------------------------------------------
--------------- Rare Item Files ------------------
--------------------------------------------------
["amulets.lua"] = {kind = "Amulet"},
["armors.lua"] = {kind = "Armor"},
["belts.lua"] = {kind = "Belt"},
["boots.lua"] = {kind = "Boots"},
["circlets.lua"] = {kind = "Circlet"},
["classHelms.lua"] = {kind = "ClassHelm"},
["classShields.lua"] = {kind = "ClassShield"},
["gloves.lua"] = {kind = "Gloves"},
["helms.lua"] = {kind = "Helm"},
["jewels.lua"] = {kind = "Jewel"},
["shields.lua"] = {kind = "Shield"},
["rings.lua"] = {kind = "Ring"},
["weapons.lua"] = {kind = "AnyWeapon"},
--------------------------------------------------
}
set.files = {
--------------------------------------------------
--------------- Set Item Files -------------------
--------------------------------------------------
--["setItems.lua"] = {noLimiters = true}
--------------------------------------------------
}
magic.files = {
--------------------------------------------------
--------------- Magic Item Files -----------------
--------------------------------------------------
["amulets.lua"] = {kind = "Amulet"},
["armors.lua"] = {kind = "Armor"},
--["belts.lua"] = {kind = "Belt"}, -- for crafting
--["boots.lua"] = {kind = "Boots"}, -- for crafting
["circlets.lua"] = {kind = "Circlet"},
["classHelms.lua"] = {kind = "ClassHelm"},
["classShields.lua"] = {kind = "ClassShield"},
["gloves.lua"] = {kind = "Gloves"},
["grandCharms.lua"] = {kind = "LargeCharm"},
--["helms.lua"] = {kind = "Helm"},
["jewels.lua"] = {kind = "Jewel"},
--["largeCharms.lua"] = {kind = "MediumCharm"},
["rings.lua"] = {kind = "Ring"},
["shields.lua"] = {kind = "Shield"},
["smallCharms.lua"] = {kind = "SmallCharm"},
["weapons.lua"] = {kind = "AnyWeapon"},
--------------------------------------------------
}
white.files = {
--------------------------------------------------
--------------- White Item Files -----------------
--------------------------------------------------
["armors.lua"] = {kind = "Armor"},
--["gems.lua"] = {kind = "Gem"},
["helms.lua"] = {kind = "AnyHelm"},
["keys.lua"] = {kind = "Quest"},
["runes.lua"] = {kind = "Rune"},
["shields.lua"] = {kind = "AnyShield"},
["weapons.lua"] = {kind = "AnyWeapon"},
--------------------------------------------------
}