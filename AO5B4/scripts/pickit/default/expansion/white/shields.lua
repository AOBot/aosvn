-- expansion\white\shields.lua "strict"

local goodPalShield =
{ priority = 8,
	goodItem =
		( not Ethereal
			and hasSockets("034")
			and (AllResist >= 45 or MaxDamagePercent >= 63)
			and DefensePercent >= 15
		or Ethereal
			and Sockets == 0
			and DefensePercent == 0
			and MaxDurabilityPercent == 0
			and (AllResist >= 45 or MaxDamagePercent >= 63)
			--and (AllResist >= 45 or MaxDamagePercent >= 65)			
			--and BaseDefense >= 237			
		)
}

local goodThreeSocket =
{ priority = 7,
	goodItem =
		( not Ethereal
			and hasSockets("03")
			and DefensePercent >= 15
		--or Ethereal
			--and Sockets == 0
			--and DefensePercent == 0
			--and MaxDurabilityPercent == 0
			--and BaseDefense >= 258			
		)
}

local goodFourSocket =
{ priority = 7,
	goodItem =
		( not Ethereal
			and hasSockets("04")
			and DefensePercent >= 15
		--or Ethereal
			--and Sockets == 0
			--and DefensePercent == 0
			--and MaxDurabilityPercent == 0
			--and BaseDefense >= 222
		)
}

local anyGoodShield =
{ priority = 6,
	goodItem =
		( not Ethereal
			and hasSockets("0234")
			and DefensePercent >= 15
		or Ethereal
			and Sockets == 0
			and DefensePercent == 0
			and MaxDurabilityPercent == 0
		)
}

local isGoodNecShield =
function(item)
	local pnbSkills, nsSkills, curSkills = false, false, false
	
	pnbSkills =
		( BoneSpiritSkill >= 3
				and (BoneSpearSkill + BonePrisonSkill + BoneWallSkill + TeethSkill) >= 1 
			or BoneSpearSkill >= 3
				and (BoneSpiritSkill + BonePrisonSkill + BoneWallSkill + TeethSkill) >= 1
			--or PoisonNovaSkill >= 3
			--or TeethSkill >= 3
			--or BoneArmorSkill >= 3
			--or PoisonDaggerSkill >= 3
			--or CorpseExplosionSkill >= 3
			--or BoneWallSkill >= 3
			--or PoisonExplosionSkill >= 3
			--or BonePrisonSkill >= 3
		)
	nsSkills =
		( RaiseSkeletonSkill >= 3
			--or SkeletonMasterySkill >= 3
			--or ClayGolemSkill >= 3
			--or GolemMasterySkill >= 3
			--or RaiseSkeletalMageSkill >= 3
			--or BloodgolemSkill >= 3
			--or SummonResistSkill >= 3
			--or IronGolemSkill >= 3
			--or FiregolemSkill >= 3
			--or ReviveSkill >= 3
		)
	curSkills =
		( LowerResistSkill >= 3
			--or DecrepifySkill >= 3
			--or AmplifyDamageSkill >= 3
			--or DimVisionSkill >= 3
			--or WeakenSkill >= 3
			--or IronMaidenSkill >= 3
			--or TerrorSkill >= 3
			--or ConfuseSkill >= 3
			--or LifeTapSkill >= 3
			--or AttractSkill >= 3
		)	
	return
		( not Ethereal
			and hasSockets("02")
			and ( pnbSkills
				--or nsSkills
				--or curSkills
				)
		--or Ethereal
			--and Sockets == 0
			--and DefensePercent == 0
			--and MaxDurabilityPercent == 0
			--and ( pnbSkills
				--or nsSkills
				--or curSkills )
		)
end 

white.shields =
{ checkStats = true,
--[[ Normal Shields
	["sml"] = anyGoodShield, -- Small Shield
	["lrg"] = anyGoodShield, -- Large Shield
	["kit"] = anyGoodShield, -- Kite Shield
	["spk"] = anyGoodShield, -- Spiked Shield
	["tow"] = anyGoodShield, -- Tower Shield
	["bsh"] = anyGoodShield, -- Bone Shield
	["gts"] = anyGoodShield, -- Gothic Shield
--]]

--[[ Exceptional Shields
	["xml"] = anyGoodShield, -- Round Shield
	["xrg"] = anyGoodShield, -- Scutum
	["xit"] = anyGoodShield, -- Dragon Shield
	["xpk"] = anyGoodShield, -- Barbed Shield
	["xow"] = anyGoodShield, -- Pavise
	["xsh"] = anyGoodShield, -- Grim Shield
	["xts"] = anyGoodShield, -- Ancient Shield
--]]

---[[ Elite Shields
	--["uuc"] = anyGoodShield, -- Heater
	--["uml"] = anyGoodShield, -- Luna
	--["urg"] = goodThreeSocket, -- Hyperion
	["uit"] = goodFourSocket, -- Monarch
	--["upk"] = goodThreeSocket, -- Blade Barrier
	--["uow"] = goodFourSocket, -- Aegis
	["ush"] = goodThreeSocket, -- Troll Nest
	--["uts"] = goodFourSocket, -- Ward


--]]

--[[ Paladin Shields "Normal"
	["pa1"] = goodPalShield, -- Targe
	["pa2"] = goodPalShield, -- Rondache
	["pa3"] = goodPalShield, -- Heraldic Shield
	["pa4"] = goodPalShield, -- Aerin Shield
	["pa5"] = goodPalShield, -- Crown Shield
--]]

--[[ Paladin Shields "Exceptional" 
	--["pa6"] = goodPalShield, -- Akaran Targe
	--["pa7"] = goodPalShield, -- Akaran Rondache
	--["pa8"] = goodPalShield, -- Protector Shield
	["pa9"] = goodPalShield, -- Gilded Shield
	["paa"] = goodPalShield, -- Royal Shield
--]]

---[[ Paladin Shields "Elite"
	["pab"] = goodPalShield, -- Sacred Targe
	--["pac"] = goodPalShield, -- Sacred Rondache
	--["pad"] = goodPalShield, -- Kurast Shield
	--["pae"] = goodPalShield, -- Zakarum Shield
	["paf"] = goodPalShield, -- Vortex Shield
--]]
	
---[[ Necromancer Shields 
	["VoodooHeads"] =  
		{ priority = 6,
			isGoodItem = isGoodNecShield
		},
--]]
}