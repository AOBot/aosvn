-- expansion\white\keys.lua

local key =
{ priority = 5,
	pickLimit = 3
}

white.keys =
{
	--["pk1"] = key, -- Key Of Terror
	--["pk2"] = key, -- Key Of Hate
	--["pk3"] = key, -- Key Of Destruction
	
	["dhn"] = {priority = 9}, -- Diablos' Horn
	["bey"] = {priority = 9}, -- Baals' Eye
	["mbr"] = {priority = 9}, -- Mephistos' Brain
	
	["toa"] = {priority = 9}, -- Token of Absolution
	
	--["tes"] = {priority = 9}, -- Suffering Essence
	--["ceh"] = {priority = 9}, -- Hatred Essence
	--["bet"] = {priority = 9}, -- Terror Esence
	--["fed"] = {priority = 9}, -- Destruction Essence
}