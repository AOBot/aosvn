-- expansion\white\runes.lua "strict"

white.runes =
{
	["r33"] = {priority = 9}, -- Zod
	["r32"] = {priority = 9}, -- Cham
	["r31"] = {priority = 9}, -- Jah
	["r30"] = {priority = 9}, -- Ber
	["r29"] = {priority = 9}, -- Sur
	["r28"] = {priority = 9}, -- Lo
	["r27"] = {priority = 9}, -- Ohm
	["r26"] = {priority = 9}, -- Vex
	["r25"] = {priority = 8}, -- Gul
	["r24"] = {priority = 8}, -- Ist
	["r23"] = {priority = 8}, -- Mal
	["r22"] = {priority = 7}, -- Um
	["r21"] = {priority = 7}, -- Pul
	--["r20"] = {priority = 6, pickLimit = 3}, -- Lem
	--["r19"] = {priority = 5, pickLimit = 3}, -- Fal
	--["r18"] = {priority = 4, pickLimit = 3}, -- Ko
	--["r17"] = {priority = 2, pickLimit = 3}, -- Lum
	--["r16"] = {priority = 2, pickLimit = 3}, -- Io
	--["r15"] = {priority = 2, pickLimit = 3}, -- Hel
	--["r14"] = {priority = 2, pickLimit = 3}, -- Dol
	--["r13"] = {priority = 2, pickLimit = 3}, -- Shael
	--["r12"] = {priority = 3, pickLimit = 3}, -- Sol
	--["r11"] = {priority = 3, pickLimit = 3}, -- Amn
	--["r10"] = {priority = 2, pickLimit = 1}, -- Thul
	--["r09"] = {priority = 2, pickLimit = 3}, -- Ort
	--["r08"] = {priority = 3, pickLimit = 3}, -- Ral
	--["r07"] = {priority = 3, pickLimit = 3}, -- Tal
	--["r06"] = {priority = 3, pickLimit = 3}, -- Ith
	--["r05"] = {priority = 3, pickLimit = 3}, -- Eth
	--["r04"] = {priority = 3, pickLimit = 3}, -- Nef
	--["r03"] = {priority = 3, pickLimit = 3}, -- Tir
	--["r02"] = {priority = 3, pickLimit = 3}, -- Eld
	--["r01"] = {priority = 3, pickLimit = 3}, -- El
}