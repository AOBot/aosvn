-- expansion\white\weapons.lua "strict"

----------------------------------------------------
-------------------- Settings ----------------------
----------------------------------------------------

local minEDam = 15

local goodCastWeapon =
{ priority = 7,
	goodItem =
		( hasSockets("45")
			and Ethereal
		)
}

local goodMeleeWeapon =
{ priority = 7,
	goodItem =
		( Ethereal
			and hasSockets("0456")
			--and MaxDamagePercent >= minEDam
		or not Ethereal
			and hasSockets("0456")
			and MaxDamagePercent >= minEDam
		)
}

local goodMissleWeapon =
{ priority = 7,
	goodItem =
		( hasSockets("04")
			and MaxDamagePercent >= minEDam
		)
}

local botdOnlyWeapon =
{ priority = 7,
	goodItem =
		( Ethereal
			and hasSockets("06")
			and MaxDamagePercent >= minEDam
		)
}

local goodMercWeapon =
{ priority = 7,
	goodItem =
		( Ethereal
			and hasSockets("045")
			and MaxDamagePercent >= minEDam
		)
}

local anyGoodWeapon =
{ priority = 7,
	goodItem =
		( Ethereal
			and not Sockets == 1
			--and MaxDamagePercent >= minEDam
		or not Ethereal
			and not Sockets == 1
			and MaxDamagePercent >= minEDam
		)
}

----------------------------------------------------
--------------------- Weapons ----------------------
----------------------------------------------------

white.weapons =
{ checkStats = true,
----------------------------------------------------
-- Class-dependant Weapons
----------------------------------------------------
---[[ Paladin Skill Scepters
	["Scepter"] = 
		{ priority = 7,
			goodItem =
				( isItemInList{"wsp", "9ws", "7ws"} -- 4-5soc max scepters
						and hasSockets("045")
						and	( ConvictionSkill >= 3 and FistOfTheHeavensSkill >= 3 )
						or ( ZealSkill >= 3 and FanaticismSkill >= 3 and SmiteSkill >= 3)
						--or HolyShieldSkill >= 3
				)
		},
--]]

---[[ Amazon Skill Bows
	["AmazonBow"] = 
		{ priority = 7,
			goodItem =
				( isItemInList{"amb", "amc"} -- mat, gmat
						and hasSockets("4")
						and SkillTabBonus >= 3
						and MaxDamagePercent >= 15
				)
		},
--]]

---[[ "Chaos" Runeword Claws
	["HandToHand2"] = 
		{ priority = 7,
			goodItem =
				( hasSockets("03")
						and LightningSentrySkill >= 3
						or (MindBlastSkill >= 3 and DragonFlightSkill >= 1)
						--or (MindBlastSkill + DragonFlightSkill + WeaponBlockSkill) >= 1
						--and VenomSkill >= 3
						--and MaxDamagePercent >= minEDam
				)
		},
--]]

--[[ "Memory" Runeword Staves (ES Staff)
	["Staff"] = 
		{ priority = 7,
			goodItem =
				( isItemInList{"gst", "bst", "8cs", "8bs", "6cs", "6bs"} -- 4soc max staves
						and hasSockets("04")
						and EnergyShieldSkill >= 3
				)
		},
--]]

---[[ "White" Runeword Wands
	["Wand"] = 
		{ priority = 7,
			goodItem =
				( not isItemInList{"wnd", "ywn", "9wn"} -- not 1soc max wands
						and hasSockets("02")
						and (BoneSpiritSkill + BoneSpearSkill) >= 6
				)
		},
--]]

----------------------------------------------------
-- Axes 
----------------------------------------------------
--[[ Normal Axes
	["hax"] = anyGoodWeapon, -- Hand Axe
	["axe"] = anyGoodWeapon, -- Axe
	["2ax"] = anyGoodWeapon, -- Double Axe
	["mpi"] = anyGoodWeapon, -- Military Pick
	["wax"] = anyGoodWeapon, -- War Axe
	["lax"] = anyGoodWeapon, -- Large Axe
	["bax"] = anyGoodWeapon, -- Broad Axe
	["btx"] = anyGoodWeapon, -- Battle Axe
	["gax"] = anyGoodWeapon, -- Great Axe
	["gix"] = anyGoodWeapon, -- Giant Axe
--]]

--[[ Exceptional Axes
	["9ha"] = anyGoodWeapon, -- Hatchet
	["9ax"] = anyGoodWeapon, -- Cleaver
	["92a"] = anyGoodWeapon, -- Twin Axe
	["9mp"] = anyGoodWeapon, -- Crowbill
	["9wa"] = anyGoodWeapon, -- Naga
	["9la"] = anyGoodWeapon, -- Military Axe
	["9ba"] = anyGoodWeapon, -- Bearded Axe
	["9bt"] = anyGoodWeapon, -- Tabar
	["9ga"] = anyGoodWeapon, -- Gothic Axe
	["9gi"] = anyGoodWeapon, -- Ancient Axe
--]]

---[[ Elite Axes
    --["7ha"] = anyGoodWeapon, -- Tomahalk
	--["7ax"] = anyGoodWeapon, -- Small Crescent
	--["72a"] = goodMeleeWeapon, -- Ettin Axe
	--["7mp"] = goodMeleeWeapon, -- War Spike
	["7wa"] = goodMeleeWeapon, -- Berserker Axe
	--["7la"] = anyGoodWeapon, -- Feral Axe
	--["7ba"] = anyGoodWeapon, -- Silver Edged Axe
	--["7bt"] = anyGoodWeapon, -- Decapitator
	--["7ga"] = botdOnlyWeapon, -- Champion Axe
	--["7gi"] = botdOnlyWeapon, -- Glorious Axe
--]]

----------------------------------------------------
-- Bows
----------------------------------------------------
--[[ Normal Bows
	["sbw"] = anyGoodWeapon, -- Short Bow
	["hbw"] = anyGoodWeapon, -- Hunter's Bow
	["lbw"] = anyGoodWeapon, -- Long Bow
	["cbw"] = anyGoodWeapon, -- Composite Bow
	["sbb"] = anyGoodWeapon, -- Short Battle Bow
	["lbb"] = anyGoodWeapon, -- Long Battle Bow
	["swb"] = anyGoodWeapon, -- Short War Bow
	["lwb"] = anyGoodWeapon, -- Long War Bow
--]]

--[[ Exceptional Bows
	["8sb"] = anyGoodWeapon, -- Edge Bow
	["8hb"] = anyGoodWeapon, -- Razor Bow
	["8lb"] = anyGoodWeapon, -- Cedar Bow
	["8cb"] = anyGoodWeapon, -- Double Bow
	["8s8"] = anyGoodWeapon, -- Short Siege Bow
	["8l8"] = anyGoodWeapon, -- Large Siege Bow
	["8sw"] = anyGoodWeapon, -- Rune Bow
	["8lw"] = anyGoodWeapon, -- Gothic Bow
--]]

---[[ Elite Bows
	--["6sb"] = anyGoodWeapon, -- Spider Bow
	--["6hb"] = anyGoodWeapon, -- Blade Bow
	--["6lb"] = anyGoodWeapon, -- Shadow Bow
	["6cb"] = goodMissleWeapon, -- Great Bow
	--["6s7"] = anyGoodWeapon, -- Diamond Bow
	--["6l7"] = anyGoodWeapon, -- Crusader Bow
	--["6sw"] = anyGoodWeapon, -- Ward Bow
	--["6lw"] = goodMissleWeapon, -- Hydra Bow
--]]

----------------------------------------------------
-- Crossbows
----------------------------------------------------
--[[ Normal Crossbows
	["lxb"] = anyGoodWeapon, -- Light Crossbow
	["mxb"] = anyGoodWeapon, -- Crossbow
	["hxb"] = anyGoodWeapon, -- Heavy Crossbow
	["rxb"] = anyGoodWeapon, -- Repeating Crossbow
--]]

--[[ Exceptional Crossbows
	["8lx"] = anyGoodWeapon, -- Arbalest
	["8mx"] = anyGoodWeapon, -- Siege Crossbow
	["8hx"] = anyGoodWeapon, -- Ballista
	["8rx"] = anyGoodWeapon, -- Chu-Ko-Nu
--]]

--[[ Elite Crossbows
	["6lx"] = anyGoodWeapon, -- Pellet Bow
	["6mx"] = anyGoodWeapon, -- Gorgon Crossbow
	["6hx"] = anyGoodWeapon, -- Colossus Crossbow
	["6rx"] = anyGoodWeapon, -- Demon Crossbow
--]]

----------------------------------------------------
-- Daggers
----------------------------------------------------
--[[ Normal Daggers
	["dgr"] = anyGoodWeapon, -- Dagger
	["dir"] = anyGoodWeapon, -- Dirk
	["kri"] = anyGoodWeapon, -- Kris
	["bld"] = anyGoodWeapon, -- Blade
--]]

--[[ Exceptional Daggers
	["9dg"] = anyGoodWeapon, -- Poignard
	["9di"] = anyGoodWeapon, -- Rondel
	["9kr"] = anyGoodWeapon, -- Cinquedeas
	["9bl"] = anyGoodWeapon, -- Stiletto
--]]

--[[ Elite Daggers
	["7dg"] = anyGoodWeapon, -- Bone Knife
	["7di"] = anyGoodWeapon, -- Mithral Point
	["7kr"] = anyGoodWeapon, -- Fanged Knife
	["7bl"] = anyGoodWeapon, -- Legend Spike
--]]

----------------------------------------------------
-- Maces 
----------------------------------------------------
---[[ Normal Maces
	--["clb"] = anyGoodWeapon, -- Club
	--["spc"] = anyGoodWeapon, -- Spiked Club
	--["mac"] = anyGoodWeapon, -- Mace
	--["mst"] = anyGoodWeapon, -- Morning Star
	["fla"] = goodCastWeapon, -- Flail
	--["whm"] = anyGoodWeapon, -- War Hammer
	--["mau"] = anyGoodWeapon, -- Maul
	--["gma"] = anyGoodWeapon, -- Great Maul
--]]

--[[ Exceptional Maces
	["9cl"] = anyGoodWeapon, -- Cudgel
	["9sp"] = anyGoodWeapon, -- Barbed Club
	["9ma"] = anyGoodWeapon, -- Flanged Mace
	["9mt"] = anyGoodWeapon, -- Jagged Star
	["9fl"] = anyGoodWeapon, -- Knout
	["9wh"] = anyGoodWeapon, -- Battle Hammer
	["9m9"] = anyGoodWeapon, -- War Club
	["9gm"] = anyGoodWeapon, -- Martel de Fer
--]]

---[[ Elite Maces
    --["7cl"] = anyGoodWeapon, -- Truncheon
	--["7sp"] = anyGoodWeapon, -- Tyrant Club
	--["7ma"] = anyGoodWeapon, -- Reinforced Mace
	--["7mf"] = anyGoodWeapon, -- Devil Star
	["7fl"] = goodMeleeWeapon, -- Scourge
	--["7wh"] = anyGoodWeapon, -- Legendary Mallet
	--["7m7"] = botdOnlyWeapon, -- Ogre Maul
	["7gm"] = botdOnlyWeapon, -- Thunder Maul
--]]

----------------------------------------------------
-- Polearms
----------------------------------------------------
--[[ Normal Polearms
	["bar"] = anyGoodWeapon, -- Bardiche
	["vou"] = anyGoodWeapon, -- Voulge
	["scy"] = anyGoodWeapon, -- Scythe
	["pax"] = anyGoodWeapon, -- Poleaxe
	["hal"] = anyGoodWeapon, -- Halberd
	["wsc"] = anyGoodWeapon, -- War Scythe
--]]

--[[ Exceptional Polearms
	["9b7"] = anyGoodWeapon, -- Lochaber Axe
	["9vo"] = goodMercWeapon, -- Bill
	["9s8"] = goodMercWeapon, -- Battle Scythe
	["9pa"] = goodMercWeapon, -- Partizan
	["9h9"] = goodMercWeapon, -- Bec-De-Corbin
	["9wc"] = goodMercWeapon, -- Grim Scythe
--]]

---[[ Elite Polearms
	--["7o7"] = anyGoodWeapon, -- Ogre Axe
	["7vo"] = goodMercWeapon, -- Colossus Voulge
	["7s8"] = goodMercWeapon, -- Thresher
	["7pa"] = goodMercWeapon, -- Cryptic Axe
	["7h7"] = goodMercWeapon, -- Great Poleaxe
	["7wc"] = goodMercWeapon, -- Giant Thresher
--]]

----------------------------------------------------
-- Spears 
----------------------------------------------------
--[[ Normal Spears
	["spr"] = anyGoodWeapon, -- Spear
	["tri"] = anyGoodWeapon, -- Trident
	["brn"] = anyGoodWeapon, -- Brandistock
	["spt"] = anyGoodWeapon, -- Spetum
	["pik"] = anyGoodWeapon, -- Pike
--]]

--[[ Exceptional Spears
	["9sr"] = anyGoodWeapon, -- War Spear
	["9tr"] = anyGoodWeapon, -- Fuscina
	["9br"] = anyGoodWeapon, -- War Fork
	["9st"] = anyGoodWeapon, -- Yari
	["9p9"] = anyGoodWeapon, -- Lance
--]]

---[[ Elite Spears
	--["7sr"] = anyGoodWeapon, -- Hyperion Spear
	--["7tr"] = anyGoodWeapon, -- Stygian Pike
	--["7br"] = anyGoodWeapon, -- Mancatcher
	--["7st"] = botdOnlyWeapon, -- Ghost Spear
	["7p7"] = botdOnlyWeapon, -- War Pike
--]]

----------------------------------------------------
-- Swords
----------------------------------------------------
---[[ Normal Swords
	--["ssd"] = anyGoodWeapon, -- Short Sword
	--["scm"] = anyGoodWeapon, -- Scimitar
	--["sbr"] = anyGoodWeapon, -- Sabre
	--["flc"] = anyGoodWeapon, -- Falchion
	["crs"] = goodCastWeapon, -- Crystal Sword
	--["bsd"] = anyGoodWeapon, -- Broad Sword
	--["lsd"] = anyGoodWeapon, -- Long Sword
	--["wsd"] = anyGoodWeapon, -- War Sword
	--["2hs"] = anyGoodWeapon, -- Two-Handed Sword
	--["clm"] = anyGoodWeapon, -- Claymore
	--["gis"] = anyGoodWeapon, -- Giant Sword
	--["bsw"] = anyGoodWeapon, -- Bastard Sword
	--["flb"] = anyGoodWeapon, -- Flamberge
	--["gsd"] = anyGoodWeapon, -- Great Sword
--]]

--[[ Exceptional Swords
	["9ss"] = anyGoodWeapon, -- Gladius
	["9sm"] = anyGoodWeapon, -- Cutlass
	["9sb"] = anyGoodWeapon, -- Shamshir
	["9fc"] = anyGoodWeapon, -- Tulwar
	["9cr"] = anyGoodWeapon, -- Dimensional Blade
	["9bs"] = anyGoodWeapon, -- Battle Sword
	["9ls"] = anyGoodWeapon, -- Rune Sword
	["9wd"] = anyGoodWeapon, -- Ancient Sword
	["92h"] = anyGoodWeapon, -- Espandon
	["9cm"] = anyGoodWeapon, -- Dacian Falx
	["9gs"] = anyGoodWeapon, -- Tusk Sword
	["9b9"] = anyGoodWeapon, -- Gothic Sword
	["9fb"] = anyGoodWeapon, -- Zweihander
	["9gd"] = anyGoodWeapon, -- Executioner Sword
--]]

---[[ Elite Swords
	--["7ss"] = anyGoodWeapon, -- Falcata
	--["7sm"] = anyGoodWeapon, -- Ataghan
	--["7sb"] = anyGoodWeapon, -- Elegant Blade
	--["7fc"] = anyGoodWeapon, -- Hydra Edge
	["7cr"] = goodMeleeWeapon, -- Phase Blade
	--["7bs"] = goodMeleeWeapon, -- Conquest Sword
	["7ls"] = goodMeleeWeapon, -- Cryptic Sword
	--["7wd"] = anyGoodWeapon, -- Mythical Sword
	--["72h"] = anyGoodWeapon, -- Legend Sword
	--["7cm"] = anyGoodWeapon, -- Highland Blade
	--["7gs"] = anyGoodWeapon, -- Balrog Blade
	--["7b7"] = anyGoodWeapon, -- Champion Sword
	--["7fb"] = goodMeleeWeapon, -- Colossal Sword
	["7gd"] = goodMeleeWeapon, -- Colossus Blade
--]]

}