-- expansion\white\armors.lua "strict"

local goodWhiteOrGlitchArmor =
{ priority = 7,
	goodItem =
		( not Ethereal
			and hasSockets("034")
			and DefensePercent >= 15
		or Ethereal
			and Sockets == 0
			and DefensePercent == 0
			and MaxDurabilityPercent == 0
			and BaseDefense >= 750
		)
}

local goodWhiteArmor =
{ priority = 7,
	goodItem =
		( not Ethereal
			and hasSockets("034")
			and DefensePercent >= 15
		)
}

local goodGlitchArmor =
{ priority = 7,
	goodItem =
	    ( Ethereal
			and Sockets == 0
			and DefensePercent == 0
			and MaxDurabilityPercent == 0
			and BaseDefense >= 850
		)
}

local anyGoodArmor =
{ priority = 7,
	goodItem =
		( not Ethereal
			and hasSockets("0234")
			and DefensePercent >= 15
		or Ethereal
			and Sockets == 0
			and DefensePercent == 0
			and MaxDurabilityPercent == 0
		)
}

local greatWhiteArmor =
{ priority = 7,
	goodItem =
		( not Ethereal
			and hasSockets("034")
			and DefensePercent >= 15
		)
}

white.armors =
{ checkStats = true,
---[[ Normal Armors
	--["qui"] = anyGoodArmor, -- Quilted Armor
	--["lea"] = anyGoodArmor, -- Leather Armor
	--["hla"] = anyGoodArmor, -- Hard Leather Armor
	--["stu"] = anyGoodArmor, -- Studded Leather
	--["rng"] = anyGoodArmor, -- Ring Mail
	--["scl"] = anyGoodArmor, -- Scale Mail
	--["brs"] = greatWhiteArmor, -- Breast Plate
	--["chn"] = anyGoodArmor, -- Chain Mail
	--["spl"] = anyGoodArmor, -- Splint Mail
	--["ltp"] = greatWhiteArmor, -- Light Plate
	--["plt"] = anyGoodArmor, -- Plate Mail
	--["fld"] = anyGoodArmor, -- Field Plate
	--["gth"] = anyGoodArmor, -- Gothic Plate
	--["ful"] = anyGoodArmor, -- Full Plate Mail
	--["aar"] = anyGoodArmor, -- Ancient Armor
--]]

---[[ Exceptional Armors
	--["xui"] = anyGoodArmor, -- Ghost Armor
	--["xea"] = anyGoodArmor, -- Serpentskin Armor
	--["xla"] = anyGoodArmor, -- Demonhide Armor
	--["xtu"] = anyGoodArmor, -- Trellised Armor
	--["xcl"] = anyGoodArmor, -- Tigulated Mail
	--["xng"] = anyGoodArmor, -- Linked Mail
	--["xrs"] = greatWhiteArmor, -- Cuirass
	--["xhn"] = anyGoodArmor, -- Mesh Armor
	--["xpl"] = anyGoodArmor, -- Russet Armor
	["xtp"] = greatWhiteArmor, -- Mage Plate
	--["xlt"] = anyGoodArmor, -- Templar Coat
	--["xld"] = anyGoodArmor, -- Sharktooth Armor
	--["xth"] = anyGoodArmor, -- Embossed Plate
	--["xul"] = anyGoodArmor, -- Chaos Armor
	--["xar"] = anyGoodArmor, -- Ornate Plate
--]]

---[[ Elite Armors
	--["uui"] = goodWhiteArmor, -- Dusk Shroud
	--["uea"] = goodWhiteArmor, -- Wyrmhide
	--["ula"] = goodWhiteArmor, -- Scarab Husk
	--["utu"] = goodWhiteArmor, -- Wire Fleece
	--["ung"] = goodGlitchArmor, -- Diamond Mail
	--["ucl"] = goodGlitchArmor, -- Loricated Mail
	--["urs"] = goodGlitchArmor, -- Great Hauberk
	--["uhn"] = goodGlitchArmor, -- Boneweave
	--["upl"] = goodGlitchArmor, -- Balrog Skin
	["utp"] = goodWhiteOrGlitchArmor, -- Archon Plate
	--["uld"] = goodGlitchArmor, -- Kraken Shell
	--["ult"] = goodGlitchArmor, -- Hellforge Plate
	--["uth"] = goodGlitchArmor, -- Lacquered Plate
	--["uul"] = goodGlitchArmor, -- Shadow Plate
	["uar"] = goodGlitchArmor, -- Sacred Armor
--]]
}