loadfile("scripts\\includes.lua")()
IncludeSettings()
IncludeLibs()
--[[    Item Functions  ]]--
Routine = function()
	local t = os.clock()
	Ao.commands:lock()
	Log(1, "timerpick", "locked commands "..os.clock()-t)
	repeat
		local item = Ao.pickit:pop()
		local picked = Pick(item)
		Log(1, "timerpick", "Picked Item? "..os.clock()-t)
		if item.baseItem.code == "gld" or item.baseItem.code == "tsc" or item.baseItem.code == "isc" or Items:GetPotType(item.baseItem.code) ~= PotionType.Error then
		else
			if picked then
				ItemMessage("Picked : ", item)
			else
				ItemMessage("Missed : ", item)
			end
		end
		Log(1, "timerpick", "done chatting "..os.clock()-t)
	until Ao.pickit:empty() or item.uid == 0
	Log(1, "timerpick", "unlocking commands "..os.clock()-t)
	Ao.commands:unlock()
end

Pick = function(item)
	local t = os.clock()
	if IsFullInv(item) then
		if Settings.Bot.Public then
			Dump()
		else
			QuickSell()
		end
	end
	Log(1, "timerpick", "Now Fitting Item "..os.clock()-t)
	repeat
		local dist = distance(Ao.me.x, Ao.me.y, item.x, item.y)
		if dist > 50 then
			Log(1, "timerpick", "distance too far "..os.clock()-t)
			return false
		elseif dist > 5 then
			Move.ToLocation(item.x, item.y)
			Log(1, "timerpick", "moved to pick "..os.clock()-t)
		end
		Ao:PickItem(4, item.uid, false)
		--Ao:WaitForItem(750)
		Sleep(150)
		Log(1, "timerpick", "actual pick of item "..os.clock()-t)
	until (not Ao.items:isOnGround(item.uid)) or inventory.FindByUID(item.uid)
	if item.baseItem.code == "gld" or item.baseItem.code == "tsc" or item.baseItem.code == "isc" or Items:GetPotType(item.baseItem.code) ~= PotionType.Error then
		Log(1, "timerpick", "auto return for pots/scrolls/gold "..os.clock()-t)
		return true
	elseif inventory.FindByUID(item.uid) then 
		if Settings.Inventory.DumpItems then
			Dump()
		end
		Log(1, "timerpick", "dumped and done "..os.clock()-t)
		return true
	end
	Log(1, "timerpick", "odd... "..os.clock()-t)
	return false
end

--[[    End Item Functions      ]]--
--[[    Inventory Management    ]]--
function IsFullInv(item)
        if (item.baseItem.code ~= "gld" and item.baseItem.code ~= "tsc" and item.baseItem.code ~= "isc" and Items:GetPotType(item.baseItem.code) == PotionType.Error) then
                local inv = Ao.me.inventory:findLocation(item)
                if (inv == nil or inv.x < 0 or inv.y < 0 or inv.x > 9 or inv.y > 3) then
                        return true
                end
        end
        
        return false
end

function IsInInv(item)
        local inv = Ao.me.inventory:findByCode(item.baseItem.code)
        
        if (inv ~= nil) then
                for i = 0, inv:size() - 1 do
                        if (inv[i].uid == item.uid) then
                                return true
                        end
                end
        end
        
        return false
end

function QuickSell()

        Move.ToTown()
        
        local x = Ao.me.x
        local y = Ao.me.y
        
        local ibk = Ao.me.inventory:findByCode("ibk")            
        Barter() -- might as well do it all if were doing it
        Stash(true)   --Will not cube runes/gems/charms when Quickselling
        
        Move.ToLocation(x, y)
        
        local portal = 0
        
        repeat
                portal = Ao.objects:FindTownPortalByOwner(Ao.me.uid)
                if (portal == 0) then Ao:Sleep(500) end
                --Ao:Chat(string.format("portal.uid = %d", portal))
        until (portal ~= 0)
  
        local count = 0
  
        repeat
                Ao:InitWaitingPacket(GS["MapAdd"])
                Ao:UnitInteract(UnitType.GameObject, portal);
        Ao:WaitForInitPacket(3000)
                if (Ao.maps:inTown()) then Ao:Sleep(100) end
                Ao:UpdatePosition(Ao.me.x, Ao.me.y)
                Ao:RequestReassign(UnitType.Player, Ao.me.uid)
                count = count + 1
        until (not Ao.maps:inTown() or count > 10)
        
        Log(2, "Awesom-O", "Back To Picking", true)
        
end
--[[    End Inventory Management        ]]--
 
