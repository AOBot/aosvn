Files inside the "Reference" folder often contain numbers, these
	numbers are simply the index of the actual variable within the table
	passed from AO to Lua. You will most likely want to ignore these
	numbers.