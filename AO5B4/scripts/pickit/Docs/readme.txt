----------------------------------------------------------------------
-- readme.txt Index
----------------------------------------------------------------------
	1: Install
	2: Global Settings
	3: Adding/Removing Items
	4: Terms Explained
	5: Notes
----------------------------------------------------------------------
-- 1: Install
----------------------------------------------------------------------
	Backup your current "AO\config\lua" folder
	Delete your current "AO\config\lua" folder
	*Renaming your current lua folder to something like "lua_back"
		effectively completes the two above steps.
	Place the "lua" folder that came with your PR2 download into your
		"AO\config" folder
	Comment out or delete all of the lua settings in bot.ini
		(and awesom-o.ini if you have them there)
----------------------------------------------------------------------
-- 2: Global Settings
----------------------------------------------------------------------
	*Located in "expansion\includes.lua" and "classic\includes.lua"

	_MinGoldPile = integer (500 to ...)
		Default: 1000
		The minimum gold amount required for pickup.
		*You can set this below 500, but it is NOT recommended as doing
		so could create unwanted side effects.

	_CheckUniqueItemStats = true/false
		Default: true
		Setting this to false will cause PR to pick and keep all listed
		unique items unidentified, without checking the item stats.

	_SellUnlistedItems = true/false
		Default: false
		Setting this to true will cause PR to sell all unlisted items that
		happen to find their way into your inventory.
		*This is very useful when gambling items.
		*Please ensure that you have InventoryLock set up correctly in AO
		before attempting to use this feature.

	_UseItemLogging = true/false
		Default: true
		Currently, PR does not offer many item logging features.
		It does however provide basic unique item pick/skip logging.
		These logs should appear in your "AO\logs" folder.
		If you do not like seeing the unique item logs, or are concerned
		with PR's speed, simply set this option to false.

	_TestAllFiles = true/false
		Default: false
		Two of the main new features of PR include self-syntax checking
		and loading only required files for any given item.
		Setting this to true will allow you to force PR to syntax check
		every uncommented file in "includes.lua".
		*See "PR_testing.txt" for more information.

	_LogErrors = true/false
		Default: true
		Determines if PR should log syntax errors.
		If a syntax error is found, or _TestAllFiles = true, the file
		"PR_Error.log" should appear in your "AO\logs" folder.

	_LogWarnings = true/false
		Default: true
		Determines if PR should log items that are being handled by more
		than one itemTable (logs potentially undesired results).
		If an item is handled by more than one itemTable, the file
		"PR_Warning.log" should appear in your "AO\logs" folder.
----------------------------------------------------------------------
-- 3: Adding/Removing Items
----------------------------------------------------------------------
	*If you want to add an item that does not already have a file,
	see "itemFiles.txt".

	If you want to add or remove a whole group of items (such as gems),
	then simply comment or uncomment the line in "includes.lua" that
	contains the file.
	*Make sure you put the comment at the beginning of the line.

	If you want to remove a specific item in a file, then simply place a
	comment before the "[...] =" part of the itemEntry.
	*Setting the item priority to 0 will also effectively stop the item
	from being picked up.

	Likewise, if you want to add a specific item, simply uncomment the
	item entry.
	*If you want to add an item that doesn't already have an entry,
	see "itemEntries.txt".
----------------------------------------------------------------------
-- 4: Terms Explained
----------------------------------------------------------------------
	comments
		Anything on a single line after "--"
		*Block comments begin with "--[[" and end with "]]" and can span
		more than one line; "---[[" (note the 3 "-"s) does NOT begin a
		block comment, instead, it begins a line comment.

	itemFiles
		Files that contain item data
		*See "itemFiles.txt" for more information.

	itemTables
		Tables within itemFiles that contain specific items and PR
		settings for those items.
		*See "itemTables.txt" for more information.

	itemEntries
		Specific item settings within an itemTable.
		*See "itemEntries.txt" for more information.

	listed items
		Items that have an itemEntry in an itemTable
		(Items that are not commented out).

	unlisted items
		Items that have no itemEntry in an itemTable
		(Items that are commented out or simply aren't listed).
----------------------------------------------------------------------
-- 5: Notes
----------------------------------------------------------------------
	PR files from versions before 2.0 WILL NO LONGER WORK.
	PR automatically decides which files to use (classic/expansion).
	Classic item files have not yet been written.
	PR does NOT handle potion or scroll picking! AO handles this.


