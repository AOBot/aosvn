﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace ApplicationWindowEater
{
    class RealmClass
    {
        public static void SetRealm()
        {

            Microsoft.Win32.RegistryKey BattleNetKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Battle.net\\Configuration", true);
            string currentRealm;
            string[] data;
            data = (string[])BattleNetKey.GetValue("Diablo II Battle.net gateways");

            if (data != null)
            {
                if (((data.Length - 2) % 3) != 0)
                {
                    string[] gateways = new string[17];
                    gateways[0] = "1002";
                    gateways[1] = "05";
                    gateways[2] = "uswest.battle.net";
                    gateways[3] = "8";
                    gateways[4] = "U.S. West";
                    gateways[5] = "useast.battle.net";
                    gateways[6] = "6";
                    gateways[7] = " U.S. East";
                    gateways[8] = "asia.battle.net";
                    gateways[9] = "-9";
                    gateways[10] = "Asia";
                    gateways[11] = "europe.battle.net";
                    gateways[12] = "-1";
                    gateways[13] = "Europe";
                    gateways[14] = "localhost";
                    gateways[15] = "0";
                    gateways[16] = "Private Realm";
                    BattleNetKey.SetValue("Diablo II Battle.net gateways", gateways);
                }
                else
                {
                    int count = 0;
                    int line = 0;
                    bool found = false;
                    currentRealm = data[1];

                    while (!found)
                    {
                        if (count <= data.Length)
                        {
                            if ((count + 1) >= data.Length)
                            {
                                break;
                            }
                            line++;
                            if (string.Compare(data[count + 1], "localhost") == 0
                                || string.Compare(data[count + 1], "127.0.0.1") == 0)
                            {
                                found = true;
                                break;
                            }
                        }
                        count++;
                    }

                    if (found)
                    {
                        string proxyRealm = ((((line - 2) / 3) + 1).ToString());
                        if (proxyRealm.Length == 1)
                        {
                            proxyRealm = "0" + proxyRealm;
                        }
                        if (currentRealm == proxyRealm)
                        {
                            //Logging.WriteInfo("No need to change realm");
                        }
                        else
                        {
                            //Logging.WriteInfo("Not the correct realm \n Setting the correct realm");
                            data[1] = proxyRealm;
                            BattleNetKey.SetValue("Diablo II Battle.net gateways", data);
                            //Logging.WriteInfo("Set to the correct realm");
                        }
                    }
                    else
                    {
                        /*Logging.WriteInfo("Proxy realm non-existant");
                        Logging.WriteInfo("Creating a proxy realm");*/

                        int length = data.Length;
                        line = 0;
                        string[] newRealmKey = new string[length + 3];
                        foreach (var s in data)
                        {
                            newRealmKey[line] = s;
                            line++;
                        }
                        newRealmKey[line] = "localhost";
                        newRealmKey[line + 1] = "0";
                        newRealmKey[line + 2] = "Private Realm";
                        BattleNetKey.SetValue("Diablo II Battle.net gateways", newRealmKey);
                        SetRealm();

                        //Logging.WriteInfo("Proxy realm created");
                    }
                }
            }
        }

        // http://stackoverflow.com/questions/20797/how-to-split-a-byte-array
        private static void splitByteArray(byte[] source, out byte[] first, out byte[] second, int lengthOfFirst)
        {
            first = new byte[lengthOfFirst];
            second = new byte[source.Length - lengthOfFirst];
            Array.Copy(source, 0, first, 0, lengthOfFirst);
            Array.Copy(source, lengthOfFirst, second, 0, source.Length - lengthOfFirst);
        }

        // CTS ;)
        private static string[] bytesToStringArray(byte[] data)
        {
            List<string> ret = new List<string>();
            byte[] current;
            int counter = 0;
            int lastSpot = 0;

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == 0)
                {
                    //System.Text.Encoding.Default.GetString();
                    //current = new byte[counter];
                    //Array.Copy(data, lastSpot, current, 0, counter);
                    ret.Add(System.Text.Encoding.Default.GetString(data, lastSpot, counter));

                    lastSpot = i+1;
                    counter = 0;
                    continue;
                }
                else
                    counter++;
            }

            return ret.ToArray();
        }

        public static string GetRealm()
        {
            RegistryKey BattleNetKey = Registry.CurrentUser;
            BattleNetKey = BattleNetKey.OpenSubKey("Software\\Battle.net\\Configuration");


            string currentRealm = "default";
            int currentRealmIndex;
            string[] data = new string[0];
            var d = BattleNetKey.GetValue("Diablo II Battle.net gateways");
            RegistryValueKind kind = BattleNetKey.GetValueKind("Diablo II Battle.net gateways");

            if (kind.Equals(RegistryValueKind.Binary))
                data = bytesToStringArray((byte[])d);
            else if (kind.Equals(RegistryValueKind.MultiString))
                data = (string[])d;
            else
                throw new Exception("Kind UNKNOWN");

            currentRealmIndex = Int32.Parse(data[1]);
            currentRealmIndex *= 3;
            currentRealmIndex -= 1;

            currentRealm = data[currentRealmIndex];

            return currentRealm;
        }

    }
}
