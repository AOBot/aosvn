﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using Utilities.IniControl;
using Microsoft.Win32;

namespace ApplicationWindowEater
{
    // This is a wrapper class for the iniReader Class
    public class Settings
    {
        private IniReader ini;

        // Properties
        // Created properties to the Ini class to get/set values
        public string ManagerD2Path
        {
            get {
                string inipath = ini.ReadString("Manager", "D2Path", "C:\\Program Files (x86)\\Diablo II\\Game.exe");
                if (inipath == "auto") {
                    RegistryKey regKey = Registry.CurrentUser;
                    regKey = regKey.OpenSubKey("Software\\Blizzard Entertainment\\Diablo II");

                    if (regKey != null)
                    {
                        string diapath = (string)regKey.GetValue("InstallPath") + "Game.exe";
                        inipath = diapath;
                    }
                }
                return inipath; 
            }
            set { ini.Write("Manager", "D2Path", value); }
        }
        public string ManagerArguments
        {
            get { return ini.ReadString("Manager", "Arguments", System.IO.Directory.GetCurrentDirectory()); }
            set { ini.Write("Manager", "Arguments", value); }
        }
        public string ManagerAOPath
        {
            get { return ini.ReadString("Manager", "AOPath", System.IO.Directory.GetCurrentDirectory()); }
            set { ini.Write("Manager", "AOPath", value); }
        }
        public string ManagerRealm
        {
            get { return ini.ReadString("Manager", "Realm", System.IO.Directory.GetCurrentDirectory()); }
            set { ini.Write("Manager", "Realm", value); }
        }
        public string ManagerHideCore
        {
            get { return ini.ReadString("Manager", "HideCore", "true"); }
            set { ini.Write("Manager", "HideCore", value); }
        }
        public string ManagerStealthMode
        {
            get { return ini.ReadString("Manager", "StealthMode", "false"); }
            set { ini.Write("Manager", "StealthMode", value); }
        }
        public string ManagerStartDelay
        {
            get { return ini.ReadString("Manager", "StartDelay", "3000"); }
            set { ini.Write("Manager", "StartDelay", value); }
        }
            
        // Initializer
        public Settings()
        {
            ini = new IniReader(System.IO.Directory.GetCurrentDirectory() + "\\settings.ini");
        }
    }
}
