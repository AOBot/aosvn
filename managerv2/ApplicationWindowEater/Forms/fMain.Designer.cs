﻿namespace ApplicationWindowEater
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panelWindow1 = new System.Windows.Forms.Panel();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panelOverview1 = new System.Windows.Forms.Panel();
            this.panelOverview2 = new System.Windows.Forms.Panel();
            this.panelOverview3 = new System.Windows.Forms.Panel();
            this.panelOverview4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage0 = new System.Windows.Forms.TabPage();
            this.StoppAll = new System.Windows.Forms.Button();
            this.CpyLog = new System.Windows.Forms.Button();
            this.ClearLogs = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.BotE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Error = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Bot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsRunning = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Success = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Picked = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sold = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stashed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gambled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panelWindow2 = new System.Windows.Forms.Panel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panelWindow3 = new System.Windows.Forms.Panel();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panelWindow4 = new System.Windows.Forms.Panel();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.buttonStartCore = new System.Windows.Forms.Button();
            this.richTextBoxCoreConsole = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPath = new System.Windows.Forms.TextBox();
            this.Settings = new System.Windows.Forms.Button();
            this.BotSettings = new System.Windows.Forms.Button();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button1.Location = new System.Drawing.Point(317, 685);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Launch Window 1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button2.Location = new System.Drawing.Point(447, 685);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Launch Window 2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button3.Location = new System.Drawing.Point(577, 685);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Launch Window 3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button4.Location = new System.Drawing.Point(707, 685);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(124, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Launch Window 4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.tabPage2.Controls.Add(this.panelWindow1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(814, 614);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Window 1";
            // 
            // panelWindow1
            // 
            this.panelWindow1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelWindow1.Location = new System.Drawing.Point(6, 6);
            this.panelWindow1.Name = "panelWindow1";
            this.panelWindow1.Size = new System.Drawing.Size(800, 600);
            this.panelWindow1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.tabPage1.Controls.Add(this.panelOverview1);
            this.tabPage1.Controls.Add(this.panelOverview2);
            this.tabPage1.Controls.Add(this.panelOverview3);
            this.tabPage1.Controls.Add(this.panelOverview4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(814, 614);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Window Overview";
            // 
            // panelOverview1
            // 
            this.panelOverview1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelOverview1.Location = new System.Drawing.Point(6, 5);
            this.panelOverview1.Name = "panelOverview1";
            this.panelOverview1.Size = new System.Drawing.Size(397, 298);
            this.panelOverview1.TabIndex = 0;
            // 
            // panelOverview2
            // 
            this.panelOverview2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelOverview2.Location = new System.Drawing.Point(409, 5);
            this.panelOverview2.Name = "panelOverview2";
            this.panelOverview2.Size = new System.Drawing.Size(397, 298);
            this.panelOverview2.TabIndex = 1;
            // 
            // panelOverview3
            // 
            this.panelOverview3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelOverview3.Location = new System.Drawing.Point(6, 309);
            this.panelOverview3.Name = "panelOverview3";
            this.panelOverview3.Size = new System.Drawing.Size(397, 298);
            this.panelOverview3.TabIndex = 1;
            // 
            // panelOverview4
            // 
            this.panelOverview4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelOverview4.Location = new System.Drawing.Point(409, 309);
            this.panelOverview4.Name = "panelOverview4";
            this.panelOverview4.Size = new System.Drawing.Size(397, 298);
            this.panelOverview4.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage0);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(822, 640);
            this.tabControl1.TabIndex = 5;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage0
            // 
            this.tabPage0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.tabPage0.Controls.Add(this.StoppAll);
            this.tabPage0.Controls.Add(this.CpyLog);
            this.tabPage0.Controls.Add(this.ClearLogs);
            this.tabPage0.Controls.Add(this.dataGridView2);
            this.tabPage0.Controls.Add(this.dataGridView1);
            this.tabPage0.Location = new System.Drawing.Point(4, 22);
            this.tabPage0.Name = "tabPage0";
            this.tabPage0.Size = new System.Drawing.Size(814, 614);
            this.tabPage0.TabIndex = 4;
            this.tabPage0.Text = "Bot Overview";
            // 
            // StoppAll
            // 
            this.StoppAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.StoppAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StoppAll.ForeColor = System.Drawing.Color.DodgerBlue;
            this.StoppAll.Location = new System.Drawing.Point(264, 165);
            this.StoppAll.Name = "StoppAll";
            this.StoppAll.Size = new System.Drawing.Size(118, 40);
            this.StoppAll.TabIndex = 7;
            this.StoppAll.Text = "Stop all bots";
            this.StoppAll.UseVisualStyleBackColor = false;
            // 
            // CpyLog
            // 
            this.CpyLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.CpyLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CpyLog.ForeColor = System.Drawing.Color.DodgerBlue;
            this.CpyLog.Location = new System.Drawing.Point(141, 165);
            this.CpyLog.Name = "CpyLog";
            this.CpyLog.Size = new System.Drawing.Size(117, 40);
            this.CpyLog.TabIndex = 6;
            this.CpyLog.Text = "Copy logs to clipboard";
            this.CpyLog.UseVisualStyleBackColor = false;
            // 
            // ClearLogs
            // 
            this.ClearLogs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.ClearLogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearLogs.ForeColor = System.Drawing.Color.DodgerBlue;
            this.ClearLogs.Location = new System.Drawing.Point(17, 165);
            this.ClearLogs.Name = "ClearLogs";
            this.ClearLogs.Size = new System.Drawing.Size(118, 40);
            this.ClearLogs.TabIndex = 5;
            this.ClearLogs.Text = "Clear logs";
            this.ClearLogs.UseVisualStyleBackColor = false;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BotE,
            this.Error,
            this.Description,
            this.Time});
            this.dataGridView2.Location = new System.Drawing.Point(14, 214);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(786, 392);
            this.dataGridView2.TabIndex = 3;
            // 
            // BotE
            // 
            this.BotE.HeaderText = "Bot";
            this.BotE.Name = "BotE";
            // 
            // Error
            // 
            this.Error.HeaderText = "Error Code";
            this.Error.Name = "Error";
            this.Error.Width = 200;
            // 
            // Description
            // 
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.Width = 300;
            // 
            // Time
            // 
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Bot,
            this.IsRunning,
            this.Success,
            this.Picked,
            this.Sold,
            this.Stashed,
            this.Gambled});
            this.dataGridView1.Location = new System.Drawing.Point(17, 9);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(783, 150);
            this.dataGridView1.TabIndex = 2;
            // 
            // Bot
            // 
            this.Bot.HeaderText = "Bot";
            this.Bot.Name = "Bot";
            // 
            // IsRunning
            // 
            this.IsRunning.HeaderText = "Start/Stop";
            this.IsRunning.Name = "IsRunning";
            this.IsRunning.Text = "Start";
            // 
            // Success
            // 
            this.Success.HeaderText = "Success %";
            this.Success.Name = "Success";
            // 
            // Picked
            // 
            this.Picked.HeaderText = "Picked";
            this.Picked.Name = "Picked";
            // 
            // Sold
            // 
            this.Sold.HeaderText = "Sold";
            this.Sold.Name = "Sold";
            // 
            // Stashed
            // 
            this.Stashed.HeaderText = "Stashed";
            this.Stashed.Name = "Stashed";
            // 
            // Gambled
            // 
            this.Gambled.HeaderText = "Gambled";
            this.Gambled.Name = "Gambled";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.tabPage3.Controls.Add(this.panelWindow2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(814, 614);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Window 2";
            // 
            // panelWindow2
            // 
            this.panelWindow2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelWindow2.Location = new System.Drawing.Point(6, 6);
            this.panelWindow2.Name = "panelWindow2";
            this.panelWindow2.Size = new System.Drawing.Size(800, 600);
            this.panelWindow2.TabIndex = 2;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.tabPage4.Controls.Add(this.panelWindow3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(814, 614);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Window 3";
            // 
            // panelWindow3
            // 
            this.panelWindow3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelWindow3.Location = new System.Drawing.Point(6, 6);
            this.panelWindow3.Name = "panelWindow3";
            this.panelWindow3.Size = new System.Drawing.Size(800, 600);
            this.panelWindow3.TabIndex = 2;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.tabPage5.Controls.Add(this.panelWindow4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(814, 614);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "Window 4";
            // 
            // panelWindow4
            // 
            this.panelWindow4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelWindow4.Location = new System.Drawing.Point(6, 6);
            this.panelWindow4.Name = "panelWindow4";
            this.panelWindow4.Size = new System.Drawing.Size(800, 600);
            this.panelWindow4.TabIndex = 2;
            this.panelWindow4.Enter += new System.EventHandler(this.panel8_Enter);
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.tabPage6.Controls.Add(this.buttonStartCore);
            this.tabPage6.Controls.Add(this.richTextBoxCoreConsole);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(814, 614);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "AO Core";
            // 
            // buttonStartCore
            // 
            this.buttonStartCore.Location = new System.Drawing.Point(736, 588);
            this.buttonStartCore.Name = "buttonStartCore";
            this.buttonStartCore.Size = new System.Drawing.Size(75, 23);
            this.buttonStartCore.TabIndex = 1;
            this.buttonStartCore.Text = "Start Core";
            this.buttonStartCore.UseVisualStyleBackColor = true;
            this.buttonStartCore.Click += new System.EventHandler(this.buttonStartCore_Click);
            // 
            // richTextBoxCoreConsole
            // 
            this.richTextBoxCoreConsole.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.richTextBoxCoreConsole.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxCoreConsole.ForeColor = System.Drawing.Color.DarkOrange;
            this.richTextBoxCoreConsole.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxCoreConsole.Name = "richTextBoxCoreConsole";
            this.richTextBoxCoreConsole.ReadOnly = true;
            this.richTextBoxCoreConsole.Size = new System.Drawing.Size(808, 579);
            this.richTextBoxCoreConsole.TabIndex = 0;
            this.richTextBoxCoreConsole.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(14, 662);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Path (w/o game.exe) :";
            // 
            // textBoxPath
            // 
            this.textBoxPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.textBoxPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPath.ForeColor = System.Drawing.Color.DodgerBlue;
            this.textBoxPath.Location = new System.Drawing.Point(166, 659);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.Size = new System.Drawing.Size(665, 20);
            this.textBoxPath.TabIndex = 7;
            this.textBoxPath.Text = "C:\\Program Files\\Diablo II\\Game.exe";
            // 
            // Settings
            // 
            this.Settings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.Settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Settings.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Settings.Location = new System.Drawing.Point(17, 685);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(122, 23);
            this.Settings.TabIndex = 8;
            this.Settings.Text = "Manager Settings";
            this.Settings.UseVisualStyleBackColor = false;
            this.Settings.Click += new System.EventHandler(this.Settings_Click);
            // 
            // BotSettings
            // 
            this.BotSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.BotSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotSettings.ForeColor = System.Drawing.Color.DodgerBlue;
            this.BotSettings.Location = new System.Drawing.Point(146, 685);
            this.BotSettings.Name = "BotSettings";
            this.BotSettings.Size = new System.Drawing.Size(122, 23);
            this.BotSettings.TabIndex = 9;
            this.BotSettings.Text = "Bot Settings";
            this.BotSettings.UseVisualStyleBackColor = false;
            this.BotSettings.Click += new System.EventHandler(this.BotSettings_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.ClientSize = new System.Drawing.Size(849, 722);
            this.Controls.Add(this.BotSettings);
            this.Controls.Add(this.Settings);
            this.Controls.Add(this.textBoxPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.Text = "AO Manager";
            this.Activated += new System.EventHandler(this.fMain_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.tabPage2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panelWindow1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panelOverview1;
        private System.Windows.Forms.Panel panelOverview2;
        private System.Windows.Forms.Panel panelOverview4;
        private System.Windows.Forms.Panel panelOverview3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panelWindow2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panelWindow3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panelWindow4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPath;
        private System.Windows.Forms.Button Settings;
        private System.Windows.Forms.TabPage tabPage0;
        private System.Windows.Forms.Button StoppAll;
        private System.Windows.Forms.Button CpyLog;
        private System.Windows.Forms.Button ClearLogs;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn BotE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Error;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bot;
        private System.Windows.Forms.DataGridViewButtonColumn IsRunning;
        private System.Windows.Forms.DataGridViewTextBoxColumn Success;
        private System.Windows.Forms.DataGridViewTextBoxColumn Picked;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sold;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stashed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gambled;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.RichTextBox richTextBoxCoreConsole;
        private System.Windows.Forms.Button buttonStartCore;
        private System.Windows.Forms.Button BotSettings;
    }
}

