﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace ApplicationWindowEater
{
    public partial class fMain : Form
    {
        [DllImport("USER32.DLL")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
        [DllImport("USER32.dll")]
        private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);

        [DllImport("USER32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("USER32.dll")]
        static extern int ShowWindow(IntPtr hwnd, int nCmdShow);

        static uint style = (0x80000000 + 0x10000000);// WS_POPUP + WS_VISIBLE
        int d2style = (int)style;
        public int GWL_STYLE = -16;

        public Process _Window1;
        public Process _Window2;
        public Process _Window3;
        public Process _Window4;
        public bool _Window1Running = false;
        public bool _Window2Running = false;
        public bool _Window3Running = false;
        public bool _Window4Running = false;

        private Settings ini;

        // This is what runs on the form's initialization
        public fMain()
        {
            InitializeComponent(); // This is default to all forms, which loads the GUI code
            // This is where you should set values to gui elements if you're not doing some 
            //  static text
            ini = new ApplicationWindowEater.Settings();
            textBoxPath.Text = ini.ManagerD2Path;
        }

        public void HideDias(string Tab)
        {
            for (int i = 1; i  <= 4; i++)
            {
                if (i.ToString() != Tab)
                {
                    if (_Window1Running && i.ToString() != Tab && i == 1) { ShowWindow(_Window1.MainWindowHandle, 0); }
                    if (_Window2Running && i.ToString() != Tab && i == 2) { ShowWindow(_Window2.MainWindowHandle, 0); }
                    if (_Window3Running && i.ToString() != Tab && i == 3) { ShowWindow(_Window3.MainWindowHandle, 0); }
                    if (_Window4Running && i.ToString() != Tab && i == 4) { ShowWindow(_Window4.MainWindowHandle, 0); }
                }else{
                    if (_Window1Running && i.ToString() == Tab) { ShowWindow(_Window1.MainWindowHandle, 5); }
                    if (_Window2Running && i.ToString() == Tab) { ShowWindow(_Window2.MainWindowHandle, 5); }
                    if (_Window3Running && i.ToString() == Tab) { ShowWindow(_Window3.MainWindowHandle, 5); }
                    if (_Window4Running && i.ToString() == Tab) { ShowWindow(_Window4.MainWindowHandle, 5); }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LaunchDiabloWindow(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LaunchDiabloWindow(2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LaunchDiabloWindow(3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            LaunchDiabloWindow(4);
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Window1Running)
                _Window1.Kill();
            if (_Window2Running)
                _Window2.Kill();
            if (_Window3Running)
                _Window3.Kill();
            if (_Window4Running)
                _Window4.Kill();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                case 1:
                    ResizeWindows(true);
                    break;
                default:
                    ResizeWindows(false);
                    break;
            }
        }

        public void LaunchDiabloWindow(int window)
        {
            Process process = new Process();
            process.StartInfo.Arguments = "-w  -ns " + ini.ManagerArguments;
            process.StartInfo.FileName = textBoxPath.Text + "\\Game.exe";
            process.Start();
            process.WaitForInputIdle();
            SetWindowLong(process.MainWindowHandle, GWL_STYLE, d2style);

            switch (window)
            {
                case 1: ;
                    _Window1 = process;
                    _Window1Running = true;
                    SetParent(_Window1.MainWindowHandle, this.panelWindow1.Handle);
                    MoveWindow(_Window1.MainWindowHandle, 0, 0, 800, 600, true);
                    break;
                case 2:
                    _Window2 = process;
                    _Window2Running = true;
                    SetParent(_Window2.MainWindowHandle, this.panelWindow2.Handle);
                    MoveWindow(_Window2.MainWindowHandle, 0, 0, 800, 600, true);
                    break;
                case 3:
                    _Window3 = process;
                    _Window3Running = true;
                    SetParent(_Window3.MainWindowHandle, this.panelWindow3.Handle);
                    MoveWindow(_Window3.MainWindowHandle, 0, 0, 800, 600, true);
                    break;
                case 4:
                    _Window4 = process;
                    _Window4Running = true;
                    SetParent(_Window4.MainWindowHandle, this.panelWindow4.Handle);
                    MoveWindow(_Window4.MainWindowHandle, 0, 0, 800, 600, true);
                    break;
            }
        }

        public void ResizeWindows(bool overviewScreen)
        {
            if (overviewScreen)
            {
                //code
            }
            else
            {
               HideDias(tabControl1.SelectedIndex.ToString());
                if (_Window1Running)
                {
                    SetParent(_Window1.MainWindowHandle, this.panelWindow1.Handle);
                    MoveWindow(_Window1.MainWindowHandle, 0, 0, 800, 600, true);
                }
                if (_Window2Running)
                {
                    SetParent(_Window2.MainWindowHandle, this.panelWindow2.Handle);
                    MoveWindow(_Window2.MainWindowHandle, 0, 0, 800, 600, true);
                }
                if (_Window3Running)
                {
                    SetParent(_Window3.MainWindowHandle, this.panelWindow3.Handle);
                    MoveWindow(_Window3.MainWindowHandle, 0, 0, 800, 600, true);
                }
                if (_Window4Running)
                {
                    SetParent(_Window4.MainWindowHandle, this.panelWindow4.Handle);
                    MoveWindow(_Window4.MainWindowHandle, 0, 0, 800, 600, true);
                }
            }
        }

        private void Settings_Click(object sender, EventArgs e)
        {
            fSettings settings = new fSettings(this);
            settings.Show();
        }

        public void setPath(string path)
        {
            textBoxPath.Text = path;
        }

        private void setOnTop()
        {
            this.Activate();
        }

        private void fMain_Activated(object sender, EventArgs e)
        {
            Console.WriteLine("Test");
        }

        private void panel8_Enter(object sender, EventArgs e)
        {
            Console.WriteLine("Entered 8");
        }

        // Thanks to http://stackoverflow.com/questions/415620/redirect-console-output-to-textbox-in-separate-program-c-sharp
        private delegate void consoleHandler(string msg);
        void RunWithRedirect(string cmdPath)
        {
            var proc = new Process();
            proc.StartInfo.FileName = cmdPath;
            proc.StartInfo.Arguments = "-d \""+ini.ManagerD2Path+"\" -r uswest.battle.net";

            // set up output redirection
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            //proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.EnableRaisingEvents = true;
            proc.StartInfo.CreateNoWindow = true;
            // see below for output handler
            proc.ErrorDataReceived += proc_DataReceived;
            proc.OutputDataReceived += proc_DataReceived;

            proc.Start();

            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();
            //consoleInput = proc.StandardInput;

            //proc.WaitForExit();
        }


        void proc_DataReceived(object sender, DataReceivedEventArgs e)
        {
            // output will be in string e.Data
            Console.WriteLine(e.Data);
            writeConsole(e.Data);
            //consoleOutput(e.Data);
        }

        private void writeConsole(string msg)
        {
            if (msg != null)
            {
                if (this.richTextBoxCoreConsole.InvokeRequired)
                {
                    consoleHandler d = new consoleHandler(writeConsole);
                    this.Invoke(d, new object[] { msg });
                }
                else
                {
                    this.richTextBoxCoreConsole.AppendText(msg+"\n");
                }
            }
        }

        private void buttonStartCore_Click(object sender, EventArgs e)
        {
            RunWithRedirect(ini.ManagerAOPath + "\\Awesom-O.exe ");
        }

        private void BotSettings_Click(object sender, EventArgs e)
        {
            Forms.BotSettings settings = new Forms.BotSettings();
            settings.Show();
        }

    }
}
