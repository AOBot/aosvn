﻿namespace ApplicationWindowEater
{
    partial class fSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.D2Path = new System.Windows.Forms.TextBox();
            this.Arguments = new System.Windows.Forms.TextBox();
            this.browse = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.Discard = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Realm = new System.Windows.Forms.ComboBox();
            this.customrealm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.hidecore = new System.Windows.Forms.CheckBox();
            this.Stealth = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.AOPath = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.StartDelay = new System.Windows.Forms.TextBox();
            this.BrowseAO = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Diablo II path";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(12, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Diablo II arguments";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(141)))), ((int)(((byte)(146)))));
            this.label3.Location = new System.Drawing.Point(9, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "    -w is forced";
            // 
            // D2Path
            // 
            this.D2Path.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.D2Path.ForeColor = System.Drawing.Color.DodgerBlue;
            this.D2Path.Location = new System.Drawing.Point(116, 37);
            this.D2Path.Name = "D2Path";
            this.D2Path.Size = new System.Drawing.Size(264, 20);
            this.D2Path.TabIndex = 3;
            // 
            // Arguments
            // 
            this.Arguments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.Arguments.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Arguments.Location = new System.Drawing.Point(117, 64);
            this.Arguments.Name = "Arguments";
            this.Arguments.Size = new System.Drawing.Size(263, 20);
            this.Arguments.TabIndex = 4;
            // 
            // browse
            // 
            this.browse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.browse.ForeColor = System.Drawing.Color.DodgerBlue;
            this.browse.Location = new System.Drawing.Point(390, 39);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(75, 22);
            this.browse.TabIndex = 5;
            this.browse.Text = "Browse";
            this.browse.UseVisualStyleBackColor = false;
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // Save
            // 
            this.Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Save.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Save.Location = new System.Drawing.Point(299, 213);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(82, 22);
            this.Save.TabIndex = 6;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = false;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Discard
            // 
            this.Discard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.Discard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Discard.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Discard.Location = new System.Drawing.Point(387, 212);
            this.Discard.Name = "Discard";
            this.Discard.Size = new System.Drawing.Size(75, 23);
            this.Discard.TabIndex = 7;
            this.Discard.Text = "Discard";
            this.Discard.UseVisualStyleBackColor = false;
            this.Discard.Click += new System.EventHandler(this.Discard_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button1.Location = new System.Drawing.Point(16, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Auto configure";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(12, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Realm";
            // 
            // Realm
            // 
            this.Realm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.Realm.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Realm.Items.AddRange(new object[] {
            "europe.battle.net",
            "uswest.battle.net",
            "useast.battle.net",
            "asia.battle.net",
            "classicbeta.battle.net",
            "custom"});
            this.Realm.Location = new System.Drawing.Point(116, 97);
            this.Realm.Name = "Realm";
            this.Realm.Size = new System.Drawing.Size(264, 21);
            this.Realm.TabIndex = 10;
            this.Realm.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // customrealm
            // 
            this.customrealm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.customrealm.ForeColor = System.Drawing.Color.DodgerBlue;
            this.customrealm.Location = new System.Drawing.Point(116, 125);
            this.customrealm.Name = "customrealm";
            this.customrealm.Size = new System.Drawing.Size(264, 20);
            this.customrealm.TabIndex = 11;
            this.customrealm.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(12, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "IP";
            // 
            // hidecore
            // 
            this.hidecore.AutoSize = true;
            this.hidecore.ForeColor = System.Drawing.Color.DodgerBlue;
            this.hidecore.Location = new System.Drawing.Point(116, 148);
            this.hidecore.Name = "hidecore";
            this.hidecore.Size = new System.Drawing.Size(73, 17);
            this.hidecore.TabIndex = 14;
            this.hidecore.Text = "Hide Core";
            this.hidecore.UseVisualStyleBackColor = true;
            // 
            // Stealth
            // 
            this.Stealth.AutoSize = true;
            this.Stealth.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Stealth.Location = new System.Drawing.Point(196, 148);
            this.Stealth.Name = "Stealth";
            this.Stealth.Size = new System.Drawing.Size(88, 17);
            this.Stealth.TabIndex = 15;
            this.Stealth.Text = "Stealth mode";
            this.Stealth.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(12, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "AO directory";
            // 
            // AOPath
            // 
            this.AOPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.AOPath.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AOPath.Location = new System.Drawing.Point(116, 12);
            this.AOPath.Name = "AOPath";
            this.AOPath.Size = new System.Drawing.Size(265, 20);
            this.AOPath.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label7.Location = new System.Drawing.Point(13, 172);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "AO start delay (ms)";
            // 
            // StartDelay
            // 
            this.StartDelay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.StartDelay.ForeColor = System.Drawing.Color.DodgerBlue;
            this.StartDelay.Location = new System.Drawing.Point(117, 172);
            this.StartDelay.Name = "StartDelay";
            this.StartDelay.Size = new System.Drawing.Size(264, 20);
            this.StartDelay.TabIndex = 19;
            this.StartDelay.Text = "3000";
            // 
            // BrowseAO
            // 
            this.BrowseAO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(27)))), ((int)(((byte)(29)))));
            this.BrowseAO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseAO.ForeColor = System.Drawing.Color.DodgerBlue;
            this.BrowseAO.Location = new System.Drawing.Point(390, 12);
            this.BrowseAO.Name = "BrowseAO";
            this.BrowseAO.Size = new System.Drawing.Size(75, 22);
            this.BrowseAO.TabIndex = 20;
            this.BrowseAO.Text = "Browse";
            this.BrowseAO.UseVisualStyleBackColor = false;
            this.BrowseAO.Click += new System.EventHandler(this.BrowseAO_Click);
            // 
            // fSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(470, 236);
            this.Controls.Add(this.BrowseAO);
            this.Controls.Add(this.StartDelay);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.AOPath);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Stealth);
            this.Controls.Add(this.hidecore);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.customrealm);
            this.Controls.Add(this.Realm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Discard);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.browse);
            this.Controls.Add(this.Arguments);
            this.Controls.Add(this.D2Path);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "fSettings";
            this.Text = "AO Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox D2Path;
        private System.Windows.Forms.TextBox Arguments;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Discard;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Realm;
        private System.Windows.Forms.TextBox customrealm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox hidecore;
        private System.Windows.Forms.CheckBox Stealth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox AOPath;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox StartDelay;
        private System.Windows.Forms.Button BrowseAO;
    }
}