﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using Utilities.IniControl;
using Microsoft.Win32;
using System.IO;

namespace ApplicationWindowEater
{
    public partial class fSettings : Form
    {
        private Settings ini;
        private fMain parent;

        public fSettings(fMain parent)
        {
            InitializeComponent();
            this.parent = parent;
            ini = new Settings();
            // On form init. Set Textbox Values to Ini values
            AOPath.Text = ini.ManagerAOPath;
            D2Path.Text = ini.ManagerD2Path; //  this is setting the text when the form initliaizes brb dog barking
            Arguments.Text = ini.ManagerArguments;
            string realm = ini.ManagerRealm ;
            if (realm == "europe.battle.net" ||
                realm == "uswest.battle.net" ||
                realm == "useast.battle.net" ||
                realm == "asia.battle.net" ||
                realm == "classicbeta.battle.net"
                )
            {
                Realm.Text = realm;
                customrealm.Text = "";
            }
            else { Realm.Text = "custom"; customrealm.Text = realm; }
            if (ini.ManagerHideCore == "true") { hidecore.Checked = true; } else { hidecore.Checked = false; }
            if (ini.ManagerStealthMode == "true") { Stealth.Checked = true; } else { Stealth.Checked = false; }
            StartDelay.Text = ini.ManagerStartDelay;
        }

        private void Save_Click(object sender, EventArgs e)
        {
            string realm = Realm.Text;
            if (realm == "custon") { realm = customrealm.Text;}
            ini.ManagerAOPath = AOPath.Text;
            ini.ManagerD2Path = D2Path.Text;
            ini.ManagerArguments = Arguments.Text;
            ini.ManagerRealm = realm;
            ini.ManagerStartDelay = StartDelay.Text;
            ini.ManagerStealthMode = Convert.ToString(Stealth.Checked);
            ini.ManagerHideCore = Convert.ToString(hidecore.Text);
            parent.setPath(ini.ManagerD2Path); // Set the textboxPath in the Main Form
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RegistryKey regKey = Registry.CurrentUser;
            regKey = regKey.OpenSubKey("Software\\Blizzard Entertainment\\Diablo II");

            if (regKey == null)
            {
                Console.WriteLine("Bad registry key: {0}", regKey);
                D2Path.Text = "C:\\Programm Files\\Diablo II\\Game.exe";
            }else{
                string diapath = (string)regKey.GetValue("InstallPath")+"Game.exe";
                D2Path.Text = diapath;
            }
            AOPath.Text = System.IO.Directory.GetCurrentDirectory();
            Arguments.Text = "";
            hidecore.Checked = false;
            Stealth.Checked = false;
            StartDelay.Text = "3000";
            MessageBox.Show(RealmClass.GetRealm());

        }

        private void BrowseAO_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = Path.GetDirectoryName(Application.ExecutablePath);
            dialog.ShowDialog();
            AOPath.Text = dialog.SelectedPath;
        }

        //onhover = 0xBC2A4D (ACP)
        private void browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = Path.GetDirectoryName(ini.ManagerD2Path);
            dialog.Filter = "Executable (exe)|*.exe";
            dialog.ShowDialog();
            D2Path.Text = dialog.FileName ;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Realm.Text == "custom") { customrealm.Visible = true;label5.Visible = true;}
            else { customrealm.Visible = false; label5.Visible = false; }
        }

        private void Discard_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
