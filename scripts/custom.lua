--[[ Custom User-Defined Scripts ]]--
--[[
	custom.lua will be used as an easy place for users to input their own lua scripts without having to worry about where to put them or how to include them.
	This is the simplest way to be able to make AO extensible, as well as have a concise place for supports to look if someone is need of help and they
	have added in their own functionality to Awesom-O.
--]]
--Ao:Chat("custom.lua opened")