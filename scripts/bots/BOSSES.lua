boss = {}
--> Basic boss structure 'class' used to define new bosses with their areas and NPCCode
function boss:new(area, npc)
	local newBoss = {areaLevel = area, npcCode = npc}
	setmetatable(newBoss, self)
	self.__index = self
	return newBoss
end
function boss:find()
	Move.ToArea(self.areaLevel)
	return Move.ToNpc(self.npcCode)
end
--> Basic boss functionality used to move to the boss area and then move to and kill the boss npc
function boss:kill()
	local npc = self:find()
	if npc.uid > 0 then --> if there is a uid then we found the npc
		Kill.NPC(npc) --> kill the npc
	end
	if Settings.Path.KillBossMinions then --> check if we should clear the area
		Kill.AllInRadius(Ao.me.x, Ao.me.y, 25) --> clear the area
	end
end
function boss:getNpc()
	return self.npcCode
end
function boss:getNpcArea()
	return self.areaLevel
end
seal = {}
--> Basic object opening 'class' defined for opening Chaos Sanctuary seals
function seal:new(gameobj, npc)
	local newSeal = {gameObject = gameobj, npcCode = npc}
	setmetatable(newSeal, self)
	self.__index = self
	return newSeal
end
--> Basic functionality for opening seals for Chaos Sanctuary
function seal:open()
	Move.ToObject(self.gameObject)
	local lock = Ao.objects:find(self.gameObject) --> find the object itself
	if lock.objectMode == 0 then --> if its not open
		repeat --> continually attempt to open until successful
			Ao:UnitInteract(UnitType.GameObject, lock.uid)
			Sleep(100)
			lock = Ao.objects:find(self.gameObject)
			if lock.objectMode == 0 then
				Move.ToObject(self.gameObject)
			end
		until lock.objectMode > 0
	end
end
--> Find and kill the boss from the seal
function seal:kill()
	local npc = Ao.npcs:findBySuperUnique(self.npcCode)
	while npc.uid == 0 do
		if self.npcCode == SuperUnique.InfectorOfSouls then
			Move.ToLocation(7920, 5290)
		elseif self.npcCode == SuperUnique.LordDeSeis then
			Move.ToLocation(7770, 5200)
		elseif self.npcCode == SuperUnique.GrandVizierOfChaos then
			Move.ToLocation(7676, 5310)
		end
		npc = Ao.npcs:findBySuperUnique(self.npcCode)
	end
	Kill.NPC(npc)
end
--[[ Act 1 ]]--
Corpsefire = boss:new("DenOfEvil", "Corpsefire")
Bishibosh = boss:new("ColdPlains", "Bishibosh")
Bonebreaker = boss:new("Crypt", "Bonebreaker")
function Bonebreaker:kill()
	Move.ToArea(self.areaLevel)
	local npc = Move.ToNpc(self.npcCode)
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 25)
end
BloodRaven = boss:new("BurialGrounds", "BloodRaven")
Coldcrow = boss:new("CaveLevel1", "Coldcrow")
Rakanishu = boss:new("StonyField", "Rakanishu")
TreeheadWoodfist = boss:new("DarkWood", "TreeheadWoodfist")
Griswold = boss:new("Tristram", "Griswold")
TheCountess = boss:new("TowerCellarLevel5", "TheCountess")
function TheCountess:kill()
	Move.ToArea(self.areaLevel)
	local npc = Move.ToNpc(self.npcCode)
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 25)
end
PitspawnFouldog = boss:new("JailLevel2", "PitspawnFouldog")
BoneAsh = boss:new("Catherdal", "BoneAsh") --> Requires testing
TheSmith = boss:new("Barracks", "TheSmith")
Andariel = boss:new("CatacombsLevel4", "Andariel")
TheCowKing = boss:new("MooMooFarm", "TheCowKing")
--[[ Act 2 ]]--
Radament = boss:new("SewersLevel3Act2", "Radament")
CreepingFeature = boss:new("StonyTombLevel2", "CreepingFeature")
BloodWitchTheWild = boss:new("HallsOfTheDeadLevel3", "BloodWitchTheWild")
Beetleburst = boss:new("FarOasis", "Beetleburst")
ColdwormTheBurrower = boss:new("MaggotLairLevel3", "ColdwormTheBurrower")
DarkElder = boss:new("LostCity", "DarkElder")
Fangskin = boss:new("ClawViperTempleLevel2", "Fangskin")
FireEye = boss:new("PalaceCellarLevel3", "FireEye")
Summoner = boss:new("ArcaneSanctuary", "Summoner")
--AncientKaatheSoulless = boss:new("TalRashasTomb", "AncientKaatheSoulles") --> must figure out the correct tomb
Duriel = boss:new("DurielsLair", "Duriel")
--[[ Act 3 ]]--
SszarkTheBurning = boss:new("SpiderCavern", "SszarkTheBurning") --> Requires testing
WitchDoctorEndugu = boss:new("FlayerDungeonLevel3", "WitchDoctorEndugu")
Stormtree = boss:new("FlayerJungle", "Stormtree")
BattlemaidSarina = boss:new("RuinedTemple", "BattlemaidSarina")
function BattlemaidSarina:kill()
	Move.ToArea(self.areaLevel)
	local npc = Move.ToNpc(self.npcCode)
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 25)
end
IcehawkRiftwing = boss:new("SewersLevel1Act3", "IcehawkRiftwing")
IsmailVilehand = boss:new("Travincal", "IsmailVilehand")
GelebFlamefinger = boss:new("Travincal", "GelebFlamefinger")
ToorcIcefist = boss:new("Travincal", "ToorcIcefist")
BremmSparkfist = boss:new("DuranceOfHateLevel3", "BremmSparkfist")
WyandVoidbringer = boss:new("DuranceOfHateLevel3", "WyandVoidbringer")
MafferDragonhand = boss:new("DuranceOfHateLevel3", "MafferDragonhand")
Mephisto = boss:new("DuranceOfHateLevel3", "Mephisto")
--[[ Act 4 ]]--
Izual = boss:new("PlainsOfDespair", "Izual")
HephastoTheArmorer = boss:new("RiverOfFlame", "HephastoTheArmorer")
GrandVizierOfChaos = boss:new("ChaosSanctuary", "GrandVizierOfChaos")
function GrandVizierOfChaos:kill()
	Move.ToArea(self.areaLevel)
	local bossSeal = seal:new(GameObjectID.DiabloSeal5, getEnumValue(SuperUnique, self.npcCode))
	bossSeal:open()
	bossSeal:kill()
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 25)
	local nullSeal = seal:new(GameObjectID.DiabloSeal4)
	nullSeal:open()
end
LordDeSeis = boss:new("ChaosSanctuary", "LordDeSeis")
function LordDeSeis:kill()
	Move.ToArea(self.areaLevel)
	local bossSeal = seal:new(GameObjectID.DiabloSeal3, getEnumValue(SuperUnique, self.npcCode))
	bossSeal:open()
	bossSeal:kill()
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 25)
end
InfectorOfSouls = boss:new("ChaosSanctuary", "InfectorOfSouls")
function InfectorOfSouls:kill()
	Move.ToArea(self.areaLevel)
	local bossSeal = seal:new(GameObjectID.DiabloSeal1, getEnumValue(SuperUnique, self.npcCode))
	bossSeal:open()
	bossSeal:kill()
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 25)
	local nullSeal = seal:new(GameObjectID.DiabloSeal2)
	nullSeal:open()
end
Diablo = boss:new("ChaosSanctuary", "Diablo")
function Diablo:kill()
	Move.ToArea(self.areaLevel)
	GrandVizierOfChaos:kill()
	LordDeSeis:kill()
	InfectorOfSouls:kill()
	Move.ToLocation(7795, 5290)
	PreCast(true)
	local bigD = Ao.npcs:findByNpcCode(getEnumValue(NPCCode, self.npcCode))
	repeat
		Kill.Preattack(Ao.me.x, Ao.me.y)
		bigD = Ao.npcs:findByNpcCode(getEnumValue(NPCCode, self.npcCode))
	until bigD.uid > 0
	Kill.NPC(bigD)
end
--[[ Act 5 ]]--
DacFarren = boss:new("BloodyFoothills", "DacFarren")
ShenkTheOverseer = boss:new("BloodyFoothills", "ShenkTheOverseer")
EldritchTheRectifier = boss:new("FrigidHighlands", "EldritchTheRectifier")
EyebackTheUnleashed = boss:new("FrigidHighlands", "EyebackTheUnleashed")
SharptoothSlayer = boss:new("FrigidHighlands", "SharptoothSlayer")
ThreshSocket = boss:new("ArreatPlateau", "ThreshSocket")
Frozenstein = boss:new("FrozenRiver", "Frozenstein")
BonesawBreaker = boss:new("GlacialTrail", "BonesawBreaker")
SnapchipShatter = boss:new("IcyCellar", "SnapchipShatter")
Pindleskin = boss:new("NihlathaksTemple", "Pindleskin")
NihlathakBoss = boss:new("HallsOfVaught", "NihlathakBoss")
function NihlathakBoss:kill()
	Move.ToArea(self.areaLevel)
	Move.ToObject("NihlathakWildernessStartPosition")
	local npc = Move.ToNpc(self.npcCode)
	if npc.uid > 0 then
		Kill.NPC(npc)
	end
	if Settings.Path.KillBossMinions then
		Kill.AllInRadius(Ao.me.x, Ao.me.y, 25)
	end
end
Talic = boss:new("ArreatSummit", "AncientBarbarian") --> Might be a different ancient
function Talic:kill()
	Move.ToArea(self.areaLevel)
	local altar = seal:new(GameObjectID.AncientsAltar)
	altar:open()
	Sleep(1000)
	Ao:DisplayQuestMessage(4294967295, 20002)
	Sleep(1000)
	local npc = Move.ToNpc(self.npcCode)
	if npc.uid > 0 then
		Kill.NPC(npc)
	end
end
Madawc = boss:new("ArreatSummit", "AncientBarbarian2") --> Might be a different ancient
function Madawc:kill()
	Move.ToArea(self.areaLevel)
	local altar = seal:new(GameObjectID.AncientsAltar)
	altar:open()
	Sleep(1000)
	Ao:DisplayQuestMessage(4294967295, 20002)
	Sleep(1000)
	local npc = Move.ToNpc(self.npcCode)
	if npc.uid > 0 then
		Kill.NPC(npc)
	end
end
Korlic = boss:new("ArreatSummit", "AncientBarbarian3") --> Might be a different ancient
function Korlic:kill()
	Move.ToArea(self.areaLevel)
	local altar = seal:new(GameObjectID.AncientsAltar)
	altar:open()
	Sleep(1000)
	Ao:DisplayQuestMessage(4294967295, 20002)
	Sleep(1000)
	local npc = Move.ToNpc(self.npcCode)
	if npc.uid > 0 then
		Kill.NPC(npc)
	end
end
ColenzotheAnnihilator = boss:new("ThroneOfDestruction", "ColenzotheAnnihilator") --> Need functionality
AchmeltheCursed = boss:new("ThroneOfDestruction", "AchmeltheCursed") --> Need functionality
BartuctheBloody = boss:new("ThroneOfDestruction", "BartuctheBloody") --> Need functionality
VentartheUnholy = boss:new("ThroneOfDestruction", "VentartheUnholy") --> Need functionality
ListertheTormentor = boss:new("ThroneOfDestruction", "ListertheTormentor") --> Need functionality
Baal = boss:new("WorldstoneChamber", "Baal") --> Need functionality