area = {}
function area:new(thisarea, nextarea, prevarea)
	local newArea = {area = thisarea, nArea = nextarea, pArea = prevarea}
	setmetatable(newArea, self)
	self.__index = self
	return newArea
end
function area:findArea()
	Move.ToArea(self.area)
end
function area:findNext()
	Move.ToArea(self.nArea)
end
function area:findPrev()
	Move.ToArea(self.pArea)
end
function area:clear()
	self:findArea()
	Move.Explore(true)
end
function area:clearNext()
	self:findNext()
	Move.Explore(true)
end
function area:clearPrev()
	self:findPrev()
	Move.Explore(true)
end
function area:getArea()
	return self.area
end
function area:getNextArea()
	return self.nArea
end
function area:getPrevArea()
	return self.pArea
end

seal = {}
--> Basic object opening 'class' defined for opening Chaos Sanctuary seals
function seal:new(gameobj, npc)
	local newSeal = {gameObject = gameobj, npcCode = npc}
	setmetatable(newSeal, self)
	self.__index = self
	return newSeal
end
--> Basic functionality for opening seals for Chaos Sanctuary
function seal:open()
	Move.ToObject(self.gameObject, true)
	local lock = Ao.objects:find(self.gameObject) --> find the object itself
	if lock.objectMode == 0 then --> if its not open
		repeat --> continually attempt to open until successful
			Ao:UnitInteract(UnitType.GameObject, lock.uid)
			Sleep(100)
			lock = Ao.objects:find(self.gameObject)
			if lock.objectMode == 0 then
				Move.ToObject(self.gameObject, true)
			end
		until lock.objectMode > 0
	end
end
--> Find and kill the boss from the seal
function seal:kill()
	local npc = Ao.npcs:findBySuperUnique(self.npcCode)
	while npc.uid == 0 do
		if self.npcCode == SuperUnique.InfectorOfSouls then
			Move.ToLocation(7920, 5290)
		elseif self.npcCode == SuperUnique.LordDeSeis then
			Move.ToLocation(7770, 5200)
		elseif self.npcCode == SuperUnique.GrandVizierOfChaos then
			Move.ToLocation(7676, 5310)
		end
		npc = Ao.npcs:findBySuperUnique(self.npcCode)
	end
	Kill.NPC(npc)
end

--Act 1
RogueEncampment = area:new("RogueEncampment", "BloodMoor", "None")
BloodMoor = area:new("BloodMoor", "ColdPlains", "RogueEncampment")
ColdPlains = area:new("ColdPlains", "StonyField", "BloodMoor")
StonyField = area:new("StonyField", "UndergroundPassageLevel1", "ColdPlains")
UndergroundPassageLevel1 = area:new("UndergroundPassageLevel1", "DarkWood", "StonyField")
DarkWood = area:new("DarkWood", "BlackMarsh", "UndergroundPassageLevel1")
BlackMarsh = area:new("BlackMarsh", "TamoeHighland", "DarkWood")
TamoeHighland = area:new("TamoeHighland", "MonasteryGate", "BlackMarsh")
MonasteryGate = area:new("MonasteryGate", "OuterCloister", "TamoeHighland")
OuterCloister = area:new("OuterCloister", "Barracks", "MonasteryGate")
Barracks = area:new("OuterCloister", "Barracks", "OuterCloister")
JailLevel1 = area:new("JailLevel1", "JailLevel2", "Barracks")
JailLevel2 = area:new("JailLevel2", "JailLevel3", "JailLevel1")
JailLevel3 = area:new("JailLevel3", "InnerCloister", "JailLevel2")
InnerCloister = area:new("InnerCloister", "Cathedral", "JailLevel3")
Cathedral = area:new("Cathedral", "CatacombsLevel1", "InnerCloister")
CatacombsLevel1 = area:new("CatacombsLevel1", "CatacombsLevel2", "Cathedral")
CatacombsLevel2 = area:new("CatacombsLevel2", "CatacombsLevel3", "CatacombsLevel1")
CatacombsLevel3 = area:new("CatacombsLevel3", "CatacombsLevel4", "CatacombsLevel2")
CatacombsLevel4 = area:new("CatacombsLevel4", "None", "CatacombsLevel3")

--Act 2
LutGholein = area:new("LutGholein", "RockyWaste", "None")
RockyWaste = area:new("RockyWaste", "DryHills", "LutGholein")
DryHills = area:new("DryHills", "FarOasis", "RockyWaste")
FarOasis = area:new("FarOasis", "LostCity", "DryHills")
LostCity = area:new("LostCity", "ValleyOfSnakes", "FarOasis")
ValleyOfSnakes = area:new("ValleyOfSnakes", "ClawViperTempleLevel1", "LostCity")
ClawViperTempleLevel1 = area:new("ClawViperTempleLevel1", "ClawViperTempleLevel2", "ValleyOfSnakes")
ClawViperTempleLevel2 = area:new("ClawViperTempleLevel2", "None", "ClawViperTempleLevel1")
HaremLevel1 = area:new("HaremLevel1", "HaremLevel2", "LutGholein")
HaremLevel2 = area:new("HaremLevel2", "PalaceCellarLevel1", "HaremLevel1")
PalaceCellarLevel1 = area:new("PalaceCellarLevel1", "PalaceCellarLevel3", "HaremLevel2")
PalaceCellarLevel2 = area:new("PalaceCellarLevel3", "PalaceCellarLevel3", "PalaceCellarLevel1")
PalaceCellarLevel3 = area:new("PalaceCellarLevel3", "ArcaneSanctuary", "PalaceCellarLevel2")
ArcaneSanctuary = area:new("ArcaneSanctuary", "CanyonOfTheMagi", "PalaceCellarLevel3")
CanyonOfTheMagi = area:new("CanyonOfTheMagi", "TalRashasTomb1", "ArcaneSanctuary")
TalRashasTomb1 = area:new("TalRashasTomb1", "TalRashasTomb2", "CanyonOfTheMagi")
TalRashasTomb2 = area:new("TalRashasTomb2", "TalRashasTomb3", "TalRashasTomb1")
TalRashasTomb3 = area:new("TalRashasTomb3", "TalRashasTomb4", "TalRashasTomb2")
TalRashasTomb4 = area:new("TalRashasTomb4", "TalRashasTomb5", "TalRashasTomb3")
TalRashasTomb5 = area:new("TalRashasTomb5", "TalRashasTomb6", "TalRashasTomb4")
TalRashasTomb6 = area:new("TalRashasTomb6", "TalRashasTomb7", "TalRashasTomb5")
TalRashasTomb7 = area:new("TalRashasTomb7", "None", "TalRashasTomb6")
-->DurielsLair = area:new("DurielsLair", "None", Ao.maps:GetCorrectTomb())  --> I think GetCorrectTomb() cant be called on loading game

--Act 3
KurastDocks = area:new("KurastDocks", "SpiderForest", "None")
SpiderForest = area:new("SpiderForest", "GreatMarsh", "KurastDocks")
GreatMarsh = area:new("GreatMarsh", "FlayerJungle", "SpiderForest")
FlayerJungle = area:new("FlayerJungle", "LowerKurast", "GreatMarsh")
LowerKurast = area:new("LowerKurast", "KurastBazaar", "FlayerJungle")
KurastBazaar = area:new("KurastBazaar", "UpperKurast", "LowerKurast")
UpperKurast = area:new("UpperKurast", "KurastCauseway", "KurastBazaar")
KurastCauseway = area:new("KurastCauseway", "Travincal", "UpperKurast")
Travincal = area:new("Travincal", "DuranceOfHateLevel1", "KurastCauseway")
DuranceOfHateLevel1 = area:new("DuranceOfHateLevel1", "DuranceOfHateLevel2", "Travincal")
DuranceOfHateLevel1 = area:new("DuranceOfHateLevel2", "DuranceOfHateLevel3", "DuranceOfHateLevel1")
DuranceOfHateLevel1 = area:new("DuranceOfHateLevel3", "None", "DuranceOfHateLevel2")

--Act 4
ThePandemoniumFortress = area:new("ThePandemoniumFortress", "OuterSteppes", "None")
OuterSteppes = area:new("OuterSteppes", "PlainsOfDespair", "ThePandemoniumFortress")
PlainsOfDespair = area:new("PlainsOfDespair", "CityOfTheDamned", "OuterSteppes")
CityOfTheDamned = area:new("CityOfTheDamned", "RiverOfFlame", "PlainsOfDespair")
RiverOfFlame = area:new("RiverOfFlame", "ChaosSanctuary", "CityOfTheDamned")
ChaosSanctuary = area:new("ChaosSanctuary", "ChaosSanctuary", "RiverOfFlame")
function ChaosSanctuary:clear()
	self:findArea()
	if Ao.me.y >= 5525 then
		Move.ToLocation(7795, 5525, true) --> Just inside Chaos
	end
	local def = Settings.Path.KillAll
	Settings.Path.KillAll = true
	Move.ToLocation(7795, 5290, true) --> Move To Star Clearing the way
	--> GrandVizier Seals
	local bossSeal = seal:new(GameObjectID.DiabloSeal5, getEnumValue(SuperUnique, "GrandVizierOfChaos"))
	bossSeal:open()
	bossSeal:kill()
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 50)
	local nullSeal = seal:new(GameObjectID.DiabloSeal4)
	nullSeal:open()
	--> LordDeSeis Seal
	bossSeal = seal:new(GameObjectID.DiabloSeal3, getEnumValue(SuperUnique, "LordDeSeis"))
	bossSeal:open()
	bossSeal:kill()
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 50)
	--> Infector Seals
	bossSeal = seal:new(GameObjectID.DiabloSeal1, getEnumValue(SuperUnique, "InfectorOfSouls"))
	bossSeal:open()
	bossSeal:kill()
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 25)
	nullSeal = seal:new(GameObjectID.DiabloSeal2)
	nullSeal:open()
	Move.ToLocation(7795, 5290)
	PreCast(true)
	local bigD = Ao.npcs:findByNpcCode(getEnumValue(NPCCode, "Diablo"))
	repeat
		Kill.Preattack(Ao.me.x, Ao.me.y)
		bigD = Ao.npcs:findByNpcCode(getEnumValue(NPCCode, "Diablo"))
	until bigD.uid > 0
	Kill.NPC(bigD)
	Settings.Path.KillAll = def
end

--Act 5
Harrogath = area:new("Harrogath", "BloodyFoothills", "None")
BloodyFoothills = area:new("BloodyFoothills", "FrigidHighlands", "Harrogath")
FrigidHighlands = area:new("FrigidHighlands", "ArreatPlateau", "BloodyFoothills")
ArreatPlateau = area:new("ArreatPlateau", "CrystallinePassage", "FrigidHighlands")
CrystallinePassage = area:new("CrystallinePassage", "GlacialTrail", "ArreatPlateau")
GlacialTrail = area:new("GlacialTrail", "FrozenTundra", "CrystallinePassage")
FrozenTundra = area:new("FrozenTundra", "TheAncientsWay", "GlacialTrail")
TheAncientsWay = area:new("TheAncientsWay", "ArreatSummit", "FrozenTundra")
ArreatSummit = area:new("ArreatSummit", "TheWorldStoneKeepLevel1", "TheAncientsWay")
TheWorldStoneKeepLevel1 = area:new("TheWorldStoneKeepLevel1", "TheWorldStoneKeepLevel2", "ArreatSummit")
TheWorldStoneKeepLevel2 = area:new("TheWorldStoneKeepLevel2", "TheWorldStoneKeepLevel3", "TheWorldStoneKeepLevel1")
TheWorldStoneKeepLevel3 = area:new("TheWorldStoneKeepLevel3", "ThroneOfDestruction", "TheWorldStoneKeepLevel2")
ThroneOfDestruction = area:new("ThroneOfDestruction", "TheWorldstoneChamber", "TheWorldStoneKeepLevel3")
TheWorldstoneChamber = area:new("TheWorldstoneChamber", "None", "ThroneOfDestruction")