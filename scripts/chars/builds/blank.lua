--[[ Blank Build Structure ]]--

--[[ Skill Options ]]--
buffs = {} --> character buffs listed within parenthesis(ex : "BattleOrders", "BattleCommand")

--[[ Killing Options ]]--
killMinions = true --> clear radius around bosses
killHeroes = false --> clear heroes while pathing
killAll = false --> clear all monsters while pathing

--[[ Chaos Options ]]--
chaosStartMessage = "Starting Chaos" --> message to send when starting chaos script (NOT DIABLO SCRIPT)
diabloKillMessage = "Kill Big D" --> message to send when spawning Diablo in chaos script
chaosTPLocation = {7792, 5292} --> TP and starting location for chaos script

--[[ Baal Options ]]--
baalStartMessage = "Starting Baal" --> message to send when starting baal script
baalKillMessage = "Once More Into The Breach My Friends" --> message to send when ready to enter worldstone chamber
baalTPLocation = {15116, 5006} --> TP location in throne room
waveAuras = {} --> auras for paladins between waves(ie : "Salvation", "Redemption", "Concentration")
preWaveLocationsX = {15095, 15095, 15095, 15095, 15095} --> X value for pre wave STANDING locations
preWaveLocationsY = {5031, 5031, 5031, 5031, 5031} --> Y value for pre wave STANDING locations
preWaveAttackRadius = 10 --> radius value for randomizing preWave skill cast locations
preWaveAttackX = {15092, 15092, 15092, 15092, 15092} --> X value for pre wave attack location (spread over preWaveAttackRadius)
preWaveAttackY = {5028, 5028, 5028, 5028, 5028} --> Y value for pre wave attack locations (spread over preWaveAttackRadius)
