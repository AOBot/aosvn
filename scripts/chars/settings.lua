--[[ Character Settings File ]]--

--[[ Botting Options ]]--
doThis = "" --> lua code for runs goes here (similar to old sequence.lua)
public = true --> true : send invites, chat, open TP @ boss

--[[ Character Options ]]--
build = "blank" --> character build type (check scripts\chars\builds for options)
lowLife = 75 --> % of life to drink a red potion at
rejuvLife = 50 --> % of life to drink a purple potion at
lowMana = 20 --> % of mana to drink a blue potion at
rejuvMana = 0 --> % of mana to drink a purple potion at
chickenTown = 30 --> % of life to chicken to town
chickenExit = 20 --> % of life to chicken exit game
useMerc = true --> true to check for merc when in town
autoResMerc = true --> true to automatically resurrect merc when he dies

--[[ Item/Inventory Options ]]--
belt = "Blue Red Purple Purple" --> characters belt setup
invLock = { --> inventory lock 1 = locked || 0 = unlocked
	1,1,1,1,1,1,1,1,1,1,
	1,1,1,1,1,1,1,1,1,1,
	1,1,1,1,1,1,1,1,1,1,
	1,1,1,1,1,1,1,1,1,1,
}
pickit = "default" --> pickit folder name
minGold = 1000 --> minimum gold amount to pick
autoDump = false --> automatically check picked items (good for characters with low inventory space)
disablePickit = false --> set to true for leeching

--[[ Cubing Options ]]--
makeRune = "" --> list of runes to make (ie : "Zod Vex Ohm Cham Hel")
makeFlawed = "" --> list of flawed gems to make (ie : "Ruby Sapphire")
makeNormal = "" --> list of normal gems to make (ie : "Diamond Emerald")
makeFlawless = "" --> list of flawless gems to make (ie : "Skull Topaz")
makePerfect = "" --> list of perfect gems to make (ie : "Amethyst")
makeTokens = false --> set true to auto-pick essences and make tokens

--[[ Player Options ]]--
killHostiles = false --> set true to kill Hostiles when in sight (false to chicken exit on sight)
squelchLevel = 0 --> minimum level to not be squelched

--[[ Misc Options ]]--
openAllChests = false --> true to open all chests, false will open sparkling chests still
shrinePriority = {} --> CURRENTLY NOT WORKING priority list for getting shrines (ie : "Experience", "Skill", "FireResist")

--[[ Message Options ]]--
townChickenMessage = "Town Break" --> message to send when chickening to town
exitChickenMessage = "Ouch, leaving" --> message to send when chickening from game
hotTPMessage = "Hot TP" --> message to send when TP has monsters around it
coldTPMessage = "Cold TP" --> message to send when TP has no monster around it
newGameMessage = "Next!" --> message to send when moving on to next game