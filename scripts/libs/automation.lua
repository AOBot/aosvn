--[[ Settings automator for new characters ]]--
--[[
	automation.lua was made to generate valid character files for new characters.  It requires
	that the character file not already be created.  For now it works with a blank file and just
	writes all the values line by line.  I may later look into creating a settings switcher which
	will edit the character file via chat commands.
	
	The settings automation uses a "lua class" to make character settings files easily expandable and
	contractible without a ton of extra coding.  Each setting has a name, description 2 default values
	(one for under level 60, and one for level 60 and above) as well as a type indicator.
	
	Every setting is required to have a default constructor, but the run function can be overriden at
	any point.  All the default constructors are at the top of the file with the exceptions (noted)
	listed below all the default constructors.  Any default values from the constructor are not
	necessarily used if the run function has been overridden.
	
	Core Functions/Variables Used : 
	Ao.me.level -> returns level of the character
	Ao.me.uid -> uid of the character (UID is unique identifier (a number representation of our unique character))
	Ao.merc.uid -> uid of our merc
	Ao.character -> name of the character
	Ao.players:find(PLAYERUIDHERE) -> finds a player based off UID (typically used just to update information)
	Ao.me.belt:getBeltRow(ROWNUMBERHERE) -> returns a list of items in a belt (vertical) row
	Ao.me.inventory:get() -> returns a list of items in characters inventory
	
	Ao:Sleep(TIMEINMILLISECONDSHERE) -> sleep, u better know this!
	Ao:set("VARIABLENAMEHERE", "VALUEHERE") -> sets a variable in core
	Ao:getString("VARIABLENAMEHERE") -> retrieves a set variable in core, useful for when using data across stop/start and across different lua VMs (also used to set information during event calling)
--]]
trace:log("automation.lua opened")
--[[ autoSettings class ]]--
autoSettings = {}
function autoSettings:new(name, description, defaultUnder60, defaultOver60, settingsType) --> autoSettings constructor
	local newAutoSettings = { --> data relay
		name = name,
		description = description,
		defaultUnder60 = defaultUnder60,
		defaultOver60 = defaultOver60,
		settingsType = settingsType,
	}
	setmetatable(newAutoSettings, self) --> set meta table to itself so we can seperate references
	self.__index = self --> same with index
	return newAutoSettings --> feed back the new wrapper so that original is kept in tact
end
function autoSettings:run(auto) --> run module for autoSettings
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize the name
	if auto then --> check if setting should be automated
		if Ao.me.level > 60 then 
			return self.defaultOver60 --> just use the default under level 60 option set when constructed
		else
			return self.defaultUnder60 --> use the default over level 60 option set when constructed
		end
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		if self.settingsType == "truefalse" then --> input checking for true/false options
			local checkInput = false
			while not checkInput do --> keep looping until we have good input from user
				Ao:Sleep(300)
				if string.lower(Ao:getString("message")) == "true"then --> only respond to true commands
					Ao:set("message", "true")
					checkInput = true
				elseif string.lower(Ao:getString("message")) == "false" then --> or false commands
					Ao:set("message", "false")
					checkInput = true
				elseif string.lower(Ao:getString("message")) == "auto" then --> account for auto apply settings
					Ao:set("message", "auto")
					checkInput = true
				elseif Ao:getString("message") ~= "ao" then --> anything but our standard... throw the error!
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response please use either �c2true�c0 or �c2false")
					Ao:set("message", "ao")
				end
			end
		elseif self.settingsType == "number" then --> need a way to check numeric input (even though its input as a string!)
			while Ao:getString("message") == "ao" do --> keep looping until we have new input from user
				Ao:Sleep(300)
			end
		elseif self.settingsType == "string" then --> assume all strings are correct... I guess we could do some quote checking and such later
			while Ao:getString("message") == "ao" do --> keep looping until we have new input from user
				Ao:Sleep(300)
			end
		else
			error:log("unknown settingsType : "..self.settingsType) --> error return for unknown settingsType
			return "ERROR"
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			info:log(self.name.." will automatically be set for you")
			if Ao.me.level > 60 then --> again set defaults based off level
				return self.defaultOver60
			else
				return self.defaultUnder60
			end
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> user input is acceptable, let them know what they input and move on
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end

--[[ autoSettings default constructors ]]--
autoDoThis = autoSettings:new("doThis", "�c3lua code for runs (similar to old sequence.lua)", "Andariel:kill() Mephisto:kill", "Chaos:clear() Baal:kill()", "string")
autoPublic = autoSettings:new("public", "�c2true�c3(send invites, speak in game, open TPs @ bosses) �c2false�c3(accept invites, no �c3TPs at bosses, no speaking)", "false", "true", "truefalse")
autoBuild = autoSettings:new("build", "�c2Hammerdin XSorc", "xsorc", "xsorc", "string") --> Needs testing!
--> autoBuild:run() has been overridden, please check below the default constructor for how it works
autoLowLife = autoSettings:new("lowLife", "�c3%% of life to drink a red potion at", "90", "80", "number")
autoLowMana = autoSettings:new("lowMana", "�c3%% of mana to drink a blue potion at", "40", "20", "number")
autoRejuvMana = autoSettings:new("rejuvMana", "�c3%% of mana to drink a purple potion at", "0", "5", "number")
autoRejuvLife = autoSettings:new("rejuvLife", "�c3%% of life to drink a purple potion at", "70", "60", "number")
autoChickenTown = autoSettings:new("chickenTown", "�c3%% of life to chicken to town", "50", "30", "number")
autoChickenExit = autoSettings:new("chickenExit", "�c3%% of life to chicken exit game", "35", "25", "number")
autoUseMerc = autoSettings:new("useMerc", "�c2true�c3(check to revive merc when in town) �c2false�c3(dont check to revive merc)", "false", "true", "truefalse") --> Test
--> autoUseMerc:run() has been overridden, please check below the defualt constructor for how it works
autoAutoResMerc = autoSettings:new("autoResMerc", "�c2true�c3(immediately return to town to revive merc upon death) �c2false�c3(continue without merc until next pass through town)", "true", "true", "truefalse")
autoBelt = autoSettings:new("belt", "�c2Blue Red Purple�c3 Must have 4 entries", "Blue Red Red Purple", "Blue Red Purple Purple", "string") --> Test!!!
--> autoBelt:run() has been overridden, please check below the defualt constructor for how it works
autoInvLock1 = autoSettings:new("invLock1", "�c21 0�c3 Top row of inventory (must have 10 entries AND end with a comma)", "1,1,1,1,1,1,1,1,1,1,", "1,1,1,1,1,1,1,1,1,1,", "string") --> Test
--> autoInvLock1:run() has been overridden, please check below the defualt constructor for how it works
autoInvLock2 = autoSettings:new("invLock2", "�c21 0�c3 Second row down of inventory (must have 10 entries AND end with a comma)", "1,1,1,1,1,1,1,1,1,1,", "1,1,1,1,1,1,1,1,1,1", "string") --> Test
--> autoInvLock2:run() has been overridden, please check below the defualt constructor for how it works
autoInvLock3 = autoSettings:new("invLock3", "�c21 0�c3 Third row down of inventory (must have 10 entries AND end with a comma)", "1,1,1,1,1,1,1,1,1,1,", "1,1,1,1,1,1,1,1,1,1,", "string") --> Test
--> autoInvLock3:run() has been overridden, please check below the defualt constructor for how it works
autoInvLock4 = autoSettings:new("invLock4", "�c21 0�c3 Bottom row of inventory (must have 10 entries AND end with a comma)", "1,1,1,1,1,1,1,1,1,1,", "1,1,1,1,1,1,1,1,1,1,", "string") --> Test
--> autoInvLock4:run() has been overridden, please check below the defualt constructor for how it works
autoPickit = autoSettings:new("pickit", "�c2default�c3 (if you have added new pickit folders just put the name in)", "default", "default", "string")
autoMinGold = autoSettings:new("minGold", "�c2minimum gold amount to pick", "500", "5000", "number")
autoAutoDump = autoSettings:new("autoDump", "�c2true�c3(immediately check picked item) �c2false�c3(check picked item in town)", "false", "true", "truefalse")
autoDisablePickit = autoSettings:new("disablePickit", "�c2true�c3(do not pick items) �c2false�c3(pick items as normal)", "false", "false", "truefalse")
autoMakeRune = autoSettings:new("makeRune", "�c2Eld Tir Nef Eth Ith Tal Ral Ort Thul Amn Sol Shael Dol Hel Io Lum Ko Fal �c2Lem Pul Um Mal Ist Gul Vex Ohm Lo Sur Ber Jah Cham Zod�c3(order does NOT matter reply with none if you do �c3not wish to cube runes)", "", "", "string") --> Special input check
--> autoMakeRune:run() has been overridden, please check below the defualt constructor for how it works
autoMakeFlawed = autoSettings:new("makeFlawed", "�c2Skull Amethyst Sapphire Emerald Ruby Diamond Topaz�c3(collects and �c3cubes chipped gems of these types reply with none if you do not wish to cube these gems)", "", "", "string") --> Special input check
--> autoMakeFlawed:run() has been overridden, please check below the defualt constructor for how it works
autoMakeNormal = autoSettings:new("makeNormal", "�c2Skull Amethyst Sapphire Emerald Ruby Diamond Topaz�c3(collects and �c3cubes flawed gems of these types reply with none if you do not wish to cube these gems)", "", "", "string") --> Special input check
--> autoMakeNormal:run() has been overridden, please check below the defualt constructor for how it works
autoMakeFlawless = autoSettings:new("makeFlawless", "�c2Skull Amethyst Sapphire Emerald Ruby Diamond Topaz�c3(collects and �c3cubes normal gems of these types reply with none if you do not wish to cube these gems)", "", "", "string") --> Special input check
--> autoMakeFlawless:run() has been overridden, please check below the defualt constructor for how it works
autoMakePerfect = autoSettings:new("makePerfect", "�c2Skull Amethyst Sapphire Emerald Ruby Diamond Topaz�c3(collects and �c3cubes flawless gems of these types reply with none if you do not wish to cube these gems)", "", "", "string") --> Special input check
--> autoMakePerfect:run() has been overridden, please check below the defualt constructor for how it works
autoMakeTokens = autoSettings:new("makeTokens", "�c2true�c3(pick and cube essences) �c2false�c3(do not pick essences unless in pickit)", "false", "true", "truefalse")
autoKillHostiles = autoSettings:new("killHostiles", "�c2true�c3(kill hostiles on sight) �c2false�c3(exit game on sight of hostiles)", "false", "true", "truefalse")
autoSquelchLevel = autoSettings:new("squelchLevel", "�c2minimum level to not be squelched", "0", "0", "number")
autoOpenAllChests = autoSettings:new("openAllChests", "�c2true�c3(open all chests during movement) �c2false�c3(open only sparkly chests �c3during movement)", "false", "false", "truefalse")
autoShrinePriority = autoSettings:new("shrinePriority", "�c3 This is a work in progress still please enter �c2none", "", "", "string")
autoTownChickenMessage = autoSettings:new("townChickenMessage", "�c2message to send when chickening to town", "Im too young to die", "Im too old to die", "string")
autoExitChickenMessage = autoSettings:new("exitChickenMessage", "�c2message to send when chickening from game", "Fuck SIDS", "Alzheimers got me", "string")
autoHotTPMessage = autoSettings:new("hotTPMessage", "�c2message to send when TP has monsters around it", "Help help Im being repressed", "Burning up!", "string")
autoColdTPMessage = autoSettings:new("coldTPMessage", "�c2message to send when TP has no monsters around", "Look you silly bastard your arms off!", "Lonely in here", "string")
autoNewGameMessage = autoSettings:new("newGameMessage", "�c2message to sned when moving on to next game", "finally... fucking... done", "well that was quick", "string")

--[[ overrides for all auto settings that do not use the typical structure ]]--
function autoBuild:run(auto) --> custom run module for autoBuild
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize name
	if auto then --> check if setting should be automated
		local me = Ao.players:find(Ao.me.uid)
		if me.charClass == CharacterClass.Amazon then --> Amazon build options here
			error:log("I am sorry we do not have a build for �c7Amazon")
			return "blank"
		elseif me.charClass == CharacterClass.Sorceress then --> Sorceress build options here
			return "XSorc"
		elseif me.charClass == CharacterClass.Necromancer then --> Necromancer build options here
			error:log("I am sorry we do not have a build for �c7Necromancer")
			return "blank"
		elseif me.charClass == CharacterClass.Paladin then --> Paladin build options here
			return "Hammerdin"
		elseif me.charClass == CharacterClass.Druid then --> Druid build options here
			error:log("I am sorry we do not have a build for �c7Druid")
			return "blank"
		elseif me.charClass == CharacterClass.Assassin then --> Assassin build options here
			error:log("I am sorry we do not have a build for �c7Assassin")
			return "blank"
		else --> Cant read their class? or bad class
			error:log("But I cannot determine your character class!!!")
			return "blank"
		end
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> await proper input from user
			Ao:Sleep(300)
			if Ao:getString("message") == "auto" then
				break
			elseif Ao:getString("message") ~= "ao" then --> new input recieved
				if not protectedLoadFile(_BuildsDir..Ao:getString("message")) then --> test to load the file they want and verify it exists
					error:log("I am sorry but : �c2"..Ao:getString("message").."�c0 is not a valid build file")
					Ao:set("message", "ao")
				else
					checkInput = true
				end
			end
		end
		if Ao:getString("message") == "auto" then --> check to see if user would like this setting automated
			info:log(self.name.." will automatically be set for you")
			local me = Ao.players:find(Ao.me.uid)
			if me.charClass == CharacterClass.Amazon then --> Amazon build options here
				error:log("I am sorry we do not have a build for �c7Amazon")
				return "blank"
			elseif me.charClass == CharacterClass.Sorceress then --> Sorceress build options here
				return "XSorc"
			elseif me.charClass == CharacterClass.Necromancer then --> Necromancer build options here
				error:log("I am sorry we do not have a build for �c7Necromancer")
				return "blank"
			elseif me.charClass == CharacterClass.Paladin then --> Paladin build options here
				return "Hammerdin"
			elseif me.charClass == CharacterClass.Druid then --> Druid build options here
				error:log("I am sorry we do not have a build for �c7Druid")
				return "blank"
			elseif me.charClass == CharacterClass.Assassin then --> Assassin build options here
				error:log("I am sorry we do not have a build for �c7Assassin")
				return "blank"
			else --> Cant read their class? or bad class
				error:log("But I cannot determine your character class!!!")
				return "blank"
			end
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> return a blank value because user does not want this set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> relay user input back
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the user value for input in settings
		end
	end
end
function autoUseMerc:run(auto) --> custom run module for autoUseMerc
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize name
	if auto then --> check if setting should be automated
		if Ao.merc.uid > 0 then --> check if merc exists based off having a UID
			return "true"
		else --> no merc exists
			return "false"
		end
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		if self.settingsType == "truefalse" then --> input checking for true/false options
			local checkInput = false
			while not checkInput do --> keep looping until we have good input from user
				Ao:Sleep(300)
				if string.lower(Ao:getString("message")) == "true"then --> only respond to true commands
					Ao:set("message", "true")
					checkInput = true
				elseif string.lower(Ao:getString("message")) == "false" then --> or false commands
					Ao:set("message", "false")
					checkInput = true
				elseif string.lower(Ao:getString("message")) == "auto" then --> account for auto apply settings
					Ao:set("message", "auto")
					checkInput = true
				elseif Ao:getString("message") ~= "ao" then --> anything but our standard... throw the error!
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response please use either �c2true�c0 or �c2false")
					Ao:set("message", "ao")
				end
			end
		elseif self.settingsType == "number" then --> need a way to check numeric input (even though its input as a string!)
			while Ao:getString("message") == "ao" do --> keep looping until we have new input from user
				Ao:Sleep(300)
			end
		elseif self.settingsType == "string" then --> assume all strings are correct... I guess we could do some quote checking and such later
			while Ao:getString("message") == "ao" do --> keep looping until we have new input from user
				Ao:Sleep(300)
			end
		else
			error:log("unknown settingsType : "..self.settingsType) --> error return for unknown settingsType
			return "ERROR"
		end
		if Ao:getString("message") == "auto" then --> check to see if user would like this setting automated
			info:log(self.name.." will automatically be set for you")
			if Ao.merc.uid > 0 then --> check if merc exists based off having uid
				return "true"
			else --> no merc found
				return "false"
			end
		else --> not user opted automation
			if Ao:getString("message") == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoBelt:run(auto) --> custom run module for autoBelt
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize name
	if auto then --> check if setting should be automated
		info:log("Automating your belt, any blank rows will be counted as Rejuv slots!")
		local beltReturn = ""
		for i = 0, 3 do --> loop the 4 rows
			local beltRow = Ao.me.belt:getBeltRow(i) --> returns all potions in a row
			local blueCount = 0
			local redCount = 0
			local purpleCount = 0
			for j = 0, beltRow:size() - 1 do --> loop through the potions in row
				if Items:GetPotType(beltRow[j].baseItem.code) == PotionType.Blue then --> check if potion is blue
					blueCount = blueCount + 1
				elseif Items:GetPotType(beltRow[j].baseItem.code) == PotionType.Red then --> check if potion is red
					redCount = redCount + 1
				else --> if not blue and not red, were going with purple!
					purpleCount = purpleCount + 1
				end
			end			
			if blueCount > redCount and blueCount > purpleCount then --> mostly blue, set as such
				beltReturn = beltReturn.."Blue "
			elseif redCount > blueCount and redCount > purpleCount then --> mostly red, set as such
				beltReturn = beltReturn.."Red "
			else --> anything else is purple!
				beltReturn = beltReturn.."Purple "
			end
		end
		return beltReturn
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> wait for new user input
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> run to automate
				break
			elseif Ao:getString("message") ~= "ao" then --> new input, lets check it
				local beltReturn = ""
				local count = 0
				for word in string.gmatch(Ao:getString("message"), "%w+") do --> loop each word in the user input (defined by spaces!)
					if string.lower(word) == "blue" then --> if blue (any case) then normalize for our use and count we have a successful match
						beltReturn = beltReturn.."Blue "
						count = count + 1
					elseif string.lower(word) == "red" then --> if red (any case) then normalize for our use and count we have a successful match
						beltReturn = beltReturn.."Red "
						count = count + 1
					elseif string.lower(word) == "purple" then --> if purple (any case) then normalize for our use and count we have a successful match
						beltReturn = beltReturn.."Purple "
						count = count + 1
					else --> any other input is invalid, spit it back out as such and set message back to default along with counter
						error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response")
						Ao:set("message", "ao")
						count = 0
						break
					end
				end
				if count == 0 then --> likely we defaulted the counter meaning we had an error in input
				elseif count < 4 then --> not enough successful matches counted
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not enough belt positions)")
					Ao:set("message", "ao")
				elseif count > 4 then --> too many successful matches counted
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (too many belt positions)")
					Ao:set("message", "ao")
				else --> all clear on our input
					trace:log("returning for belt "..beltReturn)
					return beltReturn
				end
			end
		end
		if Ao:getString("message") == "auto" then --> check to see if user would like this setting automated
			info:log("Automating your belt, any blank rows will be counted as Rejuv slots!")
			local beltReturn = ""
			for i = 0, 3 do --> loop the 4 rows
				local beltRow = Ao.me.belt:getBeltRow(i) --> returns all potions in a row
				local blueCount = 0
				local redCount = 0
				local purpleCount = 0
				for j = 0, beltRow:size() - 1 do --> loop through the potions in row
					if Items:GetPotType(beltRow[j].baseItem.code) == PotionType.Blue then --> check if potion is blue
						blueCount = blueCount + 1
					elseif Items:GetPotType(beltRow[j].baseItem.code) == PotionType.Red then --> check if potion is red
						redCount = redCount + 1
					else --> if not blue and not red, were going with purple!
						purpleCount = purpleCount + 1
					end
				end
				if blueCount > redCount and blueCount > purpleCount then --> mostly blue, set as such
					beltReturn = beltReturn.."Blue "
				elseif redCount > blueCount and redCount > purpleCount then --> mostly red, set as such
					beltReturn = beltReturn.."Red "
				else --> anything else is purple!
					beltReturn = beltReturn.."Purple "
				end
			end
			return beltReturn
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> relay user input for their assurance
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoInvLock1:run(auto) --> custom run module for autoInvLock1
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize name
	if auto then --> check if setting should be automated
		local lockReturn = ""
		local items = Ao.me.inventory:get() --> get list of ALL items in inventory
		local inv = { --> create a blank inventory layout (easiest to fill a full inventory then chop the line we need)
			[1] = {0,0,0,0,0,0,0,0,0,0},
			[2] = {0,0,0,0,0,0,0,0,0,0},
			[3] = {0,0,0,0,0,0,0,0,0,0},
			[4] = {0,0,0,0,0,0,0,0,0,0}
		}
		for i = 0, items:size() - 1 do --> loop through items in inventory
			for j = items[i].y+1, items[i].y+items[i].baseItem.invHeight do --> check y location of item (AO holds location being the top left corner of item) and expand to its height
				for k = items[i].x+1, items[i].x+items[i].baseItem.invWidth do --> check x location of item (AO holds location being the top left corner of item) and expand to its width
					inv[j][k] = 1 --> mark the item spaces as locked
				end
			end
		end
		for i = 1, 10 do --> normalize for our purposes the proper line of inventory
			lockReturn = lockReturn..inv[1][i]..","
		end
		return lockReturn --> return the normalized lock line to be input to settings
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> loop until proper input!
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> break out if user decides to automate the process
				break
			elseif Ao:getString("message") ~= "ao" then --> if input is different from default
				local invLockReturn = ""
				local countComma = 0
				local countValue = 0
				local b = string.find(Ao:getString("message"), ",") --> find the first comma (string.find returns location in string
				while b do --> while b is not nil (ie : we found a comma) count all commas found
					countComma = countComma + 1
					b = string.find(Ao:getString("message"), ",", b+1) --> b is a placeholder for last comma found, so search from b+1 for next comma
				end
				b = string.find(Ao:getString("message"), "1") --> find the first 1
				while b do --> while we are finding 1s count them up
					countValue = countValue + 1
					b = string.find(Ao:getString("message"), "1", b+1) --> find next 1 by using b+1 (string location of last 1 '+1')
				end
				b = string.find(Ao:getString("message"), "0") --> find the first 0
				while b do --> while we are finding 0s count them up
					countValue = countValue + 1
					b = string.find(Ao:getString("message"), "0", b+1) --> find all the remaining 0s
				end
				if countComma ~= 10 then --> if we have more or less than 10 commas user input is wrong
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not the right number of commas "..countComma.." of 10)")
					Ao:set("message", "ao")
				elseif countValue ~= 10 then --> if we have more or less than 10 1's and 0's user input is wrong
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not the right number of values "..countValue.." of 10)")
					Ao:set("message", "ao")
				else --> looks good!
					checkInput = true
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			local lockReturn = ""
			local items = Ao.me.inventory:get() --> get list of ALL items in inventory
			local inv = { --> create a blank inventory layout (easiest to fill a full inventory then chop the line we need)
				[1] = {0,0,0,0,0,0,0,0,0,0},
				[2] = {0,0,0,0,0,0,0,0,0,0},
				[3] = {0,0,0,0,0,0,0,0,0,0},
				[4] = {0,0,0,0,0,0,0,0,0,0}
			}
			for i = 0, items:size() - 1 do --> loop through items in inventory
				for j = items[i].y+1, items[i].y+items[i].baseItem.invHeight do --> check y location of item (AO holds location being the top left corner of item) and expand to its height
					for k = items[i].x+1, items[i].x+items[i].baseItem.invWidth do --> check x location of item (AO holds location being the top left corner of item) and expand to its width
						inv[j][k] = 1 --> mark the item spaces as locked
					end
				end
			end
			for i = 1, 10 do --> normalize for our purposes the proper line of inventory
				lockReturn = lockReturn..inv[1][i]..","
			end
			return lockReturn --> return the normalized lock line to be input to settings
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> relay user input for them to see
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoInvLock2:run(auto) --> custom run module for autoInvLock2
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize name
	if auto then --> check if setting should be automated
		local lockReturn = ""
		local items = Ao.me.inventory:get() --> get list of ALL items in inventory
		local inv = { --> create a blank inventory layout (easiest to fill a full inventory then chop the line we need)
			[1] = {0,0,0,0,0,0,0,0,0,0},
			[2] = {0,0,0,0,0,0,0,0,0,0},
			[3] = {0,0,0,0,0,0,0,0,0,0},
			[4] = {0,0,0,0,0,0,0,0,0,0}
		}
		for i = 0, items:size() - 1 do --> loop through items in inventory
			for j = items[i].y+1, items[i].y+items[i].baseItem.invHeight do --> check y location of item (AO holds location being the top left corner of item) and expand to its height
				for k = items[i].x+1, items[i].x+items[i].baseItem.invWidth do --> check x location of item (AO holds location being the top left corner of item) and expand to its width
					inv[j][k] = 1 --> mark the item spaces as locked
				end
			end
		end
		for i = 1, 10 do --> normalize for our purposes the proper line of inventory
			lockReturn = lockReturn..inv[2][i]..","
		end
		return lockReturn --> return the normalized lock line to be input to settings
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> loop until proper input!
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> break out if user decides to automate the process
				break
			elseif Ao:getString("message") ~= "ao" then --> if input is different from default
				local invLockReturn = ""
				local countComma = 0
				local countValue = 0
				local b = string.find(Ao:getString("message"), ",") --> find the first comma (string.find returns location in string
				while b do --> while b is not nil (ie : we found a comma) count all commas found
					countComma = countComma + 1
					b = string.find(Ao:getString("message"), ",", b+1) --> b is a placeholder for last comma found, so search from b+1 for next comma
				end
				b = string.find(Ao:getString("message"), "1") --> find the first 1
				while b do --> while we are finding 1s count them up
					countValue = countValue + 1
					b = string.find(Ao:getString("message"), "1", b+1) --> find next 1 by using b+1 (string location of last 1 '+1')
				end
				b = string.find(Ao:getString("message"), "0") --> find the first 0
				while b do --> while we are finding 0s count them up
					countValue = countValue + 1
					b = string.find(Ao:getString("message"), "0", b+1) --> find all the remaining 0s
				end
				if countComma ~= 10 then --> if we have more or less than 10 commas user input is wrong
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not the right number of commas "..countComma.." of 10)")
					Ao:set("message", "ao")
				elseif countValue ~= 10 then --> if we have more or less than 10 1's and 0's user input is wrong
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not the right number of values "..countValue.." of 10)")
					Ao:set("message", "ao")
				else --> looks good!
					checkInput = true
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			local lockReturn = ""
			local items = Ao.me.inventory:get() --> get list of ALL items in inventory
			local inv = { --> create a blank inventory layout (easiest to fill a full inventory then chop the line we need)
				[1] = {0,0,0,0,0,0,0,0,0,0},
				[2] = {0,0,0,0,0,0,0,0,0,0},
				[3] = {0,0,0,0,0,0,0,0,0,0},
				[4] = {0,0,0,0,0,0,0,0,0,0}
			}
			for i = 0, items:size() - 1 do --> loop through items in inventory
				for j = items[i].y+1, items[i].y+items[i].baseItem.invHeight do --> check y location of item (AO holds location being the top left corner of item) and expand to its height
					for k = items[i].x+1, items[i].x+items[i].baseItem.invWidth do --> check x location of item (AO holds location being the top left corner of item) and expand to its width
						inv[j][k] = 1 --> mark the item spaces as locked
					end
				end
			end
			for i = 1, 10 do --> normalize for our purposes the proper line of inventory
				lockReturn = lockReturn..inv[2][i]..","
			end
			return lockReturn --> return the normalized lock line to be input to settings
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> relay user input for them to see
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoInvLock3:run(auto) --> custom run module for autoInvLock3
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize name
	if auto then --> check if setting should be automated
		local lockReturn = ""
		local items = Ao.me.inventory:get() --> get list of ALL items in inventory
		local inv = { --> create a blank inventory layout (easiest to fill a full inventory then chop the line we need)
			[1] = {0,0,0,0,0,0,0,0,0,0},
			[2] = {0,0,0,0,0,0,0,0,0,0},
			[3] = {0,0,0,0,0,0,0,0,0,0},
			[4] = {0,0,0,0,0,0,0,0,0,0}
		}
		for i = 0, items:size() - 1 do --> loop through items in inventory
			for j = items[i].y+1, items[i].y+items[i].baseItem.invHeight do --> check y location of item (AO holds location being the top left corner of item) and expand to its height
				for k = items[i].x+1, items[i].x+items[i].baseItem.invWidth do --> check x location of item (AO holds location being the top left corner of item) and expand to its width
					inv[j][k] = 1 --> mark the item spaces as locked
				end
			end
		end
		for i = 1, 10 do --> normalize for our purposes the proper line of inventory
			lockReturn = lockReturn..inv[3][i]..","
		end
		return lockReturn --> return the normalized lock line to be input to settings
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> loop until proper input!
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> break out if user decides to automate the process
				break
			elseif Ao:getString("message") ~= "ao" then --> if input is different from default
				local invLockReturn = ""
				local countComma = 0
				local countValue = 0
				local b = string.find(Ao:getString("message"), ",") --> find the first comma (string.find returns location in string
				while b do --> while b is not nil (ie : we found a comma) count all commas found
					countComma = countComma + 1
					b = string.find(Ao:getString("message"), ",", b+1) --> b is a placeholder for last comma found, so search from b+1 for next comma
				end
				b = string.find(Ao:getString("message"), "1") --> find the first 1
				while b do --> while we are finding 1s count them up
					countValue = countValue + 1
					b = string.find(Ao:getString("message"), "1", b+1) --> find next 1 by using b+1 (string location of last 1 '+1')
				end
				b = string.find(Ao:getString("message"), "0") --> find the first 0
				while b do --> while we are finding 0s count them up
					countValue = countValue + 1
					b = string.find(Ao:getString("message"), "0", b+1) --> find all the remaining 0s
				end
				if countComma ~= 10 then --> if we have more or less than 10 commas user input is wrong
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not the right number of commas "..countComma.." of 10)")
					Ao:set("message", "ao")
				elseif countValue ~= 10 then --> if we have more or less than 10 1's and 0's user input is wrong
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not the right number of values "..countValue.." of 10)")
					Ao:set("message", "ao")
				else --> looks good!
					checkInput = true
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			local lockReturn = ""
			local items = Ao.me.inventory:get() --> get list of ALL items in inventory
			local inv = { --> create a blank inventory layout (easiest to fill a full inventory then chop the line we need)
				[1] = {0,0,0,0,0,0,0,0,0,0},
				[2] = {0,0,0,0,0,0,0,0,0,0},
				[3] = {0,0,0,0,0,0,0,0,0,0},
				[4] = {0,0,0,0,0,0,0,0,0,0}
			}
			for i = 0, items:size() - 1 do --> loop through items in inventory
				for j = items[i].y+1, items[i].y+items[i].baseItem.invHeight do --> check y location of item (AO holds location being the top left corner of item) and expand to its height
					for k = items[i].x+1, items[i].x+items[i].baseItem.invWidth do --> check x location of item (AO holds location being the top left corner of item) and expand to its width
						inv[j][k] = 1 --> mark the item spaces as locked
					end
				end
			end
			for i = 1, 10 do --> normalize for our purposes the proper line of inventory
				lockReturn = lockReturn..inv[3][i]..","
			end
			return lockReturn --> return the normalized lock line to be input to settings
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> relay user input for them to see
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoInvLock4:run(auto) --> custom run module for autoInvLock4
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize name
	if auto then --> check if setting should be automated
		local lockReturn = ""
		local items = Ao.me.inventory:get() --> get list of ALL items in inventory
		local inv = { --> create a blank inventory layout (easiest to fill a full inventory then chop the line we need)
			[1] = {0,0,0,0,0,0,0,0,0,0},
			[2] = {0,0,0,0,0,0,0,0,0,0},
			[3] = {0,0,0,0,0,0,0,0,0,0},
			[4] = {0,0,0,0,0,0,0,0,0,0}
		}
		for i = 0, items:size() - 1 do --> loop through items in inventory
			for j = items[i].y+1, items[i].y+items[i].baseItem.invHeight do --> check y location of item (AO holds location being the top left corner of item) and expand to its height
				for k = items[i].x+1, items[i].x+items[i].baseItem.invWidth do --> check x location of item (AO holds location being the top left corner of item) and expand to its width
					inv[j][k] = 1 --> mark the item spaces as locked
				end
			end
		end
		for i = 1, 10 do --> normalize for our purposes the proper line of inventory
			lockReturn = lockReturn..inv[4][i]..","
		end
		return lockReturn --> return the normalized lock line to be input to settings
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> loop until proper input!
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> break out if user decides to automate the process
				break
			elseif Ao:getString("message") ~= "ao" then --> if input is different from default
				local invLockReturn = ""
				local countComma = 0
				local countValue = 0
				local b = string.find(Ao:getString("message"), ",") --> find the first comma (string.find returns location in string
				while b do --> while b is not nil (ie : we found a comma) count all commas found
					countComma = countComma + 1
					b = string.find(Ao:getString("message"), ",", b+1) --> b is a placeholder for last comma found, so search from b+1 for next comma
				end
				b = string.find(Ao:getString("message"), "1") --> find the first 1
				while b do --> while we are finding 1s count them up
					countValue = countValue + 1
					b = string.find(Ao:getString("message"), "1", b+1) --> find next 1 by using b+1 (string location of last 1 '+1')
				end
				b = string.find(Ao:getString("message"), "0") --> find the first 0
				while b do --> while we are finding 0s count them up
					countValue = countValue + 1
					b = string.find(Ao:getString("message"), "0", b+1) --> find all the remaining 0s
				end
				if countComma ~= 10 then --> if we have more or less than 10 commas user input is wrong
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not the right number of commas "..countComma.." of 10)")
					Ao:set("message", "ao")
				elseif countValue ~= 10 then --> if we have more or less than 10 1's and 0's user input is wrong
					error:log("I am sorry but �c2"..Ao:getString("message").."�c0 is not an acceptable response (not the right number of values "..countValue.." of 10)")
					Ao:set("message", "ao")
				else --> looks good!
					checkInput = true
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			local lockReturn = ""
			local items = Ao.me.inventory:get() --> get list of ALL items in inventory
			local inv = { --> create a blank inventory layout (easiest to fill a full inventory then chop the line we need)
				[1] = {0,0,0,0,0,0,0,0,0,0},
				[2] = {0,0,0,0,0,0,0,0,0,0},
				[3] = {0,0,0,0,0,0,0,0,0,0},
				[4] = {0,0,0,0,0,0,0,0,0,0}
			}
			for i = 0, items:size() - 1 do --> loop through items in inventory
				for j = items[i].y+1, items[i].y+items[i].baseItem.invHeight do --> check y location of item (AO holds location being the top left corner of item) and expand to its height
					for k = items[i].x+1, items[i].x+items[i].baseItem.invWidth do --> check x location of item (AO holds location being the top left corner of item) and expand to its width
						inv[j][k] = 1 --> mark the item spaces as locked
					end
				end
			end
			for i = 1, 10 do --> normalize for our purposes the proper line of inventory
				lockReturn = lockReturn..inv[4][i]..","
			end
			return lockReturn --> return the normalized lock line to be input to settings
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> relay user input for them to see
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoMakeRune:run(auto) --> custom run module for autoMakeRune
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize the name
	if auto then --> check if setting should be automated
		if Ao.me.level > 60 then 
			return self.defaultOver60 --> just use the default under level 60 option set when constructed
		else
			return self.defaultUnder60 --> use the default over level 60 option set when constructed
		end
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> check if user decided to automate this setting
				break
			elseif string.lower(Ao:getString("message")) == "none" then --> user does not want this set!
				break
			elseif Ao:getString("message") ~= "ao" then --> check if input is not default
				local runeReturn = ""
				local fail = false
				for word in string.gmatch(Ao:getString("message"), "%w+") do --> loop each word in the user input (defined by spaces!)
					if string.lower(word) == "eld" then --> check rune and normalize it
						runeReturn = runeReturn.."Eld "
					elseif string.lower(word) == "tir" then --> check rune and normalize it
						runeReturn = runeReturn.."Tir "
					elseif string.lower(word) == "nef" then --> check rune and normalize it
						runeReturn = runeReturn.."Nef "
					elseif string.lower(word) == "eth" then --> check rune and normalize it
						runeReturn = runeReturn.."Eth "
					elseif string.lower(word) == "ith" then --> check rune and normalize it
						runeReturn = runeReturn.."Ith "
					elseif string.lower(word) == "tal" then --> check rune and normalize it
						runeReturn = runeReturn.."Tal "
					elseif string.lower(word) == "ral" then --> check rune and normalize it
						runeReturn = runeReturn.."Ral "
					elseif string.lower(word) == "ort" then --> check rune and normalize it
						runeReturn = runeReturn.."Ort "
					elseif string.lower(word) == "thul" then --> check rune and normalize it
						runeReturn = runeReturn.."Thul "
					elseif string.lower(word) == "amn" then --> check rune and normalize it
						runeReturn = runeReturn.."Amn "
					elseif string.lower(word) == "sol" then --> check rune and normalize it
						runeReturn = runeReturn.."Sol "
					elseif string.lower(word) == "shael" then --> check rune and normalize it
						runeReturn = runeReturn.."Shael "
					elseif string.lower(word) == "dol" then --> check rune and normalize it
						runeReturn = runeReturn.."Dol "
					elseif string.lower(word) == "hel" then --> check rune and normalize it
						runeReturn = runeReturn.."Hel "
					elseif string.lower(word) == "io" then --> check rune and normalize it
						runeReturn = runeReturn.."Io "
					elseif string.lower(word) == "lum" then --> check rune and normalize it
						runeReturn = runeReturn.."Lum "
					elseif string.lower(word) == "ko" then --> check rune and normalize it
						runeReturn = runeReturn.."Ko "
					elseif string.lower(word) == "fal" then --> check rune and normalize it
						runeReturn = runeReturn.."Fal "
					elseif string.lower(word) == "lem" then --> check rune and normalize it
						runeReturn = runeReturn.."Lem "
					elseif string.lower(word) == "pul" then --> check rune and normalize it
						runeReturn = runeReturn.."Pul "
					elseif string.lower(word) == "um" then --> check rune and normalize it
						runeReturn = runeReturn.."Um "
					elseif string.lower(word) == "mal" then --> check rune and normalize it
						runeReturn = runeReturn.."Mal "
					elseif string.lower(word) == "ist" then --> check rune and normalize it
						runeReturn = runeReturn.."Ist "
					elseif string.lower(word) == "gul" then --> check rune and normalize it
						runeReturn = runeReturn.."Gul "
					elseif string.lower(word) == "vex" then --> check rune and normalize it
						runeReturn = runeReturn.."Vex "
					elseif string.lower(word) == "ohm" then --> check rune and normalize it
						runeReturn = runeReturn.."Ohm "
					elseif string.lower(word) == "lo" then --> check rune and normalize it
						runeReturn = runeReturn.."Lo "
					elseif string.lower(word) == "sur" then --> check rune and normalize it
						runeReturn = runeReturn.."Sur "
					elseif string.lower(word) == "ber" then --> check rune and normalize it
						runeReturn = runeReturn.."Ber "
					elseif string.lower(word) == "jah" then --> check rune and normalize it
						runeReturn = runeReturn.."Jah "
					elseif string.lower(word) == "cham" then --> check rune and normalize it
						runeReturn = runeReturn.."Cham "
					elseif string.lower(word) == "zod" then --> check rune and normalize it
						runeReturn = runeReturn.."Zod "
					else --> not a real rune!
						error:log("I am sorry but �c2"..word.."�c0 is not an acceptable response please fix this and reinput your list")
						Ao:set("message", "ao")
						fail = true
						break
					end
				end
				if not fail then --> no errors in list, so its good to go!
					checkInput = true
					Ao:set("message", runeReturn) --> replace old message with normalized input
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			info:log(self.name.." will automatically be set for you")
			if Ao.me.level > 60 then --> again set defaults based off level
				return self.defaultOver60
			else
				return self.defaultUnder60
			end
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> user input is acceptable, let them know what they input and move on
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoMakeFlawed:run(auto) --> custom run module for autoMakeFlawed
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize the name
	if auto then --> check if setting should be automated
		if Ao.me.level > 60 then 
			return self.defaultOver60 --> just use the default under level 60 option set when constructed
		else
			return self.defaultUnder60 --> use the default over level 60 option set when constructed
		end
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> loop until we have proper input
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> user decided to automate setting
				break
			elseif string.lower(Ao:getString("message")) == "none" then --> user decided to blank out this setting
				break
			elseif Ao:getString("message") ~= "ao" then
				local gemReturn = ""
				local fail = false
				for word in string.gmatch(Ao:getString("message"), "%w+") do --> loop each word in the user input (defined by spaces!)
					if string.lower(word) == "skull" then --> check gem type and normalize it
						gemReturn = gemReturn.."Skull "
					elseif string.lower(word) == "amethyst" then --> check gem type and normalize it
						gemReturn = gemReturn.."Amethyst "
					elseif string.lower(word) == "sapphire" then --> check gem type and normalize it
						gemReturn = gemReturn.."Sapphire "
					elseif string.lower(word) == "emerald" then --> check gem type and normalize it
						gemReturn = gemReturn.."Emerald "
					elseif string.lower(word) == "ruby" then --> check gem type and normalize it
						gemReturn = gemReturn.."Ruby "
					elseif string.lower(word) == "diamond" then --> check gem type and normalize it
						gemReturn = gemReturn.."Diamond "
					elseif string.lower(word) == "topaz" then --> check gem type and normalize it
						gemReturn = gemReturn.."Topaz "
					else --> not a proper gem
						error:log("I am sorry but �c2"..word.."�c0 is not an acceptable response please fix this and reinput your list")
						Ao:set("message", "ao")
						fail = true
						break
					end
				end
				if not fail then --> no errors!
					checkInput = true
					Ao:set("message", gemReturn) --> set the message to our normalized input
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			info:log(self.name.." will automatically be set for you")
			if Ao.me.level > 60 then --> again set defaults based off level
				return self.defaultOver60
			else
				return self.defaultUnder60
			end
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> user input is acceptable, let them know what they input and move on
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoMakeNormal:run(auto) --> custom run module for autoMakeNormal
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize the name
	if auto then --> check if setting should be automated
		if Ao.me.level > 60 then 
			return self.defaultOver60 --> just use the default under level 60 option set when constructed
		else
			return self.defaultUnder60 --> use the default over level 60 option set when constructed
		end
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> loop until we have proper input
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> user decided to automate setting
				break
			elseif string.lower(Ao:getString("message")) == "none" then --> user decided to blank out this setting
				break
			elseif Ao:getString("message") ~= "ao" then
				local gemReturn = ""
				local fail = false
				for word in string.gmatch(Ao:getString("message"), "%w+") do --> loop each word in the user input (defined by spaces!)
					if string.lower(word) == "skull" then --> check gem type and normalize it
						gemReturn = gemReturn.."Skull "
					elseif string.lower(word) == "amethyst" then --> check gem type and normalize it
						gemReturn = gemReturn.."Amethyst "
					elseif string.lower(word) == "sapphire" then --> check gem type and normalize it
						gemReturn = gemReturn.."Sapphire "
					elseif string.lower(word) == "emerald" then --> check gem type and normalize it
						gemReturn = gemReturn.."Emerald "
					elseif string.lower(word) == "ruby" then --> check gem type and normalize it
						gemReturn = gemReturn.."Ruby "
					elseif string.lower(word) == "diamond" then --> check gem type and normalize it
						gemReturn = gemReturn.."Diamond "
					elseif string.lower(word) == "topaz" then --> check gem type and normalize it
						gemReturn = gemReturn.."Topaz "
					else --> not a proper gem
						error:log("I am sorry but �c2"..word.."�c0 is not an acceptable response please fix this and reinput your list")
						Ao:set("message", "ao")
						fail = true
						break
					end
				end
				if not fail then --> no errors!
					checkInput = true
					Ao:set("message", gemReturn) --> set the message to our normalized input
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			info:log(self.name.." will automatically be set for you")
			if Ao.me.level > 60 then --> again set defaults based off level
				return self.defaultOver60
			else
				return self.defaultUnder60
			end
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> user input is acceptable, let them know what they input and move on
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoMakeFlawless:run(auto) --> custom run module for autoMakeFlawless
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize the name
	if auto then --> check if setting should be automated
		if Ao.me.level > 60 then 
			return self.defaultOver60 --> just use the default under level 60 option set when constructed
		else
			return self.defaultUnder60 --> use the default over level 60 option set when constructed
		end
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> loop until we have proper input
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> user decided to automate setting
				break
			elseif string.lower(Ao:getString("message")) == "none" then --> user decided to blank out this setting
				break
			elseif Ao:getString("message") ~= "ao" then
				local gemReturn = ""
				local fail = false
				for word in string.gmatch(Ao:getString("message"), "%w+") do --> loop each word in the user input (defined by spaces!)
					if string.lower(word) == "skull" then --> check gem type and normalize it
						gemReturn = gemReturn.."Skull "
					elseif string.lower(word) == "amethyst" then --> check gem type and normalize it
						gemReturn = gemReturn.."Amethyst "
					elseif string.lower(word) == "sapphire" then --> check gem type and normalize it
						gemReturn = gemReturn.."Sapphire "
					elseif string.lower(word) == "emerald" then --> check gem type and normalize it
						gemReturn = gemReturn.."Emerald "
					elseif string.lower(word) == "ruby" then --> check gem type and normalize it
						gemReturn = gemReturn.."Ruby "
					elseif string.lower(word) == "diamond" then --> check gem type and normalize it
						gemReturn = gemReturn.."Diamond "
					elseif string.lower(word) == "topaz" then --> check gem type and normalize it
						gemReturn = gemReturn.."Topaz "
					else --> not a proper gem
						error:log("I am sorry but �c2"..word.."�c0 is not an acceptable response please fix this and reinput your list")
						Ao:set("message", "ao")
						fail = true
						break
					end
				end
				if not fail then --> no errors!
					checkInput = true
					Ao:set("message", gemReturn) --> set the message to our normalized input
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			info:log(self.name.." will automatically be set for you")
			if Ao.me.level > 60 then --> again set defaults based off level
				return self.defaultOver60
			else
				return self.defaultUnder60
			end
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> user input is acceptable, let them know what they input and move on
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end
function autoMakePerfect:run(auto) --> custom run module for autoMakePerfect
	trace:log("aobot.lua "..self.name.."() called")
	self.name = "�c1"..self.name.."�c0" --> colorize the name
	if auto then --> check if setting should be automated
		if Ao.me.level > 60 then 
			return self.defaultOver60 --> just use the default under level 60 option set when constructed
		else
			return self.defaultUnder60 --> use the default over level 60 option set when constructed
		end
	else --> settings not automated, talk to user about it!
		info:log("We are now going to set the value for : "..self.name)
		Ao:Sleep(500)
		info:log(self.name.." options : "..self.description)
		Ao:Sleep(500)
		Ao:set("message", "ao")
		local checkInput = false
		while not checkInput do --> loop until we have proper input
			Ao:Sleep(300)
			if string.lower(Ao:getString("message")) == "auto" then --> user decided to automate setting
				break
			elseif string.lower(Ao:getString("message")) == "none" then --> user decided to blank out this setting
				break
			elseif Ao:getString("message") ~= "ao" then
				local gemReturn = ""
				local fail = false
				for word in string.gmatch(Ao:getString("message"), "%w+") do --> loop each word in the user input (defined by spaces!)
					if string.lower(word) == "skull" then --> check gem type and normalize it
						gemReturn = gemReturn.."Skull "
					elseif string.lower(word) == "amethyst" then --> check gem type and normalize it
						gemReturn = gemReturn.."Amethyst "
					elseif string.lower(word) == "sapphire" then --> check gem type and normalize it
						gemReturn = gemReturn.."Sapphire "
					elseif string.lower(word) == "emerald" then --> check gem type and normalize it
						gemReturn = gemReturn.."Emerald "
					elseif string.lower(word) == "ruby" then --> check gem type and normalize it
						gemReturn = gemReturn.."Ruby "
					elseif string.lower(word) == "diamond" then --> check gem type and normalize it
						gemReturn = gemReturn.."Diamond "
					elseif string.lower(word) == "topaz" then --> check gem type and normalize it
						gemReturn = gemReturn.."Topaz "
					else --> not a proper gem
						error:log("I am sorry but �c2"..word.."�c0 is not an acceptable response please fix this and reinput your list")
						Ao:set("message", "ao")
						fail = true
						break
					end
				end
				if not fail then --> no errors!
					checkInput = true
					Ao:set("message", gemReturn) --> set the message to our normalized input
				end
			end
		end
		if string.lower(Ao:getString("message")) == "auto" then --> check to see if user would like this setting automated
			info:log(self.name.." will automatically be set for you")
			if Ao.me.level > 60 then --> again set defaults based off level
				return self.defaultOver60
			else
				return self.defaultUnder60
			end
		else --> not user opted automation
			if string.lower(Ao:getString("message")) == "none" then --> set a blank value if the user does not want this option set
				info:log(self.name.." will not be set to anything")
				Ao:set("message", "")
			else --> user input is acceptable, let them know what they input and move on
				info:log(self.name.." will be set to �c2"..Ao:getString("message"))
			end
			Ao:Sleep(500)
			return Ao:getString("message") --> return the message to be input to character file
		end
	end
end

--[[ application of automating settings ]]--
automateSettings = function() --> create new settings file for unknown characters!
	trace:log("aobot.lua automateSettings() called")
	os.execute("copy scripts\\chars\\settings.lua scripts\\chars\\"..Ao.character..".lua") --> copy the settings.lua (it could be any dummy file we are writing from scratch)
	os.execute("mkdir logs\\"..Ao.character)
	local f, e = io.open("scripts\\chars\\"..Ao.character..".lua", "w+") --> open the character file and erase all old data (easier to write it all and be 100% sure!)
	if f then
		info:log("Welcome to the Awesom-O character automation process, we will attempt to automate the settings for you character")
		Ao:Sleep(2000)
		info:log("After this process is complete it is suggested that you open your characters settings file and look it over to be sure there was no error during this process")
		Ao:Sleep(2000)
		info:log("Would you like the entire character settings to be automated? yes/no (you have 1 minute to respond)")
		Ao:set("message", "ao")
		local automate = true --> assume automation in case someone clicks start and wanders off!
		for i = 0, 60 do --> 60 ticks of 1 second = 1 minute
			if Ao:getString("message") == "no" then --> no automation, manual creation via prompts
				automate = false
				info:log("Ok it looks like you would like to set up your character step-by-step!")
				Ao:Sleep(2000)
				info:log("At any time during this process you may answer any of the questions with the term auto")
				Ao:Sleep(2000)
				info:log("Using the term auto will apply automatic settings to just that particular setting")
				Ao:Sleep(2000)
				info:log("Options are laid out as follows : �c1Setting name in red�c0, �c2setting options in green�c0, �c3setting info in �c3blue")
				Ao:Sleep(2000)
				break
			elseif Ao:getString("message") == "yes" then --> full automation (to consider having full automation for remaining files done at any point!)
				break
			else --> no new input, just sleep for a second
				Ao:Sleep(1000)
			end
		end
		local input
		f:write("--[[ Character Settings File ]]--", "\n")
		f:write("\n")
		f:write("--[[ Botting Options ]]--", "\n")
		input = autoDoThis:run(automate)
		f:write("doThis = \"", input, "\" --> lua code for runs goes here (similar to old sequence.lua)", "\n")
		input = autoPublic:run(automate)
		f:write("public = ", input, " --> true : send invites, chat, open TP @ boss", "\n")
		f:write("\n")
		f:write("--[[ Character Options ]]--", "\n")
		input = autoBuild:run(automate)
		f:write("build = \"", input, "\" --> character build type (check scripts\\chars\\builds for options)", "\n")
		input = autoLowLife:run(automate)
		f:write("lowLife = ", input, " --> % of life to drink a red potion at", "\n")
		input = autoRejuvLife:run(automate)
		f:write("rejuvLife = ", input, " --> % of life to drink a purple potion at", "\n")
		input = autoLowMana:run(automate)
		f:write("lowMana = ", input, " --> % of mana to drink a blue potion at", "\n")
		input = autoRejuvMana:run(automate)
		f:write("rejuvMana = ", input, " --> % of mana to drink a purple potion at", "\n")
		input = autoChickenTown:run(automate)
		f:write("chickenTown = ", input, " --> % of life to chicken to town", "\n")
		input = autoChickenExit:run(automate)
		f:write("chickenExit = ", input, " --> % of life to chicken exit game", "\n")
		input = autoUseMerc:run(automate)
		f:write("useMerc = ", input, " --> true to check for merc when in town", "\n")
		input = autoAutoResMerc:run(automate)
		f:write("autoResMerc = ", input, " --> true to automatically resurrect merc when he dies", "\n")
		f:write("\n")
		f:write("--[[ Item/Inventory Options ]]--", "\n")
		input = string.gsub(autoBelt:run(automate), "^%s*(.-)%s*$", "%1") --> trim trailing space
		f:write("belt = \"", input, "\" --> characters belt setup", "\n")
		f:write("invLock = { --> inventory lock 1 = locked || 0 = unlocked", "\n")
		input = autoInvLock1:run(automate)
			f:write("\t", input, "\n")
		input = autoInvLock2:run(automate)
			f:write("\t", input, "\n")
		input = autoInvLock3:run(automate)
			f:write("\t", input, "\n")
		input = autoInvLock4:run(automate)
			f:write("\t", input, "\n")
		f:write("}", "\n")
		input = autoPickit:run(automate)
		f:write("pickit = \"", input, "\" --> pickit folder name", "\n")
		input = autoMinGold:run(automate)
		f:write("minGold = ", input, " --> minimum gold amount to pick", "\n")
		input = autoAutoDump:run(automate)
		f:write("autoDump = ", input, " --> automatically check picked items (good for characters with low inventory space)", "\n")
		input = autoDisablePickit:run(automate)
		f:write("disablePickit = ", input, " --> set to true for leeching", "\n")
		f:write("\n")
		f:write("--[[ Cubing Options ]]--", "\n")
		input = autoMakeRune:run(automate)
		f:write("makeRune = \"", input, "\" --> list of runes to make (ie : \"Zod Vex Ohm Cham Hel\")", "\n")
		input = autoMakeFlawed:run(automate)
		f:write("makeFlawed = \"", input, "\" --> list of flawed gems to make (ie : \"Ruby Sapphire\")", "\n")
		input = autoMakeNormal:run(automate)
		f:write("makeNormal = \"", input, "\" --> list of normal gems to make (ie : \"Diamond Emerald\")", "\n")
		input = autoMakeFlawless:run(automate)
		f:write("makeFlawless = \"", input, "\" --> list of flawless gems to make (ie : \"Skull Topaz\")", "\n")
		input = autoMakePerfect:run(automate)
		f:write("makePerfect = \"", input, "\" --> list of perfect gems to make (ie : \"Amethyst\")", "\n")
		input = autoMakeTokens:run(automate)
		f:write("makeTokens = ", input, " --> set true to auto-pick essences and make tokens", "\n")
		f:write("\n")
		f:write("--[[ Player Options ]]--", "\n")
		input = autoKillHostiles:run(automate)
		f:write("killHostiles = ", input, " --> set true to kill Hostiles when in sight (false to chicken exit on sight)", "\n")
		input = autoSquelchLevel:run(automate)
		f:write("squelchLevel = ", input, " --> minimum level to not be squelched", "\n")
		f:write("\n")
		f:write("--[[ Misc Options ]]--")
		input = autoOpenAllChests:run(automate)
		f:write("openAllChests = ", input, " --> true to open all chests, false will open sparkling chests still", "\n")
		input = autoShrinePriority:run(automate)
		f:write("shrinePriority = {", "} --> CURRENTLY NOT WORKING priority list for getting shrines (ie : \"Experience\", \"Skill\", \"FireResist\")", "\n")
		f:write("\n")
		f:write("--[[ Message Options ]]--")
		input = autoTownChickenMessage:run(automate)
		f:write("townChickenMessage = \"", input, "\" --> message to send when chickening to town", "\n")
		input = autoExitChickenMessage:run(automate)
		f:write("exitChickenMessage = \"", input, "\" --> message to send when chickening from game", "\n")
		input = autoHotTPMessage:run(automate)
		f:write("hotTPMessage = \"", input, "\" --> message to send when TP has monsters around it", "\n")
		input = autoColdTPMessage:run(automate)
		f:write("coldTPMessage = \"", input, "\" --> message to send when TP has no monster around it", "\n")
		input = autoNewGameMessage:run(automate)
		f:write("newGameMessage = \"", input, "\" --> message to send when moving on to next game", "\n")
	else
		error:log("automation error, couldnt open character file.  Error = "..tostring(e))
	end
	f:close()

	trace:log("aobot.lua automateSettings() finished")
end
