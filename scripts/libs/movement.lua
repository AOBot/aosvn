--[[ Moving : including town movements, walking and teleporting ]]--
--[[
	With AO being a packet based bot, it is hard sometimes to get a real feel for how well we are tracking ourselves.  Location updates in town are sometimes
	really far apart making it hard to determine if we have gotten stuck, or veered from our path.  One of the ways we are going to try to combat this is to cast
	any spells we can in town if we have full mana.  When life and mana are updated (and mana regens pretty regularly) we are given location updates.  This of
	course will not work with every character, but when it does work it will be very helpful in keeping town location more up to date.  
	One of the goals in the production of movements for AO is to work seamless with characters that do not have teleport, and with characters that do have teleport.
	This includes characters that have teleport but do not have a lot of mana (low level sorcs for example).  We will attempt to make the process of walking and teleporting
	as smooth as possible.  Another goal will be for a hammerdin that walks, and to cast hammers as he is moving which would be ideal for classic paladins that have to walk
	through chaos sanctuary, or any paladin that gets stuck while walking around.  We have all that is necessary for charging too... lets apply that.
	Walk pathing is something that is pretty difficult to do, it takes a ton of consideration to get it done right.  There will a strong effort to make sure that Awesom-O is 
	a bot that is readily available for ladder resets, classic, as well as hardcore characters of any sort.  
	
	So the issue arises on how we deal with a bot that can teleport but might have to walk? (maybe for now we dont?)
	Should there be a seperate function for that, and we keep the seperation of walking and teleporting?
	How can this been done object oriented...
	Must change how we check warps/transitions.  We now have an event that will keep in memory for us the last loaded map (if the last loaded map is our map, transition... otherwise DONT!)
	
	
	Core Functions/Variables Used : 
	Ao.maps:inTown() -> return true if character is in town, false if character is out of town
	Ao.maps:getLevel() -> returns the arealevel the character is currently in
	Ao.maps:pathToNpc(NPCCode) -> returns the x,y points of the given NPCCode
	Ao.maps:pathTo(X, Y, BOOLEANTELEPORTOPTION, STEPRANGE) -> returns an array of points for a path to given x,y either teleporting or walking and how far each step should be
	Ao.maps:pathToObject(GameObjectID) -> returns the x,y points of the given GameObjectID
	Ao.maps:getPath(AreaLevel1, AreaLevel2) -> returns an array of areas to path from AreaLevel1 to AreaLevel2
	Ao.maps:GetCorrectTomb() -> returns the correct AreaLevel of the TalRashasTomb containing the duriel portal
	Ao.maps:pathToLevel(AreaLevel) -> returns the x,y points of the given transition to AreaLevel
	Ao.maps:explore() -> generates points in the current map that will encompas the entire area
	Ao.maps:getWp() -> returns the object of a waypoint in the current map (or 0)
	
	Ao.objects:FindTownPortalByDestination(AREALEVELHERE) -> returns an object (which maybe be a null object (checked with uid)) of a townportal for a specific destination
	Ao.objects:find(GameObjectID) -> returns an object (which may be a null object (checked with uid)) of the given GameObjectID
	Ao.objects:FindWaypoint() -> returns the object of a close wp (used when interacting with a WP) check with uid
	
	Ao.npcs:findByNpcCode(NPCCode) -> return an npc (which may be a null npc (checked with uid)) of the given NPCCode
	Ao.npcs:findBySuperUnique(uniqueid) -> returns an npc (which may be a null npc (checked with uid)) of the given superuniqueid
	Ao.npcs:countInRadius(x, y, rad) -> returns number of npcs in the given radius from point x,y
	
	Ao.warps:find(x, y, rad) -> returns a warp (transition) within given radius of point x,y (or returns 0)
	Ao.warps:find(WarpType) -> returns a warp (transition) within "visiable radius" of character of the WarpType
	
	Ao.skills:find(SkillType) -> returns true if character has skill, false if not
	
	Ao.me.act -> the current act of the character
	Ao.me.x -> characters x location
	Ao.me.y -> characters y location
	Ao.me.uid -> characters unique identifying number
	Ao.me.waypoints[x] -> a list of wps in bool (true = has that wp, false = does no have)
	Ao.me.states:isActive(StateType) -> returns true if character has StateType applied, false if not
	
	Ao:GoToTownFolk(UnitType, uid, x, y) -> gets attention of UnitType initiating interaction in town
	Ao:RunToTarget(UnitType, uid) -> character runs to target of UnitType and uid
	Ao:RunToLocation(x, y) -> character runs to point x,y
	Ao:InitWaitingPacket(PacketNumber) -> notifies core we will be waiting for a specific packet soon
	Ao:WaitForInitPacket(ms) -> sleeps in ms waiting returning true if the Ao:InitWaitingPacket() was intercepted during the total time (false otherwise)
	Ao:UnitInteract(UnitType, uid) -> character interacts with uid of UnitType
	Ao:WaypointInteract(waypointUID, WaypointDestination) -> interacts with a waypoint of uid moving to WaypointDestination level
	Ao:RequestReassign(UnitType, uid) -> sends a request to server for reassignment of uid of UnitType
	Ao:Chat(msg) -> displays msg in core window and on Diablo II screen
	Ao:getNumber("variablename") -> retrieves a number from core of variable name variablename
--]]
town = {
	tpLoc = { --> Locations where TPs pop up, act 1 is easiest to do the Bonfire, we will have to check this when using this table
		[1] = {GameObjectID.RogueBonfire},
		[2] = {5171, 5058},
		[3] = {5154, 5068},
		[4] = {5047, 5041},
		[5] = {5100, 5020},
	},
	areaLevel = { --> constructor to get the town for the area a player is in, will fill in with a loop
	},
}
for i = 1, 137 do --> all the area levels in the game
	if i < 40 then --> 1 to 39 are act 1
		town.areaLevel[i] = AreaLevel.RogueEncampment
	elseif i < 75 then --> 40 to 74 are act 2
		town.areaLevel[i] = AreaLevel.LutGholein
	elseif i < 103 then --> 75 to 102 are act 3
		town.areaLevel[i] = AreaLevel.KurastDocks
	elseif i < 109 then --> 103 to 108 are act 4
		town.areaLevel[i] = AreaLevel.ThePandemoniumFortress
	elseif i < 137 then --> 109 to 136 are act 5
		town.areaLevel[i] = AreaLevel.Harrogath
	else --> just a default to be sure
		town.areaLevel[i] = AreaLevel.RogueEncampment
	end
end

wp = {
	area = {
		[0] = AreaLevel.RogueEncampment,
		[1] = AreaLevel.ColdPlains,
		[2] = AreaLevel.StonyField,
		[3] = AreaLevel.DarkWood,
		[4] = AreaLevel.BlackMarsh,
		[5] = AreaLevel.OuterCloister,
		[6] = AreaLevel.JailLevel1,
		[7] = AreaLevel.InnerCloister,
		[8] = AreaLevel.CatacombsLevel2,
		[9] = AreaLevel.LutGholein,
		[10] = AreaLevel.SewersLevel2Act2,
		[11] = AreaLevel.DryHills,
		[12] = AreaLevel.HallsOfTheDeadLevel2,
		[13] = AreaLevel.FarOasis,
		[14] = AreaLevel.LostCity,
		[15] = AreaLevel.PalaceCellarLevel1,
		[16] = AreaLevel.ArcaneSanctuary,
		[17] = AreaLevel.CanyonOfTheMagi,
		[18] = AreaLevel.KurastDocks,
		[19] = AreaLevel.SpiderForest,
		[20] = AreaLevel.GreatMarsh,
		[21] = AreaLevel.FlayerJungle,
		[22] = AreaLevel.LowerKurast,
		[23] = AreaLevel.KurastBazaar,
		[24] = AreaLevel.UpperKurast,
		[25] = AreaLevel.Travincal,
		[26] = AreaLevel.DuranceOfHateLevel2,
		[27] = AreaLevel.ThePandemoniumFortress,
		[28] = AreaLevel.CityOfTheDamned,
		[29] = AreaLevel.RiverOfFlame,
		[30] = AreaLevel.Harrogath,
		[31] = AreaLevel.FrigidHighlands,
		[32] = AreaLevel.ArreatPlateau,
		[33] = AreaLevel.CrystallinePassage,
		[34] = AreaLevel.GlacialTrail,
		[35] = AreaLevel.HallsOfPain,
		[36] = AreaLevel.FrozenTundra,
		[37] = AreaLevel.TheAncientsWay,
		[38] = AreaLevel.TheWorldStoneKeepLevel2,
	},
	list = {
		[AreaLevel.RogueEncampment] = 0,
		[AreaLevel.ColdPlains] = 1,
		[AreaLevel.StonyField] = 2,
		[AreaLevel.DarkWood] = 3,
		[AreaLevel.BlackMarsh] = 4,
		[AreaLevel.OuterCloister] = 5,
		[AreaLevel.JailLevel1] = 6,
		[AreaLevel.InnerCloister] = 7,
		[AreaLevel.CatacombsLevel2] = 8,
		[AreaLevel.LutGholein] = 9,
		[AreaLevel.SewersLevel2Act2] = 10,
		[AreaLevel.DryHills] = 11,
		[AreaLevel.HallsOfTheDeadLevel2] = 12,
		[AreaLevel.FarOasis] = 13,
		[AreaLevel.LostCity] = 14,
		[AreaLevel.PalaceCellarLevel1] = 15,
		[AreaLevel.ArcaneSanctuary] = 16,
		[AreaLevel.CanyonOfTheMagi] = 17,
		[AreaLevel.KurastDocks] = 18,
		[AreaLevel.SpiderForest] = 19,
		[AreaLevel.GreatMarsh] = 20,
		[AreaLevel.FlayerJungle] = 21,
		[AreaLevel.LowerKurast] = 22,
		[AreaLevel.KurastBazaar] = 23,
		[AreaLevel.UpperKurast] = 24,
		[AreaLevel.Travincal] = 25,
		[AreaLevel.DuranceOfHateLevel2] = 26,
		[AreaLevel.ThePandemoniumFortress] = 27,
		[AreaLevel.CityOfTheDamned] = 28,
		[AreaLevel.RiverOfFlame] = 29,
		[AreaLevel.Harrogath] = 30,
		[AreaLevel.FrigidHighlands] = 31,
		[AreaLevel.ArreatPlateau] = 32,
		[AreaLevel.CrystallinePassage] = 33,
		[AreaLevel.GlacialTrail] = 34,
		[AreaLevel.HallsOfPain] = 35,
		[AreaLevel.FrozenTundra] = 36,
		[AreaLevel.TheAncientsWay] = 37,
		[AreaLevel.TheWorldStoneKeepLevel2] = 38,
	},
}
wp.dest = function(area)
	if (area == AreaLevel.RogueEncampment) then return WaypointDestination.RogueEncampment
	elseif (area == AreaLevel.ColdPlains) then return WaypointDestination.ColdPlains
    elseif area == AreaLevel.StonyField then return WaypointDestination.StonyField
	elseif area == AreaLevel.DarkWood then return WaypointDestination.DarkWood
	elseif area == AreaLevel.BlackMarsh then return WaypointDestination.BlackMarsh
	elseif area == AreaLevel.OuterCloister then return WaypointDestination.OuterCloister
	elseif area == AreaLevel.JailLevel1 then return WaypointDestination.JailLevel1
	elseif area == AreaLevel.InnerCloister then return WaypointDestination.InnerCloister
	elseif area == AreaLevel.CatacombsLevel2 then return WaypointDestination.CatacombsLevel2
	elseif area == AreaLevel.LutGholein then return WaypointDestination.LutGholein
	elseif area == AreaLevel.SewersLevel2Act2 then return WaypointDestination.LutGholeinSewersLevel2
	elseif area == AreaLevel.DryHills then return WaypointDestination.DryHills
	elseif area == AreaLevel.HallsOfTheDeadLevel2 then return WaypointDestination.HallsOfTheDeadLevel2
	elseif area == AreaLevel.FarOasis then return WaypointDestination.FarOasis
	elseif area == AreaLevel.LostCity then return WaypointDestination.LostCity
	elseif area == AreaLevel.PalaceCellarLevel1 then return WaypointDestination.PalaceCellarLevel1
	elseif area == AreaLevel.ArcaneSanctuary then return WaypointDestination.ArcaneSanctuary
	elseif area == AreaLevel.CanyonOfTheMagi then return WaypointDestination.CanyonOfTheMagi
	elseif area == AreaLevel.KurastDocks then return WaypointDestination.KurastDocks
	elseif area == AreaLevel.SpiderForest then return WaypointDestination.SpiderForest
	elseif area == AreaLevel.GreatMarsh then return WaypointDestination.GreatMarsh
	elseif area == AreaLevel.FlayerJungle then return WaypointDestination.FlayerJungle
	elseif area == AreaLevel.LowerKurast then return WaypointDestination.LowerKurast
	elseif area == AreaLevel.KurastBazaar then return WaypointDestination.KurastBazaar
	elseif area == AreaLevel.UpperKurast then return WaypointDestination.UpperKurast
	elseif area == AreaLevel.Travincal then return WaypointDestination.Travincial
	elseif area == AreaLevel.DuranceOfHateLevel2 then return WaypointDestination.DuranceOfHateLevel2
	elseif area == AreaLevel.ThePandemoniumFortress then return WaypointDestination.ThePandemoniumFortress
	elseif area == AreaLevel.CityOfTheDamned then return WaypointDestination.CityOfTheDamned
	elseif area == AreaLevel.RiverOfFlame then return WaypointDestination.RiverOfFlame
	elseif area == AreaLevel.Harrogath then return WaypointDestination.Harrogath
	elseif area == AreaLevel.BloodyFoothills then return WaypointDestination.Harrogath
	elseif area == AreaLevel.FrigidHighlands then return WaypointDestination.FrigidHighlands
	elseif area == AreaLevel.ArreatPlateau then return WaypointDestination.ArreatPlateau
	elseif area == AreaLevel.CrystallinePassage then return WaypointDestination.CrystallinePassage
	elseif area == AreaLevel.GlacialTrail then return WaypointDestination.GlacialTrail
	elseif area == AreaLevel.HallsOfPain then return WaypointDestination.HallsOfPain
	elseif area == AreaLevel.FrozenTundra then return WaypointDestination.FrozenTundra
	elseif area == AreaLevel.TheAncientsWay then return WaypointDestination.TheAncientsWay
	elseif area == AreaLevel.TheWorldStoneKeepLevel2 then return WaypointDestination.WorldstoneKeepLevel2
    end

	return WaypointDestination.CloseWaypoint
end
Walkthrough = {
	[1] = { 1 },
	[2] = { 1, 2 },
	[3] = { 1,2,3 },
	[4] = { 1,2,3,4 },
	[5] = { 1,2,3,4,10,5 },
	[6] = { 1,2,3,4,10,5,6 },
	[7] = { 1,2,3,4,10,5,6,7 },
	[8] = { 1,2,8 },
	[9] = { 1,2,3,9 },
	[10] = { 1,2,3,4,10 },
	[11] = { 1,2,3,4,10,5,6,11 },
	[12] = { 1,2,3,4,10,5,6,7,12 },
	[13] = { 1,2,3,9,13 },
	[14] = { 1,2,3,4,10,14 },
	[15] = { 1,2,3,4,10,5,6,11,15 },
	[16] = { 1,2,3,4,10,5,6,7,16 },
	[17] = { 1,2,3,17 },
	[18] = { 1,2,3,17,18 },
	[19] = { 1,2,3,17,19 },
	[20] = { 1,2,3,4,10,5,6,20 },
	[21] = { 1,2,3,4,10,5,6,20,21 },
	[22] = { 1,2,3,4,10,5,6,20,21,22 },
	[23] = { 1,2,3,4,10,5,6,20,21,22,23 },
	[24] = { 1,2,3,4,10,5,6,20,21,22,23,24 },
	[25] = { 1,2,3,4,10,5,6,20,21,22,23,24,25 },
	[26] = { 1,2,3,4,10,5,6,7,26 },
	[27] = { 1,2,3,4,10,5,6,7,26,27 },
	[28] = { 1,2,3,4,10,5,6,7,26,27,28 },
	[29] = { 1,2,3,4,10,5,6,7,26,27,28,29 },
	[30] = { 1,2,3,4,10,5,6,7,26,27,28,29,30 },
	[31] = { 1,2,3,4,10,5,6,7,26,27,28,29,30,31 },
	[32] = { 1,2,3,4,10,5,6,7,26,27,28,29,30,31,32 },
	[33] = { 1,2,3,4,10,5,6,7,26,27,28,29,30,31,32,33 },
	[34] = { 1,2,3,4,10,5,6,7,26,27,28,29,30,31,32,33,34 },
	[35] = { 1,2,3,4,10,5,6,7,26,27,28,29,30,31,32,33,34,35 },
	[36] = { 1,2,3,4,10,5,6,7,26,27,28,29,30,31,32,33,34,35,36 },
	[37] = { 1,2,3,4,10,5,6,7,26,27,28,29,30,31,32,33,34,35,36,37 },
	[38] = { 1,2,3,4,38 },
	[39] = { 1,39 },
	--Act2,
	[40] = { 40 },
	[41] = { 40,41 },
	[42] = { 40,41,42 },
	[43] = { 40,41,42,43 },
	[44] = { 40,41,42,43,44 },
	[45] = { 40,41,42,43,44,45 },
	[46] = { 40,50,51,52,53,54,74,46 },
	[47] = { 40,47 },
	[48] = { 40,47,48 },
	[49] = { 40,47,48,49 },
	[50] = { 40,50 },
	[51] = { 40,50,51 },
	[52] = { 40,50,51,52 },
	[53] = { 40,50,51,52,53 },
	[54] = { 40,50,51,52,53,54 },
	[55] = { 40,41,55 },
	[56] = { 40,41,42,56 },
	[57] = { 40,41,42,56,57 },
	[58] = { 40,41,42,43,44,45,58 },
	[59] = { 40,41,55,59 },
	[60] = { 40,41,42,56,57,60 },
	[61] = { 40,41,42,43,44,45,58,61 },
	[62] = { 40,41,42,43,62 },
	[63] = { 40,41,42,43,62,63 },
	[64] = { 40,41,42,43,62,63,64 },
	[65] = { 40,41,42,43,44,65 },
	[66] = { 40,50,51,52,53,54,74,46,66 },
	[67] = { 40,50,51,52,53,54,74,46,67 },
	[68] = { 40,50,51,52,53,54,74,46,68 },
	[69] = { 40,50,51,52,53,54,74,46,69 },
	[70] = { 40,50,51,52,53,54,74,46,70 },
	[71] = { 40,50,51,52,53,54,74,46,71 },
	[72] = { 40,50,51,52,53,54,74,46,72 },
	[73] = { 0 }, --Can't get this :<
	[74] = { 40,50,51,52,53,54,74 },
	--Act3
	[75] = { 75 },
	[76] = { 75,76 },
	[77] = { 75,76,77 },
	[78] = { 75,76,78 }, --Spiderforest (76) is bound to fail.,
	[79] = { 75,76,78,79 },
	[80] = { 75,76,78,79,80 },
	[81] = { 75,76,78,79,80,81 },
	[82] = { 75,76,78,79,80,81,82 },
	[83] = { 75,76,78,79,80,81,82,83},
	[84] = { 75,76,84 }, --Cave, wtf?,
	[85] = { 75,76,85 }, --Cavern,
	[86] = { 75,76,78,86 },
	[87] = { 75,76,78,86,87 },
	[88] = { 75,76,78,88 },
	[89] = { 75,76,78,88,89 },
	[90] = { 75,76,78,86,87,90 },
	[91] = { 75,76,78,88,89,91 },
	[92] = { 75,76,78,79,80,92 },
	[93] = { 75,76,78,79,80,92,93 },
	[94] = { 75,76,78,79,80,94 },
	[95] = { 75,76,78,79,80,95 },
	[96] = { 75,76,78,79,80,81,96 },
	[97] = { 75,76,78,79,80,81,97 },
	[98] = { 75,76,78,79,80,81,82,98 },
	[99] = { 75,76,78,79,80,81,82, 99 },
	[100] = { 75,76,78,79,80,81,82,83,100 },
	[101] = { 75,76,78,79,80,81,82,83,100,101 },
	[102] = { 75,76,78,79,80,81,82,83,100,101,102 },
	--Act4
	[103] = { 103 },
	[104] = { 103,104 },
	[105] = { 103,104,105 },
	[106] = { 103,104,105,106 },
	[107] = { 103,104,105,106,107 },
	[108] = { 103,104,105,106,107, 108 },
	--Act5
	[109] = {109 },
	[110] = {109,110 },
	[111] = {109,110,111 },
	[112] = {109,110,111,112 },
	[113] = {109,110,111,112,113 },
	[114] = {109,110,111,112,114 },
	[115] = {109,110,111,112,113,115 },
	[116] = {109,110,111,112,113,115,116 },
	[117] = {109,110,111,112,113,115,117 },
	[118] = {109,110,111,112,113,115,117,118 },
	[119] = {109,110,111,112,113,115,117,118,119 },
	[120] = {109,110,111,112,113,115,117,118,120 },
	[121] = {109,121 },
	[122] = {109,121,122 },
	[123] = {109,121,122,123 },
	[124] = {109,121,122,123,124 },
	[125] = {109,110,111,125 },
	[126] = {109,110,111,112,126 },
	[127] = {109,110,111,112,113,115,117,127 },
	[128] = {109,110,111,112,113,115,117,118,120,128 },
	[129] = {109,110,111,112,113,115,117,118,120,128,129 },
	[130] = {109,110,111,112,113,115,117,118,120,128,129,130 },
	[131] = {109,110,111,112,113,115,117,118,120,128,129,130,131 },
	[132] = {109,110,111,112,113,115,117,118,120,128,129,130,131,132 },
	[133] = {109,133 },
	[134] = {109,134 },
	[135] = {109,135 },
	[136] = {109,136 },
}

move = {}
move.npc = function(npcID) --> move to an npc
	trace:log("move.npc")
	if Ao.maps:inTown() then --> if we are in town go call the townMove functions instead
		trace:log("move.npc routing for town")
		return townMove.npc(npcID)
	end
	local npc = {} --> table constructor to hold the npc class in
	npc.uid = 0 --> initialize the npc.uid to 0 so we can always return the npc and have uid = 0 if no npc was found
	
	trace:log("move.npc checking for npc and super unique")
	npc.id, npc.superunique, npc.uniqueid = check.superNpc(npcID) --> set the values of npc class that we can find so far with enumerators
	if npc.id == -1 then
		error:log("move.npc  check.superNpc() failed")
		return npc
	end
	
	trace:log("move.npc checking npc")
	npc = find.npc(npc.id, npc.superunique, npc.uniqueid) --> find the npc (if we have received actual packet data or not)
	if npc.uid > 0 then --> found the npc
		debug:log("move.npc found the npc returning")
		return npc
	end
	
	trace:log("move.npc locating npc")
	npc = locate.npc(npc.id) --> set the x,y value of npc
	local route
	if npc.x > 0 and npc.y > 0 then --> location is good
		debug:log("move.npc found npc, generating a route to npc")
		route = path.to(npc.x, npc.y)
		if route:size() == 0 then --> no path generated
			error:log("move.npc failed to generate a path to npc")
			return npc
		end
	else
		error:log("move.npc map location was invalid")
		return npc
	end

	trace:log("move.npc moving to npc")
	local step, walkTimer, checkX, checkY = 0, os.clock(), Ao.me.x, Ao.me.y --> initialize some check variables
	local var = {} --> simplify new vs old locations
	repeat --> movement loop
		trace:log("move.npc step "..1+step.." of "..1+route:size())
		if step == 0 then --> set old location properly
			var.x = Ao.me.x
			var.y = Ao.me.y
		else 
			var.x = route[step-1].x
			var.y = route[step-1].y
		end
		if not move.point(route[step], var.x, var.y) then --> move failed (teleport failed specifically)
			debug:log("move.npc repathing couldnt teleport")
			route = path.to(npc.x, npc.y) --> repath cuz stuck
			step = -1
			if route:size() == 0 then --> bad path
				error:log("move.npc failed to repath to npc")
				return npc
			end
		end
		if not Ao.skills:find(SkillType.Teleport) then --> were walking
			trace:log("move.npc checking walk timer")
			if os.clock() - walkTimer > .75 then --> time it takes to update location
				trace:log("move.npc  walk timer tripped")
				if Ao.me.x == checkX and Ao.me.y == checkY then --> we havent moved
					debug:log("move.npc repathing couldnt walk")
					route = path.to(npc.x, npc.y) --> repath
					if route:size() == 0 then --> repath failed
						error:log("move.npc failed to repath to npc")
						return npc
					end
					step = - 1 --> will be incremented to 0
					walkTimer = os.clock()
				else --> we have moved and it registered!
					debug:log("move.npc resetting walk stats")
					walkTimer = os.clock()
					checkX = Ao.me.x
					checkY = Ao.me.y
				end
			end
			if step == route:size() - 1 then --> check distance at last step with a longer waiting time
				trace:log("move.npc last step for walkers")
				repeat --> end of route timer
					sleep(100)
				until os.clock() - walkTimer >= 2 or calc.dist(Ao.me.x, Ao.me.y, npc.x, npc.y) <= 6 --> 2 second at most to update or distance is good
				if calc.dist(Ao.me.x, Ao.me.y, npc.x, npc.y) > 6 then --> swing and a miss
					debug:log("move.npc last step failed to make the distance")
					route = path.to(npc.x, npc.y) --> repath
					if route:size() == 0 then
						error:log("move.npc failed to repath at end of loop")
						return npc
					end
					step = -1
					walkTimer = os.clock()
					checkX = Ao.me.x
					checkY = Ao.me.y
				end
			end
		end
		npc = find.npc(npc.id, npc.superunique, npc.uniqueid) --> find the npc
		if npc.uid > 0 then --> found him
			debug:log("move.npc found the npc during pathing")
			return npc
		end
		step = step + 1
	until step == route:size() --> end of path
	return npc --> default return
end
move.object = function(obj, telekinesis) --> move to an object (telekinesis should be true if the object CAN be activated by it)
	trace:log("move.object")
	if Ao.maps:inTown() then --> we are in town call the town moving function
		trace:log("move.object routing for town")
		return townMove.object(obj, telekinesis)
	end
	
	trace:log("move.object allocating object")
	obj = getNumber(GameObjectID, obj) --> get number value for object
	if not obj then
		error:log("move.object invalid GameObjectID")
		return false
	end
	local object = {}
	object.uid = 0
	
	trace:log("move.object locating object")
	local point = locate.object(obj)
	local route
	if point.x > 0 and point.y > 0 then --> location good
		debug:log("move.object found object, generating a path")
		route = path.to(point.x, point.y)
		if route:size() == 0 then --> no path generated
			error:log("move.object failed to generate a path")
			return false
		end
	else
		error:log("move.object location was bad")
		return false
	end
	
	trace:log("move.object moving to object")
	local step, walkTimer, checkX, checkY = 0, os.clock(), Ao.me.x, Ao.me.y --> some check variables
	local var = {} --> simplify walking new vs old locations
	repeat --> movement loop
		trace:log("move.object step "..1+step.." of "..1+route:size())
		if step == 0 then --> set old location properly
			var.x = Ao.me.x
			var.y = Ao.me.y
		else
			var.x = route[step-1].x
			var.y = route[step-1].y
		end
		if not move.point(route[step], var.x, var.y) then --> move failed (teleport specifically)
			debug:log("move.object repathing couldnt teleport")
			route = path.to(point.x, point.y) --> repath cuz suck
			step = -1
			if route:size() == 0 then 
				error:log("move.object failed to repath to object")
				return false
			end
		end
		if not Ao.skills:find(SkillType.Teleport) then --> we are a walker
			trace:log("move.object checking walk timer")
			if os.clock() - walkTimer > .75 then --> time it takes to update location
				trace:log("move.object walk timer tripped")
				if Ao.me.x == checkX and Ao.me.y == checkY then --> we havent moved
					debug:log("move.object repathing couldnt walk")
					route = path.to(point.x, point.y)
					if route:size() == 0 then --> fail
						error:log("move.object failed to repath to object")
						return false
					end
					step = -1
					walkTimer = os.clock()
				else --> we have moved successfully
					debug:log("move.object resetting walking stats")
					walkTimer = os.clock()
					checkX = Ao.me.x
					checkY = Ao.me.y
				end
			end
			if step == route:size() - 1 then --> check distance on last step
				trace:log("move.object last step for walkers")
				repeat
					sleep(100)
				until os.clock() - walkTimer >= 1 or calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) <= 6 --> 1 second or distance less than 6
				if calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) > 6 then --> fail
					debug:log("move.object last step failed to reach distance")
					route = path.to(point.x, point.y)
					if route:size() == 0 then
						error:log("move.object failed to repath at end of loop")
						return false
					end
					step = -1
					walkTimer = os.clock()
					checkX = Ao.me.x
					checkY = Ao.me.y
				end
			end
		end
		if telekinesis then --> we just need to be close enough to use telekinesis
			object = find.object(obj)
			if object.uid > 0 and distance(Ao.me.x, Ao.me.y, object.x, object.y) <= 20 then
				return true --> we are close enough to kinesis
			end
		end 
		step = step + 1
	until path == route:size()
	
	trace:log("move.object finalizing movement with a tounit call")
	object = find.object(obj)
	if object.uid > 0 then --> found the object
		debug:log("move.object found object for target")
		if not Ao.skills:find(SkillType.Teleport) then --> should only have to use target if we dont teleport
			move.target(UnitType.GameObject, object)
		end
		return true
	else
		error:log("move.object couldnt find object for target")
	end
end
move.point = function(point, prevX, prevY) -->move to specific point
	trace:log("move.point")
	if Ao.skills:find(SkillType.Teleport) and Ao.me.mana > 20 then --> teleport is a go
		return move.tele(point.x, point.y)
	else --> no teleporting
		if Ao.skills:find(SkillType.Teleport) then --> we can teleport but didnt (low mana) This could be troublesome when the teleport path was across a natural barrier (wall, river, etc)
			local route = path.walk(point.x, point.y)
			if route:size() == 0 then --> bad pathing
				error:log("move.point couldnt generate a walking path to teleport point")
				return false
			end
			local step, walkTimer, checkX, checkY = 0, os.clock(), Ao.me.x, Ao.me.y
			repeat
				if step == 0 then --> first step walking check against current location (for dist calculations)
					move.walk(route[step].x, route[step].y, Ao.me.x, Ao.me.y)
				else --> all other steps use previous step for distance calculations
					move.walk(route[step].x, route[step].y, route[step-1].x, route[step-1].y)
				end
				if os.clock() - walkTimer > .75 then --> time it takes to update location
					if Ao.me.x == checkX and Ao.me.y == checkY then --> we havent moved
						route = path.walk(point.x, point.y) --> repath
						if route:size() == 0 then --> repath failed
							error:log("move.point failed to repath to teleporting walk point")
							return false
						end
						step = step - 1 --> will be incremented to 0
						walkTimer = os.clock()
					else --> we have moved and it registered!
						walkTimer = os.clock()
						checkX = Ao.me.x
						checkY = Ao.me.y
					end
				end
				if step == route:size() - 1 then --> check distance at last step with a longer waiting time
					repeat --> end of route timer
						sleep(100)
					until os.clock() - walkTimer >= 1 or calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) <= 6 --> 1 second at most to update or distance is good
					if calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) > 6 then --> swing and a miss
						route = path.to(point.x, point.y) --> repath
						if route:size() == 0 then
							error:log("move.point failed to repath at end of loop of teleporting walk point")
							return false
						end
						step = -1
						walkTimer = os.clock()
						checkX = Ao.me.x
						checkY = Ao.me.y
					end
				end
				step = step + 1
			until step == route:size() --> end of path
			return true
		else --> cant teleport, just a normal call
			return move.walk(point.x, point.y, prevX, prevY) 
		end
	end
	return false --> default
end
move.target = function(unitType, unit) --> move to unit calls
	trace:log("move.target")
	if Ao.skills:find(SkillType.Charge) and Ao.me.mana > 10 then --> charge it why not
		trace:log("move.target charge")
		--Charge:on(unitType, unit.uid, (calc.dist(Ao.me.x, Ao.me.y, unit.x, unit.y)/calc.frw("charge")))
		--CastOn(unitType, unit.uid, (calc.dist(Ao.me.x, Ao.me.y, unit.x, unit.y)/calc.frw("charge")))
	else --> no charge
		if Ao.me.stamina > 30 then --> run
			trace:log("move.target run")
			Ao:RunToTarget(unitType, unit.uid)
			sleep(calc.dist(Ao.me.x, Ao.me.y, unit.x, unit.y)/calc.frw("run"))
		else --> walk
			trace:log("move.target walk")
			Ao:WalkToTarget(unitType, unit.uid)
			sleep(calc.dist(Ao.me.x, Ao.me.y, unit.x, unit.y)/calc.frw("walk"))
		end
	end
end
move.tele = function(x, y) --> teleport to spot
	trace:log("move.tele")
	local oldX, oldY = Ao.me.x, Ao.me.y --> initialize check variables
	local count = 0 --> loop counter
	trace:log("move.tele starting casting loop")
	repeat --> casting/counting loop
		if count == 0 or count % 10 == 0 then --> cast first try and every 200 ms after
			trace:log("move.tele casting")
			--Teleport:at(x, y)
			--CastAt(x, y, "Teleport", 0) --> commented due to non-existance
		end
		sleep(20) --> small sleep for fastest movement time
		count = count + 1
	until calc.dist(Ao.me.x, Ao.me.y, x, y) < 6 or oldX ~= Ao.me.x or oldY ~= Ao.me.y or count >= 30
	trace:log("move.tele done with casting loop")
	return calc.dist(Ao.me.x, Ao.me.y, x, y) < 6
end
move.walk = function(toX, toY, fromX, fromY) --> charge/run/walk to spot
	trace:log("move.walk called")
	if Ao.skills:find(SkillType.Charge) and Ao.me.mana > 10 then --> if we can charge do it
		debug:log("move.walk charging")
		--Charge:at(toX, toY)
		--sleep(calc.dist(fromX, fromY, toX, toY)/calc.frw("charge"))
		--CastAt(toX, toY, "Charge", (calc.dist(fromX, fromY, toX, toY)/calc.frw("charge"))) --> commented becuase this doesnt exist yet
	else --> no charging!
		if Ao.me.stamina > 30 then --> we have stamina, run!
			debug:log("move.walk running")
			Ao:RunToLocation(toX, toY) --> move packet
			sleep(calc.dist(fromX, fromY, toX, toY)/calc.frw("run")) --> time to get to destination
		else --> no stamina, walk
			debug:log("move.walk walking")
			Ao:WalkToLocation(toX, toY) --> move packet
			sleep(calc.dist(fromX, fromY, toX, toY)/calc.frw("walk")) --> time to get to destination
		end
	end
	return true
end
move.area = function(area) --> moving between areas
	--[[ Exceptions : 
		starting in map youre moving to!
		starting in DurielsLair -> tp to town
		starting in DuranceOfHateLevel3 (and town path is faster than map path) -> take meph portal
		starting in ArcaneSanctuary (and close to the "YetAnotherTome" I think) -> take portal to canyon
		starting in UberTristram -> move to PermanentTownPortal
		heading to DurielsLair -> path to Ao.maps:GetCorrectTomb()
		moving to Abaddon, PitOfAcheron, InfernalPit -> move to PermanentTownPortal
		moving to Tristram -> move to CairnStoneAlpha move to PermanentTownPortal (check if portal doesnt exist)
		moving to ArcaneSanctuary (and current map is PalaceCellarLevel3) -> move to ArcaneSanctuaryPortal
		moving to PalaceCellarLevel3 (and current map is ArcaneSanctuary (or we have that wp?)) -> move to ArcaneSanctuaryPortal
		moving to MooMooFarm -> move to RogueEncampment move to PermanentTownPortal
		moving to "a town map" -> tp to town
		moving to area we are already in -> dont move
		ending in DurielsLair -> move to HoradricOrifice (check DurielsLairPortal) move to DurielsLairPortal
		
		Do we really care about the Anya portal (I vote no!)
	--]]
	trace:log("move.area")
	area = getNumber(AreaLevel, area)
	debug:log("move.area to "..getString(AreaLevel, area))
	
	trace:log("move.area checking if we need to escape current area")
	local curArea = Ao.maps:getLevel() --> set current area so we dont call the core function 100 times
	if curArea == area then --> were already here!
		debug:log("move.area already here")
		return true
	elseif curArea == AreaLevel.DurielsLair then --> duriels lair
		debug:log("move.area in Duriels Lair, tping out")
		repeat
			move.town()
			sleep(100)
		until Ao.maps:inTown()
		curArea = Ao.maps:getLevel()
	elseif curArea == AreaLevel.UberTristam then --> only portal out (no TP allowed!!!)
		debug:log("move.area in Uber Trist, taking portal out")
		if move.object("PermanentTownPortal") then --> get to the portal
			if not warp.portal(AreaLevel.Harrogath) then --> cant take portal to harrogath
				return false
			end
		else --> didnt make it to portal
			return false
		end
	end
	
	trace:log("move.area checking best route")
	local map, town
	if area == AreaLevel.DurielsLair then --> need to move to the right tomb before duriels lair
		local tomb = Ao.maps:getCorrectTomb()
		map = path.area(tomb, curArea) --> to, from
		town = path.area(tomb, town.area())
	else --> standard pathing
		map = Ao.maps:getPath(area, curArea)
		town = Ao.maps:getPath(area, town.area())
	end
	if town:size() < map:size() then --> faster to go through town
		trace:log("move.area moving through town!")
		if curArea == AreaLevel.DuranceOfHateLevel3 then --> starting where we could use meph portal
			local bridge = locate.object("MephistoBridge")
			if bridge.x > 0 and bridge.y > 0 and calc.dist(Ao.me.x, Ao.me.y, bridge.x, bridge.y) < 40 then --> gotta be close otherwise act 3 
				if move.object("MephistoBridge") then --> made it to bridge
					if warp.portal("ThePandemoniumFortress") then --> made it in portal
						sleep(100)
						curArea = Ao.maps:getLevel()
						map = Ao.maps:getPath(area, curArea)
					end
				end
			end
		elseif curArea == AreaLevel.ArcaneSanctuary then --> which is closer wp or tome
			local tome = locate.object("YetAnotherTome")
			local wp = locate.object(Ao.maps:getWp())
			if wp.x ~= 0 and wp.y ~= 0 and tome.x ~= 0 and tome.y ~= 0 then --> found tome and wp
				if calc.dist(Ao.me.x, Ao.me.y, wp.x, wp.y) > calc.dist(Ao.me.x, Ao.me.y, tome.x, tome.y) then --> wp closer
					if move.object("YetAnotherTome") then
						local tome = find.object("YetAnotherTome")
						if tome.uid > 0 and tome.objectmode == 0 then
							local count = 0
							repeat
								if count == 0 or count % 10 == 0 then
									Ao:UnitInteract(UnitType.GameObject, tome.uid)
								end
								sleep(20)
								tome = find.object("YetAnotherTome")
								count = count + 1
							until tome.objectmode > 0 or count > 30
							local portal = find.object("PermanentTownPortal")
							count = 0
							while portal.uid == 0 and count < 31 do
								sleep(20)
								portal = find.object("PermanentTownPortal")
							end
						else
							if tome.objectmode > 0 then
								trace:log("move.area tome already open")
							else
								error:log("move.area couldnt find tome")
							end
						end
						if not warp.portal("CanyonOfTheMagi") then
							error:log("move.area failed to take canyon portal") --> no return the map loop should still get us where we want to be
						end
					end
				end
			end
		end
		debug:log("move.area town is faster")
		map = town
	end
	
	trace:log("move.area starting movement")
	for i = 0, map:size() - 1 do
		trace:log("move.area moving to map "..1+i.." of "..map:size())
		debug:log("move.area moving to "..getString(AreaLevel, map[i]))
		if map[i] == AreaLevel.Abaddon or map[i] == AreaLevel.PitOfAcheron or map[i] == AreaLevel.InfernalPit then --> portal areas
			debug:log("move.area moving to portal for "..getString(AreaLevel, map[i]))
			if not warp.portal(map[i]) then --> failed to take portal
				error:log("move.area failed to take portal")
				return false
			end
		elseif map[i] == AreaLevel.Tristram then --> need to move to cairn stones and then take the portal (portal doesnt show until weve made it to the stones)
			debug:log("move.area moving to cairn stones to reach tristram")
			if move.object("CairnStoneAlpha") then
				if not warp.portal(map[i]) then --> failed to take portal
					error:log("move.area failed to take portal")
					return false
				end
			else
				error:log("move.area failed to reach cairn stones")
				return false
			end
		elseif (map[i] == AreaLevel.ArcaneSanctuary and Ao.maps:getLevel() == AreaLevel.PalaceCellarLevel3) or (map[i] == AreaLevel.PalaceCellarLevel3 and Ao.maps:getLevel() == AreaLevel.ArcaneSanctuary) then --> portal between the two
			debug:log("move.area moving to arcane portal to get to "..getString(AreaLevel, map[i]))
			if not move.object("ArcaneSanctuaryPortal") then
				error:log("move.area failed to reach Arcane Portal")
				return false
			else --> made it to arcane portal
				local arcane = find.object("ArcaneSanctuaryPortal")
				if arcane.uid == 0 then --> no portal found
					error:log("move.area couldnt find arcane portal")
					return false
				end
				if arcane.objectmode == 0 then --> not active
					local count = 0
					repeat --> open portal
						if count == 0 or count % 10 == 0 then
							Ao:UnitInteract(UnitType.GameObject, arcane.uid)
						end
						sleep(20)
						arcane = find.object("ArcaneSanctuaryPortal")
						count = count + 1
					until arcane.objectmode > 0 or count > 30
				end
				if arcane.objectmode > 0 then --> active
					repeat --> enter portal
						if count == 0 or count % 10 == 0 then
							Ao:UnitInteract(UnitType.GameObject, arcane.uid)
						end
						sleep(20)
						count = count + 1
					until Ao.maps:getLevel() == map[i]
				else
					error:log("move.area Arcane Portal never opened")
					return false
				end
			end
		elseif map[i] == town.area() then
			debug:log("move.area moving to town")
			--TownPortal:at(Ao.me.x, Ao.me.y) --> add sleep checks until portal is found
			--CastAt(Ao.me.x, Ao.me.y, "TownPortal", 200)
			if not warp.portal(map[i]) then --> take our portal
				error:log("move.area couldnt take our tp")
				return false
			end
		elseif map[i] == Ao.maps:getLevel() then --> duhh already there
			debug:log("move.area already in that area")
		else
			local point = path.area(map[i]) --> path to next area
			if point.x == 0 and point.y == 0 then --> no next map, check for waypoint
				debug:log("move.area no points to path to map, check for waypoint")
				local wp = find.object(Ao.maps:getWp())
				if wp.x == 0 and wp.y == 0 then --> no waypoint, this isnt good
					error:log("move.area no map points or waypoint to take at "..getString(AreaLevel, Ao.maps:getLevel()).." to "..getString(AreaLevel, map[i]))
					return false
				end
				if not move.object(wp.objectid) then --> move to the wp
					error:log("move.area couldnt move to waypoint")
					return false
				end
				Ao:InitWaitingPacket(GS["OpenWaypoint"]) --> required to have a wait so that WPs can load
				local theWP = Ao.objects:FindWaypoint()
				local count = 0
				repeat --> interact until it opens and we receive our wp data
					if count == 0 or count % 10 == 0 then
						Ao:UnitInteract(UnitType.GameObject, theWP)
					end
					count = count + 1
				until Ao:WaitForInitPacket(20)
				if Ao.me.waypoints[wp.list[map[i]]] then --> if we have the wp, this is a nasty nested array
					local count = 0
					repeat --> interact until we change maps (multiple interaction shouldnt hurt as we are sending desired area, recurring interaction should end up the same as 1)
						if count == 0 or count % 10 == 0 then
							Ao:WaypointInteract(theWP, wp.dest(map[i]))
						end
						count = count + 1
					until Ao.maps:getLevel() == map[i] or count > 30
				else --> no wp, this is where we put in the wp hunter
					error:log("move.area we dont have the wp we want")
					return false
				end
				if Ao.maps:getLevel() ~= map[i] then
					error:log("move.area waypoint failed")
					return false
				end
			else
				--[[
				
						Code For Actual Moving To Next Area
				
				--]]
			end
		end
		Ao:RequestReassign(UnitType.Player, Ao.me.uid)
		sleep(20)
	end
end
move.explore = function() --> exploring an area (needs a sort function)
	trace:log("move.explore")
	local explore = Ao.maps:explore()
	
	trace:log("move.explore moving to points")
	for i = 0, explore:size() - 1 do --> move to all the explore points (this can be reduced by skipping some) should probably sort these in some fashion too
		local route = path.to(explore[i].x, explore[i].y)
		if route:size() == 0 then --> fail
			error:log("move.explore couldnt path")
			return false
		end
		
		trace:log("move.explore moving to point "..1+i)
		local step, walkTimer, checkX, checkY = 0, os.clock(), Ao.me.x, Ao.me.y --> some check variables
		local var = {} --> simplify walking new vs old locations
		repeat --> movement loop
			trace:log("move.explore step "..1+step.." of "..1+route:size())
			if step == 0 then --> set old location properly
				var.x = Ao.me.x
				var.y = Ao.me.y
			else
				var.x = route[step-1].x
				var.y = route[step-1].y
			end
			if not move.point(route[step], var.x, var.y) then --> move failed (teleport specifically)
				debug:log("move.explore repathing couldnt teleport")
				route = path.to(explore[i].x, explore[i].y) --> repath cuz suck
				step = -1
				if route:size() == 0 then 
					error:log("move.explore failed to repath to point "..1+i)
					return false
				end
			end
			if not Ao.skills:find(SkillType.Teleport) then --> we are a walker
				trace:log("move.explore checking walk timer")
				if os.clock() - walkTimer > .75 then --> time it takes to update location
					trace:log("move.explore walk timer tripped")
					if Ao.me.x == checkX and Ao.me.y == checkY then --> we havent moved
						debug:log("move.explore repathing couldnt walk")
						route = path.to(explore[i].x, explore[i].y)
						if route:size() == 0 then --> fail
							error:log("move.explore failed to repath to point "..1+i)
							return false
						end
						step = -1
						walkTimer = os.clock()
					else --> we have moved successfully
						debug:log("move.explore resetting walking stats")
						walkTimer = os.clock()
						checkX = Ao.me.x
						checkY = Ao.me.y
					end
				end
				if step == route:size() - 1 then --> check distance on last step
					trace:log("move.explore last step for walkers")
					repeat
						sleep(100)
					until os.clock() - walkTimer >= 1 or calc.dist(Ao.me.x, Ao.me.y, explore[i].x, explore[i].y) <= 6 --> 1 second or distance less than 6
					if calc.dist(Ao.me.x, Ao.me.y, explore[i].x, explore[i].y) > 6 then --> fail
						debug:log("move.explore last step failed to reach distance")
						route = path.to(explore[i].x, explore[i].y)
						if route:size() == 0 then
							error:log("move.explore failed to repath at end of loop")
							return false
						end
						step = -1
						walkTimer = os.clock()
						checkX = Ao.me.x
						checkY = Ao.me.y
					end
				end
			end
			step = step + 1
		until path == route:size()
	end
	return true
end

townMove = {}
townMove.npc = function(npc) --> move in town to an npc
	trace:log("townMove.npc")
	npc = getNumber(NPCCode, npc) --> get the numeric value of the NPCCode
	if not npc then --> check to make sure that the id was found correctly
		error:log("townMove.npc failed to find npc")
		return false
	end
	debug:log("townMove.npc "..stringValue(NPCCode, npc))
	local point = locate.npc(npc) --> get the location of the npc
	if not point then --> no location
		error:log("townMove.npc failed to find npc location")
		return false
	end
	trace:log("townMove.npc moving to point")
	if not townMove.point(point) then --> failed to move
		error:log("townMove.npc failed to move")
		return false
	end
	trace:log("townMove.npc moved to point, find npc for interaction")
	npc = Ao.npcs:findByNpcCode(npc) --> find the npc
	debug:log("townMove.npc npc.uid = "..npc.uid.." after findByNpcCode")
	if npc.uid == 0 then --> check to make sure we found npc
		error:log("townMove.npc couldnt find npc "..stringValue(NPCCode, npc.id))
		return false
	end
	trace:log("townMove.npc npc found, getting npc attention")
	Ao:GoToTownFolk(UnitType.NPC, npc.uid, npc.x, npc.y) --> gets npc attention and calls it towards us
	trace:log("townMove.npc final move to npc")
	townMove.to(UnitType.NPC, npc) --> pass the UnitType and the npc
	return true --> default we made it!
end
townMove.withNpc = function(npc, point) --> move in town with an npc to specific location (you will need to start next to npc)
	trace:log("townMove.withNpc")
	npc = getNumber(NPCCode, npc)
	if not npc then --> couldnt find the enumerated npc from given NPCCode
		error:log("townMove.withNpc couldnt find npc")
		return false
	end
	debug:log("townMove.withNpc "..stringValue(NPCCode, npc))
	local map = path.walk(point.x, point.y, 10, 10) --> generate a path that has steps of only 10 (cant move npc too far)
	
	local npcfrw = calc.npcfrw(npc) --> npcs are slow
	trace:log("townMove.withNpc refreshing npc")
	npc = Ao.npcs:findByNpcCode(npc) --> ensure we find our npc!
	if npc.uid == 0 then
		error:log("townMove.withNpc could not find npc to walk with")
		return false
	end
	local step, timer, oldx, oldy = 0, os.clock(), Ao.me.x, Ao.me.y --> default timer and location
	repeat --> path loop
		debug:log("townMove.withNpc stepping "..step.."/"..map:size())
		if step == 0 then --> first step starts at our location
			trace:log("townMove.withNpc taking step")
			Ao:WalkToLocation(map[step].x, map[step].y) --> Walks to given location
			trace:log("townMove.withNpc calling npc")
			Ao:GoToTownFolk(UnitType.NPC, npc.uid, map[step].x, map[step].y) --> call npc to location
			sleep(calc.dist(Ao.me.x, Ao.me.y, map[step].x, map[step].y)/npcfrw) --> sleep time it takes for npc to walk
		else --> not first step so we assume we made it to last step
			trace:log("townMove.withNpc taking step")
			Ao:WalkToLocation(map[step].x, map[step].y) --> Walks to given location
			trace:log("townMove.withNpc calling npc")
			Ao:GoToTownFolk(UnitType.NPC, npc.uid, map[step].x, map[step].y) --> call npc to location
			sleep(calc.dist(map[step-1].x, map[step-1].y, map[step].x, map[step].y)/npcfrw) --> sleep time it takes for npc to walk
		end
		if os.clock() - timer > 3.5 then --> if enough time has passed for x/y update lets check it
			trace:log("townMove.withNpc location change timer tripped")
			if Ao.me.x == oldx and Ao.me.y == oldy then --> we havent moved!
				debug:log("townMove.withNpc we havent moved")
				map = path.walk(point.x, point.y, 10, 10) --> generate a path with step size of 10
				if not map then --> no path generated
					error:log("townMove.withNpc no path was found")
					return false
				end
				step = -1 --> default to -1 because loop will increment it
				timer = os.clock() -->reset timer
			else
				debug:log("townMove.withNpc we have moved")
				timer = os.clock() --> reset timer
				oldx = Ao.me.x --> reset
				oldy = Ao.me.y --> reset
			end
		end
		step = step + 1 --> increment the step
		if step == map:size() - 1 then --> last step here, we do NOT want to call the npc out here though!!!
			Ao:WalkToLocation(map[step].x, map[step].y) --> walk to location
			sleep(calc.dist(map[step-1].x, map[step-1].y, map[step].x, map[step].y)/npcfrw) --> sleep the time it takes to walk
			step = map:size()  --> force end the loop!
		end
	until step == map:size()
end
townMove.object = function(obj, telekinesis) --> move in town to an object
	trace:log("townMove.object")
	obj = getNumber(GameObjectID, obj) --> get numeric value of the GameObjectID
	if not obj then --> check to make sure that the id was found correctly
		error:log("townMove.object failed to find object")
		return false
	end
	debug:log("townMove.object "..stringValue(GameObjectID, obj))
	local point = locate.object(obj) --> get the location of the object
	if not point then --> no location
		error:log("townMove.object failed to find object location")
		return false
	end
	trace:log("townMove.object moving to point")
	if not townMove.point(point) then --> failed to move
		error:log("townMove.object failed to move")
		return false
	end
	trace:log("townMove.object moved to point, lets find the object again")
	obj = find.object(obj) --> find the object
	debug:log("townMove.object obj.uid after find() = "..obj.uid)
	if obj.uid == 0 then --> check if we found obj
		error:log("townMove.object couldnt find object ")
		return false
	end
	if telekinesis then --> we just need to be close enough to use telekinesis
		if distance(Ao.me.x, Ao.me.y, obj.x, obj.y) <= 20 then
			return true --> we are close enough to kinesis
		end
	end 
	trace:log("townMove.object obj found, moving in")
	townMove.To(UnitType.GameObject, obj) --> pass the object type and the object itself
	return true --> default
end
townMove.point = function(point, telekinesis) --> move in town to a specific location
	trace:log("townMove.point "..point.x..", "..point.y)
	if not point or point.x < 0 or point.y < 0 then --> double check point is valid
		error:log("bad or no point in townMove.point")
		return false
	end
	local map = path.walk(point.x, point.y) --> generate a path to the point
	if not map then --> no path was generated
		error:log("townMove.point couldnt path to location")
		return false
	end
	local step, timer, oldx, oldy = 0, os.clock(), Ao.me.x, Ao.me.y --> default timer and location values
	repeat --> pathing loop
		debug:log("townMove.point stepping "..step.."/"..map:size())
		if step == 0 then --> first step check so we move from our location instead of last location
			trace:log("townMove.point  taking step")
			townMove.to(map[step].x, map[step].y, Ao.me.x, Ao.me.y) --> make the movement
		else --> not first step, so lets assume we made it to the last point
			trace:log("townMove.point taking step")
			townMove.to(map[step].x, map[step].y, map[step-1].x, map[step-1].y) --> make the movement
		end
		if os.clock() - timer > 3.5 then --> 3.5 is the time it takes to update location in town
			trace:log("townMove.point location change timer tripped")
			if Ao.me.x == oldx and Ao.me.y == oldy then --> we havent moved!!!
				debug:log("townMove.point we havent moved")
				map = path.walk(point.x, point.y) -->generate walking path
				if not map then --> no path generated
					error:log("townMove.point no path was found")
					return false
				end
				step = -1 --> set to -1 because end of loop will increment it back to 0
				timer = os.clock() --> reset timer
			else --> we have moved, lets update!
				debug:log("townMove.point we have moved")
				timer = os.clock() --> reset timer
				oldx = Ao.me.x --> reset
				oldy = Ao.me.y --> reset
			end
		end
		if step == map:size() - 1 then --> this is the last step in the path, and we have already moved check distance
			debug:log("townMove.point checking distance after last step")
			repeat --> end loop timer to check to make sure path has finalized (timer check and distance check)
				sleep(100)
			until os.clock() - timer >= 5 or calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) <= 6 --> 3.5 is the average so lets wait 5 or distance is less than 6
			if calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) > 6 then --> not close enough... repath!
				debug:log("townMove.point end point, distance too far "..calc.dist(Ao.me.x, Ao.me.y, point.x, point.y))
				map = path.walk(point.x, point.y) --> generate walking path
				if not map then --> no path generated
					return false
				end
				step = -1 --> set to -1 because end of loop will increment it back to 0
				timer = os.clock() --> reset
				oldx = Ao.me.x --> reset
				oldy = Ao.me.y --> reset
			end
		end
		if telekinesis then --> we just need to be close enough to use telekinesis
			if distance(Ao.me.x, Ao.me.y, point.x, point.y) <= 20 then
				return true --> we are close enough to kinesis
			end
		end 
		step = step + 1
	until step == map:size()
	return true
end
townMove.to = function(toX, toY, fromX, fromY) --> simple wrapper for moving
	trace:log("townMove.to called")
	if not fromX or not fromY then --> this will be a RunToTarget call!
		debug:log("townMove.to running to target")
		Ao:RunToTarget(toX, toY.uid) --> toX will be the UnitType, toY will be the unit  which will have a uid
		sleep(calc.dist(Ao.me.x, Ao.me.y, toY.x, toY.y)/calc.frw()) --> looks strange but toY is the object which will have an x and y value
	else --> to location
		debug:log("townMove.to running to location")
		Ao:RunToLocation(toX, toY) --> Run to the location, permanent run here because its in town
		sleep(calc.dist(fromX, fromY, toX, toY)/calc.frw()) --> sleep the distance divided by our speed (calc.frw() default is run speed)
	end
end

walk = {}
walk.explore = function()  --> explore the area (returns LOTS of points, we wont need them all)
	trace:log("walk.explore called")
	local explore = Ao.maps:explore()
	for i = 0, explore:size() - 1 do --> move to all the explore points, we can sort this after getting it if needed
		local map = path.walk(explore[i].x, explore[i].y)
		if not map then --> couldnt path
			error:log("walk.explore couldnt path")
			return false
		end
		local step = 0
		repeat --> moving loop
			walk.point(map[step]) --> move to next point (still allowing move to wrap the walk/teleport)
			step = step + 1 --> increment step
		until step = path:size() --> finished moving to new point
	end
	return true
end
walk.area = function(area) --> walk/run/charge to an area

end
walk.npc = function(npcid) --> walk/run/charge to an npc but only close enough to have it as an object to pass to kill
	trace:log("walk.npc called")
	local super = false --> not a super unique (yet)
	local id = -1 --> super unique id
	local npc = {} --> array for npc
	npc.uid = 0 --> default to this so we have a return that is always the same (in case we never find npc)
	
	local npcCode = getNumber(NPCCode, npcid) --> get the number value of npc (default)
	if getNumber(SuperUniqueNpc, npcid) then --> its a super unique (maybe check if getNumber(NPCCode) didnt work)
		npcCode = getNumber(SuperUniqueNpc, npcid) -->set the number value
		if not npcCode then --> no value found!
			error:log("walk.npc invalid NPCCode/SuperUniqueNpc")
			return npc --> return npc because outside functions will always check uid
		end
		super = true --> it is a super unique
		id = getNumber(SuperUnique, npcid) --> set the id number... super uniques are entirely seperate and weird
		debug:log("walk.npc moving to "..stringValue(SuperUniqueNpc, npcCode))
	else --> not a super
		debug:log("walk.npc moving to "..stringValue(NPCCode, npcCode))
	end
	npcid = npcCode --> set our npcid to the npc we found
	if super and id >= 0 then --> super unique
		npc = Ao.npcs:findBySuperUnique(id)
	else --> not a super
		npc = Ao.npcs:findByNpcCode(npcid)
	end
	if npc.uid > 0 then --> found the npc return it
		debug:log("walk.npc found the npc, and returning it")
		return npc --> return the npc so uid can be checked/confirmed
	end
	local map, path
	map = locate.npc(npcid)
	if not map then --> couldnt find the npc at all!
		error:log("walk.npc failed to find npc location")
	else
		if map.x > 0 and map.y > 0 then --> found the location
			path = path.walk(map.x, map.y)
			if not path then --> no generated path
				error:log("walk.npc no path generated")
				return npc --> ALWAYS
			end
		else --> no location
			error:log("walk.npc map location was invalid")
			return npc --> yup return the npc array
		end
	end
	local step = 0
	repeat --> move loop
		if not walk.point(path[step]) then --> couldnt move to where we wanted
			path = path.walk(map.x, map.y) --> repath we are stuck
			step = -1 --> it will increment at end of loop
			if not path then --> no generated path
				error:log("walk.npc no path generated")
				return npc
			end
		end
		if super and is >= 0 then --> super unique
			npc = Ao.npcs:findBySuperUnique(id)
		else --> everyone else
			npc = Ao.npcs:findByNpcCode(npcid)
		end
		if npc.uid > 0 then --> found npc
			debug:log("walk.npc found npc and passing it back")
			return npc --> always as always
		end
		step = step + 1
	until step == path:size() --> end!
	return npc --> default return because worst case uid is 0
end
walk.object = function(obj) --> walk/run/charge to an object
	trace:log("walk.object called")
	obj = getNumber(GameObjectID, obj) --> get number value of object
	if not obj then
		error:log("walk.object invalid GameObjectID")
		return false
	end
	debug:log("walk.object "..stringValue(GameObjectID, obj))
	local point = locate.object(obj) --> get the location of the object
	if not point then --> no location
		error:log("walk.object faild to find the object location")
		return false
	end
	trace:log("walk.object moving to object point")
	if not walk.point(point) then --> failed to move (move.point to ensure proper move type)
		error:log("walk.object failed to move")
		return false
	end
	trace:log("walk.object move to object point, find the object!")
	obj = Ao.objects:find(obj) --> find the object
	debug:log("walk.object obj.uid = "..obj.uid)
	if obj.uid == 0 then --> we couldnt find the object
		error:log("walk.object couldnt find the object")
		return false
	end
	trace:log("walk.object finalizing movement")
	walk.to(UnitType.GameObject, obj) --> pass the object type and the object 
	return true --> must be good! (lets hope!)
end
walk.point = function(point) --> walk/run/charge to a location
	trace:log("walk.point called")
	if calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) <= 25 and Ao.maps:CheckLineOfSight(Ao.me.x, Ao.me.y, point.x, point.y) then --> less than a screens walk away and the path is clear
		debug:log("walk.point no pathing, just moving")
		return walk.to(point.x, point.y)
	end
	local map = path.walk(point.x, point.y) --> generate a path
	if not map then --> no map to follow
		error:log("walk.point no path found")
		return false
	end
	trace:log("walk.point starting to move")
	local step, timer, oldx, oldy = 0, os.clock(), Ao.me.x, Ao.me.y --> initialize some variables
	repeat --> path loop
		debug:log("walk.point step "..step.." of "..map:size())
		if step == 0 then --> first step check so we move from our location
			trace:log("walk.point taking step")
			walk.to(map[step].x, map[step].y, Ao.me.x, Ao.me.y) --> make the move
		else --> not first step, assume we made it to last point
			trace:log("walk.point taking step")
			walk.to(map[step].x, map[step].y, map[step-1].x, map[step-1].y)
		end
		if os.clock() - timer > .75 then --> .75 is the time it takes to update location in town
			trace:log("walk.point location change timer tripped")
			if Ao.me.x == oldx and Ao.me.y == oldy then --> we havent moved!
				debug:log("walk.point we havent moved")
				map = path.walk(point.x, point.y) --> generate a path
				if not map then --> no path
					error:log("walk.point no path found")
					return false
				end
				step = -1 --> will be incremented to 0
				timer = os.clock()
			else --> we have moved, update
				debug:log("walk.point we have moved")
				timer = os.clock() --> reset
				oldx = Ao.me.x --> reset
				oldy = Ao.me.y --> reset
			end
		end
		if step == map:size() - 1 then --> last step check movement distance and such
			debug:log("walk.point checking distance after last step")
			repeat --> end loop timer to check to make sure path has finalized (timer and distance)
				sleep(100)
			until os.clock() - timer >= 1 or calc.dist(Ao.me.x, point.x, point.y) <= 6 --> 1 second or distance is close enough
			if calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) > 6 then --> not close enough repath
				debug:log("walk.point end point but not close enough "..calc.dist(Ao.me.x, Ao.me.y, point.x, point.y))
				map = path.walk(point.x, point.y) --> generate path
				if not map then --> no path
					return false
				end
				step = -1 --> will increment to 0
				timer = os.clock() --> reset
				oldx = Ao.me.x --> reset
				oldy = Ao.me.y --> reset
			end
		end
		step = step + 1 --> increment
	until step == map:size()
	return true
end
walk.to = function(toX, toY, fromX, fromY) --> calc for walking/run/charge here
	trace:log("walk.to called")
	if not fromX or not fromY then --> this will be a MoveToTarget call
		if Ao.skills:find(SkillType.Charge) and Ao.me.mana > 10 then --> if we can charge do it
			debug:log("walk.to charging")
			--CastOn(toX, toY.uid, "Charge", (calc.dist(Ao.me.x, Ao.me.y, toY.x, toY.y)/calc.frw("charge"))) --> commented because this doesnt exist yet
			--> toX will be UnitType, toY will be the unit (which will have a uid and an x, y)
		else --> no charging
			if Ao.me.stamina > 30 then --> run!
				debug:log("walk.to running")
				Ao:RunToTarget(toX, toY.uid) --> toX will be UnitType, toY will be unit (with a uid)
				sleep(calc.dist(Ao.me.x, Ao.me.y, toY.x, toY.y)/calc.frw("run")) --> looks odd but toY is the unit with x/y values
			else --> walk
				Ao:WalkToTarget(toX, toY.uid) --> toX will be UnitType, toY will be unit (with uid)
				sleep(calc.dist(Ao.me.x, Ao.me.y, toY.x, toY.y)/calc.frw("walk")) --> looks odd but toY is the unit with x/y values
			end
		end
	else --> to location
		if Ao.skills:find(SkillType.Charge) and Ao.me.mana > 10 then --> if we can charge do it
			debug:log("walk.to charging")
			--CastAt(toX, toY, "Charge", (calc.dist(fromX, fromY, toX, toY)/calc.frw("charge"))) --> commented becuase this doesnt exist yet
		else --> no charging!
			if Ao.me.stamina > 30 then --> we have stamina, run!
				debug:log("walk.to running")
				Ao:RunToLocation(toX, toY) --> move packet
				sleep(calc.dist(fromX, fromY, toX, toY)/calc.frw("run")) --> time to get to destination
			else --> no stamina, walk
				debug:log("walk.to walking")
				Ao:WalkToLocation(toX, toY) --> move packet
				sleep(calc.dist(fromX, fromY, toX, toY)/calc.frw("walk")) --> time to get to destination
			end
		end
	end
end

tele = {}
tele.explore = function() --> explore the area (returns LOTS of points, we wont need them all)
	trace:log("tele.explore called")
	local explore = Ao.maps:explore()
	for i = 0, explore:size() - 1 do --> move to all the explore points, this is where we can increase the increment to reduce the number of places we move to
		local map = path.teleport(explore[i].x, explore[i].y)
		if not map then --> couldnt path
			error:log("tele.explore couldnt path")
			return false
		end
		local step = 0
		repeat --> moving loop
			tele.point(map[step]) --> move to next point
			step = step + 1 --> increment step
		until step = path:size() --> finished moving to new point
	end
	return true
end
tele.area = function(area) --> teleports to area
	--[[ Exceptions : 
		starting in DurielsLair -> tp to town
		starting in DuranceOfHateLevel3 (and town path is faster than map path) -> take meph portal
		starting in ArcaneSanctuary (and close to the "YetAnotherTome" I think) -> take portal to canyon
		starting in UberTristram -> move to PermanentTownPortal
		heading to DurielsLair -> path to Ao.maps:GetCorrectTomb()
		moving to Abaddon, PitOfAcheron, InfernalPit -> move to PermanentTownPortal
		moving to Tristram -> move to CairnStoneAlpha move to PermanentTownPortal (check if portal doesnt exist)
		moving to ArcaneSanctuary (and current map is PalaceCellarLevel3) -> move to ArcaneSanctuaryPortal
		moving to PalaceCellarLevel3 (and current map is ArcaneSanctuary (or we have that wp?)) -> move to ArcaneSanctuaryPortal
		moving to MooMooFarm -> move to RogueEncampment move to PermanentTownPortal
		moving to "a town map" -> tp to town
		moving to area we are already in -> dont move
		ending in DurielsLair -> move to HoradricOrifice (check DurielsLairPortal) move to DurielsLairPortal
		
		Do we really care about the Anya portal (I vote no!)
	--]]
end
tele.npc = function(npcid) --> teleports close enough to an npc to see its stats (kill can take over form there)
	trace:log("tele.npc called")
	local super = false --> not a super unique (yet)
	local id = -1 --> super id if its a super
	local npc = {}  --> array constructor for the npc
	npc.uid = 0 --> default this to 0 so we can reference it
	
	local npcCode = getNumber(NPCCode, npcid) --> get the number value of npc (default)
	if getNumber(SuperUniqueNpc, npcid) then --> if it is a super unique monster
		npcCode = getNumber(SuperUniqueNpc, npcid) --> set the number value
		if not npcCode then --> no value found!
			error:log("tele.npc invalid NPCCode/SuperUniqueNpc")
			return npc --> returning npc because this will be called by the kill function which will check if the npc exists (remember we set the uid to 0)
		end
		super = true --> we found a super unique
		id = getNumber(SuperUnique, npcid) --> set the id of the super unique
		debug:log("tele.npc moving to "..stringValue(SuperUniqueNpc, npcCode))
	else --> not a super unqiue
		debug:log("tele.npc moving to "..stringValue(NPCCode, npcCode))
	end
	npcid = npcCode --> set our npcid value to the npc we found
	if super and id >= 0 then --> super unique
		npc = Ao.npcs:findBySuperUnique(id)
	else --> everyone else
		npc = Ao.npcs:findByNpcCode(npcid)
	end
	if npc.uid > 0 then --> found the npc
		debug:log("tele.npc found the npc returning it")
		return npc --> return the whole npc so kill function can do its thing (and we start at a distance)
	end
	local map, path
	map = locate.npc(npcid)
	if not map then --> couldnt find the npc at all!
		error:log("tele.npc failed to find npc location")
	else
		if map.x > 0 and map.y > 0 then --> found the location
			path = path.teleport(map.x, map.y)
			if not path then --> no generated path to point
				error:log("tele.npc no path generated")
				return npc --> always return the npc
			end
		else --> no location
			error:log("tele.npc map location was invalid")
			return npc --> always return the npc
		end
	end
	local step = 0
	repeat --> move loop
		if not tele.point(path[step]) then --> wasnt able to move (tele/walk/charge) to where we wanted!
			path = path.teleport(map.x, map.y) --> repath cuz of stuck
			step = -1 --> will be incremented
			if not path then --> no generated path
				error:log("tele.npc no path generated")
				return npc
			end
		end
		if super and id >= 0 then --> super unique
			npc = Ao.npcs:findBySuperUnique(id)
		else --> everyone else
			npc = Ao.npcs:findByNpcCode(npcid)
		end
		if npc.uid > 0 then --> found npc
			debug:log("tele.npc found npc returning it")
			return npc --> returning whole npc for kill function to handle
		end
		step = step + 1
	until step == path:size() --> end of path
	return npc --> default return npc so kill function can handle it properly
end
tele.object = function(obj) --> teleports to an object
	trace:log("tele.object called")
	obj = getNumber(GameObjectID, obj) --> get number value of object
	if not obj then
		error:log("tele.object invalid GameObjectID")
		return false
	end
	debug:log("tele.object "..stringValue(GameObjectID, obj))
	local point = locate.object(obj) --> get the location of the object
	if not point then --> no location
		error:log("tele.object failed to find object location")
		return false
	end
	trace:log("tele.object moving to object point")
	if not tele.point(point) then --> failed to move (calling move.point to ensure proper movement type)
		error:log("tele.object failed to move")
		return false
	end
	trace:log("tele.object moved to object point, find the object!")
	obj = Ao.objects:find(obj) --> find the object
	debug:log("tele.object obj.uid = "..obj.uid)
	if obj.uid == 0 then --> no object
		error:log("tele.object couldnt find object")
		return false 
	end
	return true --> everything worked!
end
tele.point = function(point) --> teleports to a location
	trace:log("tele.point called")
	if calc.dist(Ao.me.x, Ao.me.y, point.x, point.y) <= 32 then --> if we are within a screens distance why path it?
		debug:log("tele.point no pathing, just moving")
		return tele.to(point.x, point.y) --> have to return 
	end
	local map = path.teleport(point.x, point.y) --> generate a path to position
	if not map then --> make sure path exists
		error:log("tele.point no path found")
		return false
	end
	trace:log("tele.point starting movement")
	local step = 0
	repeat --> path loop
		debug:log("tele.point step "..step.." of "..map:size())
		if not tele.to(map[step].x, map[step].y) then
			map = path.teleport(point.x, point.y) --> generate a new path
			step = -1 --> default to -1 because it will be incremented to 0 (beginning of the new path)
			if not map then --> make sure path is good
				error:log("tele.point no path found")
				return false
			end
		end
		step = step + 1
	until step == map:size()
	return true
end
tele.to = function(toX, toY) --> actual calculations (walk vs tele here)
	trace:log("tele.to called")
	if Ao.me.mana < 20 then --> low mana call
		debug:log("tele.to low mana, using walking")
		local p = {}
		p.x = toX
		p.y = toY
		return walk.point(p)
	end
	local oldX, oldY = Ao.me.x, Ao.me.y --> initialize to check changes
	local count = 0 -->
	trace:log("tele.to starting cast/count loop")
	repeat --> casting/counting loop
		if count == 0 or count % 5 == 0 then --> cast first round and every 100 ms after!
			debug:log("tele.to casting")
			--CastAt(toX, toY, "Teleport", 0) --> commented due to this function doesnt exist yet
		end
		sleep(20) --> small sleep so we can move ASAP
		count = count + 1 --> increment count so this isnt infinite
	until calc.dist(Ao.me.x, Ao.me.y, toX, toY) < 6 or oldX ~= Ao.me.x or oldY ~= Ao.me.y or count >= 30--> if distance is good, x or y has moved or 600 ms has passed
	debug:log("tele.to done with loop")
	return success
end

path = {}
path.walk = function(x, y, maxD, minD) --> generate a walking path
	trace:log("path.walk")
	if not maxD then --> default max path distance
		trace:log("path.walk defaulting max step size")
		maxD = 30
	end
	if not minD then --> default min path distance
		trace:log("path.walk defaulting min step size")
		minD = 8
	end
	local path = Ao.maps:pathTo(x, y, false, math.ceil(math.min(maxD, math.max(minD, calc.dist(Ao.me.x, Ao.me.y, x, y)/4))))
	--> crazy calculations above to keep map path generation somewhat random (but based off actual dist) to reduce getting stuck
	if path:size() == 0 then --> check size of path
		error:log("path.walk no path!")
		return path
	end
	return path
end
path.teleport = function(x, y, range) --> generates a teleporting path
	trace:log("path.teleport")
	if not range then --> default max path distance
		trace:log("path.teleport defaulting range")
		range = 32
	end
	local path = Ao.maps:pathTo(x, y, true, range) --> no need to randomize teleport pathing
	if path:size() == 0 then --> confirm path size
		error:log("path.teleport no path!")
		return path
	end
	return path
end
path.to = function(x, y) --> generates proper (teleport/walk) path to location
	trace:log("path.to")
	if Ao.skills:find(SkillType.Teleport) then --> we have teleport
		debug:log("path.to found teleport, generating a teleport path")
		route = path.teleport(map.x, map.y)
	else --> no teleport
		debug:log("path.to no teleport, using a walk path")
		route = path.walk(map.x, map.y)
	end
	if route:size() == 0 then --> no real path generated
		error:log("path.to no valid path")
		return route
	end
end
path.area = function(area, from) --> generates a map of areas to path
	trace:log("path.area")
	local map
	if from then
		map = Ao.maps:getPath(from, area)
		if map:size() == 0 then
			error:log("path.area no maps to pass from")
		end
	else
		map = Ao.maps:pathToLevel(area)
		if map.x == 0 and map.y == 0 then
			error:log("path.area no point found to area")
		end
	end
	return map
end

warp = {}
warp.portal = function(dest) --> takes a portal warp to area
	trace:log("warp.portal")
	dest = getNumber(AreaLevel, dest)
	if not dest then
		error:log("warp.portal invalid area")
		return false
	end
	
	local portal = find.portal(dest)
	if portal.uid == 0 then
		error:log("warp.portal couldnt find portal")
		return false
	end
	
	local inTown = Ao.maps:inTown() --> if we are in town we can use telekinesis to take the portal
	
	if inTown then
		if not move.object(portal.id, telekinesis) then
			error:log("warp.portal failed to move to portal")
			return false
		end
	else
		if not move.object(portal.id) then
			error:log("warp.portal failed to move to portal")
			return false
		end
	end
	repeat
		if inTown and Ao.skills:find(SkillType.Telekinesis) then --> if we are in town and have telekinesis use it!
			--CastOn("GameObject", portal.uid, "Telekinesis", 200) --> function doenst exist yet
		else --> normal interaction
			Ao:UnitInteract(UnitType.GameObject, portal.uid)
			sleep(200)
		end
	until Ao.maps:getLevel() == dest
	return true
end