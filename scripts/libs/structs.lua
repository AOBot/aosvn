--[[  Structures for units ]]--
--[[
	Here we will build base structures designed around the original structure of differing units : 
	How to implement this properly is going to be a huge pain in the ass
--]]
npc = {
	-- core structure
	uid
	id
	mode
	x
	y
	life
	lifePercent
	time
	count
	revived
	uniqueid
	res
	flags
	mods
	states
	redeemed
	distance
	isImmune(ResistType)
	isHero()
	hasMod(MonsterMod)
	hasFlags(flags)
	getDistance(x, y)
	getResist(ResistType)
	
	-- additional lua structure
	unitType
	update()
	interact()
}
object = {
	-- core structure
	objectId
	interactType
	objectMode
	ownerUid
	uid
	x
	y
	destination
	canChangeBack
	getDistance(x, y)
	
	--additional lua structure
	interact()
	update()
}
pet = {
	uid
	petType
}
player = {
	-- core structure
	playerName
	accountName
	charClass
	level
	partyID
	uid
	x
	y
	hostiled
	close
	life
	relationship
	area
	getDistance(x, y)
	
	--additional lua structure
	update()
}
skill = {
	-- core structure
	skill
	baseLevel
	
	--additional lua structure
	level
}
warp = {
	-- core structure
	unitType
	uid
	id
	x
	y
	
	--additional lua structure
	take()
}
point = {
	--core structure
	x
	y
	
	--additional lua structure
	
}