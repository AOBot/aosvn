--[[ skills and casting ]]--
--[[
	we will handle all skills and casting in this section of the scripts.
	this will also include when a skill (monster or player) will affect something in game (area, character, etc)
--]]
state = {}
state.getStatValue = function(state, stat) --> gets the value of stat from the active state
	trace:log("state.getStatValue")
	stat = getNumber(StatType, stat) --> get the numeric value of the stat
	if not stat then --> check to make sure that the stat was valid
		error:log("state.getStatValue bad StatType")
		return 0  --> we are returning a number, its gonna be 0
	end
	state = getNumber(StateType, state) --> get the numeric value of the state
	if not state then
		error:log("state.getStatValue bad StateType")
		return 0 --> return 0 for failure
	end
	if not Ao.me.states:isActive(state) then --> check to make sure this state is active!
		debug:log("state.getStatValue "..stringValue(StateType, state).." is not active")
		return 0 --> return 0 for failure
	else
		state = Ao.me.states:getStats(state) --> get the Stats of the active StateType
	end
	local val = 0 --> default to 0
	trace:log("state.getStatValue check state size")
	if state:size() > 0 then --> double check state has active parts!
		trace:log("state.getStatValue start state loop")
		for i = 0, state:size() - 1 do --> state loop
			trace:log("state.getStatValue state "..i.." of "..state:size())
			if state[i].Stat.Type == stat then --> test the StatType vs given StatType
				debug:log("state.getStatValue found stat")
				val = val + state[i].Value --> increment val by the amound of the mod
			end
		end
	end
	return val --> done!
end

skill = {}
skill.getLevel = function(skill) --> will finish this function later (so much to account for based off skill itself!)
	trace:log("skill.getLevel")
	skill = getNumber(SkillType, skill) --> grab the numberic value of skill
	if not skill then --> check valid skill
		error:log("skill.getLevel bad SkillType")
		return 0 --> return 0 as we couldnt find that skill
	end
	if not Ao.skills:find(skill) then
		error:log("skill.getLevel we dont have that skill")
		return 0 --> return 0 as we dont have that skill!
	end
	
end