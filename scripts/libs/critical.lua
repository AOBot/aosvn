--[[ critical functions for AO ]]--
--[[
	critical functions will include things like calculations used across AO
	for example, distance formula, enumeration functions, etc
]]--

function dist(x1, y1, x2, y2) --> distance calculations
	trace:log("dist called")
	if x1 and y1 and x2 and y2 then --> make sure all values exist
		trace:log("all variables exist in dist function")
		if x1 > 0 and y1 > 0 and x2 > 0 and y2 > 0 then --> make sure all values are good
			debug:log("all variables above 0 in dist function")
			local dist = math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)) --> calculate!
			if dist > 0 then --> make sure we have some sort of distance (and its not negative... boost library doesnt like it)
				return dist --> return the distance found
			else --> default to a distance of 0
				return 0 --> return 0 (dont divide anything by a distance!)
			end
		else
			error:log("invalid coords in dist function")
			return 0 --> return 0 (dont divide anything by a distance!)
		end
	else
		error:log("nil coordinates in dist function")
		return 0 --> dont divide by 0!!!
	end
	return 0 --> return 0 dont divide anything by a distance!
end

function sleep(ms) --> new thread to wait in milliseconds before another action
	trace:log("sleep called")
	if ms and ms > 0 then --> make sure ms exists and is not negative!
		trace:log("sleeping "..ms)
		Ao:Sleep(ms) --> core function for sleep
	else --> ms is bad, sleep a default 100
		debug:log("invalid time in sleep "..ms)
		Ao:Sleep(100) --> default sleep
	end
end