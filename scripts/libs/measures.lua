--[[ critical functions for AO ]]--
--[[
	critical functions will include things like calculations used across AO
	for example, distance formula, enumeration functions, etc
]]--
calc = {}
calc.dist = function(x1, y1, x2, y2) --> distance calculations
	trace:log("dist called")
	if x1 and y1 and x2 and y2 then --> make sure all values exist
		trace:log("all variables exist in dist function")
		if x1 > 0 and y1 > 0 and x2 > 0 and y2 > 0 then --> make sure all values are good
			debug:log("all variables above 0 in dist function")
			local dist = math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)) --> calculate!
			if dist > 0 then --> make sure we have some sort of distance (and its not negative... boost library doesnt like it)
				return dist --> return the distance found
			else --> default to a distance of 0
				return 0 --> return 0 (dont divide anything by a distance!)
			end
		else
			error:log("invalid coords in dist function")
			return 0 --> return 0 (dont divide anything by a distance!)
		end
	else
		error:log("nil coordinates in dist function")
		return 0 --> dont divide by 0!!!
	end
	return 0 --> return 0 dont divide anything by a distance!
end

calc.frw = function(swagger) --> return the sleep time of given movement
	if not swagger then --> type of movement (walk, run, charge) default to running
		swagger = "run" --> would have used the MovementType enum, but there isnt one for charge
	end
	if Ao.skills:find(SkillType.Vigor) then --> set vigor if we have it
		--selectSkill("Vigor") --> commented due to skills not being in yet
	end
	if Ao.skills:find(SkillType.BurstOfSpeed) and not Ao.me.states:isActive(StateType.Quickness) then --> if we have Burst Of Speed and its not active, cast it
		--castAt(Ao.me.x, Ao.me.y, "BurstOfSpeed", 200) --> commented due to skills not being in yet
	end
	local skillFRW = 0 --> FRW from skills
	local itemFRW = 0 --> FRW from items
	local armor = 0 --> armor penalty
	local baseWalk = 4 --> base speed for walking
	local baseRun = 6 --> base speed for running
	local speed = 0 --> effective speed
	skillFRW = skillFRW + state.getStatValue(StateType.Stamina, StatType.VelocityPercent) --> Vigor aura
	skillFRW = skillFRW + state.getStatValue(StateType.IncreasedSpeed, StatType.VelocityPercent) --> barbarian increased speed
	skillFRW = skillFRW + state.getStatValue(StateType.Quickness, StatType.VelocityPercent) --> Burst of Speed
	--> still should add holy freeze, decrepify, cold, and slow : http://diablo2.diablowiki.net/Faster_Run_Walk
	itemFRW = itemFRW + inv.getStatValue(StatType.FasterMoveVelocity) --> inventory items
	itemFRW = itemFRW + body.getStatValue(StatType.FasterMoveVelocity) --> equipment
	
	speed = baseWalk * (1 + skillFRW / 100 + math.floor(itemFRW*150/(itemFRW+150))/100 - 10/100) --> speed calculation for walking
	speed = math.max(speed, baseWalk/4) --> can never go slower than baseWalk/4
	if swagger == "walk" then --> if we wanted walking were ready, return it
		return speed * 500 --> apply sleep multiplier here since this is all we use it for
	end
	speed = speed + 2 --> running speed is 2 higher than walking speed
	speed = math.max(speed, baseWalk/4) --> can never go slower than baseWalk/4
	if swagger == "run" then --> if we wanted running were ready, return it
		return speed * 500 --> apply sleep multiplier here since this is all we use it for
	end
	speed = speed * (1 + skillFRW / 100 - 10/100) * 1.5 --> charge speed calculation
	if swagger == "charge" then --> if we wanted charge speed were ready to go
		return speed * 500 --> apply sleep multiplier here since this is all we use it for
	end
end

calc.npcfrw = function(npc) --> speed of npc walking
	if npc == NPCCode.Gheed or npc == NPCCode.Charsi or npc == NPCCode.Elzix then --> these ones move faster
		npcfrw = 1500
	elseif npc == NPCCode.Akara or npc == NPCCode.Malah or npc == NPCCode.Asheara then --> these ones are even SLOWER
		npcfrw = 500
	end
	return 1000 --> default
end

check = {}
check.superNpc = function(npcID) --> determine if given npc is super unique or not and return appropriately
	trace:log("check.superNpc")
	local superUnique = false --> for distinguishing super uniques
	local superID = -1 --> -1 is not a super unique
	local npcCode = getNumber(NPCCode, npcID) --> gets the numeric value of the NPCCode enumerator
	if getNumber(SuperUniqueNpc, npcID) then --> this is a super unique
		debug:log("check.superNpc is in SuperUniqueNpc list")
		npcCode = getNumber(SuperUniqueNpc, npcID) --> set the npc code to the super unique value
		if not npcCode then --> double check that super unique worked
			error:log("check.superNpc invalid NPCCode and\\or SuperUniqueNpc")
			return (-1, false, -1) --> default negation values
		end
		superUnique = true --> yes its a superUnique
		superID = getNumber(SuperUnique, npcID) --> set the specific super unique id value
		debug:log("check.superNpc is super unique "..getString(SuperUniqueNpc, npcCode))
	else --> super unique list returned false
		error:log("check.superNpc is npc "..getString(NPCCode, npcCode))
	end
	return (npcCode, superUnique, superID)
end

find = {}
find.npc = function(npcID, npcSuper, npcSuperID) --> find npc
	trace:log("find.npc")
	local npc = {}
	npc.uid = 0
	if npcSuper and npcSuperID then --> just if the super unique part exists
		debug:log("find.npc looking for a super unique")
		npc = Ao.npcs:findBySuperUnique(npcSuperID)
	else --> not a super unique
		debug:log("find.npc looking for a regular npc")
		npc = Ao.npcs:findByNpcCode(npcID)
	end
	if npc.uid > 0 then --> we found the npc return the full thing
		debug:log("find.npc found the npc")
		return npc
	else --> did not find return the values that we originally had
		debug:log("find.npc did not find the npc")
		npc.id = npcID
		if npcSuper and npcSuperID then --> if these values exist set them in the npc table
			trace:log("find.npc setting super unique values")
			npc.superunique = npcSuper
			npc.uniqueid = npcSuperID
		end
		return npc
	end
end
find.object = function(objID) --> find object
	trace:log("find.object")
	local object = {}
	object.uid = 0
	objID = getNumber(GameObjectID, objID)
	if not objID then
		error:log("find.object invalid GameObjectID")
		return object
	end
	
	object = Ao.objects:find(objID)
	if object.uid > 0 then
		debug:log("find.object found the object")
	else
		debug:log("find.object failed to find object")
		object.uid = 0 --> just to be sure
	end
	return object
end
find.portal = function(dest)
	trace:log("find.portal")
	local portal = {}
	portal.uid = 0
	dest = getNumber(AreaLevel, dest)
	if not dest then
		error:log("find.portal bad AreaLevel")
		return portal
	end
	
	trace:log("find.portal check for town portals")
	portal = Ao.objects:FindTownPortalByDestination(dest)
	if portal.uid == 0 then
		debug:log("find.portal not a town portal")
		portal = Ao.objects:FindPermanentPortalByDestination(dest)
		if portal.uid == 0 then
			error:log("find.portal couldnt find the portal")
			return portal
		end
	end
	return portal
end

function sleep(ms) --> new thread to wait in milliseconds before another action
	trace:log("sleep called")
	if ms and ms > 0 then --> make sure ms exists and is not negative!
		trace:log("sleeping")
		Ao:Sleep(ms) --> core function for sleep
	else --> ms is bad, sleep a default 100
		debug:log("invalid time in sleep "..ms)
		Ao:Sleep(100) --> default sleep
	end
end

function getString(enumType, enumValue) --> enumerator casting
	trace:log("stringValue called")
	if type(enumValue) ~= "string" then --> check if value is already a string
		local values = {}
		for k, v in pairs(enumType) do
			values[v] = k
		end
		trace:log("returning string value")
		return values[enumValue]
	else
		debug:log("already a string!")
		return enumValue
	end
	error:log("stringValue bad value entered")
	return false
end

function getNumber(enumType, enumValue) --> enumerator casting
	trace:log("numberValue called")
	if type(enumValue) == "string" then --> check if value is already a number
		for v, k in pairs(enumType) do
			if v == enumValue then
				return tonumber(k)
			end
		end
		error:log("numberValue bad value entered")
		return false
	else
		debug:log("already a number")
		return enumValue
	end
	error:log("numberValue bad value entered")
	return false
end

locate = {}
locate.object = function(unit) --> get the location of an object in the area
	trace:log("locate.object")
	local point = {}
	unit = getNumber(GameObjectID, unit)
	if not unit then
		error:log("locate.object couldnt find the object number")
		point.x = 0
		point.y = 0
		return point
	end
	point = Ao.objects:find(unit) --> try to find the object itself
	if point.x == 0 and point.y == 0 then --> object wasnt found
		point = Ao.maps:pathToObject(unit) --> use map to find
		if point.x == 0 and point.y == 0 then --> object on map not found
			error:log("locate.object unable to find a point for object")
			return point
		end
	end
	return point
end
locate.npc = function(unit) --> get the location of an npc in the area
	trace:log("locate.npc")
	local point = {}
	unit = getNumber(NPCCode, unit)
	if not unit then
		error:log("locate.npc couldnt find the npccode")
		point.x = 0
		point.y = 0
		return point
	end
	point = Ao.npcs:findByNpcCode(unit) --> try to find the npc to use its coords as a point
	if point.x == 0 and point.y == 0 then --> npc not found (via packets)
		debug:log("locate.npc point = "..point.x..", "..point.y)
		point = Ao.maps:pathToNpc(npc) --> use map to find
		if point.x == 0 and point.y == 0 then --> map was NOT able to generate a point for us
			error:log("locate.npc unable to find a point for npc")
			return point
		end
	end
	return point
end

town = {}
town.area = function(player) --> returns town of player (or your town if called without player)

end