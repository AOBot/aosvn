--[[ Interactions : npcs, objects, shrines, etc ]]--
--[[
	This file will handle every possible type of interaction that the bot will be encountering.
	This will range from talking to in-town npcs, opening chests, using portals, etc.
	We will want "high level" functions here as well, that will call movement and interaction in one,
	while also retaining just basic interactions to keep things simple and extensible.  The reason
	for the high level functions is so that we can use telekinesis properly.
--]]

interact = {}
