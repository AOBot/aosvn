--[[ containers are the different places items can be held ]]--
--[[
	containers have been done pretty nice so far.  The only thing I would like to make sure we focus on here is to keep it simple.
	polite made a good point once that we dont have a generic (put in cube) function, we should keep things like that in mind
	as we go along.  There are several areas were we can improve upon this!
--]]

inv = {}
inv.getStatValue = function(stat) --> return the total value of given StatType from all charms in inventory (non-charms dont help anyway)
	trace:log("inv.getStatValue")
	stat = getNumber(StatType, stat) --> get the numeric value of the stat
	if not stat then --> check to make sure that the stat was valid
		error:log("inv.getStatValue bad StatType")
		return 0 --> since were returning a number, we will make it 0
	end
	trace:log("inv.getStatValue get items")
	local items = Ao.me.inventory:get() --> list of all items in inventory
	local val = 0 --> default to 0
	trace:log("inv.getStatValue start item loop")
	for i = 0, items:size() - 1 do --> item loop
		trace:log("inv.getStatValue item "..i.." of "..items:size())
		if items[i].baseItem.code == "cm1" or items[i].baseItem.code == "cm2" or items[i].baseItem.code == "cm1" then --> only charms count!
		trace:log("inv.getStatValue found a charm")
			for j = 0, items[i].mods:size() - 1 do --> mods loop of item
				trace:log("inv.getStatValue mod "..j.." of "..items[i].mods:size())
				local _mods = items[i].mods[j] --> set the current mod to a variable so we can test it
				if _mods.Stat.Type == stat then --> test the stat type vs given stat type
					debug:log("inv.getStatValue found mod, apply it")
					val = val + _mods.Value --> increment val by the amount of the mod
				end
			end
		end
	end
	return val --> done!
end

body = {}
body.getStatValue = function(stat) --> return the total value of given StatType from all equipment (switch weaponset doesnt count)
	trace:log("body.getStatValue")
	stat = getNumber(StatType, stat) --> get the numeric value of the stat
	if not stat then
		error:log("body.getStatValue bad StatType")
		return 0 --> since were returning a number, lets do 0
	end
	trace:log("body.getStatValue get items")
	local items = Ao.me.body:getBody() --> list all the items equipped
	local val = 0 --> default to 0
	trace:log("body.getStatValue start item loop")
	for i = 0, items:size() - 1 do --> item loop
		trace:log("body.getStatValue item "..i.." of "..items:size())
		if items[i].location ~= EquipmentLocation.RightHandSwitch and items[i].location ~= EquipmentLocation.LeftHandSwitch then --> no switch sets!
			trace:log("body.getStatValue found a non-switch item")
			for j = 0, items[i].mods:size() - 1 do --> mods loop of item
				trace:log("body.getStatValue mod "..j.." of "..items[i].mods:size())
				local _mods = items[i].mods[j] --> set the current mod to a variavle so we can test it
				if _mods.Stat.Type == stat then --> test the stat vs given StatType
					debug:log("body.getStatValue found mod, apply it")
					val = val + _mods.Value --> increment val by the amount of the mod
				end
			end
		end
	end
	return val --> done!
end