--[[ Logging system ]]--
--[[
	AO logging has 4 levels.  When the user sets his log level, all logs of THAT level and above will be shown.
	This is important when thinking about what type of logging to use.
	1->Trace = Step-by-step actions of AO.  When creating a new file a trace log should be at almost every statement you create.  This includes loops, if/else, start and end of functions.
	2->Debug = Data known and calculated with AO.  These should override trace logging for when performing calculations and dealing with variables.
	3->Info = The first level of logging displayed to the user.  Should be general information of actions performed by AO.
	4->Error = Pretty straight forward here, any erroneous data or actions that AO has obtained gets logged here.
	
	Items are also passed through the log (at a level of 5) but are an exception to normal logging.
]]--
PREFIX = {}
SUFFIX = {}
RUNESC = {}
Ao:Chat("logging.lua opened")
--[[ logging class ]]--
logging = {}
function logging:new(loggingLevel, loggingFile, sendToMgr) --> logging constructor
	local newLogging = { --> data relay
		loggingLevel = loggingLevel,
		loggingFile = loggingFile,
		sendToMgr = sendToMgr,
	}
	setmetatable(newLogging, self) --> assign meta table to self to create references
	self.__index = self --> same with index
	return newLogging --> return the new array/reference
end
function logging:exists()
	--Ao:Chat(self.loggingFile)
	local check = io.open(_LogDir..self.loggingFile..".log")
	if check then
		check:close()
		return true
	else
		return false
	end
end
function logging:log(msg) --> generic log structure
	if logLevel <= self.loggingLevel then --> check to make sure its a "perceivable" log
		if self.sendToMgr then --> relay information to manager to display
			Ao:SendController(string.format("%s", msg), 101)
		end
		if logDisplay then --> check if we should be displaying it 
			Ao:Chat(string.format("[�c"..self.loggingLevel.."%s�c0] �c0%s", self.loggingFile, msg))
		end
	end
	
	local file
	repeat --> safety check for opening the log file
		if not self:exists() then
			os.execute("copy leechMessages.txt ".._LogDir..self.loggingFile..".log")
			file = assert(io.open(_LogDir..self.loggingFile..".log", "w+"))
			if file then
				file:close()
			end
		end
		file = assert(io.open(_LogDir..self.loggingFile..".log", "a+")) --> open with adding/editing (a+)
		Ao:Sleep(16)
	until file --> returns a value when successfully opened
	file:write(os.date().." -> "..msg.."\n") --> write the time and the message
	file:close()
end

--[[ default logging constructors ]]--
trace = logging:new(1, "trace", false)
debug = logging:new(2, "debug", false)
info = logging:new(3, "info", true)
error = logging:new(4, "error", true)
sold = logging:new(5, "sold", false)
dumped = logging:new(5, "dumped", false)
gambled = logging:new(5, "gambled", false)
shopped = logging:new(5, "shopped", true)
stashed = logging:new(5, "stashed", true)
skipped = logging:new(5, "skipped", true)
--stats = logging:new(2, "stats", false)

