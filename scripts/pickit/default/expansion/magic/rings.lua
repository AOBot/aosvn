-- expansion\magic\rings.lua "strict"

-- Rings For Craft Settings
local minItemLvl = 91 --note: forCrafting
local keepCraftUnid = (ItemLevel < minItemLvl)
local forCrafting = ItemLevel >= minItemLvl


local isGoodMagicRing =
function(item)
	local goodMod = false
	
	goodMod =
		( AllResist >= 15
				and (MagicFind >= 25 or FasterCastRate ~= 0)
			--or AllResist >= 15
			or MagicFind >= 40
			--or MinDamage >= 13
				--and (ToHit >= 101 or AllResist >= 14)
		)
	prefix = 
		(
			MaxMana >= 120
			or AllResist >= 15
			or ToHit >= 120
		)
	suffix = 
		(
			Strength >= 20
			or Dexterity >= 20
			or MaxLife >= 40
		)
	return
		( Unidentified
			or MagicFind >= 40
		    --or forCrafting
			--or goodMod
		)
end
magic.rings =
{ checkStats = true,
	["rin"] = -- Ring
		{ priority = 0, identify = true,
			isGoodItem = isGoodMagicRing
		},
}