-- expansion\magic\belts.lua "strict"

local magicCraftBelt =
{ priority = 3, identify = false,
   goodItem = ItemLevel >= 91
}

magic.belts =
{ checkStats = true,
	--["tbl"] = magicCraftBelt, -- Heavy Belt / Hit Power
	--["ztb"] = magicCraftBelt, -- Battle Belt / Hit Power
	["utc"] = magicCraftBelt, -- Troll Belt / Hit Power
	--["mlb"] = magicCraftBelt, -- Belt / Blood
	--["zmb"] = magicCraftBelt, -- Mesh Belt / Blood
	["umc"] = magicCraftBelt, -- Mithril Coil / Blood
	--["vbl"] = magicCraftBelt, -- Light Belt / Caster
	--["zvb"] = magicCraftBelt, -- Sharkskin Belt / Caster
	["uvc"] = magicCraftBelt, -- Vampirefang Belt / Caster
	--["lbl"] = magicCraftBelt, -- Sash / Safety
	--["zlb"] = magicCraftBelt, -- Demonhide Sash / Safety
	["ulc"] = magicCraftBelt, -- Spiderweb Sash / Safety
}
