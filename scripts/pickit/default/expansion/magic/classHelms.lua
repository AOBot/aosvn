-- expansion\magic\classHelms.lua "strict"

local anySuffix = SuffixIndex ~= 0

-- Minimum Skill Level Setting
local minSkillLevel = 5

local isGoodMagicBarbHelm =
function(item)
	local barbSkiller = false

	barbSkiller = 
		(ClassSkillsBonus + WarcriesTab + BattleOrdersSkill)
		--or (ClassSkillsBonus + WarcriesTab + WarCrySkill)
		--or (ClassSkillsBonus + WarcriesTab + BattleCommandSkill)
		--or (ClassSkillsBonus + WarcriesTab + ShoutSkill)
		--or (ClassSkillsBonus + WarcriesTab + HowlSkill)
		--or (ClassSkillsBonus + WarcriesTab + TauntSkill)
		--or (ClassSkillsBonus + WarcriesTab + FindPotionSkill)
		--or (ClassSkillsBonus + WarcriesTab + FindItemSkill)
		--or (ClassSkillsBonus + WarcriesTab + BattleCrySkill)
		--or (ClassSkillsBonus + WarcriesTab + GrimWardSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + WhirlwindSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + BerserkSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + FrenzySkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + BashSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + DoubleSwingSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + LeapSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + DoubleThrowSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + StunSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + LeapAttackSkill)
		--or (ClassSkillsBonus + BarbarianCombatTab + ConcentrateSkill)
		--or (ClassSkillsBonus + MasteriesTab + IronSkinSkill)
		--or (ClassSkillsBonus + MasteriesTab + NaturalResistanceSkill)
		--or (ClassSkillsBonus + MasteriesTab + SwordMasterySkill)
		--or (ClassSkillsBonus + MasteriesTab + AxeMasterySkill)
		--or (ClassSkillsBonus + MasteriesTab + MaceMasterySkill)
		--or (ClassSkillsBonus + MasteriesTab + PoleArmMasterySkill)
		--or (ClassSkillsBonus + MasteriesTab + ThrowingMasterySkill)
		--or (ClassSkillsBonus + MasteriesTab + SpearMasterySkill)
		--or (ClassSkillsBonus + MasteriesTab + IncreasedStaminaSkill)
		--or (ClassSkillsBonus + MasteriesTab + IncreasedSpeedSkill)

	return
		( Unidentified
			or BattleOrdersSkill >= 3
				and (ClassSkillsBonus >= 2 or WarcriesTab >= 3)
				and (MaxLife >= 1 or FasterHitRecovery >= 1 or LifePerLevel ~= 0)
					or (Ethereal and (ReplenishDurability ~= 0 or Indestructible))
			--or barbSkiller >= minSkillLevel
				--and anySuffix
          	--or SkillTabBonus >= 3
          	--or ClassSkillsBonus >= 2
		)
end	

local isGoodMagicDruidHelm =
function(item)
	local druidSkiller = false

	druidSkiller =
		(ClassSkillsBonus + ElementalTab + TornadoSkill)
		--or (ClassSkillsBonus + ElementalTab + HurricaneSkill)
		--or (ClassSkillsBonus + ElementalTab + ArmageddonSkill)
		--or (ClassSkillsBonus + ElementalTab + VolcanoSkill)
		--or (ClassSkillsBonus + ElementalTab + FirestormSkill)
		--or (ClassSkillsBonus + ElementalTab + MoltenBoulderSkill)
		--or (ClassSkillsBonus + ElementalTab + ArcticBlastSkill)
		--or (ClassSkillsBonus + ElementalTab + FissureSkill)
		--or (ClassSkillsBonus + ElementalTab + CycloneArmorSkill)
		--or (ClassSkillsBonus + ElementalTab + TwisterSkill)
		--or (ClassSkillsBonus + DruidSummoningTab + OakSageSkill)
		--or (ClassSkillsBonus + DruidSummoningTab + HeartOfWolverineSkill)
		--or (ClassSkillsBonus + DruidSummoningTab + SummonGrizzlySkill)
		--or (ClassSkillsBonus + DruidSummoningTab + RavenSkill)
		--or (ClassSkillsBonus + DruidSummoningTab + PoisonCreeperSkill)
		--or (ClassSkillsBonus + DruidSummoningTab + SummonSpiritWolfSkill)
		--or (ClassSkillsBonus + DruidSummoningTab + CycleOfLifeSkill) -- Carrion Vine
		--or (ClassSkillsBonus + DruidSummoningTab + SummonDireWolfSkill)
		--or (ClassSkillsBonus + DruidSummoningTab + VinesSkill) -- Solar Creeper
		--or (ClassSkillsBonus + DruidSummoningTab + SpiritOfBarbsSkill)
		--or (ClassSkillsBonus + ShapeShiftingTab + FireClawsSkill) 
		--or (ClassSkillsBonus + ShapeShiftingTab + WerewolfSkill)
		--or (ClassSkillsBonus + ShapeShiftingTab + WearbearSkill)
		--or (ClassSkillsBonus + ShapeShiftingTab + FurySkill)
		--or (ClassSkillsBonus + ShapeShiftingTab + ShapeShiftingSkill) -- Lycanthropy
		--or (ClassSkillsBonus + ShapeShiftingTab + FeralRageSkill)
		--or (ClassSkillsBonus + ShapeShiftingTab + MaulSkill)
		--or (ClassSkillsBonus + ShapeShiftingTab + RabiesSkill)
		--or (ClassSkillsBonus + ShapeShiftingTab + ShockWaveSkill)
		--or (ClassSkillsBonus + ShapeShiftingTab + HungerSkill)
		
	return
		( Unidentified
			or TornadoSkill >= 3
				and (ClassSkillsBonus >= 2 or ElementalTab >= 3)
				and (MaxLife >= 1 or FasterHitRecovery >= 1 or LifePerLevel ~= 0)
					or (Ethereal and (ReplenishDurability ~= 0 or Indestructible))
			--or druidSkiller >= minSkillLevel
				--and anySuffix
          	--or SkillTabBonus >= 3
          	--or ClassSkillsBonus >= 2
		)
end	

magic.classHelms =
{ checkStats = true,
---[[ Barbarian Helms
	["PrimalHelm"] = 
		{ priority = 3, identify = true,
   			isGoodItem = isGoodMagicBarbHelm
		},
--]]

---[[ Druid Pelts
	["Pelt"] = 
		{ priority = 3, identify = true,
   			isGoodItem = isGoodMagicDruidHelm
		},
--]]
}