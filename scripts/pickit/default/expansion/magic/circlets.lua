-- expansion\magic\circlets.lua "strict"

local isGoodMagicCirclet =
function(item)
-- Good Skill Amount Settings
	local minSkill = 2 -- Class Skills
	local minTab = 3 -- Skill Tab
------------------------------------------------------------------------------
	local prefix, casterSkill, meleeSkill, suffix = false, false, false, false
	local fcr, frw = false, false

	prefix =
		( ClassSkillsBonus >= minSkill
			or SkillTabBonus >= minTab
			or Sockets >= 3
		)
		
	casterSkill =
		( PaladinSkills >= minSkill
			or SorceressSkills >= minSkill
			or DruidSkills >= minSkill
			or AssassinSkills >= minSkill
			or NecromancerSkills >= minSkill

			or PaladinCombatTab >= minTab
	      	or LightningTab >= minTab
			--or OffensiveAurasTab >= minTab
	        --or DefensiveAurasTab >= minTab
	        or FireTab >= minTab
	        or ColdTab >= minTab
	        --or CursesTab >= minTab
	        or PoisonAndBoneTab >= minTab
	        or NecroSummoningTab >= minTab
	        or WarcriesTab >= minTab
	        or ElementalTab >= minTab
		)
			
	meleeSkill =
	    ( BarbarianSkills >= minSkill
	        or AmazonSkills >= minSkill
			or PaladinSkills >= minSkill
			or DruidSkills >= minSkill
			or AssassinSkills >= minSkill
			
			or PaladinCombatTab >= minTab
	        --or BowAndCrossBowTab >= minTab
	        or PassiveAndMagicTab >= minTab
	        or JavelinAndSpearTab >= minTab
	        or BarbarianCombatTab >= minTab
	        --or MasteriesTab >= minTab
	        or ShapeShiftingTab >= minTab
	        or ShadowDisciplinesTab >= minTab
	        or MartialArtsTab >= minTab
		)
		
	suffix =
		( MaxLife >= 100
			or Strength >= 30
			or Dexterity >= 30
			--or Energy >= 28
			--or MagicFind >= 33
			--or DamageReduction >= 23
			--or LifePerLevel ~= 0
			--or ManaPerLevel ~= 0
			--or PoisonLengthReduction >= 75
			--or MaxDamage >= 12
			--or MinDamage >= 13
		)

	fcr = FasterCastRate >= 20
	
	frw = FasterMoveVelocity >= 30	
	
	return
		( Unidentified
			or (casterSkill or Sockets >= 3) 
				and (fcr or suffix)
			or (meleeSkill or Sockets >= 3) 
				and (frw or suffix)
			--or prefix 
				--and (suffix or fcr or frw)
		)
end

magic.circlets =
{ checkStats = true,
	["Circlet"] = -- Circlets
		{ priority = 3, identify = true,
			isGoodItem = isGoodMagicCirclet
		}
}