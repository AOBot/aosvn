-- expansion\magic\classShields.lua "strict"

local anySuffix = SuffixIndex ~= 0

local isGoodMagicPalShield =
function(item)
	local allRes, sockets, anySuffix = false, false, false

	allRes = AllResist >= 40
	sockets = Sockets == 4

	return
		( Unidentified
			or AllResist >= 45
				and Sockets >= 4
				and FasterBlockRate >= 30
			--or allRes
				--and sockets
				--and anySuffix
          	--or SkillTabBonus >= 3
				--and allRes
          	--or ClassSkillsBonus >= 2
				--and allRes
		)
end

local isGoodMagicNecShield =
function(item)
	local necroSkiller = false
	
	necroSkiller = 
		(ClassSkillsBonus + PoisonAndBoneTab + BoneSpearSkill)
		or (ClassSkillsBonus + PoisonAndBoneTab + BoneSpiritSkill)
		or (ClassSkillsBonus + PoisonAndBoneTab + PoisonNovaSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + TeethSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BoneArmorSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + PoisonDaggerSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + CorpseExplosionSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BoneWallSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + PoisonExplosionSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BonePrisonSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + RaiseSkeletonSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + SkeletonMasterySkill)
		--or (ClassSkillsBonus + NecroSummoningTab + ClayGolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + GolemMasterySkill)
		--or (ClassSkillsBonus + NecroSummoningTab + RaiseSkeletalMageSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + BloodgolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + SummonResistSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + IronGolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + FiregolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + ReviveSkill)
		--or (ClassSkillsBonus + CursesTab + LowerResistSkill)
		--or (ClassSkillsBonus + CursesTab + DecrepifySkill)
		--or (ClassSkillsBonus + CursesTab + AmplifyDamageSkill)
		--or (ClassSkillsBonus + CursesTab + DimVisionSkill)
		--or (ClassSkillsBonus + CursesTab + WeakenSkill)
		--or (ClassSkillsBonus + CursesTab + IronMaidenSkill)
		--or (ClassSkillsBonus + CursesTab + TerrorSkill)
		--or (ClassSkillsBonus + CursesTab + ConfuseSkill)
		--or (ClassSkillsBonus + CursesTab + LifeTapSkill)
		--or (ClassSkillsBonus + CursesTab + AttractSkill)

	return
		( Unidentified
			or BoneSpearSkill >= 3
				and (ClassSkillsBonus >= 2 or PoisonAndBoneTab >= 3)
				and FasterBlockRate >= 30
			--or necroSkiller >= 5
				--and anySuffix		
			--or SkillTabBonus >= 3
			--or ClassSkillsBonus >= 2
		)
end

magic.classShields =
{ checkStats = true,
---[[ Paladin Shields
	["AuricShields"] = 
		{ priority = 3, identify = true,
			isGoodItem = isGoodMagicPalShield
		},
--]]

---[[ Necromancer Shields
	["VoodooHeads"] = 
		{ priority = 3, identify = true,
			isGoodItem = isGoodMagicNecShield
		},
--]]
}