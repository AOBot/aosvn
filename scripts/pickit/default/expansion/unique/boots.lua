-- expansion\unique\boots.lua "strict"

unique.boots =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- ** Normal Boots ** --
-----------------------------------------------------
--[[ Hotspur (Boots)
	["lbt"] =
		{
			priority = 2, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or DefensePercent >= 20 --note: 10-20 ed
					)
				)
		},
--]]
-----------------------------------------------------
--[[ Gorefoot (Heavy Boots)
	["vbt"] =
		{
			priority = 2, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or DefensePercent >= 30 --note: 20-30 ed
					)
				)
		},
--]]
-----------------------------------------------------
--[[ Treads of Cthon (Chain Boots)
	["mbt"] =
		{
			priority = 2, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or DefensePercent >= 40 --note: 30-40 ed
					)
				)
		},
	
--]]
-----------------------------------------------------
--[[ Goblin Toe (Light Plated Boots)
	["tbt"] =
		{
			priority = 2, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or DefensePercent >= 60 --note: 50-60 ed
					)
				)
		},
	
--]]
-----------------------------------------------------
--[[ Tearhaunch (Greaves)
	["hbt"] =
		{
			priority = 2, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or DefensePercent >= 80 --note: 60-80 ed
					)
				)
		},
	
--]]
-----------------------------------------------------
-- ** Exceptional Boots ** --
-----------------------------------------------------
--[[ Infernostride (Demonhide Boots)
	["xlb"] =
		{
			priority = 3, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or DefensePercent >= 140 --note: 120-150 ed
						    and GoldFind >= 70 --note: 40-70 extra gold
					)
				)
		},
	
--]]
-----------------------------------------------------
---[[ Waterwalk (Sharkskin Boots)
	["xvb"] =
		{
			priority = 5, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or MaxLife >= 65 --note: 45-65 life
							and DefensePercent >= 210 --note: 180-210 ed
					)
				)
		},
--]]
-----------------------------------------------------
--[[ Silkweave (Mesh Boots)
	["xmb"] =
		{
			priority = 4, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or DefensePercent >= 185 --note: 150-190 ed
					)
				)
		},
	
--]]
-----------------------------------------------------
---[[ War Traveler (Battle Boots)
	["xtb"] =
		{
			priority = 7, identify = false,
			goodItem =
				( not Ethereal and
					( Unidentified
						or MagicFind >= 50 --note: 30-50 mf
						    --and DefensePercent >= 150 --note: 150-190 ed
						    --and AttackerTakesDamage >= 5 --note: 5-10 attacker takes
					)
				)
		},
--]]
-----------------------------------------------------
---[[ Gore Rider (War Boots)
	["xhb"] =
		{
			priority = 6, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or DefensePercent >= 200 --note: 160-200 ed
					)
				)
		},
--]]
-----------------------------------------------------
-- ** Elite Boots ** --
-----------------------------------------------------
---[[ Sandstorm Trek (Scarabshell Boots) (Repairs 1 Durability In 20 Seconds)
	["uvb"] =
		{
			priority = 7, identify = (not Ethereal),
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 140 --note: 140-170 ed
					    and Strength >= 15 --note: 10-15 str
					    and Vitality >= 15 --note: 10-15 vit
					    --and PoisonResist >= 40 --note: 40-70 poison res
					--or not Ethereal
						--and DefensePercent >= 170 --note: 140-170 ed
					    --and Strength >= 15 --note: 10-15 str
					    --and Vitality >= 15 --note: 10-15 vit
					    --and PoisonResist >= 40 --note: 40-70 poison res
				)
		},
--]]
-----------------------------------------------------
--[[ Marrowwalk (Boneweave Boots)
	["umb"] =
		{
			priority = 7, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
						or SkeletonMasterySkill >= 2 --note: 1-2 SkeleMastery
							and Strength >= 18 --note: 10-20 str
							--and DefensePercent >= 170 --note: 170-200 ed
					)
				)
		},
--]]
-----------------------------------------------------
---[[ Shadow Dancer (Myrmidon Greaves)
	["uhb"] =
		{
			priority = 8, identify = true,
			goodItem =
				( not Ethereal and
					( Unidentified
					    or DefensePercent >= 70 --note: 70-100 ed
							and ShadowDisciplinesTab >= 2 --note: 1-2 ShadowDisc
							and Dexterity >= 25 --note: 15-25 dex
					)
				)
		}
--]]
-----------------------------------------------------
}
