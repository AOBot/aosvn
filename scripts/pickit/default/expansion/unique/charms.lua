-- expansion\unique\charms.lua "strict"

unique.charms =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
---[[ Annihilus (Small Charm)
	["cm1"] =
		{ priority = 9,
			isGoodItem =
				function(item)
					return (item.action ~= ItemActionType.DropToGround)
				end
		},
--]]
-----------------------------------------------------
---[[ Hellfire Torch (Large Charm)
	["cm2"] =
		{ priority = 9,
			isGoodItem =
				function(item)
					return (item.action ~= ItemActionType.DropToGround)
				end
		},
--]]
-----------------------------------------------------
---[[ Gheed's Fortune (Grand Charm)
	["cm3"] =
		{
			priority = 7, identify = true,
			isGoodItem =
				function(item)
					return
						( item.action ~= ItemActionType.DropToGround and
							( Unidentified
								or MagicFind >= 40 --note: 20-40 mf
									and GoldFind >= 160 --note: 80-160 extra gold
									and ReducedPrices >= 15 --note: 10-15 priceReduc
							)
						)
				end
		}
--]]
-----------------------------------------------------
}