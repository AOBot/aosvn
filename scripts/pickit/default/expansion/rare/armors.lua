-- expansion\rare\armors.lua "strict"

local goodRareArmor =
{ priority = 2, identify = true,
	goodItem =
		( Unidentified
			or not Ethereal
				and Sockets >= 2
				and (MaxLife >= 41 or FasterHitRecovery >= 24)
				--and (DefensePercent >= 101 or ArmorPerLevel ~= 0)
			or Ethereal
				and Sockets >= 2
				and (MaxLife >= 41 or FasterHitRecovery >= 24)
				--and (DefensePercent >= 101 or ArmorPerLevel ~= 0)
		)
}

rare.armors =
{ checkStats = true,
---[[ Normal Armors
	["qui"] = goodRareArmor, -- Quilted Armor
	["lea"] = goodRareArmor, -- Leather Armor
	["hla"] = goodRareArmor, -- Hard Leather Armor
	["stu"] = goodRareArmor, -- Studded Leather
	["rng"] = goodRareArmor, -- Ring Mail
	["scl"] = goodRareArmor, -- Scale Mail
	["brs"] = goodRareArmor, -- Breast Plate
	["chn"] = goodRareArmor, -- Chain Mail
	["spl"] = goodRareArmor, -- Splint Mail
	["ltp"] = goodRareArmor, -- Light Plate
	["plt"] = goodRareArmor, -- Plate Mail
	["fld"] = goodRareArmor, -- Field Plate
	["gth"] = goodRareArmor, -- Gothic Plate
	["ful"] = goodRareArmor, -- Full Plate Mail
	["aar"] = goodRareArmor, -- Ancient Armor
--]]

---[[ Exceptional Armors
	["xui"] = goodRareArmor, -- Ghost Armor
	["xea"] = goodRareArmor, -- Serpentskin Armor
	["xla"] = goodRareArmor, -- Demonhide Armor
	["xtu"] = goodRareArmor, -- Trellised Armor
	["xcl"] = goodRareArmor, -- Tigulated Mail
	["xng"] = goodRareArmor, -- Linked Mail
	["xrs"] = goodRareArmor, -- Cuirass
	["xhn"] = goodRareArmor, -- Mesh Armor
	["xpl"] = goodRareArmor, -- Russet Armor
	["xtp"] = goodRareArmor, -- Mage Plate
	["xlt"] = goodRareArmor, -- Templar Coat
	["xld"] = goodRareArmor, -- Sharktooth Armor
	["xth"] = goodRareArmor, -- Embossed Plate
	["xul"] = goodRareArmor, -- Chaos Armor
	["xar"] = goodRareArmor, -- Ornate Plate
--]]
	
---[[ Elite Armors	
	["uui"] = goodRareArmor, -- Dusk Shroud
	["uea"] = goodRareArmor, -- Wyrmhide
	["ula"] = goodRareArmor, -- Scarab Husk
	["utu"] = goodRareArmor, -- Wire Fleece
	["ucl"] = goodRareArmor, -- Loricated Mail
	["ung"] = goodRareArmor, -- Diamond Mail
	["urs"] = goodRareArmor, -- Great Hauberk
	["uhn"] = goodRareArmor, -- Boneweave
	["upl"] = goodRareArmor, -- Balrog Skin
	["utp"] = goodRareArmor, -- Archon Plate
	["ult"] = goodRareArmor, -- Hellforge Plate
	["uld"] = goodRareArmor, -- Kraken Shell
	["uth"] = goodRareArmor, -- Lacquered Plate
	["uul"] = goodRareArmor, -- Shadow Plate
	["uar"] = goodRareArmor, -- Sacred Armor
--]]
}