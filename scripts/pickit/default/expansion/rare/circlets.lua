-- expansion\rare\circlets.lua "strict"

local isGoodRareCirclet =
function(item)
-- Good Skill Amount Settings
	local minSkill = 2 -- Class Skills
	local minTab = 2 -- Skill Tab
-----------------------------------------------------------------
	local casterSkill, meleeSkill, addStats = false, false, false
	local fcr, frw = false, false
	local goodMods, statMods, meleeMods = 0, 0, 0

	skiller = (
		PaladinSkills >= 2
		or SorceressSkill >= 2
		or Druidskills >= 2
		or AssassinSkills >= 2
		or NecromancerSkills >= 2
		or AmazonSkills >= 2
		or BarbarianSkills >= 2
		or LightningTab >= 2
		or FireTab >= 2
		or ColdTab >= 2
		or PoisonAndBoneTab >= 2
		or TrapsTab >= 2
		or ElementalTab >= 2
		or JavelinAndSpearTab >= 2
	)
	prefix = count{
		MaxMana >= 30,
		AllResist >= 10,
		Sockets >= 2,
	}
	suffix = count{
		Strength >= 15,
		Dexterity >= 10,
		MaxLife >= 20,
		LifeRegen >= 4,
		FasterCastRate >= 20,
		FasterMoveVelocity >= 30,
	}
	casterSkill =
		( PaladinSkills >= minSkill 
		or SorceressSkills >= minSkill 
		or DruidSkills >= minSkill
		or AssassinSkills >= minSkill
		or NecromancerSkills >= minSkill
		or AmazonSkills >= minSkill
		or BarbarianSkills >= minSkill

		--or PaladinCombatTab >= minTab
		or LightningTab >= minTab
		--or OffensiveAurasTab >= minTab
		--or DefensiveAurasTab >= minTab
		or FireTab >= minTab
		or ColdTab >= minTab
		--or CursesTab >= minTab
		or PoisonAndBoneTab >= minTab
		--or NecroSummoningTab >= minTab
		or WarcriesTab >= minTab
		or ElementalTab >= minTab
		--or DruidSummoningTab >= minTab
		or TrapsTab >= minTab
		--or BarbarianCombatTab >= minTab
		--or MasteriesTab >= minTab
		--or JavelinAndSpearTab >= minTab
		)

	meleeSkill =
		( BarbarianSkills >= minSkill
		or AmazonSkills >= minSkill
		or PaladinSkills >= minSkill
		or DruidSkills >= minSkill
		or AssassinSkills >= minSkill

		or PaladinCombatTab >= minTab
		or BowAndCrossBowTab >= minTab
		--or PassiveAndMagicTab >= minTab
		or JavelinAndSpearTab >= minTab
		or BarbarianCombatTab >= minTab
		--or MasteriesTab >= minTab
		or ShapeShiftingTab >= minTab
		or ShadowDisciplinesTab >= minTab
		or MartialArtsTab >= minTab
		)
			
	fcr = FasterCastRate >= 20
	
	frw = FasterMoveVelocity >= 30
			 
	addStats = (MaxLife + MaxMana + Strength + Dexterity + Energy)
				
	goodMods =
		count{
			Sockets == 2,
			TripleResist >= 55,
			DoubleResist >= 65,
			AllResist >= 16,
		}
		
	statMods =
		count{
			MaxLife >= 40,
			MaxMana >= 61,
			MaxLife >= 21 and MaxMana >= 21,
			Strength >= 21,
			Dexterity >= 16 and Energy >= 16,
		}

	meleeMods =
		count{
			ToHitPerLevel ~= 0,
			ToHit >= 101,
			MaxDamagePercent >= 28,
			--LifeDrainMinDamage >= 7 and ManaDrainMinDamage >= 7,
			(LifeDrainMinDamage + ManaDrainMinDamage) >= 13,
			FasterHitRecovery >= 10,
			MaxDamage >= 7 and MinDamage >= 6,
		}
	
	return
		( Unidentified
			or skiller and (prefix + suffix) >= 3
			--or fcr and casterSkill 
				--and (goodMods + statMods) >= 2
		--	or frw and meleeSkill 
				--and (goodMods + meleeMods + statMods) >= 2
			--or (casterSkill or meleeSkill)
				--and addStats >= 53
			--or SkillTabBonus >= 2
			--or ClassSkillsBonus >= 2
		)
end

rare.circlets =
{ checkStats = true,
	["Circlet"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareCirclet
		}
}
--[[
prefix
ar 101-120
edam 21-30
mana 61-90
sres 31-40
ares 16-20
cskill 2
tskill 2
soc 2

suffix
str 21-30
dex 16-20
eng 16-20
life 41-60
ll 6-9
ml 6-9
fcr 20
rw 30
]]