-- expansion\rare\jewels.lua "strict"

local isGoodRareJewel =
function(item)
	local prefixes, suffixes = 0, 0
	local damMods = false
		
	prefixes =
		count{
			MaxDamagePercent >= 25,
			AllResist >= 10,
			SingleResist >= 30,
			ArmorClass >= 40,
			ToHit >= 60,
			DamageToMana == 12,
			TotalResist >= 55,
			--DemonDamagePercent >= 40,
		}
	suffixes =
		count{
			FasterHitRecovery ~= 0,
			LowerRequirementsPercent ~= 0,
			Strength >= 9,
			Dexterity >= 9,
			Energy >= 9,
			--LightMaxDamage >= 97,
		}
	damMods =
		( MaxDamagePercent >= 30
			or (MaxDamage + MinDamage) >= 16
			or MaxDamagePercent >= 25
				and (MaxDamage >= 8 or MinDamage >= 9)
		)
	return
		( Unidentified
			or (prefixes + suffixes) >= 2
			or damMods
			--or prefixes >= 2
			--or suffixes >= 2
		)
end

rare.jewels =
{ checkStats = true,
	["jew"] =
		{ priority = 5, identify = true, 
			isGoodItem = isGoodRareJewel
		}
}