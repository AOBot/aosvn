-- expansion\rare\helms.lua "strict"

local goodRareHelm =
{ priority = 4, identify = true,
	goodItem =
		( Unidentified
			or not Ethereal
				and ToHitPerLevel ~= 0
				and Sockets == 2
				and MaxLife >= 31
			or Ethereal
				and ToHitPerLevel ~= 0
				and Sockets == 2
				and MaxLife >= 31
		)
}

rare.helms =
{ checkStats = true,
---[[ Normal Helms
	["cap"] = goodRareHelm, -- Cap
	["skp"] = goodRareHelm, -- Skull Cap
	["hlm"] = goodRareHelm, -- Helm
	["fhl"] = goodRareHelm, -- Full Helm
	["ghm"] = goodRareHelm, -- Great Helm
	["msk"] = goodRareHelm, -- Mask
	["crn"] = goodRareHelm, -- Crown
	["bhm"] = goodRareHelm, -- Bone Helm
--]]

---[[ Exceptional Helms
	["xap"] = goodRareHelm, -- War Hat
	["xkp"] = goodRareHelm, -- Sallet
	["xlm"] = goodRareHelm, -- Casque
	["xhl"] = goodRareHelm, -- Basinet
	["xhm"] = goodRareHelm, -- Winged Helm
	["xsk"] = goodRareHelm, -- Death Mask
	["xrn"] = goodRareHelm, -- Grand Crown
	["xh9"] = goodRareHelm, -- Grim Helm
--]]

---[[ Elite Helms
	["uap"] = goodRareHelm, -- Shako
	["ukp"] = goodRareHelm, -- Hydraskull
	["ulm"] = goodRareHelm, -- Armet
	["uhl"] = goodRareHelm, -- Giant Conch
	["uhm"] = goodRareHelm, -- Spired Helm
	["usk"] = goodRareHelm, -- Demonhead
	["urn"] = goodRareHelm, -- Corona
	["uh9"] = goodRareHelm, -- Bone Visage
--]]
}