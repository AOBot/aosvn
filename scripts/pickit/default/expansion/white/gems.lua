-- expansion\white\gems.lua

white.gems =
{ --pickToSell = true,
    --pickLimit = 1
---[[ Perfect Gems
	["gpv"] = {}, -- Perfect Amethyst
	["gpw"] = {}, -- Perfect Diamond
	["gpg"] = {}, -- Perfect Emerald
	["gpr"] = {}, -- Perfect Ruby
	["gpb"] = {}, -- Perfect Sapphire
	["skz"] = {}, -- Perfect Skull
	["gpy"] = {}, -- Perfect Topaz
--]]

---[[ Flawless Gems
	["gzv"] = {pickLimit = 3}, -- Flawless Amethyst
	["glw"] = {pickLimit = 3}, -- Flawless Diamond
	["glg"] = {pickLimit = 3}, -- Flawless Emerald
	["glr"] = {pickLimit = 3}, -- Flawless Ruby
	["glb"] = {pickLimit = 3}, -- Flawless Sapphire
	["skl"] = {pickLimit = 3}, -- Flawless Skull
	["gly"] = {pickLimit = 3}, -- Flawless Topaz
--]]

--[[ Normal Gems
	["gsv"] = {}, -- Amethyst
	["gsw"] = {}, -- Diamond
	["gsg"] = {}, -- Emerald
	["gsr"] = {}, -- Ruby
	["gsb"] = {}, -- Sapphire
	["sku"] = {}, -- Skull
	["gsy"] = {}, -- Topaz
--]]

--[[ Flawed Gems
	["gfv"] = {}, -- Flawed Amethyst
	["gfw"] = {}, -- Flawed Diamond
	["gfg"] = {}, -- Flawed Emerald
	["gfr"] = {}, -- Flawed Ruby
	["gfb"] = {}, -- Flawed Sapphire
	["skf"] = {}, -- Flawed Skull
	["gfy"] = {}, -- Flawed Topaz
--]]


--[[ Chipped Gems
	["gcv"] = {}, -- Chipped Amethyst
	["gcw"] = {}, -- Chipped Diamond
	["gcg"] = {}, -- Chipped Emerald
	["gcr"] = {}, -- Chipped Ruby
	["gcb"] = {}, -- Chipped Sapphire
	["skc"] = {}, -- Chipped Skull
	["gcy"] = {}, -- Chipped Topaz
--]]

--[[ All Gems
	["Amethyst"] = {}, -- All Amethysts
	["Diamond"] = {}, -- All Diamonds
	["Emerald"] = {}, -- All Emeralds
	["Ruby"] = {}, -- All Rubies
	["Sapphire"] = {}, -- All Sapphires
	["Skull"] = {}, -- All Skulls
	["Topaz"] = {}, -- All Topazes
--]]
}