-- expansion\white\helms.lua "strict"

local goodWhiteOrGlitchHelm =
{ priority = 6,
	goodItem =
		( not Ethereal
			and hasSockets("03")
			and DefensePercent >= 15
		or Ethereal
			and Sockets == 0
			and DefensePercent == 0
			and MaxDurabilityPercent == 0
			--and BaseDefense >= 230
		)
}

local goodWhiteHelm =
{ priority = 5,
	goodItem =
		( not Ethereal
			and hasSockets("03")
			and DefensePercent >= 15
		)
}

local anyGoodHelm =
{ priority = 5,
	goodItem =
		( not Ethereal
			and hasSockets("023")
			and DefensePercent >= 15
       	or Ethereal
			and Sockets == 0
			and DefensePercent == 0
			and MaxDurabilityPercent == 0
		)
}

local goodGlitchHelm =
{ priority = 6,
	goodItem =
		( Ethereal
			and Sockets == 0
			and DefensePercent == 0
			and MaxDurabilityPercent == 0
			--and BaseDefense >= 230
		)
}

local isGoodBarbHelm =
function(item)
	local bcSkills, mSkills, wcSkills = false, false, false

	bcSkills =
		( WhirlwindSkill >= 3
			or BerserkSkill >= 3
			or FrenzySkill >= 3
			--or BashSkill >= 3
			--or DoubleSwingSkill >= 3
			--or LeapSkill >= 3
			--or DoubleThrowSkill >= 3
			--or StunSkill >= 3
			--or LeapAttackSkill >= 3
			--or ConcentrateSkill >= 3 
		)
	mSkills =
		( IronSkinSkill >= 3
			or NaturalResistanceSkill >= 3
			--or SwordMasterySkill >= 3
			--or AxeMasterySkill >= 3
			--or MaceMasterySkill >= 3
			--or PoleArmMasterySkill >= 3
			--or ThrowingMasterySkill >= 3
			--or SpearMasterySkill >= 3
			--or IncreasedStaminaSkill >= 3
			--or IncreasedSpeedSkill >= 3
		)
	wcSkills =
		( BattleOrdersSkill >= 3
			--and (ShoutSkill + BattleCommandSkill) >= 3
			--and BattleCommandSkill >= 3
			--or WarCrySkill >= 3
			--or BattleCommandSkill >= 3
			--or ShoutSkill >= 3
			--or HowlSkill >= 3
			--or TauntSkill >= 3
			--or FindPotionSkill >= 3
			--or FindItemSkill >= 3
			--or BattleCrySkill >= 3
			--or GrimWardSkill >= 3
		)
	return
		( not Ethereal
			and hasSockets("0123")
			and ( wcSkills
				--or bcSkills
				--or mSkills
				)
		--or Ethereal
			--and Sockets == 0
			--and DefensePercent == 0
			--and MaxDurabilityPercent == 0
			--and ( wcSkills
				--or bcSkills
				--or mSkills )
		)
end	

local isGoodDruidHelm =
function(item)
	local dsSkills, ssSkills, eSkills = false, false, false

	dsSkills =
		( OakSageSkill >= 3
			or HeartOfWolverineSkill >= 3
			or SummonGrizzlySkill >= 3
			--or RavenSkill >= 3
			--or PoisonCreeperSkill >= 3
			--or SummonSpiritWolfSkill >= 3
			--or CycleOfLifeSkill >= 3 	-- Carrion Vine
			--or SummonDireWolfSkill >= 3
			--or VinesSkill >= 3 		-- Solar Creeper
			--or SpiritOfBarbsSkill >= 3
		)
	ssSkills =
		( WerewolfSkill >= 3
			or WearbearSkill >= 3
			--or FurySkill >= 3
			--or ShapeShiftingSkill >= 3 -- Lycanthropy 
			--or FireClawsSkill >= 3
			--or FeralRageSkill >= 3
			--or MaulSkill >= 3
			or RabiesSkill >= 3
			--or ShockWaveSkill >= 3
			--or HungerSkill >= 3
		)
	eSkills =
		( TornadoSkill >= 3
				and (HurricaneSkill + TwisterSkill + CycloneArmorSkill) >= 1
			--or HurricaneSkill >= 3
			--or ArmageddonSkill >= 3
			--or VolcanoSkill >= 3
			--or FirestormSkill >= 3
			--or MoltenBoulderSkill >= 3
			--or ArcticBlastSkill >= 3
			--or FissureSkill >= 3
			--or CycloneArmorSkill >= 3
			--or TwisterSkill >= 3
		)
	return
		( not Ethereal
			and hasSockets("03")
			and ( eSkills
				--or ssSkills
				--or dsSkills
				)
		--or Ethereal
			--and Sockets == 0
			--and DefensePercent == 0
			--and MaxDurabilityPercent == 0
			--and ( eSkills
				--or ssSkills
				--or dsSkills )
		)
end	

white.helms =
{ checkStats = true,
--[[ Normal Helms
	["cap"] = anyGoodHelm, -- Cap
	["skp"] = anyGoodHelm, -- Skull Cap
	["hlm"] = anyGoodHelm, -- Helm
	["fhl"] = anyGoodHelm, -- Full Helm
	["ghm"] = anyGoodHelm, -- Great Helm
	["msk"] = anyGoodHelm, -- Mask
	["crn"] = anyGoodHelm, -- Crown
	["bhm"] = anyGoodHelm, -- Bone Helm
--]]

--[[ Exceptional Helms
	["xap"] = anyGoodHelm, -- War Hat
	["xkp"] = anyGoodHelm, -- Sallet
	["xlm"] = anyGoodHelm, -- Casque
	["xhl"] = anyGoodHelm, -- Basinet
	["xhm"] = anyGoodHelm, -- Winged Helm
	["xsk"] = anyGoodHelm, -- Death Mask
	["xrn"] = anyGoodHelm, -- Grand Crown
	["xh9"] = anyGoodHelm, -- Grim Helm
--]]

---[[ Elite Helms
	--["uap"] = anyGoodHelm, -- Shako
	--["ukp"] = anyGoodHelm, -- Hydraskull
	--["ulm"] = anyGoodHelm, -- Armet
	--["uhl"] = anyGoodHelm, -- Giant Conch
	["uhm"] = goodGlitchHelm, -- Spired Helm
	["usk"] = goodWhiteOrGlitchHelm, -- Demonhead
	["urn"] = goodGlitchHelm, -- Corona
	["uh9"] = goodWhiteOrGlitchHelm, -- Bone Visage
--]]

--[[ Circlets
	["ci0"] = anyGoodHelm, -- Circlet
	["ci1"] = anyGoodHelm, -- Coronet
	["ci2"] = goodWhiteHelm, -- Tiara
	["ci3"] = goodWhiteHelm, -- Diadem
--]]

---[[ Barbarian Helms	
	["PrimalHelm"] = 
		{ priority = 4,
			isGoodItem = isGoodBarbHelm
		},
--]]

---[[ Druid Pelts		
	["Pelt"] = 
		{ priority = 4,
			isGoodItem = isGoodDruidHelm
		},
--]]
}