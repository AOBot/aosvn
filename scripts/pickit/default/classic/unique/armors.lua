-- classic\unique\armors.lua "strict"

unique.armors =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- Armors -------------------------------------------
-----------------------------------------------------
--[[ Greyform (Quilted Armor)
	["qui"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 31 --note: 28-31 total def
				)
		},

--]]
-----------------------------------------------------
--[[ Blinkbat's Form (Leather Armor)
	["lea"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 42 --note: 39-42 total def
				)
		},

--]]
-----------------------------------------------------
--[[ The Centurion (Hard Leather Armor)
	["hla"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 54 --note: 51-54 total def
				)
		},

--]]
-----------------------------------------------------
--[[ Twitchthroe (Studded Leather)
	["stu"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 60 --note: 57-60 total def
				)
		},

--]]
-----------------------------------------------------
--[[ Darkglow (Ring Mail)
	["rng"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 95 --note: 70-100 ed
				)
		},

--]]
-----------------------------------------------------
--[[ Hawkmail (Scale Mail)
	["scl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 95 --note: 80-100 ed
				)
		},

--]]
-----------------------------------------------------
--[[ Venom Ward (Breast Plate)
	["brs"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 95 --note: 60-100 ed
				)
		},

--]]
-----------------------------------------------------
--[[ Sparking Mail (Chain Mail)
	["chn"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 80 --note: 75-85 ed
					    and AttackerTakesLightningDamage >= 14 --note: 10-14 ldam
				)
		},

--]]
-----------------------------------------------------
--[[ Iceblink (Splint Mail)
	["spl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 80 --note: 70-80 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Heavenly Garb (Light Plate)
	["ltp"] =
		{
			priority = 2, identify = true,
			--note: all the same
		},
--]]
-----------------------------------------------------
--[[ Boneflesh (Plate Mail)
	["plt"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 120 --note: 100-120 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Rockfleece (Field Plate)
	["fld"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 125 --note: 100-130 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Rattlecage (Gothic Plate)
	["gth"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 333 --note: 328-335 total def
				)
		},
--]]
-----------------------------------------------------
---[[ Goldskin (Full Plate Mail)
	["ful"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 150 --note: 120-150 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Silks of the Victor (Ancient Armor)
	["aar"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 120 --note: 100-120 ed
				)
		},
	
--]]
-----------------------------------------------------
}

