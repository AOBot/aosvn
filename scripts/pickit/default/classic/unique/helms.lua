-- classic\unique\helms.lua "strict"

unique.helms =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- Helms --------------------------------------------
-----------------------------------------------------
--[[ Biggin's Bonnet (Cap)
	["cap"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 19 --note: 17-19 total def
				)
		},
	
--]]
-----------------------------------------------------
---[[ Tarnhelm (Skull Cap)
	["skp"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or BaseDefense >= 8 --note: 8-11 base def
					    and MagicFind >= 50 --note: 25-50 mf
				)
		},
	
--]]
-----------------------------------------------------
--[[ Coif of Glory (Helm)
	["hlm"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 28 --note: 25-28 total def
				)
		},

--]]
-----------------------------------------------------
--[[ Duskdeep (Full Helm)
	["fhl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 58 --note: 45-60 total def
				)
		},
	
--]]
-----------------------------------------------------
--[[ Howltusk (Great Helm)
	["ghm"] =
		{
			priority = 2, identify = true,
			--note: all the same
		},
	
--]]
-----------------------------------------------------
--[[ The Face of Horror (Mask)
	["msk"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 52 --note: 34-52 total def
				)
		},
	
--]]
-----------------------------------------------------
--[[ Undead Crown (Crown)
	["crn"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or TotalDefense >= 110 --note: 99-113 total def
					    and UndeadToHit >= 98 --note: 50-100 AR to undead
				)
		},
	
--]]
-----------------------------------------------------
--[[ Wormskull (Bone Helm)
	["bhm"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or BaseDefense >= 36 --note: 33-36 base def
				)
		},
	
--]]
-----------------------------------------------------
}