-- classic\unique\rings.lua "strict"

unique.rings =
{ checkStats = _CheckUniqueItemStats,
	["rin"] = ------ Ring
		{ priority = 8, identify = true,
			goodItem =
				( Unidentified
-----------------------------------------------------
-- This part keeps rings regardless of stats
-- Note: this part overrides the next part
-----------------------------------------------------
					--or UniqueIndex == 120 -- Nagelring
					--or UniqueIndex == 121 -- Manald Heal
					or UniqueIndex == 122 -- The Stone of Jordan
					
-----------------------------------------------------
-- This part keeps rings with specific stats
-----------------------------------------------------
---[[ Nagelring (perfect is 30mf, 75ar)
					or UniqueIndex == 120 -- Nagelring
						and MagicFind >= 30 --note: 15-30 MF
						--and ToHit >= 50 --note: 50-75 AR
--]]
-----------------------------------------------------
---[[ Manald Heal (perfect is 7ml, 8replenish life)
	                or UniqueIndex == 121 -- Manald Heal
	                    and ManaDrainMinDamage >= 7 --note: 4-7 manastolen
	                    and LifeRegen >= 8 --note: 5-8 replen life
--]]
-----------------------------------------------------
				)
		}
}