-- classic\unique\boots.lua "strict"

unique.boots =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- Boots --------------------------------------------
-----------------------------------------------------
---[[ Hotspur (Boots)
	["lbt"] =
		{
			priority = 2, identify = true,
			goodItem =
					( Unidentified
						or DefensePercent >= 20 --note: 10-20 ed
					)
		},
--]]
-----------------------------------------------------
--[[ Gorefoot (Heavy Boots)
	["vbt"] =
		{
			priority = 2, identify = true,
			goodItem =
					( Unidentified
						or DefensePercent >= 30 --note: 20-30 ed
					)
		},
--]]
-----------------------------------------------------
--[[ Treads of Cthon (Chain Boots)
	["mbt"] =
		{
			priority = 2, identify = true,
			goodItem =
					( Unidentified
						or DefensePercent >= 40 --note: 30-40 ed
					)
		},
	
--]]
-----------------------------------------------------
---[[ Goblin Toe (Light Plated Boots)
	["tbt"] =
		{
			priority = 2, identify = true,
			goodItem =
					( Unidentified
						or DefensePercent >= 60 --note: 50-60 ed
					)
		},
	
--]]
-----------------------------------------------------
--[[ Tearhaunch (Greaves)
	["hbt"] =
		{
			priority = 2, identify = true,
			goodItem =
					( Unidentified
						or DefensePercent >= 80 --note: 60-80 ed
					)
		},
	
--]]
-----------------------------------------------------
}
