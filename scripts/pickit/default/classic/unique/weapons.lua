-- classic\unique\weapons.lua "strict"

unique.weapons =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- Axes ---------------------------------------------
-----------------------------------------------------
--[[ The Gnasher (Hand Axe)
	["hax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 68 --note: 60-70 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Deathspade (Axe)
	["axe"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 68 --note: 60-70 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Bladebone (Double Axe)
	["2ax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 48 --note: 30-50 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Skull Splitter (Military Pick)
	["mpi"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 95 --note: 60-100 edam
					    --and LightMaxDamage >= 14 --note: 12-15 light dam
					    and ToHit >= 90 --note: 50-100 AR
				)
		},
--]]
-----------------------------------------------------
--[[ Rakescar (War Axe)
	["wax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 140 --note: 75-150 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Axe of Fechmar (Large Axe)
	["lax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 88 --note: 70-90 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Goreshovel (Broad Axe)
	["bax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 48 --note: 40-50 edam
				)
		},
--]]
-----------------------------------------------------
--[[ The Chieftain (Battle Axe)
	["btx"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or AllResist >= 20 --note: 10-20 all res
				)
		},
--]]
-----------------------------------------------------
--[[ Brainhew (Great Axe)
	["gax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 75 --note: 50-80 edam
					    and ManaDrainMinDamage >= 13 --note: 10-13 manastolen
				)
		},
--]]
-----------------------------------------------------
--[[ Humongous (Giant Axe)
	["gix"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 110 --note: 80-120 edam
					    and Strength >= 30 --note: 20-30 str
				)
		},
--]]

-----------------------------------------------------
-- Bows ---------------------------------------------
-----------------------------------------------------
--[[ Pluckeye (Short Bow)
	["sbw"] = 
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Witherstring (Hunter's Bow)
	["hbw"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 48 --note: 40-50 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Raven Claw (Long Bow)
	["lbw"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 68 --note: 60-70 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Rogue's Bow (Composite Bow) 
	["cbw"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 58 --note: 40-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Stormstrike (Short Battle Bow) 
	["sbb"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 88 --note: 70-90 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Wizendraw (Long Battle Bow)
	["lbb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 75 --note: 70-80 edam
						--and ToHit >= 90 --note: 50-100 AR
						and PassiveColdPierce >= 33 --note: 20-35 -cold res
				)
		},	
--]]
-----------------------------------------------------
--[[ Hellclap (Short War Bow)
	["swb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 85 --note: 70-90 edam
						and FireMaxDamage >= 50 --note: 30-50 max fire dam
						--and ToHit >= 70 --note: 50-75 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Blastbark (Long War Bow)
	["lwb"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 125 --note: 70-130 edam
				)
		},	
--]]

-----------------------------------------------------
-- Crossbows ----------------------------------------
-----------------------------------------------------
--[[ Leadcrow (Light Crossbow)
	["lxb"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Ichorsting (Crossbow) 
	["mxb"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Hellcast (Heavy Crossbow)
	["hxb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 78 --note: 70-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Doomslinger (Repeating Crossbow)
	["rxb"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 95 --note: 60-100 edam
				)
		},	
--]]
 
-----------------------------------------------------
-- Daggers ------------------------------------------
-----------------------------------------------------
---[[ Gull (Dagger)
	["dgr"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ The Diggler (Dirk)
	["dir"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ The Jade Tan do (Kris)
	["kri"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or ToHit >= 145 --note: 100-150 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Spectral Shard (Blade)
	["bld"] = 
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]

-----------------------------------------------------
-- Maces --------------------------------------------
-----------------------------------------------------
--[[ Felloak (Club)
	["clb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 78 --note: 70-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Stoutnail (Spiked Club)
	["spc"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or AttackerTakesDamage >= 10 --note: 3-10 dam to enemy
				)
		},	
--]]
-----------------------------------------------------
--[[ Crushflange (Mace)
	["mac"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 58 --note: 50-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Bloodrise (Morning Star)
	["mst"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ The General's Tan Do Li Ga (Flail) 
	["fla"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 58 --note: 50-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Ironstone (War Hammer)
	["whm"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 140 --note: 100-150 edam
						and ToHit >= 140 --note: 100-150 AR
				)
		},	
--]]
-----------------------------------------------------
---[[ Bonesnap (Maul)
	["mau"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 300 --note: 200-300 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Steeldriver (Great Maul)
	["gma"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 240 --note: 150-250 edam
				)
		},	
--]]
 
-----------------------------------------------------
-- Polearms -----------------------------------------
-----------------------------------------------------
--[[ Dimoak's Hew (Bardiche)
	["bar"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Steelgoad (Voulge)
	["vou"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 77 --note: 60-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Soul Harvest (Scythe)
	["scy"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 85 --note: 50-90 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Battlebranch (Poleaxe)
	["pax"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 67 --note: 50-70 edam
						and ToHit >= 90 --note: 50-100 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Woestave (Halberd)
	["hal"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 37 --note: 20-40 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Grim Reaper (War Scythe)
	["wsc"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]

-----------------------------------------------------
-- Scepters -----------------------------------------
-----------------------------------------------------
--[[ Knell Striker (Scepter)
	["scp"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 78 --note: 70-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Rusthandle (Grand Scepter)
	["gsc"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 58 --note: 50-60 edam
						and UndeadDamagepercent >= 105 -- 100-110 dmg to undead
						and VengeanceSkill >= 3 --note: 1-3 Vengeance
				)
		},	
--]]
-----------------------------------------------------
--[[ Stormeye (War Scepter)
	["wsp"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 110 --note: 80-120 edam
						and ResistLightningSkill >= 5 --note: 3-5 ResistLightning
				)
		},	
--]]

-----------------------------------------------------
-- Spears -------------------------------------------
-----------------------------------------------------
--[[ The Dragon Chang (Spear)
	["spr"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Razortine (Trident)
	["tri"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 48 --note: 30-50 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Bloodthief (Brandistock) 
	["brn"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 67 --note: 50-70 edam
						and LifeDrainMinDamage >= 12 --note: 8-12 lifestolen
				)
		},	
--]]
-----------------------------------------------------
--[[ Lance of Yaggai (Spetum)
	["spt"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ The Tannr Gorerod (Pike)
	["pik"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 95 --note: 80-100 edam
				)
		},	
--]]
 
-----------------------------------------------------
-- Staves -------------------------------------------
-----------------------------------------------------
--[[ Bane Ash (Short Staff)
	["sst"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 58 --note: 50-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Serpent Lord (Long Staff)
	["lst"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 38 --note: 30-40 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Spire of Lazarus (Gnarled Staff)
	["cst"] = 
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ The Salamander (Battle Staff)
	["bst"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ The Iron Jang Bong (War Staff)
	["wst"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]

-----------------------------------------------------
-- Swords -------------------------------------------
-----------------------------------------------------
--[[ Rixot's Keen (Short Sword)
	["ssd"] = 
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Blood Crescent (Scimitar)
	["scm"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 77 --note: 60-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Skewer of Krintiz (Sabre)
	["sbr"] = 
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Gleamscythe (Falchion) 
	["flc"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 90 --note: 60-100 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Griswold's Edge (Broad Sword)
	["bsd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 110 --note: 80-120 edam
						and FireMaxDamage >= 25 --note: 15-25 max fire dam
				)
		},	
--]]
-----------------------------------------------------
--[[ Hellplague (Long Sword)
	["lsd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 78 --note: 70-80 edam
				)
		},		
--]]
-----------------------------------------------------
--[[ Culwen's Point (War Sword)
	["wsd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 78 --note: 70-80 edam
				)
		},		
--]]
-----------------------------------------------------
--[[ Shadowfang (Two-Handed Sword)
	["2hs"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},		
--]]
-----------------------------------------------------
--[[ Soulflay (Claymore) 
	["clm"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 90 --note: 70-100 edam
						and ManaDrainMinDamage >= 10 --note: 4-10 manastolen
				)
		},		
--]]
-----------------------------------------------------
--[[ Kinemil's Awl (Giant Sword)
	["gis"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 90 --note: 80-100 edam
						and FireMaxDamage >= 40 --note: 20-40 max fire dam
				)
		},	
--]]
-----------------------------------------------------
--[[ Blacktongue (Bastard Sword) 
	["bsw"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 58 --note: 50-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Ripsaw (Flamberge)
	["flb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 95 --note: 80-100 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Patriarch (Great Sword)
	["gsd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 115 --note: 100-120 edam
				)
		},	
--]]

-----------------------------------------------------
-- Wands --------------------------------------------
-----------------------------------------------------
--[[ Torch of Iro (Wand)
	["wnd"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Maelstrom (Yew Wand)
	["ywn"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or IronMaidenSkill >= 1 --note: 1-3 IronMaiden
						and AmplifyDamageSkill >= 1 --note: 1-3 AmpDamage
						and TerrorSkill >= 1 --note: 1-3 Terror
						and CorpseExplosionSkill >= 1 --note: 1-3 CorpExplosion
				)
		},	
--]]
-----------------------------------------------------
--[[ Gravenspine (Bone Wand)
	["bwn"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxMana >= 48 --note: 25-50 mana
				)
		},	
--]]
-----------------------------------------------------
--[[ Ume's Lament (Grim Wand)
	["gwn"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
}