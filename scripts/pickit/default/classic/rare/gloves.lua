-- classic\rare\gloves.lua "strict"

local isGoodRareGloves =
function(item)
	local ias, stat, other = false, false, false

	ias = FasterAttackRate >= 10
	stat =
		( Strength >= 9
			or Dexterity >= 15
			--or MaxMana >= 38
		)
	other =
		( TotalResist >= 55
			or LifeDrainMinDamage >= 3 and ManaDrainMinDamage >= 3
			or ToHitPercent >= 5
			--or DefensePercent >= 81
		)
	return
		( Unidentified
			or ias and stat and other
			--or ias and (stat or other)
		)
end

rare.gloves =
{ checkStats = true,
	["Gloves"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareGloves
		}
}
