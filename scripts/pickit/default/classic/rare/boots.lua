-- classic\rare\boots.lua "strict"

local isGoodRareBoots =
function(item)
	local frw, stat, other = false, false, false
	
	frw = FasterMoveVelocity >= 30
	stat =
		( Dexterity >= 9
			or MaxMana >= 38
		)
	other =
		( FasterHitRecovery >= 10
			or MagicFind >= 20
			or GoldFind >= 80
			--or DefensePercent >= 81
		)
	return
		( Unidentified
			or TripleResist >= 60
			or TotalResist >= 60 and frw
			or TotalResist >= 50 and frw and (stat or other)
			or TotalResist >= 40 and frw and stat and other
		)
end

rare.boots =
{ checkStats = true,
	["Boots"] =
		{ priority = 4, identify = true,
			isGoodItem = isGoodRareBoots
		}
}
