-- classic\rare\amulets.lua "strict"

local isGoodRareAmulet =
function(item)
-- Good Skill Amount Settings
	local minSkill = 2 -- Class Skills
-------------------------------------------------
	local casterSkill, meleeSkill, fcr = false, false, false
	local goodMods, statMods, meleeMods = 0, 0, 0

	casterSkill =
			( PaladinSkills >= minSkill
			or SorceressSkills >= minSkill
			or NecromancerSkills >= minSkill
			)

	meleeSkill =
	    	( BarbarianSkills >= minSkill
	        or AmazonSkills >= minSkill
			or PaladinSkills >= minSkill
			)
	fcr = FasterCastRate >= 10
	goodMods =
		count{
			TotalResist >= 55,
			MagicFind >= 20,
			AllResist >= 18,
		}
	statMods =
		count{
			MaxLife >= 41,
			MaxMana >= 41,
			MaxLife >= 21 and MaxMana >= 21,
			Strength >= 16,
			Dexterity >= 16 and Energy >= 16,
			--Dexterity >= 16,
			--Energy >= 16,
		}
	meleeMods =
		count{
			LifeDrainMinDamage >= 5, --note: May not exist; Just in case
			ManaDrainMinDamage >= 5,
			ToHitPercent ~= 0,
			--MaxDamage >= 4,
			--MinDamage >= 8,
			MaxDamage + MinDamage >= 10,
			
		}
	return
		( Unidentified
		    or fcr and casterSkill
		        and (goodMods + statMods) >= 2
			or meleeSkill
			    and (goodMods + statMods + meleeMods) >= 3
			--or (casterSkill or meleeSkill)
			    --and (goodMods + statMods + meleeMods) >= 3
			--or ClassSkillsBonus >= 2
		)
end

rare.amulets =
{ checkStats = true,
	["amu"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareAmulet
		}
}

--[[
prefix
mana 61-90
sres 31-40
ares 16-20
cskill 2
tskill 2
dam2mana 7-12
mf 5-10

suffix
str 21-30
dex 16-20
eng 16-20
life 41-60
ll 6-9
ml 6-9
mf 16-25
fcr 10
]]