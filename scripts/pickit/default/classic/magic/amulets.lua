-- classic\magic\amulets.lua "strict"

local isGoodMagicAmulet =
function(item)
	local greatMod, prefix, suffix = false, false, false
	
	greatMod = (MagicFind + AllResist) >= 32
	prefix =
		( AllResist >= 20
			or ClassSkillsBonus >= 2
		)
	suffix =
		( MaxLife >= 41
			or FasterCastRate >= 10
			or Strength >= 16
			or Dexterity >= 16
			or Energy >= 16
			or MagicFind >= 20
			--or PoisonLengthReduction >= 75
		)

	return
		( Unidentified
			or greatMod
			or (prefix and suffix)
			--or (prefix or suffix)
			--or prefix
		)
end

magic.amulets =
{ checkStats = true,
	["amu"] = -- Amulet
		{ priority = 2, identify = true,
			isGoodItem = isGoodMagicAmulet
		}
}