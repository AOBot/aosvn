-- classic\magic\armors.lua "strict"

local goodMagicArmor =
{ priority = 2, identify = true,
	goodItem =
		( Unidentified
			or DefensePercent >= 81
				and (MaxLife >= 41 or FasterHitRecovery >= 24)
		)
}

magic.armors =
{ checkStats = true,
--[[ Normal Armors
	["qui"] = goodMagicArmor, -- Quilted Armor
	["lea"] = goodMagicArmor, -- Leather Armor
	["hla"] = goodMagicArmor, -- Hard Leather Armor
	["stu"] = goodMagicArmor, -- Studded Leather
	["rng"] = goodMagicArmor, -- Ring Mail
	["scl"] = goodMagicArmor, -- Scale Mail
	["brs"] = goodMagicArmor, -- Breast Plate
	["chn"] = goodMagicArmor, -- Chain Mail
	["spl"] = goodMagicArmor, -- Splint Mail
	["ltp"] = goodMagicArmor, -- Light Plate
	["plt"] = goodMagicArmor, -- Plate Mail
	["fld"] = goodMagicArmor, -- Field Plate
	["gth"] = goodMagicArmor, -- Gothic Plate
	["ful"] = goodMagicArmor, -- Full Plate Mail
	["aar"] = goodMagicArmor, -- Ancient Armor
--]]

---[[ Exceptional Armors
	["xui"] = goodMagicArmor, -- Ghost Armor
	["xea"] = goodMagicArmor, -- Serpentskin Armor
	["xla"] = goodMagicArmor, -- Demonhide Armor
	["xtu"] = goodMagicArmor, -- Trellised Armor
	["xcl"] = goodMagicArmor, -- Tigulated Mail
	["xng"] = goodMagicArmor, -- Linked Mail
	["xrs"] = goodMagicArmor, -- Cuirass
	["xhn"] = goodMagicArmor, -- Mesh Armor
	["xpl"] = goodMagicArmor, -- Russet Armor
	["xtp"] = goodMagicArmor, -- Mage Plate
	["xlt"] = goodMagicArmor, -- Templar Coat
	["xld"] = goodMagicArmor, -- Sharktooth Armor
	["xth"] = goodMagicArmor, -- Embossed Plate
	["xul"] = goodMagicArmor, -- Chaos Armor
	["xar"] = goodMagicArmor, -- Ornate Plate
--]]

}