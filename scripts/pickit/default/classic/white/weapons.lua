-- classic\white\weapons.lua "strict"

local keepForImbue = Sockets == 0 -- "Charsi Quest"

local fiveSocket =
{ priority = 3,
	goodItem =
		( Sockets == 5
			and MaxDamagePercent >= 10
		--or Sockets == 0 -- keep weapon for imbue "Charsi Quest"
		)
}

local sixSocket =
{ priority = 3,
	goodItem =
		( Sockets == 6
			and MaxDamagePercent >= 10
		--or Sockets == 0 -- keep weapon for imbue "Charsi Quest"
		)
}

white.weapons =
{ checkStats = true,
----------------------------------------
-- Axes
----------------------------------------
--[[ Normal Axes
	["hax"] = keepForImbue, -- Hand Axe
	["axe"] = keepForImbue, -- Axe
	["2ax"] = keepForImbue, -- Double Axe
	["mpi"] = keepForImbue, -- Military Pick
	["wax"] = keepForImbue, -- War Axe
	["lax"] = keepForImbue, -- Large Axe
	["bax"] = keepForImbue, -- Broad Axe
	["btx"] = keepForImbue, -- Battle Axe
	["gax"] = keepForImbue, -- Great Axe
	["gix"] = keepForImbue, -- Giant Axe
--]]

---[[ Exceptional Axes
	--["9ha"] = keepForImbue, -- Hatchet
	--["9ax"] = keepForImbue, -- Cleaver
	--["92a"] = fiveSocket, -- Twin Axe
	["9mp"] = sixSocket, -- Crowbill
	["9wa"] = sixSocket, -- Naga
	--["9la"] = keepForImbue, -- Military Axe
	--["9ba"] = fiveSocket, -- Bearded Axe
	--["9bt"] = fiveSocket, -- Tabar
	["9ga"] = sixSocket, -- Gothic Axe
	["9gi"] = sixSocket, -- Ancient Axe
--]]

----------------------------------------
-- Bows
----------------------------------------
--[[ Normal Bows
	["sbw"] = keepForImbue, -- Short Bow
	["hbw"] = keepForImbue, -- Hunter's Bow
	["lbw"] = keepForImbue, -- Long Bow
	["cbw"] = keepForImbue, -- Composite Bow
	["sbb"] = keepForImbue, -- Short Battle Bow
	["lbb"] = keepForImbue, -- Long Battle Bow
	["swb"] = keepForImbue, -- Short War Bow
	["lwb"] = keepForImbue, -- Long War Bow
--]]

---[[ Exceptional Bows
	--["8sb"] = keepForImbue, -- Edge Bow
	--["8hb"] = keepForImbue, -- Razor Bow
	--["8lb"] = fiveSocket, -- Cedar Bow
	--["8cb"] = keepForImbue, -- Double Bow
	--["8s8"] = fiveSocket, -- Short Siege Bow
	["8l8"] = sixSocket, -- Large Siege Bow
	--["8sw"] = fiveSocket, -- Rune Bow
	["8lw"] = sixSocket, -- Gothic Bow
--]]

----------------------------------------
-- Crossbows
----------------------------------------
--[[ Normal Crossbows
	["lxb"] = keepForImbue, -- Light Crossbow
	["mxb"] = keepForImbue, -- Crossbow
	["hxb"] = keepForImbue, -- Heavy Crossbow
	["rxb"] = keepForImbue, -- Repeating Crossbow
--]]

--[[ Exceptional Crossbows
	["8lx"] = keepForImbue, -- Arbalest
	["8mx"] = keepForImbue, -- Siege Crossbow
	["8hx"] = sixSocket, -- Ballista
	["8rx"] = fiveSocket, -- Chu-Ko-Nu
--]]

----------------------------------------
-- Daggers
----------------------------------------
--[[ Normal Daggers
	["dgr"] = keepForImbue, -- Dagger
	["dir"] = keepForImbue, -- Dirk
	["kri"] = keepForImbue, -- Kris
	["bld"] = keepForImbue, -- Blade
--]]

--[[ Exceptional Daggers
	["9dg"] = keepForImbue, -- Poignard
	["9di"] = keepForImbue, -- Rondel
	["9kr"] = keepForImbue, -- Cinquedeas
	["9bl"] = keepForImbue, -- Stiletto
--]]

----------------------------------------
-- Maces
----------------------------------------
--[[ Normal Maces
	["clb"] = keepForImbue, -- Club
	["spc"] = keepForImbue, -- Spiked Club
	["mac"] = keepForImbue, -- Mace
	["mst"] = keepForImbue, -- Morning Star
	["fla"] = keepForImbue, -- Flail
	["whm"] = keepForImbue, -- War Hammer
	["mau"] = keepForImbue, -- Maul
	["gma"] = keepForImbue, -- Great Maul
--]]

---[[ Exceptional Maces
	--["9cl"] = keepForImbue, -- Cudgel
	--["9sp"] = keepForImbue, -- Barbed Club
	--["9ma"] = keepForImbue, -- Flanged Mace
	--["9mt"] = keepForImbue, -- Jagged Star
	["9fl"] = fiveSocket, -- Knout
	--["9wh"] = keepForImbue, -- Battle Hammer
	["9m9"] = sixSocket, -- War Club
	["9gm"] = sixSocket, -- Martel de Fer
--]]

----------------------------------------
-- Polearms
----------------------------------------
--[[ Normal Polearms
	["bar"] = keepForImbue, -- Bardiche
	["vou"] = keepForImbue, -- Voulge
	["scy"] = keepForImbue, -- Scythe
	["pax"] = keepForImbue, -- Poleaxe
	["hal"] = keepForImbue, -- Halberd
	["wsc"] = keepForImbue, -- War Scythe
--]]

---[[ Exceptional Polearms
	--["9b7"] = keepForImbue, -- Lochaber Axe
	--["9vo"] = keepForImbue, -- Bill
	--["9s8"] = fiveSocket, -- Battle Scythe
	--["9pa"] = fiveSocket, -- Partizan
	["9h9"] = sixSocket, -- Bec-De-Corbin
	["9wc"] = sixSocket, -- Grim Scythe
--]]

----------------------------------------
-- Spears
----------------------------------------
--[[ Normal Spears
	["spr"] = keepForImbue, -- Spear
	["tri"] = keepForImbue, -- Trident
	["brn"] = keepForImbue, -- Brandistock
	["spt"] = keepForImbue, -- Spetum
	["pik"] = keepForImbue, -- Pike
--]]

---[[ Exceptional Spears
	--["9sr"] = keepForImbue, -- War Spear
	--["9tr"] = keepForImbue, -- Fuscina
	--["9br"] = fiveSocket, -- War Fork
	["9st"] = sixSocket, -- Yari
	["9p9"] = sixSocket, -- Lance
--]]

----------------------------------------
-- Swords
----------------------------------------
--[[ Normal Swords
	["ssd"] = keepForImbue, -- Short Sword
	["scm"] = keepForImbue, -- Scimitar
	["sbr"] = keepForImbue, -- Sabre
	["flc"] = keepForImbue, -- Falchion
	["crs"] = keepForImbue, -- Crystal Sword
	["bsd"] = keepForImbue, -- Broad Sword
	["lsd"] = keepForImbue, -- Long Sword
	["wsd"] = keepForImbue, -- War Sword
	["2hs"] = keepForImbue, -- Two-Handed Sword
	["clm"] = keepForImbue, -- Claymore
	["gis"] = keepForImbue, -- Giant Sword
	["bsw"] = keepForImbue, -- Bastard Sword
	["flb"] = keepForImbue, -- Flamberge
	["gsd"] = keepForImbue, -- Great Sword
--]]

---[[ Exceptional Swords
	--["9ss"] = keepForImbue, -- Gladius
	--["9sm"] = keepForImbue, -- Cutlass
	--["9sb"] = keepForImbue, -- Shamshir
	--["9fc"] = keepForImbue, -- Tulwar
	["9cr"] = sixSocket, -- Dimensional Blade
	--["9bs"] = keepForImbue, -- Battle Sword
	--["9ls"] = keepForImbue, -- Rune Sword
	--["9wd"] = keepForImbue, -- Ancient Sword
	--["92h"] = keepForImbue, -- Espandon
	--["9cm"] = keepForImbue, -- Dacian Falx
	--["9gs"] = keepForImbue, -- Tusk Sword
	--["9b9"] = keepForImbue, -- Gothic Sword
	--["9fb"] = fiveSocket, -- Zweihander
	["9gd"] = sixSocket, -- Executioner Sword
--]]

----------------------------------------
-- Javelins
----------------------------------------
--[[ Normal Javelins
	["jav"] = keepForImbue, -- Javelin
	["pil"] = keepForImbue, -- Pilum
	["ssp"] = keepForImbue, -- Short Spear
	["glv"] = keepForImbue, -- Glaive
	["tsp"] = keepForImbue, -- Throwing Spear
--]]

--[[ Exceptional Javelins
	["9ja"] = keepForImbue, -- War Javelin
	["9pi"] = keepForImbue, -- Great Pilum
	["9s9"] = keepForImbue, -- Simbilan
	["9gl"] = keepForImbue, -- Spiculum
	["9ts"] = keepForImbue, -- Harpoon
--]]

----------------------------------------
-- Throwing Weapons
----------------------------------------
--[[ Normal Throwables
	["tkf"] = keepForImbue, -- Throwing Knife
	["tax"] = keepForImbue, -- Throwing Axe
	["bkf"] = keepForImbue, -- Balanced Knife
	["bal"] = keepForImbue, -- Balanced Axe
--]]

--[[ Exceptional Throwables
	["9tk"] = keepForImbue, -- Battle Dart
	["9ta"] = keepForImbue, -- Francisca
	["9bk"] = keepForImbue, -- War Dart
	["9b8"] = keepForImbue, -- Hurlbat
--]]
------------------------------
-- Class-dependant Weapons
------------------------------
	--["Scepter"] = keepForImbue,
	--["Staff"] = keepForImbue,
	--["Wand"] = keepForImbue,
}