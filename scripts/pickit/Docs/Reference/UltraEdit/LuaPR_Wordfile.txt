/L1"Lua" LUA_LANG Block Comment On = --[[ Block Comment Off = ]] Block Comment On Alt = -- Escape Char = \ String Chars = "' File Extensions = lua
/Delimiters = ~!@%^&*()-+=|\/{}[]:;"'<> ,	.?
/Function String = "%[ ^t]++function^(*(*)^)"
/Function String 1 = "%[ ^t]++^(*^)=[ ^t]++function*(*)"
/Function String 2 = "%[ ^t]++local[ ^t]++function^(*(*)^)"
/Function String 3 = "%[ ^t]++local[ ^t]++^(*^)=[ ^t]++function*(*)"
/Indent Strings = "then" "do" "else" "function"
/Unindent Strings = "end" "else"
/Open Fold Strings = "{"
/Close Fold Strings = "}"
/C1"Lua Keywords"
and
break
do
else
elseif
end
false
for
function
if
in
local
nil
not
or
repeat
return
then
true
until
while
/C2"Lua Functions"
assert
collectgarbage
dofile
error
_G
getfenv
getmetatable
gcinfo
ipairs
loadfile
loadlib
loadstring
next
pairs
pcall
ploadfile
print
rawequal
rawget
rawset
require
setfenv
setmetatable
tonumber
tostring
type
unpack
_VERSION
xpcall
/C3"PR Global & Common"
checkStats
dump
ethereal
files
goodItem
hasSockets
identify
isGoodItem
item
ITEM
kind
magic
pickLimit
pickToSell
priority
rare
set
unique
white
_CheckUniqueItemStats
_LogErrors
_LogWarnings
_MinGoldPile
_SellUnlistedItems
_TestAllFiles
_UseItemLogging
/C4"AO StatType"
AbsorbCold
AbsorbColdByTime
AbsorbColdPercent
AbsorbColdPerLevel
AbsorbCrush
AbsorbCrushPercent
AbsorbFire
AbsorbFireByTime
AbsorbFirePercent
AbsorbFirePerLevel
AbsorbLight
AbsorbLightningPercent
AbsorbLightningByTime
AbsorbLightningPerLevel
AbsorbMagic
AbsorbMagicPercent
AbsorbPoisonByTime
AbsorbPoisonPerLevel
AbsorbSlash
AbsorbSlashPercent
AbsorbThrust
AbsorbThrustPercent
AddExperience
Alignment
AllSkillsBonus
ArmorByTime
ArmorClass
ArmorClassVsMelee
ArmorClassVsMissile
ArmorOverridePercent
ArmorPercentByTime
ArmorPercentPerLevel
ArmorPerLevel
AttackerTakesDamage
AttackerTakesLightningDamage
AttackRate
AttackVsMonsterType
Aura
BurningMax
BurningMin
CannotBeFrozen
ChargedSkill
ClassSkillsBonus
ColdDamageMaxPerLevel
ColdLength
ColdMaxDamage
ColdMaxDamageByTime
ColdMinDamage
ColdResist
ConversionLevel
ConversionMaxHP
CrushDamage
CrushDamagePercent
CrushingBlow
CrushingBlowByTime
CrushingBlowPerLevel
CurseResistance
DamageAura
DamageDemonByTime
DamageDemonPerLevel
DamageFramerate
DamagePercent
DamagePercentVsMonster
DamageReduction
DamageResist
DamageTargetAC
DamageToMana
DamageUndeadByTime
DamageUndeadPerLevel
DamageVsMonster
DamageVsMonsterType
DeadlyStrike
DeadlyStrikeByTime
DeadlyStrikePerLevel
DefensePercent
DefensePercentVsMonster
DefenseVsMonster
DemonDamagePercent
DemonToHit
Dexterity
DexterityByTime
DexterityPerLevel
DoubleHerbDuration
Durability
ElementalSkillBonus
Energy
EnergyByTime
EnergyPerLevel
Experience
ExplosiveArrow
ExtraBlood
ExtraCharges
ExtraStack
FasterAttackRate
FasterBlockRate
FasterCastRate
FasterHitRecovery
FasterMoveVelocity
FindGemsByTime
FindGemsPerLevel
FindGoldByTime
FindMagicByTime
FireDamageMaxPerLevel
FireLength
FireMaxDamage
FireMaxDamageByTime
FireMinDamage
FireResist
FractionalTargetAC
Freeze
Gold
GoldBank
GoldFind
GoldFindPerLevel
GoldLost
HalfFreezeDuration
HealAfterDemonKill
HealAfterKill
HitBlindsTarget
IgnoreTargetDefense
Indesctructible
IronMaidenLevel
KickDamage
KickDamageByTime
KickDamagePerLevel
Knockback
LastBlockFrame
LastExperience
LastSentLifePercent
Level
Life
LifeByTime
LifeDrainMaxDamage
LifeDrainMinDamage
LifePerLevel
LifeRegen
LifeTapLevel
LightColor
LightMaxDamage
LightMinDamage
LightningDamageMaxPerLevel
LightningMaxDamageByTime
LightRadius
LightResist
LowerLevelRequirement
LowerLevelRequirementPercent
LowerRequirementsPercent
MagicDamageReduction
MagicFind
MagicFindPerLevel
MagicMaxDamage
MagicMinDamage
MagicResist
Mana
ManaAfterKill
ManaByTime
ManaDrainMaxDamage
ManaDrainMinDamage
ManaPerLevel
ManaRecovery
ManaRecoveryBonus
MaxColdResist
MaxDamage
MaxDamageByTime
MaxDamagePercent
MaxDamagePercentByTime
MaxDamagePercentPerLevel
MaxDamagePerLevel
MaxDurability
MaxDurabilityPercent
MaxFireResist
MaxLife
MaxLifePercent
MaxLightResist
MaxMagicResist
MaxMana
MaxManaPercent
MaxPoisonResist
MaxStamina
MinDamage
MinDamagePercent
ModifierListLevel
ModifierListSkill
MonsterPlayerCount
NextExperience
NonClassSkill
NormalDamage
OpenWounds
OpenWoundsByTime
OpenWoundsPerLevel
OtherAnimRate
PassiveAvoid
PassiveColdMastery
PassiveColdPierce
PassiveCriticalStrike
PassiveDodge
PassiveEvade
PassiveFireMastery
PassiveFirePierce
PassiveLightningMastery
PassiveLightningPierce
PassiveMagicMastery
PassiveMagicPierce
PassiveMasteryMeleeCritical
PassiveMasteryMeleeDamage
PassiveMasteryMeleeToHit
PassiveMasteryThrowCritical
PassiveMasteryThrowDamage
PassiveMasteryThrowToHit
PassivePoisonMastery
PassivePoisonPierce
PassiveSummon_resist
PassiveWarmth
PassiveWeaponBlock
PierceCold
PierceFire
PierceIdx
PierceLightning
PiercePoison
PoisonCount
PoisonDamageMaxPerLevel
PoisonLength
PoisonLengthReduction
PoisonMaxDamage
PoisonMaxDamageByTime
PoisonMinDamage
PoisonResist
PreventHeal
ProgressiveCold
ProgressiveDamage
ProgressiveFire
ProgressiveLightning
ProgressiveOther
ProgressiveSteal
ProgressiveToHit
Quantity
QuestItemDifficulty
Reanimate
ReducedPrices
RegenStaminaByTime
RegenStaminaPerLevel
ReplenishDurability
ReplenishQuantity
ResistColdByTime
ResistColdPerLevel
ResistFireByTime
ResistFirePerLevel
ResistLightningByTime
ResistLightningPerLevel
ResistPoisonByTime
ResistPoisonPerLevel
RestInPeace
SecondaryMaxDamage
SecondaryMinDamage
ShortParam
SingleSkill
SkillArmorPercent
SkillBypassBeasts
SkillBypassDemons
SkillBypassUndead
SkillChillingArmor
SkillConcentration
SkillConviction
SkillDecrepify
SkillEnchant
SkillFrenzy
SkillHandOfAthena
SkillOnAttack
SkillOnDeath
SkillOnGetHit
SkillOnKill
SkillOnLevelUp
SkillOnStriking
SkillPassiveStaminaPercent
SkillPierce
SkillPoints
SkillPoisonOverrideLength
SkillStaminaPercent
SkillTabBonus
SlashDamage
SlashDamagePercent
Slow
Sockets
SourceUnitID
SourceUnitType
StamDrainMaxDamage
StamDrainMinDamage
Stamina
StaminaByTime
StaminaDrainPercent
StaminaPerLevel
StaminaRecoveryBonus
State
StatPoints
Strength
StrengthByTime
StrengthPerLevel
StunLength
ThornsPercent
ThornsPerLevel
Throwable
ThrowMaxDamage
ThrowMinDamage
ThrustDamage
ThrustDamagePercent
TimeDuration
ToBlock
ToHit
ToHitByTime
ToHitDemonByTime
ToHitDemonPerLevel
ToHitPercent
ToHitPercentByTime
ToHitPercentPerLevel
ToHitPercentVsMonster
ToHitPerLevel
ToHitUndeadByTime
ToHitUndeadPerLevel
ToHitVsMonster
UndeadDamagepercent
UndeadToHit
UnitDoOverlay
Value
VelocityPercent
Vitality
VitalityByTime
VitalityPerLevel
/C5"AO Skills"
** Amazon
** AmplifyDamage
** AndrialSpray
** AndyPoisonBolt
** ArcaneTower
** ArcticBlast
** Armageddon
** Assassin
** Attack
** Attract
** Avoid
** AxeMastery
** BaalCloneTeleport
** BaalColdMissiles
** BaalCorpseExplode
** BaalInferno
** BaalMonsterSpawn
** BaalNova
** BaalTaunt
** BaalTeleport
** BaalTentacle
** Barbarian
** BarbarianCombat
** BarbsAura
** Bash
** BattleCommand
** BattleCry
** BattleOrders
** BearSmite
** Berserk
** Bestow
** BladeFury
** BladeSentinel
** BladeShield
** BladesOfIce
** Blaze
** BlessedAim
** BlessedHammer
** Blizzard
** Bloodgolem
** BloodLordFrenzy
** BloodMana
** BoltSentry
** BoneArmor
** BonePrison
** BoneSpear
** BoneSpirit
** BoneWall
** BookOfIdentify
** BookOfTownportal
** BowAndCrossBow
** BurstOfSpeed
** CatapultBlizzard
** CatapultChargedBall
** CatapultMeteor
** CatapultPlague
** CatapultSpikeBall
** ChainLightning
** Charge
** ChargedBolt
** ChargedBoltSentry
** ChargedStrike
** ChillingArmor
** ClawMastery
** ClawsOfThunder
** ClayGolem
** Cleansing
** CloakOfShadows
** CobraStrike
** Cold
** ColdArrow
** ColdMastery
** Concentrate
** Concentration
** Confuse
** Conversion
** Conviction
** CorpseCycler
** CorpseExplosion
** CountessFirewall
** CriticalStrike
** CryHelp
** Cursedballtrapleft
** Cursedballtrapright
** Curses
** CycleOfLife
** CycloneArmor
** DeathMaul
** DeathSentry
** DeathSentry
** DeathSentryLightning
** Decrepify
** DefenseCurse
** DefensiveAuras
** Defiance
** Delirium
** DesertTurret
** DiabloArmageddon
** DiabloCold
** DiabloFire
** DiabloFirestorm
** DiabloLight
** DiabloPrison
** DiabloRun
** DimVision
** Dodge
** DoomKnightMissile
** Dopplezon
** DoubleSwing
** DoubleThrow
** DragonClaw
** DragonFlight
** DragonTail
** DragonTalon
** Druid
** DruidSummoning
** Elemental
** Emerge
** Enchant
** EnergyShield
** Evade
** EvilHutSpawner
** ExplodingArrow
** Fade
** Fanaticism
** Fend
** FenrisRage
** FeralRage
** FetishAura
** FetishInferno
** FindItem
** FindPotion
** FingerMageSpider
** Fire
** FireArrow
** FireBall
** FireBlast
** FireBolt
** FireClaws
** Firegolem
** FireHit
** FireMastery
** Firestorm
** FireWall
** Fissure
** FistOfTheHeavens
** FistsOfFire
** FreezingArrow
** Frenzy
** FrostNova
** FrozenArmor
** FrozenOrb
** Fury
** GargoyleTrap
** GlacialSpike
** GolemMastery
** GrimWard
** GuidedArrow
** HealingVortex
** HeartOfWolverine
** HellMeteor
** HireableMissile
** HolyBolt
** HolyFire
** HolyFreeze
** HolyShield
** HolyShock
** HorrorArcticBlast
** Howl
** Hunger
** Hurricane
** Hydra
** HydraMissile
** IceArrow
** IceBlast
** IceBolt
** ImmolationArrow
** Impale
** ImpBolt
** ImpFireball
** ImpFireMissile
** ImpFireMissileEx
** ImpInferno
** Impregnate
** ImpTeleport
** IncreasedSpeed
** IncreasedStamina
** Inferno
** InfernoSentry
** InnerSight
** IronGolem
** IronMaiden
** IronSkin
** Jab
** JavelinAndSpear
** Jump
** Kick
** Leap
** LeapAttack
** LeftHandSwing
** LeftHandThrow
** LifeTap
** Lightning
** LightningBolt
** LightningFury
** LightningMastery
** LightningSentry
** LightningStrike
** LowerResist
** MaceMastery
** MaggotEgg
** MagicArrow
** MagottDown
** MagottLay
** MagottUp
** MartialArts
** Masteries
** Maul
** Meditation
** MegaDemonInferno
** MephistoFrostNova
** MephistoMissile
** Meteor
** Might
** MindBlast
** MinionSpawner
** MissileSkill
** MoltenBoulder
** MonsterBlizzard
** MonsterBoneArmor
** MonsterBoneSpirit
** MonsterBow
** MonsterColdArrow
** MonsterCurseCast
** MonsterExplodingArrow
** MonsterFireArrow
** MonsterFreezingArrow
** MonsterFrenzy
** MonsterFrozenArmor
** MonsterIceSpear
** MonsterPowerStrike
** MonsterTeleport
** Mosquito
** MultipleShot
** NaturalResistance
** NecromageMissile
** Necromancer
** NecroSummoning
** Nest
** NihlathakCorpseExplosion
** Nova
** OakSage
** OakSageAura
** OffensiveAuras
** OverseerWhip
** Paladin
** PaladinCombat
** PassiveAndMagic
** Penetrate
** PhoenixStrike
** Pierce
** PlagueJavelin
** PoisonAndBone
** PoisonBallTrap
** PoisonCreeper
** PoisonDagger
** PoisonExplosion
** PoisonJavelin
** PoisonNova
** PoleArmMastery
** PowerStrike
** Prayer
** PrimeBlaze
** PrimeBolt
** PrimeFirewall
** PrimeIceNova
** PrimeLightning
** PrimePoisonBall
** PrimePoisonNova
** PrimeSpike
** PsychicHammer
** QueenDeath
** QuickStrike
** Rabies
** RaiseSkeletalMage
** RaiseSkeleton
** Raven
** Redemption
** RegurgitatorEat
** ResistCold
** ResistFire
** ResistLightning
** Resurrect
** Resurrect
** Revive
** RogueMissile
** Sacrifice
** Salvation
** Sanctuary
** ScrollOfIdentify
** ScrollOfTownportal
** Selfresurrect
** SentryLightning
** SerpentCharge
** ShadowDisciplines
** ShadowMaster
** ShadowWarrior
** ShamanFire
** ShamanFireEx
** ShamanIce
** ShapeShifting
** ShapeShifting
** ShiverArmor
** ShockWave
** ShockWeb
** Shout
** SiegeBeastStomp
** SkeletonMastery
** Skeletonraise
** SlowMissiles
** Smite
** Sorceress
** SpearMastery
** SpiderLay
** SpiritOfBarbs
** StaticField
** Strafe
** Stun
** SubMerge
** SuccubusBolt
** SuckBlood
** SummonDireWolf
** SummonGrizzly
** SummonResist
** SummonSpiritWolf
** SwarmMove
** SwordMastery
** Taunt
** Teeth
** Telekinesis
** Teleport
** Teleport
** Terror
** Thorns
** Throw
** ThrowingMastery
** ThunderStorm
** TigerStrike
** Tornado
** TrapNova
** Traps
** Twister
** Unholybolt
** UnHolyBoltEx
** Unsummon
** Valkyrie
** Vampirefireball
** Vampirefirewall
** VampireHeal
** Vampiremeteor
** VampireMissile
** VampireRaise
** Vengeance
** Venom
** Vigor
** VineAttack
** VineCycler
** Vines
** Volcano
** WakeOfDestructionSentry
** WakeOfFire
** WakeOfInferno
** Warcries
** WarCry
** Warmth
** Weaken
** WeaponBlock
** Wearbear
** Werewolf
** Whirlwind
** WolverineAura
** ZakarumHeal
** ZakarumLightning
** Zeal
Amazon
AmplifyDamage
AndrialSpray
AndyPoisonBolt
ArcaneTower
ArcticBlast
Armageddon
Assassin
Attack
Attract
Avoid
AxeMastery
BaalCloneTeleport
BaalColdMissiles
BaalCorpseExplode
BaalInferno
BaalMonsterSpawn
BaalNova
BaalTaunt
BaalTeleport
BaalTentacle
Barbarian
BarbarianCombat
BarbsAura
Bash
BattleCommand
BattleCry
BattleOrders
BearSmite
Berserk
Bestow
BladeFury
BladeSentinel
BladeShield
BladesOfIce
Blaze
BlessedAim
BlessedHammer
Blizzard
Bloodgolem
BloodLordFrenzy
BloodMana
BoltSentry
BoneArmor
BonePrison
BoneSpear
BoneSpirit
BoneWall
BookOfIdentify
BookOfTownportal
BowAndCrossBow
BurstOfSpeed
CatapultBlizzard
CatapultChargedBall
CatapultMeteor
CatapultPlague
CatapultSpikeBall
ChainLightning
Charge
ChargedBolt
ChargedBoltSentry
ChargedStrike
ChillingArmor
ClawMastery
ClawsOfThunder
ClayGolem
Cleansing
CloakOfShadows
CobraStrike
Cold
ColdArrow
ColdMastery
Concentrate
Concentration
Confuse
Conversion
Conviction
CorpseCycler
CorpseExplosion
CountessFirewall
CriticalStrike
CryHelp
Cursedballtrapleft
Cursedballtrapright
Curses
CycleOfLife
CycloneArmor
DeathMaul
DeathSentry
DeathSentry
DeathSentryLightning
Decrepify
DefenseCurse
DefensiveAuras
Defiance
Delirium
DesertTurret
DiabloArmageddon
DiabloCold
DiabloFire
DiabloFirestorm
DiabloLight
DiabloPrison
DiabloRun
DimVision
Dodge
DoomKnightMissile
Dopplezon
DoubleSwing
DoubleThrow
DragonClaw
DragonFlight
DragonTail
DragonTalon
Druid
DruidSummoning
Elemental
Emerge
Enchant
EnergyShield
Evade
EvilHutSpawner
ExplodingArrow
Fade
Fanaticism
Fend
FenrisRage
FeralRage
FetishAura
FetishInferno
FindItem
FindPotion
FingerMageSpider
Fire
FireArrow
FireBall
FireBlast
FireBolt
FireClaws
Firegolem
FireHit
FireMastery
Firestorm
FireWall
Fissure
FistOfTheHeavens
FistsOfFire
FreezingArrow
Frenzy
FrostNova
FrozenArmor
FrozenOrb
Fury
GargoyleTrap
GlacialSpike
GolemMastery
GrimWard
GuidedArrow
HealingVortex
HeartOfWolverine
HellMeteor
HireableMissile
HolyBolt
HolyFire
HolyFreeze
HolyShield
HolyShock
HorrorArcticBlast
Howl
Hunger
Hurricane
Hydra
HydraMissile
IceArrow
IceBlast
IceBolt
ImmolationArrow
Impale
ImpBolt
ImpFireball
ImpFireMissile
ImpFireMissileEx
ImpInferno
Impregnate
ImpTeleport
IncreasedSpeed
IncreasedStamina
Inferno
InfernoSentry
InnerSight
IronGolem
IronMaiden
IronSkin
Jab
JavelinAndSpear
Jump
Kick
Leap
LeapAttack
LeftHandSwing
LeftHandThrow
LifeTap
Lightning
LightningBolt
LightningFury
LightningMastery
LightningSentry
LightningStrike
LowerResist
MaceMastery
MaggotEgg
MagicArrow
MagottDown
MagottLay
MagottUp
MartialArts
Masteries
Maul
Meditation
MegaDemonInferno
MephistoFrostNova
MephistoMissile
Meteor
Might
MindBlast
MinionSpawner
MissileSkill
MoltenBoulder
MonsterBlizzard
MonsterBoneArmor
MonsterBoneSpirit
MonsterBow
MonsterColdArrow
MonsterCurseCast
MonsterExplodingArrow
MonsterFireArrow
MonsterFreezingArrow
MonsterFrenzy
MonsterFrozenArmor
MonsterIceSpear
MonsterPowerStrike
MonsterTeleport
Mosquito
MultipleShot
NaturalResistance
NecromageMissile
Necromancer
NecroSummoning
Nest
NihlathakCorpseExplosion
Nova
OakSage
OakSageAura
OffensiveAuras
OverseerWhip
Paladin
PaladinCombat
PassiveAndMagic
Penetrate
PhoenixStrike
Pierce
PlagueJavelin
PoisonAndBone
PoisonBallTrap
PoisonCreeper
PoisonDagger
PoisonExplosion
PoisonJavelin
PoisonNova
PoleArmMastery
PowerStrike
Prayer
PrimeBlaze
PrimeBolt
PrimeFirewall
PrimeIceNova
PrimeLightning
PrimePoisonBall
PrimePoisonNova
PrimeSpike
PsychicHammer
QueenDeath
QuickStrike
Rabies
RaiseSkeletalMage
RaiseSkeleton
Raven
Redemption
RegurgitatorEat
ResistCold
ResistFire
ResistLightning
Resurrect
Resurrect
Revive
RogueMissile
Sacrifice
Salvation
Sanctuary
ScrollOfIdentify
ScrollOfTownportal
Selfresurrect
SentryLightning
SerpentCharge
ShadowDisciplines
ShadowMaster
ShadowWarrior
ShamanFire
ShamanFireEx
ShamanIce
ShapeShifting
ShapeShifting
ShiverArmor
ShockWave
ShockWeb
Shout
SiegeBeastStomp
SkeletonMastery
Skeletonraise
SlowMissiles
Smite
Sorceress
SpearMastery
SpiderLay
SpiritOfBarbs
StaticField
Strafe
Stun
SubMerge
SuccubusBolt
SuckBlood
SummonDireWolf
SummonGrizzly
SummonResist
SummonSpiritWolf
SwarmMove
SwordMastery
Taunt
Teeth
Telekinesis
Teleport
Teleport
Terror
Thorns
Throw
ThrowingMastery
ThunderStorm
TigerStrike
Tornado
TrapNova
Traps
Twister
Unholybolt
UnHolyBoltEx
Unsummon
Valkyrie
Vampirefireball
Vampirefirewall
VampireHeal
Vampiremeteor
VampireMissile
VampireRaise
Vengeance
Venom
Vigor
VineAttack
VineCycler
Vines
Volcano
WakeOfDestructionSentry
WakeOfFire
WakeOfInferno
Warcries
WarCry
Warmth
Weaken
WeaponBlock
Wearbear
Werewolf
Whirlwind
WolverineAura
ZakarumHeal
ZakarumLightning
Zeal
/C6"PR Additional Stats"
AllResist
BaseDefense
DoubleResist
Ethereal
Identified
ItemCode
ItemLevel
ItemQuality
MaxResist
MinResist
PrefixIndex
SetIndex
SingleResist
SuffixIndex
TotalDefense
TotalResist
TripleResist
Unidentified
UniqueIndex