----------------------------------------------------------------------
-- itemTables.txt Index
----------------------------------------------------------------------
	1: Basics
	2: Settings
----------------------------------------------------------------------
-- 1: Basics
----------------------------------------------------------------------
	quality.itemTable = { settings, itemEntries }
	Ex: unique.armors = { settings, itemEntries }

	itemTables must be preceded by "quality.", where quality is the
	item's quality (unique, rare, set, magic, white).
	*For more information on itemEntries, see "itemEntries.txt".
----------------------------------------------------------------------
-- 2: Settings
----------------------------------------------------------------------
	*All settings are optional, they are set to default values if not
	explicitly set.

	checkStats = true/false
		Default: true
		If this is set to false, then all itemEntries in the itemTable
		will be kept and stashed unidentified.
		*This basically just forces PR to ignore all itemEntry
		goodItem/isGoodItem settings in the itemTable.

	dump = true/false
		Default: true
		If this is set to false, then no itemEntries in the itemTable
		will be dumped.
		*This overrides the itemEntry dump settings.

	pickToSell = true/false
		Default: false
		If this is set to true, then all itemEntries in the itemTable
		will be picked, not dumped, and sold.
		*This overrides the itemEntry pickToSell settings.


