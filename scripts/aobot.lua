--[[ Main File From Which AO Runs ]]--
--[[
	aobot.lua is the lead file for AO to run from.  When a run is started it is a call to the Routine function
	held within.  Since this file is the starter for everything that AO runs it is a good place for global variables.
	We do need to consider though that this file is circumvented when an event is called from events.lua, therefore
	any global variables contained herein will not be accessible to any functions called from an event.  With this in mind
	we should NOT be putting global variables into this file, instead it should be put in the file in which that
	variable will be used... possibly even in multiple files if it is required.
	
	aobot.lua also holds the function for functional chat commands (ie: commands from the user that are complex
	and not just simple "location" or "start" or "stop") from the user.  It would be preferable to keep this file
	to a minimum of use, which should make the layout of AO much easier on the user.
	
	Core Functions/Variables Used : 
	Ao:set("VARIABLENAMEHERE", "VALUEHERE") -> sets a variable in core
	Ao:getNumber("VARIABLENAMEHERE") -> retrieves a set variable in core, useful for when using data scross stop/start and across different lua VMs (also used to get information during event calls) 
--]]

--Ao:Chat("aobot.lua loading includes")
loadfile("scripts\\includes.lua")()
--Ao:Chat("aobot.lua including settings")
includeSettings() --> includes character file, ao settings, character build, and custom.lua
includeCritical() --> logging


function Routine() --> Main function AO runs from
	trace:log("aobot.lua Routine() called")
	trace:log("aobot.lua Routine() check automateSettings "..Ao:getNumber("automateSettings"))
	if Ao:getNumber("automateSettings") == 1 then --> Check if core variable was for automating settings
		trace:log("aobot.lua Automating Settings")
		if protectedLoadFile(dirs.libs.."automation") then --> include automation lua to automate character
			automateSettings()
			Ao:set("automateSettings", 0)
		end
	end
	--[[ After settings are all loaded, we should set some core variables for a few of the options like public mode, etc to help other scripts ]]--
	trace:log("aobot.lua Routine() finished")
end

function Command()
	trace:log("Command called")
	local cmd = Ao:getString("cmd")
	assert(loadstring(string.sub(cmd, 2)))()
end

function clearStatus()
	Ao:set("killing", 0)
	Ao:set("moving", 0)
	Ao:set("interact", 0)
end