﻿using System;

namespace AoManager
{
    public static class ItemParser
    {
        public static Item ParseItemLine(string line)
        {
            string[] parsed = line.Split('|');
            Item.Action check = Item.Action.None;
            if (parsed[0].Contains("Picked")) { check = Item.Action.Picked; }
            else if (parsed[0].Contains("Identified")) { check = Item.Action.Identified; }
            else if (parsed[0].Contains("Sold")) { check = Item.Action.Sold; }
            else if (parsed[0].Contains("Stashed")) { check = Item.Action.Stashed; }
            else if (parsed[0].Contains("Gambled")) { check = Item.Action.Gambled; }
            else { check = Item.Action.Picked; }
            if (check != Item.Action.None)
                return new Item(check, parsed[1], Convert.ToInt32(parsed[2]), (Item.ItemQuality)Convert.ToInt32(parsed[3]));
            return null;
        }
    }

    public enum ItemQuality
    {
        NotApplicable = 0,
        Inferior = 1,
        Normal = 2,
        Superior = 3,
        Magic = 4,
        Set = 5,
        Rare = 6,
        Unique = 7,
        Crafted = 8,
    }

    public class Item
    {
        Action _action;
        string _item;
        int _ilevel;
        ItemQuality _iquality;

        public Item(Action a, string i, int il, ItemQuality iq)
        {
            this._action = a;
            this._item = i;
            this._ilevel = il;
            this._iquality = iq;
        }

        public Action ActionPerformed
        {
            get { return _action; }
        }

        public string ItemName
        {
            get { return _item; }
        }

        public int iLevel
        {
            get { return _ilevel; }
        }

        public ItemQuality iQuality
        {
            get { return _iquality; }
        }

        public enum Action
        {
            Picked,
            Identified,
            Sold,
            Stashed,
            Gambled,
            None
        }

        public enum ItemQuality
        {
            NotApplicable = 0,
            Inferior = 1,
            Normal = 2,
            Superior = 3,
            Magic = 4,
            Set = 5,
            Rare = 6,
            Unique = 7,
            Crafted = 8,
        }
    }

    public struct Game
    {
        string _started;
        string _name;
        string _finished;
        bool _success;

        public Game(string started, string name)
        {
            this._name = name;
            this._started = started;
            this._finished = started;
            this._success = false;
        }

        public Game(string started, string name, string finished, bool success)
        {
            this._name = name;
            this._started = started;
            this._finished = finished;
            this._success = success;
        }

        public string StartTime
        {
            get { return _started; }
            set { this._started = value; }
        }

        public string EndTime
        {
            get { return _finished; }
            set { this._finished = value; }
        }

        public string GameName
        {
            get { return _name; }
            set { this._name = value; }
        }

        public bool Success
        {
            get { return _success; }
            set { this._success = value; }
        }

        public void Reset()
        {
            this._name = string.Empty;
            this._started = string.Empty;
            this._finished = string.Empty;
            this._success = false;
        }
    }
}
