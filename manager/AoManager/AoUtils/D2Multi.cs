﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;

namespace AoManager
{
    public static class D2Multi
    {
        private static string multipath = Application.StartupPath + "\\D2Multi\\";

        private static bool CreateNewMultiIni(string diapath, Logging log)
        {
            try
            {
                using (StreamWriter s = new StreamWriter(multipath + "D2Multi.ini", false))
                {
                    s.WriteLine("[Settings]");
                    s.WriteLine("D2Folder=\"" + diapath + "\\" + "\"");
                    s.Close();
                }

                return true;
            }
            catch (Exception e) { log.addCError("CreateNewMultiIni(): " + e.Message); return false; }
        }

        public static Process StartD2Multi(string diapath, string title, Logging log)
        {
            try
            {
                if (CreateNewMultiIni(diapath, log))
                {
                    Process D2MultiProc = new Process();
                    D2MultiProc.StartInfo.FileName = multipath + "D2Multi.exe";
                    D2MultiProc.StartInfo.Arguments = "-w -ns -skiptobnet -title " + "\"" + title + "\"";
                    D2MultiProc.StartInfo.WorkingDirectory = multipath;
                    D2MultiProc.StartInfo.UseShellExecute = false;
                    D2MultiProc.EnableRaisingEvents = true;
                    bool Started = D2MultiProc.Start();
                    System.Threading.Thread.Sleep(3000); // need to make it a setting
                    if (Started)
                    {
                        Process[] children = ProcessExtensions.GetChildProcesses(D2MultiProc);
                        if (children.Length > 0)
                                return children[0];
                    }
                    return null;
                }
                return null;
            }
            catch (Exception e) { log.addCError("StartD2Multi(): " + e.Message); return null; }
        }
    }
}
