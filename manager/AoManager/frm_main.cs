﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace AoManager
{
    public partial class frm_main : Form
    {
        private frmAoSettings frmAoset;
        private frmBotSettings frmBotset;
        private frmPickitLogShow frmPickitLog;
        private AoManager manager;
        private Logging log;

        #region FormEvents

        public frm_main()
        {
            InitializeComponent();
            if (!File.Exists(AoSettings.xmlpath))
            {
                Properties.Settings.Default.IsAoSettings = false;
                Properties.Settings.Default.Save();
            }
            if (!File.Exists(BotSettings.xmlpath))
            {
                Properties.Settings.Default.IsBotSettings = false;
                Properties.Settings.Default.Save();
            }
            if (!Directory.Exists(Application.StartupPath + "\\logs\\AOManager"))
                try
                {
                    Directory.CreateDirectory(Application.StartupPath + "\\logs\\AOManager");
                }
                catch (Exception) { ;}

            frmPickitLog = new frmPickitLogShow();
            frmPickitLog.Hide();
            log = new Logging(Logging.LogPath + "AoManager.log", true);
            log.addSeparator();
            log.addInfo("AoManager loaded!");
            log.addSeparator();
        }

        private void frm_main_Shown(object sender, EventArgs e)
        {
        }  

        private void btn_botsettings_Click(object sender, EventArgs e)
        {
            if (frmBotset == null)
            {
                frmBotset = new frmBotSettings();
                frmBotset.BotSettingsChanged += new frmBotSettings.frmBotSettings_Handler(frmBotset_BotSettingsChanged);
                frmBotset.ShowDialog();
            }
        }

        private void btn_AoSettings_Click(object sender, EventArgs e)
        {
            if (frmAoset == null)
            {
                frmAoset = new frmAoSettings();
                frmAoset.AoSettingsSaved += new frmAoSettings.frmAoSettings_Handler(frmAoset_AoSettingsSaved);
                frmAoset.ShowDialog();
            }
        }


        void frmBotset_BotSettingsChanged(object sender, bool saved)
        {
            if (saved)
            {
                Properties.Settings.Default.IsBotSettings = true;
                Properties.Settings.Default.Save();
            }
            frmBotset = null;
        }

        void frmAoset_AoSettingsSaved(object sender, bool saved)
        {
            if (saved)
            {
                Properties.Settings.Default.IsAoSettings = true;
                Properties.Settings.Default.Save();
            }
            frmAoset = null;
        }


        private void frm_main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (manager != null)
                manager.Dispose();
            if (frmBotset != null)
                frmBotset.Dispose();
            if (frmAoset != null)
                frmAoset.Dispose();
            frmPickitLog.Dispose();
            this.Dispose();

            log.addSeparator();
            log.addInfo("AoManager exited!");
            log.addSeparator();
            Environment.Exit(0);
        }

        private void btn_Start_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.IsAoSettings && Properties.Settings.Default.IsBotSettings)
            {
                if (btn_Start.Text == "Start")
                    Start();
                else
                    Stop();
            }
            else
                MessageBox.Show("You haven't configured AoManager!", "Config Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btn_Items_Click(object sender, EventArgs e)
        {
            frmPickitLog.Show();
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowDia();
        }

        private void hideDiabloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideDia();
        }

        private void frm_main_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
                this.Hide();
        }

        private void trayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }


        private void showCoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (manager != null)
                manager.ShowCore();
        }

        private void hideCoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (manager != null)
                manager.HideCore();
        }

        private void btn_clearerrors_Click(object sender, EventArgs e)
        {
            if (listView_Error.Items.Count > 0)
                listView_Error.Items.Clear();
        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel lbl = (LinkLabel)sender;
            try
            {
                Process.Start(lbl.Text);
            }
            catch (Exception) { }
        }

        private void btn_showstat_Click(object sender, EventArgs e)
        {
            if (manager != null)
                manager.ShowStatusWindows();
        }

        private void btn_hidestat_Click(object sender, EventArgs e)
        {
            if (manager != null)
                manager.HideStatusWindows();
        }

        #endregion

        #region Controls

        private void Stop()
        {
            gb_Status.Enabled = false;
            manager.Dispose();
            manager = null;
            btn_Start.Text = "Start";
            MenuItemStart.Text = "Start";
            btn_AoSettings.Enabled = true;
            btn_BotSettings.Enabled = true;
            log.addSeparator();
            log.addInfo("AoManager stopped!");
            log.addSeparator();
        }

        private void Start()
        {
            if (File.Exists(AoSettings.xmlpath) && File.Exists(BotSettings.xmlpath))
            {
                gb_Status.Enabled = true;
                btn_Start.Text = "Stop";
                MenuItemStart.Text = "Stop";
                btn_AoSettings.Enabled = false;
                btn_BotSettings.Enabled = false;
                manager = new AoManager(BotSettings.GetSettings());
                //manager.AoManagerChanged += new GlobalEvent_Handler(manager_AoManagerChanged);
                manager.StartBots();
                log.addSeparator();
                log.addInfo("AoManager started!");
                log.addSeparator();
            }
            else
                MessageBox.Show("Can't locate files: \n" + AoSettings.xmlpath + "\n" + BotSettings.xmlpath + "\nplease reconfigure AoManager", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ShowDia()
        {
            if (manager != null)
                manager.ShowAllBots();
        }

        private void HideDia()
        {
           if (manager != null)
                manager.HideAllBots();
        }


        private void AddErrorToList(GlobalEventArgs.ErrorCode err, string profilename)
        {
            string[] str = new string[4];

            str[0] = err.ToString();
            if (err.Equals(GlobalEventArgs.ErrorCode.AoExited))
                str[1] = "The Awesom-O Process has been closed";
            else if (err.Equals(GlobalEventArgs.ErrorCode.AONoStart))
                str[1] = "Couldn't start Awesom-O.exe";
            else if (err.Equals(GlobalEventArgs.ErrorCode.ControllerNoConnect))
                str[1] = "AoManger cant connect to the Core";
            else if (err.Equals(GlobalEventArgs.ErrorCode.D2ErrorWin))
                str[1] = "Diablo 2 crashed, will be followed by D2Exited";
            else if (err.Equals(GlobalEventArgs.ErrorCode.D2Exited))
                str[1] = "The Diablo 2 Process has been closed";
            else if (err.Equals(GlobalEventArgs.ErrorCode.D2NoStart))
                str[1] = "Couldnt start Diablo 2";
            else if (err.Equals(GlobalEventArgs.ErrorCode.IpBan))
                str[1] = "Received Ip Ban";
            else if (err.Equals(GlobalEventArgs.ErrorCode.LoginTooLong))
                str[1] = "The battle.net login took too long";
            else if (err.Equals(GlobalEventArgs.ErrorCode.MaxGameTimer))
                str[1] = "The maximum in game time has exceeded";
            else if (err.Equals(GlobalEventArgs.ErrorCode.TooManyGamesThisHour))
                str[1] = "Created too many games in one hour,waiting";
            else if (err.Equals(GlobalEventArgs.ErrorCode.TooManyLoginAttemps))
                str[1] = "Too many login attempts";
            else if (err.Equals(GlobalEventArgs.ErrorCode.D2NoGrab))
                str[1] = "Couldn't find the Diablo process D2Multi was starting";
            else if (err.Equals(GlobalEventArgs.ErrorCode.JoinGameTooLong))
                str[1] = "Joining a game took too long!";
            else if (err.Equals(GlobalEventArgs.ErrorCode.CantConnectToBnet))
                str[1] = "Could not connect to bnet, pausing 5 minutes";
            else if (err.Equals(GlobalEventArgs.ErrorCode.KeyInUseOrInvalid))
                str[1] = "Cd Key in use, waiting";

            str[2] = DateTime.Now.ToShortTimeString();
            str[3] = profilename;
            ListViewItem itm = new ListViewItem(str);
            listView_Error.Items.Add(itm);
        }

        #endregion

        #region GlobalEvent
        private void manager_AoManagerChanged(object sender, GlobalEventArgs myArgs)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new GlobalEvent_Handler(manager_AoManagerChanged), new object[] { sender, myArgs });
                return;
            }

            switch (myArgs.Event)
            {
                case GlobalEventArgs.EventCode.Error:

                    if (myArgs.Error == GlobalEventArgs.ErrorCode.AoExited)
                    {
                        AddErrorToList(myArgs.Error, "All");
                        manager.StopBots();
                        Thread.Sleep(3000);
                        manager.StartBots();
                    }
                    else if (myArgs.Error == GlobalEventArgs.ErrorCode.AONoStart)
                    {
                        Stop();
                        AddErrorToList(myArgs.Error, "All");
                    }
                    else
                    {
                        DiaWatcher b = (DiaWatcher)sender;
                        AddErrorToList(myArgs.Error, b.ProfileName);
                    }

                break;

                case GlobalEventArgs.EventCode.Item:
                    if (myArgs.Item.ActionPerformed == Item.Action.Sold)
                        lbl_Sold.Text = (Convert.ToInt32(lbl_Sold.Text) + 1).ToString();
                    else if (myArgs.Item.ActionPerformed == Item.Action.Picked)
                        lbl_Picked.Text = (Convert.ToInt32(lbl_Picked.Text) + 1).ToString();
                    else if (myArgs.Item.ActionPerformed == Item.Action.Stashed)
                        lbl_Stashed.Text = (Convert.ToInt32(lbl_Stashed.Text) + 1).ToString();
                    else if (myArgs.Item.ActionPerformed == Item.Action.Gambled)
                        lbl_Gambled.Text = (Convert.ToInt32(lbl_Gambled.Text) + 1).ToString();
                    DiaWatcher bw = (DiaWatcher)sender;
                    frmPickitLog.AddToList(myArgs.Item, bw.ProfileName);
                    break;

               
            }
        }

        #endregion      

        private void gb_Items_Enter(object sender, EventArgs e)
        {

        }


    }
}
