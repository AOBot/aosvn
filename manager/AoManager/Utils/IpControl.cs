﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Net.Configuration;
using System.Configuration;
using System.Reflection;

namespace AoManager
{
    static public class IpControl
    {
       //public static bool SetAllowUnsafeHeaderParsing20()
       //{
       //   //Get the assembly that contains the internal class
       //   Assembly aNetAssembly = Assembly.GetAssembly(typeof(System.Net.Configuration.SettingsSection));
       //   if (aNetAssembly != null)
       //   {
       //      //Use the assembly in order to get the internal type for the internal class
       //      Type aSettingsType = aNetAssembly.GetType("System.Net.Configuration.SettingsSectionInternal");
       //      if (aSettingsType != null)
       //      {
       //         //Use the internal static property to get an instance of the internal settings class.
       //         //If the static instance isn't created allready the property will create it for us.
       //         object anInstance = aSettingsType.InvokeMember("Section",
       //           BindingFlags.Static | BindingFlags.GetProperty | BindingFlags.NonPublic, null, null, new object[] { });

       //         if (anInstance != null)
       //         {
       //            //Locate the private bool field that tells the framework is unsafe header parsing should be allowed or not
       //            FieldInfo aUseUnsafeHeaderParsing = aSettingsType.GetField("useUnsafeHeaderParsing", BindingFlags.NonPublic | BindingFlags.Instance);
       //            if (aUseUnsafeHeaderParsing != null)
       //            {
       //               aUseUnsafeHeaderParsing.SetValue(anInstance, true);
       //               return true;
       //            }
       //         }
       //      }
       //   }
       //   return false;
       //}

        static public bool ChangeIP()
        {
           //SetAllowUnsafeHeaderParsing20();
            if (Properties.Settings.Default.IsIpSettings)
            {
                IpChangerStruct config = IpChanger.GetSettings();
                if (config != null)
                {
                   if (!string.IsNullOrEmpty(config.ip.ToString()) &&
                      !string.IsNullOrEmpty(config.login.ToString()) &&
                      !string.IsNullOrEmpty(config.disconnect.ToString()) &&
                      (!string.IsNullOrEmpty(config.connect.ToString()) || config.autoReconnect) &&
                      (string.IsNullOrEmpty(config.preAuthUrl.ToString()) && !config.preAuth) &&
                      ((!string.IsNullOrEmpty(config.challengeSearch.ToString()) && !string.IsNullOrEmpty(config.challengeUrl.ToString())) || !config.challenge))
                   {
                      HttpWebRequest req = null;
                      CookieContainer ccon = new CookieContainer();
                      //WebHeaderCollection headers = new WebHeaderCollection();
                      StreamReader sr;
                      Stream dataStream = null;
                      string res_str = "";
                      string buffer_str = "";
                      byte[] bytes;
                      string loginUrl = "";
                      string searchVal = "";
                      string challenge = "";

                      // PreAuth?
                      try
                      {
                         if (config.preAuth)
                         {
                            req = (HttpWebRequest)WebRequest.Create("http://" + config.ip.ToString() + "/" + config.preAuthUrl.ToString().ToString());
                            req.CookieContainer = ccon;
                            //req.AllowAutoRedirect = false;
                            req.KeepAlive = true;
                            req.ProtocolVersion = HttpVersion.Version10;
                            req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6";
                            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                            req.Headers.Add("Accept-Language: en-us,en;q=0.5");
                            req.Headers.Add("Accept-Encoding: gzip,deflate");
                            req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                            //req.Timeout = 300;
                            req.Method = "GET";
                         }
                      }
                      catch (Exception )
                      {
                         //Logging.WriteError(e.Message);
                      }

                      // Send the connect request!
                      try
                      {
                         if (config.preAuth)
                         {
                            if (req != null)
                            {
                               HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                               if (res == null)
                               {
                                  throw new Exception("The Request did not return a Response! (NULL)");
                               }
                               else
                               {
                                  foreach (Cookie cookie in res.Cookies)
                                  {
                                     ccon.Add(cookie);
                                  }
                                  //sr = new StreamReader(res.GetResponseStream());
                                  //res_str = sr.ReadToEnd().Trim(); // Fuck that?
                                  res.Close();
                               }
                            }
                            else
                            {
                               throw new Exception("Connect Request is fucked up!");
                            }
                         }
                      }
                      catch (Exception )
                      {
                         //Logging.WriteError(e.Message);
                      }


                      // Get the challenge value if necessary
                      try
                      {
                         if (config.challenge)
                         {
                            if (!string.IsNullOrEmpty(config.challengeSearch))
                            {
                               req = (HttpWebRequest)WebRequest.Create("http://" + config.ip.ToString() + "/" + config.challengeUrl.ToString());
                               req.CookieContainer = ccon;
                               //req.AllowAutoRedirect = false;
                               req.KeepAlive = true;
                               req.ProtocolVersion = HttpVersion.Version10;
                               req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6";
                               req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                               req.Headers.Add("Accept-Language: en-us,en;q=0.5");
                               req.Headers.Add("Accept-Encoding: gzip,deflate");
                               req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                               //req.Timeout = 300;
                               req.Method = "GET";
                            }
                            else
                            {
                               throw new Exception("You did not specify a URL to search the Challenge value!");
                            }

                            if (req != null)
                            {
                               HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                               if (res == null)
                               {
                                  return false;
                               }
                               else
                               {
                                  foreach (Cookie cookie in res.Cookies)
                                  {
                                     ccon.Add(cookie);
                                  }
                                  sr = new StreamReader(res.GetResponseStream());
                                  res_str = sr.ReadToEnd().Trim();
                                  //Console.Write(res_str); // TODO: Remove
                                  res.Close();
                               }
                            }

                            if (!string.IsNullOrEmpty(res_str))
                            {
                               try
                               {
                                  searchVal = config.challengeSearch.ToString();
                                  challenge = res_str.Substring(res_str.IndexOf(searchVal) + searchVal.Length, res_str.IndexOf('"', (res_str.IndexOf(searchVal) + searchVal.Length)) - (res_str.IndexOf(searchVal) + searchVal.Length));
                               }
                               catch (Exception )
                               {
                                  //Logging.WriteError(e.Message);
                               }
                            }

                            else
                            {
                               throw new Exception("The HttpWebRequest seems to be NULL!");
                            }
                         }
                      }
                      catch (Exception )
                      {
                         //Logging.WriteError(e.Message);
                      }
                      finally
                      {
                         if (dataStream != null)
                         {
                            dataStream.Close();
                         }
                      }

                      // Going to clear up the login string (variables)
                      try
                      {
                         if (!string.IsNullOrEmpty(config.login.ToString()))
                         {
                            loginUrl = config.login.ToString();
                            loginUrl = loginUrl.Replace("%username%", config.username.ToString());
                            loginUrl = loginUrl.Replace("%password%", config.password.ToString());
                            loginUrl = loginUrl.Replace("%challenge%", challenge);

                            System.Security.Cryptography.MD5CryptoServiceProvider csp = new System.Security.Cryptography.MD5CryptoServiceProvider();

                            while (loginUrl.ToLower().Contains("md5("))
                            {
                               buffer_str = loginUrl.Substring(loginUrl.ToLower().LastIndexOf("md5(") + 4, loginUrl.ToLower().IndexOf(")", (loginUrl.ToLower().LastIndexOf("md5(") + 4)) - (loginUrl.ToLower().LastIndexOf("md5(") + 4));
                               bytes = System.Text.Encoding.UTF8.GetBytes(buffer_str);
                               bytes = csp.ComputeHash(bytes);
                               StringBuilder s = new StringBuilder();
                               foreach (byte b in bytes)
                               {
                                  s.Append(b.ToString("x2").ToLower());
                               }
                               loginUrl = loginUrl.Replace(loginUrl.Substring(loginUrl.ToLower().LastIndexOf("md5("), loginUrl.ToLower().IndexOf(")", (loginUrl.ToLower().LastIndexOf("md5("))) - (loginUrl.ToLower().LastIndexOf("md5(") - 1)), s.ToString());
                            }
                         }
                         else
                         {
                            throw new Exception("Response stream is empty!");
                         }
                      }
                      catch (Exception )
                      {
                         //Logging.WriteError(e.Message);
                      }

                      // Now let's login to the site
                      try
                      {
                         if (!string.IsNullOrEmpty(loginUrl))
                         {
                            if (config.post && loginUrl.Contains("?"))
                            {
                               req = (HttpWebRequest)WebRequest.Create("http://" + config.ip.ToString() + "/" + loginUrl.Split('?')[0].ToString());
                               req.CookieContainer = ccon;
                               //req.AllowAutoRedirect = false;
                               req.KeepAlive = true;
                               req.ProtocolVersion = HttpVersion.Version10;
                               req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6";
                               req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                               req.Headers.Add("Accept-Language: en-us,en;q=0.5");
                               req.Headers.Add("Accept-Encoding: gzip,deflate");
                               req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                               //req.Timeout = 300;
                               req.Method = "POST";
                               req.ContentType = "application/x-www-form-urlencoded";
                               req.ContentLength = loginUrl.Split('?')[1].ToString().Length;
                               //req.ServicePoint.Expect100Continue = false;

                               bytes = Encoding.ASCII.GetBytes(loginUrl.Split('?')[1].ToString());
                               dataStream = req.GetRequestStream();
                               dataStream.Write(bytes, 0, bytes.Length);
                               //req.ServicePoint.Expect100Continue = false;
                            }
                            else
                            {
                               req = (HttpWebRequest)WebRequest.Create("http://" + config.ip.ToString() + "/" + loginUrl.ToString());
                               req.CookieContainer = ccon;
                               //req.AllowAutoRedirect = false;
                               req.KeepAlive = true;
                               req.ProtocolVersion = HttpVersion.Version10;
                               req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6";
                               req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                               req.Headers.Add("Accept-Language: en-us,en;q=0.5");
                               req.Headers.Add("Accept-Encoding: gzip,deflate");
                               req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                               //req.Timeout = 300;
                               req.Method = "GET";
                            }
                         }
                         else
                         {
                            throw new Exception("Login string is fucked up!");
                         }
                      }
                      catch (Exception )
                      {
                         //Logging.WriteError(e.Message);
                      }
                      finally
                      {
                         if (dataStream != null)
                         {
                            dataStream.Close();
                         }
                      }

                      // Send the login request!
                      try
                      {
                         if (req != null)
                         {
                            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                            if (res == null)
                            {
                               throw new Exception("The Request did not return a Response! (NULL)");
                            }
                            else
                            {
                               foreach (Cookie cookie in res.Cookies)
                               {
                                  ccon.Add(cookie);
                               }
                               //sr = new StreamReader(res.GetResponseStream());
                               //res_str = sr.ReadToEnd().Trim(); // Fuck that?
                               //Console.Write(res_str); // TODO: Remove!
                               res.Close();
                            }
                         }
                         else
                         {
                            throw new Exception("Login Request is fucked up!");
                         }
                      }
                      catch (Exception )
                      {
                         //Logging.WriteError(e.Message);
                      }

                      // We should now be albe to restart the connection!
                      try
                      {
                         if (!string.IsNullOrEmpty(config.disconnect.ToString()))
                         {
                            if (config.postDisconnect && config.disconnect.ToString().Contains("?"))
                            {
                               req = (HttpWebRequest)WebRequest.Create("http://" + config.ip.ToString() + "/" + config.disconnect.ToString().Split('?')[0].ToString());
                               req.CookieContainer = ccon;
                               //req.AllowAutoRedirect = false;
                               req.KeepAlive = true;
                               req.ProtocolVersion = HttpVersion.Version10;
                               req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6";
                               req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                               req.Headers.Add("Accept-Language: en-us,en;q=0.5");
                               req.Headers.Add("Accept-Encoding: gzip,deflate");
                               req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                               //req.Timeout = 300;
                               req.Method = "POST";
                               req.ContentType = "application/x-www-form-urlencoded";
                               req.ContentLength = config.disconnect.ToString().Split('?')[1].ToString().Length;
                               //req.ServicePoint.Expect100Continue = false;

                               bytes = Encoding.ASCII.GetBytes(config.disconnect.ToString().Split('?')[1].ToString());
                               dataStream = req.GetRequestStream();
                               dataStream.Write(bytes, 0, bytes.Length);
                               //req.ServicePoint.Expect100Continue = false;
                            }
                            else
                            {
                               req = (HttpWebRequest)WebRequest.Create("http://" + config.ip.ToString() + "/" + config.disconnect.ToString().ToString());
                               req.CookieContainer = ccon;
                               //req.AllowAutoRedirect = false;
                               req.KeepAlive = true;
                               req.ProtocolVersion = HttpVersion.Version10;
                               req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6";
                               req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                               req.Headers.Add("Accept-Language: en-us,en;q=0.5");
                               req.Headers.Add("Accept-Encoding: gzip,deflate");
                               req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                               //req.Timeout = 300;
                               req.Method = "GET";
                            }
                         }
                         else
                         {
                            throw new Exception("Disconnect string is fucked up!");
                         }
                      }
                      catch (Exception )
                      {
                         //Logging.WriteError(e.Message);
                      }

                      // Send the disconnect request!
                      try
                      {
                         if (req != null)
                         {
                            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                            if (res == null)
                            {
                               throw new Exception("The Request did not return a Response! (NULL)");
                            }
                            else
                            {
                               foreach (Cookie cookie in res.Cookies)
                               {
                                  ccon.Add(cookie);
                               }
                               //sr = new StreamReader(res.GetResponseStream());
                               //res_str = sr.ReadToEnd().Trim(); // Fuck that?
                               //Console.Write(res_str); // TODO: Remove!
                               res.Close();
                            }
                         }
                         else
                         {
                            throw new Exception("Disconnect Request is fucked up!");
                         }
                      }
                      catch (Exception )
                      {
                         //Logging.WriteError(e.Message);
                      }

                      if (!config.autoReconnect)
                      {
                         // We should now be albe to restart the connection!
                         try
                         {
                            if (!string.IsNullOrEmpty(config.connect.ToString()))
                            {
                               if (config.postReconnect && config.connect.ToString().Contains("?"))
                               {
                                  req = (HttpWebRequest)WebRequest.Create("http://" + config.ip.ToString() + "/" + config.connect.ToString().Split('?')[0].ToString());
                                  req.CookieContainer = ccon;
                                  //req.AllowAutoRedirect = false;
                                  req.KeepAlive = true;
                                  req.ProtocolVersion = HttpVersion.Version10;
                                  req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6";
                                  req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                                  req.Headers.Add("Accept-Language: en-us,en;q=0.5");
                                  req.Headers.Add("Accept-Encoding: gzip,deflate");
                                  req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                                  //req.Timeout = 300;
                                  req.Method = "POST";
                                  req.ContentType = "application/x-www-form-urlencoded";
                                  req.ContentLength = config.connect.ToString().Split('?')[1].ToString().Length;
                                  //req.ServicePoint.Expect100Continue = false;

                                  bytes = Encoding.ASCII.GetBytes(config.connect.ToString().Split('?')[1].ToString());
                                  dataStream = req.GetRequestStream();
                                  dataStream.Write(bytes, 0, bytes.Length);
                                  //req.ServicePoint.Expect100Continue = false;
                               }
                               else
                               {
                                  req = (HttpWebRequest)WebRequest.Create("http://" + config.ip.ToString() + "/" + config.connect.ToString().ToString());
                                  req.CookieContainer = ccon;
                                  //req.AllowAutoRedirect = false;
                                  req.KeepAlive = true;
                                  req.ProtocolVersion = HttpVersion.Version10;
                                  req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6";
                                  req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                                  req.Headers.Add("Accept-Language: en-us,en;q=0.5");
                                  req.Headers.Add("Accept-Encoding: gzip,deflate");
                                  req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                                  //req.Timeout = 300;
                                  req.Method = "GET";
                               }
                            }
                            else
                            {
                               throw new Exception("Connect string is fucked up!");
                            }
                         }
                         catch (Exception )
                         {
                            //Logging.WriteError(e.Message);
                         }

                         // Send the connect request!
                         try
                         {
                            if (req != null)
                            {
                               HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                               if (res == null)
                               {
                                  throw new Exception("The Request did not return a Response! (NULL)");
                               }
                               else
                               {
                                  foreach (Cookie cookie in res.Cookies)
                                  {
                                     ccon.Add(cookie);
                                  }
                                  //sr = new StreamReader(res.GetResponseStream());
                                  //res_str = sr.ReadToEnd().Trim(); // Fuck that?
                                  //Console.Write(res_str); // TODO: Remove!
                                  res.Close();
                               }
                            }
                            else
                            {
                               throw new Exception("Connect Request is fucked up!");
                            }
                         }
                         catch (Exception )
                         {
                            //Logging.WriteError(e.Message);
                         }
                      }
                   }
                   else
                   {
                      System.Windows.Forms.MessageBox.Show("You did not specify all necessary values!");
                   }
                }
            }
            return false;
        }
    }
}
