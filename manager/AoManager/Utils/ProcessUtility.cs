﻿
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System;

    
    /* thx to lord2800 for these ones:
        http://code.assembla.com/d2bs/subversion/nodes/loader?rev=929*/

    public static class NTDll
    {
        [DllImport("ntdll.dll")]
        private static extern int NtQueryInformationProcess(IntPtr hProcess, int processInformationClass, ref PROCESS_BASIC_INFORMATION processBasicInformation, uint processInformationLength, out uint returnLength);
        public static bool ProcessIsChildOf(Process parent, Process child)
        {
            PROCESS_BASIC_INFORMATION pbi = new PROCESS_BASIC_INFORMATION();
            try
            {
                uint bytesWritten;
                NtQueryInformationProcess(child.Handle, 0, ref pbi, (uint)Marshal.SizeOf(pbi), out bytesWritten);
                if (pbi.InheritedFromUniqueProcessId == parent.Id)
                    return true;
            }
            catch (Exception e) { if (e.Message == "r") { }  return false; }
            return false;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PROCESS_BASIC_INFORMATION
    {
        public int ExitStatus;
        public int PebBaseAddress;
        public int AffinityMask;
        public int BasePriority;
        public uint UniqueProcessId;
        public uint InheritedFromUniqueProcessId;
    }

    // had to change this since .net 2.0 does not support any extensions
    static class ProcessExtensions
    {
        public static Process[] GetChildProcesses(Process process)
        {
            List<Process> children = new List<Process>();
            Process[] processes = Process.GetProcesses();
            foreach (Process p in processes)
            {
                if (NTDll.ProcessIsChildOf(process, p))
                    children.Add(p);
            }
            return children.ToArray();
        }
    }


