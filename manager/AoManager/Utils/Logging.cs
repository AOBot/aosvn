﻿/*
    MansClasses, free classes library.
    Copyright (C) 2008  Marc André 'Manhim' Audet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * 
 * Main Version: 1.0
 * Release: 1.0 Beta 3
 * 
 * Version: 1.1
 * 
 * */

using System;
using System.IO;
using System.Windows.Forms;

    namespace AoManager
    {
        /// <summary>
        /// Version 1.1
        /// </summary>
       
        public class Logging
        {
            public static string LogPath = Application.StartupPath + "\\logs\\AOManager\\";
            private String _logFile;
            private String _separator;

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="logFile">The name of the log file (Must include full path)</param>
            /// <remarks>It will not check and delete if there was a file with that name already. It will write at the end of it.</remarks>
            public Logging(String logFile)
                : this(logFile, false)
            {
            }

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="logFile">The name of the log file (Must include full path)</param>
            /// <param name="deleteLogFile">Decide to check if there is already a file with that name. If there is, it is deleted.</param>
            public Logging(String logFile, bool deleteLogFile)
            {
                this._logFile = logFile;

                this._separator = "=======================================================================";

                if (System.IO.File.Exists(logFile) && deleteLogFile)
                {
                    System.IO.File.Delete(logFile);
                }
            }

            /// <summary>
            /// Change the current separator.
            /// </summary>
            /// <param name="separator">The new separator</param>
            public void setSeparator(String separator)
            {
                this._separator = separator;
            }

            private bool _add(String line)
            {
                if (!Directory.Exists(Path.GetDirectoryName(this._logFile)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(this._logFile));
                }

                try
                {
                    File.AppendAllText(this._logFile, line + Environment.NewLine);
                }
                catch (Exception)
                {
                    return false;
                }

                return true;
            }

            private String _currentTime()
            {
                DateTime now = DateTime.Now;

                bool isPm = false;
                int hour;
                String partOfDay = "";

                if (now.Hour > 12)
                {
                    isPm = true;
                    hour = now.Hour - 12;
                }
                else
                {
                    hour = now.Hour;
                }

                if (isPm == true)
                {
                    partOfDay = "PM";
                }
                else
                {
                    partOfDay = "AM";
                }

                return now.Month.ToString().PadLeft(2, '0') + "/" + now.Day.ToString().PadLeft(2, '0') + "/" + now.Year.ToString() + " " + now.Hour.ToString().PadLeft(2, '0') + ":" + now.Minute.ToString().PadLeft(2, '0') + ":" + now.Second.ToString().PadLeft(2, '0') + " " + partOfDay;
            }

            /// <summary>
            /// Add a separator to the log file.
            /// </summary>
            /// <returns>Returns if the line was succefuly added are not.</returns>
            public bool addSeparator()
            {
                String line = this._separator;

                if (this._add(line))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Add a custom line to the log file.
            /// </summary>
            /// <param name="content">The content</param>
            /// <returns>Returns if the line was succefuly added are not.</returns>
            public bool addCustom(String content)
            {
                String line = this._currentTime() + " : " + content;

                if (this._add(line))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Add a Info line to the log file.
            /// </summary>
            /// <param name="content">The content</param>
            /// <returns>Returns if the line was succefuly added are not.</returns>
            public bool addInfo(String content)
            {
                String line = this._currentTime() + " : Info : " + content;

                if (this._add(line))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Add a Debug line to the log file.
            /// </summary>
            /// <param name="content">The content</param>
            /// <returns>Returns if the line was succefuly added are not.</returns>
            public bool addDebug(String content)
            {
                String line = this._currentTime() + " : Debug : " + content;

                if (this._add(line))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Add a Warning line to the log file.
            /// </summary>
            /// <param name="content">The content</param>
            /// <returns>Returns if the line was succefuly added are not.</returns>
            public bool addWarning(String content)
            {
                String line = this._currentTime() + " : Warning : " + content;

                if (this._add(line))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Add a Error line to the log file.
            /// </summary>
            /// <param name="content">The content</param>
            /// <returns>Returns if the line was succefuly added are not.</returns>
            public bool addError(String content)
            {
                String line = this._currentTime() + " : Error : " + content;

                if (this._add(line))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Add a Critical Error line to the log file.
            /// </summary>
            /// <param name="content">The content</param>
            /// <returns>Returns if the line was succefuly added are not.</returns>
            public bool addCError(String content)
            {
                String line = this._currentTime() + " : CRITICAL ERROR! : " + content;

                if (this._add(line))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

