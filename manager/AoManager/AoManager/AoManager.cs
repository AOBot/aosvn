﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Security.Permissions;

[assembly: PermissionSet(SecurityAction.RequestMinimum, Name = "FullTrust")]
namespace AoManager
{

    internal class AoManager : IDisposable
    {
        private bool AllowEvents = true;
        private bool exited = false;
        private List<DiaWatcher> Bots = new List<DiaWatcher>();
        //private Queue<DiaWatcher> NextBot = new Queue<DiaWatcher>();
        private AoSettingStruct AoConfig;
        private Process AobotProc;
        private Thread StarterThread;
        private WinHider Hider;
        private Logging log;
        

        public AoManager(BotSetting config)
        {
            this.AoConfig = AoSettings.GetSettings();

            foreach (Profile p in config.Profiles)
            {
                DiaWatcher b = new DiaWatcher(config, p, this.AoConfig);
                Bots.Add(b);
            }

            log = new Logging(Logging.LogPath + "AoWatcher.log", true);

            ArchiveCoreLogs();

        }

        #region Controls

        public void StartBots()
        {
            StarterThread = new Thread(new ThreadStart(this.StartThread));
            StarterThread.Start();
        }

        private void StartThread()
        {
            try
            {
                AllowEvents = true;
                exited = false;

                if (!AoConfig.AoNoStart)
                {
                    if (!RunAO())
                    {
                        log.addCError("FATAL ERROR: Cant start Awesom-O");
                        //AoManagerChanged(this, new GlobalEventArgs(GlobalEventArgs.EventCode.Error, GlobalEventArgs.ErrorCode.AONoStart));
                        return;
                    }

                    Hider = new WinHider(AobotProc.MainWindowHandle, AoConfig.HideCore);
                }

                for (int i = 0; i < Bots.Count; i++ )
                {
                    Bots[i].NextAction.Enqueue(new ADiaConConnect());
                    //if (i != 0)
                        //Bots[i].NextAction.Enqueue(new APause(i * Properties.Settings.Default.LauncherStartDelay,"LauncherStartDelay"));
                    //master = Ao.players:findByPlayerName(Settings.Bot.Master[i])Bots[i].NextAction.Enqueue(new ADiaStart());
                }
               

                while (true)
                {
                    foreach (DiaWatcher d in Bots)
                    {
                        d.Tick();
                        Thread.Sleep(10);
                    }

                    if (CheckErrors() || exited)
                    {
                        foreach (DiaWatcher d in Bots)
                        {
                            d.DisConnectController();
                            d.StopDiablo();
                            d.NextError.Clear();
                            d.NextAction.Clear();
                        }
                        break;
                    }
                    Thread.Sleep(100);
                }

            }
            catch (Exception) { }

            //Ao exited
            Thread.Sleep(4000);
            StartThread();
            
            /*if (Bots.Count > 0)
            {
                if (Properties.Settings.Default.UseD2Launcher && Bots.Count > 1)
                {
                    foreach (BotWatcher bot in Bots)
                    {
                        //bot.Start(true);
                        //Thread.Sleep(Properties.Settings.Default.LauncherStartDelay);
                        NextBot.Enqueue(bot);
                    }
                    NextBot.Dequeue().Start(true);
                }
                else
                {
                    Bots[0].Start(true);
                }
            }*/

        }

        public void StopBots()
        {
            AllowEvents = false;

            //NextBot.Clear();

            try
            {
                if (StarterThread != null)
                {
                    if (StarterThread.IsAlive)
                    {
                        StarterThread.Abort();
                        StarterThread.Join();
                    }
                }
            }
            catch (Exception) { }

            if (Bots.Count > 0)
            {
                if (Bots.Count > 1)
                {
                    foreach (DiaWatcher bot in Bots)
                    {
                        bot.DisConnectController();
                        bot.StopDiablo();
                    }
                }
                else
                {
                    Bots[0].DisConnectController();
                    Bots[0].StopDiablo();
                }
            }

            if (Hider != null)
                Hider.Dispose();

            if (AobotProc != null)
            {
                if (!AobotProc.HasExited)
                    KillAO();
                AobotProc.Dispose();
            }
            AobotProc = null;

        }

        public void ShowAllBots()
        {
            foreach (DiaWatcher bot in Bots)
            {
                bot.ShowDiablo();
            }
        }

        public void HideAllBots()
        {
            foreach (DiaWatcher bot in Bots)
            {
                bot.HideDiablo();
            }
        }

        public void ShowCore()
        {
            if (Hider != null)
                Hider.UnHideWindow();
        }

        public void HideCore()
        {
            if (Hider != null)
                Hider.HideWindow();
        }

        public void ShowStatusWindows()
        {
            foreach (DiaWatcher bot in Bots)
            {
                bot.ShowStatus();
            }
        }

        public void HideStatusWindows()
        {
            foreach (DiaWatcher bot in Bots)
            {
                bot.HideStatus();
            }
        }

        #endregion

        #region Events


        private void AobotProc_Exited(object sender, EventArgs e)
        {
            log.addError("Awesom-O has exited");
            if (AllowEvents)
                exited = true;
        }

        #endregion

        private bool CheckErrors()
        {
          
                WindowFuncs windowfuncs = new WindowFuncs();
                if (AobotProc != null)
                {
                    try
                    {
                            if (!AobotProc.HasExited && !AobotProc.Responding)
                            {
                                KillAO();
                                return true;
                            }
                            if (windowfuncs.WinExistsByName("Awesom-O.exe"))
                            {
                                windowfuncs.CloseWindowByName("Awesom-O.exe");
                                windowfuncs.KillWindowByName("Awesom-O.exe");
                                return true;
                            }
                    }
                    catch (Exception e) { log.addCError("AoErrorWorker: " + e.Message); return false; }
                 }
                return false;
        }


        private bool RunAO()
        {
            KillAO();
            //
            try
            {
                AobotProc = new Process();
                AobotProc.StartInfo.FileName = AoConfig.AOPath + "\\Awesom-O.exe";
                AobotProc.StartInfo.Arguments = AoConfig.AoFlags;
                AobotProc.StartInfo.WorkingDirectory = AoConfig.AOPath;
                AobotProc.StartInfo.ErrorDialog = false;
                AobotProc.EnableRaisingEvents = true;
                bool Started = AobotProc.Start();
                //AobotProc.EnablePrivilege(Privilege.TakeOwnership);
                AobotProc.Exited += new EventHandler(AobotProc_Exited);
                //
                Thread.Sleep(AoConfig.StartDelay);
                //
                return Started;
            }
            catch (Exception e) { log.addError(e.Message); AobotProc = null; return false; }
        }

        private void KillAO()
        {
            if (AobotProc != null)
            {
                try { AobotProc.Kill(); }
                catch (Exception e) { log.addError("KillAo: " + e.Message); }
            }
            else
            {
                try
                {
                    Process[] ao = Process.GetProcessesByName("Awesom-O");
                    if (ao.Length > 0)
                    {
                        foreach (Process p in ao)
                        {  
                            p.Kill();
                        }
                    }
                }
                catch (Exception e) { log.addError("KillAo by name: " + e.Message); }
            }
        }

        private void ArchiveCoreLogs()
        {
            try
            {
                if (!Directory.Exists(AoConfig.AOPath + "\\logs"))
                    Directory.CreateDirectory(AoConfig.AOPath + "\\logs");

                if (!Directory.Exists(AoConfig.AOPath + "\\logs\\archive"))
                    Directory.CreateDirectory(AoConfig.AOPath + "\\logs\\archive");

                string i = DateTime.Now.ToString("d/MM/yyyy/hh/mm");

                if (File.Exists(AoConfig.AOPath + "\\core.log"))
                    File.Delete(AoConfig.AOPath + "\\core.log");
                //File.Move(AoConfig.AOPath + "\\core.log", AoConfig.AOPath + "\\logs\\archive\\" + "core " + i + ".log");

                foreach (string s in Directory.GetFiles(AoConfig.AOPath + "\\logs\\"))
                {
                    File.Delete(s);
                    //File.Move(s, AoConfig.AOPath + "\\logs\\archive\\" + Path.GetFileName(s) + DateTime.Now.ToString() + " "+ DateTime.Now.ToLongTimeString() + ".log");
                }

            }
            catch (Exception e) { string i = e.Message; }
        }
        
        public void Dispose()
        {
            StopBots();

            foreach (DiaWatcher bot in Bots)
            {
                bot.Dispose();
            }
        }
    }

    #region GlobalEvent

    public delegate void GlobalEvent_Handler(object sender, GlobalEventArgs myArgs);

    public class GlobalEventArgs : EventArgs
    {
        private EventCode _event;
        private ErrorCode _error;
        private Item _item;
        private BotStatistic _bstat;
        private string _message;

        public GlobalEventArgs(EventCode _e)
        {
            this._event = _e;
        }

        public GlobalEventArgs(EventCode _e, ErrorCode _er)
        {
            this._event = _e;
            this._error = _er;
        }

        public GlobalEventArgs(EventCode _e, Item _i)
        {
            this._event = _e;
            this._item = _i;
        }

        public GlobalEventArgs(EventCode _e, BotStatistic _b)
        {
            this._event = _e;
            this._bstat = _b;
        }

        public GlobalEventArgs(EventCode _e, string _m)
        {
            this._event = _e;
            this._message = _m;
        }

        public EventCode Event
        {
            get { return _event; }
        }

        public ErrorCode Error
        {
            get { return _error; }
        }

        public Item Item
        {
            get { return _item; }
        }

        public string Message
        {
            get { return _message; }
        }

        public BotStatistic Bstat
        {
            get { return _bstat; }
        }

        public enum EventCode
        {
            Error,
            KeyChange,
            Item,
            BotConnected
        }

        public enum ErrorCode
        {
            D2NoStart,
            D2NoGrab,
            D2Exited,
            AONoStart,
            AoExited,
            IpBan,
            LoginTooLong,
            JoinGameTooLong,
            ControllerNoConnect,
            D2ErrorWin,
            MaxGameTimer,
            TooManyGamesThisHour,
            TooManyLoginAttemps,
            CantConnectToBnet,
            ControllerSocketError,
            KeyInUseOrInvalid,
        }
    }
    #endregion
}