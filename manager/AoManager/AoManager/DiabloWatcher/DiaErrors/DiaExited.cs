﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaExited : IDiaError
    {
        public string ErrorDescription()
        {
            return "Diablo II has exited... restarting Diablo II";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.NextAction.Enqueue(new APause(4000, "Restart delay!"));
            b.NextAction.Enqueue(new ADiaStart());
        }
    }
}
