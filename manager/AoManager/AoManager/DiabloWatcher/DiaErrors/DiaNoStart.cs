﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaNoStart : IDiaError
    {
        public string ErrorDescription()
        {
            return "Can't start Diablo! Pausing Bot for 5 minutes.";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(60 * 50000, ErrorDescription()));
            b.NextAction.Enqueue(new ADiaStart());
        }
    }
}
