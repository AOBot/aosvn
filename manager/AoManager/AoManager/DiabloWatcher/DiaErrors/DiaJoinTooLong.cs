﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaJoinTooLong : IDiaError
    {
        public string ErrorDescription()
        {
            return "Joining a game took too long!";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(10000, ErrorDescription()));
            b.NextAction.Enqueue(new ADiaStart());
        }
    }
}
