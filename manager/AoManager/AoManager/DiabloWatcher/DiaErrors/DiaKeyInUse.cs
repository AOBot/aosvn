﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaKeyInUse : IDiaError
    {
        public string ErrorDescription()
        {
            return "Cd Key in use, pausing";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(b.Delays.CdKeyInUseDelay * 60 * 1000, ErrorDescription()));
            b.NextAction.Enqueue(new ADiaStart());
            
        }
    }
}
