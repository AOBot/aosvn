﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaErrorWin: IDiaError
    {
        private string error;

        public DiaErrorWin(string error)
        {
            this.error = "Diablo Error window: " + error;
        }

        public string ErrorDescription()
        {
            return this.error;
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(4000, "Restart delay!"));
            b.NextAction.Enqueue(new ADiaStart());
        }

    }

    internal class DiaErrorWinNames
    {
         List<String> windownames;
         string diawintitle;
         
         public DiaErrorWinNames(string diawintitle)
         {
             this.diawintitle = diawintitle;
         }

         public List<String> WindowNames
         {
             get
             {
                 windownames = new List<string>();
                 windownames.Add("End Program - " + diawintitle);
                 windownames.Add("Diablo II Exception");
                 windownames.Add("Hey guys");
                 windownames.Add("D2 Exception");
                 windownames.Add("Diablo II Error");
                 windownames.Add("Game");
                 windownames.Add(diawintitle + ": Diablo II.exe - Application Error");
                 windownames.Add("Diablo II Critical Error");
                 return windownames;   
             }
         }

    }

}
