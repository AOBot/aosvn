﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaCSocketError : IDiaError
    {
        public string ErrorDescription()
        {
            return "Controller Socket Error!";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.NextError.Clear();
            b.DisConnectController();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(4000, "Restart delay!"));
            b.NextAction.Enqueue(new ADiaConConnect());
            b.NextAction.Enqueue(new ADiaStart());
        }
    }
}
