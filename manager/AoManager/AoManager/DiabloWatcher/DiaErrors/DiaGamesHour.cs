﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaGamesHour : IDiaError
    {
        int remainingtime;

        public DiaGamesHour(int remainingtime)
        {
            this.remainingtime = remainingtime;
        }

        public string ErrorDescription()
        {
            return "Created too many games in one hour,waiting";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(remainingtime, ErrorDescription()));
            b.NextAction.Enqueue(new ADiaStart());
        }
    }
}
