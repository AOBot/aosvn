﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace AoManager
{
    internal class DiaIpBan : IDiaError
    {
        string runonban;

        public DiaIpBan(string runonban)
        {
            this.runonban = runonban;
        }

        public DiaIpBan() { }

        public string ErrorDescription()
        {
            return "Received Ip Ban, pausing bot!";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            if (!string.IsNullOrEmpty(runonban))
            {
                try
                {
                    Process.Start(runonban);
                }
                catch (Exception) { }
            }
            b.NextAction.Enqueue(new APause(b.Delays.BanWaitTime * 60 * 1000, ErrorDescription()));
            b.NextAction.Enqueue(new ADiaStart());
        }

    }
}
