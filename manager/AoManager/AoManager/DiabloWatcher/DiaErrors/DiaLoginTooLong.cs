﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaLoginTooLong : IDiaError
    {
        public string ErrorDescription()
        {
            return "Login took to long for unknown reason, restarting after 10 seconds!";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(10000, ErrorDescription()));
            b.NextAction.Enqueue(new ADiaStart());
            // check zu oft
        }
    }
}
