﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    public interface IDiaError
    {
        string ErrorDescription();
        void HandleError(DiaWatcher b);
    }
}
