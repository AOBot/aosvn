﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaControllerNoConnect : IDiaError
    {
        public string ErrorDescription()
        {
            return "AoManger can't connect to the Core, stopping AoManager";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.NextError.Clear();
            b.StopDiablo();
        }
    }
}
