﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaMaxGameTimer : IDiaError
    {
        public string ErrorDescription()
        {
            return "Maximum game time hit, restarting!";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(4000, "Restart delay"));
            b.NextAction.Enqueue(new ADiaStart());
        }
    }
}
