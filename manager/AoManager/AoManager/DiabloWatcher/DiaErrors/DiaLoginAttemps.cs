﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaLoginAttemps : IDiaError
    {
        public string ErrorDescription()
        {
            return "Too many login attempts";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(5 * 60 * 1000, ErrorDescription()));
            b.NextAction.Enqueue(new ADiaStart());
        }
    }
}
