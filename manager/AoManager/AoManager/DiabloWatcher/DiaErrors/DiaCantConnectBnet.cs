﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class DiaCantConnectBnet : IDiaError
    {
        public string ErrorDescription()
        {
            return "Received Ip Ban or cant connect to bnet, pausing bot";
        }

        public void HandleError(DiaWatcher b)
        {
            b.NextAction.Clear();
            b.StopDiablo();
            b.NextAction.Enqueue(new APause(60 * b.Delays.CantConnectDelay * 1000, ErrorDescription()));
            b.NextAction.Enqueue(new ADiaStart());
        }
    }
}
