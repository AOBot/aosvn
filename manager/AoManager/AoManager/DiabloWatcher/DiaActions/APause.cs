﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace AoManager
{
    internal class APause: IDiaAction
    {
        int waitime;
        string reason;
        Stopwatch timer;

        public APause(int waitime, string reason)
        {
            this.waitime = waitime;
            this.reason = "Pausing " + (waitime/1000).ToString() +" seconds! Reason: " + reason;
            timer = new Stopwatch();
            timer.Start();
        }

        public string ActionName()
        {
            return reason;
        }

        public void TakeAction(DiaWatcher d)
        {
            if (timer.ElapsedMilliseconds > waitime)
            {
                d.NextAction.Dequeue();
            }
        }
    }
}
