﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class AUpdateStatus: IDiaAction
    {
        string status;

        public AUpdateStatus(string status)
        {
            this.status = status;
        }

        public string ActionName()
        {
            return null;
        }

        public void TakeAction(DiaWatcher d)
        {
            d.Frmstats.UpdateStatus(status);
        }
    }
}
