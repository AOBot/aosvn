﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaStop: IDiaAction
    {
        public string ActionName()
        {
            return "Stopping Diablo!";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.StopDiablo();
        }
    }
}
