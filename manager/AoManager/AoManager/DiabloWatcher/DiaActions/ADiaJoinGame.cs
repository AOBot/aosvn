﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaJoinGame: IDiaAction
    {
        string gamename;

        public ADiaJoinGame(string gamename)
        {
            this.gamename = gamename;
        }

        public string ActionName()
        {
            return "Joining game: " + gamename;
        }

        public void TakeAction(DiaWatcher d)
        {
            d.JoinGame(gamename);
        }
    }
}
