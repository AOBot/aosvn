﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaLobbyJoin: IDiaAction
    {
        public string ActionName()
        {
            return "Joined Lobby!";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.LobbyJoin();
        }
    }
}
