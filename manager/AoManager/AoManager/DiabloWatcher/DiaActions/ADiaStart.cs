﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaStart: IDiaAction
    {
        public string ActionName()
        {
            return "Starting Diablo!";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.RunDiablo();
        }
    }
}
