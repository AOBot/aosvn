﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    public interface IDiaAction
    {
        string ActionName();
        void TakeAction(DiaWatcher d);
    }
}
