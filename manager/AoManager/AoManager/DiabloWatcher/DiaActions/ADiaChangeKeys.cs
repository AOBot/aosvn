﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaChangeKeys: IDiaAction
    {
        public string ActionName()
        {
            return "Changing Keys!";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.StopDiablo();
            d.ChangeCdKeys();
            d.NextAction.Enqueue(new APause(3000, "Restart delay"));
            d.NextAction.Enqueue(new ADiaStart());
        }
    }
}
