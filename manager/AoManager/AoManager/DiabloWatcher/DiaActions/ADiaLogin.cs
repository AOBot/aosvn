﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaLogin: IDiaAction
    {
        public string ActionName()
        {
            return "Logging into bnet!";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.DiaLogin();
        }
    }
}
