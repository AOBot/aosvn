﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaCharSelect: IDiaAction
    {
        public string ActionName()
        {
            return "Selecting Character!";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.SelectChar();
        }
    }
}
