﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaCreateGame: IDiaAction
    {
        public string ActionName()
        {
            return "Creating game!";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.MakeGame();
        }
    }
}
