﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaConDisconnect: IDiaAction
    {
        public string ActionName()
        {
            return null;
        }

        public void TakeAction(DiaWatcher d)
        {
            d.DisConnectController();
        }
    }
}
