﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaChannelJoin: IDiaAction
    {
        public string ActionName()
        {
            return "";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.ChannelJoin();
        }
    }
}
