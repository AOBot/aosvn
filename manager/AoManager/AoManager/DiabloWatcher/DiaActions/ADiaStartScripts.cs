﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AoManager
{
    internal class ADiaStartScripts: IDiaAction
    {
        public string ActionName()
        {
            return "Starting Ao scripts!";
        }

        public void TakeAction(DiaWatcher d)
        {
            d.StartScripts();
        }
    }
}
