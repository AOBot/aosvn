﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using System.Security.Permissions;

[assembly: PermissionSet(SecurityAction.RequestMinimum, Name = "FullTrust")]
namespace AoManager
{
    internal enum Location
    {
        Login,
        Lobby,
        Chat,
        Ingame,
        None
    }

    public class DiaWatcher : IDisposable
    {
        bool AllowEvents;
        bool FirstJoinChannel;
        bool FirstJoin;
        int GameCountName = 0;
        int GameCountThisHour = 0;
        int GameCountKeyChange = 0;
        int LoginAttempts = 0;
        int DiabloTries = 0;

        string CurrentClassicKey;
        string CurrentExpKey;

        Logging log;

        Location _Location = Location.Login;
        BotStatistic Botstat = new BotStatistic();
        Game LastGame = new Game();
        //Settings
        Profile Profile;
        AoSettingStruct AoConfig;
        //Cdkeys
        CdKeyManager CdKeyManager;
        //Process Watching
        Process DiabloProc = null;
        //Diablo Interactors
        WinHider Hider;
        DiaWinHandler Dia;
        AwesomoControllerClient Controller;

        //Timers
        Stopwatch MaxGameTimer = new Stopwatch();
        Stopwatch GameLimiterSW = new Stopwatch();
        Stopwatch LoginTimer = new Stopwatch();
        Stopwatch GameJoinTimer = new Stopwatch();
        string gameName = "";

        // delays aka Settings
        public BotSetting Delays;

        //Status Form
        public frmStats Frmstats;

        //  work queues
        public Queue<IDiaError> NextError = new Queue<IDiaError>();
        public Queue<IDiaAction> NextAction = new Queue<IDiaAction>();

        public DiaWatcher(BotSetting delays, Profile profile, AoSettingStruct AoConfig) 
        {
            this.Profile = profile;
            this.Delays = delays;
            this.AoConfig = AoConfig;
            this.CdKeyManager = new CdKeyManager(profile.ClassicKeys, profile.ExpansionKeys); 
            this.CurrentClassicKey = CdKeyManager.GetNextClassicKey();
            this.CurrentExpKey = CdKeyManager.GetNextExpansionKey();

            this.log = new Logging(Logging.LogPath + Profile.ProfileName + ".log", true);
            this.log.addInfo("Diablo watcher started!");

            GameLimiterSW.Start();
            Frmstats = new frmStats(Profile.ProfileName, this, Profile.HideDiabloWindow);
            Frmstats.Show();
        }

        public string ProfileName
        {
            get { return Profile.ProfileName; }
        }

        //Main handler, this function will be called from AoManager.cs
        public void Tick()
        {
            IDiaError err;

            if (NextError.Count > 0)
            {
                err =  NextError.Dequeue();
                Frmstats.IncreaseInterventions();
                Console.WriteLine(err.ErrorDescription());
                if (!string.IsNullOrEmpty(err.ErrorDescription()))
                    log.addError(err.ErrorDescription());
                err.HandleError(this);
                return;
            }

            err = CheckForErrors();
            if (err != null)
            {
                Console.WriteLine(err.ErrorDescription());
                Frmstats.IncreaseInterventions();
                if (!string.IsNullOrEmpty(err.ErrorDescription()))
                    log.addError(err.ErrorDescription());
                err.HandleError(this);
                return;
            }

            while (NextAction.Count > 0)
            {
                IDiaAction a;
                a = NextAction.Peek();
                Console.WriteLine(a.ActionName());
                if (!(a is APause))
                {
                    a = NextAction.Dequeue();
                    if (!string.IsNullOrEmpty(a.ActionName()))
                        log.addInfo(a.ActionName());
                    a.TakeAction(this);
                }
                else
                {
                    a.TakeAction(this);
                    break;
                }     
            }
            
        }

        #region Controls

        public void ShowDiablo()
        {
            if (Hider != null)
                Hider.UnHideWindow();
        }

        public void HideDiablo()
        {
            if (Hider != null)
                Hider.HideWindow();
        }

        public void ShowStatus()
        {
            if (Frmstats != null)
                Frmstats.Show();
        }

        public void HideStatus()
        {
            if (Frmstats != null)
                Frmstats.Hide();
        }

        #endregion

        #region ErrorChecker
        private IDiaError CheckForErrors()
        {
            
                WindowFuncs WinFuncs = new WindowFuncs();
                DiaErrorWinNames wins = new DiaErrorWinNames(Profile.ProfileName);

                if (DiabloProc != null)
                {
                    try
                    {
                        // Maxgametime check
                        if (Profile.MaxGameTime > 0)
                        {
                            if (MaxGameTimer.ElapsedMilliseconds >= Profile.MaxGameTime * 1000)
                            {
                                return new DiaMaxGameTimer();
                            }
                        }

                        // Max Games this hour check
                        if (GameLimiterSW.ElapsedMilliseconds >= 3600000)
                        {
                            log.addInfo("GameHourLimiter has been reset");
                            GameLimiterSW.Reset();
                            GameLimiterSW.Start();
                            GameCountThisHour = 0;  
                        }
                        else if (GameCountThisHour >= Profile.MaxGamesAnHour)
                        {
                            return new DiaGamesHour(3600000 - (int)GameLimiterSW.ElapsedMilliseconds);   
                        }

                        // If Login takes too long
                        if (LoginTimer.ElapsedMilliseconds > (Delays.LoginTooLongDelay * 1000) && _Location.Equals(Location.Login))
                        {
                            return new DiaLoginTooLong();
                        }
                            
                        // if game creation failes e.g lost connection to bnet
                        if (GameJoinTimer.ElapsedMilliseconds > (Delays.GameJoinTooLongDelay * 1000) && (_Location.Equals(Location.Lobby) || _Location.Equals(Location.Chat)))
                        {
                           return new DiaJoinTooLong();  
                        }

                        //check for error Windows
                        foreach (string winname in wins.WindowNames)
                        {
                            if (WinFuncs.WinExistsByName(winname))
                            {
                                AllowEvents = false;
                                WinFuncs.CloseWindowByName(winname);
                                WinFuncs.KillWindowByName(winname);
                                return new DiaErrorWin(winname);
                            }
                        }

                        //Process error
                        /*if (!DiabloProc.HasExited && !DiabloProc.Responding)
                        {
                            log.addError("Diablo II doesn't seem to be responsive");
                            KillDia();
                        }*/
                        // Check for Error Windows 

                    }
                    catch (Exception) { return null;}
                }
                return null;
        }
        #endregion

        #region Functions

        public void ConnectController()
        {
           
             this.Controller = new AwesomoControllerClient(AoConfig.ControllerIp);
             this.Controller.PacketReceived += new AwesomoControllerClient_Handler(Controller_PacketReceived);

                if (!Controller.IsClientConnected)
                {
                    log.addInfo("Connecting to Awesom-O Core...");
                    //
                    System.Diagnostics.Stopwatch StopWatch = new System.Diagnostics.Stopwatch();
                    StopWatch.Start();
                    //
                    while (true)
                    {
                        Controller.CreateNewControllerClient(Profile.DiabloOwnerName);
                        System.Threading.Thread.Sleep(500);
                        if (Controller.IsClientConnected)
                        {
                            log.addInfo("Connected to Awesom-O Core");
                            break;
                        }
                        else if ((StopWatch.ElapsedMilliseconds / 1000) >= 20)
                        {
                            log.addInfo("Error Connecting to Awesom-O Core");
                            NextError.Enqueue(new DiaControllerNoConnect());
                            return;
                        }
                        Thread.Sleep(100);
                    }
                    if (!string.IsNullOrEmpty(CurrentClassicKey) && (!string.IsNullOrEmpty(CurrentExpKey)))
                    {
                        log.addInfo("Keys are empty, adding getting next set!");
                        Thread.Sleep(500);
                        log.addInfo("Going to send key to controller");
                        Controller.AddCdkey(CurrentClassicKey, "6");
                        Thread.Sleep(500);
                        Controller.AddCdkey(CurrentExpKey, "10");
                        log.addInfo("Added Cdkeys to Core!");
                        Thread.Sleep(1000);
                    }
                    Controller.Start();
                
            }

        }

        public void DisConnectController()
        {
            if (Controller != null)
            {
                Controller.Dispose();
                Controller = null;
            }
        }

        public void InitDiaHandlers()
        {
            IntPtr hWnd = Process.GetProcessById(DiabloProc.Id).MainWindowHandle;

            Hider = new WinHider(hWnd, false);

            Dia = new DiaWinHandler(hWnd);
            Dia.KeyDelay = AoConfig.KeyDelay;
            Dia.ClickDelay = AoConfig.ClickDelay;
        }

        public void RunDiablo()
        {
            _Location = Location.Login;
            KillDia();
            CleanBnetCacheFiles();
            AllowEvents = true;

            LoginTimer.Reset();
            LoginTimer.Start();
            GameJoinTimer.Stop();
            GameJoinTimer.Reset();

            this.DiabloTries++; // Count how long we've been tryng to connect

            DiabloProc = D2Multi.StartD2Multi(Profile.DiabloPath, Profile.ProfileName, log);
            if (DiabloProc == null)
                NextError.Enqueue(new DiaNoStart());
            return;
            /*if (Properties.Settings.Default.UseD2Launcher)
            {
               // DiabloProc = D2Launcher.StartD2Launcher(Profile.DiabloPath, Profile.ProfileName, log);
                DiabloProc = D2Multi.StartD2Multi(Profile.DiabloPath, Profile.ProfileName, log);
                if (DiabloProc == null)
                    NextError.Enqueue(new DiaNoStart());
                    
                return;   
            }
            try
            {
                DiabloProc = new Process();
                DiabloProc.StartInfo.FileName = Profile.DiabloPath + "\\" + "game.exe";
                DiabloProc.StartInfo.Arguments = "-w -ns -skiptobnet";
                DiabloProc.StartInfo.WorkingDirectory = Profile.DiabloPath;
                DiabloProc.EnableRaisingEvents = true;
                bool Started = DiabloProc.Start();
                DiabloProc.Exited += new EventHandler(DiabloProc_Exited);
                DiabloProc.WaitForInputIdle(3000);
                if(!Started)
                    NextError.Enqueue(new DiaNoStart());
            }
            catch (Exception e) { log.addCError("RunDiablo(): " + e.Message); DiabloProc = null; NextError.Enqueue(new DiaNoStart()); }
            */
            
        }

        public void StopDiablo()
        {
            AllowEvents = false;

            if (Hider != null)
            {
                Hider.Dispose();
                Hider = null;
            }

            //
            if (DiabloProc != null)
            {
                try { if (!DiabloProc.HasExited) { KillDia(); } }
                catch (Exception e) { log.addInfo("BotWatcher Stop: " + e.Message); }
                DiabloProc.Dispose();
            }
            DiabloProc = null;
            //

            MaxGameTimer.Stop();
            MaxGameTimer.Reset();
            LoginTimer.Stop();
            LoginTimer.Reset();

            _Location = Location.None;
        }

        private void KillDia()
        {
            if (DiabloProc != null)
            {
                try
                {
                    if (!DiabloProc.HasExited)
                        DiabloProc.Kill();
                    //wait max 10 seconds for Process closing
                    for (int i = 0; i < 10 && !DiabloProc.HasExited; i++)
                        Thread.Sleep(1000);

                    if (!DiabloProc.HasExited)
                        DiabloProc.Kill();
                }
                catch (Exception e) { log.addCError("KillDia: " + e.Message); }
            }
            else 
            {
                Process[] procs = Process.GetProcessesByName("game");
                if (procs.Length <= 0) { return; }
                foreach (Process proc in procs)
                {
                    try
                    {
                        if (proc.MainWindowTitle == Profile.ProfileName)
                        {
                            proc.CloseMainWindow();
                            //wait max 10 seconds for Process closing
                            for (int i = 0; i < 10 && !proc.HasExited; i++)
                                Thread.Sleep(1000);

                            if (!proc.HasExited)
                            {
                                WindowFuncs WinFuncs = new WindowFuncs();
                                WinFuncs.KillWindowByName(Profile.ProfileName);
                                Thread.Sleep(2000);
                            }
                            //proc.WaitForExit(); 
                        }
                    }
                    catch (Exception e) { log.addCError("KillDia by name: " + e.Message); }
                }
            }
        }

        public void MakeGame()
        {
            
            GameCountName++;
            int gameDifficulty = 0; //Initializing
            if (Profile.MakeMuleGame) //If bot must create a game for muling, difficulty is Normal
                gameDifficulty = 2;
            else // else difficulty is what it is set in Settings
                gameDifficulty = Profile.GameDifficulty;

            GameJoinTimer.Start();

            if (Profile.MakeMuleGame) // If muling, make a random game
            {
                Dia.CreateRandomGame(gameDifficulty);
                return;
            }

            if (!Profile.HasGamePass) //If no game pass
            {
                if (string.IsNullOrEmpty(Profile.GameName)) //If no game name, make a game with a random name and no password
                {
                    LastGame.GameName = Dia.CreateGame(Dia.RandomString(Dia.RandomNumber(Dia.minimumGameNameChars, Dia.maximumGameNameChars)), "", gameDifficulty);
                    return;
                }
                else // If it does have a static game name, it will send channel message if necessary and make a game with the specified name and no pass
                {
                    if (!string.IsNullOrEmpty(Profile.Channel) && !Profile.NowMuling && !Profile.MakeMuleGame && !Profile.Follower) { Dia.ChatChannel(Profile.NextGameMessage + " " + Profile.GameName + GameCountName.ToString()); }
                    LastGame.GameName = Dia.CreateGame(Profile.GameName + GameCountName.ToString(), "", gameDifficulty);
                    return;
                }
            }
            else //If game pass
            {
                if (string.IsNullOrEmpty(Profile.GameName) && string.IsNullOrEmpty(Profile.GamePassword)) // If no specified pass and no game name specified, make random game
                {
                    LastGame.GameName = Dia.CreateRandomGame(gameDifficulty);
                    return;
                }
                else if (string.IsNullOrEmpty(Profile.GameName)) // If no game name specified but has a pass specified, make a game with a random name and with the static pass
                {
                    LastGame.GameName = Dia.CreateGame(Dia.RandomString(Dia.RandomNumber(Dia.minimumGameNameChars, Dia.maximumGameNameChars)), Profile.GamePassword, gameDifficulty);
                    return;
                }
                else if (string.IsNullOrEmpty(Profile.GamePassword)) // If game name specified but not pass, make a game with correct game name and random pass
                {
                    if (!string.IsNullOrEmpty(Profile.Channel) && !Profile.NowMuling && !Profile.MakeMuleGame && !Profile.Follower) { Dia.ChatChannel(Profile.NextGameMessage + " " + Profile.GameName + GameCountName.ToString()); }
                    LastGame.GameName = Dia.CreateGame(Profile.GameName + GameCountName.ToString(), Dia.RandomString(Dia.RandomNumber(Dia.minimumGamePassChars, Dia.maximumGamePassChars)), gameDifficulty);
                    return;
                }
                else //Both names and pass are specified, make game using these
                {
                    if (!string.IsNullOrEmpty(Profile.Channel) && !Profile.NowMuling && !Profile.MakeMuleGame && !Profile.Follower) { Dia.ChatChannel(Profile.NextGameMessage + " " + Profile.GameName + GameCountName.ToString()); }
                    LastGame.GameName = Dia.CreateGame(Profile.GameName + GameCountName.ToString(), Profile.GamePassword, gameDifficulty);
                    return;
                }
            }
        }

        public void JoinGame(string gameName)
        {
            GameJoinTimer.Start();
           
            if (!string.IsNullOrEmpty(Profile.FollowGamePassword))
                LastGame.GameName = Dia.JoinGame(gameName, Profile.FollowGamePassword);
            else
                LastGame.GameName = Dia.JoinGame(gameName, "");
        }

        private bool CheckKeyChange()
        {
            return Profile.KeyChange > 0 && GameCountKeyChange >= Profile.KeyChange;
        }

        private void CleanBnetCacheFiles()
        {
            if (Directory.Exists(Profile.DiabloPath))
            {
                foreach (string subfile in Directory.GetFiles(Profile.DiabloPath))
                {
                    if (Path.GetFileName(subfile).ToLower().Contains("bncache") && Path.GetExtension(subfile).ToLower() == ".dat"
                        || Path.GetFileName(subfile).ToLower().StartsWith("d") && Path.GetExtension(subfile).ToLower() == ".txt"
                        || Path.GetFileName(subfile).ToLower().Contains("bnetlog") && Path.GetExtension(subfile).ToLower() == ".log")
                    {
                        try
                        {
                            File.Delete(subfile);
                        }
                        catch (Exception) { }   // do nothing, it's not important
                    }
                }
            }
        }

        private string GetGameName(string p_message)
        {
            string gameNameIs = "";

            StreamReader str = File.OpenText(Application.StartupPath + "\\data\\leechMessages.txt");
            string line = "";
            string beforeGame = "";
            string afterGame = "";
            while (!str.EndOfStream)
            {
                line = str.ReadLine();
                if (line.StartsWith("--") || string.IsNullOrEmpty(line.Trim()))
                    continue;
                else
                {
                    line = line.Replace("%MASTERACCOUNT%", Profile.MasterAccount);
                    beforeGame = line.Remove(line.IndexOf("%GAMENAME%"));
                    afterGame = line.Remove(0, line.IndexOf("%GAMENAME%") + "%GAMENAME%".Length);
                    if (p_message.Contains(beforeGame))
                    {
                        p_message = p_message.Remove(0, p_message.IndexOf(beforeGame) + beforeGame.Length);
                        p_message = p_message.Remove(p_message.IndexOf(afterGame));
                        gameNameIs = p_message.Trim();
                        return gameNameIs;
                    }
                }
            }
            str.Close();
            return gameNameIs;
        }

        private void SaveCurrentProfile()
        {
            // a bit confusing because Delays are actually the whole setting struct
            for (int i = 0; i < Delays.Profiles.Count; i++)
            {
                if (Delays.Profiles[i].ProfileName.Equals(Profile.ProfileName))
                {
                    Delays.Profiles[i] = Profile;
                    break;
                }

            }
            BotSettings.WriteSettings(Delays);
        }

        public void ChangeCdKeys()
        {
            GameCountKeyChange = 0;
            log.addInfo("Releasing CD Keys for " + Profile.ProfileName);
            Controller.ReleaseCdkeys();
            Controller.ClearCdkeys();
            CurrentClassicKey = CdKeyManager.GetNextClassicKey();
            CurrentExpKey = CdKeyManager.GetNextExpansionKey();
            log.addInfo("Going to send keys to controller");
            Controller.AddCdkey(CurrentClassicKey, "6");
            Thread.Sleep(1000);
            Controller.AddCdkey(CurrentExpKey, "10");
        }

        public void DiaLogin()
        {
            FirstJoinChannel = true;
            FirstJoin = true;
            LoginAttempts = 0;
            _Location = Location.Login;
            Dia.ChangeDiaWinTitle(Profile.ProfileName);
            Dia.BringDiaToFront();
            Dia.Login(Profile.DiabloAccount, Profile.DiabloPassword);
        }

        public void SelectChar()
        {
            Dia.BringDiaToFront();
            this.DiabloTries = 0;
            if (!Profile.NowMuling)
                Dia.SelectChar(Profile.DiabloCharPosition);
            else
                Dia.SelectChar(Profile.MuleCharPos);
            if (Profile.HideDiabloWindow)
                Hider.HideWindow();
           
        }

        public void LobbyJoin()
        {
            FirstJoin = false;

            Frmstats.AddStatistics(Botstat);

            MaxGameTimer.Stop();
            LoginTimer.Stop();
            LoginTimer.Reset();

            if (CheckKeyChange())
            {
                AllowEvents = false;
                NextAction.Enqueue(new ADiaChangeKeys());
                return;
            }

            if (MaxGameTimer.ElapsedMilliseconds < Profile.MinGameTime * 1000 && string.IsNullOrEmpty(Profile.Channel) && MaxGameTimer.ElapsedMilliseconds > 0)
               NextAction.Enqueue(new APause((int)(Profile.MinGameTime * 1000 - MaxGameTimer.ElapsedMilliseconds), "Game To Short"));

            if (!string.IsNullOrEmpty(Profile.Channel) && FirstJoinChannel)
            {
                Dia.JoinChannel(Profile.Channel);
                Thread.Sleep(1000);
                if (!Profile.Follower)
                    Dia.ChatChannel(Profile.FirstJoinChannelMessage);
            }
            else if (string.IsNullOrEmpty(Profile.Channel))
            {
                Console.WriteLine("Hallo1");
                if (!Profile.Follower)
                {
                    Console.WriteLine("Hallo1");
                    NextAction.Enqueue(new APause(Delays.CreateGameDelay * 1000, "Create Game Delay"));
                    NextAction.Enqueue(new ADiaCreateGame());
                }
            }
        }

        public void ChannelJoin()
        {
            if (MaxGameTimer.ElapsedMilliseconds < Profile.MinGameTime * 1000 && !FirstJoinChannel)
            {
                if (!string.IsNullOrEmpty(Profile.TooShortMessage) && !Profile.NowMuling && !Profile.MakeMuleGame && !Profile.Follower)
                    Dia.ChatChannel(Profile.TooShortMessage + " " + ((Profile.MinGameTime * 1000 - MaxGameTimer.ElapsedMilliseconds) / 1000).ToString() + " seconds");
                
                NextAction.Enqueue(new APause((int)(Profile.MinGameTime * 1000 - MaxGameTimer.ElapsedMilliseconds), "Game too short"));
            }
            FirstJoinChannel = false;

            NextAction.Enqueue(new APause(Delays.CreateGameDelay * 1000, "Create Game Delay"));
            NextAction.Enqueue(new ADiaCreateGame());
            
        }

        public void StartScripts()
        {

            if (Profile.Follower) // Setting var for leeching/cobaaling/joiner
            {
                Controller.SetCoreVar("leechmaster", Profile.MasterCharacter);
                Thread.Sleep(1000);
            }

             //If normal run or leech , do this
             if (Profile.AutoStartRun)
                Controller.StartRun();
             MaxGameTimer.Reset();
             MaxGameTimer.Start();
             Botstat.IncreaseGameCount();
        }

        #endregion

        #region Events


        private void DiabloProc_Exited(object sender, EventArgs e)
        {
            if (AllowEvents)
            {
                NextError.Enqueue(new DiaExited()); 
            }
        }


        private void Controller_PacketReceived(object sender, PacketArgs myArgs)
        {

            switch (myArgs.Event)
            {
                case AwesomoControllerClient.ControllerEventPackets.CONNECTION_SUCCESSFUL:
                    //log.addInfo("Checking realm!");
                    AoRealmHelper.SetRealm();
                    AoRealmHelper.PreventAct5Bug(Profile.DiabloPath + "\\");
                    break;
                case AwesomoControllerClient.ControllerEventPackets.ALREADY_CONNECTED:
                    break;
                case AwesomoControllerClient.ControllerEventPackets.BNET_AUTH_RESPONSE:
                    if (myArgs.Result == 1) // CD key in use
                    {
                        if (Profile.ClassicKeys.Count > 1)
                        {
                            NextAction.Enqueue(new ADiaChangeKeys());
                        }

                        NextError.Enqueue(new DiaKeyInUse());
                        return;
                    }
                    NextAction.Enqueue(new ADiaInitDiaHandlers());
                    NextAction.Enqueue(new ADiaLogin());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.BNET_LOGON_RESPONSE:
                    LoginAttempts++;
                    if (LoginAttempts > 2)
                    {
                        NextError.Enqueue(new DiaLoginAttemps());
                        return;
                    }
                    if ((myArgs.Result == 1 || myArgs.Result == 2) && LoginAttempts < 3)
                    {
                        Dia.RemoveLoginError();
                        NextAction.Enqueue(new ADiaLogin());
                    }
                    break;
                case AwesomoControllerClient.ControllerEventPackets.CANT_CONNECT:
                    NextError.Enqueue(new DiaCantConnectBnet());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.TEMPORARY_IP_BAN:
                    if (AoConfig.RunOnBan)
                        NextError.Enqueue(new DiaIpBan(AoConfig.RunOnBanExePath));
                    else
                        NextError.Enqueue(new DiaIpBan());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.CHARACTER_SELECT:
                    LoginAttempts = 0;
                    NextAction.Enqueue(new ADiaCharSelect());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.LOGON_SUCCESS:
              
                    _Location = Location.Lobby;

                    //if (FirstJoin)
                        //BotWatcher_Changed(this, new GlobalEventArgs(GlobalEventArgs.EventCode.BotConnected));
                    NextAction.Enqueue(new ADiaLobbyJoin());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.JOIN_CHANNEL:
                    _Location = Location.Chat;
                    NextAction.Enqueue(new ADiaChannelJoin());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.JOIN_GAME_SUCCESSFUL:
                    break;
                case AwesomoControllerClient.ControllerEventPackets.FAILED_TO_JOIN:
                    if (Profile.Follower)
                    {
                        NextAction.Enqueue(new APause(Profile.JoinGameDelay * 1000, "Join game Delay"));
                        NextAction.Enqueue(new ADiaJoinGame(Profile.FollowGameName));
                    }
                    else
                    {
                        GameCountKeyChange++;
                        GameCountThisHour++;
                    }
                    break;
                case AwesomoControllerClient.ControllerEventPackets.GAME_ALREADY_EXISTS:
                    NextAction.Enqueue(new ADiaCreateGame());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.GAME_DOES_NOT_EXISTS:
                    break;
                case AwesomoControllerClient.ControllerEventPackets.CONNECTED_GAME_PROXY:
                    //LastGame.StartTime = DateTime.Now.ToString("M/d/yyyy H:mm:ss");
                    GameJoinTimer.Stop();
                    GameJoinTimer.Reset();
                    _Location = Location.Ingame;
                    GameCountKeyChange++;
                    GameCountThisHour++;

                    NextAction.Enqueue(new APause(Delays.StartRunDelay * 1000, "Start run delay"));
                    NextAction.Enqueue(new ADiaStartScripts());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.EXIT_GAME:
                    //LastGame.EndTime = DateTime.Now.ToString("M/d/yyyy H:mm:ss");
                    //LastGame.Success = true;
                    Botstat.IncreaseGameCountSuccess();
                    Botstat.AddInGameTime(MaxGameTimer.ElapsedMilliseconds);
                    break;
                case AwesomoControllerClient.ControllerEventPackets.ITEM_MESSAGE:
                    Item item = ItemParser.ParseItemLine(myArgs.Message);
                    if (item != null)
                        NextAction.Enqueue(new AAdditem(item));
                    break;
                case AwesomoControllerClient.ControllerEventPackets.STATUS_MESSAGE:
                    NextAction.Enqueue(new AUpdateStatus(myArgs.Message));
                    break;
                case AwesomoControllerClient.ControllerEventPackets.CHICKEN_EXIT:
                    Botstat.IncreaseChickens();
                    break;
                case AwesomoControllerClient.ControllerEventPackets.DEATH:
                    Botstat.IncreaseDeaths();
                    break;
                case AwesomoControllerClient.ControllerEventPackets.SOCKET_ERROR:
                    NextError.Enqueue(new DiaCSocketError());
                    break;
                case AwesomoControllerClient.ControllerEventPackets.HOT_IP:
                    MaxGameTimer.Stop();
                    if (Hider.IsHidden)
                        Hider.UnHideWindow();
                    break;
                case AwesomoControllerClient.ControllerEventPackets.DIA_CLONE:
                    MaxGameTimer.Stop();
                    if (Hider.IsHidden)
                        Hider.UnHideWindow();
                    break;
                case AwesomoControllerClient.ControllerEventPackets.CHAT_EVENT:
                    // Your friend %ACCOUNT% entered a Diablo II Lord of Destruction game called %GAMENAME%.
                    // myArgs.Account + myArgs.CharName + myArgs.Message
                    if (Profile.Follower)
                    {
                        if (_Location == Location.Chat || _Location == Location.Lobby)
                        {
                            if (myArgs.Account == Profile.MasterAccount && myArgs.CharName == Profile.MasterCharacter)
                            {
                                if (!(String.IsNullOrEmpty(gameName = GetGameName(myArgs.Message))))
                                {
                                    Profile.FollowGameName = gameName;

                                    NextAction.Enqueue(new APause(Profile.JoinGameDelay * 1000, "Join game Delay"));
                                    NextAction.Enqueue(new ADiaJoinGame(gameName));
                                }
                            }
                        }
                    }
                    break;
                case AwesomoControllerClient.ControllerEventPackets.GAME_EXP:
                    Botstat.AddGameExperience(Convert.ToInt32(myArgs.Message.Trim()));
                    break;
            }

        }

        #endregion

        public void Dispose()
        {
            DisConnectController();
            StopDiablo();
            if (Frmstats != null)
                Frmstats.Dispose();
        }
    }
}
