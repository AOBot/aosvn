---------------------- HOW TO USE THIS FILE --------------------------

-------- THIS FILE MUST BE WHERE AOMANAGER.EXE IS --------------------

-- First, lines beginning with '--' are considered as comments and 
--  wont be read

-- Each line is a Language, to add a language, simply add a new line
-- and enter the exact same sentence that is said when a friend joins a game.

-- Just look at how the other languages are made and do the same for your language.
-- EVERYTHING IS CASE SENSITIVE

--If your language doesnt work but is there, make sure everything is correct.
-- If its not correct edit it and please report it so we can change it in the file.

--If it is correct, case sensitive and everything, but still doesnt work,
-- please pm nathan259 on aobot.org, with the language the error is occuring with and
-- the exact sentence (case sensitive). And I will be proud to find a fix :)

-- %MASTERACCOUNT%  is where the account should be.
-- %GAMENAME% is where the game name should be.

-------------------- EDIT OR ADD LANGUAGES BELOW THIS LINE ------------

--English:

Your friend %MASTERACCOUNT% entered a Diablo II Lord of Destruction game called %GAMENAME%.


--German:

Ihr Freund %MASTERACCOUNT% hat sich in ein Diablo II Lord of Destruction-Spiel mit dem Namen %GAMENAME% eingeklinkt.


--French:

Votre ami %MASTERACCOUNT% est entr� dans une partie Diablo II Lord of Destruction nomm�e %GAMENAME%.


--Italian:

Il tuo amico %MASTERACCOUNT% � appena entrato in una partita di Diablo II Lord of Destruction chiamata %GAMENAME%.


--Spanish:

Tu amigo %MASTERACCOUNT% ha entrado en una partida Diablo II Lord of Destruction llamada %GAMENAME%.

 
