﻿namespace AoManager
{
    partial class frmAoSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAoSettings));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxKeyDelay = new System.Windows.Forms.TextBox();
            this.textBoxClickDelay = new System.Windows.Forms.TextBox();
            this.toolTiper = new System.Windows.Forms.ToolTip(this.components);
            this.textBoxAwesomoDirectory = new System.Windows.Forms.TextBox();
            this.comboBoxRealm = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDiabloDirectory = new System.Windows.Forms.TextBox();
            this.textBoxStartDelay = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chk_Stealth = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chk_HideCore = new System.Windows.Forms.CheckBox();
            this.chk_RunOnBan = new System.Windows.Forms.CheckBox();
            this.txt_RunOnBan = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.gb_Expert = new System.Windows.Forms.GroupBox();
            this.buttonConfIp = new System.Windows.Forms.Button();
            this.checkBoxIpChanger = new System.Windows.Forms.CheckBox();
            this.chk_AoNoStart = new System.Windows.Forms.CheckBox();
            this.chk_Cip = new System.Windows.Forms.CheckBox();
            this.txt_Cip = new System.Windows.Forms.TextBox();
            this.txtAoFlags = new System.Windows.Forms.TextBox();
            this.chk_Expert = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gb_Expert.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Key Delay:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Click Delay:";
            // 
            // textBoxKeyDelay
            // 
            this.textBoxKeyDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxKeyDelay.Location = new System.Drawing.Point(131, 145);
            this.textBoxKeyDelay.Name = "textBoxKeyDelay";
            this.textBoxKeyDelay.Size = new System.Drawing.Size(114, 20);
            this.textBoxKeyDelay.TabIndex = 3;
            this.textBoxKeyDelay.Text = "50";
            this.toolTiper.SetToolTip(this.textBoxKeyDelay, "The delay between key strokes");
            this.textBoxKeyDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // textBoxClickDelay
            // 
            this.textBoxClickDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxClickDelay.Location = new System.Drawing.Point(131, 171);
            this.textBoxClickDelay.Name = "textBoxClickDelay";
            this.textBoxClickDelay.Size = new System.Drawing.Size(114, 20);
            this.textBoxClickDelay.TabIndex = 4;
            this.textBoxClickDelay.Text = "50";
            this.toolTiper.SetToolTip(this.textBoxClickDelay, "The delay between mouse clicks");
            this.textBoxClickDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // toolTiper
            // 
            this.toolTiper.AutomaticDelay = 1;
            this.toolTiper.AutoPopDelay = 10000;
            this.toolTiper.InitialDelay = 250;
            this.toolTiper.ReshowDelay = 0;
            this.toolTiper.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTiper.ToolTipTitle = "Quick Tip";
            // 
            // textBoxAwesomoDirectory
            // 
            this.textBoxAwesomoDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAwesomoDirectory.Location = new System.Drawing.Point(131, 12);
            this.textBoxAwesomoDirectory.Name = "textBoxAwesomoDirectory";
            this.textBoxAwesomoDirectory.ReadOnly = true;
            this.textBoxAwesomoDirectory.Size = new System.Drawing.Size(183, 20);
            this.textBoxAwesomoDirectory.TabIndex = 16;
            this.toolTiper.SetToolTip(this.textBoxAwesomoDirectory, "The directory to your Awesom-O Core");
            this.textBoxAwesomoDirectory.Click += new System.EventHandler(this.textBoxAwesomoDirectory_Click);
            // 
            // comboBoxRealm
            // 
            this.comboBoxRealm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRealm.FormattingEnabled = true;
            this.comboBoxRealm.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.comboBoxRealm.Items.AddRange(new object[] {
            "uswest.battle.net",
            "useast.battle.net",
            "asia.battle.net",
            "europe.battle.net",
            "classicbeta.battle.net"});
            this.comboBoxRealm.Location = new System.Drawing.Point(131, 62);
            this.comboBoxRealm.MaxDropDownItems = 4;
            this.comboBoxRealm.Name = "comboBoxRealm";
            this.comboBoxRealm.Size = new System.Drawing.Size(183, 21);
            this.comboBoxRealm.TabIndex = 37;
            this.toolTiper.SetToolTip(this.comboBoxRealm, "Select The Realm That Your Character Is On");
            this.comboBoxRealm.SelectedIndexChanged += new System.EventHandler(this.comboBoxRealm_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 65);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 36;
            this.label13.Text = "Realm:";
            this.toolTiper.SetToolTip(this.label13, "Select The Realm That Your Character Is On");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Stealth Mode";
            this.toolTiper.SetToolTip(this.label1, "Your Bnet Friends Will Not See You Online Or The Games You Join");
            // 
            // textBoxDiabloDirectory
            // 
            this.textBoxDiabloDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDiabloDirectory.Location = new System.Drawing.Point(131, 38);
            this.textBoxDiabloDirectory.Name = "textBoxDiabloDirectory";
            this.textBoxDiabloDirectory.ReadOnly = true;
            this.textBoxDiabloDirectory.Size = new System.Drawing.Size(183, 20);
            this.textBoxDiabloDirectory.TabIndex = 42;
            this.toolTiper.SetToolTip(this.textBoxDiabloDirectory, "The directory to your Diablo Installation");
            this.textBoxDiabloDirectory.Click += new System.EventHandler(this.textBoxDiabloDirectory_Click);
            // 
            // textBoxStartDelay
            // 
            this.textBoxStartDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStartDelay.Location = new System.Drawing.Point(131, 197);
            this.textBoxStartDelay.Name = "textBoxStartDelay";
            this.textBoxStartDelay.Size = new System.Drawing.Size(114, 20);
            this.textBoxStartDelay.TabIndex = 44;
            this.textBoxStartDelay.Text = "3000";
            this.toolTiper.SetToolTip(this.textBoxStartDelay, "The delay after starting the Awesom-O.exe");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Ao Start Delay";
            this.toolTiper.SetToolTip(this.label8, "Time to wait for Ao starting!");
            // 
            // chk_Stealth
            // 
            this.chk_Stealth.AutoSize = true;
            this.chk_Stealth.Location = new System.Drawing.Point(131, 119);
            this.chk_Stealth.Name = "chk_Stealth";
            this.chk_Stealth.Size = new System.Drawing.Size(15, 14);
            this.chk_Stealth.TabIndex = 38;
            this.toolTiper.SetToolTip(this.chk_Stealth, "Your Bnet Friends Will Not See You Online Or The Games You Join");
            this.chk_Stealth.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "Hide Core";
            this.toolTiper.SetToolTip(this.label9, "Hides The Core Window");
            // 
            // chk_HideCore
            // 
            this.chk_HideCore.AutoSize = true;
            this.chk_HideCore.Location = new System.Drawing.Point(131, 91);
            this.chk_HideCore.Name = "chk_HideCore";
            this.chk_HideCore.Size = new System.Drawing.Size(15, 14);
            this.chk_HideCore.TabIndex = 46;
            this.toolTiper.SetToolTip(this.chk_HideCore, "Hides The Core Window");
            this.chk_HideCore.UseVisualStyleBackColor = true;
            // 
            // chk_RunOnBan
            // 
            this.chk_RunOnBan.AutoSize = true;
            this.chk_RunOnBan.Location = new System.Drawing.Point(6, 94);
            this.chk_RunOnBan.Name = "chk_RunOnBan";
            this.chk_RunOnBan.Size = new System.Drawing.Size(95, 17);
            this.chk_RunOnBan.TabIndex = 7;
            this.chk_RunOnBan.Text = "Run on IP ban";
            this.toolTiper.SetToolTip(this.chk_RunOnBan, "Excute a .bat ore .exe on Ip Ban");
            this.chk_RunOnBan.UseVisualStyleBackColor = true;
            this.chk_RunOnBan.CheckedChanged += new System.EventHandler(this.chk_RunOnBan_CheckedChanged);
            // 
            // txt_RunOnBan
            // 
            this.txt_RunOnBan.Enabled = false;
            this.txt_RunOnBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RunOnBan.Location = new System.Drawing.Point(116, 90);
            this.txt_RunOnBan.Name = "txt_RunOnBan";
            this.txt_RunOnBan.ReadOnly = true;
            this.txt_RunOnBan.Size = new System.Drawing.Size(114, 20);
            this.txt_RunOnBan.TabIndex = 48;
            this.toolTiper.SetToolTip(this.txt_RunOnBan, "The path to an executable you want to execute on ip ban");
            this.txt_RunOnBan.Click += new System.EventHandler(this.txt_RunOnBan_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(251, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "milliseconds";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(251, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "milliseconds";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Awesom-O Directory:";
            // 
            // gb_Expert
            // 
            this.gb_Expert.Controls.Add(this.txt_RunOnBan);
            this.gb_Expert.Controls.Add(this.chk_RunOnBan);
            this.gb_Expert.Controls.Add(this.buttonConfIp);
            this.gb_Expert.Controls.Add(this.checkBoxIpChanger);
            this.gb_Expert.Controls.Add(this.chk_AoNoStart);
            this.gb_Expert.Controls.Add(this.chk_Cip);
            this.gb_Expert.Controls.Add(this.txt_Cip);
            this.gb_Expert.Controls.Add(this.txtAoFlags);
            this.gb_Expert.Controls.Add(this.chk_Expert);
            this.gb_Expert.Location = new System.Drawing.Point(15, 223);
            this.gb_Expert.Name = "gb_Expert";
            this.gb_Expert.Size = new System.Drawing.Size(299, 143);
            this.gb_Expert.TabIndex = 40;
            this.gb_Expert.TabStop = false;
            this.gb_Expert.Text = "Expert Settings";
            // 
            // buttonConfIp
            // 
            this.buttonConfIp.Location = new System.Drawing.Point(125, 116);
            this.buttonConfIp.Name = "buttonConfIp";
            this.buttonConfIp.Size = new System.Drawing.Size(75, 23);
            this.buttonConfIp.TabIndex = 6;
            this.buttonConfIp.Text = "Configure";
            this.buttonConfIp.UseVisualStyleBackColor = true;
            this.buttonConfIp.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBoxIpChanger
            // 
            this.checkBoxIpChanger.AutoSize = true;
            this.checkBoxIpChanger.Location = new System.Drawing.Point(6, 120);
            this.checkBoxIpChanger.Name = "checkBoxIpChanger";
            this.checkBoxIpChanger.Size = new System.Drawing.Size(113, 17);
            this.checkBoxIpChanger.TabIndex = 5;
            this.checkBoxIpChanger.Text = "Change IP on Ban";
            this.checkBoxIpChanger.UseVisualStyleBackColor = true;
            this.checkBoxIpChanger.CheckedChanged += new System.EventHandler(this.checkBoxIpChanger_CheckedChanged);
            // 
            // chk_AoNoStart
            // 
            this.chk_AoNoStart.AutoSize = true;
            this.chk_AoNoStart.Location = new System.Drawing.Point(6, 71);
            this.chk_AoNoStart.Name = "chk_AoNoStart";
            this.chk_AoNoStart.Size = new System.Drawing.Size(98, 17);
            this.chk_AoNoStart.TabIndex = 4;
            this.chk_AoNoStart.Text = "Don\'t start core";
            this.chk_AoNoStart.UseVisualStyleBackColor = true;
            // 
            // chk_Cip
            // 
            this.chk_Cip.AutoSize = true;
            this.chk_Cip.Location = new System.Drawing.Point(6, 48);
            this.chk_Cip.Name = "chk_Cip";
            this.chk_Cip.Size = new System.Drawing.Size(106, 17);
            this.chk_Cip.TabIndex = 3;
            this.chk_Cip.Text = "Use Controller ip:";
            this.chk_Cip.UseVisualStyleBackColor = true;
            this.chk_Cip.CheckedChanged += new System.EventHandler(this.chk_Cip_CheckedChanged);
            // 
            // txt_Cip
            // 
            this.txt_Cip.Enabled = false;
            this.txt_Cip.Location = new System.Drawing.Point(116, 45);
            this.txt_Cip.Name = "txt_Cip";
            this.txt_Cip.Size = new System.Drawing.Size(114, 20);
            this.txt_Cip.TabIndex = 2;
            this.txt_Cip.Text = "127.0.0.1";
            // 
            // txtAoFlags
            // 
            this.txtAoFlags.Enabled = false;
            this.txtAoFlags.Location = new System.Drawing.Point(116, 19);
            this.txtAoFlags.Name = "txtAoFlags";
            this.txtAoFlags.Size = new System.Drawing.Size(114, 20);
            this.txtAoFlags.TabIndex = 1;
            // 
            // chk_Expert
            // 
            this.chk_Expert.AutoSize = true;
            this.chk_Expert.Location = new System.Drawing.Point(6, 22);
            this.chk_Expert.Name = "chk_Expert";
            this.chk_Expert.Size = new System.Drawing.Size(96, 17);
            this.chk_Expert.TabIndex = 0;
            this.chk_Expert.Text = "Use own Flags";
            this.chk_Expert.UseVisualStyleBackColor = true;
            this.chk_Expert.CheckedChanged += new System.EventHandler(this.chk_Expert_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 41;
            this.label4.Text = "Diablo II Directory:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(251, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "milliseconds";
            // 
            // frmAoSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 392);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.chk_HideCore);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxStartDelay);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxDiabloDirectory);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gb_Expert);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chk_Stealth);
            this.Controls.Add(this.comboBoxRealm);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBoxAwesomoDirectory);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxClickDelay);
            this.Controls.Add(this.textBoxKeyDelay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmAoSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Awesom-O Core Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Global_Settings_Config_FormClosing);
            this.gb_Expert.ResumeLayout(false);
            this.gb_Expert.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxKeyDelay;
        private System.Windows.Forms.TextBox textBoxClickDelay;
        private System.Windows.Forms.ToolTip toolTiper;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxAwesomoDirectory;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxRealm;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chk_Stealth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gb_Expert;
        private System.Windows.Forms.CheckBox chk_Expert;
        private System.Windows.Forms.TextBox txtAoFlags;
        private System.Windows.Forms.TextBox textBoxDiabloDirectory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxStartDelay;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chk_Cip;
        private System.Windows.Forms.TextBox txt_Cip;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chk_HideCore;
        private System.Windows.Forms.CheckBox chk_AoNoStart;
        private System.Windows.Forms.CheckBox checkBoxIpChanger;
        private System.Windows.Forms.Button buttonConfIp;
        private System.Windows.Forms.TextBox txt_RunOnBan;
        private System.Windows.Forms.CheckBox chk_RunOnBan;
    }
}