﻿namespace AoManager
{
    partial class frmBotSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBotSettings));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageDelay = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.textBoxCantConnectDelay = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.textBoxCdKeyInUseDelay = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.textBoxGameJoinTooLongDelay = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.textBoxLoginTooLongDelay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCreateGameDelay = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxStartRunDelay = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxTempBanSleepTime = new System.Windows.Forms.TextBox();
            this.tabControlProfiles = new System.Windows.Forms.TabPage();
            this.gb_keys = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxKeyChange = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.buttonExpansionKeyRemove = new System.Windows.Forms.Button();
            this.buttonClassicKeyRemove = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.listBoxExpansionKeys = new System.Windows.Forms.ListBox();
            this.listBoxClassicKeys = new System.Windows.Forms.ListBox();
            this.textBoxExpansionKey = new System.Windows.Forms.TextBox();
            this.buttonExpansionKeyAdd = new System.Windows.Forms.Button();
            this.buttonClassicKeyAdd = new System.Windows.Forms.Button();
            this.textBoxClassicKey = new System.Windows.Forms.TextBox();
            this.gb_Game = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxMaxGamesHour = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxMaxGameTime = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxMinGameTime = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxFirstJoinChannelMessage = new System.Windows.Forms.TextBox();
            this.checkBoxNoPass = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxDifficulty = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxTooShortMessage = new System.Windows.Forms.TextBox();
            this.textBoxChannel = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxGameName = new System.Windows.Forms.TextBox();
            this.textBoxNextGameMessage = new System.Windows.Forms.TextBox();
            this.textBoxGamePass = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.gb_logindata = new System.Windows.Forms.GroupBox();
            this.chk_AutoStartRun = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDiabloFolderDirectory = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxOwnerName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chk_Hide = new System.Windows.Forms.CheckBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.radioButtonCharPos0 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos1 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos2 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos3 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos4 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos5 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos6 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos7 = new System.Windows.Forms.RadioButton();
            this.gb_Profiles = new System.Windows.Forms.GroupBox();
            this.button_Save = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.textBoxProfileName = new System.Windows.Forms.TextBox();
            this.listBoxProfiles = new System.Windows.Forms.ListBox();
            this.buttonProfileRemove = new System.Windows.Forms.Button();
            this.buttonProfileAdd = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonNothing = new System.Windows.Forms.RadioButton();
            this.radioButtonFollow = new System.Windows.Forms.RadioButton();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox_JoinGameDelay = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBoxMasterCharacter = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBoxFollowPassword = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxMasterAccount = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbl_warning = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBoxMulePass = new System.Windows.Forms.TextBox();
            this.textBoxMuleAccount = new System.Windows.Forms.TextBox();
            this.checkBoxAutoMule = new System.Windows.Forms.CheckBox();
            this.tabControl.SuspendLayout();
            this.tabPageDelay.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControlProfiles.SuspendLayout();
            this.gb_keys.SuspendLayout();
            this.gb_Game.SuspendLayout();
            this.gb_logindata.SuspendLayout();
            this.gb_Profiles.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageDelay);
            this.tabControl.Controls.Add(this.tabControlProfiles);
            this.tabControl.Location = new System.Drawing.Point(0, 1);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(616, 659);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageDelay
            // 
            this.tabPageDelay.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPageDelay.Controls.Add(this.groupBox3);
            this.tabPageDelay.Controls.Add(this.groupBox1);
            this.tabPageDelay.Location = new System.Drawing.Point(4, 22);
            this.tabPageDelay.Name = "tabPageDelay";
            this.tabPageDelay.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDelay.Size = new System.Drawing.Size(608, 633);
            this.tabPageDelay.TabIndex = 0;
            this.tabPageDelay.Text = "Manager Delays & Muling";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label52);
            this.groupBox1.Controls.Add(this.label53);
            this.groupBox1.Controls.Add(this.textBoxCantConnectDelay);
            this.groupBox1.Controls.Add(this.label50);
            this.groupBox1.Controls.Add(this.label51);
            this.groupBox1.Controls.Add(this.textBoxCdKeyInUseDelay);
            this.groupBox1.Controls.Add(this.label48);
            this.groupBox1.Controls.Add(this.label49);
            this.groupBox1.Controls.Add(this.textBoxGameJoinTooLongDelay);
            this.groupBox1.Controls.Add(this.label47);
            this.groupBox1.Controls.Add(this.label46);
            this.groupBox1.Controls.Add(this.textBoxLoginTooLongDelay);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxCreateGameDelay);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textBoxStartRunDelay);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.textBoxTempBanSleepTime);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(2, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 223);
            this.groupBox1.TabIndex = 87;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Timers && Delay Settings";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(312, 180);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(43, 13);
            this.label52.TabIndex = 90;
            this.label52.Text = "minutes";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(7, 180);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(104, 13);
            this.label53.TabIndex = 89;
            this.label53.Text = "Can\'t Connect Delay";
            // 
            // textBoxCantConnectDelay
            // 
            this.textBoxCantConnectDelay.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxCantConnectDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCantConnectDelay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxCantConnectDelay.Location = new System.Drawing.Point(151, 177);
            this.textBoxCantConnectDelay.Name = "textBoxCantConnectDelay";
            this.textBoxCantConnectDelay.Size = new System.Drawing.Size(155, 20);
            this.textBoxCantConnectDelay.TabIndex = 88;
            this.toolTip.SetToolTip(this.textBoxCantConnectDelay, "Delay after AO detected no internet connection is established");
            this.textBoxCantConnectDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.textBoxCantConnectDelay.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(312, 154);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(43, 13);
            this.label50.TabIndex = 87;
            this.label50.Text = "minutes";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(7, 154);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(107, 13);
            this.label51.TabIndex = 86;
            this.label51.Text = "CD Key In Use Delay";
            // 
            // textBoxCdKeyInUseDelay
            // 
            this.textBoxCdKeyInUseDelay.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxCdKeyInUseDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCdKeyInUseDelay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxCdKeyInUseDelay.Location = new System.Drawing.Point(151, 151);
            this.textBoxCdKeyInUseDelay.Name = "textBoxCdKeyInUseDelay";
            this.textBoxCdKeyInUseDelay.Size = new System.Drawing.Size(155, 20);
            this.textBoxCdKeyInUseDelay.TabIndex = 85;
            this.toolTip.SetToolTip(this.textBoxCdKeyInUseDelay, "Wait time after receiving a \"CD key in use\" from bnet ");
            this.textBoxCdKeyInUseDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.textBoxCdKeyInUseDelay.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(312, 128);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(47, 13);
            this.label48.TabIndex = 84;
            this.label48.Text = "seconds";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(7, 128);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(139, 13);
            this.label49.TabIndex = 83;
            this.label49.Text = "Game Join Too Long Delay:";
            // 
            // textBoxGameJoinTooLongDelay
            // 
            this.textBoxGameJoinTooLongDelay.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxGameJoinTooLongDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGameJoinTooLongDelay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxGameJoinTooLongDelay.Location = new System.Drawing.Point(151, 125);
            this.textBoxGameJoinTooLongDelay.Name = "textBoxGameJoinTooLongDelay";
            this.textBoxGameJoinTooLongDelay.Size = new System.Drawing.Size(155, 20);
            this.textBoxGameJoinTooLongDelay.TabIndex = 82;
            this.toolTip.SetToolTip(this.textBoxGameJoinTooLongDelay, "Will restart bot if it takes longer than this amount of time to join a game prope" +
                    "rly");
            this.textBoxGameJoinTooLongDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.textBoxGameJoinTooLongDelay.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(312, 102);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(47, 13);
            this.label47.TabIndex = 81;
            this.label47.Text = "seconds";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(7, 102);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(115, 13);
            this.label46.TabIndex = 80;
            this.label46.Text = "Login Too Long Delay:";
            // 
            // textBoxLoginTooLongDelay
            // 
            this.textBoxLoginTooLongDelay.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxLoginTooLongDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLoginTooLongDelay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxLoginTooLongDelay.Location = new System.Drawing.Point(151, 99);
            this.textBoxLoginTooLongDelay.Name = "textBoxLoginTooLongDelay";
            this.textBoxLoginTooLongDelay.Size = new System.Drawing.Size(155, 20);
            this.textBoxLoginTooLongDelay.TabIndex = 79;
            this.toolTip.SetToolTip(this.textBoxLoginTooLongDelay, "Will restart bot if it takes longer than this amount of time to login into bnet");
            this.textBoxLoginTooLongDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.textBoxLoginTooLongDelay.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(312, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 76;
            this.label2.Text = "minutes";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(7, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 13);
            this.label20.TabIndex = 68;
            this.label20.Text = "Create Game Delay:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(312, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 75;
            this.label4.Text = "seconds";
            // 
            // textBoxCreateGameDelay
            // 
            this.textBoxCreateGameDelay.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxCreateGameDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCreateGameDelay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxCreateGameDelay.Location = new System.Drawing.Point(151, 21);
            this.textBoxCreateGameDelay.Name = "textBoxCreateGameDelay";
            this.textBoxCreateGameDelay.Size = new System.Drawing.Size(155, 20);
            this.textBoxCreateGameDelay.TabIndex = 69;
            this.toolTip.SetToolTip(this.textBoxCreateGameDelay, "Delay before creating a new game");
            this.textBoxCreateGameDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.textBoxCreateGameDelay.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(312, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 13);
            this.label15.TabIndex = 74;
            this.label15.Text = "seconds";
            // 
            // textBoxStartRunDelay
            // 
            this.textBoxStartRunDelay.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxStartRunDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStartRunDelay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxStartRunDelay.Location = new System.Drawing.Point(151, 47);
            this.textBoxStartRunDelay.Name = "textBoxStartRunDelay";
            this.textBoxStartRunDelay.Size = new System.Drawing.Size(155, 20);
            this.textBoxStartRunDelay.TabIndex = 70;
            this.toolTip.SetToolTip(this.textBoxStartRunDelay, "The delay before starting the bot in a game");
            this.textBoxStartRunDelay.TextChanged += new System.EventHandler(this.textBoxStartRunDelay_TextChanged);
            this.textBoxStartRunDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.textBoxStartRunDelay.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(7, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(115, 13);
            this.label18.TabIndex = 73;
            this.label18.Text = "Temp Ban Sleep Time:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 13);
            this.label19.TabIndex = 71;
            this.label19.Text = "Start Run Delay:";
            // 
            // textBoxTempBanSleepTime
            // 
            this.textBoxTempBanSleepTime.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxTempBanSleepTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTempBanSleepTime.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxTempBanSleepTime.Location = new System.Drawing.Point(151, 73);
            this.textBoxTempBanSleepTime.Name = "textBoxTempBanSleepTime";
            this.textBoxTempBanSleepTime.Size = new System.Drawing.Size(155, 20);
            this.textBoxTempBanSleepTime.TabIndex = 72;
            this.toolTip.SetToolTip(this.textBoxTempBanSleepTime, "Wait time after ip ban");
            this.textBoxTempBanSleepTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.textBoxTempBanSleepTime.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // tabControlProfiles
            // 
            this.tabControlProfiles.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabControlProfiles.Controls.Add(this.groupBox2);
            this.tabControlProfiles.Controls.Add(this.gb_keys);
            this.tabControlProfiles.Controls.Add(this.gb_Game);
            this.tabControlProfiles.Controls.Add(this.gb_logindata);
            this.tabControlProfiles.Controls.Add(this.gb_Profiles);
            this.tabControlProfiles.Location = new System.Drawing.Point(4, 22);
            this.tabControlProfiles.Name = "tabControlProfiles";
            this.tabControlProfiles.Padding = new System.Windows.Forms.Padding(3);
            this.tabControlProfiles.Size = new System.Drawing.Size(608, 633);
            this.tabControlProfiles.TabIndex = 1;
            this.tabControlProfiles.Text = "Bot Profiles";
            // 
            // gb_keys
            // 
            this.gb_keys.Controls.Add(this.label28);
            this.gb_keys.Controls.Add(this.textBoxKeyChange);
            this.gb_keys.Controls.Add(this.label29);
            this.gb_keys.Controls.Add(this.buttonExpansionKeyRemove);
            this.gb_keys.Controls.Add(this.buttonClassicKeyRemove);
            this.gb_keys.Controls.Add(this.label30);
            this.gb_keys.Controls.Add(this.label31);
            this.gb_keys.Controls.Add(this.listBoxExpansionKeys);
            this.gb_keys.Controls.Add(this.listBoxClassicKeys);
            this.gb_keys.Controls.Add(this.textBoxExpansionKey);
            this.gb_keys.Controls.Add(this.buttonExpansionKeyAdd);
            this.gb_keys.Controls.Add(this.buttonClassicKeyAdd);
            this.gb_keys.Controls.Add(this.textBoxClassicKey);
            this.gb_keys.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_keys.Location = new System.Drawing.Point(243, 6);
            this.gb_keys.Name = "gb_keys";
            this.gb_keys.Size = new System.Drawing.Size(346, 174);
            this.gb_keys.TabIndex = 92;
            this.gb_keys.TabStop = false;
            this.gb_keys.Text = "Cd Keys";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(272, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(38, 13);
            this.label28.TabIndex = 89;
            this.label28.Text = "games";
            // 
            // textBoxKeyChange
            // 
            this.textBoxKeyChange.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxKeyChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxKeyChange.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxKeyChange.Location = new System.Drawing.Point(105, 15);
            this.textBoxKeyChange.MaxLength = 15;
            this.textBoxKeyChange.Name = "textBoxKeyChange";
            this.textBoxKeyChange.Size = new System.Drawing.Size(161, 20);
            this.textBoxKeyChange.TabIndex = 84;
            this.textBoxKeyChange.Text = "0";
            this.toolTip.SetToolTip(this.textBoxKeyChange, "Change keys after this amount of games.");
            this.textBoxKeyChange.TextChanged += new System.EventHandler(this.textbox_textChanged);
            this.textBoxKeyChange.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.textBoxKeyChange.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(6, 18);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(93, 13);
            this.label29.TabIndex = 88;
            this.label29.Text = "Change keys after";
            // 
            // buttonExpansionKeyRemove
            // 
            this.buttonExpansionKeyRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExpansionKeyRemove.Location = new System.Drawing.Point(260, 65);
            this.buttonExpansionKeyRemove.Name = "buttonExpansionKeyRemove";
            this.buttonExpansionKeyRemove.Size = new System.Drawing.Size(65, 20);
            this.buttonExpansionKeyRemove.TabIndex = 87;
            this.buttonExpansionKeyRemove.Text = "Remove";
            this.buttonExpansionKeyRemove.UseVisualStyleBackColor = true;
            this.buttonExpansionKeyRemove.Click += new System.EventHandler(this.buttonExpansionKeyRemove_Click);
            // 
            // buttonClassicKeyRemove
            // 
            this.buttonClassicKeyRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClassicKeyRemove.Location = new System.Drawing.Point(92, 65);
            this.buttonClassicKeyRemove.Name = "buttonClassicKeyRemove";
            this.buttonClassicKeyRemove.Size = new System.Drawing.Size(65, 20);
            this.buttonClassicKeyRemove.TabIndex = 86;
            this.buttonClassicKeyRemove.Text = "Remove";
            this.buttonClassicKeyRemove.UseVisualStyleBackColor = true;
            this.buttonClassicKeyRemove.Click += new System.EventHandler(this.buttonClassicKeyRemove_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(217, 50);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(80, 13);
            this.label30.TabIndex = 85;
            this.label30.Text = "Expansion Key:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(60, 49);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 13);
            this.label31.TabIndex = 84;
            this.label31.Text = "Classic Key:";
            // 
            // listBoxExpansionKeys
            // 
            this.listBoxExpansionKeys.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.listBoxExpansionKeys.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxExpansionKeys.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.listBoxExpansionKeys.FormattingEnabled = true;
            this.listBoxExpansionKeys.Location = new System.Drawing.Point(179, 117);
            this.listBoxExpansionKeys.Name = "listBoxExpansionKeys";
            this.listBoxExpansionKeys.Size = new System.Drawing.Size(161, 43);
            this.listBoxExpansionKeys.TabIndex = 5;
            // 
            // listBoxClassicKeys
            // 
            this.listBoxClassicKeys.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.listBoxClassicKeys.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxClassicKeys.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.listBoxClassicKeys.FormattingEnabled = true;
            this.listBoxClassicKeys.Location = new System.Drawing.Point(9, 117);
            this.listBoxClassicKeys.Name = "listBoxClassicKeys";
            this.listBoxClassicKeys.Size = new System.Drawing.Size(161, 43);
            this.listBoxClassicKeys.TabIndex = 4;
            // 
            // textBoxExpansionKey
            // 
            this.textBoxExpansionKey.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxExpansionKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxExpansionKey.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxExpansionKey.Location = new System.Drawing.Point(179, 91);
            this.textBoxExpansionKey.MaxLength = 16;
            this.textBoxExpansionKey.Name = "textBoxExpansionKey";
            this.textBoxExpansionKey.Size = new System.Drawing.Size(161, 20);
            this.textBoxExpansionKey.TabIndex = 3;
            // 
            // buttonExpansionKeyAdd
            // 
            this.buttonExpansionKeyAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExpansionKeyAdd.Location = new System.Drawing.Point(189, 65);
            this.buttonExpansionKeyAdd.Name = "buttonExpansionKeyAdd";
            this.buttonExpansionKeyAdd.Size = new System.Drawing.Size(65, 20);
            this.buttonExpansionKeyAdd.TabIndex = 2;
            this.buttonExpansionKeyAdd.Text = "Add";
            this.buttonExpansionKeyAdd.UseVisualStyleBackColor = true;
            this.buttonExpansionKeyAdd.Click += new System.EventHandler(this.buttonExpansionKeyAdd_Click);
            // 
            // buttonClassicKeyAdd
            // 
            this.buttonClassicKeyAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClassicKeyAdd.Location = new System.Drawing.Point(18, 65);
            this.buttonClassicKeyAdd.Name = "buttonClassicKeyAdd";
            this.buttonClassicKeyAdd.Size = new System.Drawing.Size(65, 20);
            this.buttonClassicKeyAdd.TabIndex = 1;
            this.buttonClassicKeyAdd.Text = "Add";
            this.buttonClassicKeyAdd.UseVisualStyleBackColor = true;
            this.buttonClassicKeyAdd.Click += new System.EventHandler(this.buttonClassicKeyAdd_Click);
            // 
            // textBoxClassicKey
            // 
            this.textBoxClassicKey.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxClassicKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxClassicKey.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxClassicKey.Location = new System.Drawing.Point(9, 91);
            this.textBoxClassicKey.MaxLength = 16;
            this.textBoxClassicKey.Name = "textBoxClassicKey";
            this.textBoxClassicKey.Size = new System.Drawing.Size(161, 20);
            this.textBoxClassicKey.TabIndex = 0;
            // 
            // gb_Game
            // 
            this.gb_Game.Controls.Add(this.label21);
            this.gb_Game.Controls.Add(this.textBoxMaxGamesHour);
            this.gb_Game.Controls.Add(this.label12);
            this.gb_Game.Controls.Add(this.textBoxMaxGameTime);
            this.gb_Game.Controls.Add(this.label13);
            this.gb_Game.Controls.Add(this.textBoxMinGameTime);
            this.gb_Game.Controls.Add(this.label16);
            this.gb_Game.Controls.Add(this.label17);
            this.gb_Game.Controls.Add(this.label33);
            this.gb_Game.Controls.Add(this.textBoxFirstJoinChannelMessage);
            this.gb_Game.Controls.Add(this.checkBoxNoPass);
            this.gb_Game.Controls.Add(this.label10);
            this.gb_Game.Controls.Add(this.comboBoxDifficulty);
            this.gb_Game.Controls.Add(this.label23);
            this.gb_Game.Controls.Add(this.label8);
            this.gb_Game.Controls.Add(this.textBoxTooShortMessage);
            this.gb_Game.Controls.Add(this.textBoxChannel);
            this.gb_Game.Controls.Add(this.label9);
            this.gb_Game.Controls.Add(this.label22);
            this.gb_Game.Controls.Add(this.textBoxGameName);
            this.gb_Game.Controls.Add(this.textBoxNextGameMessage);
            this.gb_Game.Controls.Add(this.textBoxGamePass);
            this.gb_Game.Controls.Add(this.label11);
            this.gb_Game.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_Game.Location = new System.Drawing.Point(243, 186);
            this.gb_Game.Name = "gb_Game";
            this.gb_Game.Size = new System.Drawing.Size(346, 300);
            this.gb_Game.TabIndex = 91;
            this.gb_Game.TabStop = false;
            this.gb_Game.Text = "Game Settings";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(6, 106);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 13);
            this.label21.TabIndex = 102;
            this.label21.Text = "Max games hour:";
            // 
            // textBoxMaxGamesHour
            // 
            this.textBoxMaxGamesHour.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxMaxGamesHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMaxGamesHour.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxMaxGamesHour.Location = new System.Drawing.Point(150, 103);
            this.textBoxMaxGamesHour.Name = "textBoxMaxGamesHour";
            this.textBoxMaxGamesHour.Size = new System.Drawing.Size(190, 20);
            this.textBoxMaxGamesHour.TabIndex = 101;
            this.textBoxMaxGamesHour.Text = "16";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 132);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 13);
            this.label12.TabIndex = 95;
            this.label12.Text = "Minimum Game Time:";
            // 
            // textBoxMaxGameTime
            // 
            this.textBoxMaxGameTime.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxMaxGameTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMaxGameTime.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxMaxGameTime.Location = new System.Drawing.Point(150, 155);
            this.textBoxMaxGameTime.MaxLength = 4;
            this.textBoxMaxGameTime.Name = "textBoxMaxGameTime";
            this.textBoxMaxGameTime.Size = new System.Drawing.Size(132, 20);
            this.textBoxMaxGameTime.TabIndex = 96;
            this.textBoxMaxGameTime.Text = "360";
            this.toolTip.SetToolTip(this.textBoxMaxGameTime, "Maximun game time. The bot will restart if its longer than this time in a game");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 158);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 13);
            this.label13.TabIndex = 97;
            this.label13.Text = "Maximum Game Time:";
            // 
            // textBoxMinGameTime
            // 
            this.textBoxMinGameTime.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxMinGameTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMinGameTime.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxMinGameTime.Location = new System.Drawing.Point(150, 129);
            this.textBoxMinGameTime.MaxLength = 4;
            this.textBoxMinGameTime.Name = "textBoxMinGameTime";
            this.textBoxMinGameTime.Size = new System.Drawing.Size(132, 20);
            this.textBoxMinGameTime.TabIndex = 98;
            this.textBoxMinGameTime.Text = "180";
            this.toolTip.SetToolTip(this.textBoxMinGameTime, "Minimum game time.");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(293, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 13);
            this.label16.TabIndex = 99;
            this.label16.Text = "seconds";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(293, 158);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 13);
            this.label17.TabIndex = 100;
            this.label17.Text = "seconds";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(5, 212);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(139, 13);
            this.label33.TabIndex = 86;
            this.label33.Text = "First Join Channel Message:";
            // 
            // textBoxFirstJoinChannelMessage
            // 
            this.textBoxFirstJoinChannelMessage.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxFirstJoinChannelMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFirstJoinChannelMessage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxFirstJoinChannelMessage.Location = new System.Drawing.Point(149, 209);
            this.textBoxFirstJoinChannelMessage.MaxLength = 15;
            this.textBoxFirstJoinChannelMessage.Name = "textBoxFirstJoinChannelMessage";
            this.textBoxFirstJoinChannelMessage.Size = new System.Drawing.Size(190, 20);
            this.textBoxFirstJoinChannelMessage.TabIndex = 85;
            this.toolTip.SetToolTip(this.textBoxFirstJoinChannelMessage, "Message after joining a channel the first time.Used for !login commands etc.");
            this.textBoxFirstJoinChannelMessage.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // checkBoxNoPass
            // 
            this.checkBoxNoPass.AutoSize = true;
            this.checkBoxNoPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.checkBoxNoPass.Location = new System.Drawing.Point(288, 51);
            this.checkBoxNoPass.Name = "checkBoxNoPass";
            this.checkBoxNoPass.Size = new System.Drawing.Size(52, 17);
            this.checkBoxNoPass.TabIndex = 84;
            this.checkBoxNoPass.Text = "None";
            this.checkBoxNoPass.UseVisualStyleBackColor = true;
            this.checkBoxNoPass.CheckedChanged += new System.EventHandler(this.checkBoxNoPass_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 54;
            this.label10.Text = "Game Name:";
            // 
            // comboBoxDifficulty
            // 
            this.comboBoxDifficulty.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.comboBoxDifficulty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDifficulty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDifficulty.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.comboBoxDifficulty.FormattingEnabled = true;
            this.comboBoxDifficulty.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.comboBoxDifficulty.Items.AddRange(new object[] {
            "Hell",
            "Nightmare",
            "Normal"});
            this.comboBoxDifficulty.Location = new System.Drawing.Point(150, 75);
            this.comboBoxDifficulty.MaxDropDownItems = 4;
            this.comboBoxDifficulty.Name = "comboBoxDifficulty";
            this.comboBoxDifficulty.Size = new System.Drawing.Size(190, 21);
            this.comboBoxDifficulty.TabIndex = 49;
            this.comboBoxDifficulty.SelectedIndexChanged += new System.EventHandler(this.comboBoxDifficulty_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 264);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(103, 13);
            this.label23.TabIndex = 83;
            this.label23.Text = "Too Short Message:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 50;
            this.label8.Text = "Difficulty:";
            // 
            // textBoxTooShortMessage
            // 
            this.textBoxTooShortMessage.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxTooShortMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTooShortMessage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxTooShortMessage.Location = new System.Drawing.Point(149, 261);
            this.textBoxTooShortMessage.MaxLength = 15;
            this.textBoxTooShortMessage.Name = "textBoxTooShortMessage";
            this.textBoxTooShortMessage.Size = new System.Drawing.Size(190, 20);
            this.textBoxTooShortMessage.TabIndex = 82;
            this.toolTip.SetToolTip(this.textBoxTooShortMessage, "Message to be sent in channel if last game was too short!");
            this.textBoxTooShortMessage.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // textBoxChannel
            // 
            this.textBoxChannel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxChannel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxChannel.Location = new System.Drawing.Point(149, 183);
            this.textBoxChannel.MaxLength = 21;
            this.textBoxChannel.Name = "textBoxChannel";
            this.textBoxChannel.Size = new System.Drawing.Size(190, 20);
            this.textBoxChannel.TabIndex = 51;
            this.toolTip.SetToolTip(this.textBoxChannel, "Chat Channel. Leave it empty if you dont want to join a channel");
            this.textBoxChannel.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 186);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "Channel:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(5, 238);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(109, 13);
            this.label22.TabIndex = 80;
            this.label22.Text = "Next Game Message:";
            // 
            // textBoxGameName
            // 
            this.textBoxGameName.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxGameName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGameName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxGameName.Location = new System.Drawing.Point(150, 23);
            this.textBoxGameName.MaxLength = 15;
            this.textBoxGameName.Name = "textBoxGameName";
            this.textBoxGameName.Size = new System.Drawing.Size(190, 20);
            this.textBoxGameName.TabIndex = 53;
            this.toolTip.SetToolTip(this.textBoxGameName, "How do you want to name your games? Leave it empty for random game names!");
            this.textBoxGameName.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // textBoxNextGameMessage
            // 
            this.textBoxNextGameMessage.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxNextGameMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNextGameMessage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxNextGameMessage.Location = new System.Drawing.Point(149, 235);
            this.textBoxNextGameMessage.MaxLength = 15;
            this.textBoxNextGameMessage.Name = "textBoxNextGameMessage";
            this.textBoxNextGameMessage.Size = new System.Drawing.Size(190, 20);
            this.textBoxNextGameMessage.TabIndex = 79;
            this.toolTip.SetToolTip(this.textBoxNextGameMessage, "Message which announces next game in channel!");
            this.textBoxNextGameMessage.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // textBoxGamePass
            // 
            this.textBoxGamePass.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxGamePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGamePass.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxGamePass.Location = new System.Drawing.Point(150, 49);
            this.textBoxGamePass.MaxLength = 15;
            this.textBoxGamePass.Name = "textBoxGamePass";
            this.textBoxGamePass.Size = new System.Drawing.Size(132, 20);
            this.textBoxGamePass.TabIndex = 55;
            this.textBoxGamePass.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 13);
            this.label11.TabIndex = 56;
            this.label11.Text = "Game Password:";
            // 
            // gb_logindata
            // 
            this.gb_logindata.Controls.Add(this.chk_AutoStartRun);
            this.gb_logindata.Controls.Add(this.label1);
            this.gb_logindata.Controls.Add(this.textBoxDiabloFolderDirectory);
            this.gb_logindata.Controls.Add(this.label3);
            this.gb_logindata.Controls.Add(this.textBoxOwnerName);
            this.gb_logindata.Controls.Add(this.label5);
            this.gb_logindata.Controls.Add(this.chk_Hide);
            this.gb_logindata.Controls.Add(this.textBoxUsername);
            this.gb_logindata.Controls.Add(this.label14);
            this.gb_logindata.Controls.Add(this.textBoxPassword);
            this.gb_logindata.Controls.Add(this.label6);
            this.gb_logindata.Controls.Add(this.label7);
            this.gb_logindata.Controls.Add(this.radioButtonCharPos0);
            this.gb_logindata.Controls.Add(this.radioButtonCharPos1);
            this.gb_logindata.Controls.Add(this.radioButtonCharPos2);
            this.gb_logindata.Controls.Add(this.radioButtonCharPos3);
            this.gb_logindata.Controls.Add(this.radioButtonCharPos4);
            this.gb_logindata.Controls.Add(this.radioButtonCharPos5);
            this.gb_logindata.Controls.Add(this.radioButtonCharPos6);
            this.gb_logindata.Controls.Add(this.radioButtonCharPos7);
            this.gb_logindata.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_logindata.Location = new System.Drawing.Point(3, 186);
            this.gb_logindata.Name = "gb_logindata";
            this.gb_logindata.Size = new System.Drawing.Size(234, 300);
            this.gb_logindata.TabIndex = 90;
            this.gb_logindata.TabStop = false;
            this.gb_logindata.Text = "Login Data";
            // 
            // chk_AutoStartRun
            // 
            this.chk_AutoStartRun.AutoSize = true;
            this.chk_AutoStartRun.Checked = true;
            this.chk_AutoStartRun.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_AutoStartRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_AutoStartRun.Location = new System.Drawing.Point(64, 277);
            this.chk_AutoStartRun.Name = "chk_AutoStartRun";
            this.chk_AutoStartRun.Size = new System.Drawing.Size(96, 17);
            this.chk_AutoStartRun.TabIndex = 85;
            this.chk_AutoStartRun.Text = "Auto Start Run";
            this.toolTip.SetToolTip(this.chk_AutoStartRun, "Start bot in game or not!");
            this.chk_AutoStartRun.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Diablo II Directory:";
            // 
            // textBoxDiabloFolderDirectory
            // 
            this.textBoxDiabloFolderDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDiabloFolderDirectory.Location = new System.Drawing.Point(106, 22);
            this.textBoxDiabloFolderDirectory.Name = "textBoxDiabloFolderDirectory";
            this.textBoxDiabloFolderDirectory.ReadOnly = true;
            this.textBoxDiabloFolderDirectory.Size = new System.Drawing.Size(122, 20);
            this.textBoxDiabloFolderDirectory.TabIndex = 29;
            this.toolTip.SetToolTip(this.textBoxDiabloFolderDirectory, "Note: For multibotting you will need to select different Diablo folders!");
            this.textBoxDiabloFolderDirectory.Click += new System.EventHandler(this.textBoxDiabloFolderDirectory_Click);
            this.textBoxDiabloFolderDirectory.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Owner Name:";
            // 
            // textBoxOwnerName
            // 
            this.textBoxOwnerName.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxOwnerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOwnerName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxOwnerName.Location = new System.Drawing.Point(106, 48);
            this.textBoxOwnerName.Name = "textBoxOwnerName";
            this.textBoxOwnerName.Size = new System.Drawing.Size(122, 20);
            this.textBoxOwnerName.TabIndex = 33;
            this.toolTip.SetToolTip(this.textBoxOwnerName, "The name you installed Diablo classic with. Note: you will need to have different" +
                    " owner names for multi botting!");
            this.textBoxOwnerName.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Account Name:";
            // 
            // chk_Hide
            // 
            this.chk_Hide.AutoSize = true;
            this.chk_Hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Hide.Location = new System.Drawing.Point(106, 260);
            this.chk_Hide.Name = "chk_Hide";
            this.chk_Hide.Size = new System.Drawing.Size(90, 17);
            this.chk_Hide.TabIndex = 81;
            this.chk_Hide.Text = "Hide Window";
            this.toolTip.SetToolTip(this.chk_Hide, "Minimize and hide Diablo");
            this.chk_Hide.UseVisualStyleBackColor = true;
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUsername.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxUsername.Location = new System.Drawing.Point(106, 74);
            this.textBoxUsername.MaxLength = 15;
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(122, 20);
            this.textBoxUsername.TabIndex = 37;
            this.toolTip.SetToolTip(this.textBoxUsername, "The bnet Account name!");
            this.textBoxUsername.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 261);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 13);
            this.label14.TabIndex = 63;
            this.label14.Text = "Diablo II Window:";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPassword.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxPassword.Location = new System.Drawing.Point(106, 100);
            this.textBoxPassword.MaxLength = 12;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(122, 20);
            this.textBoxPassword.TabIndex = 38;
            this.textBoxPassword.UseSystemPasswordChar = true;
            this.textBoxPassword.TextChanged += new System.EventHandler(this.textbox_textChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Password:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(64, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "Character Position:";
            // 
            // radioButtonCharPos0
            // 
            this.radioButtonCharPos0.AutoSize = true;
            this.radioButtonCharPos0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCharPos0.Location = new System.Drawing.Point(67, 151);
            this.radioButtonCharPos0.Name = "radioButtonCharPos0";
            this.radioButtonCharPos0.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos0.TabIndex = 41;
            this.radioButtonCharPos0.Text = "0";
            this.radioButtonCharPos0.UseVisualStyleBackColor = true;
            this.radioButtonCharPos0.CheckedChanged += new System.EventHandler(this.radioButtonCharPos0_CheckedChanged);
            // 
            // radioButtonCharPos1
            // 
            this.radioButtonCharPos1.AutoSize = true;
            this.radioButtonCharPos1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCharPos1.Location = new System.Drawing.Point(125, 151);
            this.radioButtonCharPos1.Name = "radioButtonCharPos1";
            this.radioButtonCharPos1.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos1.TabIndex = 42;
            this.radioButtonCharPos1.Text = "1";
            this.radioButtonCharPos1.UseVisualStyleBackColor = true;
            this.radioButtonCharPos1.CheckedChanged += new System.EventHandler(this.radioButtonCharPos0_CheckedChanged);
            // 
            // radioButtonCharPos2
            // 
            this.radioButtonCharPos2.AutoSize = true;
            this.radioButtonCharPos2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCharPos2.Location = new System.Drawing.Point(67, 174);
            this.radioButtonCharPos2.Name = "radioButtonCharPos2";
            this.radioButtonCharPos2.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos2.TabIndex = 43;
            this.radioButtonCharPos2.Text = "2";
            this.radioButtonCharPos2.UseVisualStyleBackColor = true;
            this.radioButtonCharPos2.CheckedChanged += new System.EventHandler(this.radioButtonCharPos0_CheckedChanged);
            // 
            // radioButtonCharPos3
            // 
            this.radioButtonCharPos3.AutoSize = true;
            this.radioButtonCharPos3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCharPos3.Location = new System.Drawing.Point(125, 174);
            this.radioButtonCharPos3.Name = "radioButtonCharPos3";
            this.radioButtonCharPos3.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos3.TabIndex = 44;
            this.radioButtonCharPos3.Text = "3";
            this.radioButtonCharPos3.UseVisualStyleBackColor = true;
            this.radioButtonCharPos3.CheckedChanged += new System.EventHandler(this.radioButtonCharPos0_CheckedChanged);
            // 
            // radioButtonCharPos4
            // 
            this.radioButtonCharPos4.AutoSize = true;
            this.radioButtonCharPos4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCharPos4.Location = new System.Drawing.Point(67, 197);
            this.radioButtonCharPos4.Name = "radioButtonCharPos4";
            this.radioButtonCharPos4.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos4.TabIndex = 45;
            this.radioButtonCharPos4.Text = "4";
            this.radioButtonCharPos4.UseVisualStyleBackColor = true;
            this.radioButtonCharPos4.CheckedChanged += new System.EventHandler(this.radioButtonCharPos0_CheckedChanged);
            // 
            // radioButtonCharPos5
            // 
            this.radioButtonCharPos5.AutoSize = true;
            this.radioButtonCharPos5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCharPos5.Location = new System.Drawing.Point(125, 197);
            this.radioButtonCharPos5.Name = "radioButtonCharPos5";
            this.radioButtonCharPos5.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos5.TabIndex = 46;
            this.radioButtonCharPos5.Text = "5";
            this.radioButtonCharPos5.UseVisualStyleBackColor = true;
            this.radioButtonCharPos5.CheckedChanged += new System.EventHandler(this.radioButtonCharPos0_CheckedChanged);
            // 
            // radioButtonCharPos6
            // 
            this.radioButtonCharPos6.AutoSize = true;
            this.radioButtonCharPos6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCharPos6.Location = new System.Drawing.Point(67, 220);
            this.radioButtonCharPos6.Name = "radioButtonCharPos6";
            this.radioButtonCharPos6.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos6.TabIndex = 47;
            this.radioButtonCharPos6.Text = "6";
            this.radioButtonCharPos6.UseVisualStyleBackColor = true;
            this.radioButtonCharPos6.CheckedChanged += new System.EventHandler(this.radioButtonCharPos0_CheckedChanged);
            // 
            // radioButtonCharPos7
            // 
            this.radioButtonCharPos7.AutoSize = true;
            this.radioButtonCharPos7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCharPos7.Location = new System.Drawing.Point(125, 220);
            this.radioButtonCharPos7.Name = "radioButtonCharPos7";
            this.radioButtonCharPos7.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos7.TabIndex = 48;
            this.radioButtonCharPos7.Text = "7";
            this.radioButtonCharPos7.UseVisualStyleBackColor = true;
            this.radioButtonCharPos7.CheckedChanged += new System.EventHandler(this.radioButtonCharPos0_CheckedChanged);
            // 
            // gb_Profiles
            // 
            this.gb_Profiles.Controls.Add(this.button_Save);
            this.gb_Profiles.Controls.Add(this.label41);
            this.gb_Profiles.Controls.Add(this.textBoxProfileName);
            this.gb_Profiles.Controls.Add(this.listBoxProfiles);
            this.gb_Profiles.Controls.Add(this.buttonProfileRemove);
            this.gb_Profiles.Controls.Add(this.buttonProfileAdd);
            this.gb_Profiles.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_Profiles.Location = new System.Drawing.Point(3, 6);
            this.gb_Profiles.Name = "gb_Profiles";
            this.gb_Profiles.Size = new System.Drawing.Size(234, 174);
            this.gb_Profiles.TabIndex = 89;
            this.gb_Profiles.TabStop = false;
            this.gb_Profiles.Text = "Profiles";
            // 
            // button_Save
            // 
            this.button_Save.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Save.ForeColor = System.Drawing.Color.Red;
            this.button_Save.Location = new System.Drawing.Point(9, 137);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(219, 23);
            this.button_Save.TabIndex = 1;
            this.button_Save.Text = "Save Profile";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(6, 17);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(67, 13);
            this.label41.TabIndex = 90;
            this.label41.Text = "Profile Name";
            // 
            // textBoxProfileName
            // 
            this.textBoxProfileName.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxProfileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxProfileName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxProfileName.Location = new System.Drawing.Point(79, 14);
            this.textBoxProfileName.Name = "textBoxProfileName";
            this.textBoxProfileName.Size = new System.Drawing.Size(149, 20);
            this.textBoxProfileName.TabIndex = 89;
            this.toolTip.SetToolTip(this.textBoxProfileName, "The name of your profile");
            // 
            // listBoxProfiles
            // 
            this.listBoxProfiles.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.listBoxProfiles.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.listBoxProfiles.FormattingEnabled = true;
            this.listBoxProfiles.ItemHeight = 15;
            this.listBoxProfiles.Location = new System.Drawing.Point(79, 40);
            this.listBoxProfiles.Name = "listBoxProfiles";
            this.listBoxProfiles.Size = new System.Drawing.Size(149, 94);
            this.listBoxProfiles.TabIndex = 86;
            this.listBoxProfiles.SelectedIndexChanged += new System.EventHandler(this.listBoxProfiles_SelectedIndexChanged);
            // 
            // buttonProfileRemove
            // 
            this.buttonProfileRemove.Location = new System.Drawing.Point(6, 69);
            this.buttonProfileRemove.Name = "buttonProfileRemove";
            this.buttonProfileRemove.Size = new System.Drawing.Size(64, 23);
            this.buttonProfileRemove.TabIndex = 88;
            this.buttonProfileRemove.Text = "Remove";
            this.buttonProfileRemove.UseVisualStyleBackColor = true;
            this.buttonProfileRemove.Click += new System.EventHandler(this.buttonProfileRemove_Click);
            // 
            // buttonProfileAdd
            // 
            this.buttonProfileAdd.Location = new System.Drawing.Point(6, 40);
            this.buttonProfileAdd.Name = "buttonProfileAdd";
            this.buttonProfileAdd.Size = new System.Drawing.Size(64, 23);
            this.buttonProfileAdd.TabIndex = 87;
            this.buttonProfileAdd.Text = "Add";
            this.buttonProfileAdd.UseVisualStyleBackColor = true;
            this.buttonProfileAdd.Click += new System.EventHandler(this.buttonProfileAdd_Click);
            // 
            // toolTip
            // 
            this.toolTip.IsBalloon = true;
            this.toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(286, 33);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(38, 13);
            this.label27.TabIndex = 89;
            this.label27.Text = "games";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(6, 33);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(93, 13);
            this.label26.TabIndex = 88;
            this.label26.Text = "Change keys after";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(6, 134);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 13);
            this.label25.TabIndex = 85;
            this.label25.Text = "Expansion Key:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(6, 60);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 13);
            this.label24.TabIndex = 84;
            this.label24.Text = "Classic Key:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(6, 80);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(91, 13);
            this.label32.TabIndex = 87;
            this.label32.Text = "Master Character:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(6, 119);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(87, 13);
            this.label34.TabIndex = 89;
            this.label34.Text = "Game Password:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(6, 41);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(85, 13);
            this.label35.TabIndex = 85;
            this.label35.Text = "Master Account:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonNothing);
            this.groupBox2.Controls.Add(this.radioButtonFollow);
            this.groupBox2.Controls.Add(this.label45);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.textBox_JoinGameDelay);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.textBoxMasterCharacter);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this.textBoxFollowPassword);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.textBoxMasterAccount);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(82, 492);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(431, 124);
            this.groupBox2.TabIndex = 93;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Leech Bot";
            // 
            // radioButtonNothing
            // 
            this.radioButtonNothing.AutoSize = true;
            this.radioButtonNothing.Checked = true;
            this.radioButtonNothing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radioButtonNothing.Location = new System.Drawing.Point(6, 43);
            this.radioButtonNothing.Name = "radioButtonNothing";
            this.radioButtonNothing.Size = new System.Drawing.Size(81, 17);
            this.radioButtonNothing.TabIndex = 95;
            this.radioButtonNothing.TabStop = true;
            this.radioButtonNothing.Text = "De-Activate";
            this.radioButtonNothing.UseVisualStyleBackColor = true;
            // 
            // radioButtonFollow
            // 
            this.radioButtonFollow.AutoSize = true;
            this.radioButtonFollow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radioButtonFollow.Location = new System.Drawing.Point(6, 20);
            this.radioButtonFollow.Name = "radioButtonFollow";
            this.radioButtonFollow.Size = new System.Drawing.Size(64, 17);
            this.radioButtonFollow.TabIndex = 93;
            this.radioButtonFollow.Text = "Activate";
            this.radioButtonFollow.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(311, 86);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(47, 13);
            this.label45.TabIndex = 92;
            this.label45.Text = "seconds";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(191, 67);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(93, 13);
            this.label44.TabIndex = 91;
            this.label44.Text = "Join Game Delay :";
            // 
            // textBox_JoinGameDelay
            // 
            this.textBox_JoinGameDelay.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_JoinGameDelay.Enabled = false;
            this.textBox_JoinGameDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_JoinGameDelay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBox_JoinGameDelay.Location = new System.Drawing.Point(194, 83);
            this.textBox_JoinGameDelay.MaxLength = 15;
            this.textBox_JoinGameDelay.Name = "textBox_JoinGameDelay";
            this.textBox_JoinGameDelay.Size = new System.Drawing.Size(111, 20);
            this.textBox_JoinGameDelay.TabIndex = 90;
            this.textBox_JoinGameDelay.Text = "3";
            this.toolTip.SetToolTip(this.textBox_JoinGameDelay, "Delay before joining a game!");
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(251, 22);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(91, 13);
            this.label36.TabIndex = 87;
            this.label36.Text = "Master Character:";
            // 
            // textBoxMasterCharacter
            // 
            this.textBoxMasterCharacter.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxMasterCharacter.Enabled = false;
            this.textBoxMasterCharacter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMasterCharacter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxMasterCharacter.Location = new System.Drawing.Point(254, 38);
            this.textBoxMasterCharacter.MaxLength = 15;
            this.textBoxMasterCharacter.Name = "textBoxMasterCharacter";
            this.textBoxMasterCharacter.Size = new System.Drawing.Size(136, 20);
            this.textBoxMasterCharacter.TabIndex = 86;
            this.toolTip.SetToolTip(this.textBoxMasterCharacter, "Character name of Character you want to follow/leech");
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(6, 67);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(87, 13);
            this.label37.TabIndex = 89;
            this.label37.Text = "Game Password:";
            // 
            // textBoxFollowPassword
            // 
            this.textBoxFollowPassword.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxFollowPassword.Enabled = false;
            this.textBoxFollowPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFollowPassword.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxFollowPassword.Location = new System.Drawing.Point(9, 83);
            this.textBoxFollowPassword.MaxLength = 15;
            this.textBoxFollowPassword.Name = "textBoxFollowPassword";
            this.textBoxFollowPassword.Size = new System.Drawing.Size(136, 20);
            this.textBoxFollowPassword.TabIndex = 88;
            this.toolTip.SetToolTip(this.textBoxFollowPassword, "Optional pw for private games!");
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(105, 24);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(85, 13);
            this.label38.TabIndex = 85;
            this.label38.Text = "Master Account:";
            // 
            // textBoxMasterAccount
            // 
            this.textBoxMasterAccount.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxMasterAccount.Enabled = false;
            this.textBoxMasterAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMasterAccount.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxMasterAccount.Location = new System.Drawing.Point(108, 40);
            this.textBoxMasterAccount.MaxLength = 15;
            this.textBoxMasterAccount.Name = "textBoxMasterAccount";
            this.textBoxMasterAccount.Size = new System.Drawing.Size(136, 20);
            this.textBoxMasterAccount.TabIndex = 84;
            this.toolTip.SetToolTip(this.textBoxMasterAccount, "Account name of Character you want to follow/leech");
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbl_warning);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.textBoxMulePass);
            this.groupBox3.Controls.Add(this.textBoxMuleAccount);
            this.groupBox3.Controls.Add(this.checkBoxAutoMule);
            this.groupBox3.Enabled = false;
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox3.Location = new System.Drawing.Point(396, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(186, 221);
            this.groupBox3.TabIndex = 93;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Muling Settings";
            // 
            // lbl_warning
            // 
            this.lbl_warning.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_warning.ForeColor = System.Drawing.Color.Red;
            this.lbl_warning.Location = new System.Drawing.Point(6, 149);
            this.lbl_warning.Name = "lbl_warning";
            this.lbl_warning.Size = new System.Drawing.Size(153, 69);
            this.lbl_warning.TabIndex = 90;
            this.lbl_warning.Text = "UNDER DEVELOPMENT\r\nHOPE TO BE IN SOON IN A NEW SAFER MANNER!";
            this.lbl_warning.Visible = false;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label39.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label39.Location = new System.Drawing.Point(3, 91);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(102, 13);
            this.label39.TabIndex = 5;
            this.label39.Text = "Mule Account Pass:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label40.Location = new System.Drawing.Point(3, 52);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(107, 13);
            this.label40.TabIndex = 4;
            this.label40.Text = "Mule Account Name:";
            // 
            // textBoxMulePass
            // 
            this.textBoxMulePass.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxMulePass.Enabled = false;
            this.textBoxMulePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxMulePass.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxMulePass.Location = new System.Drawing.Point(6, 107);
            this.textBoxMulePass.Name = "textBoxMulePass";
            this.textBoxMulePass.Size = new System.Drawing.Size(139, 20);
            this.textBoxMulePass.TabIndex = 2;
            this.textBoxMulePass.UseSystemPasswordChar = true;
            // 
            // textBoxMuleAccount
            // 
            this.textBoxMuleAccount.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxMuleAccount.Enabled = false;
            this.textBoxMuleAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxMuleAccount.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxMuleAccount.Location = new System.Drawing.Point(6, 68);
            this.textBoxMuleAccount.Name = "textBoxMuleAccount";
            this.textBoxMuleAccount.Size = new System.Drawing.Size(139, 20);
            this.textBoxMuleAccount.TabIndex = 1;
            this.toolTip.SetToolTip(this.textBoxMuleAccount, "Mule Account name. Note: you will need an account with 8 empty characters!");
            // 
            // checkBoxAutoMule
            // 
            this.checkBoxAutoMule.AutoSize = true;
            this.checkBoxAutoMule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.checkBoxAutoMule.ForeColor = System.Drawing.SystemColors.ControlText;
            this.checkBoxAutoMule.Location = new System.Drawing.Point(6, 21);
            this.checkBoxAutoMule.Name = "checkBoxAutoMule";
            this.checkBoxAutoMule.Size = new System.Drawing.Size(115, 17);
            this.checkBoxAutoMule.TabIndex = 0;
            this.checkBoxAutoMule.Text = "Active Auto-Muling";
            this.checkBoxAutoMule.UseVisualStyleBackColor = true;
            // 
            // frmBotSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 672);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBotSettings";
            this.Text = "Awesom-O Manager & Bot Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBotSettings_FormClosing);
            this.tabControl.ResumeLayout(false);
            this.tabPageDelay.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControlProfiles.ResumeLayout(false);
            this.gb_keys.ResumeLayout(false);
            this.gb_keys.PerformLayout();
            this.gb_Game.ResumeLayout(false);
            this.gb_Game.PerformLayout();
            this.gb_logindata.ResumeLayout(false);
            this.gb_logindata.PerformLayout();
            this.gb_Profiles.ResumeLayout(false);
            this.gb_Profiles.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageDelay;
        private System.Windows.Forms.TabPage tabControlProfiles;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button button_Save;
        //private System.Windows.Forms.CheckBox checkBoxD2launcher;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCreateGameDelay;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxStartRunDelay;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxTempBanSleepTime;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ListBox listBoxProfiles;
        private System.Windows.Forms.Button buttonProfileRemove;
        private System.Windows.Forms.Button buttonProfileAdd;
        private System.Windows.Forms.GroupBox gb_Profiles;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBoxProfileName;
        private System.Windows.Forms.GroupBox gb_logindata;
        private System.Windows.Forms.CheckBox chk_AutoStartRun;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDiabloFolderDirectory;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxOwnerName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioButtonCharPos0;
        private System.Windows.Forms.RadioButton radioButtonCharPos1;
        private System.Windows.Forms.RadioButton radioButtonCharPos2;
        private System.Windows.Forms.RadioButton radioButtonCharPos3;
        private System.Windows.Forms.RadioButton radioButtonCharPos4;
        private System.Windows.Forms.RadioButton radioButtonCharPos5;
        private System.Windows.Forms.RadioButton radioButtonCharPos6;
        private System.Windows.Forms.RadioButton radioButtonCharPos7;
        private System.Windows.Forms.GroupBox gb_Game;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxFirstJoinChannelMessage;
        private System.Windows.Forms.CheckBox checkBoxNoPass;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxDifficulty;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox chk_Hide;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxTooShortMessage;
        private System.Windows.Forms.TextBox textBoxChannel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxGameName;
        private System.Windows.Forms.TextBox textBoxNextGameMessage;
        private System.Windows.Forms.TextBox textBoxGamePass;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox gb_keys;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBoxKeyChange;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button buttonExpansionKeyRemove;
        private System.Windows.Forms.Button buttonClassicKeyRemove;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ListBox listBoxExpansionKeys;
        private System.Windows.Forms.ListBox listBoxClassicKeys;
        private System.Windows.Forms.TextBox textBoxExpansionKey;
        private System.Windows.Forms.Button buttonExpansionKeyAdd;
        private System.Windows.Forms.Button buttonClassicKeyAdd;
        private System.Windows.Forms.TextBox textBoxClassicKey;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBoxLoginTooLongDelay;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBoxGameJoinTooLongDelay;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox textBoxCdKeyInUseDelay;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textBoxCantConnectDelay;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxMaxGamesHour;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxMaxGameTime;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxMinGameTime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonNothing;
        private System.Windows.Forms.RadioButton radioButtonFollow;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox_JoinGameDelay;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxMasterCharacter;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBoxFollowPassword;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBoxMasterAccount;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbl_warning;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBoxMulePass;
        private System.Windows.Forms.TextBox textBoxMuleAccount;
        private System.Windows.Forms.CheckBox checkBoxAutoMule;
    }
}