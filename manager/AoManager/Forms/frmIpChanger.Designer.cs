﻿namespace AoManager
{
    partial class frmIpChanger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           this.components = new System.ComponentModel.Container();
           System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIpChanger));
           this.labelRouterIp = new System.Windows.Forms.Label();
           this.textBoxRouterIp = new System.Windows.Forms.TextBox();
           this.textBoxLoginUrl = new System.Windows.Forms.TextBox();
           this.labelLoginUrl = new System.Windows.Forms.Label();
           this.labelDiscUrl = new System.Windows.Forms.Label();
           this.labelConnUrl = new System.Windows.Forms.Label();
           this.textBoxDiscUrl = new System.Windows.Forms.TextBox();
           this.textBoxConnUrl = new System.Windows.Forms.TextBox();
           this.checkBoxPostLogin = new System.Windows.Forms.CheckBox();
           this.buttonTest = new System.Windows.Forms.Button();
           this.textBoxUsername = new System.Windows.Forms.TextBox();
           this.labelUserName = new System.Windows.Forms.Label();
           this.labelPassword = new System.Windows.Forms.Label();
           this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
           this.checkBoxChallenge = new System.Windows.Forms.CheckBox();
           this.textBoxChallengeUrl = new System.Windows.Forms.TextBox();
           this.labelChallengeUrl = new System.Windows.Forms.Label();
           this.textBoxChallengeSearch = new System.Windows.Forms.TextBox();
           this.labelChallengeSearch = new System.Windows.Forms.Label();
           this.checkBoxAutoReconnect = new System.Windows.Forms.CheckBox();
           this.checkBoxPostDiscon = new System.Windows.Forms.CheckBox();
           this.checkBoxPostRecon = new System.Windows.Forms.CheckBox();
           this.checkBoxPreAuth = new System.Windows.Forms.CheckBox();
           this.labelPreAuthURL = new System.Windows.Forms.Label();
           this.textBoxPreAuthURL = new System.Windows.Forms.TextBox();
           this.toolTipIpChanger = new System.Windows.Forms.ToolTip(this.components);
           this.SuspendLayout();
           // 
           // labelRouterIp
           // 
           this.labelRouterIp.AutoSize = true;
           this.labelRouterIp.Location = new System.Drawing.Point(12, 15);
           this.labelRouterIp.Name = "labelRouterIp";
           this.labelRouterIp.Size = new System.Drawing.Size(55, 13);
           this.labelRouterIp.TabIndex = 0;
           this.labelRouterIp.Text = "Router IP:";
           // 
           // textBoxRouterIp
           // 
           this.textBoxRouterIp.Location = new System.Drawing.Point(112, 12);
           this.textBoxRouterIp.Name = "textBoxRouterIp";
           this.textBoxRouterIp.Size = new System.Drawing.Size(295, 20);
           this.textBoxRouterIp.TabIndex = 1;
           this.toolTipIpChanger.SetToolTip(this.textBoxRouterIp, "Insert the IP address or the URL of your Router here (no leading \"http://\" and no" +
                   " trailing \"/\")\nPort may be specified with \":<portnumber> after the IP (optional)" +
                   ".\nExample: \"192.168.1.100:8080\"");
           // 
           // textBoxLoginUrl
           // 
           this.textBoxLoginUrl.Location = new System.Drawing.Point(112, 38);
           this.textBoxLoginUrl.Name = "textBoxLoginUrl";
           this.textBoxLoginUrl.Size = new System.Drawing.Size(234, 20);
           this.textBoxLoginUrl.TabIndex = 2;
           this.toolTipIpChanger.SetToolTip(this.textBoxLoginUrl, resources.GetString("textBoxLoginUrl.ToolTip"));
           // 
           // labelLoginUrl
           // 
           this.labelLoginUrl.AutoSize = true;
           this.labelLoginUrl.Location = new System.Drawing.Point(12, 41);
           this.labelLoginUrl.Name = "labelLoginUrl";
           this.labelLoginUrl.Size = new System.Drawing.Size(61, 13);
           this.labelLoginUrl.TabIndex = 5;
           this.labelLoginUrl.Text = "Login URL:";
           // 
           // labelDiscUrl
           // 
           this.labelDiscUrl.AutoSize = true;
           this.labelDiscUrl.Location = new System.Drawing.Point(12, 67);
           this.labelDiscUrl.Name = "labelDiscUrl";
           this.labelDiscUrl.Size = new System.Drawing.Size(89, 13);
           this.labelDiscUrl.TabIndex = 8;
           this.labelDiscUrl.Text = "Disconnect URL:";
           // 
           // labelConnUrl
           // 
           this.labelConnUrl.AutoSize = true;
           this.labelConnUrl.Location = new System.Drawing.Point(12, 93);
           this.labelConnUrl.Name = "labelConnUrl";
           this.labelConnUrl.Size = new System.Drawing.Size(75, 13);
           this.labelConnUrl.TabIndex = 9;
           this.labelConnUrl.Text = "Connect URL:";
           // 
           // textBoxDiscUrl
           // 
           this.textBoxDiscUrl.Location = new System.Drawing.Point(112, 64);
           this.textBoxDiscUrl.Name = "textBoxDiscUrl";
           this.textBoxDiscUrl.Size = new System.Drawing.Size(234, 20);
           this.textBoxDiscUrl.TabIndex = 4;
           this.toolTipIpChanger.SetToolTip(this.textBoxDiscUrl, "Specify the URL to disconnect the router from the Internet here.\n" +
              "If you make this a POST call (check box on the right), everything after the \"?\" will be used as POST data.\n" +
              "Example: \"lineconn.tri?wanId=1&Submit=\"");
           // 
           // textBoxConnUrl
           // 
           this.textBoxConnUrl.Location = new System.Drawing.Point(112, 90);
           this.textBoxConnUrl.Name = "textBoxConnUrl";
           this.textBoxConnUrl.Size = new System.Drawing.Size(234, 20);
           this.textBoxConnUrl.TabIndex = 6;
           this.toolTipIpChanger.SetToolTip(this.textBoxConnUrl, "Specify the URL to (re)connect the router to the Internet here.\n" +
              "If you make this a POST call (check box on the right), everything after the \"?\" will be used as POST data.\n" +
              "Note: If your router will reconnect automatically after a disconnect, you may activate the \"AutoReconnect\" option below.\n" +
              "Example: \"lineconn.tri?wanId=1&Submit=\"");
           // 
           // checkBoxPostLogin
           // 
           this.checkBoxPostLogin.AutoSize = true;
           this.checkBoxPostLogin.Location = new System.Drawing.Point(352, 40);
           this.checkBoxPostLogin.Name = "checkBoxPostLogin";
           this.checkBoxPostLogin.Size = new System.Drawing.Size(55, 17);
           this.checkBoxPostLogin.TabIndex = 3;
           this.checkBoxPostLogin.Text = "POST";
           this.toolTipIpChanger.SetToolTip(this.checkBoxPostLogin, "Is the login request a POST or a GET call?");
           this.checkBoxPostLogin.UseVisualStyleBackColor = true;
           // 
           // buttonTest
           // 
           this.buttonTest.Location = new System.Drawing.Point(332, 248);
           this.buttonTest.Name = "buttonTest";
           this.buttonTest.Size = new System.Drawing.Size(75, 23);
           this.buttonTest.TabIndex = 16;
           this.buttonTest.Text = "Test";
           this.buttonTest.UseVisualStyleBackColor = true;
           this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
           // 
           // textBoxUsername
           // 
           this.textBoxUsername.Location = new System.Drawing.Point(112, 116);
           this.textBoxUsername.Name = "textBoxUsername";
           this.textBoxUsername.Size = new System.Drawing.Size(295, 20);
           this.textBoxUsername.TabIndex = 8;
           this.toolTipIpChanger.SetToolTip(this.textBoxUsername, "Specify the username for the router here.\n" +
              "Use it in the login URL with %username%");
           // 
           // labelUserName
           // 
           this.labelUserName.AutoSize = true;
           this.labelUserName.Location = new System.Drawing.Point(12, 119);
           this.labelUserName.Name = "labelUserName";
           this.labelUserName.Size = new System.Drawing.Size(58, 13);
           this.labelUserName.TabIndex = 14;
           this.labelUserName.Text = "Username:";
           // 
           // labelPassword
           // 
           this.labelPassword.AutoSize = true;
           this.labelPassword.Location = new System.Drawing.Point(12, 146);
           this.labelPassword.Name = "labelPassword";
           this.labelPassword.Size = new System.Drawing.Size(56, 13);
           this.labelPassword.TabIndex = 15;
           this.labelPassword.Text = "Password:";
           // 
           // maskedTextBox1
           // 
           this.maskedTextBox1.Location = new System.Drawing.Point(112, 143);
           this.maskedTextBox1.Name = "maskedTextBox1";
           this.maskedTextBox1.PasswordChar = '*';
           this.maskedTextBox1.Size = new System.Drawing.Size(295, 20);
           this.maskedTextBox1.TabIndex = 9;
           this.toolTipIpChanger.SetToolTip(this.maskedTextBox1, "Specify the password for the router here.\n" +
              "Use it in the login URL with %password%");
           // 
           // checkBoxChallenge
           // 
           this.checkBoxChallenge.AutoSize = true;
           this.checkBoxChallenge.Location = new System.Drawing.Point(122, 252);
           this.checkBoxChallenge.Name = "checkBoxChallenge";
           this.checkBoxChallenge.Size = new System.Drawing.Size(73, 17);
           this.checkBoxChallenge.TabIndex = 14;
           this.checkBoxChallenge.Text = "Challenge";
           this.toolTipIpChanger.SetToolTip(this.checkBoxChallenge, "Enable this if your router requires a challenge string.\n" +
              "You have to specify the Challenge URL and the Challenge Search String above to make use of this!\n" +
              "Note: Getting a challenge value makes PreAuth needless.\n" +
              "Use it in the login URL with %challenge%");
           this.checkBoxChallenge.UseVisualStyleBackColor = true;
           this.checkBoxChallenge.CheckedChanged += new System.EventHandler(this.checkBoxChallenge_CheckedChanged);
           // 
           // textBoxChallengeUrl
           // 
           this.textBoxChallengeUrl.Location = new System.Drawing.Point(112, 195);
           this.textBoxChallengeUrl.Name = "textBoxChallengeUrl";
           this.textBoxChallengeUrl.Size = new System.Drawing.Size(295, 20);
           this.textBoxChallengeUrl.TabIndex = 11;
           this.toolTipIpChanger.SetToolTip(this.textBoxChallengeUrl, "Specify the URL where to get the callenge value from if necessary.\n" +
              "You have to specify the Challenge Search String to make use of this!\n" +
              "Example: \"login.htm\"");
           // 
           // labelChallengeUrl
           // 
           this.labelChallengeUrl.AutoSize = true;
           this.labelChallengeUrl.Location = new System.Drawing.Point(12, 198);
           this.labelChallengeUrl.Name = "labelChallengeUrl";
           this.labelChallengeUrl.Size = new System.Drawing.Size(82, 13);
           this.labelChallengeUrl.TabIndex = 19;
           this.labelChallengeUrl.Text = "Challenge URL:";
           // 
           // textBoxChallengeSearch
           // 
           this.textBoxChallengeSearch.Location = new System.Drawing.Point(112, 222);
           this.textBoxChallengeSearch.Name = "textBoxChallengeSearch";
           this.textBoxChallengeSearch.Size = new System.Drawing.Size(295, 20);
           this.textBoxChallengeSearch.TabIndex = 12;
           this.toolTipIpChanger.SetToolTip(this.textBoxChallengeSearch, "Specify the search string that is right before the challenge value (in HTML source)\n" +
              "You have to specify the Challenge URL to make use of this!\n" +
              "Example: name=\"challenge\" value=\"");
           // 
           // labelChallengeSearch
           // 
           this.labelChallengeSearch.AutoSize = true;
           this.labelChallengeSearch.Location = new System.Drawing.Point(12, 225);
           this.labelChallengeSearch.Name = "labelChallengeSearch";
           this.labelChallengeSearch.Size = new System.Drawing.Size(94, 13);
           this.labelChallengeSearch.TabIndex = 21;
           this.labelChallengeSearch.Text = "Challenge Search:";
           // 
           // checkBoxAutoReconnect
           // 
           this.checkBoxAutoReconnect.AutoSize = true;
           this.checkBoxAutoReconnect.Location = new System.Drawing.Point(15, 252);
           this.checkBoxAutoReconnect.Name = "checkBoxAutoReconnect";
           this.checkBoxAutoReconnect.Size = new System.Drawing.Size(101, 17);
           this.checkBoxAutoReconnect.TabIndex = 13;
           this.checkBoxAutoReconnect.Text = "AutoReconnect";
           this.toolTipIpChanger.SetToolTip(this.checkBoxAutoReconnect, "Enable this, if the Router will reconnect autoamtically after it disconnected.");
           this.checkBoxAutoReconnect.UseVisualStyleBackColor = true;
           this.checkBoxAutoReconnect.CheckedChanged += new System.EventHandler(this.checkBoxAutoReconnect_CheckedChanged);
           // 
           // checkBoxPostDiscon
           // 
           this.checkBoxPostDiscon.AutoSize = true;
           this.checkBoxPostDiscon.Location = new System.Drawing.Point(352, 66);
           this.checkBoxPostDiscon.Name = "checkBoxPostDiscon";
           this.checkBoxPostDiscon.Size = new System.Drawing.Size(55, 17);
           this.checkBoxPostDiscon.TabIndex = 5;
           this.checkBoxPostDiscon.Text = "POST";
           this.toolTipIpChanger.SetToolTip(this.checkBoxPostDiscon, "Is the disconnect request a POST or a GET call?");
           this.checkBoxPostDiscon.UseVisualStyleBackColor = true;
           // 
           // checkBoxPostRecon
           // 
           this.checkBoxPostRecon.AutoSize = true;
           this.checkBoxPostRecon.Location = new System.Drawing.Point(352, 92);
           this.checkBoxPostRecon.Name = "checkBoxPostRecon";
           this.checkBoxPostRecon.Size = new System.Drawing.Size(55, 17);
           this.checkBoxPostRecon.TabIndex = 7;
           this.checkBoxPostRecon.Text = "POST";
           this.toolTipIpChanger.SetToolTip(this.checkBoxPostRecon, "Is the connect request a POST or a GET call?");
           this.checkBoxPostRecon.UseVisualStyleBackColor = true;
           // 
           // checkBoxPreAuth
           // 
           this.checkBoxPreAuth.AutoSize = true;
           this.checkBoxPreAuth.Location = new System.Drawing.Point(201, 252);
           this.checkBoxPreAuth.Name = "checkBoxPreAuth";
           this.checkBoxPreAuth.Size = new System.Drawing.Size(64, 17);
           this.checkBoxPreAuth.TabIndex = 15;
           this.checkBoxPreAuth.Text = "PreAuth";
           this.toolTipIpChanger.SetToolTip(this.checkBoxPreAuth, "Enable this, if your router needs to set cookies, or establish a session before it let's you log in.\n" +
              "You have to specify the PreAuth URL to make use of this!\n" +
              "Note: This is not needed if you make use of the Challenge option.");
           this.checkBoxPreAuth.UseVisualStyleBackColor = true;
           this.checkBoxPreAuth.CheckedChanged += new System.EventHandler(this.checkBoxPreAuth_CheckedChanged);
           // 
           // labelPreAuthURL
           // 
           this.labelPreAuthURL.AutoSize = true;
           this.labelPreAuthURL.Location = new System.Drawing.Point(12, 172);
           this.labelPreAuthURL.Name = "labelPreAuthURL";
           this.labelPreAuthURL.Size = new System.Drawing.Size(73, 13);
           this.labelPreAuthURL.TabIndex = 26;
           this.labelPreAuthURL.Text = "PreAuth URL:";
           // 
           // textBoxPreAuthURL
           // 
           this.textBoxPreAuthURL.Location = new System.Drawing.Point(112, 169);
           this.textBoxPreAuthURL.Name = "textBoxPreAuthURL";
           this.textBoxPreAuthURL.Size = new System.Drawing.Size(295, 20);
           this.textBoxPreAuthURL.TabIndex = 10;
           this.toolTipIpChanger.SetToolTip(this.textBoxPreAuthURL, "Specify the URL to establish a session or get cookies if necessary.\n" +
              "Note: This is the first URL ever called (if enabled).\n" +
              "Example: welcome.htm");
           // 
           // toolTipIpChanger
           // 
           this.toolTipIpChanger.AutomaticDelay = 0;
           this.toolTipIpChanger.AutoPopDelay = 30000;
           this.toolTipIpChanger.InitialDelay = 250;
           this.toolTipIpChanger.ReshowDelay = 100;
           this.toolTipIpChanger.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
           this.toolTipIpChanger.ToolTipTitle = "Quick Tip";
           // 
           // frmIpChanger
           // 
           this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
           this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
           this.ClientSize = new System.Drawing.Size(419, 282);
           this.Controls.Add(this.textBoxPreAuthURL);
           this.Controls.Add(this.labelPreAuthURL);
           this.Controls.Add(this.checkBoxPreAuth);
           this.Controls.Add(this.checkBoxPostRecon);
           this.Controls.Add(this.checkBoxPostDiscon);
           this.Controls.Add(this.checkBoxAutoReconnect);
           this.Controls.Add(this.labelChallengeSearch);
           this.Controls.Add(this.textBoxChallengeSearch);
           this.Controls.Add(this.labelChallengeUrl);
           this.Controls.Add(this.textBoxChallengeUrl);
           this.Controls.Add(this.checkBoxChallenge);
           this.Controls.Add(this.maskedTextBox1);
           this.Controls.Add(this.labelPassword);
           this.Controls.Add(this.labelUserName);
           this.Controls.Add(this.textBoxUsername);
           this.Controls.Add(this.buttonTest);
           this.Controls.Add(this.textBoxConnUrl);
           this.Controls.Add(this.textBoxDiscUrl);
           this.Controls.Add(this.labelConnUrl);
           this.Controls.Add(this.labelDiscUrl);
           this.Controls.Add(this.checkBoxPostLogin);
           this.Controls.Add(this.labelLoginUrl);
           this.Controls.Add(this.textBoxLoginUrl);
           this.Controls.Add(this.textBoxRouterIp);
           this.Controls.Add(this.labelRouterIp);
           this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
           this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
           this.MaximizeBox = false;
           this.MinimizeBox = false;
           this.Name = "frmIpChanger";
           this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
           this.Text = "IP Changer";
           this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmIpChanger_FormClosing);
           this.ResumeLayout(false);
           this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRouterIp;
        private System.Windows.Forms.TextBox textBoxRouterIp;
        private System.Windows.Forms.TextBox textBoxLoginUrl;
        private System.Windows.Forms.Label labelLoginUrl;
        private System.Windows.Forms.Label labelDiscUrl;
        private System.Windows.Forms.Label labelConnUrl;
        private System.Windows.Forms.TextBox textBoxDiscUrl;
        private System.Windows.Forms.TextBox textBoxConnUrl;
        private System.Windows.Forms.CheckBox checkBoxPostLogin;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.CheckBox checkBoxChallenge;
        private System.Windows.Forms.TextBox textBoxChallengeUrl;
        private System.Windows.Forms.Label labelChallengeUrl;
        private System.Windows.Forms.TextBox textBoxChallengeSearch;
        private System.Windows.Forms.Label labelChallengeSearch;
        private System.Windows.Forms.CheckBox checkBoxAutoReconnect;
        private System.Windows.Forms.CheckBox checkBoxPostDiscon;
        private System.Windows.Forms.CheckBox checkBoxPostRecon;
        private System.Windows.Forms.CheckBox checkBoxPreAuth;
        private System.Windows.Forms.Label labelPreAuthURL;
        private System.Windows.Forms.TextBox textBoxPreAuthURL;
        private System.Windows.Forms.ToolTip toolTipIpChanger;
    }
}