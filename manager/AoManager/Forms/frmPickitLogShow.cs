﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace AoManager
{
    public partial class frmPickitLogShow : Form
    {

        private ListViewColumnSorter lvwColumnSorter;
        private System.Timers.Timer timerClock = new System.Timers.Timer();

        public frmPickitLogShow()
        {
            InitializeComponent();
            lvwColumnSorter = new ListViewColumnSorter();
            this.listView1.ListViewItemSorter = lvwColumnSorter;
            timerClock.Elapsed += new System.Timers.ElapsedEventHandler(timerClock_Elapsed);
        }


        #region Functions

        
        bool recent = false;
        Item LastItem;
        public void AddToList(Item Item , string profilename)
        {
            if (!recent || (recent && LastItem != Item))
            {
                ListViewItem listViewItemToAdd = new ListViewItem(new string[] { Item.ActionPerformed.ToString(), Item.ItemName, Item.iLevel.ToString(), Item.iQuality.ToString(), DateTime.Now.ToShortTimeString(), profilename });
                //
                this.listView1.Items.AddRange(new ListViewItem[] { listViewItemToAdd });
            }
            
            timerClock.Interval = 4000; // check for the double item send for 4 seconds
            timerClock.Enabled = true;
            timerClock.Start();

            recent = true;
            LastItem = Item;
        }

        void timerClock_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            recent = false;
        }

        private void btn_hide_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void frmPickitLogShow_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count > 0)
                listView1.Items.Clear();
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListView myListView = (ListView)sender;

            // Determine if clicked column is already the column that is being sorted.

            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.

                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.

                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.

            myListView.Sort();

        }

       /* private void RemoveItem(ListViewItem Item)
        {
            string WriteBack = "";
            string[] Lines = File.ReadAllLines(LogPath);
            foreach (string str in Lines)
            {
                if (!str.Contains(Item.SubItems[0].Text) &&
                    !str.Contains(Item.SubItems[1].Text) &&
                    !str.Contains(Item.SubItems[2].Text) &&
                    !str.Contains(Item.SubItems[3].Text))
                { WriteBack += str + Environment.NewLine; }
            }
            Item.Remove();
        }*/

        #endregion
    }
}
