﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace AoManager
{
    public partial class frmBotSettings : Form
    {
        BotSetting config;
        List<Profile> profiles;
        bool editing = false;
        bool loaded = false;
        int selectedindex = 0;
        public delegate void frmBotSettings_Handler(object sender, bool saved);
        public event frmBotSettings_Handler BotSettingsChanged;


        public frmBotSettings()
        {
            InitializeComponent();

            //checkBoxD2launcher.Checked = Properties.Settings.Default.UseD2Launcher;

            if (!Properties.Settings.Default.IsBotSettings || !File.Exists(BotSettings.xmlpath))
            {
                this.comboBoxDifficulty.SelectedIndex = 0;
                this.config = new BotSetting();
                this.profiles = new List<Profile>();
            }
            else
            {
                this.config = BotSettings.GetSettings();
                this.profiles = config.Profiles;
                this.LoadDelays();
                if (profiles.Count > 0)
                {
                    listBoxProfiles.Items.Clear();
                    foreach (Profile p in profiles)
                    {
                        listBoxProfiles.Items.Add(p.ProfileName);
                    }
                    listBoxProfiles.SetSelected(0,true) ;
                    this.LoadProfile(0);
                    editing = false;
                }
                loaded = true;
            }
        }

        #region Load
        private void LoadDelays()
        {
            textBoxCreateGameDelay.Text = config.CreateGameDelay.ToString();
            //textBoxMaxGamesHour.Text = config.MaxGamesAnHour.ToString();
            //textBoxMaxGameTime.Text = config.MaxGameTime.ToString();
            //textBoxMinGameTime.Text = config.MinGameTime.ToString();
            textBoxStartRunDelay.Text = config.StartRunDelay.ToString();
            textBoxTempBanSleepTime.Text = config.BanWaitTime.ToString();
            textBoxLoginTooLongDelay.Text = config.LoginTooLongDelay.ToString();
            textBoxGameJoinTooLongDelay.Text = config.GameJoinTooLongDelay.ToString();
            textBoxCdKeyInUseDelay.Text = config.CdKeyInUseDelay.ToString();
            textBoxCantConnectDelay.Text = config.CantConnectDelay.ToString();
            //textBoxLauncherStartDelay.Text = Properties.Settings.Default.LauncherStartDelay.ToString();
            
        }

        private void LoadProfile(int index)
        {
            textBoxDiabloFolderDirectory.Text = profiles[index].DiabloPath;
            textBoxOwnerName.Text = profiles[index].DiabloOwnerName;
            textBoxUsername.Text = profiles[index].DiabloAccount;
            textBoxPassword.Text = profiles[index].DiabloPassword;
            textBoxChannel.Text = profiles[index].Channel;
            textBoxGameName.Text = profiles[index].GameName;

            textBox_JoinGameDelay.Text = profiles[index].JoinGameDelay.ToString();

            if (profiles[index].HasGamePass)
            {
                checkBoxNoPass.Checked = false;
                textBoxGamePass.Text = profiles[index].GamePassword;
                textBoxGamePass.Enabled = true;
            }
            else
            {
                checkBoxNoPass.Checked = true;
                textBoxGamePass.Clear();
                textBoxGamePass.Enabled = false;
            }

            textBoxFirstJoinChannelMessage.Text = profiles[index].FirstJoinChannelMessage;
            textBoxNextGameMessage.Text = profiles[index].NextGameMessage;
            textBoxTooShortMessage.Text = profiles[index].TooShortMessage;
            textBoxKeyChange.Text = profiles[index].KeyChange.ToString();

            comboBoxDifficulty.SelectedIndex = profiles[index].GameDifficulty;
            chk_Hide.Checked = profiles[index].HideDiabloWindow;
            if (profiles[index].Follower)
            {
                radioButtonFollow.Checked = profiles[index].Follower;
                radioButtonNothing.Checked = false;
            }
            else
            {
                radioButtonNothing.Checked = true;
                radioButtonFollow.Checked = profiles[index].Follower;
            }
            chk_AutoStartRun.Checked = profiles[index].AutoStartRun;
            textBoxMasterAccount.Text = profiles[index].MasterAccount;
            textBoxMasterCharacter.Text = profiles[index].MasterCharacter;
            textBoxFollowPassword.Text = profiles[index].FollowGamePassword;
            checkBoxAutoMule.Checked = profiles[index].AutoMule; //nathan's mule
            textBoxMuleAccount.Text = profiles[index].MuleAccount; //nathan's mule
            textBoxMulePass.Text = profiles[index].MulePassword; //nathan's mule
            textBoxMaxGamesHour.Text = profiles[index].MaxGamesAnHour.ToString();
            textBoxMaxGameTime.Text = profiles[index].MaxGameTime.ToString();
            textBoxMinGameTime.Text = profiles[index].MinGameTime.ToString();

            if (profiles[index].Follower)
            {
                textBoxMasterAccount.Enabled = true;
                textBoxMasterCharacter.Enabled = true;
                textBoxFollowPassword.Enabled = true;
            }
            else
            {
                textBoxMasterAccount.Enabled = false;
                textBoxMasterCharacter.Enabled = false;
                textBoxFollowPassword.Enabled = false;
            }

            if (profiles[index].AutoMule) //nathan's mule
            {
                textBoxMuleAccount.Enabled = true;   //nathan's mule
                textBoxMulePass.Enabled = true;      //nathan's mule
            }
            else                  //nathan's mule
            {
                textBoxMuleAccount.Enabled = false;   //nathan's mule
                textBoxMulePass.Enabled = false;      //nathan's mule
            }

            int charp = profiles[index].DiabloCharPosition;
            if (charp == 0)
                radioButtonCharPos0.Checked = true;
            else if (charp == 1)
                radioButtonCharPos1.Checked = true;
            else if (charp == 2)
                radioButtonCharPos2.Checked = true;
            else if (charp == 3)
                radioButtonCharPos3.Checked = true;
            else if (charp == 4)
                radioButtonCharPos4.Checked = true;
            else if (charp == 5)
                radioButtonCharPos5.Checked = true;
            else if (charp == 6)
                radioButtonCharPos6.Checked = true;
            else if (charp == 7)
                radioButtonCharPos7.Checked = true;

            if (profiles[index].ClassicKeys.Count > 0)
                listBoxClassicKeys.Items.AddRange(profiles[index].ClassicKeys.ToArray());

            if (profiles[index].ExpansionKeys.Count > 0)
                listBoxExpansionKeys.Items.AddRange(profiles[index].ExpansionKeys.ToArray());
            editing = false;
        }

        private void LoadDelaysDefault()
        {
            textBoxCreateGameDelay.Text = "8";
            textBoxMaxGamesHour.Text = "19";
            textBoxMaxGameTime.Text = "360";
            textBoxMinGameTime.Text = "180";
            textBoxStartRunDelay.Text = "5";
            textBoxTempBanSleepTime.Text = "60";
            textBoxLoginTooLongDelay.Text = "60";
            textBoxGameJoinTooLongDelay.Text = "90";
            textBoxCdKeyInUseDelay.Text = "5";
            textBoxCantConnectDelay.Text = "5";
        }

        private void LoadProfileDefault()
        {
            
            textBoxProfileName.Clear();
            textBoxDiabloFolderDirectory.Clear();
            textBoxOwnerName.Clear();
            textBoxUsername.Clear();
            textBoxPassword.Clear();
            textBoxChannel.Clear();
            textBoxGameName.Clear();

            checkBoxNoPass.Checked = false;
            textBoxGamePass.Clear();

            textBoxFirstJoinChannelMessage.Clear();
            textBoxNextGameMessage.Clear();
            textBoxTooShortMessage.Clear();
            textBoxKeyChange.Text = "0";

            textBox_JoinGameDelay.Text = "3";

            comboBoxDifficulty.SelectedIndex = 0;
            chk_Hide.Checked = false;
            radioButtonNothing.Checked = true;
            chk_AutoStartRun.Checked = true;
            textBoxMasterAccount.Clear();
            textBoxMasterCharacter.Clear();
            textBoxFollowPassword.Clear();
            checkBoxAutoMule.Checked = false; //nathan's mule
            textBoxMuleAccount.Clear(); ; //nathan's mule
            textBoxMulePass.Clear(); ; //nathan's mule

            radioButtonCharPos0.Checked = true;

            listBoxClassicKeys.Items.Clear();
            listBoxExpansionKeys.Items.Clear();
            editing = false;

        }

        #endregion

        #region Save
        private void SaveProfile()
        {

             Profile profile = new Profile();
          
             profile.ProfileName = listBoxProfiles.Text;
             profile.DiabloPath = textBoxDiabloFolderDirectory.Text;
             profile.DiabloOwnerName = textBoxOwnerName.Text;
             profile.DiabloAccount = textBoxUsername.Text;
             profile.DiabloPassword = textBoxPassword.Text;
             profile.Channel = textBoxChannel.Text;
             profile.GameName = textBoxGameName.Text;
             profile.HasGamePass = !checkBoxNoPass.Checked;
             if (profile.HasGamePass)
                 profile.GamePassword = textBoxGamePass.Text;
             profile.FirstJoinChannelMessage = textBoxFirstJoinChannelMessage.Text;
             profile.NextGameMessage = textBoxNextGameMessage.Text;
             profile.TooShortMessage = textBoxTooShortMessage.Text;
             
             profile.KeyChange = Convert.ToInt32(textBoxKeyChange.Text);
             profile.JoinGameDelay = Convert.ToInt32(textBox_JoinGameDelay.Text);
             //comboBoxDiabloIIWindow.Text = ProfileSettings.GetSetting(profile, StringSettings.);
             profile.DiabloCharPosition = CharPosition();
             profile.GameDifficulty = comboBoxDifficulty.SelectedIndex;
             profile.HideDiabloWindow = chk_Hide.Checked;
             profile.Follower = radioButtonFollow.Checked;
             profile.AutoStartRun = chk_AutoStartRun.Checked;
             profile.MasterAccount = textBoxMasterAccount.Text;
             profile.MasterCharacter = textBoxMasterCharacter.Text;
             profile.FollowGamePassword = textBoxFollowPassword.Text;
             profile.AutoMule = checkBoxAutoMule.Checked;            //nathan's mule
             profile.MuleAccount = textBoxMuleAccount.Text;          //nathan's mule
             profile.MulePassword = textBoxMulePass.Text;            //nathan's mule
             //nathan's mule
             profile.MinGameTime = Convert.ToInt32(textBoxMinGameTime.Text);
             profile.MaxGameTime = Convert.ToInt32(textBoxMaxGameTime.Text);
             profile.MaxGamesAnHour = Convert.ToInt32(textBoxMaxGamesHour.Text);

             if (listBoxClassicKeys.Items.Count > 0)
             {
                 List<string> keys = new List<string>();
                 foreach (string key in listBoxClassicKeys.Items)
                 {
                     keys.Add(key);
                 }
                 profile.ClassicKeys = keys;
             }
             else
                 profile.ClassicKeys = new List<string>();

             if (listBoxExpansionKeys.Items.Count > 0)
             {
                 List<string> keys = new List<string>();
                 foreach (string key in listBoxExpansionKeys.Items)
                 {
                     keys.Add(key);
                 }
                 profile.ExpansionKeys = keys;
             }
             else
                 profile.ExpansionKeys = new List<string>();

             int a = -1;
             for (int i = 0; i < profiles.Count; i++ )
             {
                  if (profiles[i].ProfileName.Equals(profile.ProfileName))
                  {
                        a = i;
                        break;
                  }
             }

             if (a != -1)
             {
                 if (a != listBoxProfiles.SelectedIndex)
                 {
                     Profile z = new Profile();
                     z = profiles[listBoxProfiles.SelectedIndex];
                     profiles[listBoxProfiles.SelectedIndex] = profile;
                     profiles[a] = z;
                 }
                 else
                 {
                     profiles[a] = profile;
                 }
             }
             else
                 profiles.Add(profile);

             //LoadProfileDefault();
             //listBoxProfiles.SetSelected(0, true);
            
        }

        private void SaveBotSettings()
        {
            config.BanWaitTime = Convert.ToInt32(textBoxTempBanSleepTime.Text);
            config.StartRunDelay = Convert.ToInt32(textBoxStartRunDelay.Text);
            config.CreateGameDelay = Convert.ToInt32(textBoxCreateGameDelay.Text);
            config.LoginTooLongDelay = Convert.ToInt32(textBoxLoginTooLongDelay.Text);
            config.GameJoinTooLongDelay = Convert.ToInt32(textBoxGameJoinTooLongDelay.Text);
            config.CdKeyInUseDelay = Convert.ToInt32(textBoxCdKeyInUseDelay.Text);
            config.CantConnectDelay = Convert.ToInt32(textBoxCantConnectDelay.Text);
            config.Profiles = profiles;

            //Properties.Settings.Default.LauncherStartDelay = Convert.ToInt32(textBoxLauncherStartDelay.Text);
            Properties.Settings.Default.Save();

            BotSettings.WriteSettings(config);
        }

        #endregion

        #region Functions
        private int CharPosition()
        {
            if (radioButtonCharPos0.Checked) { return 0; }
            if (radioButtonCharPos1.Checked) { return 1; }
            if (radioButtonCharPos2.Checked) { return 2; }
            if (radioButtonCharPos3.Checked) { return 3; }
            if (radioButtonCharPos4.Checked) { return 4; }
            if (radioButtonCharPos5.Checked) { return 5; }
            if (radioButtonCharPos6.Checked) { return 6; }
            else { return 7; }
        }

        private void Confirm(string ToConfirm, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit? All information will not be saved.\n" + ToConfirm + " then you can exit and settings will be saved.", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
            /*else
                BotSettingsChanged(this, false);*/
        }

        /* private void Config_FormClosing(object sender, FormClosingEventArgs e)
         {

             if (textBoxDiabloFolderDirectory.Text == string.Empty)
             {
                 Confirm("Select the diablo 2 directory", e);
             }
             else if (textBoxOwnerName.Text == string.Empty)
             {
                 Confirm("Enter your owner name", e);
             }
             else if (textBoxUsername.Text == string.Empty)
             {
                 Confirm("Enter your user name", e);
             }
             else if (textBoxPassword.Text == string.Empty)
             {
                 Confirm("Enter your password", e);
             }
             else if (comboBoxDifficulty.Text == string.Empty)
             {
                 Confirm("Select a difficulty", e);
             }
             else if (textBoxMinGameTime.Text == string.Empty)
             {
                 Confirm("Enter a minimum game time", e);
             }
             else if (textBoxMaxGameTime.Text == string.Empty)
             {
                 Confirm("Enter a maximum game time", e);
             }
             else if (textBoxCreateGameDelay.Text == string.Empty)
             {
                 Confirm("Enter a Create Game Delay", e);
             }
             else if (textBoxStartRunDelay.Text == string.Empty)
             {
                 Confirm("Enter a Start Run Delay", e);
             }
             else if (textBoxTempBanSleepTime.Text == string.Empty)
             {
                 Confirm("Enter a Temp Ban Delay", e);
             }
             else
             {
                 if (chk_Follow.Checked)
                 {
                     if (textBoxMasterAccount.Text == string.Empty)
                         Confirm("Enter a Master Account", e);
                     else if (textBoxMasterCharacter.Text == string.Empty)
                         Confirm("Enter a Master Character", e);
                 }
                 SaveSettings();
                 BotSettingsChanged(this, true);
                 this.Dispose();
             }
         }*/
        #endregion

        #region Form Events

        private void textBoxDiabloFolderDirectory_Click(object sender, EventArgs e)
        {
            /* if (textBoxDiabloFolderDirectory.Text == string.Empty)
             {
                 RegistryKey BlizzardSoftwareKey = Registry.CurrentUser.OpenSubKey("Software\\Blizzard Entertainment\\Diablo II", true);

                 string data = (string) BlizzardSoftwareKey.GetValue("InstallPath");
                
                 if (data != null)
                 {
                     textBoxDiabloFolderDirectory.Text = data;// +"\\Game.exe";
                 }
             }
             else
             {*/
            FolderBrowserDialog fDialog = new FolderBrowserDialog
            {
                Description = "Select a Diablo II folder for executing Diablo",
                SelectedPath = textBoxDiabloFolderDirectory.Text
            };

            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxDiabloFolderDirectory.Text = fDialog.SelectedPath;
            }
            fDialog.Dispose();
            //}
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void checkBoxNoPass_CheckedChanged(object sender, EventArgs e)
        {
            textBoxGamePass.Enabled = (!checkBoxNoPass.Checked);
        }

        private void radioButtonNothing_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMasterAccount.Enabled = !radioButtonNothing.Checked;
            textBoxMasterCharacter.Enabled = !radioButtonNothing.Checked;
            textBoxFollowPassword.Enabled = !radioButtonNothing.Checked;
            textBox_JoinGameDelay.Enabled = !radioButtonNothing.Checked;
            editing = true;
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            if (string.IsNullOrEmpty(t.Text))
                t.Text = "0";
        }

        private void checkBoxAutoMule_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMuleAccount.Enabled = checkBoxAutoMule.Checked;   //nathan's mule
            textBoxMulePass.Enabled = checkBoxAutoMule.Checked;      //nathan's mule
            lbl_warning.Visible = checkBoxAutoMule.Checked;
        }

        /*private void checkBoxD2launcher_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLauncherStartDelay.Enabled = checkBoxD2launcher.Checked;
            Properties.Settings.Default.UseD2Launcher = checkBoxD2launcher.Checked;
            Properties.Settings.Default.Save();
        }*/

        private void buttonClassicKeyAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxClassicKey.Text) && textBoxClassicKey.Text.Length == 16 && !textBoxClassicKey.Text.Contains("-"))
            {
                listBoxClassicKeys.Items.Add(textBoxClassicKey.Text);
                textBoxClassicKey.Text = "";
                editing = true;
            }
            else
                MessageBox.Show("Textfield is empty or Key has the wrong format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonClassicKeyRemove_Click(object sender, EventArgs e)
        {
            if (listBoxClassicKeys.SelectedIndex > -1)
            {
                listBoxClassicKeys.Items.RemoveAt(listBoxClassicKeys.SelectedIndex);
                editing = true;
            }
        }

        private void buttonExpansionKeyAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxExpansionKey.Text) && textBoxExpansionKey.Text.Length == 16 && !textBoxExpansionKey.Text.Contains("-"))
            {
                listBoxExpansionKeys.Items.Add(textBoxExpansionKey.Text);
                textBoxExpansionKey.Text = "";
                editing = true;
            }
            else
                MessageBox.Show("Textfield is empty or Key has the wrong format!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonExpansionKeyRemove_Click(object sender, EventArgs e)
        {
            if (listBoxExpansionKeys.SelectedIndex > -1)
            {
                listBoxExpansionKeys.Items.RemoveAt(listBoxExpansionKeys.SelectedIndex);
                editing = true;
            }
        }

        #endregion

        private void button_Save_Click(object sender, EventArgs e)
        {
            if (listBoxProfiles.SelectedIndex > -1)
            {
                SaveProfile();
                editing = false;
                MessageBox.Show("Selected Profile stored!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
                MessageBox.Show("No Profile selected!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void frmBotSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (editing)
            {
                if (MessageBox.Show("You are currently editing a profile. Are you sure you want to exit? \nAll information will not be saved!", "Info", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }

            SaveBotSettings();
            BotSettingsChanged(this, true);
        }

        private void buttonProfileAdd_Click(object sender, EventArgs e)
        {
            if (editing)
            {
                if (MessageBox.Show("You are currently editing a profile. Are you sure you want to exit? \nAll information will not be saved!", "Info", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.No)
                    return;
            }

            if (!string.IsNullOrEmpty(textBoxProfileName.Text))
            {
                foreach (string s in listBoxProfiles.Items)
                {
                    if (s.Equals(textBoxProfileName.Text))
                    {
                        MessageBox.Show("Profile name already used!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                listBoxProfiles.Items.Add(textBoxProfileName.Text);
                LoadProfileDefault();
                listBoxProfiles.SetSelected(listBoxProfiles.Items.Count - 1, true);
                selectedindex = listBoxProfiles.Items.Count - 1;
            }
            else
                MessageBox.Show("No Profile name specified!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonProfileRemove_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < profiles.Count; i++)
            {
                if (profiles[i].ProfileName.Equals(listBoxProfiles.Items[listBoxProfiles.SelectedIndex]))
                {
                    profiles.RemoveAt(i);
                }
            }

            if (listBoxProfiles.SelectedIndex > -1)
                listBoxProfiles.Items.RemoveAt(listBoxProfiles.SelectedIndex);
            if (listBoxProfiles.Items.Count > 0)
            {
                listBoxProfiles.SelectedIndexChanged -= this.listBoxProfiles_SelectedIndexChanged;
                listBoxProfiles.SetSelected(0, true);
                listBoxProfiles.SelectedIndexChanged += new EventHandler(listBoxProfiles_SelectedIndexChanged);
            }
        }

        private void listBoxProfiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (loaded)
            {
                if (listBoxProfiles.SelectedIndex > -1)
                {
                    if (editing)
                    {
                        if (MessageBox.Show("You are currently editing a profile. Are you sure you want to exit? \nAll information will not be saved!", "Info", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.No)
                        {
                            listBoxProfiles.SelectedIndexChanged -= this.listBoxProfiles_SelectedIndexChanged;
                            listBoxProfiles.SetSelected(selectedindex, true);
                            listBoxProfiles.SelectedIndexChanged += new EventHandler(listBoxProfiles_SelectedIndexChanged);
                            return;
                        }
                    }

                    selectedindex = listBoxProfiles.SelectedIndex;

                    LoadProfileDefault();
                    editing = false;

                    for (int i = 0; i < profiles.Count; i++)
                    {
                        if (profiles[i].ProfileName.Equals(listBoxProfiles.Items[listBoxProfiles.SelectedIndex]))
                        {
                            LoadProfile(i);
                            editing = false;
                            break;
                        }

                    }

                }
            }
        }

        /*private void buttonMoveUp_Click(object sender, EventArgs e)
        {
            if (listBoxProfiles.SelectedIndex > -1)
            {
                object item = listBoxProfiles.SelectedItem;
                if (listBoxProfiles.SelectedIndex > 0)
                {
                    int i = listBoxProfiles.SelectedIndex;
                    listBoxProfiles.Items.Remove(item);
                    listBoxProfiles.Items.Insert(i - 1, item);
                    listBoxProfiles.SelectedIndex = i - 1;                    
                }
            }
            else
                MessageBox.Show("No Profile selected!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonMoveDown_Click(object sender, EventArgs e)
        {
            if (listBoxProfiles.SelectedIndex > -1)
            {
                object item = listBoxProfiles.SelectedItem;
                if (listBoxProfiles.SelectedIndex + 1 < listBoxProfiles.Items.Count)
                {
                    int i = listBoxProfiles.SelectedIndex;
                    listBoxProfiles.Items.Remove(item);
                    listBoxProfiles.Items.Insert(i + 1, item);
                    listBoxProfiles.SelectedIndex = i + 1;
                }
            }
            else
                MessageBox.Show("No Profile selected!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }*/

        private void textbox_textChanged(object sender, EventArgs e)
        {
            editing = true;
        }

        private void comboBoxDifficulty_SelectedIndexChanged(object sender, EventArgs e)
        {
            editing = true;
        }

        /*private void checkBoxD2launcher_CheckStateChanged(object sender, EventArgs e)
        {
            editing = true;
        }*/

        private void radioButtonCharPos0_CheckedChanged(object sender, EventArgs e)
        {
            editing = true;
        }

        private void textBoxStartRunDelay_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
