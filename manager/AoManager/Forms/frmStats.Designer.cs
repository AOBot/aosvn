﻿namespace AoManager
{
    partial class frmStats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.totalExpLabel = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.AvgExpLabel = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lbl_AvgTime = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lbl_deaths = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lbl_chicken = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_Action = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_Uptime = new System.Windows.Forms.Label();
            this.lbl_Rate = new System.Windows.Forms.Label();
            this.lbl_Games = new System.Windows.Forms.Label();
            this.lbl_Bans = new System.Windows.Forms.Label();
            this.lbl_Interv = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblProfileName = new System.Windows.Forms.Label();
            this.buttonStopStart = new System.Windows.Forms.Button();
            this.buttonHideShow = new System.Windows.Forms.Button();
            this.timer_UpTime = new System.Windows.Forms.Timer(this.components);
            this.MenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.hideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // totalExpLabel
            // 
            this.totalExpLabel.AutoSize = true;
            this.totalExpLabel.BackColor = System.Drawing.Color.Transparent;
            this.totalExpLabel.Location = new System.Drawing.Point(191, 27);
            this.totalExpLabel.Name = "totalExpLabel";
            this.totalExpLabel.Size = new System.Drawing.Size(13, 13);
            this.totalExpLabel.TabIndex = 47;
            this.totalExpLabel.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(99, 27);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(66, 13);
            this.label28.TabIndex = 46;
            this.label28.Text = "Exp gained :";
            // 
            // AvgExpLabel
            // 
            this.AvgExpLabel.AutoSize = true;
            this.AvgExpLabel.BackColor = System.Drawing.Color.Transparent;
            this.AvgExpLabel.Location = new System.Drawing.Point(191, 40);
            this.AvgExpLabel.Name = "AvgExpLabel";
            this.AvgExpLabel.Size = new System.Drawing.Size(13, 13);
            this.AvgExpLabel.TabIndex = 45;
            this.AvgExpLabel.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(99, 40);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 13);
            this.label26.TabIndex = 44;
            this.label26.Text = "Avg Exp/Game :";
            // 
            // lbl_AvgTime
            // 
            this.lbl_AvgTime.AutoSize = true;
            this.lbl_AvgTime.BackColor = System.Drawing.Color.Transparent;
            this.lbl_AvgTime.Location = new System.Drawing.Point(191, 66);
            this.lbl_AvgTime.Name = "lbl_AvgTime";
            this.lbl_AvgTime.Size = new System.Drawing.Size(13, 13);
            this.lbl_AvgTime.TabIndex = 42;
            this.lbl_AvgTime.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(99, 66);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 13);
            this.label24.TabIndex = 41;
            this.label24.Text = "Avg Game Time:";
            // 
            // lbl_deaths
            // 
            this.lbl_deaths.AutoSize = true;
            this.lbl_deaths.BackColor = System.Drawing.Color.Transparent;
            this.lbl_deaths.Location = new System.Drawing.Point(80, 66);
            this.lbl_deaths.Name = "lbl_deaths";
            this.lbl_deaths.Size = new System.Drawing.Size(13, 13);
            this.lbl_deaths.TabIndex = 40;
            this.lbl_deaths.Text = "0";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(12, 66);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 13);
            this.label22.TabIndex = 39;
            this.label22.Text = "Deaths:";
            // 
            // lbl_chicken
            // 
            this.lbl_chicken.AutoSize = true;
            this.lbl_chicken.BackColor = System.Drawing.Color.Transparent;
            this.lbl_chicken.Location = new System.Drawing.Point(80, 53);
            this.lbl_chicken.Name = "lbl_chicken";
            this.lbl_chicken.Size = new System.Drawing.Size(13, 13);
            this.lbl_chicken.TabIndex = 38;
            this.lbl_chicken.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(12, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 37;
            this.label10.Text = "Chickens:";
            // 
            // lbl_Action
            // 
            this.lbl_Action.AutoSize = true;
            this.lbl_Action.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Action.Location = new System.Drawing.Point(132, 92);
            this.lbl_Action.Name = "lbl_Action";
            this.lbl_Action.Size = new System.Drawing.Size(33, 13);
            this.lbl_Action.TabIndex = 36;
            this.lbl_Action.Text = "None";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(52, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 35;
            this.label9.Text = "Action:";
            // 
            // lbl_Uptime
            // 
            this.lbl_Uptime.AutoSize = true;
            this.lbl_Uptime.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Uptime.Location = new System.Drawing.Point(191, 53);
            this.lbl_Uptime.Name = "lbl_Uptime";
            this.lbl_Uptime.Size = new System.Drawing.Size(13, 13);
            this.lbl_Uptime.TabIndex = 34;
            this.lbl_Uptime.Text = "0";
            // 
            // lbl_Rate
            // 
            this.lbl_Rate.AutoSize = true;
            this.lbl_Rate.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Rate.Location = new System.Drawing.Point(80, 40);
            this.lbl_Rate.Name = "lbl_Rate";
            this.lbl_Rate.Size = new System.Drawing.Size(13, 13);
            this.lbl_Rate.TabIndex = 33;
            this.lbl_Rate.Text = "0";
            // 
            // lbl_Games
            // 
            this.lbl_Games.AutoSize = true;
            this.lbl_Games.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Games.Location = new System.Drawing.Point(80, 27);
            this.lbl_Games.Name = "lbl_Games";
            this.lbl_Games.Size = new System.Drawing.Size(13, 13);
            this.lbl_Games.TabIndex = 32;
            this.lbl_Games.Text = "0";
            // 
            // lbl_Bans
            // 
            this.lbl_Bans.AutoSize = true;
            this.lbl_Bans.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Bans.Location = new System.Drawing.Point(80, 79);
            this.lbl_Bans.Name = "lbl_Bans";
            this.lbl_Bans.Size = new System.Drawing.Size(13, 13);
            this.lbl_Bans.TabIndex = 31;
            this.lbl_Bans.Text = "0";
            // 
            // lbl_Interv
            // 
            this.lbl_Interv.AutoSize = true;
            this.lbl_Interv.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Interv.Location = new System.Drawing.Point(191, 79);
            this.lbl_Interv.Name = "lbl_Interv";
            this.lbl_Interv.Size = new System.Drawing.Size(13, 13);
            this.lbl_Interv.TabIndex = 30;
            this.lbl_Interv.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(99, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Uptime:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(12, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Success %:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(13, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Games:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(12, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Bans:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(99, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Interventions:";
            // 
            // lblProfileName
            // 
            this.lblProfileName.AutoSize = true;
            this.lblProfileName.BackColor = System.Drawing.Color.Transparent;
            this.lblProfileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfileName.Location = new System.Drawing.Point(80, 11);
            this.lblProfileName.Name = "lblProfileName";
            this.lblProfileName.Size = new System.Drawing.Size(53, 16);
            this.lblProfileName.TabIndex = 48;
            this.lblProfileName.Text = "Profile";
            // 
            // buttonStopStart
            // 
            this.buttonStopStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStopStart.Location = new System.Drawing.Point(16, 108);
            this.buttonStopStart.Name = "buttonStopStart";
            this.buttonStopStart.Size = new System.Drawing.Size(90, 23);
            this.buttonStopStart.TabIndex = 49;
            this.buttonStopStart.Text = "Start";
            this.buttonStopStart.UseVisualStyleBackColor = true;
            this.buttonStopStart.Click += new System.EventHandler(this.buttonStopStart_Click);
            // 
            // buttonHideShow
            // 
            this.buttonHideShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonHideShow.Location = new System.Drawing.Point(114, 108);
            this.buttonHideShow.Name = "buttonHideShow";
            this.buttonHideShow.Size = new System.Drawing.Size(90, 23);
            this.buttonHideShow.TabIndex = 50;
            this.buttonHideShow.Text = "Hide";
            this.buttonHideShow.UseVisualStyleBackColor = true;
            this.buttonHideShow.Click += new System.EventHandler(this.buttonHideShow_Click);
            // 
            // timer_UpTime
            // 
            this.timer_UpTime.Interval = 60000;
            this.timer_UpTime.Tick += new System.EventHandler(this.timer_UpTime_Tick);
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hideToolStripMenuItem});
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size(159, 26);
            // 
            // hideToolStripMenuItem
            // 
            this.hideToolStripMenuItem.Name = "hideToolStripMenuItem";
            this.hideToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.hideToolStripMenuItem.Text = "Hide Status Win";
            this.hideToolStripMenuItem.Click += new System.EventHandler(this.hideToolStripMenuItem_Click);
            // 
            // frmStats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::AoManager.Properties.Resources.stats_page_background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(246, 149);
            this.ContextMenuStrip = this.MenuStrip;
            this.Controls.Add(this.label28);
            this.Controls.Add(this.lblProfileName);
            this.Controls.Add(this.totalExpLabel);
            this.Controls.Add(this.AvgExpLabel);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.buttonStopStart);
            this.Controls.Add(this.lbl_AvgTime);
            this.Controls.Add(this.lbl_deaths);
            this.Controls.Add(this.buttonHideShow);
            this.Controls.Add(this.lbl_chicken);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbl_Uptime);
            this.Controls.Add(this.lbl_Rate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lbl_Games);
            this.Controls.Add(this.lbl_Action);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbl_Bans);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbl_Interv);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmStats";
            this.Opacity = 0.7D;
            this.ShowInTaskbar = false;
            this.Text = "Stats";
            this.TopMost = true;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmStats_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmStats_MouseMove);
            this.MenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label totalExpLabel;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label AvgExpLabel;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lbl_AvgTime;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lbl_deaths;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lbl_chicken;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_Action;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_Uptime;
        private System.Windows.Forms.Label lbl_Rate;
        private System.Windows.Forms.Label lbl_Games;
        private System.Windows.Forms.Label lbl_Bans;
        private System.Windows.Forms.Label lbl_Interv;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblProfileName;
        private System.Windows.Forms.Button buttonStopStart;
        private System.Windows.Forms.Button buttonHideShow;
        private System.Windows.Forms.Timer timer_UpTime;
        private System.Windows.Forms.ContextMenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem hideToolStripMenuItem;
    }
}