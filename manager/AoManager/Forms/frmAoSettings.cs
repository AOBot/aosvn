﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;

namespace AoManager
{
    public partial class frmAoSettings : Form
    {
        public delegate void frmAoSettings_Handler(object sender, bool saved);

        public event frmAoSettings_Handler AoSettingsSaved;

        public frmAoSettings()
        {
            InitializeComponent();
            if (!Properties.Settings.Default.IsAoSettings || !File.Exists(AoSettings.xmlpath))
            {
                comboBoxRealm.SelectedIndex = 0;
                if (File.Exists(Application.StartupPath + "\\Awesom-O.exe"))
                    textBoxAwesomoDirectory.Text = Application.StartupPath;
            }
            else
                LoadAoConfig();
        }

        private void LoadAoConfig()
        {
            AoSettingStruct config = AoSettings.GetSettings();
            if (config != null)
            {
                textBoxAwesomoDirectory.Text = config.AOPath;
                textBoxClickDelay.Text = config.ClickDelay.ToString();
                textBoxKeyDelay.Text = config.KeyDelay.ToString();
                comboBoxRealm.Text = config.Realm;
                textBoxDiabloDirectory.Text = config.DiabloFolder;
                textBoxStartDelay.Text = config.StartDelay.ToString();
                txt_RunOnBan.Text = config.RunOnBanExePath;
                chk_HideCore.Checked = config.HideCore;
                chk_Stealth.Checked = config.Stealth;
                chk_AoNoStart.Checked = config.AoNoStart;
                checkBoxIpChanger.Checked = config.IpChangerUse;
                chk_RunOnBan.Checked = config.RunOnBan;

                if (config.Expert)
                {
                    txtAoFlags.Text = config.AoFlags;
                    chk_Expert.Checked = true;
                }

                if (config.ExpertIp)
                {
                    txt_Cip.Text = config.ControllerIp;
                    chk_Cip.Checked = true;
                }
            }
        }

        private void textBoxAwesomoDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fDialog = new FolderBrowserDialog
            {
                Description = "Select the Awesom-O Core folder to read from",
                SelectedPath = textBoxAwesomoDirectory.Text
            };

            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxAwesomoDirectory.Text = fDialog.SelectedPath;
            }
            fDialog.Dispose();
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Confirm(string toConfirm, CancelEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit? All information will not be saved.\n" + toConfirm + " then you can exit and settings will be saved.", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
                AoSettingsSaved(this, false);
        }

        private void SaveSettings()
        {
            AoSettingStruct config = new AoSettingStruct();

            config.AOPath = textBoxAwesomoDirectory.Text;
            config.ClickDelay = Convert.ToInt32(textBoxClickDelay.Text);
            config.KeyDelay = Convert.ToInt32(textBoxKeyDelay.Text);
            config.Expert = chk_Expert.Checked;
            config.ExpertIp = chk_Cip.Checked;
            config.HideCore = chk_HideCore.Checked;
            config.AoNoStart = chk_AoNoStart.Checked;
            config.DiabloFolder = textBoxDiabloDirectory.Text;
            config.RunOnBanExePath = txt_RunOnBan.Text;
            config.Realm = comboBoxRealm.Text;
            config.Stealth = chk_Stealth.Checked;
            config.StartDelay = Convert.ToInt32(textBoxStartDelay.Text);
            config.IpChangerUse = checkBoxIpChanger.Checked;
            config.RunOnBan = chk_RunOnBan.Checked;

            if (chk_Expert.Checked)
                config.AoFlags = txtAoFlags.Text;
            else 
            {
                config.AoFlags = "-d \"" + config.DiabloFolder + "\"" + " -r " + config.Realm;
                if (chk_Stealth.Checked)
                    config.AoFlags = config.AoFlags + " -s";
            }
            if (chk_Cip.Checked)
                config.ControllerIp = txt_Cip.Text;
            else
                config.ControllerIp = "127.0.0.1";

            AoSettings.WriteSettings(config);
        }


        private void Global_Settings_Config_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxAwesomoDirectory.Text))
                Confirm("Select the Awesom-O directory!", e);
            else if (!chk_Expert.Checked && string.IsNullOrEmpty(textBoxDiabloDirectory.Text))
                Confirm("Select the Diablo 2 directory!", e);
            else if (string.IsNullOrEmpty(textBoxKeyDelay.Text))
                Confirm("Define a Key delay!", e);
            else if (string.IsNullOrEmpty(textBoxClickDelay.Text))
                Confirm("Define a Click delay!", e);
            else if (string.IsNullOrEmpty(textBoxStartDelay.Text))
                Confirm("Define a Start delay!", e);
            else if (chk_Expert.Checked && string.IsNullOrEmpty(txtAoFlags.Text))
                Confirm("Define some Ao Flags!", e);
            else
            {
                SaveSettings();
                AoSettingsSaved(this, true);
                this.Dispose();
            }
        }

        private void textBoxDiabloDirectory_Click(object sender, EventArgs e)
        {        
                FolderBrowserDialog fDialog = new FolderBrowserDialog
                {
                    Description = "Select a Diablo II folder for the Awesom-O Core to read from",
                    SelectedPath = textBoxDiabloDirectory.Text
                };

                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    textBoxDiabloDirectory.Text = fDialog.SelectedPath;
                }
                fDialog.Dispose();            
        }

        private void chk_Expert_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_Expert.Checked)
            {
                textBoxDiabloDirectory.Enabled = false;
                comboBoxRealm.Enabled = false;
                chk_Stealth.Enabled = false;
                txtAoFlags.Enabled = true;
            }
            else
            {
                textBoxDiabloDirectory.Enabled = true;
                comboBoxRealm.Enabled = true;
                chk_Stealth.Enabled = true;
                txtAoFlags.Enabled = false;
            }
        }

        private void chk_Cip_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_Cip.Checked)
                txt_Cip.Enabled = true;
            else
                txt_Cip.Enabled = false;
        }

        private void checkBoxIpChanger_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxIpChanger.Checked && !Properties.Settings.Default.IsIpSettings)
            {
                frmIpChanger frmIP = new frmIpChanger();
                frmIP.IpChangersSaved += new frmIpChanger.frmIPChanger_Handler(frmIpChanger_IpChangerChanged);
                frmIP.ShowDialog();
                if (frmIP.DialogResult == DialogResult.Abort)
                    checkBoxIpChanger.Checked = false;
            }
        }

        void frmIpChanger_IpChangerChanged(object sender, bool saved)
        {
            if (saved)
            {
                Properties.Settings.Default.IsIpSettings = true;
                Properties.Settings.Default.Save();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmIpChanger frmIP = new frmIpChanger();
            frmIP.IpChangersSaved += new frmIpChanger.frmIPChanger_Handler(frmIpChanger_IpChangerChanged);
            frmIP.ShowDialog();
        }

        private void chk_RunOnBan_CheckedChanged(object sender, EventArgs e)
        {
            txt_RunOnBan.Enabled = chk_RunOnBan.Checked;
        }

        private void txt_RunOnBan_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();

            if (f.ShowDialog() == DialogResult.OK)
                txt_RunOnBan.Text = f.FileName;

            f.Dispose();

        }

        private void comboBoxRealm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxRealm.Text == "classicbeta.battle.net")
            {
                MessageBox.Show("Note: If you want to bot on PTR 1.13 you have to select a Diablo folder which does contain Diablo version 1.12 mpq files!\nPls be aware AO does not fully support 1.13 yet and it will crash on new items which will drop by andy etc.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
