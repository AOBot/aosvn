﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;

namespace AoManager
{
    public partial class frmIpChanger : Form
    {
        public delegate void frmIPChanger_Handler(object sender, bool saved);

        public event frmIPChanger_Handler IpChangersSaved;

        public frmIpChanger()
        {
            InitializeComponent();
            if (Properties.Settings.Default.IsIpSettings || File.Exists(IpChanger.xmlpath))
                LoadIpConfig();
            if (!checkBoxChallenge.Checked)
            {
                textBoxChallengeUrl.Enabled = false;
                textBoxChallengeSearch.Enabled = false;
            }
            if (checkBoxAutoReconnect.Checked)
            {
               textBoxConnUrl.Enabled = false;
               checkBoxPostRecon.Enabled = false;
            }
            if (!checkBoxPreAuth.Checked)
            {
               textBoxPreAuthURL.Enabled = false;
            }
        }

        private void LoadIpConfig()
        {
            IpChangerStruct config = IpChanger.GetSettings();
            if (config != null)
            {
                textBoxRouterIp.Text = config.ip.ToString();
                textBoxLoginUrl.Text = config.login.ToString();
                textBoxDiscUrl.Text = config.disconnect.ToString();
                textBoxConnUrl.Text = config.connect.ToString();
                textBoxUsername.Text = config.username.ToString();
                maskedTextBox1.Text = config.password.ToString();
                textBoxPreAuthURL.Text = config.preAuthUrl.ToString();
                textBoxChallengeUrl.Text = config.challengeUrl.ToString();
                textBoxChallengeSearch.Text = config.challengeSearch.ToString();
                checkBoxChallenge.Checked = (bool)config.challenge;
                checkBoxPostLogin.Checked = (bool)config.post;
                checkBoxPostDiscon.Checked = (bool)config.postDisconnect;
                checkBoxPostRecon.Checked = (bool)config.postReconnect;
                checkBoxAutoReconnect.Checked = (bool)config.autoReconnect;
                checkBoxPreAuth.Checked = (bool)config.preAuth;
            }
        }

        private void SaveSettings()
        {
            IpChangerStruct config = new IpChangerStruct();

            config.ip = textBoxRouterIp.Text;
            config.login = textBoxLoginUrl.Text;
            config.disconnect = textBoxDiscUrl.Text;
            config.connect = textBoxConnUrl.Text;
            config.username = textBoxUsername.Text;
            config.password = maskedTextBox1.Text;
            config.preAuthUrl = textBoxPreAuthURL.Text;
            config.challengeUrl = textBoxChallengeUrl.Text;
            config.challengeSearch = textBoxChallengeSearch.Text;
            config.challenge = checkBoxChallenge.Checked;
            config.post = checkBoxPostLogin.Checked;
            config.postDisconnect = checkBoxPostDiscon.Checked;
            config.postReconnect = checkBoxPostRecon.Checked;
            config.autoReconnect = checkBoxAutoReconnect.Checked;
            config.preAuth = checkBoxPreAuth.Checked;

            IpChanger.WriteSettings(config);
        }

        private void frmIpChanger_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxRouterIp.Text))
                Confirm("Define the rounter's IP address!", e);
            else if (string.IsNullOrEmpty(textBoxLoginUrl.Text))
                Confirm("Define the router's login URL!", e);
            else if (string.IsNullOrEmpty(textBoxDiscUrl.Text))
                Confirm("Define the router's URL to disconnect!", e);
            else if (string.IsNullOrEmpty(textBoxConnUrl.Text) && !checkBoxAutoReconnect.Checked)
                Confirm("Define the router's URL to connect!", e);
            else if (string.IsNullOrEmpty(maskedTextBox1.Text))
                Confirm("Define the password to connect!", e);
            else if (checkBoxChallenge.Checked && string.IsNullOrEmpty(textBoxChallengeSearch.Text) && string.IsNullOrEmpty(textBoxChallengeUrl.Text))
                Confirm("Either disable Challenge, or define the URL and search string!", e);
            else if (checkBoxPreAuth.Checked && string.IsNullOrEmpty(textBoxPreAuthURL.Text))
               Confirm("Either disable Challenge, or define the URL and search string!", e);
            else
            {
                SaveSettings();
                IpChangersSaved(this, true);
                this.Dispose();
            }
        }

        private void Confirm(string toConfirm, CancelEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit? All information will not be saved.\n" + toConfirm + " then you can exit and settings will be saved.", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                IpChangersSaved(this, false);
                this.DialogResult = DialogResult.Abort;
            }
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
           if (MessageBox.Show("Warning: This will disconnect your Internet connection and save all changes!\nAre you sure you want to test?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
           {
              buttonTest.Enabled = false;
              SaveSettings();
              IpControl.ChangeIP();
              buttonTest.Enabled = true;
           }
        }

        private void checkBoxChallenge_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxChallenge.Checked)
            {
                textBoxChallengeUrl.Enabled = true;
                textBoxChallengeSearch.Enabled = true;
                checkBoxPreAuth.Checked = false;
            }
            else
            {
                textBoxChallengeUrl.Enabled = false;
                textBoxChallengeSearch.Enabled = false;
            }
        }

        private void checkBoxAutoReconnect_CheckedChanged(object sender, EventArgs e)
        {
           if (checkBoxAutoReconnect.Checked)
           {
              textBoxConnUrl.Enabled = false;
              checkBoxPostRecon.Enabled = false;
           }
           else
           {
              textBoxConnUrl.Enabled = true;
              checkBoxPostRecon.Enabled = true;
           }
        }

        private void checkBoxPreAuth_CheckedChanged(object sender, EventArgs e)
        {
           if (checkBoxPreAuth.Checked)
           {
              textBoxPreAuthURL.Enabled = true;
              checkBoxChallenge.Checked = false;
           }
           else
           {
              textBoxPreAuthURL.Enabled = false;
           }
        }
    }
}
