﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace AoManager
{
    public partial class frmStats : Form
    {
        private string ProfileName;
        private Point mouseOffset;
        private DiaWatcher b;
        private int interventions = 0;
        private delegate void frmStats_delagate(BotStatistic stat);
        private delegate void frmStats_delagate_status(string msg);
        private Stopwatch Uptimer = new Stopwatch();

        public frmStats(string profileName, DiaWatcher b , bool d2windowhidden)
        {
            InitializeComponent();
            this.ProfileName = profileName;
            lblProfileName.Text = this.ProfileName;
            this.b = b;
            if (d2windowhidden)
                buttonHideShow.Text = "Show";
        }

        public void IncreaseInterventions()
        {
            interventions++;
        }

        public void AddStatistics(BotStatistic stat)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new frmStats_delagate(AddStatistics),  stat );
                return;
            }
             
            lbl_Games.Text = stat.Games.ToString();
            lbl_Rate.Text = ((int)stat.SuccessRate).ToString() + " %";
            lbl_Bans.Text = stat.Bans.ToString();
            lbl_chicken.Text = stat.Chickens.ToString();
            lbl_deaths.Text = stat.Deaths.ToString();
            lbl_AvgTime.Text = stat.AverageGameTime.ToString() + " s";
            totalExpLabel.Text = stat.TotalExpGained.ToString() + " exp";
            AvgExpLabel.Text = stat.AvgGameExp.ToString() + " exp";

            lbl_Interv.Text = interventions.ToString();
            Uptimer.Start();
            timer_UpTime.Start();

        }

        public void UpdateStatus(string msg)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new frmStats_delagate_status(UpdateStatus), msg);
                return;
            }

            lbl_Action.Text = msg;
        }

        #region Form Events
        private void frmStats_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
        }

        private void frmStats_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos;
            }
        }


        private void btn_ShowLog_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(Application.StartupPath + "\\AoManager.log"))
                {
                    Process.Start(new ProcessStartInfo(Application.StartupPath + "\\AoManager.log"));
                }
            }
            catch (Exception) { }
        }

        #endregion

        private void buttonStopStart_Click(object sender, EventArgs e)
        {
            if (buttonStopStart.Text == "Stop")
            {
                buttonStopStart.Text = "Start";
                b.NextAction.Clear();
                b.NextError.Clear();
                b.NextAction.Enqueue(new ADiaStop());
                lbl_Action.Text = "Stopped";
                Uptimer.Stop();
                timer_UpTime.Stop();
            }
            else
            {
                buttonStopStart.Text = "Stop";
                b.NextAction.Enqueue(new ADiaStart());
                Uptimer.Start();
                timer_UpTime.Start();
            }
        }

        private void buttonHideShow_Click(object sender, EventArgs e)
        {
            if (buttonHideShow.Text == "Hide")
            {
                buttonHideShow.Text = "Show";
                b.HideDiablo();
            }
            else
            {
                buttonHideShow.Text = "Hide";
                b.ShowDiablo();
            }
        }

        private void timer_UpTime_Tick(object sender, EventArgs e)
        {
            lbl_Uptime.Text = (Uptimer.ElapsedMilliseconds / 1000 / 60).ToString() + " m";
        }

        private void hideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

    }
}
