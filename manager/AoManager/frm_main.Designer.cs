﻿namespace AoManager
{
    partial class frm_main
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.btn_BotSettings = new System.Windows.Forms.Button();
            this.btn_AoSettings = new System.Windows.Forms.Button();
            this.btn_Start = new System.Windows.Forms.Button();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.trayIconMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemStart = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hideDiabloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showCoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hideCoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gb_Items = new System.Windows.Forms.GroupBox();
            this.lbl_Gambled = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.btn_Items = new System.Windows.Forms.Button();
            this.lbl_Stashed = new System.Windows.Forms.Label();
            this.lbl_Sold = new System.Windows.Forms.Label();
            this.lbl_Picked = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage_Status = new System.Windows.Forms.TabPage();
            this.gb_Status = new System.Windows.Forms.GroupBox();
            this.btn_hidestat = new System.Windows.Forms.Button();
            this.btn_showstat = new System.Windows.Forms.Button();
            this.gb_error = new System.Windows.Forms.GroupBox();
            this.btn_clearerrors = new System.Windows.Forms.Button();
            this.listView_Error = new System.Windows.Forms.ListView();
            this.column_ErrorCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.column_Description = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.column_Time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnProfile = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage_About = new System.Windows.Forms.TabPage();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.label21 = new System.Windows.Forms.Label();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.label20 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label19 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.trayIconMenu.SuspendLayout();
            this.gb_Items.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage_Status.SuspendLayout();
            this.gb_Status.SuspendLayout();
            this.gb_error.SuspendLayout();
            this.tabPage_About.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_BotSettings
            // 
            this.btn_BotSettings.Location = new System.Drawing.Point(119, 405);
            this.btn_BotSettings.Name = "btn_BotSettings";
            this.btn_BotSettings.Size = new System.Drawing.Size(85, 23);
            this.btn_BotSettings.TabIndex = 0;
            this.btn_BotSettings.Text = "Bot Settings";
            this.btn_BotSettings.UseVisualStyleBackColor = true;
            this.btn_BotSettings.Click += new System.EventHandler(this.btn_botsettings_Click);
            // 
            // btn_AoSettings
            // 
            this.btn_AoSettings.Location = new System.Drawing.Point(6, 405);
            this.btn_AoSettings.Name = "btn_AoSettings";
            this.btn_AoSettings.Size = new System.Drawing.Size(85, 23);
            this.btn_AoSettings.TabIndex = 1;
            this.btn_AoSettings.Text = "Ao Settings";
            this.btn_AoSettings.UseVisualStyleBackColor = true;
            this.btn_AoSettings.Click += new System.EventHandler(this.btn_AoSettings_Click);
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(228, 405);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(85, 23);
            this.btn_Start.TabIndex = 2;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // trayIcon
            // 
            this.trayIcon.ContextMenuStrip = this.trayIconMenu;
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Visible = true;
            this.trayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trayIcon_MouseDoubleClick);
            // 
            // trayIconMenu
            // 
            this.trayIconMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemStart,
            this.showToolStripMenuItem,
            this.hideDiabloToolStripMenuItem,
            this.showCoreToolStripMenuItem,
            this.hideCoreToolStripMenuItem});
            this.trayIconMenu.Name = "trayIconMenu";
            this.trayIconMenu.Size = new System.Drawing.Size(144, 114);
            // 
            // MenuItemStart
            // 
            this.MenuItemStart.Name = "MenuItemStart";
            this.MenuItemStart.Size = new System.Drawing.Size(143, 22);
            this.MenuItemStart.Text = "Start";
            this.MenuItemStart.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.showToolStripMenuItem.Text = "Show Diablo";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // hideDiabloToolStripMenuItem
            // 
            this.hideDiabloToolStripMenuItem.Name = "hideDiabloToolStripMenuItem";
            this.hideDiabloToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.hideDiabloToolStripMenuItem.Text = "Hide Diablo";
            this.hideDiabloToolStripMenuItem.Click += new System.EventHandler(this.hideDiabloToolStripMenuItem_Click);
            // 
            // showCoreToolStripMenuItem
            // 
            this.showCoreToolStripMenuItem.Name = "showCoreToolStripMenuItem";
            this.showCoreToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.showCoreToolStripMenuItem.Text = "Show Core";
            this.showCoreToolStripMenuItem.Click += new System.EventHandler(this.showCoreToolStripMenuItem_Click);
            // 
            // hideCoreToolStripMenuItem
            // 
            this.hideCoreToolStripMenuItem.Name = "hideCoreToolStripMenuItem";
            this.hideCoreToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.hideCoreToolStripMenuItem.Text = "Hide Core";
            this.hideCoreToolStripMenuItem.Click += new System.EventHandler(this.hideCoreToolStripMenuItem_Click);
            // 
            // gb_Items
            // 
            this.gb_Items.Controls.Add(this.lbl_Gambled);
            this.gb_Items.Controls.Add(this.label23);
            this.gb_Items.Controls.Add(this.btn_Items);
            this.gb_Items.Controls.Add(this.lbl_Stashed);
            this.gb_Items.Controls.Add(this.lbl_Sold);
            this.gb_Items.Controls.Add(this.lbl_Picked);
            this.gb_Items.Controls.Add(this.label3);
            this.gb_Items.Controls.Add(this.label2);
            this.gb_Items.Controls.Add(this.label1);
            this.gb_Items.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_Items.Location = new System.Drawing.Point(3, 290);
            this.gb_Items.Name = "gb_Items";
            this.gb_Items.Size = new System.Drawing.Size(166, 79);
            this.gb_Items.TabIndex = 4;
            this.gb_Items.TabStop = false;
            this.gb_Items.Text = "Items:";
            this.gb_Items.Enter += new System.EventHandler(this.gb_Items_Enter);
            // 
            // lbl_Gambled
            // 
            this.lbl_Gambled.AutoSize = true;
            this.lbl_Gambled.Location = new System.Drawing.Point(61, 48);
            this.lbl_Gambled.Name = "lbl_Gambled";
            this.lbl_Gambled.Size = new System.Drawing.Size(13, 13);
            this.lbl_Gambled.TabIndex = 8;
            this.lbl_Gambled.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 48);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 13);
            this.label23.TabIndex = 7;
            this.label23.Text = "Gambled:";
            // 
            // btn_Items
            // 
            this.btn_Items.Location = new System.Drawing.Point(90, 19);
            this.btn_Items.Name = "btn_Items";
            this.btn_Items.Size = new System.Drawing.Size(57, 48);
            this.btn_Items.TabIndex = 6;
            this.btn_Items.Text = "Show Items";
            this.btn_Items.UseVisualStyleBackColor = true;
            this.btn_Items.Click += new System.EventHandler(this.btn_Items_Click);
            // 
            // lbl_Stashed
            // 
            this.lbl_Stashed.AutoSize = true;
            this.lbl_Stashed.Location = new System.Drawing.Point(61, 32);
            this.lbl_Stashed.Name = "lbl_Stashed";
            this.lbl_Stashed.Size = new System.Drawing.Size(13, 13);
            this.lbl_Stashed.TabIndex = 5;
            this.lbl_Stashed.Text = "0";
            // 
            // lbl_Sold
            // 
            this.lbl_Sold.AutoSize = true;
            this.lbl_Sold.Location = new System.Drawing.Point(61, 64);
            this.lbl_Sold.Name = "lbl_Sold";
            this.lbl_Sold.Size = new System.Drawing.Size(13, 13);
            this.lbl_Sold.TabIndex = 4;
            this.lbl_Sold.Text = "0";
            // 
            // lbl_Picked
            // 
            this.lbl_Picked.AutoSize = true;
            this.lbl_Picked.Location = new System.Drawing.Point(61, 16);
            this.lbl_Picked.Name = "lbl_Picked";
            this.lbl_Picked.Size = new System.Drawing.Size(13, 13);
            this.lbl_Picked.TabIndex = 3;
            this.lbl_Picked.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Stashed:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sold:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Picked:";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage_Status);
            this.tabControl.Controls.Add(this.tabPage_About);
            this.tabControl.Location = new System.Drawing.Point(-4, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(334, 398);
            this.tabControl.TabIndex = 6;
            // 
            // tabPage_Status
            // 
            this.tabPage_Status.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPage_Status.Controls.Add(this.gb_Status);
            this.tabPage_Status.Controls.Add(this.gb_error);
            this.tabPage_Status.Controls.Add(this.gb_Items);
            this.tabPage_Status.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Status.Name = "tabPage_Status";
            this.tabPage_Status.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Status.Size = new System.Drawing.Size(326, 372);
            this.tabPage_Status.TabIndex = 0;
            this.tabPage_Status.Text = "Global Status";
            // 
            // gb_Status
            // 
            this.gb_Status.Controls.Add(this.btn_hidestat);
            this.gb_Status.Controls.Add(this.btn_showstat);
            this.gb_Status.Enabled = false;
            this.gb_Status.Location = new System.Drawing.Point(167, 290);
            this.gb_Status.Name = "gb_Status";
            this.gb_Status.Size = new System.Drawing.Size(153, 79);
            this.gb_Status.TabIndex = 6;
            this.gb_Status.TabStop = false;
            this.gb_Status.Text = "Bot Status Window Control";
            // 
            // btn_hidestat
            // 
            this.btn_hidestat.Location = new System.Drawing.Point(42, 44);
            this.btn_hidestat.Name = "btn_hidestat";
            this.btn_hidestat.Size = new System.Drawing.Size(75, 23);
            this.btn_hidestat.TabIndex = 1;
            this.btn_hidestat.Text = "Hide Status";
            this.btn_hidestat.UseVisualStyleBackColor = true;
            this.btn_hidestat.Click += new System.EventHandler(this.btn_hidestat_Click);
            // 
            // btn_showstat
            // 
            this.btn_showstat.Location = new System.Drawing.Point(42, 19);
            this.btn_showstat.Name = "btn_showstat";
            this.btn_showstat.Size = new System.Drawing.Size(75, 23);
            this.btn_showstat.TabIndex = 0;
            this.btn_showstat.Text = "Show Status";
            this.btn_showstat.UseVisualStyleBackColor = true;
            this.btn_showstat.Click += new System.EventHandler(this.btn_showstat_Click);
            // 
            // gb_error
            // 
            this.gb_error.Controls.Add(this.btn_clearerrors);
            this.gb_error.Controls.Add(this.listView_Error);
            this.gb_error.Location = new System.Drawing.Point(6, 6);
            this.gb_error.Name = "gb_error";
            this.gb_error.Size = new System.Drawing.Size(313, 278);
            this.gb_error.TabIndex = 5;
            this.gb_error.TabStop = false;
            this.gb_error.Text = "Error list";
            // 
            // btn_clearerrors
            // 
            this.btn_clearerrors.Location = new System.Drawing.Point(126, 249);
            this.btn_clearerrors.Name = "btn_clearerrors";
            this.btn_clearerrors.Size = new System.Drawing.Size(100, 25);
            this.btn_clearerrors.TabIndex = 2;
            this.btn_clearerrors.Text = "Clear Errors";
            this.btn_clearerrors.UseVisualStyleBackColor = true;
            this.btn_clearerrors.Click += new System.EventHandler(this.btn_clearerrors_Click);
            // 
            // listView_Error
            // 
            this.listView_Error.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.listView_Error.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.column_ErrorCode,
            this.column_Description,
            this.column_Time,
            this.columnProfile});
            this.listView_Error.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.listView_Error.FullRowSelect = true;
            this.listView_Error.Location = new System.Drawing.Point(6, 17);
            this.listView_Error.Name = "listView_Error";
            this.listView_Error.Size = new System.Drawing.Size(301, 226);
            this.listView_Error.TabIndex = 1;
            this.listView_Error.UseCompatibleStateImageBehavior = false;
            this.listView_Error.View = System.Windows.Forms.View.Details;
            // 
            // column_ErrorCode
            // 
            this.column_ErrorCode.Text = "Error Code";
            this.column_ErrorCode.Width = 74;
            // 
            // column_Description
            // 
            this.column_Description.Text = "Description";
            this.column_Description.Width = 112;
            // 
            // column_Time
            // 
            this.column_Time.Text = "Time";
            this.column_Time.Width = 47;
            // 
            // columnProfile
            // 
            this.columnProfile.Text = "Profile";
            this.columnProfile.Width = 64;
            // 
            // tabPage_About
            // 
            this.tabPage_About.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tabPage_About.Controls.Add(this.linkLabel4);
            this.tabPage_About.Controls.Add(this.label21);
            this.tabPage_About.Controls.Add(this.linkLabel3);
            this.tabPage_About.Controls.Add(this.label20);
            this.tabPage_About.Controls.Add(this.linkLabel2);
            this.tabPage_About.Controls.Add(this.label19);
            this.tabPage_About.Controls.Add(this.linkLabel1);
            this.tabPage_About.Controls.Add(this.label18);
            this.tabPage_About.Controls.Add(this.label17);
            this.tabPage_About.Controls.Add(this.label16);
            this.tabPage_About.Controls.Add(this.label15);
            this.tabPage_About.Controls.Add(this.label14);
            this.tabPage_About.Controls.Add(this.label13);
            this.tabPage_About.Controls.Add(this.label12);
            this.tabPage_About.Controls.Add(this.label11);
            this.tabPage_About.Location = new System.Drawing.Point(4, 22);
            this.tabPage_About.Name = "tabPage_About";
            this.tabPage_About.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_About.Size = new System.Drawing.Size(326, 372);
            this.tabPage_About.TabIndex = 2;
            this.tabPage_About.Text = "About";
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(77, 244);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(265, 13);
            this.linkLabel4.TabIndex = 14;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "http://www.assembla.com/spaces/aomanager/stream";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 244);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 13);
            this.label21.TabIndex = 13;
            this.label21.Text = "Manager Svn:";
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(71, 224);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(253, 13);
            this.linkLabel3.TabIndex = 12;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "http://www.assembla.com/spaces/aobotlua/stream";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 224);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "Script Svn:";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(71, 204);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(254, 13);
            this.linkLabel2.TabIndex = 10;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "http://code.assembla.com/aobot/subversion/nodes";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 204);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 9;
            this.label19.Text = "Core Svn:";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(71, 184);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(79, 13);
            this.linkLabel1.TabIndex = 8;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "www.aobot.org";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 184);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Support:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 171);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Links:";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(6, 111);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(290, 60);
            this.label16.TabIndex = 5;
            this.label16.Text = "Sheppard, emjay for the clientless map reading.\r\nfoosoft, zoxc for Redvex,\r\nlord2" +
                "800 for C# help\r\nand all countless people who developed RedVex plugins";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 98);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Credits:";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(6, 279);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(290, 90);
            this.label14.TabIndex = 3;
            this.label14.Text = "Original Developers : kingsob, irrlicht, ejact, zoohnoes\r\n\r\nCurrent Developers : " +
                "Rakshasa\r\n\r\nSpecial Thanks : linkage2020, nathan259, stickybomb, polite, Master1" +
                "5, xGetReal";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 266);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(131, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "The Awesom-O Team:";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(6, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(284, 72);
            this.label12.TabIndex = 1;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "AoManager:";
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(326, 434);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btn_BotSettings);
            this.Controls.Add(this.btn_AoSettings);
            this.Controls.Add(this.btn_Start);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Awesom-O Manager Beta 4";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_main_FormClosing);
            this.Shown += new System.EventHandler(this.frm_main_Shown);
            this.Resize += new System.EventHandler(this.frm_main_Resize);
            this.trayIconMenu.ResumeLayout(false);
            this.gb_Items.ResumeLayout(false);
            this.gb_Items.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage_Status.ResumeLayout(false);
            this.gb_Status.ResumeLayout(false);
            this.gb_error.ResumeLayout(false);
            this.tabPage_About.ResumeLayout(false);
            this.tabPage_About.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_BotSettings;
        private System.Windows.Forms.Button btn_AoSettings;
        private System.Windows.Forms.Button btn_Start;
        public System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.ContextMenuStrip trayIconMenu;
        private System.Windows.Forms.ToolStripMenuItem MenuItemStart;
        private System.Windows.Forms.GroupBox gb_Items;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Items;
        private System.Windows.Forms.Label lbl_Stashed;
        private System.Windows.Forms.Label lbl_Sold;
        private System.Windows.Forms.Label lbl_Picked;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hideDiabloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showCoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hideCoreToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage_Status;
        private System.Windows.Forms.TabPage tabPage_About;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lbl_Gambled;
        private System.Windows.Forms.GroupBox gb_error;
        private System.Windows.Forms.Button btn_clearerrors;
        private System.Windows.Forms.ListView listView_Error;
        private System.Windows.Forms.ColumnHeader column_ErrorCode;
        private System.Windows.Forms.ColumnHeader column_Description;
        private System.Windows.Forms.ColumnHeader column_Time;
        private System.Windows.Forms.ColumnHeader columnProfile;
        private System.Windows.Forms.GroupBox gb_Status;
        private System.Windows.Forms.Button btn_showstat;
        private System.Windows.Forms.Button btn_hidestat;
    }
}

