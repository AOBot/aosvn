﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace AoCoreLauncher
{
    public partial class Form1 : Form
    {
        AoSettingStruct AoConfig;

        public Form1()
        {
            InitializeComponent();
            if (!File.Exists(AoSettings.xmlpath))
            {
                MessageBox.Show("Can't load Settings, please configure the Ao Settings in Aomanager first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                AoConfig = AoSettings.GetSettings();
                if (AoConfig == null)
                {
                    MessageBox.Show("Can't load Settings, please configure the Ao Settings in Aomanager first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (!RunAO())
                        MessageBox.Show("Could not start the Awesom-o core!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            this.Dispose();
            Environment.Exit(0);
        }

        private bool RunAO()
        {
            Process AobotProc;

            try
            {
                AobotProc = new Process();
                AobotProc.StartInfo.FileName = AoConfig.AOPath + "\\Awesom-O.exe";
                AobotProc.StartInfo.Arguments = AoConfig.AoFlags;
                AobotProc.StartInfo.WorkingDirectory = AoConfig.AOPath;
                AobotProc.StartInfo.ErrorDialog = false;
                AobotProc.EnableRaisingEvents = true;
                bool Started = AobotProc.Start();
                //
                Thread.Sleep(2000);
                //
                return Started;
            }
            catch (Exception) { return false; }
        }
    }
}
