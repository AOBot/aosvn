﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace AoCoreLauncher
{

    [XmlRoot("Profile")]
    public class ProfileStruct
    {
        public string ProfileName;
        public string DiabloPath;
        public string DiabloOwnerName;
        public string DiabloAccount;
        public string DiabloPassword;
        public string Channel;
        public string GameName;
        public string GamePassword;
        public bool HasGamePass;
        public string NextGameMessage;
        public string TooShortMessage;
        public int DiabloCharPosition;
        public int GameDifficulty;
        public int MaxGameTime;
        public int MinGameTime;
        public int MaxGamesAnHour;
        public int BanWaitTime;
        public int CreateGameDelay;
        public int JoinGameDelay;
        public int StartRunDelay;
        public int KeyChange;
        public bool HideDiabloWindow;
        public bool UseD2Launcher;
        public bool Follower;
        public bool Cobaaler;
        public bool Joiner;
        public string MasterAccount;
        public string MasterCharacter;
        public string FollowGamePassword;
        public List<string> ClassicKeys;
        public List<string> ExpansionKeys;
        public bool AutoMule;  //nathan's mule
        public string MuleAccount; //nathan's mule
        public string MulePassword; //nathan's mule
        public string MuleCharNamePrefix; //nathan's mule
        public bool NowMuling; //nathan's mule
        public int MuleCharPos; //nathan's mule
        public string MuleGameName;
        public string MuleGamePassword;
        public int GameDifficultySave;
        public bool MakeMuleGame;
      

        public ProfileStruct() { }
    }

    public static class ProfileSettings
    {
        public static string xmlpath = Application.StartupPath + "\\AoManagerProfiles.xml";

        public static void WriteSettings(List<ProfileStruct> profiles)
        {
            XmlSer.Serialize(xmlpath, typeof(List<ProfileStruct>), profiles);
        }

        public static List<ProfileStruct> GetSettings()
        {
            List<ProfileStruct> profiles = (List<ProfileStruct>)XmlSer.DeSerialize(xmlpath, typeof(List<ProfileStruct>));
            return profiles;
        }

    }

    public static class AoSettings
    {
        public static string xmlpath = Application.StartupPath + "\\AoManager.xml";

        public static void WriteSettings(AoSettingStruct config)
        {
            XmlSer.Serialize(xmlpath, typeof(AoSettingStruct), config);
        }

        public static AoSettingStruct GetSettings()
        {
            AoSettingStruct config =(AoSettingStruct) XmlSer.DeSerialize(xmlpath, typeof(AoSettingStruct));
            return config;
        }

    }

    [XmlRoot("AoSettingStruct")]
    public class AoSettingStruct
    {
        public string AOPath;
        public string DiabloFolder;
        public string AoFlags;
        public string Realm;
        public bool Expert;
        public bool Stealth;
        public bool AoNoStart;
        public int ClickDelay;
        public int KeyDelay;
        public int StartDelay;
        public string ControllerIp;
        public bool ExpertIp;
        public bool HideCore;
        public bool IpChangerUse;

        public AoSettingStruct() { }
        
    }

    public static class IpChanger
    {
        public static string xmlpath = Application.StartupPath + "\\IPChanger.xml";

        public static void WriteSettings(IpChangerStruct config)
        {
            XmlSer.Serialize(xmlpath, typeof(IpChangerStruct), config);
        }

        public static IpChangerStruct GetSettings()
        {
            IpChangerStruct config = (IpChangerStruct)XmlSer.DeSerialize(xmlpath, typeof(IpChangerStruct));
            return config;
        }

    }

    [XmlRoot("IpChangerStruct")]
    public class IpChangerStruct
    {
        public string ip;
        public string login;
        public string disconnect;
        public string connect;
        public string username;
        public string password;
        public string preAuthUrl;
        public string challengeUrl;
        public string challengeSearch;
        public bool post;
        public bool postDisconnect;
        public bool postReconnect;
        public bool preAuth;
        public bool challenge;
        public bool autoReconnect;

        public IpChangerStruct() { }
    }
}
